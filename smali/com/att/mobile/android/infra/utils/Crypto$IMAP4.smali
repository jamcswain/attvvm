.class public Lcom/att/mobile/android/infra/utils/Crypto$IMAP4;
.super Ljava/lang/Object;
.source "Crypto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/infra/utils/Crypto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IMAP4"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "base64Password"    # Ljava/lang/String;
    .param p1, "tn"    # Ljava/lang/String;

    .prologue
    .line 174
    const-string v1, "luckytechnologycompany"

    .line 175
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 177
    .local v6, "tmpKey":[C
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v7, v6

    if-ge v0, v7, :cond_1

    const-string v7, "luckytechnologycompany"

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v0, v7, :cond_1

    .line 179
    aget-char v7, v6, v0

    const-string v8, "luckytechnologycompany"

    invoke-virtual {v8, v0}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-eq v7, v8, :cond_0

    .line 181
    aget-char v7, v6, v0

    const-string v8, "luckytechnologycompany"

    invoke-virtual {v8, v0}, Ljava/lang/String;->charAt(I)C

    move-result v8

    xor-int/2addr v7, v8

    int-to-char v7, v7

    aput-char v7, v6, v0

    .line 182
    aget-char v7, v6, v0

    or-int/lit8 v7, v7, 0x60

    int-to-char v7, v7

    aput-char v7, v6, v0

    .line 183
    aget-char v7, v6, v0

    and-int/lit8 v7, v7, 0x6f

    int-to-char v7, v7

    aput-char v7, v6, v0

    .line 177
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    :cond_1
    const/4 v7, 0x0

    invoke-static {p0, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v5

    .line 191
    .local v5, "passwdBA":[B
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 192
    .local v3, "passwBB":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v4

    .line 193
    .local v4, "passwCB":Ljava/nio/CharBuffer;
    invoke-virtual {v4}, Ljava/nio/CharBuffer;->limit()I

    move-result v7

    new-array v2, v7, [C

    .line 194
    .local v2, "output":[C
    invoke-virtual {v4, v2}, Ljava/nio/CharBuffer;->get([C)Ljava/nio/CharBuffer;

    .line 196
    const/4 v0, 0x0

    :goto_1
    array-length v7, v2

    if-ge v0, v7, :cond_3

    array-length v7, v6

    if-ge v0, v7, :cond_3

    .line 198
    aget-char v7, v2, v0

    aget-char v8, v6, v0

    if-eq v7, v8, :cond_2

    .line 199
    aget-char v7, v2, v0

    aget-char v8, v6, v0

    xor-int/2addr v7, v8

    int-to-char v7, v7

    aput-char v7, v2, v0

    .line 196
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 203
    :cond_3
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v2}, Ljava/lang/String;-><init>([C)V

    return-object v7
.end method

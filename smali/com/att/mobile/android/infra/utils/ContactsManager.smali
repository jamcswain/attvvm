.class public Lcom/att/mobile/android/infra/utils/ContactsManager;
.super Ljava/lang/Object;
.source "ContactsManager.java"


# static fields
.field private static sInstance:Lcom/att/mobile/android/infra/utils/ContactsManager;


# instance fields
.field private context:Landroid/content/Context;

.field private phoneLookup:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/att/mobile/android/vvm/model/Contact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/att/mobile/android/infra/utils/ContactsManager;

    invoke-direct {v0}, Lcom/att/mobile/android/infra/utils/ContactsManager;-><init>()V

    sput-object v0, Lcom/att/mobile/android/infra/utils/ContactsManager;->sInstance:Lcom/att/mobile/android/infra/utils/ContactsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/infra/utils/ContactsManager;->phoneLookup:Ljava/util/HashMap;

    .line 36
    return-void
.end method

.method private createContactFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/Contact;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    .line 107
    new-instance v1, Lcom/att/mobile/android/vvm/model/Contact;

    invoke-direct {v1}, Lcom/att/mobile/android/vvm/model/Contact;-><init>()V

    .line 108
    .local v1, "result":Lcom/att/mobile/android/vvm/model/Contact;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109
    invoke-static {p1}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContactId(Landroid/database/Cursor;)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/att/mobile/android/vvm/model/Contact;->contactId:J

    .line 110
    invoke-direct {p0, v1, p2}, Lcom/att/mobile/android/infra/utils/ContactsManager;->initContactNames(Lcom/att/mobile/android/vvm/model/Contact;Ljava/lang/String;)V

    .line 112
    move-object v0, v1

    .line 113
    .local v0, "current":Lcom/att/mobile/android/vvm/model/Contact;
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    new-instance v2, Lcom/att/mobile/android/vvm/model/Contact;

    invoke-direct {v2}, Lcom/att/mobile/android/vvm/model/Contact;-><init>()V

    .line 115
    .local v2, "tmp":Lcom/att/mobile/android/vvm/model/Contact;
    invoke-static {p1}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContactId(Landroid/database/Cursor;)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/att/mobile/android/vvm/model/Contact;->contactId:J

    .line 116
    invoke-direct {p0, v2, p2}, Lcom/att/mobile/android/infra/utils/ContactsManager;->initContactNames(Lcom/att/mobile/android/vvm/model/Contact;Ljava/lang/String;)V

    .line 117
    iput-object v2, v0, Lcom/att/mobile/android/vvm/model/Contact;->next:Lcom/att/mobile/android/vvm/model/Contact;

    .line 118
    move-object v0, v2

    .line 119
    goto :goto_0

    .line 121
    .end local v0    # "current":Lcom/att/mobile/android/vvm/model/Contact;
    .end local v2    # "tmp":Lcom/att/mobile/android/vvm/model/Contact;
    :cond_0
    return-object v1
.end method

.method private static getContactDisplayName(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 180
    const-string v2, "data1"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 181
    .local v0, "index":I
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "result":Ljava/lang/String;
    return-object v1
.end method

.method private static getContactFirstName(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 186
    const-string v2, "data2"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 187
    .local v0, "index":I
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "result":Ljava/lang/String;
    return-object v1
.end method

.method private static getContactLastName(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 198
    const-string v2, "data3"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 199
    .local v0, "index":I
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "result":Ljava/lang/String;
    return-object v1
.end method

.method private static getContactMidName(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 192
    const-string v2, "data5"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 193
    .local v0, "index":I
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 194
    .local v1, "result":Ljava/lang/String;
    return-object v1
.end method

.method private static getContactName(Landroid/content/Context;J)Landroid/database/Cursor;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contactId"    # J

    .prologue
    const/4 v2, 0x0

    .line 162
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 163
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 164
    .local v1, "uri":Landroid/net/Uri;
    const-string v3, "contact_id = ? AND mimetype = ?"

    .line 165
    .local v3, "where":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, "vnd.android.cursor.item/name"

    aput-object v7, v4, v5

    .local v4, "whereArgs":[Ljava/lang/String;
    move-object v5, v2

    .line 168
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 169
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 170
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-nez v5, :cond_0

    .line 171
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 175
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v6

    goto :goto_0
.end method

.method public static getInstance()Lcom/att/mobile/android/infra/utils/ContactsManager;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/att/mobile/android/infra/utils/ContactsManager;->sInstance:Lcom/att/mobile/android/infra/utils/ContactsManager;

    return-object v0
.end method

.method private initContactNames(Lcom/att/mobile/android/vvm/model/Contact;Ljava/lang/String;)V
    .locals 6
    .param p1, "c"    # Lcom/att/mobile/android/vvm/model/Contact;
    .param p2, "num"    # Ljava/lang/String;

    .prologue
    .line 130
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/ContactsManager;->context:Landroid/content/Context;

    iget-wide v4, p1, Lcom/att/mobile/android/vvm/model/Contact;->contactId:J

    invoke-static {v2, v4, v5}, Lcom/att/mobile/android/infra/utils/ContactsManager;->getContactName(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v1

    .line 131
    .local v1, "nameCursor":Landroid/database/Cursor;
    const-string v2, ""

    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->displayName:Ljava/lang/String;

    .line 132
    if-eqz v1, :cond_1

    .line 134
    :try_start_0
    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/ContactsManager;->getContactDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->displayName:Ljava/lang/String;

    .line 135
    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/ContactsManager;->getContactFirstName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->firstName:Ljava/lang/String;

    .line 137
    iget-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->firstName:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 138
    const-string v2, ""

    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->firstName:Ljava/lang/String;

    .line 139
    :cond_0
    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/ContactsManager;->getContactMidName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->middleName:Ljava/lang/String;

    .line 140
    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/ContactsManager;->getContactLastName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->lastName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 145
    :cond_1
    iget-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->displayName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 146
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/ContactsManager;->context:Landroid/content/Context;

    iget-wide v4, p1, Lcom/att/mobile/android/vvm/model/Contact;->contactId:J

    invoke-static {v2, v4, v5}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContactInfo(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v0

    .line 147
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    .line 149
    :try_start_1
    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContactDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->displayName:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 151
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 154
    :cond_2
    iget-object v2, p1, Lcom/att/mobile/android/vvm/model/Contact;->displayName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 155
    iput-object p2, p1, Lcom/att/mobile/android/vvm/model/Contact;->displayName:Ljava/lang/String;

    .line 157
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_3
    return-void

    .line 142
    :catchall_0
    move-exception v2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v2

    .line 151
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v2
.end method


# virtual methods
.method public lookupContactByPhone(Landroid/content/Context;Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/Contact;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 60
    iput-object p1, p0, Lcom/att/mobile/android/infra/utils/ContactsManager;->context:Landroid/content/Context;

    .line 62
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v1, v2

    .line 95
    :cond_0
    :goto_0
    return-object v1

    .line 67
    :cond_1
    iget-object v3, p0, Lcom/att/mobile/android/infra/utils/ContactsManager;->phoneLookup:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/vvm/model/Contact;

    .line 68
    .local v1, "result":Lcom/att/mobile/android/vvm/model/Contact;
    if-nez v1, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 76
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p1, p2}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContactInfoByPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 77
    if-nez v0, :cond_3

    .line 93
    if-eqz v0, :cond_2

    .line 95
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v1, v2

    goto :goto_0

    .line 82
    :cond_3
    :try_start_1
    invoke-direct {p0, v0, p2}, Lcom/att/mobile/android/infra/utils/ContactsManager;->createContactFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/Contact;

    move-result-object v1

    .line 86
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/ContactsManager;->phoneLookup:Ljava/util/HashMap;

    invoke-virtual {v2, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    if-eqz v0, :cond_0

    .line 95
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_4

    .line 95
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
.end method

.method public resetCache()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/att/mobile/android/infra/utils/ContactsManager;->phoneLookup:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 48
    return-void
.end method

.class public final enum Lcom/att/mobile/android/infra/utils/BluetoothRouter;
.super Ljava/lang/Enum;
.source "BluetoothRouter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/att/mobile/android/infra/utils/BluetoothRouter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/att/mobile/android/infra/utils/BluetoothRouter;

.field public static final ADD_CONNECTED_DEVICE:I = 0x67

.field public static final EXTRA_DEVICE_ADDRESS:Ljava/lang/String; = "deviceAddress"

.field public static final enum INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

.field public static final LOG_TAG:Ljava/lang/String; = "BluetoothRouter"

.field public static final REMOVE_CONNECTED_DEVICE:I = 0x68

.field public static final START_ROUTE:I = 0x65

.field public static final STOP_ROUTE:I = 0x66

.field private static bluetoothLockObject:Ljava/lang/Object;


# instance fields
.field private bluetoothConnectionsHandler:Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;

.field private connectedDevices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private helperHandlerThread:Landroid/os/HandlerThread;

.field private routed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    sget-object v1, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    aput-object v1, v0, v2

    sput-object v0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->$VALUES:[Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->bluetoothLockObject:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->routed:Z

    .line 75
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BluetoothRouter"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->helperHandlerThread:Landroid/os/HandlerThread;

    .line 76
    iget-object v0, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->helperHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/infra/utils/BluetoothRouter;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/infra/utils/BluetoothRouter;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->addConnectedDevice(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/att/mobile/android/infra/utils/BluetoothRouter;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/infra/utils/BluetoothRouter;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->removeConnectedDevice(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private addConnectedDevice(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 200
    :try_start_0
    sget-object v2, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->bluetoothLockObject:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :try_start_1
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 202
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->loadConnectedDevices(Landroid/content/Context;)V

    .line 204
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    .line 205
    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 206
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_1
    invoke-direct {p0, p1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->saveConnectedDevices(Landroid/content/Context;)V

    .line 209
    monitor-exit v2

    .line 213
    :goto_0
    return-void

    .line 209
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BluetoothRouter"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private removeConnectedDevice(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 224
    :try_start_0
    sget-object v2, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->bluetoothLockObject:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    :try_start_1
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 226
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->loadConnectedDevices(Landroid/content/Context;)V

    .line 228
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    if-eqz v1, :cond_1

    .line 229
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 231
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 232
    :cond_2
    iget-boolean v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->routed:Z

    if-eqz v1, :cond_3

    .line 233
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->stopRouteAudioToBluetooth(Landroid/content/Context;)V

    .line 234
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->routed:Z

    .line 237
    :cond_3
    invoke-direct {p0, p1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->saveConnectedDevices(Landroid/content/Context;)V

    .line 238
    monitor-exit v2

    .line 242
    :goto_0
    return-void

    .line 238
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BluetoothRouter"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private saveConnectedDevices(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v6, 0x7f0800c6

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 252
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 254
    const-string v1, "BluetoothRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BluetoothRouterHandler.saveConnectedDevices() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    .line 255
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " connected Bluetooth devices"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 254
    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "connectedbt.ser"

    aput-object v3, v2, v4

    .line 258
    invoke-virtual {p1, v6, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 257
    invoke-static {p1, v1, v2}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->saveSerializable(Landroid/content/Context;Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    .line 261
    .local v0, "success":Z
    if-eqz v0, :cond_0

    .line 262
    const-string v1, "BluetoothRouter"

    const-string v2, "BluetoothRouterHandler.saveConnectedDevices() file saved"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :goto_0
    return-void

    .line 265
    :cond_0
    const-string v1, "BluetoothRouter"

    const-string v2, "BluetoothRouterHandler.saveConnectedDevices() failed to save file"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 271
    .end local v0    # "success":Z
    :cond_1
    const-string v1, "BluetoothRouter"

    const-string v2, "BluetoothRouterHandler.saveConnectedDevices() no connected Bluetooth devices"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "connectedbt.ser"

    aput-object v2, v1, v4

    invoke-virtual {p1, v6, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 277
    .restart local v0    # "success":Z
    if-eqz v0, :cond_2

    .line 278
    const-string v1, "BluetoothRouter"

    const-string v2, "BluetoothRouterHandler.saveConnectedDevices() file deleted"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 281
    :cond_2
    const-string v1, "BluetoothRouter"

    const-string v2, "BluetoothRouterHandler.saveConnectedDevices() failed to delete file"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/att/mobile/android/infra/utils/BluetoothRouter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    return-object v0
.end method

.method public static values()[Lcom/att/mobile/android/infra/utils/BluetoothRouter;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->$VALUES:[Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    invoke-virtual {v0}, [Lcom/att/mobile/android/infra/utils/BluetoothRouter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    return-object v0
.end method


# virtual methods
.method public deviceConnectionStateChanged(ILjava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p1, "eventId"    # I
    .param p2, "deviceAddress"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 316
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->bluetoothConnectionsHandler:Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;

    if-nez v2, :cond_0

    .line 317
    new-instance v2, Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;

    iget-object v3, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->helperHandlerThread:Landroid/os/HandlerThread;

    .line 318
    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;-><init>(Lcom/att/mobile/android/infra/utils/BluetoothRouter;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->bluetoothConnectionsHandler:Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;

    .line 321
    :cond_0
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->bluetoothConnectionsHandler:Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;

    invoke-virtual {v2, p1, p3}, Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 323
    .local v1, "message":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 324
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "deviceAddress"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 326
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->bluetoothConnectionsHandler:Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;

    invoke-virtual {v2, v1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;->sendMessage(Landroid/os/Message;)Z

    .line 327
    return-void
.end method

.method public loadConnectedDevices(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 293
    const v1, 0x7f0800c6

    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "connectedbt.ser"

    aput-object v4, v2, v3

    .line 294
    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-static {p1, v1}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->loadSerializable(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    iput-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :goto_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 300
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 301
    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    .line 303
    :cond_0
    const-string v1, "BluetoothRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BluetoothRouterHandler.loadConnectedDevices() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    .line 304
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " connected devices"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 303
    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    return-void

    .line 296
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BluetoothRouter"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public notifyRouted(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    const-string v0, "BluetoothRouter"

    const-string v1, "notifyRouted()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    return-void
.end method

.method public startRouteAudioToBluetooth(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    const-string v2, "audio"

    .line 90
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 92
    .local v0, "audioManager":Landroid/media/AudioManager;
    const-string v2, "BluetoothRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startRouteAudioToBluetooth() isBluetoothScoAvailableOffCall = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 94
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 92
    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string v2, "BluetoothRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startRouteAudioToBluetooth() isBluetoothScoOn = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 96
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 95
    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v2, "BluetoothRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startRouteAudioToBluetooth() isBluetoothA2dpOn = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 98
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 97
    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->connectedDevices:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 107
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoAvailableOffCall()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v2

    if-nez v2, :cond_0

    .line 110
    const-string v2, "BluetoothRouter"

    const-string v3, "routeAudioToBluetooth() going to route audio to Bluetooth device"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 115
    :try_start_0
    invoke-virtual {v0}, Landroid/media/AudioManager;->startBluetoothSco()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 120
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setMode(I)V

    .line 127
    :cond_0
    return-void

    .line 116
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v2, "BluetoothRouter"

    const-string v3, "startBluetoothSco() failed. no bluetooth device connected."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public stopRouteAudioToBluetooth(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 136
    const-string v1, "audio"

    .line 137
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 138
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    .line 139
    invoke-virtual {v0}, Landroid/media/AudioManager;->stopBluetoothSco()V

    .line 140
    iput-boolean v2, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->routed:Z

    .line 141
    return-void
.end method

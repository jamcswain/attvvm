.class public Lcom/att/mobile/android/infra/utils/AlertDlgUtils;
.super Ljava/lang/Object;
.source "AlertDlgUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field static body:Landroid/widget/TextView;

.field static header:Landroid/widget/TextView;

.field static negativeButton:Landroid/widget/Button;

.field static positiveButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static initRAUIElements(Landroid/support/v7/app/AlertDialog;)V
    .locals 4
    .param p0, "dialog"    # Landroid/support/v7/app/AlertDialog;

    .prologue
    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 235
    const v1, 0x7f0f008e

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sput-object v1, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    .line 236
    const v1, 0x7f0f008f

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sput-object v1, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->body:Landroid/widget/TextView;

    .line 237
    const v1, 0x7f0f0092

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sput-object v1, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    .line 238
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 242
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 243
    sget-object v1, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 244
    const v1, 0x7f0f0091

    invoke-virtual {p0, v1}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sput-object v1, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    .line 245
    return-void
.end method

.method private static initUIElements(Landroid/support/v7/app/AlertDialog;)V
    .locals 1
    .param p0, "dialog"    # Landroid/support/v7/app/AlertDialog;

    .prologue
    .line 229
    const v0, 0x7f0f008e

    invoke-virtual {p0, v0}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    .line 230
    const v0, 0x7f0f008f

    invoke-virtual {p0, v0}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->body:Landroid/widget/TextView;

    .line 231
    const v0, 0x7f0f0092

    invoke-virtual {p0, v0}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    .line 232
    const v0, 0x7f0f0091

    invoke-virtual {p0, v0}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    .line 233
    return-void
.end method

.method private static setTypeface()V
    .locals 4

    .prologue
    const v3, 0x7f0200d1

    const/4 v2, 0x1

    .line 114
    sget-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 115
    sget-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->body:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 116
    sget-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 118
    sget-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 119
    sget-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 120
    sget-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 121
    sget-object v0, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 122
    return-void
.end method

.method public static showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "headerText"    # I
    .param p2, "bodyText"    # I
    .param p3, "positiveBtn"    # I
    .param p4, "negativeBtn"    # I
    .param p5, "cancelOnTouchOutside"    # Z
    .param p6, "btnActions"    # Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 47
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 48
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f040021

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setView(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 52
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1, p5}, Landroid/support/v7/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 53
    invoke-virtual {v1, p5}, Landroid/support/v7/app/AlertDialog;->setCancelable(Z)V

    .line 55
    new-instance v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$2;

    invoke-direct {v2, p5, v1}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$2;-><init>(ZLandroid/support/v7/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 69
    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->initUIElements(Landroid/support/v7/app/AlertDialog;)V

    .line 71
    if-nez p1, :cond_0

    .line 72
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    :goto_0
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->body:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(I)V

    .line 79
    invoke-static {}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->setTypeface()V

    .line 81
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v2, p3}, Landroid/widget/Button;->setText(I)V

    .line 82
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    new-instance v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$3;

    invoke-direct {v3, p6, v1}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$3;-><init>(Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;Landroid/support/v7/app/AlertDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    if-nez p4, :cond_1

    .line 94
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 111
    :goto_1
    return-void

    .line 74
    :cond_0
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 96
    :cond_1
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 97
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v2, p4}, Landroid/widget/Button;->setText(I)V

    .line 98
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 99
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    new-instance v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$4;

    invoke-direct {v3, p6, v1}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$4;-><init>(Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;Landroid/support/v7/app/AlertDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public static showDialogWithCB(Landroid/content/Context;IIIIILcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "headerText"    # I
    .param p2, "bodyText"    # I
    .param p3, "checkboxText"    # I
    .param p4, "positiveBtn"    # I
    .param p5, "negativeBtn"    # I
    .param p6, "btnActions"    # Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 126
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 127
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v3, 0x7f040021

    invoke-virtual {v0, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setView(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 129
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object v2

    .line 130
    .local v2, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->initUIElements(Landroid/support/v7/app/AlertDialog;)V

    .line 132
    const v3, 0x7f0f0090

    invoke-virtual {v2, v3}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 133
    .local v1, "cb":Landroid/widget/CheckBox;
    if-nez p3, :cond_0

    .line 134
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 149
    :goto_0
    if-nez p1, :cond_1

    .line 150
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    :goto_1
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->body:Landroid/widget/TextView;

    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(I)V

    .line 157
    invoke-static {}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->setTypeface()V

    .line 159
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v3, p4}, Landroid/widget/Button;->setText(I)V

    .line 160
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    new-instance v4, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$6;

    invoke-direct {v4, p6, v2}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$6;-><init>(Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;Landroid/support/v7/app/AlertDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    if-nez p5, :cond_2

    .line 169
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 185
    :goto_2
    return-void

    .line 136
    :cond_0
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 137
    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 138
    invoke-virtual {v1, p3}, Landroid/widget/CheckBox;->setText(I)V

    .line 140
    new-instance v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$5;

    invoke-direct {v3}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$5;-><init>()V

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 152
    :cond_1
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 153
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 171
    :cond_2
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 172
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v3, p5}, Landroid/widget/Button;->setText(I)V

    .line 173
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 174
    sget-object v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    new-instance v4, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$7;

    invoke-direct {v4, p6, v2}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$7;-><init>(Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;Landroid/support/v7/app/AlertDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method public static showNoNetworkConnectionDialog(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0800f3

    .line 34
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08013b

    .line 35
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$1;

    invoke-direct {v2}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$1;-><init>()V

    .line 36
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    .line 41
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 43
    return-void
.end method

.method public static showRightAlignedDialog(Landroid/content/Context;IIIILcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "headerText"    # I
    .param p2, "bodyText"    # I
    .param p3, "positiveBtn"    # I
    .param p4, "negativeBtn"    # I
    .param p5, "btnActions"    # Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 188
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 189
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f040021

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setView(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 191
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 192
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->initRAUIElements(Landroid/support/v7/app/AlertDialog;)V

    .line 193
    if-nez p1, :cond_0

    .line 194
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 199
    :goto_0
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->body:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(I)V

    .line 201
    invoke-static {}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->setTypeface()V

    .line 203
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    invoke-virtual {v2, p3}, Landroid/widget/Button;->setText(I)V

    .line 204
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->positiveButton:Landroid/widget/Button;

    new-instance v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$8;

    invoke-direct {v3, p5, v1}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$8;-><init>(Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;Landroid/support/v7/app/AlertDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    if-nez p4, :cond_1

    .line 213
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 227
    :goto_1
    return-void

    .line 196
    :cond_0
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 197
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->header:Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 215
    :cond_1
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 216
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    invoke-virtual {v2, p4}, Landroid/widget/Button;->setText(I)V

    .line 217
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 218
    sget-object v2, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->negativeButton:Landroid/widget/Button;

    new-instance v3, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$9;

    invoke-direct {v3, p5, v1}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils$9;-><init>(Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;Landroid/support/v7/app/AlertDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

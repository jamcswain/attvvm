.class public Lcom/att/mobile/android/infra/utils/BluetoothRouterReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothRouterReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 21
    const-string v2, "BluetoothRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BluetoothRouterReceiver.onReceive() action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 22
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 21
    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.media.ACTION_SCO_AUDIO_STATE_UPDATED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 24
    const-string v2, "android.media.extra.SCO_AUDIO_STATE"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 25
    .local v1, "state":I
    const-string v2, "BluetoothRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BluetoothRouterReceiver.onReceive() state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    if-ne v1, v5, :cond_0

    .line 31
    const-string v2, "BluetoothRouter"

    const-string v3, "BluetoothRouterReceiver.onReceive() SCO_AUDIO_STATE_CONNECTED"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    sget-object v2, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    invoke-virtual {v2, p1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->notifyRouted(Landroid/content/Context;)V

    .line 55
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 41
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    .line 42
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 44
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    sget-object v2, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    const/16 v3, 0x67

    .line 45
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 44
    invoke-virtual {v2, v3, v4, p1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->deviceConnectionStateChanged(ILjava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 47
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    .line 50
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 51
    .restart local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    sget-object v2, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    const/16 v3, 0x68

    .line 53
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 51
    invoke-virtual {v2, v3, v4, p1}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->deviceConnectionStateChanged(ILjava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

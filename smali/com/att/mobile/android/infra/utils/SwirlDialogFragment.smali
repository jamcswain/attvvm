.class public Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SwirlDialogFragment.java"


# instance fields
.field loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

.field private mBodyText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    const v2, 0x7f04003b

    invoke-virtual {p1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 32
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0f0080

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    iput-object v2, p0, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    .line 34
    const v2, 0x7f0f00ea

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 35
    const v2, 0x7f0f00eb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 36
    .local v0, "bodyTextView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->mBodyText:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 43
    :goto_0
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->start()V

    .line 44
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 46
    return-object v1

    .line 39
    :cond_0
    iget-object v2, p0, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->mBodyText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method

.method public setBodyText(Ljava/lang/String;)V
    .locals 0
    .param p1, "bodyText"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->mBodyText:Ljava/lang/String;

    .line 26
    return-void
.end method

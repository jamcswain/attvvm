.class public final enum Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;
.super Ljava/lang/Enum;
.source "FontUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/infra/utils/FontUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FontNames"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum OmnesATT_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum OmnesATT_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum OmnesATT_MediumItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum OmnesATT_RegularItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum Roboto_Italic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum Roboto_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum Roboto_MediumItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

.field public static final enum Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "Roboto_Medium"

    invoke-direct {v0, v1, v3}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 25
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "Roboto_MediumItalic"

    invoke-direct {v0, v1, v4}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_MediumItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 26
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "Roboto_Italic"

    invoke-direct {v0, v1, v5}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Italic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 27
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "Roboto_Regular"

    invoke-direct {v0, v1, v6}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 28
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "OmnesATT_Medium"

    invoke-direct {v0, v1, v7}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 29
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "OmnesATT_MediumItalic"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_MediumItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 30
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "OmnesATT_RegularItalic"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_RegularItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 31
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "OmnesATT_Regular"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 32
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "Roboto_LightItalic"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 33
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "Roboto_Light"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 34
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "OmnesATT_LightItalic"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 35
    new-instance v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    const-string v1, "OmnesATT_Light"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .line 23
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v1, v0, v3

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_MediumItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v1, v0, v4

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Italic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v1, v0, v5

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v1, v0, v6

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_MediumItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_RegularItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    aput-object v2, v0, v1

    sput-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->$VALUES:[Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    return-object v0
.end method

.method public static values()[Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->$VALUES:[Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-virtual {v0}, [Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    return-object v0
.end method

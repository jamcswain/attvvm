.class Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;
.super Landroid/os/Handler;
.source "BluetoothRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/infra/utils/BluetoothRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothConnectionsHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/infra/utils/BluetoothRouter;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/infra/utils/BluetoothRouter;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;->this$0:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    .line 168
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 169
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 174
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x67

    if-ne v0, v1, :cond_0

    .line 175
    const-string v0, "BluetoothRouter"

    const-string v1, "BluetoothRouterHandler.handleMessage() ADD_CONNECTED_DEVICE"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;->this$0:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Context;

    .line 178
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "deviceAddress"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-static {v1, v0, v2}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->access$000(Lcom/att/mobile/android/infra/utils/BluetoothRouter;Landroid/content/Context;Ljava/lang/String;)V

    .line 187
    :goto_0
    return-void

    .line 179
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_1

    .line 180
    const-string v0, "BluetoothRouter"

    const-string v1, "BluetoothRouterHandler.handleMessage() REMOVE_CONNECTED_DEVICE"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/BluetoothRouter$BluetoothConnectionsHandler;->this$0:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Context;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "deviceAddress"

    .line 183
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 182
    invoke-static {v1, v0, v2}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->access$100(Lcom/att/mobile/android/infra/utils/BluetoothRouter;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method

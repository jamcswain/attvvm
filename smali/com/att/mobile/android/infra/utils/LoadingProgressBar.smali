.class public Lcom/att/mobile/android/infra/utils/LoadingProgressBar;
.super Landroid/view/View;
.source "LoadingProgressBar.java"


# static fields
.field private static final DEFAULT_SHADOW_POSITION:I = 0x2

.field private static final DEFAULT_WIDTH:I = 0x5


# instance fields
.field private arc:F

.field private bottomDegree:I

.field private changeBigger:Z

.field private color:I

.field private isStart:Z

.field private loadingRectF:Landroid/graphics/RectF;

.field private mPaint:Landroid/graphics/Paint;

.field private shadowPosition:I

.field private shadowRectF:Landroid/graphics/RectF;

.field private topDegree:I

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 33
    const/16 v0, 0xa

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    .line 34
    const/16 v0, 0xbe

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->changeBigger:Z

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart:Z

    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->initView(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/16 v0, 0xa

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    .line 34
    const/16 v0, 0xbe

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->changeBigger:Z

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart:Z

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->initView(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/16 v0, 0xa

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    .line 34
    const/16 v0, 0xbe

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->changeBigger:Z

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart:Z

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->initView(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method static synthetic access$002(Lcom/att/mobile/android/infra/utils/LoadingProgressBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/infra/utils/LoadingProgressBar;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart:Z

    return p1
.end method

.method private initView(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const v4, -0x777778

    const/high16 v3, 0x40a00000    # 5.0f

    .line 64
    iput v4, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->color:I

    .line 65
    invoke-virtual {p0, p1, v3}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->dpToPx(Landroid/content/Context;F)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    .line 66
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {p0, v1, v2}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->dpToPx(Landroid/content/Context;F)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowPosition:I

    .line 68
    if-eqz p2, :cond_0

    .line 69
    sget-object v1, Lcom/att/mobile/android/vvm/R$styleable;->RotateLoading:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 70
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->color:I

    .line 71
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v3}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->dpToPx(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    .line 72
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowPosition:I

    .line 73
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 76
    .end local v0    # "typedArray":Landroid/content/res/TypedArray;
    :cond_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    .line 77
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 79
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 80
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 81
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 82
    return-void
.end method

.method private startAnimator()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v5, 0x2

    .line 153
    const-string v3, "scaleX"

    new-array v4, v5, [F

    fill-array-data v4, :array_0

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 154
    .local v1, "scaleXAnimator":Landroid/animation/ObjectAnimator;
    const-string v3, "scaleY"

    new-array v4, v5, [F

    fill-array-data v4, :array_1

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 155
    .local v2, "scaleYAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 156
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 157
    invoke-virtual {v2, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 158
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 159
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 160
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    new-array v3, v5, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 161
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 162
    return-void

    .line 153
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 154
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private stopAnimator()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v5, 0x2

    .line 165
    const-string v3, "scaleX"

    new-array v4, v5, [F

    fill-array-data v4, :array_0

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 166
    .local v1, "scaleXAnimator":Landroid/animation/ObjectAnimator;
    const-string v3, "scaleY"

    new-array v4, v5, [F

    fill-array-data v4, :array_1

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 167
    .local v2, "scaleYAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 168
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 169
    invoke-virtual {v2, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 170
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 171
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 172
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    new-array v3, v5, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 173
    new-instance v3, Lcom/att/mobile/android/infra/utils/LoadingProgressBar$1;

    invoke-direct {v3, p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar$1;-><init>(Lcom/att/mobile/android/infra/utils/LoadingProgressBar;)V

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 194
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 195
    return-void

    .line 165
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 166
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public dpToPx(Landroid/content/Context;F)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dpVal"    # F

    .prologue
    .line 199
    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p2, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public isStart()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v8, 0x168

    const/high16 v7, 0x43200000    # 160.0f

    const/high16 v6, 0x41200000    # 10.0f

    const/4 v4, 0x0

    .line 97
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 99
    iget-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart:Z

    if-nez v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    const-string v1, "#1a000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowRectF:Landroid/graphics/RectF;

    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    int-to-float v2, v0

    iget v3, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    iget-object v5, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 105
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowRectF:Landroid/graphics/RectF;

    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    int-to-float v2, v0

    iget v3, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    iget-object v5, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 107
    iget-object v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 108
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->loadingRectF:Landroid/graphics/RectF;

    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    int-to-float v2, v0

    iget v3, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    iget-object v5, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 109
    iget-object v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->loadingRectF:Landroid/graphics/RectF;

    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    int-to-float v2, v0

    iget v3, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    iget-object v5, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 111
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    add-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    .line 112
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    add-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    .line 113
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    if-le v0, v8, :cond_2

    .line 114
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    add-int/lit16 v0, v0, -0x168

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->topDegree:I

    .line 116
    :cond_2
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    if-le v0, v8, :cond_3

    .line 117
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    add-int/lit16 v0, v0, -0x168

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->bottomDegree:I

    .line 120
    :cond_3
    iget-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->changeBigger:Z

    if-eqz v0, :cond_7

    .line 121
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    cmpg-float v0, v0, v7

    if-gez v0, :cond_4

    .line 122
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    float-to-double v0, v0

    const-wide/high16 v2, 0x4004000000000000L    # 2.5

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    .line 123
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->invalidate()V

    .line 131
    :cond_4
    :goto_1
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_0

    .line 132
    :cond_5
    iget-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->changeBigger:Z

    if-nez v0, :cond_6

    const/4 v4, 0x1

    :cond_6
    iput-boolean v4, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->changeBigger:Z

    .line 133
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->invalidate()V

    goto/16 :goto_0

    .line 126
    :cond_7
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_4

    .line 127
    iget v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    const/high16 v1, 0x40a00000    # 5.0f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    .line 128
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->invalidate()V

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 88
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->arc:F

    .line 90
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, p2, v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->loadingRectF:Landroid/graphics/RectF;

    .line 91
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowPosition:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    mul-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowPosition:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    iget v4, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowPosition:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->width:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, p2, v4

    iget v5, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowPosition:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->shadowRectF:Landroid/graphics/RectF;

    .line 92
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->startAnimator()V

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart:Z

    .line 140
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->invalidate()V

    .line 141
    return-void
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->stopAnimator()V

    .line 145
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->invalidate()V

    .line 146
    return-void
.end method

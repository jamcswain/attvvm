.class public Lcom/att/mobile/android/infra/utils/TimeDateUtils;
.super Ljava/lang/Object;
.source "TimeDateUtils.java"


# static fields
.field public static final LOG_TAG_TIMEDATE_UTILS:Ljava/lang/String; = "TimeDateUtils"

.field private static dateFormatStr:Ljava/lang/String;

.field private static now:Landroid/text/format/Time;

.field private static timeFormatStr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "MM/dd"

    sput-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;

    .line 19
    const-string v0, "hh:mm AA"

    sput-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    .line 20
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    sput-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->now:Landroid/text/format/Time;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static fixDateSplitter(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 193
    const-string v0, "-"

    const-string v1, "/"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatDuration(I)Ljava/lang/String;
    .locals 7
    .param p0, "time"    # I

    .prologue
    const/16 v6, 0xa

    .line 106
    div-int/lit8 v0, p0, 0x3c

    .line 107
    .local v0, "minutes":I
    rem-int/lit8 v2, p0, 0x3c

    .line 108
    .local v2, "seconds":I
    const/4 v1, 0x0

    .line 109
    .local v1, "minutesString":Ljava/lang/String;
    if-ge v0, v6, :cond_0

    .line 110
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 113
    :goto_0
    const/4 v3, 0x0

    .line 114
    .local v3, "secondsString":Ljava/lang/String;
    if-ge v2, v6, :cond_1

    .line 115
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 118
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 112
    .end local v3    # "secondsString":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 117
    .restart local v3    # "secondsString":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private static getDateNoYear()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 171
    sget-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;

    const-string v2, "yyyy"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->fixDateSplitter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "tmpFormat":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    .line 175
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 179
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 180
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 181
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 183
    :cond_1
    return-object v0

    .line 176
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static declared-synchronized getDeviceDateFormat(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    const-class v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;

    monitor-enter v1

    .line 157
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "date_format"

    .line 156
    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;

    .line 160
    sget-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 161
    :cond_0
    const-string v0, "MM/dd"

    sput-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;

    .line 163
    :cond_1
    sget-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized getDeviceTimeFormat(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    const-class v2, Lcom/att/mobile/android/infra/utils/TimeDateUtils;

    monitor-enter v2

    .line 137
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "time_12_24"

    .line 136
    invoke-static {v1, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    .line 140
    sget-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 141
    const-string v1, "hh:mm AA"

    sput-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    .line 147
    :goto_0
    sget-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v1

    .line 144
    :cond_0
    :try_start_1
    sget-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    const-string v3, "12"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 145
    .local v0, "b24":Z
    :goto_1
    if-eqz v0, :cond_2

    const-string v1, "kk:mm"

    :goto_2
    sput-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 136
    .end local v0    # "b24":Z
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 144
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 145
    .restart local v0    # "b24":Z
    :cond_2
    :try_start_2
    const-string v1, "hh:mm AA"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public static getFriendlyDate(JLandroid/content/Context;Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 8
    .param p0, "date"    # J
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "shortFotmat"    # Ljava/lang/Boolean;

    .prologue
    .line 34
    const/4 v1, 0x0

    .line 36
    .local v1, "retVal":Ljava/lang/String;
    invoke-static {p2}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->refreshDateFormat(Landroid/content/Context;)V

    .line 38
    sget-object v5, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->now:Landroid/text/format/Time;

    invoke-virtual {v5}, Landroid/text/format/Time;->setToNow()V

    .line 40
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 41
    .local v2, "targetTime":Landroid/text/format/Time;
    invoke-virtual {v2, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 44
    sget-object v5, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->now:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->year:I

    iget v6, v2, Landroid/text/format/Time;->year:I

    if-ne v5, v6, :cond_7

    .line 46
    sget-object v5, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->now:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->yearDay:I

    iget v6, v2, Landroid/text/format/Time;->yearDay:I

    if-ne v5, v6, :cond_1

    .line 47
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p0, p1}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getMessageTime(J)Ljava/lang/String;

    move-result-object v1

    .line 72
    :goto_0
    return-object v1

    .line 47
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f08006c

    invoke-virtual {p2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0, p1}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getMessageTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 50
    :cond_1
    sget-object v5, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->now:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->yearDay:I

    add-int/lit8 v5, v5, -0x1

    iget v6, v2, Landroid/text/format/Time;->yearDay:I

    if-ne v5, v6, :cond_3

    .line 51
    const v5, 0x7f080083

    invoke-virtual {p2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "yesterdayStr":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v1, v4

    .line 53
    :goto_1
    goto :goto_0

    .line 52
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0, p1}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getMessageTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 56
    .end local v4    # "yesterdayStr":Ljava/lang/String;
    :cond_3
    iget v5, v2, Landroid/text/format/Time;->yearDay:I

    sget-object v6, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->now:Landroid/text/format/Time;

    iget v6, v6, Landroid/text/format/Time;->yearDay:I

    add-int/lit8 v6, v6, -0x7

    if-lt v5, v6, :cond_5

    .line 57
    const-string v5, "EEEE"

    invoke-static {v5, p0, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 58
    .local v0, "dow":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4

    move-object v1, v0

    .line 60
    :goto_2
    goto :goto_0

    .line 58
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0, p1}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getMessageTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 61
    .end local v0    # "dow":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getDateNoYear()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p0, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 62
    .local v3, "tmpDate":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_6

    move-object v1, v3

    .line 63
    :goto_3
    goto/16 :goto_0

    .line 62
    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0, p1}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getMessageTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 67
    .end local v3    # "tmpDate":Ljava/lang/String;
    :cond_7
    sget-object v5, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;

    invoke-static {v5}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->fixDateSplitter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p0, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 68
    .restart local v3    # "tmpDate":Ljava/lang/String;
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_8

    move-object v1, v3

    :goto_4
    goto/16 :goto_0

    :cond_8
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p0, p1}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getMessageTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4
.end method

.method public static getMessageTime(J)Ljava/lang/String;
    .locals 4
    .param p0, "date"    # J

    .prologue
    .line 84
    sget-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    invoke-static {v1, p0, p1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    .local v0, "time":Ljava/lang/String;
    sget-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    const-string v2, "hh:mm AA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    const-string v2, "AA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    const-string v1, "am"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    const-string v1, "am"

    const-string v2, "AM"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 98
    :cond_0
    :goto_0
    return-object v0

    .line 93
    :cond_1
    const-string v1, "pm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    const-string v1, "pm"

    const-string v2, "PM"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static refreshDateFormat(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    invoke-static {p0}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getDeviceDateFormat(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->dateFormatStr:Ljava/lang/String;

    .line 127
    invoke-static {p0}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getDeviceTimeFormat(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->timeFormatStr:Ljava/lang/String;

    .line 128
    return-void
.end method

.class Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;
.super Landroid/os/Handler;
.source "SimManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/infra/sim/SimManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimManagerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/infra/sim/SimManager;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/infra/sim/SimManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 333
    iput-object p1, p0, Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;->this$0:Lcom/att/mobile/android/infra/sim/SimManager;

    .line 334
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 335
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 340
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v4, :cond_0

    .line 341
    const-string v1, "SimManager"

    const-string v2, "handleMessage()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v1, p0, Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;->this$0:Lcom/att/mobile/android/infra/sim/SimManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/infra/sim/SimManager;->validateSim()Lcom/att/mobile/android/infra/sim/SimValidationResult;

    move-result-object v0

    .line 347
    .local v0, "currResult":Lcom/att/mobile/android/infra/sim/SimValidationResult;
    const-string v1, "SimManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage() SimValidationResult = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 352
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    const-string v2, "simSwap"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 353
    iget-object v1, p0, Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;->this$0:Lcom/att/mobile/android/infra/sim/SimManager;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v5}, Lcom/att/mobile/android/infra/sim/SimManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 363
    .end local v0    # "currResult":Lcom/att/mobile/android/infra/sim/SimValidationResult;
    :cond_0
    :goto_0
    return-void

    .line 355
    .restart local v0    # "currResult":Lcom/att/mobile/android/infra/sim/SimValidationResult;
    :cond_1
    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->hashCode()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    .line 357
    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->hashCode()I

    move-result v1

    if-eq v1, v4, :cond_0

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->hashCode()I

    move-result v1

    if-eqz v1, :cond_0

    .line 358
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    const-string v2, "simSwap"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 359
    iget-object v1, p0, Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;->this$0:Lcom/att/mobile/android/infra/sim/SimManager;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v5}, Lcom/att/mobile/android/infra/sim/SimManager;->notifyListeners(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.class public Lcom/att/mobile/android/infra/sim/SimManager;
.super Ljava/lang/Object;
.source "SimManager.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/IEventDispatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;
    }
.end annotation


# static fields
.field private static final ENCRYPTION_ALGORITHM:Ljava/lang/String; = "SHA-1"

.field private static final EVENT_SERVICE_STATE_CHANGED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SimManager"

.field private static instance:Lcom/att/mobile/android/infra/sim/SimManager;


# instance fields
.field private dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

.field private handler:Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;

.field private handlerThread:Landroid/os/HandlerThread;

.field private mContext:Landroid/content/Context;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/infra/sim/SimManager;->instance:Lcom/att/mobile/android/infra/sim/SimManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/att/mobile/android/infra/sim/SimManager;->mContext:Landroid/content/Context;

    .line 75
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SimManager"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->handlerThread:Landroid/os/HandlerThread;

    .line 76
    iget-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 78
    new-instance v0, Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;

    iget-object v1, p0, Lcom/att/mobile/android/infra/sim/SimManager;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;-><init>(Lcom/att/mobile/android/infra/sim/SimManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->handler:Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;

    .line 80
    new-instance v0, Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 82
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 84
    const-string v0, "SimManager"

    const-string v1, "CTOR"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    return-void
.end method

.method private encrypt(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 199
    if-nez p1, :cond_0

    .line 200
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "encrypt() Cannot encrypt a null string"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 204
    :cond_0
    :try_start_0
    const-string v4, "SHA-1"

    .line 205
    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 206
    .local v0, "algorithm":Ljava/security/MessageDigest;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 207
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    .line 210
    .local v3, "encryptedOutput":[B
    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v3}, Ljava/math/BigInteger;-><init>([B)V

    .line 211
    .local v1, "bi":Ljava/math/BigInteger;
    const/16 v4, 0x10

    invoke-virtual {v1, v4}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 212
    .end local v0    # "algorithm":Ljava/security/MessageDigest;
    .end local v1    # "bi":Ljava/math/BigInteger;
    .end local v3    # "encryptedOutput":[B
    :catch_0
    move-exception v2

    .line 218
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/Error;

    const-string v5, "Failed to create encryption algorithm"

    invoke-direct {v4, v5}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static final getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    sget-object v0, Lcom/att/mobile/android/infra/sim/SimManager;->instance:Lcom/att/mobile/android/infra/sim/SimManager;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Lcom/att/mobile/android/infra/sim/SimManager;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/infra/sim/SimManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/att/mobile/android/infra/sim/SimManager;->instance:Lcom/att/mobile/android/infra/sim/SimManager;

    .line 99
    :cond_0
    sget-object v0, Lcom/att/mobile/android/infra/sim/SimManager;->instance:Lcom/att/mobile/android/infra/sim/SimManager;

    return-object v0
.end method

.method private getSavedEncryptedSimId()Ljava/lang/String;
    .locals 7

    .prologue
    .line 164
    const/4 v2, 0x0

    .line 168
    .local v2, "savedSimId":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "simId"

    const-class v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 171
    const-string v3, "SimManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSavedEncryptedSimId() Loaded encrypted Sim ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    return-object v2

    .line 173
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "SimManager"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private getUniqueSimId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private saveEncryptedSimId()V
    .locals 6

    .prologue
    .line 182
    :try_start_0
    invoke-direct {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getUniqueSimId()Ljava/lang/String;

    move-result-object v2

    .line 184
    .local v2, "simId":Ljava/lang/String;
    const-string v3, "SimManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveEncryptedSimId() Sim ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-direct {p0, v2}, Lcom/att/mobile/android/infra/sim/SimManager;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "encryptedSimId":Ljava/lang/String;
    const-string v3, "SimManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveEncryptedSimId() Saving encrypted Sim ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "simId"

    invoke-virtual {v3, v4, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    .end local v1    # "encryptedSimId":Ljava/lang/String;
    .end local v2    # "simId":Ljava/lang/String;
    :goto_0
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SimManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 271
    iget-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/Dispatcher;->addListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 272
    return-void
.end method

.method public clearUserDataOnSimSwap()V
    .locals 3

    .prologue
    .line 301
    const-string v1, "SimManager"

    const-string v2, "clearUserDeatilsOnSimSwap()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.att.mobile.android.vvm.INTENT_CLEAR_ALL_NOTIFICATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 304
    .local v0, "intentService":Landroid/content/Intent;
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 305
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 307
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->clearPreferences()V

    .line 310
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueue()V

    .line 312
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->deleteUnsavedMessages(Z)[Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .line 321
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->markAllMessagesAsOverwrite()V

    .line 322
    return-void
.end method

.method public getSimState()I
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    return v0
.end method

.method public getSubscriberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public notifyListeners(ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 295
    return-void
.end method

.method public removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/Dispatcher;->removeListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 284
    return-void
.end method

.method public removeEventListeners()V
    .locals 0

    .prologue
    .line 329
    return-void
.end method

.method public startListening()V
    .locals 3

    .prologue
    .line 88
    const-string v0, "SimManager"

    const-string v1, "startListening"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    new-instance v1, Lcom/att/mobile/android/infra/sim/SimPhoneStateListener;

    invoke-direct {v1}, Lcom/att/mobile/android/infra/sim/SimPhoneStateListener;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 90
    return-void
.end method

.method public startSimValidation()V
    .locals 2

    .prologue
    .line 104
    const-string v0, "SimManager"

    const-string v1, "startSimValidation()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v0, p0, Lcom/att/mobile/android/infra/sim/SimManager;->handler:Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/infra/sim/SimManager$SimManagerHandler;->sendEmptyMessage(I)Z

    .line 106
    return-void
.end method

.method public validateSim()Lcom/att/mobile/android/infra/sim/SimValidationResult;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x1

    .line 115
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getSimState()I

    move-result v2

    .line 117
    .local v2, "simState":I
    const-string v4, "SimManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "validateSim() simState = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    if-ne v2, v7, :cond_0

    .line 121
    new-instance v4, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/att/mobile/android/infra/sim/SimValidationResult;-><init>(I)V

    .line 157
    :goto_0
    return-object v4

    .line 124
    :cond_0
    iget-object v4, p0, Lcom/att/mobile/android/infra/sim/SimManager;->mContext:Landroid/content/Context;

    const-string v5, "android.permission.READ_PHONE_STATE"

    invoke-static {v4, v5}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 125
    new-instance v4, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    invoke-direct {v4, v8}, Lcom/att/mobile/android/infra/sim/SimValidationResult;-><init>(I)V

    goto :goto_0

    .line 130
    :cond_1
    if-eq v2, v8, :cond_2

    .line 131
    new-instance v4, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    invoke-direct {v4, v7}, Lcom/att/mobile/android/infra/sim/SimValidationResult;-><init>(I)V

    goto :goto_0

    .line 134
    :cond_2
    invoke-direct {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getSavedEncryptedSimId()Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "savedEncryptedSimId":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 140
    invoke-direct {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->saveEncryptedSimId()V

    .line 141
    new-instance v4, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Lcom/att/mobile/android/infra/sim/SimValidationResult;-><init>(I)V

    goto :goto_0

    .line 143
    :cond_3
    invoke-direct {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getUniqueSimId()Ljava/lang/String;

    move-result-object v3

    .line 145
    .local v3, "uniqueSimId":Ljava/lang/String;
    if-nez v3, :cond_4

    .line 146
    new-instance v4, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    invoke-direct {v4, v7}, Lcom/att/mobile/android/infra/sim/SimValidationResult;-><init>(I)V

    goto :goto_0

    .line 149
    :cond_4
    :try_start_0
    invoke-direct {p0, v3}, Lcom/att/mobile/android/infra/sim/SimManager;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 150
    new-instance v4, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lcom/att/mobile/android/infra/sim/SimValidationResult;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "Iae":Ljava/lang/IllegalArgumentException;
    const-string v4, "SimManager"

    const-string v5, "encrypt sim failed"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 154
    new-instance v4, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    invoke-direct {v4, v7}, Lcom/att/mobile/android/infra/sim/SimValidationResult;-><init>(I)V

    goto :goto_0

    .line 157
    .end local v0    # "Iae":Ljava/lang/IllegalArgumentException;
    :cond_5
    new-instance v4, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    const/4 v5, 0x4

    invoke-direct {v4, v5}, Lcom/att/mobile/android/infra/sim/SimValidationResult;-><init>(I)V

    goto :goto_0
.end method

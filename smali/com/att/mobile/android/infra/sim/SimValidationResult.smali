.class public final Lcom/att/mobile/android/infra/sim/SimValidationResult;
.super Ljava/lang/Object;
.source "SimValidationResult.java"


# static fields
.field public static final FIRST_SIM_USE:I = 0x3

.field public static final NOT_READY:I = 0x1

.field public static final NO_PERMISSION:I = 0x5

.field public static final NO_SIM:I = 0x0

.field private static final RESULT_TEXT:[Ljava/lang/String;

.field public static final SIM_SWAPPED:I = 0x2

.field public static final SIM_VALID:I = 0x4


# instance fields
.field private mResult:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NO_SIM"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "NOT_READY"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SIM_SWAPPED"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "FIRST_SIM_USE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "SIM_VALID"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "NO_PERMISSION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->RESULT_TEXT:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput p1, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    .line 56
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 75
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    if-nez v1, :cond_1

    .line 79
    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    iget v1, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    check-cast p1, Lcom/att/mobile/android/infra/sim/SimValidationResult;

    .end local p1    # "obj":Ljava/lang/Object;
    iget v2, p1, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    return v0
.end method

.method public isSimPresentAndReady()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 66
    iget v1, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSimSwapped()Z
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSimValid()Z
    .locals 2

    .prologue
    .line 62
    iget v0, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ordinal()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->RESULT_TEXT:[Ljava/lang/String;

    iget v1, p0, Lcom/att/mobile/android/infra/sim/SimValidationResult;->mResult:I

    aget-object v0, v0, v1

    return-object v0
.end method

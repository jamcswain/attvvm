.class public Lcom/att/mobile/android/infra/sim/SimPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "SimPhoneStateListener.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SimPhoneStateListener"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 2
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 18
    const-string v0, "SimPhoneStateListener"

    const-string v1, "onServiceStateChanged()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/infra/sim/SimManager;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimManager;->startSimValidation()V

    .line 20
    return-void
.end method

.class public final Lcom/att/mobile/android/infra/media/AudioPlayer;
.super Ljava/lang/Object;
.source "AudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/infra/media/AudioPlayer$PlayerError;,
        Lcom/att/mobile/android/infra/media/AudioPlayer$State;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AudioPlayer"


# instance fields
.field private audioPlayer:Landroid/media/MediaPlayer;

.field private audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

.field private currentState:I

.field private mediaDuration:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    .line 89
    iput-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    .line 96
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    .line 99
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/att/mobile/android/infra/media/AudioPlayer$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/infra/media/AudioPlayer$1;-><init>(Lcom/att/mobile/android/infra/media/AudioPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 112
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/att/mobile/android/infra/media/AudioPlayer$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/infra/media/AudioPlayer$2;-><init>(Lcom/att/mobile/android/infra/media/AudioPlayer;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/infra/media/AudioPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/infra/media/AudioPlayer;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlaybackEnded()V

    return-void
.end method

.method static synthetic access$102(Lcom/att/mobile/android/infra/media/AudioPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/infra/media/AudioPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    return p1
.end method

.method static synthetic access$200(Lcom/att/mobile/android/infra/media/AudioPlayer;II)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/infra/media/AudioPlayer;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    return-void
.end method

.method private handlePlaybackEnded()V
    .locals 2

    .prologue
    .line 627
    const-string v0, "AudioPlayer"

    const-string v1, "AudioPlayer.handlePlaybackEnded() - media playback eneded"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->seekTo(I)V

    .line 633
    const/4 v0, 0x1

    iput v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    .line 635
    const-string v0, "AudioPlayer"

    const-string v1, "AudioPlayer.handlePlaybackEnded() - media playback restarted to its begining"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    invoke-interface {v0}, Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;->onPlaybackEnd()V

    .line 643
    :cond_0
    return-void
.end method

.method private handlePlayerError(II)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "extra"    # I

    .prologue
    .line 651
    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-eqz v0, :cond_0

    .line 653
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->reset()V

    .line 658
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-eqz v0, :cond_1

    .line 659
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    .line 660
    invoke-interface {v0, p1}, Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;->onPlayerError(I)V

    .line 662
    :cond_1
    return-void
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 389
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 390
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.getCurrentPosition() - couldn\'t get media current position - audio player was already released"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :goto_0
    return v0

    .line 397
    :cond_0
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-nez v1, :cond_1

    .line 399
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.getCurrentPosition() - couldn\'t get media current position - no media is currently being handled by the player"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 405
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentState()I
    .locals 1

    .prologue
    .line 526
    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    return v0
.end method

.method public getMediaDuration()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 248
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 249
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.getMediaDuration() - getting media duration couldn\'t be done - audio player was already released"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :goto_0
    return v0

    .line 256
    :cond_0
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-nez v1, :cond_1

    .line 258
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.getMediaDuration() - getting media duration couldn\'t be done - no media is currently being handled by the player"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 264
    :cond_1
    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->mediaDuration:I

    goto :goto_0
.end method

.method public initializeMedia(Ljava/lang/String;)V
    .locals 8
    .param p1, "mediaURI"    # Ljava/lang/String;

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 153
    iget v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 154
    const-string v4, "AudioPlayer"

    const-string v5, "AudioPlayer.initializeMedia() - media initialization couldn\'t be done - audio player was already released"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-eqz v4, :cond_2

    .line 162
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->reset()V

    .line 166
    :cond_2
    :try_start_0
    const-string v4, "AudioPlayer"

    const-string v5, "AudioPlayer.initializeMedia() - initializing audio player..."

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    .line 170
    :cond_3
    const-string v4, "AudioPlayer"

    const-string v5, "AudioPlayer.initializeMedia() - media initialization couldn\'t be done -no media URI"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    const/4 v4, 0x1

    const/4 v5, -0x1

    invoke-direct {p0, v4, v5}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v4, "AudioPlayer"

    const-string v5, "AudioPlayer.initializeMedia() - media initialization couldn\'t be done"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 208
    const/16 v4, 0x270f

    invoke-direct {p0, v4, v7}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    goto :goto_0

    .line 176
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_4
    :try_start_1
    const-string v4, "content:"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 177
    iget-object v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 196
    :cond_5
    :goto_1
    iget-object v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 197
    iget-object v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V

    .line 200
    const/4 v4, 0x1

    iput v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    .line 202
    const-string v4, "AudioPlayer"

    const-string v5, "AudioPlayer.initializeMedia() - media has been initialized"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 230
    iget-object v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    iput v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->mediaDuration:I

    .line 234
    iget-object v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-eqz v4, :cond_0

    .line 235
    iget-object v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    iget v5, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->mediaDuration:I

    invoke-interface {v4, v5}, Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;->onPlayerInitialization(I)V

    goto :goto_0

    .line 180
    :cond_6
    :try_start_2
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 181
    .local v2, "file":Ljava/io/File;
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/io/File;->setReadable(ZZ)Z

    .line 182
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 184
    .local v3, "mediaInputStream":Ljava/io/FileInputStream;
    :try_start_3
    iget-object v4, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 190
    if-eqz v3, :cond_5

    .line 191
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_1

    .line 210
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "mediaInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    .line 211
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "AudioPlayer"

    const-string v5, "AudioPlayer.initializeMedia() - media initialization couldn\'t be done"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 214
    invoke-direct {p0, v6, v7}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    goto/16 :goto_0

    .line 185
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "mediaInputStream":Ljava/io/FileInputStream;
    :catch_2
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v4, "AudioPlayer"

    const-string v5, "AudioPlayer.setDataSource() failed "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 190
    if-eqz v3, :cond_5

    .line 191
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 216
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "mediaInputStream":Ljava/io/FileInputStream;
    :catch_3
    move-exception v0

    .line 217
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v4, "AudioPlayer"

    const-string v5, "AudioPlayer.initializeMedia() - media initialization couldn\'t be done"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 220
    invoke-direct {p0, v6, v7}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    goto/16 :goto_0

    .line 190
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v3    # "mediaInputStream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_7

    .line 191
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    :cond_7
    throw v4
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 222
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "mediaInputStream":Ljava/io/FileInputStream;
    :catch_4
    move-exception v1

    .line 223
    .local v1, "ex":Ljava/lang/Exception;
    const-string v4, "AudioPlayer"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 224
    invoke-direct {p0, v6, v7}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    goto/16 :goto_0
.end method

.method public isPaused()Z
    .locals 2

    .prologue
    .line 458
    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 2

    .prologue
    .line 341
    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 415
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 416
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.pause() - media playback couldn\'t be paused - audio player was already released"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 422
    :cond_1
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 423
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.pause() - media playback couldn\'t be paused - media is not being played"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 430
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    .line 431
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.pause() - media playback has been paused"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const/4 v1, 0x3

    iput v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-eqz v1, :cond_0

    .line 447
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    invoke-interface {v1}, Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;->onPlaybackPause()V

    goto :goto_0

    .line 436
    :catch_0
    move-exception v0

    .line 437
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.pause() - media playback couldn\'t be paused"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 440
    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    goto :goto_0
.end method

.method public registerAudioPlayerEventsListener(Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;)V
    .locals 1
    .param p1, "audioPlayerEventsListener"    # Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    .prologue
    .line 674
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-eqz v0, :cond_0

    .line 679
    :goto_0
    return-void

    .line 678
    :cond_0
    iput-object p1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    goto :goto_0
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 591
    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-ne v0, v2, :cond_0

    .line 592
    const-string v0, "AudioPlayer"

    const-string v1, "AudioPlayer.release() - audio player was already released"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :goto_0
    return-void

    .line 599
    :cond_0
    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 601
    :cond_1
    invoke-virtual {p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->stop()V

    .line 605
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 606
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 609
    iput-object v3, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    .line 610
    iput-object v3, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    .line 613
    iput v2, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    .line 615
    const-string v0, "AudioPlayer"

    const-string v1, "AudioPlayer.release() - player has been released"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 535
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 536
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.reset() - media playback couldn\'t be restarted - audio player was already released"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 545
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 547
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.reset() - media playback has been stopped"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    const/4 v1, 0x1

    iput v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 569
    :cond_3
    :goto_1
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-ne v1, v3, :cond_0

    .line 572
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-eqz v1, :cond_4

    .line 573
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    invoke-interface {v1}, Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;->onPlaybackStop()V

    .line 576
    :cond_4
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    .line 577
    const/4 v1, 0x0

    iput v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    .line 579
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.reset() - audio player has been restarted"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 557
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.reset() - media playback couldn\'t be stopped"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public seekTo(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 354
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 355
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.seekTo() - media playback couldn\'t be seeked - audio player was already released"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :goto_0
    return-void

    .line 360
    :cond_0
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-nez v1, :cond_1

    .line 361
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.seekTo() - media playback couldn\'t be seeked - audio player\'s state is IDLE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 368
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 369
    const-string v1, "AudioPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AudioPlayer.seekTo() - media playback seeked to position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 372
    :catch_0
    move-exception v0

    .line 373
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.seekTo() - media playback couldn\'t be seeked"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 376
    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    goto :goto_0
.end method

.method public setAudioStreamType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 138
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->start(I)V

    .line 275
    return-void
.end method

.method public start(I)V
    .locals 4
    .param p1, "startingPosition"    # I

    .prologue
    const/4 v3, 0x2

    .line 289
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 290
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.start() - media playback couldn\'t be started - audio player was already released"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-ne v1, v3, :cond_3

    .line 299
    :cond_2
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.start() - media playback couldn\'t be started - audio player\'s state is not READY or PAUSED"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 305
    :cond_3
    if-ltz p1, :cond_4

    .line 307
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 312
    :cond_4
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 313
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.start() - media playback has been started"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const/4 v1, 0x2

    iput v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    invoke-interface {v1, p1}, Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;->onPlaybackStart(I)V

    goto :goto_0

    .line 318
    :catch_0
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.start() - media playback couldn\'t be started"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 323
    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 469
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 470
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.stop() - media playback couldn\'t be stopped - audio player was already released"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 476
    :cond_1
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-nez v1, :cond_2

    .line 477
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.stop() - media playback couldn\'t be stopped - the audio player is currently not handling the media."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 483
    :cond_2
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-ne v1, v3, :cond_3

    .line 484
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.stop() - media playback couldn\'t be stopped - media is not being played at the moment."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 490
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 492
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.stop() - media playback has been stopped"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    const/4 v1, 0x1

    iput v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 515
    :goto_1
    iget v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->currentState:I

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-eqz v1, :cond_0

    .line 516
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    invoke-interface {v1}, Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;->onPlaybackStop()V

    goto :goto_0

    .line 504
    :catch_0
    move-exception v0

    .line 506
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "AudioPlayer"

    const-string v2, "AudioPlayer.stop() - media playback couldn\'t be stopped"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 509
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 510
    const/4 v1, -0x1

    invoke-direct {p0, v3, v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->handlePlayerError(II)V

    goto :goto_1
.end method

.method public unregisterAudioPlayerEventsListener(Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;)V
    .locals 1
    .param p1, "audioPlayerEventsListener"    # Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    .prologue
    .line 693
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    if-ne v0, p1, :cond_0

    .line 694
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/infra/media/AudioPlayer;->audioPlayerEventsListener:Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;

    .line 696
    :cond_0
    return-void
.end method

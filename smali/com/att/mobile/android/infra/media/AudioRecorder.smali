.class public Lcom/att/mobile/android/infra/media/AudioRecorder;
.super Ljava/lang/Object;
.source "AudioRecorder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AudioRecorder"


# instance fields
.field private audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

.field private isRecording:Z

.field private maximumRecordDuration:I

.field private mediaRecorder:Landroid/media/MediaRecorder;

.field private recordedAudioFileFullPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "recordedAudioFileFullPath"    # Ljava/lang/String;
    .param p2, "maximumRecordDuration"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->isRecording:Z

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    .line 39
    iput-object p1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->recordedAudioFileFullPath:Ljava/lang/String;

    .line 42
    iput p2, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->maximumRecordDuration:I

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/infra/media/AudioRecorder;II)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/infra/media/AudioRecorder;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/infra/media/AudioRecorder;->handlePlayerError(II)V

    return-void
.end method

.method private checkStorageFileExistens()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->recordedAudioFileFullPath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 55
    .local v1, "audioFileFolder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v3

    if-nez v3, :cond_0

    .line 56
    new-instance v3, Ljava/io/IOException;

    const-string v4, "AudioRecorder.checkStorageExistene() - the path to store the audio file to be recorded doesn\'t exist and couldn\'t be created."

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 59
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->recordedAudioFileFullPath:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .local v0, "audioFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    .line 62
    .local v2, "wasDeleted":Z
    const-string v3, "AudioRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AudioRecoder.checkStorageFileExistens() - audioFile.delete() returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    .end local v2    # "wasDeleted":Z
    :cond_1
    return-void
.end method

.method private handlePlayerError(II)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "extra"    # I

    .prologue
    .line 221
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 225
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    .line 226
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->isRecording:Z

    .line 229
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    invoke-interface {v0}, Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;->onRecorderError()V

    .line 232
    :cond_1
    return-void
.end method


# virtual methods
.method public getMaxAmplitude()I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    if-nez v0, :cond_0

    .line 168
    const/4 v0, 0x0

    .line 171
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->getMaxAmplitude()I

    move-result v0

    goto :goto_0
.end method

.method public getMaximumRecordDuration()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->maximumRecordDuration:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->recordedAudioFileFullPath:Ljava/lang/String;

    return-object v0
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->isRecording:Z

    return v0
.end method

.method public registerAudioRecorderEventsListener(Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;)V
    .locals 1
    .param p1, "audioRecorderEventsListener"    # Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    if-eqz v0, :cond_0

    .line 246
    :goto_0
    return-void

    .line 245
    :cond_0
    iput-object p1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    goto :goto_0
.end method

.method public start()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 92
    :try_start_0
    invoke-direct {p0}, Lcom/att/mobile/android/infra/media/AudioRecorder;->checkStorageFileExistens()V

    .line 94
    new-instance v1, Landroid/media/MediaRecorder;

    invoke-direct {v1}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    .line 95
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    new-instance v2, Lcom/att/mobile/android/infra/media/AudioRecorder$1;

    invoke-direct {v2, p0}, Lcom/att/mobile/android/infra/media/AudioRecorder$1;-><init>(Lcom/att/mobile/android/infra/media/AudioRecorder;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    .line 110
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->reset()V

    .line 112
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 113
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 114
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 115
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v2, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->recordedAudioFileFullPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 116
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    iget v2, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->maximumRecordDuration:I

    invoke-virtual {v1, v2}, Landroid/media/MediaRecorder;->setMaxDuration(I)V

    .line 117
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->prepare()V

    .line 118
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->start()V

    .line 122
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    iget v2, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->maximumRecordDuration:I

    invoke-interface {v1, v2}, Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;->onRecorderInitialization(I)V

    .line 125
    :cond_0
    const-string v1, "AudioRecorder"

    const-string v2, "AudioRecorder.start() - init finished, starting the audio recording..."

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->isRecording:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 146
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    if-eqz v1, :cond_1

    .line 147
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    invoke-interface {v1}, Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;->onRecordingStart()V

    .line 149
    :cond_1
    :goto_0
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "AudioRecorder"

    const-string v2, "AudioRecoder.start() -  audio recording couldn\'t be started"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 132
    invoke-direct {p0, v3, v3}, Lcom/att/mobile/android/infra/media/AudioRecorder;->handlePlayerError(II)V

    goto :goto_0

    .line 134
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "AudioRecorder"

    const-string v2, "AudioRecoder.start() - audio recording couldn\'t be started"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 137
    invoke-direct {p0, v3, v3}, Lcom/att/mobile/android/infra/media/AudioRecorder;->handlePlayerError(II)V

    goto :goto_0

    .line 139
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AudioRecorder"

    const-string v2, "AudioRecoder.start() - audio recording couldn\'t be started"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 141
    invoke-direct {p0, v3, v3}, Lcom/att/mobile/android/infra/media/AudioRecorder;->handlePlayerError(II)V

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->stop()V

    .line 185
    const-string v1, "AudioRecorder"

    const-string v2, "AudioRecoder.stop() - audio recording stopped"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    .line 189
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->mediaRecorder:Landroid/media/MediaRecorder;

    .line 192
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->isRecording:Z

    .line 194
    const-string v1, "AudioRecorder"

    const-string v2, "AudioRecoder.release() - audio recorder has been released"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->isRecording:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    if-eqz v1, :cond_1

    .line 212
    iget-object v1, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    invoke-interface {v1}, Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;->onRecordingStop()V

    .line 214
    :cond_1
    return-void

    .line 197
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "AudioRecorder"

    const-string v2, "AudioRecorder.stop() - audio recording couldn\'t be stopped"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 206
    invoke-direct {p0, v3, v3}, Lcom/att/mobile/android/infra/media/AudioRecorder;->handlePlayerError(II)V

    goto :goto_0
.end method

.method public unregisterAudioRecorderEventsListener(Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;)V
    .locals 1
    .param p1, "audioRecorderEventsListener"    # Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    .prologue
    .line 256
    iget-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    if-ne v0, p1, :cond_0

    .line 257
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/infra/media/AudioRecorder;->audioRecorderEventsListener:Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;

    .line 259
    :cond_0
    return-void
.end method

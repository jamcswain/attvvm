.class public interface abstract Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;
.super Ljava/lang/Object;
.source "MediaPlayerEventsListener.java"


# virtual methods
.method public abstract onPlaybackEnd()V
.end method

.method public abstract onPlaybackPause()V
.end method

.method public abstract onPlaybackStart(I)V
.end method

.method public abstract onPlaybackStop()V
.end method

.method public abstract onPlayerError(I)V
.end method

.method public abstract onPlayerInitialization(I)V
.end method

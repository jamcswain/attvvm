.class public interface abstract Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;
.super Ljava/lang/Object;
.source "MediaRecorderEventsListener.java"


# virtual methods
.method public abstract onRecorderError()V
.end method

.method public abstract onRecorderInitialization(I)V
.end method

.method public abstract onRecordingEnd()V
.end method

.method public abstract onRecordingStart()V
.end method

.method public abstract onRecordingStop()V
.end method

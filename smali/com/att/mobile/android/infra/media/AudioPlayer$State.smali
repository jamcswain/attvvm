.class public interface abstract Lcom/att/mobile/android/infra/media/AudioPlayer$State;
.super Ljava/lang/Object;
.source "AudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/infra/media/AudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "State"
.end annotation


# static fields
.field public static final IDLE:I = 0x0

.field public static final PAUSED:I = 0x3

.field public static final READY:I = 0x1

.field public static final RELEASED:I = 0x4

.field public static final STARTED:I = 0x2

.class public Lcom/att/mobile/android/infra/network/Models/MessageObject;
.super Ljava/lang/Object;
.source "MessageObject.java"


# instance fields
.field private attributesInnerObject:Lcom/att/mobile/android/infra/network/Models/AttributesInnerObject;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "attributes"
    .end annotation
.end field

.field private correlationId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "correlationId"
    .end annotation
.end field

.field private flagsInnerObject:Lcom/att/mobile/android/infra/network/Models/FlagsInnerObject;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "flags"
    .end annotation
.end field

.field private lastModificationSequence:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lastModSeq"
    .end annotation
.end field

.field private parentFolder:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "parentFolder"
    .end annotation
.end field

.field private payloads:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "payloadPart"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/infra/network/Models/PayloadInnerObject;",
            ">;"
        }
    .end annotation
.end field

.field private resourceURL:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "resourceURL"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttributesInnerObject()Lcom/att/mobile/android/infra/network/Models/AttributesInnerObject;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Models/MessageObject;->attributesInnerObject:Lcom/att/mobile/android/infra/network/Models/AttributesInnerObject;

    return-object v0
.end method

.method public getCorrelationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Models/MessageObject;->correlationId:Ljava/lang/String;

    return-object v0
.end method

.method public getFlagsInnerObject()Lcom/att/mobile/android/infra/network/Models/FlagsInnerObject;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Models/MessageObject;->flagsInnerObject:Lcom/att/mobile/android/infra/network/Models/FlagsInnerObject;

    return-object v0
.end method

.method public getLastModificationSequence()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/att/mobile/android/infra/network/Models/MessageObject;->lastModificationSequence:I

    return v0
.end method

.method public getParentFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Models/MessageObject;->parentFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getPayloads()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/infra/network/Models/PayloadInnerObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Models/MessageObject;->payloads:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getResourceURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Models/MessageObject;->resourceURL:Ljava/lang/String;

    return-object v0
.end method

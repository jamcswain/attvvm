.class Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;
.super Ljava/lang/Object;
.source "EncoreVVMManager.java"

# interfaces
.implements Lretrofit2/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->getMessage(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit2/Callback",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

.field final synthetic val$callback:Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;->this$0:Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

    iput-object p2, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V
    .locals 2
    .param p2, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;->onFailure(Ljava/lang/String;)V

    .line 111
    :cond_0
    return-void
.end method

.method public onResponse(Lretrofit2/Call;Lretrofit2/Response;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lretrofit2/Response",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<Ljava/lang/String;>;"
    .local p2, "response":Lretrofit2/Response;, "Lretrofit2/Response<Ljava/lang/String;>;"
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 99
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-virtual {p2}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-class v3, Lcom/att/mobile/android/infra/network/Responses/MessageResponse;

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/infra/network/Responses/MessageResponse;

    .line 100
    .local v1, "messageResponse":Lcom/att/mobile/android/infra/network/Responses/MessageResponse;
    iget-object v2, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;

    if-eqz v2, :cond_0

    .line 101
    iget-object v2, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;

    invoke-interface {v2, v1}, Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;->onSuccess(Lcom/att/mobile/android/infra/network/Responses/MessageResponse;)V

    .line 104
    :cond_0
    return-void
.end method

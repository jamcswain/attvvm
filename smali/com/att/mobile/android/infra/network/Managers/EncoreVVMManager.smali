.class public Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;
.super Ljava/lang/Object;
.source "EncoreVVMManager.java"


# static fields
.field private static ourInstance:Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;


# instance fields
.field private encoreVVMRequests:Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

    invoke-direct {v0}, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;-><init>()V

    sput-object v0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->ourInstance:Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v1, Lretrofit2/Retrofit$Builder;

    invoke-direct {v1}, Lretrofit2/Retrofit$Builder;-><init>()V

    invoke-direct {p0}, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->getServerBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    .line 29
    .local v0, "retrofit":Lretrofit2/Retrofit;
    const-class v1, Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;

    iput-object v1, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->encoreVVMRequests:Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->ourInstance:Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

    return-object v0
.end method

.method private getServerBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "http://nms-sib01.si.enclab.att.net/nms/v1/base/"

    return-object v0
.end method


# virtual methods
.method public createNotificationChannel(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;)V
    .locals 2
    .param p1, "boxId"    # Ljava/lang/String;
    .param p2, "objectId"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;

    .prologue
    .line 40
    iget-object v1, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->encoreVVMRequests:Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;

    invoke-interface {v1, p1, p2}, Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;->CreateNotificationChannel(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    .line 41
    .local v0, "call":Lretrofit2/Call;, "Lretrofit2/Call<Ljava/lang/Void;>;"
    new-instance v1, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;

    invoke-direct {v1, p0, p3}, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;-><init>(Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 64
    return-void
.end method

.method public getMessage(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;)V
    .locals 2
    .param p1, "boxId"    # Ljava/lang/String;
    .param p2, "objectId"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;

    .prologue
    .line 94
    iget-object v1, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->encoreVVMRequests:Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;

    invoke-interface {v1, p1, p2}, Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;->getMessage(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    .line 95
    .local v0, "call":Lretrofit2/Call;, "Lretrofit2/Call<Ljava/lang/String;>;"
    new-instance v1, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;

    invoke-direct {v1, p0, p3}, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$3;-><init>(Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;Lcom/att/mobile/android/infra/network/Interfaces/MessageResponseCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 114
    return-void
.end method

.method public markMessageAsSeen(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;)V
    .locals 2
    .param p1, "boxId"    # Ljava/lang/String;
    .param p2, "objectId"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;

    .prologue
    .line 68
    iget-object v1, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->encoreVVMRequests:Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;

    invoke-interface {v1, p1, p2}, Lcom/att/mobile/android/infra/network/Interfaces/EncoreVVMRequests;->MarkMessageAsSeen(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    .line 69
    .local v0, "call":Lretrofit2/Call;, "Lretrofit2/Call<Ljava/lang/Void;>;"
    new-instance v1, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$2;

    invoke-direct {v1, p0, p3}, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$2;-><init>(Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 91
    return-void
.end method

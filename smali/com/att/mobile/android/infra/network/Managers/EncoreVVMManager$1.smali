.class Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;
.super Ljava/lang/Object;
.source "EncoreVVMManager.java"

# interfaces
.implements Lretrofit2/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;->createNotificationChannel(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit2/Callback",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

.field final synthetic val$callback:Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;->this$0:Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager;

    iput-object p2, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lretrofit2/Call;Ljava/lang/Throwable;)V
    .locals 2
    .param p2, "t"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;->onFailure(Ljava/lang/String;)V

    .line 61
    :cond_0
    return-void
.end method

.method public onResponse(Lretrofit2/Call;Lretrofit2/Response;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Call",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Lretrofit2/Response",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "call":Lretrofit2/Call;, "Lretrofit2/Call<Ljava/lang/Void;>;"
    .local p2, "response":Lretrofit2/Response;, "Lretrofit2/Response<Ljava/lang/Void;>;"
    iget-object v1, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;

    if-eqz v1, :cond_0

    .line 47
    new-instance v0, Lcom/att/mobile/android/infra/network/Responses/HeadersResponse;

    invoke-direct {v0}, Lcom/att/mobile/android/infra/network/Responses/HeadersResponse;-><init>()V

    .line 48
    .local v0, "headersResponse":Lcom/att/mobile/android/infra/network/Responses/HeadersResponse;
    invoke-virtual {p2}, Lretrofit2/Response;->headers()Lokhttp3/Headers;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/infra/network/Responses/HeadersResponse;->setHeaders(Lokhttp3/Headers;)V

    .line 49
    iget-object v1, p0, Lcom/att/mobile/android/infra/network/Managers/EncoreVVMManager$1;->val$callback:Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;

    invoke-interface {v1, v0}, Lcom/att/mobile/android/infra/network/Interfaces/HeadersResponseCallback;->onSuccess(Lcom/att/mobile/android/infra/network/Responses/HeadersResponse;)V

    .line 52
    .end local v0    # "headersResponse":Lcom/att/mobile/android/infra/network/Responses/HeadersResponse;
    :cond_0
    return-void
.end method

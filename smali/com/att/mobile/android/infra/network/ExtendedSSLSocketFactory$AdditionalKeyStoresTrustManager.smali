.class Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;
.super Ljava/lang/Object;
.source "ExtendedSSLSocketFactory.java"

# interfaces
.implements Ljavax/net/ssl/X509TrustManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AdditionalKeyStoresTrustManager"
.end annotation


# instance fields
.field protected x509TrustManagers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljavax/net/ssl/X509TrustManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected varargs constructor <init>([Ljava/security/KeyStore;)V
    .locals 13
    .param p1, "additionalkeyStores"    # [Ljava/security/KeyStore;

    .prologue
    const/4 v8, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;->x509TrustManagers:Ljava/util/ArrayList;

    .line 113
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v2, "factories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljavax/net/ssl/TrustManagerFactory;>;"
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v7

    .line 116
    invoke-static {v7}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v4

    .line 118
    .local v4, "original":Ljavax/net/ssl/TrustManagerFactory;
    const/4 v7, 0x0

    check-cast v7, Ljava/security/KeyStore;

    invoke-virtual {v4, v7}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 119
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    array-length v9, p1

    move v7, v8

    :goto_0
    if-ge v7, v9, :cond_0

    aget-object v3, p1, v7

    .line 122
    .local v3, "keyStore":Ljava/security/KeyStore;
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v10

    .line 121
    invoke-static {v10}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    .line 123
    .local v0, "additionalCerts":Ljavax/net/ssl/TrustManagerFactory;
    invoke-virtual {v0, v3}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 125
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 129
    .end local v0    # "additionalCerts":Ljavax/net/ssl/TrustManagerFactory;
    .end local v3    # "keyStore":Ljava/security/KeyStore;
    .end local v4    # "original":Ljavax/net/ssl/TrustManagerFactory;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, "ExtendedSSLSocketFactory"

    const-string v8, "ctor"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 131
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 136
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v4    # "original":Ljavax/net/ssl/TrustManagerFactory;
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljavax/net/ssl/TrustManagerFactory;

    .line 137
    .local v6, "tmf":Ljavax/net/ssl/TrustManagerFactory;
    invoke-virtual {v6}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v10

    array-length v11, v10

    move v7, v8

    :goto_1
    if-ge v7, v11, :cond_1

    aget-object v5, v10, v7

    .line 138
    .local v5, "tm":Ljavax/net/ssl/TrustManager;
    instance-of v12, v5, Ljavax/net/ssl/X509TrustManager;

    if-eqz v12, :cond_2

    .line 139
    iget-object v12, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;->x509TrustManagers:Ljava/util/ArrayList;

    check-cast v5, Ljavax/net/ssl/X509TrustManager;

    .end local v5    # "tm":Ljavax/net/ssl/TrustManager;
    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    :cond_2
    iget-object v12, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;->x509TrustManagers:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_3

    .line 142
    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "Couldn\'t find any X509TrustManagers"

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 137
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 146
    .end local v6    # "tmf":Ljavax/net/ssl/TrustManagerFactory;
    :cond_4
    return-void
.end method


# virtual methods
.method public checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 3
    .param p1, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p2, "authType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v1, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;->x509TrustManagers:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/X509TrustManager;

    .line 153
    .local v0, "defaultX509TrustManager":Ljavax/net/ssl/X509TrustManager;
    invoke-interface {v0, p1, p2}, Ljavax/net/ssl/X509TrustManager;->checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 7
    .param p1, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p2, "authType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 160
    const/4 v0, 0x0

    .line 161
    .local v0, "chainOne":[Ljava/security/cert/X509Certificate;
    iget-object v4, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;->x509TrustManagers:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljavax/net/ssl/X509TrustManager;

    .line 162
    .local v3, "tm":Ljavax/net/ssl/X509TrustManager;
    if-eqz p1, :cond_0

    .line 163
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/security/cert/X509Certificate;

    .line 164
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, p1

    if-ge v2, v5, :cond_0

    .line 165
    const/4 v5, 0x0

    aget-object v6, p1, v2

    aput-object v6, v0, v5

    .line 167
    :try_start_0
    invoke-interface {v3, v0, p2}, Ljavax/net/ssl/X509TrustManager;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    return-void

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Ljava/security/cert/CertificateException;
    const-string v5, "ExtendedSSLSocketFactory"

    const-string v6, "EncoreSSLSocketFactory CertificateException "

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 164
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    .end local v2    # "i":I
    .end local v3    # "tm":Ljavax/net/ssl/X509TrustManager;
    :cond_1
    new-instance v4, Ljava/security/cert/CertificateException;

    invoke-direct {v4}, Ljava/security/cert/CertificateException;-><init>()V

    throw v4
.end method

.method public getAcceptedIssuers()[Ljava/security/cert/X509Certificate;
    .locals 4

    .prologue
    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    iget-object v2, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;->x509TrustManagers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/net/ssl/X509TrustManager;

    .line 181
    .local v1, "tm":Ljavax/net/ssl/X509TrustManager;
    invoke-interface {v1}, Ljavax/net/ssl/X509TrustManager;->getAcceptedIssuers()[Ljava/security/cert/X509Certificate;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 182
    .end local v1    # "tm":Ljavax/net/ssl/X509TrustManager;
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/security/cert/X509Certificate;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/security/cert/X509Certificate;

    return-object v2
.end method

.class public Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;
.super Lorg/apache/http/conn/ssl/SSLSocketFactory;
.source "ExtendedSSLSocketFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;
    }
.end annotation


# static fields
.field private static INSTANCE:Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory; = null

.field private static final TAG:Ljava/lang/String; = "ExtendedSSLSocketFactory"


# instance fields
.field protected sslContext:Ljavax/net/ssl/SSLContext;


# direct methods
.method private constructor <init>(Ljava/io/InputStream;[CLorg/apache/http/conn/ssl/X509HostnameVerifier;)V
    .locals 10
    .param p1, "rawKeystoreInputStream"    # Ljava/io/InputStream;
    .param p2, "password"    # [C
    .param p3, "hostnameVerifier"    # Lorg/apache/http/conn/ssl/X509HostnameVerifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/KeyManagementException;,
            Ljava/security/KeyStoreException;,
            Ljava/security/UnrecoverableKeyException;,
            Ljava/security/cert/CertificateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 76
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lorg/apache/http/conn/ssl/SSLSocketFactory;-><init>(Ljava/lang/String;Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;Ljava/security/SecureRandom;Lorg/apache/http/conn/scheme/HostNameResolver;)V

    .line 35
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->sslContext:Ljavax/net/ssl/SSLContext;

    .line 78
    const-string v0, "BKS"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v7

    .line 83
    .local v7, "trusted":Ljava/security/KeyStore;
    invoke-virtual {v7, p1, p2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 84
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 88
    invoke-virtual {p0, p3}, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 90
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->sslContext:Ljavax/net/ssl/SSLContext;

    new-array v2, v9, [Ljavax/net/ssl/TrustManager;

    new-instance v3, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;

    new-array v4, v9, [Ljava/security/KeyStore;

    aput-object v7, v4, v8

    invoke-direct {v3, v4}, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory$AdditionalKeyStoresTrustManager;-><init>([Ljava/security/KeyStore;)V

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2, v1}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 93
    return-void
.end method

.method public static createInstance(Ljava/io/InputStream;[CLorg/apache/http/conn/ssl/X509HostnameVerifier;)Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;
    .locals 4
    .param p0, "rawKeystoreInputStream"    # Ljava/io/InputStream;
    .param p1, "password"    # [C
    .param p2, "hostnameVerifier"    # Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    .prologue
    .line 38
    sget-object v1, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->INSTANCE:Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;

    if-nez v1, :cond_0

    .line 40
    :try_start_0
    const-string v1, "ExtendedSSLSocketFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "going to create ExtendedSSLSocketFactory instance with X509HostnameVerifier = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    new-instance v1, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;

    invoke-direct {v1, p0, p1, p2}, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;-><init>(Ljava/io/InputStream;[CLorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    sput-object v1, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->INSTANCE:Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :cond_0
    :goto_0
    sget-object v1, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->INSTANCE:Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;

    return-object v1

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ExtendedSSLSocketFactory"

    const-string v2, "Error creating instance for EncoreSSLSocketFactory"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static getInstance()Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 50
    sget-object v0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->INSTANCE:Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/security/cert/CertificateException;

    const-string v1, "you must first create an instance with a valid Keystore and password."

    invoke-direct {v0, v1}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    sget-object v0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->INSTANCE:Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;

    return-object v0
.end method


# virtual methods
.method public createSocket()Ljava/net/Socket;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->sslContext:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 1
    .param p1, "socket"    # Ljava/net/Socket;
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "port"    # I
    .param p4, "autoClose"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/att/mobile/android/infra/network/ExtendedSSLSocketFactory;->sslContext:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

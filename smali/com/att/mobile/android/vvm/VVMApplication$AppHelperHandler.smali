.class Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;
.super Landroid/os/Handler;
.source "VVMApplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/VVMApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppHelperHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 432
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/att/mobile/android/vvm/VVMApplication$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/att/mobile/android/vvm/VVMApplication$1;

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 436
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_2

    .line 440
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->access$600(Lcom/att/mobile/android/vvm/VVMApplication;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 442
    const-string v0, "VVMApplication"

    const-string v1, "VVMApplication went to background"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setIsApplicationSpeakerOn(Z)V

    .line 453
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->refreshDateFormat(Landroid/content/Context;)V

    .line 458
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCheckAttmStatusOnForeground(Z)V

    .line 463
    :cond_1
    :goto_0
    return-void

    .line 461
    :cond_2
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method

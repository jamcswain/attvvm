.class Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;
.super Landroid/os/AsyncTask;
.source "AggregatedActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/AggregatedActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactLoaderAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/att/mobile/android/vvm/model/ContactObject;",
        ">;"
    }
.end annotation


# instance fields
.field private mPhoneNumber:Ljava/lang/String;

.field mToolBarTitleRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field mUserImageRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/AggregatedActivity;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 1
    .param p2, "phoneNumber"    # Ljava/lang/String;
    .param p3, "toolBarTitle"    # Landroid/widget/TextView;
    .param p4, "userImage"    # Landroid/widget/ImageView;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->this$0:Lcom/att/mobile/android/vvm/screen/AggregatedActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 55
    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mUserImageRef:Ljava/lang/ref/WeakReference;

    .line 57
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mToolBarTitleRef:Ljava/lang/ref/WeakReference;

    .line 58
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/att/mobile/android/vvm/model/ContactObject;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContact(Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->doInBackground([Ljava/lang/Void;)Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/att/mobile/android/vvm/model/ContactObject;)V
    .locals 7
    .param p1, "contactObject"    # Lcom/att/mobile/android/vvm/model/ContactObject;

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 70
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mToolBarTitleRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 71
    .local v2, "titleBar":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mUserImageRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 73
    .local v3, "userImage":Landroid/widget/ImageView;
    if-eqz p1, :cond_4

    .line 75
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, "displayName":Ljava/lang/String;
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "#### ContactLoaderAsync displayName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    if-eqz v2, :cond_1

    .line 79
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    .end local v1    # "displayName":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :cond_1
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhotoUri()Landroid/net/Uri;

    move-result-object v0

    .line 83
    .local v0, "contactPhotoUri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->this$0:Lcom/att/mobile/android/vvm/screen/AggregatedActivity;

    invoke-static {v4, v0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->access$102(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;Landroid/net/Uri;)Landroid/net/Uri;

    .line 84
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 85
    invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/squareup/picasso/Picasso;->load(Landroid/net/Uri;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 98
    .end local v0    # "contactPhotoUri":Landroid/net/Uri;
    :cond_2
    :goto_0
    return-void

    .line 87
    .restart local v0    # "contactPhotoUri":Landroid/net/Uri;
    :cond_3
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->this$0:Lcom/att/mobile/android/vvm/screen/AggregatedActivity;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v4, v3, v5}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->access$200(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    .end local v0    # "contactPhotoUri":Landroid/net/Uri;
    :cond_4
    if-eqz v2, :cond_5

    .line 92
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    :cond_5
    if-eqz v3, :cond_2

    .line 95
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->this$0:Lcom/att/mobile/android/vvm/screen/AggregatedActivity;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v4, v3, v5}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->access$200(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->onPostExecute(Lcom/att/mobile/android/vvm/model/ContactObject;)V

    return-void
.end method

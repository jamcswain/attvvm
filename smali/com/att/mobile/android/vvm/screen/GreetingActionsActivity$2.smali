.class Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;
.super Ljava/lang/Object;
.source "GreetingActionsActivity.java"

# interfaces
.implements Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->wireNeededListener(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

.field final synthetic val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

.field final synthetic val$originalType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Lcom/att/mobile/android/vvm/model/greeting/Greeting;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    .prologue
    .line 304
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    iput-object p3, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$originalType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleNegativeButton(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 352
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2$1;-><init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 359
    const-string v0, "GreetingActionsActivity"

    const-string v1, "GreetingActionsActivity:showMenu::onClick => item 1 was clicked"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const-string v0, "GreetingActionsActivity"

    const-string v1, "GreetingActionsActivity:showMenu::onClick => starting recorder"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getMaxAllowedRecordTime()I

    move-result v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getUniqueId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getImapSelectionVariable()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getImapRecordingVariable()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$originalType:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$200(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    return-void
.end method

.method public handlePositiveButton(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 307
    const-string v9, "GreetingActionsActivity"

    const-string v10, "GreetingActionsActivity:=> Use existing greeting pressed"

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    const/4 v6, 0x0

    .line 311
    .local v6, "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v9, Ljava/lang/StringBuffer;

    sget-object v10, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/VVMApplication;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 312
    .local v2, "pathPrefix":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, ".amr"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 314
    .local v3, "recordNameFilePath":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 317
    .local v4, "recordingAudioFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v10

    long-to-int v8, v10

    .line 318
    .local v8, "recordingAudioFileSize":I
    new-array v5, v8, [B

    .line 319
    .local v5, "recordingAudioFileData":[B
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    .end local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .local v7, "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v7, v5}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    if-eq v9, v8, :cond_2

    .line 322
    const-string v9, "GreetingActionsActivity"

    const-string v10, "recording greeting audio file data couldn\'t be read"

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 338
    if-eqz v7, :cond_0

    .line 340
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    move-object v6, v7

    .line 348
    .end local v2    # "pathPrefix":Ljava/lang/String;
    .end local v3    # "recordNameFilePath":Ljava/lang/String;
    .end local v4    # "recordingAudioFile":Ljava/io/File;
    .end local v5    # "recordingAudioFileData":[B
    .end local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .end local v8    # "recordingAudioFileSize":I
    .restart local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :cond_1
    :goto_1
    return-void

    .line 341
    .end local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "pathPrefix":Ljava/lang/String;
    .restart local v3    # "recordNameFilePath":Ljava/lang/String;
    .restart local v4    # "recordingAudioFile":Ljava/io/File;
    .restart local v5    # "recordingAudioFileData":[B
    .restart local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v8    # "recordingAudioFileSize":I
    :catch_0
    move-exception v0

    .line 342
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 325
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    const/4 v1, 0x0

    .line 327
    .local v1, "imapRecordingGreetingType":Ljava/lang/String;
    :try_start_3
    iget-object v9, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-virtual {v9}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Custom"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 328
    const-string v1, "/private/vendor/vendor.alu/messaging/Greetings/Personal"

    .line 332
    :goto_2
    iget-object v9, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->showGauge(Ljava/lang/String;)V

    .line 333
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v9

    invoke-virtual {v9, v1, v5}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueSendGreetingOperation(Ljava/lang/String;[B)V

    .line 334
    iget-object v9, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    iget-object v10, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;->val$greeting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-static {v9, v10}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$002(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Lcom/att/mobile/android/vvm/model/greeting/Greeting;)Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 338
    if-eqz v7, :cond_5

    .line 340
    :try_start_4
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move-object v6, v7

    .line 343
    .end local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 330
    .end local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :cond_3
    :try_start_5
    const-string v1, "/private/vendor/vendor.alu/messaging/RecordedName"
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 341
    :catch_1
    move-exception v0

    .line 342
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v6, v7

    .line 343
    .end local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 335
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "imapRecordingGreetingType":Ljava/lang/String;
    .end local v2    # "pathPrefix":Ljava/lang/String;
    .end local v3    # "recordNameFilePath":Ljava/lang/String;
    .end local v4    # "recordingAudioFile":Ljava/io/File;
    .end local v5    # "recordingAudioFileData":[B
    .end local v8    # "recordingAudioFileSize":I
    :catch_2
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_6
    const-string v9, "GreetingActionsActivity"

    const-string v10, "recorded greeting audio file couldn\'t be sent to server - "

    invoke-static {v9, v10, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 338
    if-eqz v6, :cond_1

    .line 340
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 341
    :catch_3
    move-exception v0

    .line 342
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 338
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_4
    if-eqz v6, :cond_4

    .line 340
    :try_start_8
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 343
    :cond_4
    :goto_5
    throw v9

    .line 341
    :catch_4
    move-exception v0

    .line 342
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 338
    .end local v0    # "e":Ljava/io/IOException;
    .end local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "pathPrefix":Ljava/lang/String;
    .restart local v3    # "recordNameFilePath":Ljava/lang/String;
    .restart local v4    # "recordingAudioFile":Ljava/io/File;
    .restart local v5    # "recordingAudioFileData":[B
    .restart local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v8    # "recordingAudioFileSize":I
    :catchall_1
    move-exception v9

    move-object v6, v7

    .end local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_4

    .line 335
    .end local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v0

    move-object v6, v7

    .end local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "imapRecordingGreetingType":Ljava/lang/String;
    .restart local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :cond_5
    move-object v6, v7

    .end local v7    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_1
.end method

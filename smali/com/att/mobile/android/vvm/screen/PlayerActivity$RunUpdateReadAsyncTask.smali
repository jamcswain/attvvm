.class Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;
.super Landroid/os/AsyncTask;
.source "PlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/PlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RunUpdateReadAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field messageID:J

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerActivity;J)V
    .locals 0
    .param p2, "messageID"    # J

    .prologue
    .line 89
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 90
    iput-wide p2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;->messageID:J

    .line 91
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 95
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;->messageID:J

    invoke-virtual {v0, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageAsRead(J)I

    .line 96
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;->messageID:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueMarkAsReadOperation(Landroid/content/Context;J)V

    .line 97
    const/4 v0, 0x0

    return-object v0
.end method

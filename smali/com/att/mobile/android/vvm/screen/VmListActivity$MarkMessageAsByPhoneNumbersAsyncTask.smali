.class public Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;
.super Landroid/os/AsyncTask;
.source "VmListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/VmListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MarkMessageAsByPhoneNumbersAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field mPhoneNumbers:[Ljava/lang/String;

.field mSavedState:I

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/String;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VmListActivity;
    .param p2, "phoneNumbers"    # [Ljava/lang/String;
    .param p3, "savedState"    # I

    .prologue
    .line 82
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 83
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->mPhoneNumbers:[Ljava/lang/String;

    .line 84
    iput p3, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->mSavedState:I

    .line 85
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->mPhoneNumbers:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->mPhoneNumbers:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 91
    const-string v0, "VmListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MarkMessageAsSaveAsyncTask.doInBackground() start mPhoneNumbers="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->mPhoneNumbers:[Ljava/lang/String;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->mPhoneNumbers:[Ljava/lang/String;

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->mSavedState:I

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessagesSavedState([Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.class public abstract Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "VVMFragmentPagerAdapter.java"


# instance fields
.field private final mFragments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;",
            ">;"
        }
    .end annotation
.end field

.field private mPrimaryFragment:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 2
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mFragments:Ljava/util/ArrayList;

    .line 25
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 35
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentPagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 36
    return-void
.end method

.method public getFragment(I)Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mFragments:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    .line 57
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFragments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mPrimaryFragment:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 28
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .line 29
    .local v0, "object":Ljava/lang/Object;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mFragments:Ljava/util/ArrayList;

    move-object v1, v0

    check-cast v1, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    return-object v0
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentPagerAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 40
    check-cast p3, Landroid/support/v4/app/Fragment;

    .end local p3    # "object":Ljava/lang/Object;
    iput-object p3, p0, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->mPrimaryFragment:Landroid/support/v4/app/Fragment;

    .line 41
    return-void
.end method

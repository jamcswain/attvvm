.class Lcom/att/mobile/android/vvm/screen/VVMActivity$1;
.super Ljava/lang/Object;
.source "VVMActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/VVMActivity;->checkAttmStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

.field final synthetic val$thisActivity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;Landroid/app/Activity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VVMActivity;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->val$thisActivity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 261
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->access$000(Lcom/att/mobile/android/vvm/screen/VVMActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCheckAttmStatusOnForeground(Z)V

    .line 264
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->val$thisActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->val$thisActivity:Landroid/app/Activity;

    .line 265
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;

    if-eq v2, v3, :cond_1

    .line 267
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->access$000(Lcom/att/mobile/android/vvm/screen/VVMActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v0

    .line 268
    .local v0, "attStatusBeforeCheck":I
    invoke-static {}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->isUibReadyToReplaceLegacyVvm()Z

    .line 269
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->access$000(Lcom/att/mobile/android/vvm/screen/VVMActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    .line 270
    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->access$000(Lcom/att/mobile/android/vvm/screen/VVMActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 272
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 273
    .local v1, "startActivityIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->val$thisActivity:Landroid/app/Activity;

    const-class v3, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 274
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 276
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-virtual {v2, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->startActivity(Landroid/content/Intent;)V

    .line 279
    .end local v0    # "attStatusBeforeCheck":I
    .end local v1    # "startActivityIntent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

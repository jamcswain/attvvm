.class public Lcom/att/mobile/android/vvm/screen/PlayerFragment;
.super Landroid/support/v4/app/Fragment;
.source "PlayerFragment.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/EventListener;
.implements Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayButtonFlipperState;,
        Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayButtonUIStates;,
        Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;,
        Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;,
        Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;,
        Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerSetErrorAsyncTask;
    }
.end annotation


# static fields
.field private static final PERMISSIONS_REQUEST_EXPORT:I = 0xb

.field private static final PERMISSIONS_REQUEST_SHARE:I = 0xc

.field private static final REQUEST_CALL_PHONE_PERMISSION:I = 0xa

.field private static final TAG:Ljava/lang/String; = "PlayerFragment"

.field private static final URL_LOADER:I = 0x0

.field private static final daysDepth:I = 0xa


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field private audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

.field private avatarImageView:Landroid/widget/QuickContactBadge;

.field private backBtn:Landroid/widget/ImageView;

.field private bgImageView:Landroid/widget/ImageView;

.field private callButton:Landroid/widget/ImageView;

.field private contactImageUri:Landroid/net/Uri;

.field private contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

.field private contactUri:Ljava/lang/String;

.field private contactsChanged:Z

.field private dataLoader:Lcom/att/mobile/android/vvm/control/AsyncLoader;

.field private dateTextView:Landroid/widget/TextView;

.field private defaultSeekBar:Landroid/widget/SeekBar;

.field private deleteButton:Landroid/widget/ImageView;

.field private displayName:Ljava/lang/String;

.field private downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

.field private filterType:I

.field private isAutoPlayMode:Z

.field private isVisible:Z

.field private mContext:Landroid/content/Context;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private menuBtn:Landroid/widget/ImageView;

.field private mespos:I

.field private message:Lcom/att/mobile/android/vvm/model/Message;

.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private nameTextView:Landroid/widget/TextView;

.field private nextMessageButton:Landroid/widget/ImageView;

.field private numberOfmessages:Landroid/widget/TextView;

.field private phoneStateListener:Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;

.field private phoneTypeLayout:Landroid/view/View;

.field private phoneTypeTextView:Landroid/widget/TextView;

.field private playButton:Landroid/widget/ImageView;

.field private playerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;

.field private playerProgressTime:Landroid/widget/TextView;

.field private playerTimeDelim:Landroid/widget/TextView;

.field private playerTotalTime:Landroid/widget/TextView;

.field private previousMessageButton:Landroid/widget/ImageView;

.field private savedImageView:Landroid/widget/ImageView;

.field private screenLaunchedByInbox:Z

.field private sendMsgButton:Landroid/widget/ImageView;

.field private senderDisplayName:Ljava/lang/String;

.field private speakerButton:Landroid/widget/ImageView;

.field private speakerText:Landroid/widget/TextView;

.field private telephonyManager:Landroid/telephony/TelephonyManager;

.field private textTranscriptionBody:Landroid/widget/TextView;

.field private timer:Ljava/util/Timer;

.field private toolbar:Landroid/support/v7/widget/Toolbar;

.field private totalMessages:I

.field private urgentImageView:Landroid/widget/ImageView;

.field private vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 123
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactUri:Ljava/lang/String;

    .line 124
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->displayName:Ljava/lang/String;

    .line 129
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->screenLaunchedByInbox:Z

    .line 137
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 142
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactImageUri:Landroid/net/Uri;

    .line 143
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->senderDisplayName:Ljava/lang/String;

    .line 150
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 151
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isAutoPlayMode:Z

    .line 155
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactsChanged:Z

    .line 204
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->timer:Ljava/util/Timer;

    .line 1847
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isVisible:Z

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initializePlayerUI(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->goToNextMessage()V

    return-void
.end method

.method static synthetic access$1300(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    return v0
.end method

.method static synthetic access$1400(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setUpPopupMenu(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->stopPlayer()V

    return-void
.end method

.method static synthetic access$1600(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->exportTofile()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->shareFile()V

    return-void
.end method

.method static synthetic access$1800(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->viewCallerDetails()V

    return-void
.end method

.method static synthetic access$1900(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->copyTextToClipboard(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/db/ModelManager;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshPlayButton()V

    return-void
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/infra/media/AudioPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayerProgressUIComponents()V

    return-void
.end method

.method static synthetic access$800(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->handlePlayButtonClick(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$900(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->call()V

    return-void
.end method

.method private call()V
    .locals 5

    .prologue
    .line 1309
    :try_start_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 1310
    .local v2, "phoneNumber":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1311
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 1312
    :cond_0
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1313
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1318
    :goto_0
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->startActivity(Landroid/content/Intent;)V

    .line 1323
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "phoneNumber":Ljava/lang/String;
    :goto_1
    return-void

    .line 1315
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "phoneNumber":Ljava/lang/String;
    :cond_1
    const-string v3, "android.intent.action.CALL"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1316
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tel:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1319
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "phoneNumber":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1320
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "PlayerFragment"

    const-string v4, "Failed to invoke call"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private copyTextToClipboard(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1725
    const-string v3, "<br/>"

    const-string v4, ""

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 1728
    :try_start_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "clipboard"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ClipboardManager;

    .line 1729
    .local v1, "clipboard":Landroid/content/ClipboardManager;
    const-string v3, "transcription"

    invoke-static {v3, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 1731
    .local v0, "clip":Landroid/content/ClipData;
    invoke-virtual {v1, v0}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 1732
    const v3, 0x7f0801e2

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1738
    .end local v0    # "clip":Landroid/content/ClipData;
    .end local v1    # "clipboard":Landroid/content/ClipboardManager;
    :goto_0
    return-void

    .line 1734
    :catch_0
    move-exception v2

    .line 1735
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "PlayerFragment"

    const-string v4, "Failed to copy transcription to clipboard"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private exportTofile()Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1800
    const-string v6, "PlayerFragment"

    const-string v7, "exportTofile()"

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1803
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 1804
    .local v3, "time":Landroid/text/format/Time;
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/Message;->getDateMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1806
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v0

    .line 1808
    .local v0, "fileName":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 1809
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "voicemail-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%Y-%m-%d.%H.%M.%S"

    invoke-virtual {v3, v7}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1812
    .local v2, "sharedFileName":Ljava/lang/String;
    const-string v6, ".mp3"

    invoke-virtual {v0, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1813
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".mp3"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1819
    :goto_0
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 1820
    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Landroid/os/Environment;->DIRECTORY_MUSIC:Ljava/lang/String;

    .line 1819
    invoke-static {v6, v7, v2, v8, v5}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->copyFileToDeviceExternalStorage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1823
    sget-object v5, Landroid/os/Environment;->DIRECTORY_MUSIC:Ljava/lang/String;

    invoke-static {v5}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    .line 1824
    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1825
    .local v1, "folder":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1826
    const/16 v5, 0x2f

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1828
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0800f8

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " \n "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(Ljava/lang/String;I)V

    .line 1837
    .end local v1    # "folder":Ljava/lang/String;
    .end local v2    # "sharedFileName":Ljava/lang/String;
    :goto_1
    return v4

    .line 1815
    .restart local v2    # "sharedFileName":Ljava/lang/String;
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".amr"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1834
    :cond_2
    const v6, 0x7f080201

    invoke-static {v6, v4}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    .end local v2    # "sharedFileName":Ljava/lang/String;
    :cond_3
    move v4, v5

    .line 1837
    goto :goto_1
.end method

.method private goToNextMessage()V
    .locals 3

    .prologue
    .line 1344
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    add-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->totalMessages:I

    if-lt v1, v2, :cond_0

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    add-int/lit8 v1, v1, 0x2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->switchPage(I)V

    .line 1345
    return-void

    .line 1344
    :cond_0
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private handleNeverAskAgain()V
    .locals 7

    .prologue
    .line 1959
    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->wasNeverAskAgainChecked(Ljava/lang/String;Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f08016b

    const v2, 0x7f0800f1

    const v3, 0x7f080152

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/att/mobile/android/vvm/screen/PlayerFragment$16;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$16;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 1970
    :cond_0
    return-void
.end method

.method private handlePlayButtonClick(Landroid/view/View;)V
    .locals 8
    .param p1, "playButton"    # Landroid/view/View;

    .prologue
    const/4 v7, -0x1

    .line 1355
    const-string v4, "PlayerFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "#####handlePlayButtonClick()mespos ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1358
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v4}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1360
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v4}, Lcom/att/mobile/android/infra/media/AudioPlayer;->pause()V

    .line 1390
    :goto_0
    return-void

    .line 1366
    :cond_0
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    .line 1367
    .local v3, "seekBarProgress":I
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    .line 1369
    .local v2, "seekBarMax":I
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v4}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getMediaDuration()I

    move-result v0

    .line 1370
    .local v0, "mediaDuration":I
    if-ne v0, v7, :cond_1

    .line 1372
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/att/mobile/android/vvm/model/Message;->getFileFullPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/att/mobile/android/infra/media/AudioPlayer;->initializeMedia(Ljava/lang/String;)V

    .line 1373
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v4}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getMediaDuration()I

    move-result v0

    .line 1375
    :cond_1
    if-eq v0, v7, :cond_2

    .line 1376
    mul-int v4, v3, v0

    div-int v1, v4, v2

    .line 1378
    .local v1, "playbackPosition":I
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/VVMApplication;->setApplicationAudioMode()V

    .line 1379
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioManager:Landroid/media/AudioManager;

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-virtual {v4, p0, v5, v6}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 1380
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshSpeakerButton()V

    .line 1383
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v4, v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->start(I)V

    .line 1389
    .end local v1    # "playbackPosition":I
    :goto_1
    const-string v4, "PlayerFragment"

    const-string v5, "handlePlayButtonClick() ended"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1385
    :cond_2
    const v4, 0x7f080184

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_1
.end method

.method private increaseButtonHitArea(Landroid/view/View;)V
    .locals 2
    .param p1, "btn"    # Landroid/view/View;

    .prologue
    .line 1293
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1294
    .local v0, "parent":Landroid/view/View;
    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;

    invoke-direct {v1, p0, p1, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1305
    return-void
.end method

.method static init(Lcom/att/mobile/android/vvm/model/Message;IIZ)Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    .locals 3
    .param p0, "message"    # Lcom/att/mobile/android/vvm/model/Message;
    .param p1, "mespos"    # I
    .param p2, "totalMessages"    # I
    .param p3, "isAutoPlay"    # Z

    .prologue
    .line 608
    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;-><init>()V

    .line 610
    .local v1, "playerFrag":Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 611
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "currentMessage"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 612
    const-string v2, "POSITION"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 613
    const-string v2, "TOTAL"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 614
    const-string v2, "AUTOPLAY"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 615
    invoke-virtual {v1, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 617
    return-object v1
.end method

.method private initButtonsListeners()V
    .locals 3

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$3;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$3;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1182
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$4;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$4;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1194
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->callButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$5;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$5;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1208
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->sendMsgButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$6;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$6;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1224
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->deleteButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1251
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$8;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$8;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1258
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nextMessageButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$9;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$9;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1265
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->previousMessageButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$10;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$10;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1271
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nextMessageButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->increaseButtonHitArea(Landroid/view/View;)V

    .line 1272
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->previousMessageButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->increaseButtonHitArea(Landroid/view/View;)V

    .line 1274
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->backBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f08004a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1275
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->backBtn:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$11;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$11;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1282
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->menuBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f080121

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1283
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->menuBtn:Landroid/widget/ImageView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$12;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$12;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1289
    return-void
.end method

.method private initContactInfo()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1690
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContact(Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    .line 1691
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v3, :cond_1

    .line 1692
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    if-eqz v3, :cond_4

    .line 1694
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/ContactObject;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 1695
    .local v1, "displayName":Ljava/lang/String;
    const-string v3, "PlayerFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "#### ContactLoaderAsync displayName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1696
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v1

    .end local v1    # "displayName":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3, v1}, Lcom/att/mobile/android/vvm/model/Message;->setSenderDisplayName(Ljava/lang/String;)V

    .line 1697
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhoneType()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhoneLabel()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhoneLabel()Ljava/lang/String;

    move-result-object v2

    .line 1698
    .local v2, "phoneTypeText":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3, v2}, Lcom/att/mobile/android/vvm/model/Message;->setPhoneNumberLabel(Ljava/lang/String;)V

    .line 1699
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhotoUri()Landroid/net/Uri;

    move-result-object v0

    .line 1700
    .local v0, "contactPhotoUri":Landroid/net/Uri;
    if-eqz v0, :cond_3

    .line 1701
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3, v0}, Lcom/att/mobile/android/vvm/model/Message;->setContactImageUri(Landroid/net/Uri;)V

    .line 1705
    :goto_1
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/ContactObject;->getContactLookup()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1706
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/ContactObject;->getContactLookup()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/model/Message;->setContactLookupKey(Ljava/lang/String;)V

    .line 1716
    .end local v0    # "contactPhotoUri":Landroid/net/Uri;
    .end local v2    # "phoneTypeText":Ljava/lang/String;
    :cond_1
    :goto_2
    return-void

    .line 1697
    :cond_2
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhoneType()I

    move-result v3

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getPhoneTypeText(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1703
    .restart local v0    # "contactPhotoUri":Landroid/net/Uri;
    .restart local v2    # "phoneTypeText":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3, v6}, Lcom/att/mobile/android/vvm/model/Message;->setContactImageUri(Landroid/net/Uri;)V

    goto :goto_1

    .line 1709
    .end local v0    # "contactPhotoUri":Landroid/net/Uri;
    .end local v2    # "phoneTypeText":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    :goto_3
    invoke-virtual {v4, v3}, Lcom/att/mobile/android/vvm/model/Message;->setSenderDisplayName(Ljava/lang/String;)V

    .line 1710
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3, v6}, Lcom/att/mobile/android/vvm/model/Message;->setContactLookupKey(Ljava/lang/String;)V

    .line 1711
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3, v6}, Lcom/att/mobile/android/vvm/model/Message;->setSenderBitmap(Landroid/graphics/Bitmap;)V

    .line 1712
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3, v6}, Lcom/att/mobile/android/vvm/model/Message;->setContactImageUri(Landroid/net/Uri;)V

    .line 1713
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3, v6}, Lcom/att/mobile/android/vvm/model/Message;->setPhoneNumberLabel(Ljava/lang/String;)V

    goto :goto_2

    .line 1709
    :cond_5
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const v5, 0x7f0801af

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3
.end method

.method private initSenderName(Ljava/lang/String;)V
    .locals 3
    .param p1, "displName"    # Ljava/lang/String;

    .prologue
    .line 918
    if-nez p1, :cond_1

    .line 919
    const-string v0, "PlayerFragment"

    const-string v1, "initSenderName for NULL"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 928
    :cond_0
    :goto_0
    return-void

    .line 922
    :cond_1
    const-string v0, "PlayerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initSenderName displName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->senderDisplayName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->senderDisplayName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 924
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 925
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->senderDisplayName:Ljava/lang/String;

    .line 926
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nameTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    goto :goto_0
.end method

.method private initializePlayerUI(Z)V
    .locals 9
    .param p1, "audioFileExists"    # Z

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 951
    const-string v1, "PlayerFragment"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " initializePlayerUI() audioFileExists="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " messageId = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " mespos = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    if-eqz p1, :cond_2

    .line 954
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshPlayButton()V

    .line 956
    invoke-direct {p0, v7}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setPlayButton(I)V

    .line 958
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->clearAnimation()V

    .line 959
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v7}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 961
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerProgressTime:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 962
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTotalTime:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 963
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTimeDelim:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 965
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayerProgressUIComponents()V

    .line 1008
    :cond_0
    :goto_1
    const-string v0, "PlayerFragment"

    const-string v1, "initializePlayerUI() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    return-void

    .line 951
    :cond_1
    const-string v0, "null"

    goto :goto_0

    .line 967
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 969
    invoke-direct {p0, v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    .line 971
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerProgressTime:Landroid/widget/TextView;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 972
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTotalTime:Landroid/widget/TextView;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->formatDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 973
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerProgressTime:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 974
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTotalTime:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 975
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTimeDelim:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 977
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v6}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 979
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/Message;->getSavedState()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 980
    :cond_3
    invoke-direct {p0, v7}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setPlayButton(I)V

    .line 981
    invoke-direct {p0, v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    .line 982
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 983
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->stop()V

    .line 986
    :cond_4
    const v0, 0x7f080185

    invoke-static {v0, v7}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_1

    .line 990
    :cond_5
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->isMemoryLow()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 992
    invoke-direct {p0, v7}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setPlayButton(I)V

    .line 993
    invoke-direct {p0, v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    .line 994
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 995
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->stop()V

    .line 997
    :cond_6
    const v0, 0x7f08014a

    invoke-static {v0, v7}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto/16 :goto_1

    .line 999
    :cond_7
    const-string v0, "PlayerFragment"

    const-string v1, "initializePlayerUI() start downloading gauge"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    invoke-direct {p0, v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setPlayButton(I)V

    .line 1001
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1002
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->start()V

    goto/16 :goto_1
.end method

.method private isValidPhoneNumber(Ljava/lang/String;)Z
    .locals 1
    .param p1, "phoneNum"    # Ljava/lang/String;

    .prologue
    .line 1841
    if-eqz p1, :cond_0

    const-string v0, ""

    .line 1842
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0801fb

    .line 1844
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1843
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadContactImages(Landroid/net/Uri;)V
    .locals 4
    .param p1, "newContactImageUri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 931
    if-nez p1, :cond_1

    .line 932
    const-string v0, "PlayerFragment"

    const-string v1, "loadContactImages for NULL uri"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 942
    :cond_0
    :goto_0
    return-void

    .line 935
    :cond_1
    const-string v0, "PlayerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadContactImages newContactImageUri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactImageUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactImageUri:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 937
    :cond_2
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/PicassoTools;->clearCache(Lcom/squareup/picasso/Picasso;)V

    .line 938
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Landroid/net/Uri;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget-object v1, Lcom/squareup/picasso/MemoryPolicy;->NO_CACHE:Lcom/squareup/picasso/MemoryPolicy;

    new-array v2, v3, [Lcom/squareup/picasso/MemoryPolicy;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/picasso/RequestCreator;->memoryPolicy(Lcom/squareup/picasso/MemoryPolicy;[Lcom/squareup/picasso/MemoryPolicy;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->bgImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 939
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Landroid/net/Uri;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    new-instance v1, Lcom/att/mobile/android/infra/utils/PicassoUtils$CircleTransform;

    invoke-direct {v1}, Lcom/att/mobile/android/infra/utils/PicassoUtils$CircleTransform;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget-object v1, Lcom/squareup/picasso/MemoryPolicy;->NO_CACHE:Lcom/squareup/picasso/MemoryPolicy;

    new-array v2, v3, [Lcom/squareup/picasso/MemoryPolicy;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/picasso/RequestCreator;->memoryPolicy(Lcom/squareup/picasso/MemoryPolicy;[Lcom/squareup/picasso/MemoryPolicy;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    const v1, 0x7f020077

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->error(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 940
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactImageUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method private openCallScreen()V
    .locals 5

    .prologue
    .line 1327
    :try_start_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 1328
    .local v2, "phoneNumber":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1329
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 1330
    :cond_0
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1331
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1336
    :goto_0
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->startActivity(Landroid/content/Intent;)V

    .line 1341
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "phoneNumber":Ljava/lang/String;
    :goto_1
    return-void

    .line 1333
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "phoneNumber":Ljava/lang/String;
    :cond_1
    const-string v3, "android.intent.action.DIAL"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1334
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "tel:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1337
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "phoneNumber":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1338
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "PlayerFragment"

    const-string v4, "Failed to invoke call"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private refreshPlayButton()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1013
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eq v0, v2, :cond_1

    .line 1014
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1015
    invoke-direct {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    .line 1027
    :goto_0
    return-void

    .line 1018
    :cond_0
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    goto :goto_0

    .line 1021
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1022
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->pause()V

    .line 1023
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    .line 1025
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    goto :goto_0
.end method

.method private setPlayButton(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1031
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1032
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1033
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0, v2}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->setVisibility(I)V

    .line 1038
    :cond_0
    :goto_0
    return-void

    .line 1034
    :cond_1
    if-nez p1, :cond_0

    .line 1035
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1036
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private setStatusBarColor()V
    .locals 7

    .prologue
    const v6, 0x7f0e0020

    const/high16 v5, 0x4000000

    const/16 v4, 0x15

    const/high16 v3, -0x80000000

    .line 1875
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getContactImageUri()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1876
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v4, :cond_0

    .line 1877
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1878
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1, v3}, Landroid/view/Window;->addFlags(I)V

    .line 1879
    invoke-virtual {v1, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 1880
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 1881
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->bgImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 1892
    .end local v1    # "window":Landroid/view/Window;
    :cond_0
    :goto_0
    return-void

    .line 1884
    :cond_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    const-string v2, ""

    :goto_1
    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/Utils;->getDefaultAvatarBackground(Ljava/lang/String;)I

    move-result v0

    .line 1885
    .local v0, "color":I
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v4, :cond_0

    .line 1886
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1887
    .restart local v1    # "window":Landroid/view/Window;
    invoke-virtual {v1, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 1888
    invoke-virtual {v1, v3}, Landroid/view/Window;->addFlags(I)V

    .line 1889
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/support/v4/content/res/ResourcesCompat;->getColor(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0

    .line 1884
    .end local v0    # "color":I
    .end local v1    # "window":Landroid/view/Window;
    :cond_3
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private setUpPopupMenu(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1410
    new-instance v0, Landroid/support/v7/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 1411
    .local v0, "popupMenu":Landroid/support/v7/widget/PopupMenu;
    invoke-virtual {v0}, Landroid/support/v7/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f100003

    invoke-virtual {v0}, Landroid/support/v7/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1413
    invoke-virtual {v0}, Landroid/support/v7/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1415
    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/support/v7/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 1490
    invoke-virtual {v0}, Landroid/support/v7/widget/PopupMenu;->show()V

    .line 1491
    return-void
.end method

.method private shareAudioFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "directory"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1792
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->showAudioShareMenu(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1793
    return-void
.end method

.method private shareFile()V
    .locals 9

    .prologue
    .line 1748
    const-string v5, "PlayerFragment"

    const-string v6, "shareFile()"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1750
    :try_start_0
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v1

    .line 1751
    .local v1, "fileName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1754
    .local v3, "success":Z
    if-eqz v1, :cond_0

    .line 1756
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 1757
    .local v4, "time":Landroid/text/format/Time;
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->getDateMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1758
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "voicemail-shared-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%Y-%m-%d.%H.%M.%S"

    .line 1759
    invoke-virtual {v4, v6}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1762
    .local v2, "sharedFileName":Ljava/lang/String;
    const-string v5, ".mp3"

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1763
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".mp3"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1769
    :goto_0
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Landroid/os/Environment;->DIRECTORY_MUSIC:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v5, v6, v2, v7, v8}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->copyFileToDeviceExternalStorage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    .line 1770
    const-string v5, "PlayerFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "shareFile() save files to external storage, result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1772
    if-eqz v3, :cond_2

    .line 1773
    const-string v5, "PlayerFragment"

    const-string v6, "shareFile() file saved to external app storage, going to share it"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1774
    sget-object v5, Landroid/os/Environment;->DIRECTORY_MUSIC:Ljava/lang/String;

    invoke-static {v5}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->shareAudioFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1789
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "sharedFileName":Ljava/lang/String;
    .end local v3    # "success":Z
    .end local v4    # "time":Landroid/text/format/Time;
    :cond_0
    :goto_1
    return-void

    .line 1765
    .restart local v1    # "fileName":Ljava/lang/String;
    .restart local v2    # "sharedFileName":Ljava/lang/String;
    .restart local v3    # "success":Z
    .restart local v4    # "time":Landroid/text/format/Time;
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".amr"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1777
    :cond_2
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->copyFileAsReadable(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1779
    if-eqz v3, :cond_0

    .line 1780
    const-string v5, "PlayerFragment"

    const-string v6, "shareFile() file saved to internal app storage, going to share it"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1781
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->shareAudioFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1786
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "sharedFileName":Ljava/lang/String;
    .end local v3    # "success":Z
    .end local v4    # "time":Landroid/text/format/Time;
    :catch_0
    move-exception v0

    .line 1787
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "PlayerFragment"

    const-string v6, "Failed to share attachment"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private showSavedDialog(Landroid/app/Activity;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1905
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v1, "doNotShowSavedDialogAgain"

    const-class v2, Ljava/lang/Boolean;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1907
    const v1, 0x7f080125

    const v2, 0x7f080124

    const v3, 0x7f0800eb

    const v4, 0x7f080153

    const v5, 0x7f0801bc

    new-instance v6, Lcom/att/mobile/android/vvm/screen/PlayerFragment$15;

    invoke-direct {v6, p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$15;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Landroid/app/Activity;)V

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialogWithCB(Landroid/content/Context;IIIIILcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 1923
    :cond_0
    return-void
.end method

.method private startObservingPlayerPlaybackProgress()V
    .locals 6

    .prologue
    const-wide/16 v2, 0xc8

    .line 547
    const-string v0, "PlayerFragment"

    const-string v1, "startObservingPlayerPlaybackProgress()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;

    if-nez v0, :cond_0

    .line 551
    new-instance v0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Lcom/att/mobile/android/vvm/screen/PlayerFragment$1;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;

    .line 552
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 557
    :cond_0
    const-string v0, "PlayerFragment"

    const-string v1, "startObservingPlayerPlaybackProgress() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    return-void
.end method

.method private stopObservingPlayerPlaybackProgress()V
    .locals 2

    .prologue
    .line 565
    const-string v0, "PlayerFragment"

    const-string v1, "stopObservingPlayerPlaybackProgress()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;->cancel()Z

    .line 571
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;

    .line 574
    :cond_0
    const-string v0, "PlayerFragment"

    const-string v1, "stopObservingPlayerPlaybackProgress() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    return-void
.end method

.method private stopPlayer()V
    .locals 4

    .prologue
    .line 1895
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1896
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->stop()V

    .line 1897
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->reset()V

    .line 1898
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1899
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerProgressTime:Landroid/widget/TextView;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1900
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->unregisterProximityListener()V

    .line 1902
    :cond_0
    return-void
.end method

.method private updateMessage()V
    .locals 4

    .prologue
    .line 253
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageFileName(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/Message;->setFileName(Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method private updateMessageTranscriptionUI()V
    .locals 2

    .prologue
    .line 843
    const-string v0, "PlayerFragment"

    const-string v1, "updateMessageTranscriptionUI()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->textTranscriptionBody:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 846
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->textTranscriptionBody:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinksClickable(Z)V

    .line 847
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->textTranscriptionBody:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/Message;->getTranscription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 849
    return-void
.end method

.method private updatePlayButtonUI(I)V
    .locals 4
    .param p1, "playButtonType"    # I

    .prologue
    const v2, 0x7f080181

    const/4 v3, 0x1

    .line 1047
    const-string v0, "PlayerFragment"

    const-string v1, "updatePlayButtonUI()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1048
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 1086
    :cond_0
    :goto_0
    return-void

    .line 1053
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 1085
    :goto_1
    const-string v0, "PlayerFragment"

    const-string v1, "updatePlayButtonUI() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1058
    :pswitch_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    const v1, 0x7f0200ed

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1059
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1060
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_1

    .line 1066
    :pswitch_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    const v1, 0x7f0200ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1067
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1068
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_1

    .line 1075
    :pswitch_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    const v1, 0x7f0200ef

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1076
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f080168

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1077
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_1

    .line 1053
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private updatePlayerProgressTextUIComponent()V
    .locals 4

    .prologue
    .line 1122
    const-string v1, "PlayerFragment"

    const-string v2, "updatePlayerProgressTextUIComponent()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getCurrentPosition()I

    move-result v0

    .line 1129
    .local v0, "mediaCurrentPosition":I
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerProgressTime:Landroid/widget/TextView;

    div-int/lit16 v2, v0, 0x3e8

    int-to-long v2, v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1131
    const-string v1, "PlayerFragment"

    const-string v2, "updatePlayerProgressTextUIComponent() ended"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1132
    return-void
.end method

.method private updatePlayerProgressUIComponents()V
    .locals 2

    .prologue
    .line 1093
    const-string v0, "PlayerFragment"

    const-string v1, "updatePlayerProgressUIComponents()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayerSeekBarProgressUIComponent()V

    .line 1096
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayerProgressTextUIComponent()V

    .line 1098
    const-string v0, "PlayerFragment"

    const-string v1, "updatePlayerProgressUIComponents() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    return-void
.end method

.method private updatePlayerSeekBarProgressUIComponent()V
    .locals 4

    .prologue
    .line 1107
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v3}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getMediaDuration()I

    move-result v1

    .line 1108
    .local v1, "mediaDuration":I
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v3}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getCurrentPosition()I

    move-result v0

    .line 1111
    .local v0, "mediaCurrentPosition":I
    if-lez v1, :cond_0

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    mul-int/2addr v3, v0

    div-int v2, v3, v1

    .line 1113
    .local v2, "progress":I
    :goto_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1115
    return-void

    .line 1111
    .end local v2    # "progress":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private viewCallerDetails()V
    .locals 5

    .prologue
    .line 1673
    const-string v3, "PlayerFragment"

    const-string v4, "PlayerActivity.viewCallerDetails()"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1676
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1677
    .local v1, "intent":Landroid/content/Intent;
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 1678
    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/Message;->getContactLookupKey()Ljava/lang/String;

    move-result-object v4

    .line 1677
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1679
    .local v2, "lookupUri":Landroid/net/Uri;
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1680
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1681
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1685
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "lookupUri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 1682
    :catch_0
    move-exception v0

    .line 1683
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "PlayerFragment"

    const-string v4, "Failed to invoke contacts activity"

    invoke-static {v3, v4, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public getFormattedDisplayName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const v3, 0x7f0801af

    .line 1976
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1978
    .local v0, "context":Landroid/content/Context;
    move-object v1, p1

    .line 1979
    .local v1, "displayName":Ljava/lang/String;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    if-eqz v2, :cond_0

    .line 1980
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactObject:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/ContactObject;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 1981
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 1993
    :goto_0
    return-object v2

    .line 1985
    :cond_0
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1986
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1989
    :cond_1
    const v2, 0x7f0801fb

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1991
    const v2, 0x7f0800d8

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    move-object v2, v1

    .line 1993
    goto :goto_0
.end method

.method public onAudioFocusChange(I)V
    .locals 0
    .param p1, "focusChange"    # I

    .prologue
    .line 353
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 700
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 701
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    .line 702
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 703
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 704
    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Lcom/att/mobile/android/vvm/screen/PlayerFragment$1;)V

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneStateListener:Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;

    .line 705
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    .line 706
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 707
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "currentMessage"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/vvm/model/Message;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 708
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "POSITION"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    .line 709
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "TOTAL"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->totalMessages:I

    .line 710
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "AUTOPLAY"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isAutoPlayMode:Z

    .line 711
    const-string v1, "PlayerFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate()  mespos="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    :cond_0
    new-instance v1, Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-direct {v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;-><init>()V

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 714
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioManager:Landroid/media/AudioManager;

    .line 715
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentActivity;->setVolumeControlStream(I)V

    .line 717
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->isAccesibilityOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 718
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setApplicationAudioMode()V

    .line 719
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x2

    invoke-virtual {v1, p0, v4, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 721
    :cond_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshSpeakerButton()V

    .line 723
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 724
    const/16 v0, 0x20

    .line 725
    .local v0, "events":I
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneStateListener:Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;

    invoke-virtual {v1, v2, v0}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 726
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 1500
    const v0, 0x7f100003

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1501
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1502
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 731
    const v3, 0x7f040053

    invoke-virtual {p1, v3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 733
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0f013f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->backBtn:Landroid/widget/ImageView;

    .line 734
    const v3, 0x7f0f0145

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->menuBtn:Landroid/widget/ImageView;

    .line 736
    const v3, 0x7f0f0112

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nextMessageButton:Landroid/widget/ImageView;

    .line 737
    const v3, 0x7f0f0140

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->previousMessageButton:Landroid/widget/ImageView;

    .line 738
    const v3, 0x7f0f0143

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->numberOfmessages:Landroid/widget/TextView;

    .line 740
    const v3, 0x7f0f0147

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/QuickContactBadge;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    .line 741
    const v3, 0x7f0f013d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->bgImageView:Landroid/widget/ImageView;

    .line 744
    const v3, 0x7f0f011a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nameTextView:Landroid/widget/TextView;

    .line 745
    const v3, 0x7f0f0114

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->dateTextView:Landroid/widget/TextView;

    .line 746
    const v3, 0x7f0f014b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneTypeTextView:Landroid/widget/TextView;

    .line 747
    const v3, 0x7f0f0149

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneTypeLayout:Landroid/view/View;

    .line 748
    const v3, 0x7f0f0148

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->savedImageView:Landroid/widget/ImageView;

    .line 749
    const v3, 0x7f0f011b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->urgentImageView:Landroid/widget/ImageView;

    .line 750
    const v3, 0x7f0f0150

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    .line 751
    const v3, 0x7f0f0155

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerText:Landroid/widget/TextView;

    .line 754
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->numberOfmessages:Landroid/widget/TextView;

    sget-object v6, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 755
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nameTextView:Landroid/widget/TextView;

    sget-object v6, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 756
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->dateTextView:Landroid/widget/TextView;

    sget-object v6, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 757
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneTypeTextView:Landroid/widget/TextView;

    sget-object v6, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 758
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerText:Landroid/widget/TextView;

    sget-object v6, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 759
    const v3, 0x7f0f0151

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    .line 761
    const v3, 0x7f0f014d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    .line 763
    const v3, 0x7f0f0154

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerProgressTime:Landroid/widget/TextView;

    .line 764
    const v3, 0x7f0f00a0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTimeDelim:Landroid/widget/TextView;

    .line 765
    const v3, 0x7f0f0153

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTotalTime:Landroid/widget/TextView;

    .line 766
    const v3, 0x7f0f0146

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->callButton:Landroid/widget/ImageView;

    .line 767
    const v3, 0x7f0f014c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->sendMsgButton:Landroid/widget/ImageView;

    .line 768
    const v3, 0x7f0f0144

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->deleteButton:Landroid/widget/ImageView;

    .line 769
    const v3, 0x7f0f0156

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->textTranscriptionBody:Landroid/widget/TextView;

    .line 771
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->textTranscriptionBody:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVerticalFadingEdgeEnabled(Z)V

    .line 773
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerProgressTime:Landroid/widget/TextView;

    sget-object v6, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 774
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTotalTime:Landroid/widget/TextView;

    sget-object v6, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 775
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->textTranscriptionBody:Landroid/widget/TextView;

    sget-object v6, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v6}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 778
    const v3, 0x7f0f0152

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    .line 779
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->numberOfmessages:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->totalMessages:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 780
    const v3, 0x7f0f0141

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 781
    .local v0, "messageNumberLayout":Landroid/widget/LinearLayout;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f080122

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->numberOfmessages:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 782
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSavedState()I

    move-result v3

    const/4 v6, 0x4

    if-ne v3, v6, :cond_0

    .line 783
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->savedImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 786
    :cond_0
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->previousMessageButton:Landroid/widget/ImageView;

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    if-lez v3, :cond_3

    move v3, v4

    :goto_0
    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 787
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->previousMessageButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->invalidate()V

    .line 788
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nextMessageButton:Landroid/widget/ImageView;

    iget v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    add-int/lit8 v6, v6, 0x1

    iget v7, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->totalMessages:I

    if-ge v6, v7, :cond_4

    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 790
    const v3, 0x7f0f0079

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/Toolbar;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 792
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    if-eqz v3, :cond_1

    .line 793
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 794
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    .line 795
    .local v1, "supportActionBar":Landroid/support/v7/app/ActionBar;
    invoke-virtual {v1, v5}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 799
    .end local v1    # "supportActionBar":Landroid/support/v7/app/ActionBar;
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initContactInfo()V

    .line 800
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updateContactDetailsUI()V

    .line 801
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updateMessageTranscriptionUI()V

    .line 804
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayer()V

    .line 807
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initButtonsListeners()V

    .line 808
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshSpeakerButton()V

    .line 810
    iget-boolean v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isAutoPlayMode:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getUserVisibleHint()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 811
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getProximitySwitcher()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 812
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->registerProximityListener()V

    .line 816
    :goto_2
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->handlePlayButtonClick(Landroid/view/View;)V

    .line 819
    :cond_2
    return-object v2

    :cond_3
    move v3, v5

    .line 786
    goto :goto_0

    :cond_4
    move v4, v5

    .line 788
    goto :goto_1

    .line 814
    :cond_5
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/VVMApplication;->acquireWakeLock()V

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 649
    const-string v0, "PlayerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy()  mespos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneStateListener:Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 652
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 653
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->release()V

    .line 656
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 658
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioManager:Landroid/media/AudioManager;

    .line 660
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->nextMessageButton:Landroid/widget/ImageView;

    .line 661
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->previousMessageButton:Landroid/widget/ImageView;

    .line 662
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    .line 663
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->savedImageView:Landroid/widget/ImageView;

    .line 664
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->urgentImageView:Landroid/widget/ImageView;

    .line 665
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playButton:Landroid/widget/ImageView;

    .line 666
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->defaultSeekBar:Landroid/widget/SeekBar;

    .line 667
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerProgressTime:Landroid/widget/TextView;

    .line 668
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTimeDelim:Landroid/widget/TextView;

    .line 669
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTotalTime:Landroid/widget/TextView;

    .line 670
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->callButton:Landroid/widget/ImageView;

    .line 671
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->sendMsgButton:Landroid/widget/ImageView;

    .line 672
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->deleteButton:Landroid/widget/ImageView;

    .line 673
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->textTranscriptionBody:Landroid/widget/TextView;

    .line 674
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    .line 675
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mProgressBar:Landroid/widget/ProgressBar;

    .line 676
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactUri:Ljava/lang/String;

    .line 677
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->displayName:Ljava/lang/String;

    .line 678
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    .line 679
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 680
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneStateListener:Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;

    .line 681
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 682
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 683
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->bgImageView:Landroid/widget/ImageView;

    .line 684
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->downloadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    .line 689
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 691
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 692
    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->numberOfmessages:Landroid/widget/TextView;

    .line 694
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 695
    return-void
.end method

.method public onNetworkFailure()V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 11
    .param p1, "item"    # Landroid/view/MenuItem;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1594
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 1595
    .local v3, "phoneNumber":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .line 1597
    .local v0, "activity":Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 1665
    :cond_0
    :goto_1
    return v8

    .line 1594
    .end local v0    # "activity":Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    .end local v3    # "phoneNumber":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1600
    .restart local v0    # "activity":Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    .restart local v3    # "phoneNumber":Ljava/lang/String;
    :sswitch_0
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->onBackPressed()V

    .line 1601
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->stopPlayer()V

    goto :goto_1

    .line 1605
    :sswitch_1
    if-eqz v0, :cond_0

    .line 1606
    new-array v5, v8, [Ljava/lang/Long;

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v9

    const/4 v6, 0x4

    invoke-virtual {v0, v5, v6}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->markMessagesAs([Ljava/lang/Long;I)V

    goto :goto_1

    .line 1610
    :sswitch_2
    if-eqz v0, :cond_0

    .line 1611
    new-array v5, v8, [Ljava/lang/Long;

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v0, v5, v10}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->markMessagesAs([Ljava/lang/Long;I)V

    goto :goto_1

    .line 1616
    :sswitch_3
    if-eqz v3, :cond_0

    .line 1618
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1619
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "vnd.android.cursor.item/contact"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1620
    const-string v5, "phone"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1621
    const-string v5, "phone_type"

    const/4 v6, 0x2

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1622
    const/high16 v5, 0x10000000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1623
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1624
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 1625
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "PlayerFragment"

    const-string v6, "Failed to invoke contacts activity"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1631
    .end local v1    # "e":Ljava/lang/Exception;
    :sswitch_4
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string v6, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v5, v6}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_2

    .line 1632
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->exportTofile()Z

    goto :goto_1

    .line 1634
    :cond_2
    new-array v5, v8, [Ljava/lang/String;

    const-string v6, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v6, v5, v9

    const/16 v6, 0xb

    invoke-virtual {p0, v5, v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_1

    .line 1639
    :sswitch_5
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string v6, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v5, v6}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    .line 1640
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->shareFile()V

    goto/16 :goto_1

    .line 1642
    :cond_3
    new-array v5, v8, [Ljava/lang/String;

    const-string v6, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v6, v5, v9

    const/16 v6, 0xc

    invoke-virtual {p0, v5, v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->requestPermissions([Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1646
    :sswitch_6
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->viewCallerDetails()V

    goto/16 :goto_1

    .line 1650
    :sswitch_7
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->getTranscription()Ljava/lang/String;

    move-result-object v4

    .line 1651
    .local v4, "text":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 1652
    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->copyTextToClipboard(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1597
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0189 -> :sswitch_5
        0x7f0f018a -> :sswitch_2
        0x7f0f018b -> :sswitch_1
        0x7f0f018c -> :sswitch_4
        0x7f0f018d -> :sswitch_7
        0x7f0f018e -> :sswitch_6
        0x7f0f018f -> :sswitch_3
    .end sparse-switch
.end method

.method public onPlaybackEnd()V
    .locals 3

    .prologue
    .line 503
    const-string v0, "PlayerFragment"

    const-string v1, "onPlaybackEnd()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->onPlaybackStop()V

    .line 508
    const-string v0, "PlayerFragment"

    const-string v1, "onPlaybackEnd() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isAutoPlayMode:Z

    if-eqz v0, :cond_0

    .line 510
    const-string v0, "PlayerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#####onPlaybackEnd()mespos ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->totalMessages:I

    if-ne v0, v1, :cond_1

    .line 512
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->unregisterProximityListener()V

    .line 513
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 515
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->goToNextMessage()V

    goto :goto_0
.end method

.method public onPlaybackPause()V
    .locals 2

    .prologue
    .line 455
    const-string v0, "PlayerFragment"

    const-string v1, "onPlaybackPause()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->stopObservingPlayerPlaybackProgress()V

    .line 460
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    .line 461
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getProximitySwitcher()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->unregisterProximityListener()V

    .line 469
    :goto_0
    const-string v0, "PlayerFragment"

    const-string v1, "onPlaybackPause() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    return-void

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->releaseWakeLock()V

    goto :goto_0
.end method

.method public onPlaybackStart(I)V
    .locals 4
    .param p1, "startingPosition"    # I

    .prologue
    .line 430
    const-string v0, "PlayerFragment"

    const-string v1, "onPlaybackStart()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/Message;->isRead()Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/Message;->getUid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->updateMessageToMarkAsReadStatus(J)V

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getProximitySwitcher()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 438
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->registerProximityListener()V

    .line 444
    :goto_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->startObservingPlayerPlaybackProgress()V

    .line 447
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    .line 449
    const-string v0, "PlayerFragment"

    const-string v1, "onPlaybackStart() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    return-void

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->acquireWakeLock()V

    goto :goto_0
.end method

.method public onPlaybackStop()V
    .locals 3

    .prologue
    .line 475
    const-string v1, "PlayerFragment"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPlaybackStop() isProximityHeld="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->wasProximityHold()Z

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->stopObservingPlayerPlaybackProgress()V

    .line 480
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayButtonUI(I)V

    .line 484
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayerProgressUIComponents()V

    .line 488
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getProximitySwitcher()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->unregisterProximityListener()V

    .line 497
    :goto_0
    const-string v0, "PlayerFragment"

    const-string v1, "onPlaybackStop() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    return-void

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->releaseWakeLock()V

    goto :goto_0
.end method

.method public onPlayerError(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 522
    const-string v0, "PlayerFragment"

    const-string v1, "onPlayerError()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    const/16 v0, 0x270f

    if-eq p1, v0, :cond_0

    .line 528
    :cond_0
    new-instance v0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerSetErrorAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerSetErrorAsyncTask;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerSetErrorAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 530
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->reset()V

    .line 531
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getProximitySwitcher()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 532
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->unregisterProximityListener()V

    .line 538
    :goto_0
    const-string v0, "PlayerFragment"

    const-string v1, "onPlayerError() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    return-void

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->releaseWakeLock()V

    goto :goto_0
.end method

.method public onPlayerInitialization(I)V
    .locals 2
    .param p1, "mediaToBePlayedDuration"    # I

    .prologue
    .line 417
    const-string v0, "PlayerFragment"

    const-string v1, "onPlayerInitialization()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initializePlayerUI(Z)V

    .line 423
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->playerTotalTime:Landroid/widget/TextView;

    div-int/lit16 v1, p1, 0x3e8

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->formatDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    const-string v0, "PlayerFragment"

    const-string v1, "onPlayerInitialization() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 14
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1513
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1515
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_8

    const/4 v5, 0x1

    .line 1516
    .local v5, "isVoiceFileExist":Z
    :goto_0
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_9

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getSavedState()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_9

    const/4 v3, 0x1

    .line 1517
    .local v3, "isError":Z
    :goto_1
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_a

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getSavedState()I

    move-result v11

    const/4 v12, 0x4

    if-ne v11, v12, :cond_a

    const/4 v4, 0x1

    .line 1520
    .local v4, "isSaved":Z
    :goto_2
    const v11, 0x7f0f018b

    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 1521
    .local v7, "save":Landroid/view/MenuItem;
    const v11, 0x7f0f018a

    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    .line 1525
    .local v9, "unsave":Landroid/view/MenuItem;
    if-eqz v7, :cond_0

    if-eqz v9, :cond_0

    .line 1526
    if-eqz v4, :cond_c

    .line 1527
    const/4 v11, 0x0

    invoke-interface {v7, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1528
    const/4 v11, 0x1

    invoke-interface {v9, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1529
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_b

    if-eqz v5, :cond_b

    if-nez v3, :cond_b

    const/4 v11, 0x1

    :goto_3
    invoke-interface {v9, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1538
    :cond_0
    :goto_4
    const v11, 0x7f0f0189

    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    .line 1539
    .local v8, "share":Landroid/view/MenuItem;
    if-eqz v8, :cond_1

    .line 1540
    if-eqz v5, :cond_e

    if-nez v3, :cond_e

    const/4 v11, 0x1

    :goto_5
    invoke-interface {v8, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1543
    :cond_1
    const v11, 0x7f0f018c

    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1544
    .local v2, "export":Landroid/view/MenuItem;
    if-eqz v2, :cond_2

    .line 1545
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_f

    if-eqz v5, :cond_f

    if-nez v3, :cond_f

    invoke-static {}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->isExternalStorageExist()Z

    move-result v11

    if-eqz v11, :cond_f

    const/4 v11, 0x1

    :goto_6
    invoke-interface {v2, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1548
    :cond_2
    const v11, 0x7f0f018e

    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    .line 1549
    .local v10, "viewCallerDetails":Landroid/view/MenuItem;
    const v11, 0x7f0f018f

    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1550
    .local v0, "addToContacts":Landroid/view/MenuItem;
    if-eqz v10, :cond_3

    if-eqz v0, :cond_3

    .line 1551
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_10

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getContactLookupKey()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_10

    .line 1552
    const/4 v11, 0x1

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1553
    const/4 v11, 0x1

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1554
    const/4 v11, 0x0

    invoke-interface {v0, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1568
    :cond_3
    :goto_7
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 1569
    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    const v13, 0x7f0801e7

    invoke-virtual {v12, v13}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_4

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v11

    const v12, 0x7f0801fb

    invoke-virtual {p0, v12}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1570
    :cond_4
    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1571
    const/4 v11, 0x0

    invoke-interface {v0, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1575
    :cond_5
    const v11, 0x7f0f018d

    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1576
    .local v1, "copyMessageText":Landroid/view/MenuItem;
    if-eqz v1, :cond_7

    .line 1577
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_12

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getTranscription()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1579
    .local v6, "messageText":Ljava/lang/String;
    :goto_8
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_6

    const-string v11, " "

    .line 1580
    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    const v11, 0x7f0801e4

    .line 1581
    invoke-virtual {p0, v11}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    const v11, 0x7f08014b

    .line 1582
    invoke-virtual {p0, v11}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 1583
    :cond_6
    const/4 v11, 0x0

    invoke-interface {v1, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1588
    .end local v6    # "messageText":Ljava/lang/String;
    :cond_7
    :goto_9
    return-void

    .line 1515
    .end local v0    # "addToContacts":Landroid/view/MenuItem;
    .end local v1    # "copyMessageText":Landroid/view/MenuItem;
    .end local v2    # "export":Landroid/view/MenuItem;
    .end local v3    # "isError":Z
    .end local v4    # "isSaved":Z
    .end local v5    # "isVoiceFileExist":Z
    .end local v7    # "save":Landroid/view/MenuItem;
    .end local v8    # "share":Landroid/view/MenuItem;
    .end local v9    # "unsave":Landroid/view/MenuItem;
    .end local v10    # "viewCallerDetails":Landroid/view/MenuItem;
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 1516
    .restart local v5    # "isVoiceFileExist":Z
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1517
    .restart local v3    # "isError":Z
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1529
    .restart local v4    # "isSaved":Z
    .restart local v7    # "save":Landroid/view/MenuItem;
    .restart local v9    # "unsave":Landroid/view/MenuItem;
    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 1531
    :cond_c
    const/4 v11, 0x1

    invoke-interface {v7, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1532
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_d

    if-eqz v5, :cond_d

    if-nez v4, :cond_d

    if-nez v3, :cond_d

    const/4 v11, 0x1

    :goto_a
    invoke-interface {v7, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1533
    const/4 v11, 0x0

    invoke-interface {v9, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_4

    .line 1532
    :cond_d
    const/4 v11, 0x0

    goto :goto_a

    .line 1540
    .restart local v8    # "share":Landroid/view/MenuItem;
    :cond_e
    const/4 v11, 0x0

    goto/16 :goto_5

    .line 1545
    .restart local v2    # "export":Landroid/view/MenuItem;
    :cond_f
    const/4 v11, 0x0

    goto/16 :goto_6

    .line 1556
    .restart local v0    # "addToContacts":Landroid/view/MenuItem;
    .restart local v10    # "viewCallerDetails":Landroid/view/MenuItem;
    :cond_10
    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1557
    const/4 v11, 0x1

    invoke-interface {v0, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1558
    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v11, :cond_11

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_11

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 1559
    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_11

    iget-object v11, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 1560
    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v11

    const v12, 0x7f0801fb

    invoke-virtual {p0, v12}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_11

    .line 1561
    const/4 v11, 0x1

    invoke-interface {v0, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_7

    .line 1563
    :cond_11
    const/4 v11, 0x0

    invoke-interface {v0, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_7

    .line 1577
    .restart local v1    # "copyMessageText":Landroid/view/MenuItem;
    :cond_12
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 1585
    .restart local v6    # "messageText":Ljava/lang/String;
    :cond_13
    const/4 v11, 0x1

    invoke-interface {v1, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_9
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .prologue
    .line 1927
    const-string v0, "PlayerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRequestPermissionsResult requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1928
    packed-switch p1, :pswitch_data_0

    .line 1954
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 1956
    :goto_0
    return-void

    .line 1931
    :pswitch_0
    array-length v0, p3

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_0

    .line 1932
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->call()V

    goto :goto_0

    .line 1934
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->openCallScreen()V

    goto :goto_0

    .line 1939
    :pswitch_1
    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->isPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1940
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->exportTofile()Z

    goto :goto_0

    .line 1942
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->handleNeverAskAgain()V

    goto :goto_0

    .line 1946
    :pswitch_2
    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->isPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1947
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->shareFile()V

    goto :goto_0

    .line 1949
    :cond_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->handleNeverAskAgain()V

    goto :goto_0

    .line 1928
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 622
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 623
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactsChanged:Z

    if-eqz v0, :cond_0

    .line 624
    new-instance v0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-direct {v0, p0, v1, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Ljava/lang/String;Lcom/att/mobile/android/vvm/model/Message;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 625
    iput-boolean v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactsChanged:Z

    .line 627
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshSpeakerButton()V

    .line 628
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshPlayButton()V

    .line 629
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 6
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/16 v2, 0x30

    if-ne p1, v2, :cond_1

    .line 218
    const-string v2, "PlayerFragment"

    const-string v3, "onUpdateListener() CONTACTS_CHANGED"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactsChanged:Z

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    const/16 v2, 0x4e

    if-ne p1, v2, :cond_2

    .line 221
    const-string v2, "PlayerFragment"

    const-string v3, "onUpdateListener() PLAYER_PAUSED"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->wasProximityHold()Z

    move-result v2

    if-nez v2, :cond_0

    .line 223
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 224
    const-string v2, "PlayerFragment"

    const-string v3, "####### onUpdateListener()  wasProximityHold = false and audioPlayer is playing"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->pause()V

    goto :goto_0

    .line 228
    :cond_2
    const/16 v2, 0x4c

    if-ne p1, v2, :cond_3

    .line 229
    const-string v2, "PlayerFragment"

    const-string v3, "onUpdateListener() MESSAGE_CONTACT_INFO_UPDATED"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->runUpdateUI()V

    goto :goto_0

    .line 231
    :cond_3
    const/16 v2, 0x16

    if-ne p1, v2, :cond_4

    .line 232
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getUserVisibleHint()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 233
    const-string v2, "PlayerFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "##### onUpdateListener() current visible fragment RETRIEVE_HEADERS_FINISHED call refreshAdapter mesId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mespos = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v4

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    invoke-virtual {v2, v4, v5, v3}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->refreshAdapter(JI)V

    goto/16 :goto_0

    .line 236
    :cond_4
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    .line 237
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 238
    .local v0, "mesId":J
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v4

    cmp-long v2, v0, v4

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getUserVisibleHint()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 239
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updateMessage()V

    .line 240
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v4, Lcom/att/mobile/android/vvm/screen/PlayerFragment$1;

    invoke-direct {v4, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$1;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v2, v4}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 246
    const-string v2, "PlayerFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "##### onUpdateListener() current visible fragment MESSAGE_FILE_DOWNLOADED  mesId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " message and player updated "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public refreshSpeakerButton()V
    .locals 4

    .prologue
    const v3, 0x7f0801d1

    const v2, 0x7f0801d0

    .line 824
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 825
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 826
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    const v1, 0x7f0200f2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 827
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 828
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 834
    :goto_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 836
    :cond_0
    return-void

    .line 830
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    const v1, 0x7f0200f4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 831
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 832
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->speakerText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method runUpdateUI()V
    .locals 2

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/PicassoTools;->clearCache(Lcom/squareup/picasso/Picasso;)V

    .line 263
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$2;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 270
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 1851
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 1852
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1853
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setStatusBarColor()V

    .line 1855
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isVisible:Z

    .line 1856
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshSpeakerButton()V

    .line 1857
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1858
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_0

    .line 1859
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updatePlayer()V

    .line 1860
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isAutoPlayMode:Z

    if-eqz v0, :cond_0

    .line 1861
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1862
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->handlePlayButtonClick(Landroid/view/View;)V

    .line 1872
    :cond_0
    :goto_0
    return-void

    .line 1868
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isVisible:Z

    .line 1869
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->stopPlayer()V

    goto :goto_0
.end method

.method updateContactDetailsUI()V
    .locals 11

    .prologue
    const/4 v7, 0x4

    const/16 v9, 0xff

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 857
    const-string v4, "PlayerFragment"

    const-string v8, "updateMessageDetailsUI()"

    invoke-static {v4, v8}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 858
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 859
    .local v3, "phoneNumber":Ljava/lang/String;
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/Message;->getContactImageUri()Landroid/net/Uri;

    move-result-object v2

    .line 860
    .local v2, "imageUri":Landroid/net/Uri;
    iget-boolean v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isVisible:Z

    if-eqz v4, :cond_0

    .line 861
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setStatusBarColor()V

    .line 863
    :cond_0
    if-eqz v2, :cond_1

    .line 864
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4, v6}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    .line 865
    invoke-direct {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->loadContactImages(Landroid/net/Uri;)V

    .line 866
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4}, Landroid/widget/QuickContactBadge;->invalidate()V

    .line 880
    :goto_0
    iget-object v8, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->callButton:Landroid/widget/ImageView;

    invoke-direct {p0, v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isValidPhoneNumber(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    move v4, v5

    :goto_1
    invoke-virtual {v8, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 882
    iget-object v8, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->sendMsgButton:Landroid/widget/ImageView;

    invoke-direct {p0, v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->isValidPhoneNumber(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    move v4, v5

    :goto_2
    invoke-virtual {v8, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 883
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->sendMsgButton:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->invalidate()V

    .line 885
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/Message;->getContactLookupKey()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 886
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->setClickable(Z)V

    .line 887
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    sget-object v5, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    iget-object v8, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/model/Message;->getContactLookupKey()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->assignContactUri(Landroid/net/Uri;)V

    .line 888
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f0800c8

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 897
    :goto_3
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4}, Landroid/widget/QuickContactBadge;->invalidate()V

    .line 898
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/Message;->getSenderDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getFormattedDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 899
    .local v1, "displayName":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initSenderName(Ljava/lang/String;)V

    .line 902
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->dateTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->getDateMillis()J

    move-result-wide v8

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-static {v8, v9, v5, v10}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getFriendlyDate(JLandroid/content/Context;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 904
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/Message;->getPhoneNumberLabel()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 905
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneTypeLayout:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 906
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneTypeTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->getPhoneNumberLabel()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 912
    :goto_4
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->urgentImageView:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->isUrgent()Z

    move-result v5

    if-eqz v5, :cond_9

    :goto_5
    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 914
    const-string v4, "PlayerFragment"

    const-string v5, "updateMessageDetailsUI() ended"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    return-void

    .line 868
    .end local v1    # "displayName":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->bgImageView:Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    .line 869
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->bgImageView:Landroid/widget/ImageView;

    invoke-static {v6, v9, v9, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 870
    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/Utils;->getDefaultAvatarBackground(Ljava/lang/String;)I

    move-result v0

    .line 871
    .local v0, "color":I
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->bgImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 872
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->bgImageView:Landroid/widget/ImageView;

    const v8, 0x7f020075

    invoke-static {v4, v8}, Lcom/att/mobile/android/infra/utils/PicassoUtils;->loadDefaultImage(Landroid/widget/ImageView;I)V

    .line 875
    .end local v0    # "color":I
    :cond_2
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    if-eqz v4, :cond_3

    .line 876
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    .line 877
    :cond_3
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->contactImageUri:Landroid/net/Uri;

    goto/16 :goto_0

    :cond_4
    move v4, v6

    .line 880
    goto/16 :goto_1

    :cond_5
    move v4, v6

    .line 882
    goto/16 :goto_2

    .line 889
    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_7

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f0801fb

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 890
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->setClickable(Z)V

    .line 891
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4, v3, v5}, Landroid/widget/QuickContactBadge;->assignContactFromPhone(Ljava/lang/String;Z)V

    .line 892
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f08009d

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 894
    :cond_7
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    invoke-virtual {v4, v6}, Landroid/widget/QuickContactBadge;->setClickable(Z)V

    .line 895
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->avatarImageView:Landroid/widget/QuickContactBadge;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    const v8, 0x7f0800c7

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 908
    .restart local v1    # "displayName":Ljava/lang/String;
    :cond_8
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->phoneTypeLayout:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_9
    move v6, v7

    .line 912
    goto/16 :goto_5
.end method

.method public updatePlayer()V
    .locals 4

    .prologue
    .line 1393
    const-string v0, "PlayerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updatePlayer message.getFileName()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " messageId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mespos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mespos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1394
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->registerAudioPlayerEventsListener(Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;)V

    .line 1395
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1396
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->updateMessage()V

    .line 1399
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/Message;->getFileName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1400
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->reset()V

    .line 1401
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initializePlayerUI(Z)V

    .line 1406
    :goto_0
    return-void

    .line 1403
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->message:Lcom/att/mobile/android/vvm/model/Message;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/Message;->getFileFullPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->initializeMedia(Ljava/lang/String;)V

    .line 1404
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initializePlayerUI(Z)V

    goto :goto_0
.end method

.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;
.super Landroid/os/AsyncTask;
.source "PlayerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactLoaderAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/att/mobile/android/vvm/model/ContactObject;",
        ">;"
    }
.end annotation


# instance fields
.field mMessage:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/att/mobile/android/vvm/model/Message;",
            ">;"
        }
    .end annotation
.end field

.field private mPhoneNumber:Ljava/lang/String;

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Ljava/lang/String;Lcom/att/mobile/android/vvm/model/Message;)V
    .locals 1
    .param p2, "phoneNumber"    # Ljava/lang/String;
    .param p3, "mes"    # Lcom/att/mobile/android/vvm/model/Message;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 305
    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    .line 306
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->mMessage:Ljava/lang/ref/WeakReference;

    .line 307
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/att/mobile/android/vvm/model/ContactObject;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContact(Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 298
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->doInBackground([Ljava/lang/Void;)Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/att/mobile/android/vvm/model/ContactObject;)V
    .locals 8
    .param p1, "contactObject"    # Lcom/att/mobile/android/vvm/model/ContactObject;

    .prologue
    const/4 v7, 0x0

    .line 317
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 319
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->mMessage:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/att/mobile/android/vvm/model/Message;

    .line 320
    .local v2, "msg":Lcom/att/mobile/android/vvm/model/Message;
    if-eqz v2, :cond_1

    .line 321
    if-eqz p1, :cond_4

    .line 323
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 324
    .local v1, "displayName":Ljava/lang/String;
    const-string v4, "PlayerFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "#### ContactLoaderAsync displayName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->mPhoneNumber:Ljava/lang/String;

    .end local v1    # "displayName":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2, v1}, Lcom/att/mobile/android/vvm/model/Message;->setSenderDisplayName(Ljava/lang/String;)V

    .line 326
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhoneType()I

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhoneLabel()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhoneLabel()Ljava/lang/String;

    move-result-object v3

    .line 327
    .local v3, "phoneTypeText":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/Message;->setPhoneNumberLabel(Ljava/lang/String;)V

    .line 328
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhotoUri()Landroid/net/Uri;

    move-result-object v0

    .line 329
    .local v0, "contactPhotoUri":Landroid/net/Uri;
    if-eqz v0, :cond_3

    .line 330
    invoke-virtual {v2, v0}, Lcom/att/mobile/android/vvm/model/Message;->setContactImageUri(Landroid/net/Uri;)V

    .line 334
    :goto_1
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getContactLookup()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 335
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getContactLookup()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/att/mobile/android/vvm/model/Message;->setContactLookupKey(Ljava/lang/String;)V

    .line 345
    .end local v0    # "contactPhotoUri":Landroid/net/Uri;
    .end local v3    # "phoneTypeText":Ljava/lang/String;
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$200(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v4

    const/16 v5, 0x4c

    invoke-virtual {v4, v5, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 346
    return-void

    .line 326
    :cond_2
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhoneType()I

    move-result v4

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getPhoneTypeText(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 332
    .restart local v0    # "contactPhotoUri":Landroid/net/Uri;
    .restart local v3    # "phoneTypeText":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2, v7}, Lcom/att/mobile/android/vvm/model/Message;->setContactImageUri(Landroid/net/Uri;)V

    goto :goto_1

    .line 338
    .end local v0    # "contactPhotoUri":Landroid/net/Uri;
    .end local v3    # "phoneTypeText":Ljava/lang/String;
    :cond_4
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v4

    :goto_3
    invoke-virtual {v2, v4}, Lcom/att/mobile/android/vvm/model/Message;->setSenderDisplayName(Ljava/lang/String;)V

    .line 339
    invoke-virtual {v2, v7}, Lcom/att/mobile/android/vvm/model/Message;->setContactLookupKey(Ljava/lang/String;)V

    .line 340
    invoke-virtual {v2, v7}, Lcom/att/mobile/android/vvm/model/Message;->setSenderBitmap(Landroid/graphics/Bitmap;)V

    .line 341
    invoke-virtual {v2, v7}, Lcom/att/mobile/android/vvm/model/Message;->setContactImageUri(Landroid/net/Uri;)V

    .line 342
    invoke-virtual {v2, v7}, Lcom/att/mobile/android/vvm/model/Message;->setPhoneNumberLabel(Ljava/lang/String;)V

    goto :goto_2

    .line 338
    :cond_5
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$100(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0801af

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 298
    check-cast p1, Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$ContactLoaderAsync;->onPostExecute(Lcom/att/mobile/android/vvm/model/ContactObject;)V

    return-void
.end method

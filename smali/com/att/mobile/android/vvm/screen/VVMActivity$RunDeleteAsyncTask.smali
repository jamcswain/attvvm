.class public Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;
.super Landroid/os/AsyncTask;
.source "VVMActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/VVMActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "RunDeleteAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field messageIDs:[Ljava/lang/Long;

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;[Ljava/lang/Long;)V
    .locals 1
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VVMActivity;
    .param p2, "messageIDs"    # [Ljava/lang/Long;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 90
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Long;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;->messageIDs:[Ljava/lang/Long;

    .line 91
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 96
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;->messageIDs:[Ljava/lang/Long;

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessagesAsDeleted([Ljava/lang/Long;)I

    .line 97
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageUIDsToDelete()[Ljava/lang/Long;

    move-result-object v0

    .line 98
    .local v0, "messagesUIDs":[Ljava/lang/Long;
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-virtual {v1, v2, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueDeleteOperation(Landroid/content/Context;[Ljava/lang/Long;)V

    .line 99
    const/4 v1, 0x0

    return-object v1
.end method

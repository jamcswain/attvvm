.class Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;
.super Ljava/lang/Object;
.source "ChangePasswordActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 161
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 155
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const v4, 0x7f020117

    const/4 v3, 0x0

    .line 135
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget v2, v2, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->minDigit:I

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->isPasswordValid(Landroid/widget/EditText;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 137
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->clearError(Ljava/lang/Boolean;)V

    .line 138
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$000(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->isConfirmPasswordValid(Landroid/widget/EditText;Landroid/widget/EditText;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->clearError(Ljava/lang/Boolean;)V

    .line 140
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->btnContinue:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 143
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$000(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    const v2, 0x7f080167

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setError(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->btnContinue:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

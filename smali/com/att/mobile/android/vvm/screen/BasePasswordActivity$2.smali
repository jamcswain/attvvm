.class Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;
.super Ljava/lang/Object;
.source "BasePasswordActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->initContinueButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 113
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    const v1, 0x7f080145

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    .line 121
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    iget v4, v4, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->minDigit:I

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->isPasswordValid(Landroid/widget/EditText;I)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->clearError(Ljava/lang/Boolean;)V

    .line 117
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    const v3, 0x7f0801f3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->showGauge(Ljava/lang/String;)V

    .line 118
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "pass":Ljava/lang/String;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    invoke-virtual {v1, v0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->changePassword(Ljava/lang/String;)V

    goto :goto_0
.end method

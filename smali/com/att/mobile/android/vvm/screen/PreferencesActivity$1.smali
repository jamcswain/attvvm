.class Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;
.super Ljava/lang/Object;
.source "PreferencesActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->onUpdateListener(ILjava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$eventId:I


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PreferencesActivity;ILandroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    iput p2, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->val$eventId:I

    iput-object p3, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const v4, 0x7f080145

    const/4 v3, 0x0

    .line 200
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->access$200(Lcom/att/mobile/android/vvm/screen/PreferencesActivity;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->val$eventId:I

    sparse-switch v1, :sswitch_data_0

    .line 257
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->dismissGauge()V

    goto :goto_0

    .line 207
    :sswitch_0
    const-string v1, "PreferencesActivity"

    const-string v2, "onUpdateListener() GET_METADATA_GREETING_FAILED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->dismissGauge()V

    goto :goto_0

    .line 213
    :sswitch_1
    const-string v1, "PreferencesActivity"

    const-string v2, "onUpdateListenerForMessages - [GET_METADATA_PASSWORD_FINISHED]"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 217
    :sswitch_2
    const-string v1, "PreferencesActivity"

    const-string v2, "onUpdateListener() GET_METADATA_GREETING_DETAILS_FINISHED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMetadata()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 220
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueGetExistingGreetingsOperation()V

    goto :goto_0

    .line 226
    :sswitch_3
    const-string v1, "PreferencesActivity"

    const-string v2, "onUpdateListener() GET_METADATA_EXISTING_GREETINGS_FINISHED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v2

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->val$context:Landroid/content/Context;

    check-cast v1, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-virtual {v2, v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 229
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->val$context:Landroid/content/Context;

    const-class v2, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 230
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-virtual {v1, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    .line 232
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->dismissGauge()V

    goto :goto_0

    .line 236
    .end local v0    # "i":Landroid/content/Intent;
    :sswitch_4
    const-string v1, "PreferencesActivity"

    const-string v2, "onUpdateListener() LOGIN_FAILED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->dismissGauge()V

    .line 240
    invoke-static {v4, v3}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0

    .line 246
    :sswitch_5
    const-string v1, "PreferencesActivity"

    const-string v2, "onUpdateListener() START_WELCOME_ACTIVITY"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    new-instance v3, Landroid/content/Intent;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->val$context:Landroid/content/Context;

    check-cast v1, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    const-class v4, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 253
    :sswitch_6
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->dismissGauge()V

    .line 254
    invoke-static {v4, v3}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto/16 :goto_0

    .line 204
    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_2
        0x19 -> :sswitch_3
        0x1a -> :sswitch_0
        0x1b -> :sswitch_1
        0x32 -> :sswitch_4
        0x40 -> :sswitch_5
        0x46 -> :sswitch_6
    .end sparse-switch
.end method

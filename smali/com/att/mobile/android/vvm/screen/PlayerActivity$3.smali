.class Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;
.super Ljava/lang/Object;
.source "PlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerActivity;->onUpdateListener(ILjava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

.field final synthetic val$eventId:I


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerActivity;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .prologue
    .line 472
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    iput p2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->val$eventId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 475
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->val$eventId:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 476
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->showSavedDialog(Landroid/app/Activity;Z)V

    .line 482
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$300(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$300(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 483
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$500(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$500(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$300(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$600(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 487
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$600(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->invalidate()V

    .line 490
    :cond_2
    return-void

    .line 477
    :cond_3
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;->val$eventId:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 478
    const v0, 0x7f080123

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0
.end method

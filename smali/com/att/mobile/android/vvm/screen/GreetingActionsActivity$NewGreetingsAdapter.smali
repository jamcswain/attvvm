.class Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "GreetingActionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NewGreetingsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;",
        ">;"
    }
.end annotation


# instance fields
.field private greetings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 161
    .local p2, "greetings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 162
    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->greetings:Ljava/util/ArrayList;

    .line 163
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->greetings:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 158
    check-cast p1, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;

    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->onBindViewHolder(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;I)V
    .locals 1
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;
    .param p2, "position"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->greetings:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    invoke-virtual {p1, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->bind(Lcom/att/mobile/android/vvm/model/greeting/Greeting;)V

    .line 178
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 167
    new-instance v0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04003d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;-><init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;Landroid/view/View;)V

    return-object v0
.end method

.method public updateItems(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "greetings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->greetings:Ljava/util/ArrayList;

    .line 172
    invoke-super {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 173
    return-void
.end method

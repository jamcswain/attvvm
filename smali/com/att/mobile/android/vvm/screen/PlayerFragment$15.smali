.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$15;
.super Ljava/lang/Object;
.source "PlayerFragment.java"

# interfaces
.implements Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;->showSavedDialog(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Landroid/app/Activity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 1907
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$15;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$15;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleNegativeButton(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1915
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$15;->val$activity:Landroid/app/Activity;

    const-class v2, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1916
    .local v0, "startActivityIntent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1917
    const-string v1, "go_to_saved"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1918
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$15;->val$activity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1919
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$15;->val$activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 1920
    return-void
.end method

.method public handlePositiveButton(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1911
    return-void
.end method

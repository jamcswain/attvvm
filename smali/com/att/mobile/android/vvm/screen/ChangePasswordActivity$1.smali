.class Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;
.super Ljava/lang/Object;
.source "ChangePasswordActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const v5, 0x7f020117

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 49
    if-nez p2, :cond_0

    .line 50
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget v2, v2, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->minDigit:I

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->isPasswordValid(Landroid/widget/EditText;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    const v2, 0x7f080162

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget v3, v3, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->minDigit:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setError(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 54
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->clearError(Ljava/lang/Boolean;)V

    .line 55
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$000(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->isConfirmPasswordValid(Landroid/widget/EditText;Landroid/widget/EditText;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->clearError(Ljava/lang/Boolean;)V

    .line 57
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->btnContinue:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

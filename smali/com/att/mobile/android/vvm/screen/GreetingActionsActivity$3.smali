.class Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;
.super Ljava/lang/Object;
.source "GreetingActionsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->onUpdateListener(ILjava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

.field final synthetic val$eventId:I


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    iput p2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->val$eventId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const v1, 0x7f080108

    const/4 v2, 0x1

    .line 568
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->val$eventId:I

    sparse-switch v0, :sswitch_data_0

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 574
    :sswitch_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$000(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 575
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    const-string v1, "/private/vendor/vendor.alu/messaging/GreetingType"

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$000(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getOriginalType()Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueSetMetaDataGreetingTypeOperation(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 577
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->dismissGauge()V

    .line 578
    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0

    .line 587
    :sswitch_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->dismissGauge()V

    .line 588
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$000(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 589
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$000(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getUniqueId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$300(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Ljava/lang/String;)V

    .line 590
    const v0, 0x7f080105

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0

    .line 595
    :cond_2
    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0

    .line 603
    :sswitch_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->dismissGauge()V

    .line 605
    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0

    .line 609
    :sswitch_3
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onUpdateListener() START_WELCOME_ACTIVITY"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    const-class v3, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 617
    :sswitch_4
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onUpdateListener() GET_METADATA_GREETING_DETAILS_FINISHED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMetadata()Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 619
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueGetExistingGreetingsOperation()V

    goto/16 :goto_0

    .line 624
    :sswitch_5
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onUpdateListener() GET_METADATA_EXISTING_GREETINGS_FINISHED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 626
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$400(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)V

    .line 628
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setGreetingRecyclerView()V

    .line 630
    :cond_3
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$100(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)V

    goto/16 :goto_0

    .line 634
    :sswitch_6
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onUpdateListener() GET_METADATA_GREETING_FAILED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$100(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)V

    .line 636
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->access$500(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)V

    goto/16 :goto_0

    .line 568
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_4
        0x19 -> :sswitch_5
        0x1a -> :sswitch_6
        0x2c -> :sswitch_0
        0x2d -> :sswitch_2
        0x2e -> :sswitch_1
        0x2f -> :sswitch_2
        0x40 -> :sswitch_3
    .end sparse-switch
.end method

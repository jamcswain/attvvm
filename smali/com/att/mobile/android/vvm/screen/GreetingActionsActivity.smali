.class public Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "GreetingActionsActivity.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/EventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;,
        Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GreetingActionsActivity"


# instance fields
.field private currentGreetingForUseExisting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

.field private greetingRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field private isLoginProcess:Z

.field private loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

.field private vf:Landroid/widget/ViewFlipper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->isLoginProcess:Z

    .line 279
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->currentGreetingForUseExisting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->currentGreetingForUseExisting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    return-object v0
.end method

.method static synthetic access$002(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Lcom/att/mobile/android/vvm/model/greeting/Greeting;)Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;
    .param p1, "x1"    # Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->currentGreetingForUseExisting:Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    return-object p1
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->stopGauge()V

    return-void
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct/range {p0 .. p6}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->startRecorderActivity(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setSelectedListViewItem(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setListView()V

    return-void
.end method

.method static synthetic access$500(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setErrorView()V

    return-void
.end method

.method private hasExistingRecording(Ljava/lang/String;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 410
    new-instance v3, Ljava/lang/StringBuffer;

    sget-object v4, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/VVMApplication;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 411
    .local v1, "pathPrefix":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ".amr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 413
    .local v2, "recordNameFilePath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 414
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 415
    const/4 v3, 0x1

    .line 417
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private setErrorView()V
    .locals 3

    .prologue
    .line 645
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->vf:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->vf:Landroid/widget/ViewFlipper;

    const v2, 0x7f0f00f7

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 646
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->toolBarTV:Landroid/widget/TextView;

    const v1, 0x7f080106

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 647
    const v0, 0x7f0f00f8

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 648
    const v0, 0x7f0f00f9

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 649
    const v0, 0x7f0f00fa

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 650
    return-void
.end method

.method private setFinishSetupButton()V
    .locals 2

    .prologue
    .line 93
    const v1, 0x7f0f00fc

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 94
    .local v0, "finishSetup":Landroid/widget/Button;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 95
    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 96
    new-instance v1, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    return-void
.end method

.method private setListView()V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->vf:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->vf:Landroid/widget/ViewFlipper;

    const v2, 0x7f0f00f5

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 108
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->toolBarTV:Landroid/widget/TextView;

    const v1, 0x7f080101

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 109
    return-void
.end method

.method private setSelectedListViewItem(Ljava/lang/String;)V
    .locals 4
    .param p1, "selectedUniqueId"    # Ljava/lang/String;

    .prologue
    .line 544
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v1

    .line 546
    .local v1, "greetingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    .line 547
    .local v0, "greeting":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getUniqueId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 548
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->setIsSelected(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 550
    :cond_0
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->setIsSelected(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 553
    .end local v0    # "greeting":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setListView()V

    .line 555
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->greetingRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 556
    return-void
.end method

.method private setSwirlView()V
    .locals 3

    .prologue
    .line 663
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->toolBarTV:Landroid/widget/TextView;

    const v1, 0x7f080101

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 664
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->vf:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->vf:Landroid/widget/ViewFlipper;

    const v2, 0x7f0f00fb

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 665
    const v0, 0x7f0f0080

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    .line 666
    const v0, 0x7f0f00ea

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 667
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->start()V

    .line 668
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->startGauge()V

    .line 669
    return-void
.end method

.method private startGauge()V
    .locals 2

    .prologue
    .line 657
    const-string v0, "GreetingActionsActivity"

    const-string v1, "startGauge"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->setVisibility(I)V

    .line 659
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->start()V

    .line 660
    return-void
.end method

.method private startRecorderActivity(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "maxAllowedRecordTime"    # I
    .param p3, "greetingUniqueId"    # Ljava/lang/String;
    .param p4, "imapSelectionGreetingType"    # Ljava/lang/String;
    .param p5, "imapRecordingGreetingType"    # Ljava/lang/String;
    .param p6, "greetingType"    # Ljava/lang/String;

    .prologue
    .line 531
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 532
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "SCREEN_TITLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 533
    const-string v1, "MAX_RECORDING_MILSEC_DURATION"

    mul-int/lit16 v2, p2, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 534
    const-string v1, "GREETING_UNIQUE_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 535
    const-string v1, "IMAP_SELECTION_GREETING_TYPE"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 536
    const-string v1, "IMAP_RECORDING_GREETING_TYPE"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 537
    const-string v1, "GREETING_TYPE"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 538
    const/16 v1, 0x21

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 539
    return-void
.end method

.method private stopGauge()V
    .locals 2

    .prologue
    .line 675
    const-string v0, "GreetingActionsActivity"

    const-string v1, "stopGauge"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    if-eqz v0, :cond_0

    .line 677
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->loadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->stop()V

    .line 679
    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 496
    invoke-super {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 498
    const/16 v1, 0x22

    if-ne p2, v1, :cond_3

    .line 499
    if-eqz p3, :cond_0

    .line 500
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 501
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 502
    const-string v1, "data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setSelectedListViewItem(Ljava/lang/String;)V

    .line 503
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->greetingRecyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->updateItems(Ljava/util/ArrayList;)V

    .line 506
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_0
    const v1, 0x7f080105

    invoke-static {v1, v3}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    .line 515
    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->isLoginProcess:Z

    if-eqz v1, :cond_2

    .line 516
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setResult(I)V

    .line 517
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->finish()V

    .line 519
    :cond_2
    return-void

    .line 508
    :cond_3
    const/16 v1, 0x23

    if-ne p2, v1, :cond_1

    .line 511
    const v1, 0x7f080108

    invoke-static {v1, v3}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 148
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onBackPressed()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->isLoginProcess:Z

    if-eqz v0, :cond_0

    .line 150
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setResult(I)V

    .line 151
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->finish()V

    .line 155
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    const-string v0, "GreetingActionsActivity"

    const-string v3, "onCreate()"

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f04003e

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setContentView(I)V

    .line 60
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    const/16 v3, 0xd

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->isLoginProcess:Z

    .line 61
    const v0, 0x7f080101

    iget-boolean v3, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->isLoginProcess:Z

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->initActionBar(IZ)V

    .line 63
    const v0, 0x7f0f00f4

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->vf:Landroid/widget/ViewFlipper;

    .line 64
    const v0, 0x7f0f00f6

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->greetingRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 66
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 67
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setListView()V

    .line 68
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setGreetingRecyclerView()V

    .line 80
    :goto_2
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->isLoginProcess:Z

    if-eqz v0, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setFinishSetupButton()V

    .line 84
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 60
    goto :goto_0

    :cond_2
    move v1, v2

    .line 61
    goto :goto_1

    .line 72
    :cond_3
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->isLoginProcess:Z

    if-eqz v0, :cond_4

    .line 73
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueGetGreetingsDetailsOperation()V

    .line 74
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setSwirlView()V

    goto :goto_2

    .line 76
    :cond_4
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->finish()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 127
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMetadata()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 132
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onDestroy() - greetings metadata cleared from model"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 135
    return-void
.end method

.method public onNetworkFailure()V
    .locals 0

    .prologue
    .line 654
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 139
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onPause()V

    .line 142
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/VVMApplication;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setVisible(Z)V

    .line 143
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 144
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 114
    const-string v0, "GreetingActionsActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 117
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->mustBackToSetupProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/VVMApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setVisible(Z)V

    .line 122
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    goto :goto_0
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 3
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 561
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const-string v0, "GreetingActionsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GreetingActionsActivity.onUpdateListener() eventId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    new-instance v0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;I)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 642
    return-void
.end method

.method public setGreetingRecyclerView()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->greetingRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 89
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->greetingRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;-><init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 90
    return-void
.end method

.method public wireNeededListener(Landroid/view/View;)V
    .locals 12
    .param p1, "listViewItem"    # Landroid/view/View;

    .prologue
    .line 292
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    .line 293
    .local v10, "greeting":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getOriginalType()Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->toString()Ljava/lang/String;

    move-result-object v11

    .line 296
    .local v11, "originalType":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isChangeable()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 297
    const-string v0, "GreetingActionsActivity"

    const-string v3, "CHANGEABLE greeting selected."

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->hasExistingRecording(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 301
    const-string v0, "GreetingActionsActivity"

    const-string v3, "Greeting contains stream, going to show a menu"

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Custom"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v1, 0x7f0800cc

    .line 303
    .local v1, "titleId":I
    :goto_0
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Custom"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v2, 0x7f0800cd

    .line 304
    .local v2, "textId":I
    :goto_1
    const v3, 0x7f08006f

    const v4, 0x7f080063

    const/4 v5, 0x1

    new-instance v6, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;

    invoke-direct {v6, p0, v10, v11}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Lcom/att/mobile/android/vvm/model/greeting/Greeting;Ljava/lang/String;)V

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 406
    .end local v1    # "titleId":I
    .end local v2    # "textId":I
    :goto_2
    return-void

    .line 302
    :cond_0
    const v1, 0x7f080139

    goto :goto_0

    .line 303
    .restart local v1    # "titleId":I
    :cond_1
    const v2, 0x7f08013a

    goto :goto_1

    .line 366
    .end local v1    # "titleId":I
    :cond_2
    const-string v0, "GreetingActionsActivity"

    const-string v3, "No stream found for greeting, going to start recorder"

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    .line 368
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getMaxAllowedRecordTime()I

    move-result v5

    .line 369
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getUniqueId()Ljava/lang/String;

    move-result-object v6

    .line 370
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getImapSelectionVariable()Ljava/lang/String;

    move-result-object v7

    .line 371
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getImapRecordingVariable()Ljava/lang/String;

    move-result-object v8

    .line 372
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getOriginalType()Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v3, p0

    .line 367
    invoke-direct/range {v3 .. v9}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->startRecorderActivity(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 377
    :cond_3
    const-string v0, "GreetingActionsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NON CHANGEABLE greeting selected. Going to set greeting ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") as selected."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 382
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getIsSelected()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    .line 384
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 385
    const-string v0, "GreetingActionsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Going to set greeting ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") as selected."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    const-string v3, "/private/vendor/vendor.alu/messaging/GreetingType"

    invoke-virtual {v0, v3, v11}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueSetMetaDataOperation(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getUniqueId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->setSelectedListViewItem(Ljava/lang/String;)V

    .line 392
    const-string v0, "GreetingActionsActivity"

    const-string v3, "Operation \'SetMetaDataOperation\' enqueued."

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const-string v0, "GreetingActionsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Operation is /private/vendor/vendor.alu/messaging/GreetingType Value is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 395
    :cond_4
    const v0, 0x7f080145

    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto/16 :goto_2

    .line 398
    :cond_5
    const-string v0, "GreetingActionsActivity"

    const-string v3, "No need to set as selected, its already is."

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 401
    :cond_6
    const-string v0, "GreetingActionsActivity"

    const-string v3, "wireNeededListener() Failed to determine original greeting type, cannot enqueue operation."

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

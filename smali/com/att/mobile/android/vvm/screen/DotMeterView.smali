.class public Lcom/att/mobile/android/vvm/screen/DotMeterView;
.super Landroid/view/View;
.source "DotMeterView.java"


# static fields
.field private static final COLOR_CHG_STEP:I

.field private static DOT_DIAMETER:I = 0x0

.field private static EQUALIZER_HEIGHT_DOTS:I = 0x0

.field private static EQUALIZER_MIN_WIDTH_DOTS:I = 0x0

.field private static final FRAME_RATE:I = 0x14

.field private static final INACTIVE_EQUALIZER_DOT_COLOR:I = -0x333334

.field private static final INACTIVE_EQUALIZER_DOT_COLOR_ON:I = -0x777778

.field private static final REFRESH_TICK_KILL:I = 0x2

.field private static final REFRESH_TICK_OFF:I = 0x0

.field private static final REFRESH_TICK_ON:I = 0x1

.field private static final SIGNAL_HALF_WIDTH_DOTS:I = 0x3


# instance fields
.field private TAG:Ljava/lang/String;

.field private bottomRowCenterY:I

.field private canvas:Landroid/graphics/Canvas;

.field private dotRadius:I

.field private equalizerState:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private equalizerWidthDots:I

.field private isColored:Z

.field private isEqualizerActive:Z

.field private paint:Landroid/graphics/Paint;

.field private random:Ljava/util/Random;

.field private refreshHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    const/16 v0, 0xc

    sput v0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_MIN_WIDTH_DOTS:I

    .line 22
    const/16 v0, 0x8

    sput v0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_HEIGHT_DOTS:I

    .line 30
    const/16 v0, 0xff

    sget v1, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_HEIGHT_DOTS:I

    add-int/lit8 v1, v1, -0x1

    div-int/2addr v0, v1

    sput v0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->COLOR_CHG_STEP:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 19
    const-string v0, "DotMeterView"

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->TAG:Ljava/lang/String;

    .line 27
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    .line 28
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    .line 29
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->bottomRowCenterY:I

    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    .line 38
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isEqualizerActive:Z

    .line 39
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isColored:Z

    .line 52
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->random:Ljava/util/Random;

    .line 261
    new-instance v0, Lcom/att/mobile/android/vvm/screen/DotMeterView$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView$1;-><init>(Lcom/att/mobile/android/vvm/screen/DotMeterView;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->refreshHandler:Landroid/os/Handler;

    .line 58
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->initDrawing()V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrSet"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const-string v0, "DotMeterView"

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->TAG:Ljava/lang/String;

    .line 27
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    .line 28
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    .line 29
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->bottomRowCenterY:I

    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    .line 38
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isEqualizerActive:Z

    .line 39
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isColored:Z

    .line 52
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->random:Ljava/util/Random;

    .line 261
    new-instance v0, Lcom/att/mobile/android/vvm/screen/DotMeterView$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView$1;-><init>(Lcom/att/mobile/android/vvm/screen/DotMeterView;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->refreshHandler:Landroid/os/Handler;

    .line 63
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->initDrawing()V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrSet"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const-string v0, "DotMeterView"

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->TAG:Ljava/lang/String;

    .line 27
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    .line 28
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    .line 29
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->bottomRowCenterY:I

    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    .line 38
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isEqualizerActive:Z

    .line 39
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isColored:Z

    .line 52
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->random:Ljava/util/Random;

    .line 261
    new-instance v0, Lcom/att/mobile/android/vvm/screen/DotMeterView$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView$1;-><init>(Lcom/att/mobile/android/vvm/screen/DotMeterView;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->refreshHandler:Landroid/os/Handler;

    .line 68
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->initDrawing()V

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/DotMeterView;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/DotMeterView;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->decreaseEqualizerValues()V

    return-void
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/DotMeterView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/DotMeterView;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->refreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private clamp(III)I
    .locals 0
    .param p1, "val"    # I
    .param p2, "low"    # I
    .param p3, "high"    # I

    .prologue
    .line 177
    if-ge p1, p2, :cond_0

    .line 182
    .end local p2    # "low":I
    :goto_0
    return p2

    .line 179
    .restart local p2    # "low":I
    :cond_0
    if-le p1, p3, :cond_1

    move p2, p3

    .line 180
    goto :goto_0

    :cond_1
    move p2, p1

    .line 182
    goto :goto_0
.end method

.method private decreaseEqualizerValues()V
    .locals 4

    .prologue
    .line 250
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 251
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    if-ge v0, v2, :cond_1

    .line 252
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 253
    .local v1, "value":I
    if-lez v1, :cond_0

    .line 254
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    add-int/lit8 v3, v1, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 251
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    .end local v0    # "i":I
    .end local v1    # "value":I
    :cond_1
    return-void
.end method

.method private drawDot(IIII)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "radius"    # I
    .param p4, "color"    # I

    .prologue
    .line 93
    if-gez p1, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The given x coordinate of the circle\'s center is negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    if-gez p2, :cond_1

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The given y coordinate of the circle\'s center is negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_1
    if-gez p3, :cond_2

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The given circle\'s radius is negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->canvas:Landroid/graphics/Canvas;

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 103
    return-void
.end method

.method private drawEmptyEqualizer()V
    .locals 4

    .prologue
    .line 153
    const/4 v0, 0x0

    .local v0, "rowNum":I
    :goto_0
    sget v1, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_HEIGHT_DOTS:I

    if-ge v0, v1, :cond_0

    .line 154
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->bottomRowCenterY:I

    sget v2, Lcom/att/mobile/android/vvm/screen/DotMeterView;->DOT_DIAMETER:I

    mul-int/2addr v2, v0

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    const/4 v3, -0x1

    invoke-direct {p0, v1, v2, v3}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->drawEqualizerRow(III)V

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_0
    return-void
.end method

.method private drawEqualizer()V
    .locals 3

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->drawEmptyEqualizer()V

    .line 166
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 167
    const/4 v0, 0x0

    .local v0, "column":I
    :goto_0
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    if-ge v0, v2, :cond_1

    .line 168
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 169
    .local v1, "curEqualizerVal":I
    if-eqz v1, :cond_0

    .line 170
    invoke-direct {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->setColumnColors(II)V

    .line 167
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    .end local v0    # "column":I
    .end local v1    # "curEqualizerVal":I
    :cond_1
    return-void
.end method

.method private drawEqualizerRow(III)V
    .locals 4
    .param p1, "rowCenterY"    # I
    .param p2, "dotRadius"    # I
    .param p3, "color"    # I

    .prologue
    .line 113
    if-gez p1, :cond_0

    .line 114
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "The given y coordinate of a equalizer\'s row center is negative"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 116
    :cond_0
    if-gez p2, :cond_1

    .line 117
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "The given circle\'s radius is negative"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 119
    :cond_1
    move v1, p2

    .line 120
    .local v1, "rowX":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    if-ge v0, v2, :cond_2

    .line 121
    invoke-direct {p0, v1, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->drawDot(IIII)V

    .line 122
    sget v2, Lcom/att/mobile/android/vvm/screen/DotMeterView;->DOT_DIAMETER:I

    add-int/2addr v1, v2

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_2
    return-void
.end method

.method private initDrawing()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->paint:Landroid/graphics/Paint;

    .line 76
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 77
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->paint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 79
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 82
    return-void
.end method

.method private initEqualizerSizes()V
    .locals 4

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->getWidth()I

    move-result v1

    .line 133
    .local v1, "viewWidth":I
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->getHeight()I

    move-result v0

    .line 135
    .local v0, "viewHeight":I
    sget v2, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_MIN_WIDTH_DOTS:I

    iput v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    .line 136
    sget v2, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_MIN_WIDTH_DOTS:I

    div-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    .line 137
    :goto_0
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    mul-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x2

    if-ne v2, v1, :cond_0

    sget v2, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_HEIGHT_DOTS:I

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    mul-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x2

    if-le v2, v0, :cond_1

    .line 138
    :cond_0
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    .line 139
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    div-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    goto :goto_0

    .line 142
    :cond_1
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    mul-int/lit8 v2, v2, 0x2

    sput v2, Lcom/att/mobile/android/vvm/screen/DotMeterView;->DOT_DIAMETER:I

    .line 144
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    sub-int v2, v0, v2

    iput v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->bottomRowCenterY:I

    .line 147
    return-void
.end method

.method private setColumnColors(II)V
    .locals 6
    .param p1, "column"    # I
    .param p2, "dotsNumForColoring"    # I

    .prologue
    .line 327
    iget v3, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    if-gt p1, v3, :cond_0

    if-gez p1, :cond_1

    .line 328
    :cond_0
    new-instance v3, Ljava/security/InvalidParameterException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The number of column "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is invalid"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 329
    :cond_1
    sget v3, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_HEIGHT_DOTS:I

    if-gt p2, v3, :cond_2

    if-gez p2, :cond_3

    .line 330
    :cond_2
    new-instance v3, Ljava/security/InvalidParameterException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The number of dots in column for coloring "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is invalid"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 332
    :cond_3
    if-nez p1, :cond_6

    iget v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    .line 333
    .local v0, "columnCenterX":I
    :goto_0
    const/4 v2, 0x0

    .local v2, "rowNum":I
    :goto_1
    if-ge v2, p2, :cond_7

    .line 336
    const v1, -0x333334

    .line 337
    .local v1, "curColor":I
    iget-boolean v3, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isEqualizerActive:Z

    if-eqz v3, :cond_4

    .line 338
    const v1, -0x777778

    .line 340
    :cond_4
    iget-boolean v3, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isColored:Z

    if-eqz v3, :cond_5

    .line 341
    const/16 v3, 0xff

    sget v4, Lcom/att/mobile/android/vvm/screen/DotMeterView;->COLOR_CHG_STEP:I

    mul-int/2addr v4, v2

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    .line 344
    :cond_5
    iget v3, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->bottomRowCenterY:I

    sget v4, Lcom/att/mobile/android/vvm/screen/DotMeterView;->DOT_DIAMETER:I

    mul-int/2addr v4, v2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    invoke-direct {p0, v0, v3, v4, v1}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->drawDot(IIII)V

    .line 333
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 332
    .end local v0    # "columnCenterX":I
    .end local v1    # "curColor":I
    .end local v2    # "rowNum":I
    :cond_6
    sget v3, Lcom/att/mobile/android/vvm/screen/DotMeterView;->DOT_DIAMETER:I

    mul-int/2addr v3, p1

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->dotRadius:I

    sub-int v0, v3, v4

    goto :goto_0

    .line 346
    .restart local v0    # "columnCenterX":I
    .restart local v2    # "rowNum":I
    :cond_7
    return-void
.end method


# virtual methods
.method public activate(Z)V
    .locals 1
    .param p1, "isColored"    # Z

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isEqualizerActive:Z

    if-nez v0, :cond_0

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isEqualizerActive:Z

    .line 192
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->startRefreshingLoop()V

    .line 194
    :cond_0
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isColored:Z

    .line 195
    return-void
.end method

.method public cancelDots(I)V
    .locals 0
    .param p1, "cancelDotsNum"    # I

    .prologue
    .line 383
    return-void
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isEqualizerActive:Z

    if-eqz v0, :cond_0

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->isEqualizerActive:Z

    .line 204
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->invalidate()V

    .line 205
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->stopRefreshingLoop()V

    .line 207
    :cond_0
    return-void
.end method

.method public initEqualizerValues()V
    .locals 3

    .prologue
    .line 311
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 313
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    if-ge v0, v1, :cond_1

    .line 314
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 313
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    :cond_1
    return-void
.end method

.method public killRefreshingLoop()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->refreshHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 300
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->killRefreshingLoop()V

    .line 304
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 350
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 351
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->canvas:Landroid/graphics/Canvas;

    .line 354
    :try_start_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->initEqualizerSizes()V

    .line 356
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    if-eq v1, v2, :cond_1

    .line 357
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->initEqualizerValues()V

    .line 359
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->drawEqualizer()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :goto_0
    return-void

    .line 361
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setEqualizerValues([I)V
    .locals 2
    .param p1, "currEqualizerState"    # [I

    .prologue
    .line 241
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 242
    aget v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->setEqualizerValues(I)[I

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_0
    return-void
.end method

.method public setEqualizerValues(I)[I
    .locals 10
    .param p1, "currentEqualizerValue"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x0

    .line 210
    if-gez p1, :cond_0

    .line 211
    new-instance v5, Ljava/security/InvalidParameterException;

    const-string v6, "The given current meter value is negative"

    invoke-direct {v5, v6}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 213
    :cond_0
    if-nez p1, :cond_2

    .line 237
    :cond_1
    return-object v4

    .line 216
    :cond_2
    iget v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerWidthDots:I

    add-int/lit8 v2, v5, -0x6

    .line 218
    .local v2, "maxSignalCenterCol":I
    if-ltz v2, :cond_1

    .line 221
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    new-array v4, v5, [I

    .line 224
    .local v4, "values":[I
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->random:Ljava/util/Random;

    invoke-virtual {v5, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    add-int/lit8 v0, v5, 0x3

    .line 225
    .local v0, "col":I
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    const/4 v6, 0x1

    sget v7, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_HEIGHT_DOTS:I

    invoke-direct {p0, p1, v6, v7}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->clamp(III)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    invoke-virtual {v5, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v4, v0

    .line 228
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 229
    .local v3, "r":Ljava/util/Random;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    const/4 v5, 0x3

    if-gt v1, v5, :cond_1

    .line 230
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    sub-int v6, v0, v1

    rsub-int/lit8 v7, v1, 0x3

    sub-int v7, p1, v7

    .line 231
    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    sget v8, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_HEIGHT_DOTS:I

    .line 230
    invoke-direct {p0, v7, v9, v8}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->clamp(III)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    sub-int v6, v0, v1

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v4, v0

    .line 233
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    add-int v6, v0, v1

    add-int/lit8 v7, v1, -0x1

    sub-int v7, p1, v7

    .line 234
    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    sget v8, Lcom/att/mobile/android/vvm/screen/DotMeterView;->EQUALIZER_HEIGHT_DOTS:I

    .line 233
    invoke-direct {p0, v7, v9, v8}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->clamp(III)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 235
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->equalizerState:Ljava/util/LinkedList;

    add-int v6, v0, v1

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v4, v0

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public startRefreshingLoop()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->refreshHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 288
    return-void
.end method

.method public stopRefreshingLoop()V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DotMeterView;->refreshHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 294
    return-void
.end method

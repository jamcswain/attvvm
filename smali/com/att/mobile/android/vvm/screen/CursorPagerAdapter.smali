.class public Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "CursorPagerAdapter.java"


# instance fields
.field private cursor:Landroid/database/Cursor;

.field private isAutoPlay:Z


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;Z)V
    .locals 0
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "isAutoPlay"    # Z

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 31
    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    .line 32
    iput-boolean p3, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->isAutoPlay:Z

    .line 33
    return-void
.end method

.method private swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 90
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    if-ne p1, v1, :cond_1

    .line 91
    const/4 v0, 0x0

    .line 99
    :cond_0
    :goto_0
    return-object v0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    .line 94
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    .line 95
    if-eqz p1, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 74
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 75
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 77
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 62
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getCurrentMessage()Lcom/att/mobile/android/vvm/model/Message;
    .locals 2

    .prologue
    .line 106
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createMessageFromCursor(Landroid/database/Cursor;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v0

    return-object v0
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->getItem(I)Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 38
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    if-nez v3, :cond_0

    .line 39
    const/4 v1, 0x0

    .line 49
    :goto_0
    return-object v1

    .line 41
    :cond_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    invoke-interface {v3, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 43
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createMessageFromCursor(Landroid/database/Cursor;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v2

    .line 45
    .local v2, "mes":Lcom/att/mobile/android/vvm/model/Message;
    :try_start_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->cursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    iget-boolean v4, p0, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->isAutoPlay:Z

    invoke-static {v2, p1, v3, v4}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->init(Lcom/att/mobile/android/vvm/model/Message;IIZ)Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .local v1, "frag":Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    goto :goto_0

    .line 46
    .end local v1    # "frag":Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    :catch_0
    move-exception v0

    .line 47
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 53
    const/4 v0, -0x2

    return v0
.end method

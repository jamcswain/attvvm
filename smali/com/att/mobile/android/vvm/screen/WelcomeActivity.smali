.class public Lcom/att/mobile/android/vvm/screen/WelcomeActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "WelcomeActivity.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;


# static fields
.field private static final REQUEST_CODE_ENTER_GREETINGS:I = 0x3

.field private static final REQUEST_CODE_ENTER_PASSWORD:I = 0x1

.field private static final REQUEST_CODE_OPEN_SETTINGS:I = 0x2

.field private static final REQUEST_CODE_RESET_PASSWORD:I = 0x4

.field private static final TAG:Ljava/lang/String; = "WelcomeActivity"

.field private static stateString:Ljava/lang/String;


# instance fields
.field private btnBeginSetup:Landroid/widget/Button;

.field private btnCallVoicemail:Landroid/widget/Button;

.field private btnCancel:Landroid/widget/Button;

.field private btnQuit:Landroid/widget/Button;

.field private btnTryAgain:Landroid/widget/Button;

.field private loadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

.field private mActivity:Landroid/app/Activity;

.field private mailboxNumber:Ljava/lang/String;

.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private final phoneStateListener:Landroid/telephony/PhoneStateListener;

.field private final rotateAnimation:Landroid/view/animation/RotateAnimation;

.field private setupController:Lcom/att/mobile/android/vvm/control/SetupController;

.field private telephonyManager:Landroid/telephony/TelephonyManager;

.field private txtSetupRetryHeader:Landroid/widget/TextView;

.field private txtSetupRetrySubHeader:Landroid/widget/TextView;

.field private txtSetupRetry_Sub_1:Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

.field private txtSetupRetry_Sub_2:Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-string v0, "N/A"

    sput-object v0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->stateString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->mailboxNumber:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnBeginSetup:Landroid/widget/Button;

    .line 59
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCancel:Landroid/widget/Button;

    .line 60
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnTryAgain:Landroid/widget/Button;

    .line 61
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    .line 62
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    .line 71
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->loadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    .line 78
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 177
    new-instance v0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->stateString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 50
    sput-object p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->stateString:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showSetupScreen()V

    return-void
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/control/SetupController;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setupController:Lcom/att/mobile/android/vvm/control/SetupController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->resolveLaunchAttm()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;
    .param p1, "x1"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->proceedToNextSetupState(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->mailboxNumber:Ljava/lang/String;

    return-object v0
.end method

.method public static backToPrevState()V
    .locals 4

    .prologue
    .line 675
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    .line 676
    .local v0, "state":I
    const-string v1, "WelcomeActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backToPrevState state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 679
    :cond_0
    const-string v1, "WelcomeActivity"

    const-string v2, "backToPrevState SET SHOW_CALL_VOICE_MAIL state."

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 682
    :cond_1
    return-void
.end method

.method private createAnimation()V
    .locals 4

    .prologue
    .line 794
    const-string v0, "WelcomeActivity"

    const-string v1, "createAnimation"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 796
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 797
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 798
    return-void
.end method

.method private getCTN()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->simManager:Lcom/att/mobile/android/infra/sim/SimManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimManager;->startListening()V

    .line 104
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->simManager:Lcom/att/mobile/android/infra/sim/SimManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimManager;->startSimValidation()V

    .line 106
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->getCTNFromSim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->mailboxNumber:Ljava/lang/String;

    .line 107
    const-string v0, "WelcomeActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCTN mailboxNumber = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->mailboxNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setupController:Lcom/att/mobile/android/vvm/control/SetupController;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->mailboxNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/SetupController;->setMailboxNumber(Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method private initLinkView(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "tvTC"    # Landroid/widget/TextView;

    .prologue
    .line 748
    if-eqz p1, :cond_0

    .line 749
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 751
    :cond_0
    return-void
.end method

.method private initTextViews()V
    .locals 1

    .prologue
    .line 616
    const v0, 0x7f0f007c

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetryHeader:Landroid/widget/TextView;

    .line 617
    const v0, 0x7f0f007d

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetrySubHeader:Landroid/widget/TextView;

    .line 618
    const v0, 0x7f0f007e

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetry_Sub_1:Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

    .line 619
    const v0, 0x7f0f007f

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetry_Sub_2:Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

    .line 620
    return-void
.end method

.method private initTitleFont()V
    .locals 3

    .prologue
    .line 444
    const v2, 0x7f0f0095

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 445
    .local v1, "visualTv":Landroid/widget/TextView;
    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 447
    const v2, 0x7f0f0094

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 448
    .local v0, "attTv":Landroid/widget/TextView;
    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_RegularItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 449
    return-void
.end method

.method private initWaitBinarySms(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const v2, 0x1020002

    .line 631
    const-string v0, "WelcomeActivity"

    const-string v1, "initWaitBinarySms"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setupController:Lcom/att/mobile/android/vvm/control/SetupController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/SetupController;->setTimerWorking(Z)V

    .line 634
    const v0, 0x7f04001d

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setContentView(I)V

    .line 635
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080095

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 636
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    .line 637
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 639
    :cond_0
    const v0, 0x7f0f0080

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->loadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    .line 641
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startGauge()V

    .line 643
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setWaitBinarySmsFontAndButtons(I)V

    .line 644
    const v0, 0x7f080031

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initActionBar(IZ)V

    .line 646
    return-void
.end method

.method private initWelcomeScreenButton()V
    .locals 2

    .prologue
    .line 725
    const v0, 0x7f0f017a

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnBeginSetup:Landroid/widget/Button;

    .line 726
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnBeginSetup:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 727
    return-void
.end method

.method private initWelcomeScreenTCTextFont()V
    .locals 5

    .prologue
    .line 731
    const v4, 0x7f0f0176

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 732
    .local v1, "byClickingTv":Landroid/widget/TextView;
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 734
    const v4, 0x7f0f0177

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 735
    .local v3, "termsOfServiceTv":Landroid/widget/TextView;
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 736
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 739
    const v4, 0x7f0f0178

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 740
    .local v0, "andTv":Landroid/widget/TextView;
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 742
    const v4, 0x7f0f0179

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 743
    .local v2, "privacyPolicyTv":Landroid/widget/TextView;
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 744
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 745
    return-void
.end method

.method private initWelcomeScreenUIElements()V
    .locals 2

    .prologue
    .line 710
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initWelcomeScreenButton()V

    .line 711
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initTitleFont()V

    .line 712
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initWelcomeScreenTCTextFont()V

    .line 713
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->rescaleTextSizeOnHugeFont()V

    .line 715
    const v1, 0x7f0f0175

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 716
    .local v0, "scroll":Landroid/widget/ScrollView;
    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$13;

    invoke-direct {v1, p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$13;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;Landroid/widget/ScrollView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 722
    return-void
.end method

.method private initWithMsisdnScreen()V
    .locals 2

    .prologue
    .line 686
    const-string v0, "WelcomeActivity"

    const-string v1, "initWithMsisdnScreen"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    const v0, 0x7f040073

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setContentView(I)V

    .line 689
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initWelcomeScreenUIElements()V

    .line 690
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnBeginSetup:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 706
    return-void
.end method

.method public static isTenMinutesPassed()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    .line 813
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v4

    const-string v5, "mosmsTimePref"

    const-class v6, Ljava/lang/Long;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 814
    .local v0, "sentTime":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-nez v4, :cond_0

    .line 815
    const-string v4, "WelcomeActivity"

    const-string v5, "isTenMinutesPassed return false."

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    :goto_0
    return v1

    .line 818
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 819
    .local v2, "timeDiff":J
    const-string v4, "WelcomeActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isTenMinutesPassed timeDiff="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    const-wide/32 v4, 0x927c0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    const/4 v1, 0x1

    .line 821
    .local v1, "tenMinutes":Z
    :cond_1
    const-string v4, "WelcomeActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isTenMinutesPassed return="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private launchATTMActivity()Z
    .locals 8

    .prologue
    .line 118
    const/4 v2, 0x0

    .line 121
    .local v2, "result":Z
    :try_start_0
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v5, "showLaunchAttmScreenOnlyOnce"

    const-class v6, Ljava/lang/Boolean;

    const/4 v7, 0x0

    .line 122
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 121
    invoke-virtual {v4, v5, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 123
    .local v0, "alreayShowedLaunchATTMScreen":Z
    if-eqz v0, :cond_0

    .line 124
    invoke-static {}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->launchATTMApplication()Z

    move-result v2

    .line 125
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->finish()V

    .line 140
    .end local v0    # "alreayShowedLaunchATTMScreen":Z
    :goto_0
    return v2

    .line 127
    .restart local v0    # "alreayShowedLaunchATTMScreen":Z
    :cond_0
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 128
    .local v3, "startActivityIntent":Landroid/content/Intent;
    const/high16 v4, 0x4000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 131
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    const/4 v2, 0x1

    goto :goto_0

    .line 136
    .end local v0    # "alreayShowedLaunchATTMScreen":Z
    .end local v3    # "startActivityIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 137
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "WelcomeActivity"

    const-string v5, "launchAttm() - error starting AT&T Messages"

    invoke-static {v4, v5, v1}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 138
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private proceedToNextSetupState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 665
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 666
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 671
    :cond_0
    :goto_0
    return-void

    .line 667
    :cond_1
    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 669
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_0
.end method

.method private rescaleTextSizeOnHugeFont()V
    .locals 6

    .prologue
    const v5, 0x7f090005

    const/4 v4, 0x0

    .line 754
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    const v3, 0x3fa66666    # 1.3f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 755
    const v2, 0x7f0f0094

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 756
    .local v0, "tv1":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 757
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 759
    :cond_0
    const v2, 0x7f0f0095

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 760
    .local v1, "tv2":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 761
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 764
    .end local v0    # "tv1":Landroid/widget/TextView;
    .end local v1    # "tv2":Landroid/widget/TextView;
    :cond_1
    return-void
.end method

.method private resolveLaunchAttm()Z
    .locals 4

    .prologue
    .line 480
    const-string v2, "WelcomeActivity"

    const-string v3, "resolveLaunchAttm()"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    const/4 v1, 0x0

    .line 484
    .local v1, "result":Z
    invoke-static {}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->isUibReadyToReplaceLegacyVvm()Z

    .line 485
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v0

    .line 489
    .local v0, "attmStatus":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 491
    const-string v2, "WelcomeActivity"

    const-string v3, "resolveLaunchAttm() ATTM status = UNKNOWN"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startGauge()V

    .line 493
    const/4 v1, 0x1

    .line 511
    :cond_0
    :goto_0
    return v1

    .line 494
    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 495
    const-string v2, "WelcomeActivity"

    const-string v3, "resolveLaunchAttm() ATTM status = PROVISIONED, going to launch ATTM"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->stopGauge()V

    .line 498
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->launchATTMActivity()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 499
    const-string v2, "WelcomeActivity"

    const-string v3, "resolveLaunchAttm() launched ATTM, going to kill VVM"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    const/4 v1, 0x1

    goto :goto_0

    .line 503
    :cond_2
    if-nez v0, :cond_4

    .line 504
    const-string v2, "WelcomeActivity"

    const-string v3, "resolveLaunchAttm() ATTM status = NOT_INSTALLED, VVM will ran as usual"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->stopGauge()V

    goto :goto_0

    .line 505
    :cond_4
    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 507
    const-string v2, "WelcomeActivity"

    const-string v3, "resolveLaunchAttm() ATTM status = INSTALLED_NOT_PROVISIONED, VVM will ran as usual"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setAccountErrorXmlFonts()V
    .locals 2

    .prologue
    .line 623
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetryHeader:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 624
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetrySubHeader:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 625
    const v0, 0x7f0f0171

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 626
    const v0, 0x7f0f0172

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 627
    return-void
.end method

.method private setCallVoicemailFontsAndButtons()V
    .locals 3

    .prologue
    .line 360
    const v0, 0x7f0f0171

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    .line 361
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 362
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    const v0, 0x7f0f0172

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    .line 372
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    const v1, 0x7f080062

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 373
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$3;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 382
    const v0, 0x7f0f00b2

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 383
    const v0, 0x7f0f00b3

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 384
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 385
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 386
    return-void
.end method

.method private setContectDescription()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x0

    const v2, 0x7f0f004d

    .line 401
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 402
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 403
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 404
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 406
    :cond_0
    return-void
.end method

.method private setNoMsisdnFontAndButtons()V
    .locals 2

    .prologue
    .line 409
    const v0, 0x7f0f0171

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    .line 410
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 411
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$4;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 419
    const v0, 0x7f0f0172

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    .line 420
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    const v1, 0x7f080062

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 421
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$5;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$5;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 430
    const v0, 0x7f0f0126

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 431
    const v0, 0x7f0f0163

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 432
    const v0, 0x7f0f0164

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 433
    const v0, 0x7f0f0165

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 434
    const v0, 0x7f0f0166

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 435
    const v0, 0x7f0f0167

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 436
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCallVoicemail:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 437
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 438
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initTitleFont()V

    .line 439
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->rescaleTextSizeOnHugeFont()V

    .line 440
    return-void
.end method

.method private setQuitSetupScreenTextAndButtons()V
    .locals 3

    .prologue
    .line 595
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetryHeader:Landroid/widget/TextView;

    const v2, 0x7f08002a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 596
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetrySubHeader:Landroid/widget/TextView;

    const v2, 0x7f08002d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 597
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetry_Sub_1:Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

    const v2, 0x7f08002b

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->setText(I)V

    .line 598
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetry_Sub_2:Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

    const v2, 0x7f08002c

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->setText(I)V

    .line 600
    const v1, 0x7f0f0172

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 601
    .local v0, "bottomButton":Landroid/widget/Button;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 603
    const v1, 0x7f0f0171

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    .line 604
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    const v2, 0x7f080062

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 605
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    new-instance v2, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$10;

    invoke-direct {v2, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$10;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 612
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setAccountErrorXmlFonts()V

    .line 613
    return-void
.end method

.method private setTryAgainScreenTextAndButtons()V
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetryHeader:Landroid/widget/TextView;

    const v1, 0x7f0801cd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 551
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetrySubHeader:Landroid/widget/TextView;

    const v1, 0x7f080030

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 552
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetry_Sub_1:Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->setText(I)V

    .line 553
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->txtSetupRetry_Sub_2:Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;

    const v1, 0x7f08002f

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->setText(I)V

    .line 555
    const v0, 0x7f0f0171

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnTryAgain:Landroid/widget/Button;

    .line 556
    const v0, 0x7f0f0172

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    .line 558
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnTryAgain:Landroid/widget/Button;

    const v1, 0x7f08006d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 559
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnTryAgain:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$8;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$8;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 567
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    const v1, 0x7f080062

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 568
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnQuit:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$9;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$9;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 576
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setAccountErrorXmlFonts()V

    .line 577
    return-void
.end method

.method private setWaitBinarySmsFontAndButtons(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 649
    const v0, 0x7f0f0082

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCancel:Landroid/widget/Button;

    .line 650
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCancel:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 651
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->btnCancel:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$11;

    invoke-direct {v1, p0, p1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$11;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 661
    const v0, 0x7f0f0081

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 662
    return-void
.end method

.method private showCallVoiceMailScreen()V
    .locals 3

    .prologue
    const v2, 0x1020002

    .line 348
    const-string v0, "WelcomeActivity"

    const-string v1, "showCallVoiceMailScreen"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    const v0, 0x7f040027

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setContentView(I)V

    .line 351
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080094

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 352
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    .line 353
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 355
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setCallVoicemailFontsAndButtons()V

    .line 356
    const v0, 0x7f080031

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initActionBar(IZ)V

    .line 357
    return-void
.end method

.method private showNoMsisdnErrorScreen()V
    .locals 2

    .prologue
    .line 390
    const-string v0, "WelcomeActivity"

    const-string v1, "showCallVoiceMailScreen"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    const v0, 0x7f04006a

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setContentView(I)V

    .line 393
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080094

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 394
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setContectDescription()V

    .line 395
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setNoMsisdnFontAndButtons()V

    .line 396
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->rescaleTextSizeOnHugeFont()V

    .line 397
    return-void
.end method

.method private showNoVoiceMailDialog()V
    .locals 7

    .prologue
    .line 515
    const v1, 0x7f0800a7

    const v2, 0x7f080130

    const v3, 0x7f0801c7

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$7;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$7;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 532
    return-void
.end method

.method private showQuitSetupScreen()V
    .locals 3

    .prologue
    const v2, 0x1020002

    .line 581
    const-string v0, "WelcomeActivity"

    const-string v1, "showQuitSetupScreen"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    const v0, 0x7f04001c

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setContentView(I)V

    .line 583
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initTextViews()V

    .line 584
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080048

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 586
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    .line 587
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 590
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setQuitSetupScreenTextAndButtons()V

    .line 591
    const v0, 0x7f080031

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initActionBar(IZ)V

    .line 592
    return-void
.end method

.method private showSetupScreen()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    .line 273
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    .line 274
    .local v0, "currentState":I
    const-string v3, "WelcomeActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "showSetupScreen currentState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/Constants;->getSetupStatusString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    const/16 v3, 0xd

    if-eq v0, v3, :cond_0

    invoke-static {}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->isTenMinutesPassed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 277
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v4, "mosmsTimePref"

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 278
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 279
    const/4 v0, -0x1

    .line 282
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 342
    :pswitch_0
    const-string v3, "WelcomeActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "#### NO SCREEN FOR STATE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :goto_0
    return-void

    .line 286
    :pswitch_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initWithMsisdnScreen()V

    goto :goto_0

    .line 290
    :pswitch_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showQuitSetupScreen()V

    goto :goto_0

    .line 296
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initWaitBinarySms(I)V

    goto :goto_0

    .line 300
    :pswitch_4
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showTryAgainScreen()V

    goto :goto_0

    .line 304
    :pswitch_5
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3, v8}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 308
    :pswitch_6
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 311
    :pswitch_7
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3, v8}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 316
    :pswitch_8
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showCallVoiceMailScreen()V

    goto :goto_0

    .line 319
    :pswitch_9
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showNoMsisdnErrorScreen()V

    goto :goto_0

    .line 323
    :pswitch_a
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showCallVoiceMailScreen()V

    .line 324
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showNoVoiceMailDialog()V

    goto :goto_0

    .line 328
    :pswitch_b
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setupController:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/control/SetupController;->close()V

    .line 329
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 330
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isNeedRefreshInbox()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 331
    .local v2, "needRefresh":Z
    const-string v3, "refresh_inbox"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 333
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 334
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->finish()V

    goto :goto_0

    .line 338
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "needRefresh":Z
    :pswitch_c
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 282
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_a
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_7
        :pswitch_9
        :pswitch_0
        :pswitch_2
        :pswitch_8
    .end packed-switch
.end method

.method private showTryAgainScreen()V
    .locals 3

    .prologue
    const v2, 0x1020002

    .line 536
    const-string v0, "WelcomeActivity"

    const-string v1, "showTryAgainScreen"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    const v0, 0x7f04001c

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setContentView(I)V

    .line 538
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initTextViews()V

    .line 539
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080098

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 540
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_0

    .line 541
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 544
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setTryAgainScreenTextAndButtons()V

    .line 545
    const v0, 0x7f080031

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initActionBar(IZ)V

    .line 546
    return-void
.end method

.method private startCallStateListener()V
    .locals 3

    .prologue
    .line 211
    const-string v1, "WelcomeActivity"

    const-string v2, "startCallStateListener => start listening to PhoneState"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 213
    const/16 v0, 0x20

    .line 214
    .local v0, "events":I
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v0}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 215
    return-void
.end method

.method private startGauge()V
    .locals 2

    .prologue
    .line 783
    const-string v0, "WelcomeActivity"

    const-string v1, "startGauge"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->loadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->setVisibility(I)V

    .line 786
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->createAnimation()V

    .line 787
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->loadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->start()V

    .line 788
    return-void
.end method

.method private stopGauge()V
    .locals 2

    .prologue
    .line 804
    const-string v0, "WelcomeActivity"

    const-string v1, "stopGauge"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->loadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    if-eqz v0, :cond_0

    .line 807
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->loadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->stop()V

    .line 808
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->loadingGauge:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->setVisibility(I)V

    .line 810
    :cond_0
    return-void
.end method

.method private stopListening()V
    .locals 3

    .prologue
    .line 221
    const-string v0, "WelcomeActivity"

    const-string v1, "AccountSetupActivity::startCallStateListener => stop listening to PhoneState"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 223
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 224
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v4, -0x1

    .line 229
    const-string v1, "WelcomeActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult requestCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resultCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    sparse-switch p2, :sswitch_data_0

    .line 266
    invoke-super {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 233
    :sswitch_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getCTN()V

    .line 234
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    .line 235
    .local v0, "currentSetupState":I
    if-ne v0, v4, :cond_1

    .line 236
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showSetupScreen()V

    goto :goto_0

    .line 238
    :cond_1
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->proceedToNextSetupState(I)V

    .line 239
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setupController:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/SetupController;->handleSetup()V

    goto :goto_0

    .line 244
    .end local v0    # "currentSetupState":I
    :sswitch_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSetupCompleted()V

    goto :goto_0

    .line 249
    :sswitch_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 250
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSetupCompleted()V

    goto :goto_0

    .line 252
    :cond_2
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 253
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_0

    .line 262
    :sswitch_3
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->finish()V

    goto :goto_0

    .line 231
    :sswitch_data_0
    .sparse-switch
        0x26 -> :sswitch_2
        0x35 -> :sswitch_2
        0x38 -> :sswitch_3
        0x3a -> :sswitch_3
        0x3d -> :sswitch_1
        0x43 -> :sswitch_3
        0x49 -> :sswitch_3
        0x4a -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    const-string v0, "WelcomeActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 88
    iput-object p0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->mActivity:Landroid/app/Activity;

    .line 89
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->handler:Landroid/os/Handler;

    .line 90
    invoke-static {p0}, Lcom/att/mobile/android/vvm/control/SetupController;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/vvm/control/SetupController;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setupController:Lcom/att/mobile/android/vvm/control/SetupController;

    .line 92
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 93
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 95
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->areRequiredPermissionsGranted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->getCTN()V

    .line 99
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->stopListening()V

    .line 171
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 172
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setupController:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/SetupController;->unregisterCallback()V

    .line 163
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onPause()V

    .line 164
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 147
    const-string v0, "WelcomeActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 149
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    invoke-static {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->isAdminUser(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->resolveLaunchAttm()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->setupController:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/SetupController;->registerCallback(Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;)V

    .line 154
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showSetupScreen()V

    goto :goto_0
.end method

.method public onSetupStateChange()V
    .locals 2

    .prologue
    .line 769
    const-string v0, "WelcomeActivity"

    const-string v1, "onSetupStateChange"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    new-instance v0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$14;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$14;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 776
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 2
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 454
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    packed-switch p1, :pswitch_data_0

    .line 472
    :goto_0
    return-void

    .line 457
    :pswitch_0
    const-string v0, "WelcomeActivity"

    const-string v1, "onUpdateListener() ATTM_SERVICE_CONNECTED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$6;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$6;-><init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_0
    .end packed-switch
.end method

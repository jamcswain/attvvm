.class public Lcom/att/mobile/android/vvm/screen/AggregatedActivity;
.super Lcom/att/mobile/android/vvm/screen/VmListActivity;
.source "AggregatedActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private filterType:I

.field private listFragment:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

.field private mCurrentPhoneNumber:Ljava/lang/String;

.field private mCurrentPhotoUri:Landroid/net/Uri;

.field private mDisplayName:Ljava/lang/String;

.field mUserImage:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AggregatedActivity;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhotoUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AggregatedActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->setColorsAndAvatarImage(Landroid/widget/ImageView;Ljava/lang/String;)V

    return-void
.end method

.method private changeToolbarColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 170
    const v1, 0x7f0f010c

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 171
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setBackgroundColor(I)V

    .line 172
    return-void
.end method

.method private refreshUIWithHandler()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 258
    return-void
.end method

.method private setColorsAndAvatarImage(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 2
    .param p1, "userImage"    # Landroid/widget/ImageView;
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {p2}, Lcom/att/mobile/android/infra/utils/Utils;->getDefaultAvatarBackground(Ljava/lang/String;)I

    move-result v0

    .line 163
    .local v0, "color":I
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 164
    const v1, 0x7f020079

    invoke-static {p1, v1}, Lcom/att/mobile/android/infra/utils/PicassoUtils;->loadDefaultImage(Landroid/widget/ImageView;I)V

    .line 165
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->setStatusBarColor(I)V

    .line 166
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->changeToolbarColor(I)V

    .line 167
    return-void
.end method

.method private setStatusBarColor(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 102
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 104
    .local v0, "window":Landroid/view/Window;
    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 105
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 106
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 108
    .end local v0    # "window":Landroid/view/Window;
    :cond_0
    return-void
.end method


# virtual methods
.method protected closeEditMode()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->listFragment:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->closeEditMode()V

    .line 290
    return-void
.end method

.method public getCurrentFilterType()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->filterType:I

    return v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method protected goToGotItScreen()V
    .locals 2

    .prologue
    .line 270
    sget-object v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    const-string v1, "goToGotItScreen"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->listFragment:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 272
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->finish()V

    .line 274
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 203
    sget-object v1, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult requestCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resultCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/16 v1, 0xa

    if-ne p1, v1, :cond_0

    .line 206
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isGroupByContact()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 207
    .local v0, "groupedByContact":Z
    sget-object v1, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult requestCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resultCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    if-nez v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->finish()V

    .line 214
    .end local v0    # "groupedByContact":Z
    :goto_0
    return-void

    .line 213
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 114
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 116
    const v1, 0x7f040020

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->setContentView(I)V

    .line 118
    const v1, 0x7f0f008c

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mUserImage:Landroid/widget/ImageView;

    .line 120
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intent.data.user.phone"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    .line 121
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intent.data.user.photo.uri"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhotoUri:Landroid/net/Uri;

    .line 122
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intent.data.user.name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "displayName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intent.data.filter.type"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->filterType:I

    .line 125
    sget-object v1, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate mCurrentPhoneNumber="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mCurrentPhotoUri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhotoUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " displayName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " filterType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->filterType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    .end local v0    # "displayName":Ljava/lang/String;
    :cond_0
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mDisplayName:Ljava/lang/String;

    .line 129
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mDisplayName:Ljava/lang/String;

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-virtual {p0, v1, v5, v5, v2}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->initInboxActionBar(Ljava/lang/String;ZZLcom/att/mobile/android/infra/utils/FontUtils$FontNames;)V

    .line 130
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->toolBarTV:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900a2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 133
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->filterType:I

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    invoke-static {v1, v2, v5}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->newInstance(ILjava/lang/String;Z)Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->listFragment:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    .line 134
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->listFragment:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->setActionListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;)V

    .line 135
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->listFragment:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->setListListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;)V

    .line 137
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0f008d

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->listFragment:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 139
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->updateUserPicBackground()V

    .line 140
    return-void
.end method

.method public onListUpdated(II)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "size"    # I

    .prologue
    const/4 v1, 0x0

    .line 279
    if-nez p2, :cond_1

    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->filterType:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    .line 281
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v2, "doNotShowSavedDialogAgain"

    const-class v3, Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->finish()V

    .line 284
    :cond_1
    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->setToolBarPlayAllBtn(Z)V

    .line 285
    return-void

    :cond_2
    move v0, v1

    .line 284
    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 146
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->handler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->createInstance(Landroid/os/Handler;)V

    .line 147
    invoke-static {}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->getInstance()Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    move-result-object v0

    invoke-virtual {v0, p0, p0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->addEventListener(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 148
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 177
    sget-object v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume isCreate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->isCreate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onResume()V

    .line 180
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mustBackToSetupProcess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->isCreate:Z

    if-nez v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->updateUserPicBackground()V

    .line 186
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->refreshUi()V

    goto :goto_0
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 3
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    sget-object v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateListener() eventId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    sparse-switch p1, :sswitch_data_0

    .line 239
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    .line 243
    :goto_0
    return-void

    .line 224
    :sswitch_0
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$1;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;I)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 235
    :sswitch_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->refreshUIWithHandler()V

    goto :goto_0

    .line 220
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method protected refreshContacts()V
    .locals 4

    .prologue
    .line 248
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->toolBarTV:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mUserImage:Landroid/widget/ImageView;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;-><init>(Lcom/att/mobile/android/vvm/screen/AggregatedActivity;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity$ContactLoaderAsync;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 249
    return-void
.end method

.method protected refreshUi()V
    .locals 2

    .prologue
    .line 263
    sget-object v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    const-string v1, "refreshUi"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->listFragment:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->refreshList(Z)V

    .line 265
    return-void
.end method

.method protected updateUserPicBackground()V
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->TAG:Ljava/lang/String;

    const-string v1, "updateUserPicBackground"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhotoUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 154
    invoke-static {p0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhotoUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Landroid/net/Uri;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mUserImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 159
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mUserImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;->setColorsAndAvatarImage(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$MyOnItemSelectedListener;
.super Ljava/lang/Object;
.source "DebugSettingsScreen.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyOnItemSelectedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$MyOnItemSelectedListener;->this$0:Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "host":Ljava/lang/String;
    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$MyOnItemSelectedListener;->this$0:Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->access$100(Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :goto_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$MyOnItemSelectedListener;->this$0:Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->access$100(Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->refreshDrawableState()V

    .line 123
    const-string v1, "DebugSettingsScreen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Host set to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    return-void

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$MyOnItemSelectedListener;->this$0:Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->access$100(Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

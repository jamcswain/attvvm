.class Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;
.super Ljava/lang/Object;
.source "AudioRecorderActivity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->initSeekBars()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 634
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "seekBarProgress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 646
    if-eqz p3, :cond_1

    .line 650
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$1000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$1000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->seekTo(I)V

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$1000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$1000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    .line 662
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$200(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Z)V

    .line 672
    :cond_1
    :goto_0
    return-void

    .line 666
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$1100(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 667
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$1100(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    move-result-object v0

    div-int/lit16 v1, p2, 0xfa

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$1200(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 680
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 688
    return-void
.end method

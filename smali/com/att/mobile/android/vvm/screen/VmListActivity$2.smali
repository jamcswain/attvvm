.class Lcom/att/mobile/android/vvm/screen/VmListActivity$2;
.super Ljava/lang/Object;
.source "VmListActivity.java"

# interfaces
.implements Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/VmListActivity;->showDeleteDialog([Ljava/lang/Long;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

.field final synthetic val$ids:[Ljava/lang/Long;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/Long;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VmListActivity;

    .prologue
    .line 327
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;->val$ids:[Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleNegativeButton(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 339
    return-void
.end method

.method public handlePositiveButton(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 330
    const-string v0, "VmListActivity"

    const-string v1, "onDeleteAction"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;->val$ids:[Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;->val$ids:[Ljava/lang/Long;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;->val$ids:[Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->deleteVMs([Ljava/lang/Long;)V

    .line 333
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->closeEditMode()V

    .line 335
    :cond_0
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;
.super Ljava/lang/Object;
.source "PlayerFragment.java"

# interfaces
.implements Landroid/support/v7/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;->setUpPopupMenu(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 1415
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 11
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1418
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v5

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v3

    .line 1419
    .local v3, "phoneNumber":Ljava/lang/String;
    :goto_0
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .line 1421
    .local v0, "activity":Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 1485
    :cond_0
    :goto_1
    return v8

    .line 1418
    .end local v0    # "activity":Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    .end local v3    # "phoneNumber":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1424
    .restart local v0    # "activity":Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    .restart local v3    # "phoneNumber":Ljava/lang/String;
    :sswitch_0
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->onBackPressed()V

    .line 1425
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1500(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    goto :goto_1

    .line 1429
    :sswitch_1
    if-eqz v0, :cond_0

    .line 1430
    new-array v5, v8, [Ljava/lang/Long;

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v6

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v9

    const/4 v6, 0x4

    invoke-virtual {v0, v5, v6}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->markMessagesAs([Ljava/lang/Long;I)V

    goto :goto_1

    .line 1434
    :sswitch_2
    if-eqz v0, :cond_0

    .line 1435
    new-array v5, v8, [Ljava/lang/Long;

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v6}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v6

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v0, v5, v10}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->markMessagesAs([Ljava/lang/Long;I)V

    goto :goto_1

    .line 1440
    :sswitch_3
    if-eqz v3, :cond_0

    .line 1442
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.INSERT_OR_EDIT"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1443
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "vnd.android.cursor.item/contact"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1444
    const-string v5, "phone"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1445
    const-string v5, "phone_type"

    const/4 v6, 0x2

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1446
    const/high16 v5, 0x10000000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1447
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v5, v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1448
    .end local v2    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 1449
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "PlayerFragment"

    const-string v6, "Failed to invoke contacts activity"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1455
    .end local v1    # "e":Ljava/lang/Exception;
    :sswitch_4
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string v6, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v5, v6}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_2

    .line 1456
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1600(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Z

    goto/16 :goto_1

    .line 1458
    :cond_2
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    new-array v6, v8, [Ljava/lang/String;

    const-string v7, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v7, v6, v9

    const/16 v7, 0xb

    invoke-virtual {v5, v6, v7}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->requestPermissions([Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1463
    :sswitch_5
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string v6, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v5, v6}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    .line 1464
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1700(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    goto/16 :goto_1

    .line 1466
    :cond_3
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    new-array v6, v8, [Ljava/lang/String;

    const-string v7, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v7, v6, v9

    const/16 v7, 0xc

    invoke-virtual {v5, v6, v7}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->requestPermissions([Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1470
    :sswitch_6
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1800(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    goto/16 :goto_1

    .line 1474
    :sswitch_7
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v5

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Message;->getTranscription()Ljava/lang/String;

    move-result-object v4

    .line 1475
    .local v4, "text":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 1476
    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$14;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v5, v4}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1900(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1421
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0189 -> :sswitch_5
        0x7f0f018a -> :sswitch_2
        0x7f0f018b -> :sswitch_1
        0x7f0f018c -> :sswitch_4
        0x7f0f018d -> :sswitch_7
        0x7f0f018e -> :sswitch_6
        0x7f0f018f -> :sswitch_3
    .end sparse-switch
.end method

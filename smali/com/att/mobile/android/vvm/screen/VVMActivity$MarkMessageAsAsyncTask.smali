.class public Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;
.super Landroid/os/AsyncTask;
.source "VVMActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/VVMActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MarkMessageAsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field mMessageIds:[Ljava/lang/Long;

.field mSavedState:I

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;[Ljava/lang/Long;I)V
    .locals 1
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VVMActivity;
    .param p2, "messageIds"    # [Ljava/lang/Long;
    .param p3, "savedState"    # I

    .prologue
    .line 110
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 111
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Long;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->mMessageIds:[Ljava/lang/Long;

    .line 112
    iput p3, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->mSavedState:I

    .line 113
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 118
    const-string v0, "VVMActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MarkMessageAsSaveAsyncTask.doInBackground() start messageId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->mMessageIds:[Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->access$000(Lcom/att/mobile/android/vvm/screen/VVMActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->mMessageIds:[Ljava/lang/Long;

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->mSavedState:I

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessagesSavedState([Ljava/lang/Long;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

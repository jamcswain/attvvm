.class Lcom/att/mobile/android/vvm/screen/VVMActivity$5;
.super Ljava/lang/Object;
.source "VVMActivity.java"

# interfaces
.implements Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/VVMActivity;->showSimSwappedDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VVMActivity;

    .prologue
    .line 636
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleNegativeButton(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 655
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->finish()V

    .line 656
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->exit(Landroid/app/Activity;)V

    .line 658
    return-void
.end method

.method public handlePositiveButton(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 641
    :try_start_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/VVMActivity;->simManager:Lcom/att/mobile/android/infra/sim/SimManager;

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/sim/SimManager;->clearUserDataOnSimSwap()V

    .line 642
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    const-class v3, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 643
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 644
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-virtual {v2, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->startActivity(Landroid/content/Intent;)V

    .line 645
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 650
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 646
    :catch_0
    move-exception v0

    .line 647
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "VVMActivity"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$IntentExtras;
.super Ljava/lang/Object;
.source "AudioRecorderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntentExtras"
.end annotation


# static fields
.field public static final GREETING_TYPE:Ljava/lang/String; = "GREETING_TYPE"

.field public static final GREETING_UNIQUE_ID:Ljava/lang/String; = "GREETING_UNIQUE_ID"

.field public static final IMAP_RECORDING_GREETING_TYPE:Ljava/lang/String; = "IMAP_RECORDING_GREETING_TYPE"

.field public static final IMAP_SELECTION_GREETING_TYPE:Ljava/lang/String; = "IMAP_SELECTION_GREETING_TYPE"

.field public static final MAX_RECORDING_MILSEC_DURATION:Ljava/lang/String; = "MAX_RECORDING_MILSEC_DURATION"

.field public static final NEXT_INTENT:Ljava/lang/String; = "NEXT_INTENT"

.field public static final SCREEN_TITLE:Ljava/lang/String; = "SCREEN_TITLE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

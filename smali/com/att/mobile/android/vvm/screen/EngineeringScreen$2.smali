.class Lcom/att/mobile/android/vvm/screen/EngineeringScreen$2;
.super Ljava/lang/Object;
.source "EngineeringScreen.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$2;->this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$2;->this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    const-string v2, "MinConfidence"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->access$100(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$2;->this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mMinConfidenceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 94
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
.super Ljava/util/TimerTask;
.source "AudioRecorderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AudioEqualizerUpdater"
.end annotation


# static fields
.field private static final UPDATE_INTERVAL:I = 0xfa

.field private static final VOLUME_MULTIPLIER:F = 2.0f


# instance fields
.field private audioEqualizerUIComponentUpdater:Ljava/lang/Runnable;

.field private audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

.field private audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

.field private audioRecorderActivity:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

.field private isUpdateForDummyRecording:Z

.field private meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

.field private playbackEqualizerValues:[[I

.field private playbackEqualizerValuesPosition:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 290
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 211
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioRecorderActivity:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .line 215
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    .line 218
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    .line 221
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 224
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->isUpdateForDummyRecording:Z

    .line 227
    check-cast v0, [[I

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValues:[[I

    .line 230
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValuesPosition:I

    .line 235
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioEqualizerUIComponentUpdater:Ljava/lang/Runnable;

    .line 291
    return-void
.end method

.method static synthetic access$1200(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;I)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
    .param p1, "x1"    # I

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->updatePlaybackEqualizerValuesPosition(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/DotMeterView;Lcom/att/mobile/android/infra/media/AudioRecorder;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "x1"    # Lcom/att/mobile/android/vvm/screen/DotMeterView;
    .param p2, "x2"    # Lcom/att/mobile/android/infra/media/AudioRecorder;

    .prologue
    .line 203
    invoke-static {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->createUpdaterForRealAudioRecorder(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/DotMeterView;Lcom/att/mobile/android/infra/media/AudioRecorder;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/DotMeterView;Lcom/att/mobile/android/infra/media/AudioPlayer;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "x1"    # Lcom/att/mobile/android/vvm/screen/DotMeterView;
    .param p2, "x2"    # Lcom/att/mobile/android/infra/media/AudioPlayer;

    .prologue
    .line 203
    invoke-static {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->createUpdaterForAudioPlayer(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/DotMeterView;Lcom/att/mobile/android/infra/media/AudioPlayer;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Lcom/att/mobile/android/infra/media/AudioPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    return-object v0
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .prologue
    .line 203
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValuesPosition:I

    return v0
.end method

.method static synthetic access$404(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .prologue
    .line 203
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValuesPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValuesPosition:I

    return v0
.end method

.method static synthetic access$500(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)[[I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValues:[[I

    return-object v0
.end method

.method static synthetic access$600(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Lcom/att/mobile/android/vvm/screen/DotMeterView;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Lcom/att/mobile/android/infra/media/AudioRecorder;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    return-object v0
.end method

.method static synthetic access$800(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->isUpdateForDummyRecording:Z

    return v0
.end method

.method private static createUpdaterForAudioPlayer(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/DotMeterView;Lcom/att/mobile/android/infra/media/AudioPlayer;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
    .locals 7
    .param p0, "audioRecorderActivity"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "meter"    # Lcom/att/mobile/android/vvm/screen/DotMeterView;
    .param p2, "audioPlayer"    # Lcom/att/mobile/android/infra/media/AudioPlayer;

    .prologue
    .line 366
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;-><init>()V

    .line 369
    .local v0, "audioEqualizerUpdater":Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
    iput-object p0, v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioRecorderActivity:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .line 373
    iput-object p1, v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    .line 374
    iput-object p2, v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 378
    const/4 v2, 0x0

    .line 379
    .local v2, "position":I
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$900()Ljava/util/LinkedList;

    move-result-object v5

    .line 380
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    new-array v5, v5, [[I

    iput-object v5, v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValues:[[I

    .line 381
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$900()Ljava/util/LinkedList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    .line 382
    .local v4, "recordingEqualizerValue":[I
    iget-object v6, v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValues:[[I

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "position":I
    .local v3, "position":I
    aput-object v4, v6, v2

    move v2, v3

    .line 383
    .end local v3    # "position":I
    .restart local v2    # "position":I
    goto :goto_0

    .line 389
    .end local v4    # "recordingEqualizerValue":[I
    :cond_0
    invoke-virtual {p2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getMediaDuration()I

    move-result v1

    .line 390
    .local v1, "mediaDuration":I
    div-int/lit16 v5, v1, 0xfa

    .line 394
    invoke-virtual {p2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getCurrentPosition()I

    move-result v6

    sub-int v6, v1, v6

    div-int/lit16 v6, v6, 0xfa

    sub-int/2addr v5, v6

    .line 391
    invoke-direct {v0, v5}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->updatePlaybackEqualizerValuesPosition(I)V

    .line 397
    return-object v0
.end method

.method private static createUpdaterForRealAudioRecorder(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/DotMeterView;Lcom/att/mobile/android/infra/media/AudioRecorder;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
    .locals 2
    .param p0, "audioRecorderActivity"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "meter"    # Lcom/att/mobile/android/vvm/screen/DotMeterView;
    .param p2, "audioRecorder"    # Lcom/att/mobile/android/infra/media/AudioRecorder;

    .prologue
    .line 338
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;-><init>()V

    .line 341
    .local v0, "audioEqualizerUpdater":Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
    iput-object p0, v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioRecorderActivity:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .line 345
    iput-object p1, v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    .line 346
    iput-object p2, v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    .line 349
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$902(Ljava/util/LinkedList;)Ljava/util/LinkedList;

    .line 352
    return-object v0
.end method

.method private declared-synchronized updatePlaybackEqualizerValuesPosition(I)V
    .locals 3
    .param p1, "playbackEqualizerValuesPosition"    # I

    .prologue
    .line 417
    monitor-enter p0

    :try_start_0
    const-string v0, "AudioEqualizerUpdater.updatePlaybackEqualizerValuesPosition()"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new equalizer values position is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iput p1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->playbackEqualizerValuesPosition:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    monitor-exit p0

    return-void

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioRecorderActivity:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->audioEqualizerUIComponentUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 407
    return-void
.end method

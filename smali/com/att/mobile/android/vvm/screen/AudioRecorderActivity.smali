.class public abstract Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "AudioRecorderActivity.java"

# interfaces
.implements Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;
.implements Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;,
        Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;,
        Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;,
        Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$PlayButtonStates;,
        Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$RecordButtonStates;,
        Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$IntentExtras;
    }
.end annotation


# static fields
.field private static final REQUEST_RECORD_AUDIO_PERMISSION:I = 0xa

.field private static final TAG:Ljava/lang/String; = "AudioRecorderActivity"

.field private static recordingEqualizerValues:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[I>;"
        }
    .end annotation
.end field


# instance fields
.field private audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

.field private audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

.field private audioPlayerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

.field private audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

.field private audioRecorderRecordingObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

.field private currentRecordingProgress:I

.field private dummyRecordingAudioFilePath:Ljava/lang/String;

.field protected imapRecordingGreetingType:Ljava/lang/String;

.field protected maximumRecordDuration:I

.field private meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

.field protected playButton:Landroid/widget/ImageButton;

.field protected recordButton:Landroid/widget/ImageButton;

.field protected recordCustomFilePath:Ljava/lang/String;

.field protected recordNameFilePath:Ljava/lang/String;

.field private recordingPlaybackProgressTextView:Landroid/widget/TextView;

.field private recordingPlaybackSeekBar:Landroid/widget/SeekBar;

.field private recordingProgressBar:Landroid/widget/SeekBar;

.field private recordingProgressTextView:Landroid/widget/TextView;

.field protected recordingTotalTextView:Landroid/widget/TextView;

.field protected screenTitle:Landroid/widget/TextView;

.field protected sendButton:Landroid/widget/Button;

.field private timer:Ljava/util/Timer;

.field private vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 453
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingEqualizerValues:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    .line 427
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->screenTitle:Landroid/widget/TextView;

    .line 435
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingProgressBar:Landroid/widget/SeekBar;

    .line 438
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingProgressTextView:Landroid/widget/TextView;

    .line 439
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingTotalTextView:Landroid/widget/TextView;

    .line 442
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackSeekBar:Landroid/widget/SeekBar;

    .line 445
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackProgressTextView:Landroid/widget/TextView;

    .line 448
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    .line 449
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    .line 450
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->sendButton:Landroid/widget/Button;

    .line 459
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->dummyRecordingAudioFilePath:Ljava/lang/String;

    .line 465
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    .line 481
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->timer:Ljava/util/Timer;

    .line 484
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .line 487
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorderRecordingObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

    .line 490
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 493
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

    .line 496
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->currentRecordingProgress:I

    return v0
.end method

.method static synthetic access$002(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->currentRecordingProgress:I

    return p1
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updateRecordingProgressUIComponents()V

    return-void
.end method

.method static synthetic access$1000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)Lcom/att/mobile/android/infra/media/AudioPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->startUpdatingAudioEqualzierForRealRecording()V

    return-void
.end method

.method static synthetic access$1500(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->startUpdatingAudioEqualzierPlayback()V

    return-void
.end method

.method static synthetic access$1700(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->stopUpdatingAudioEqualizer()V

    return-void
.end method

.method static synthetic access$1800(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchRecordButtonState(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchPlayButtonState(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updatePlaybackProgressUIComponents(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updateRecordingProgressBarProgressUIComponent()V

    return-void
.end method

.method static synthetic access$2200(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchProgressAndSeekBars(Z)V

    return-void
.end method

.method static synthetic access$2300(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updatePlaybackProgressBarProgressUIComponent()V

    return-void
.end method

.method static synthetic access$2400(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updatePlaybackProgressTextUIComponent(Z)V

    return-void
.end method

.method static synthetic access$900()Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingEqualizerValues:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$902(Ljava/util/LinkedList;)Ljava/util/LinkedList;
    .locals 0
    .param p0, "x0"    # Ljava/util/LinkedList;

    .prologue
    .line 42
    sput-object p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingEqualizerValues:Ljava/util/LinkedList;

    return-object p0
.end method

.method private adjustRecordingPlaybackSecondsProgress(II)I
    .locals 2
    .param p1, "recordTime"    # I
    .param p2, "recordingPlaybackSecondsProgress"    # I

    .prologue
    .line 1648
    rem-int/lit16 v0, p1, 0x3e8

    if-lez v0, :cond_0

    .line 1652
    add-int/lit8 v0, p2, 0x1

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->maximumRecordDuration:I

    div-int/lit16 v1, v1, 0x3e8

    if-ge v0, v1, :cond_0

    .line 1653
    add-int/lit8 p2, p2, 0x1

    .line 1656
    :cond_0
    return p2
.end method

.method private enableRecordButton(Z)V
    .locals 2
    .param p1, "shouldEnable"    # Z

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1090
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const v0, 0x7f02013f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1091
    return-void

    .line 1090
    :cond_0
    const v0, 0x7f020141

    goto :goto_0
.end method

.method private initSeekBars()V
    .locals 2

    .prologue
    const v1, 0x7f0f009e

    .line 616
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingProgressBar:Landroid/widget/SeekBar;

    .line 619
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackSeekBar:Landroid/widget/SeekBar;

    .line 634
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackSeekBar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 690
    return-void
.end method

.method private isRunningFromUIThread()Z
    .locals 4

    .prologue
    .line 981
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    .line 982
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setFontAndText()V
    .locals 4

    .prologue
    const v3, 0x7f0f009f

    const/4 v2, 0x0

    .line 585
    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingProgressTextView:Landroid/widget/TextView;

    .line 586
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingProgressTextView:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 587
    const v0, 0x7f0f00a1

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingTotalTextView:Landroid/widget/TextView;

    .line 588
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingTotalTextView:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 589
    const v0, 0x7f0f00a0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 593
    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackProgressTextView:Landroid/widget/TextView;

    .line 594
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackProgressTextView:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 597
    const v0, 0x7f0f009d

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    .line 598
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    const-string v1, "RECORD"

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 599
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    const v1, 0x7f0801b1

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 603
    const v0, 0x7f0f00a2

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    .line 604
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const-string v1, "PLAY"

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 605
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 606
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const v1, 0x7f080181

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 609
    const v0, 0x7f0f00a3

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->sendButton:Landroid/widget/Button;

    .line 610
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->sendButton:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 611
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->sendButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 612
    return-void
.end method

.method private startObservingAudioPlayerPlaybackProgress()V
    .locals 6

    .prologue
    const-wide/16 v2, 0xc8

    .line 1664
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

    if-nez v0, :cond_0

    .line 1665
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

    .line 1666
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1670
    :cond_0
    return-void
.end method

.method private startObservingAudioRecorderRecordingProgress(Z)V
    .locals 6
    .param p1, "isForDummyRecording"    # Z

    .prologue
    .line 1364
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorderRecordingObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

    if-nez v0, :cond_0

    .line 1365
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/infra/media/AudioRecorder;ZLcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorderRecordingObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

    .line 1367
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorderRecordingObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1371
    :cond_0
    return-void
.end method

.method private startUpdatingAudioEqualzierForRealRecording()V
    .locals 6

    .prologue
    .line 804
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_1

    .line 805
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$2;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 825
    :cond_0
    :goto_0
    return-void

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->activate(Z)V

    .line 819
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    if-nez v0, :cond_0

    .line 820
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-static {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$1400(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/DotMeterView;Lcom/att/mobile/android/infra/media/AudioRecorder;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .line 821
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method private startUpdatingAudioEqualzierPlayback()V
    .locals 6

    .prologue
    .line 833
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_1

    .line 834
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$3;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 845
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->activate(Z)V

    .line 848
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    if-nez v0, :cond_0

    .line 849
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-static {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$1600(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/vvm/screen/DotMeterView;Lcom/att/mobile/android/infra/media/AudioPlayer;)Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .line 850
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xfa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method private stopObservingAudioPlayerPlaybackProgress()V
    .locals 1

    .prologue
    .line 1677
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

    if-eqz v0, :cond_0

    .line 1679
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;->cancel()Z

    .line 1680
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayerPlaybackObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioPlayerPlaybackObserver;

    .line 1682
    :cond_0
    return-void
.end method

.method private stopObservingAudioRecorderRecordingProgress()V
    .locals 1

    .prologue
    .line 1378
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorderRecordingObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

    if-eqz v0, :cond_0

    .line 1380
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorderRecordingObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->cancel()Z

    .line 1381
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorderRecordingObserver:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;

    .line 1383
    :cond_0
    return-void
.end method

.method private stopUpdatingAudioEqualizer()V
    .locals 1

    .prologue
    .line 862
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_1

    .line 863
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$4;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 882
    :cond_0
    :goto_0
    return-void

    .line 874
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->deactivate()V

    .line 877
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    if-eqz v0, :cond_0

    .line 879
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->cancel()Z

    .line 880
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioEqualizerUpdater:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    goto :goto_0
.end method

.method private switchPlayButtonState(Ljava/lang/String;)V
    .locals 3
    .param p1, "playButtonState"    # Ljava/lang/String;

    .prologue
    .line 1050
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1053
    .local v0, "currentPlayButtonState":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1086
    :cond_0
    :goto_0
    return-void

    .line 1059
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1060
    new-instance v1, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$7;

    invoke-direct {v1, p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$7;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1071
    :cond_2
    const-string v1, "PAUSE_PLAYBACK"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1073
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const v2, 0x7f0200ef

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1074
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const-string v2, "PAUSE_PLAYBACK"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 1075
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->enableRecordButton(Z)V

    .line 1076
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const v2, 0x7f080168

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1079
    :cond_3
    const-string v1, "PLAY"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1081
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const v2, 0x7f0200ed

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1082
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const-string v2, "PLAY"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 1083
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->enableRecordButton(Z)V

    .line 1084
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const v2, 0x7f080181

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private switchProgressAndSeekBars(Z)V
    .locals 1
    .param p1, "isRecordingPlaybackSeekBar"    # Z

    .prologue
    .line 1394
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1395
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$13;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$13;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Z)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1404
    :cond_0
    return-void
.end method

.method private switchRecordButtonState(Ljava/lang/String;)V
    .locals 3
    .param p1, "recordButtonState"    # Ljava/lang/String;

    .prologue
    .line 993
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 996
    .local v0, "currentRecordButtonState":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1028
    :cond_0
    :goto_0
    return-void

    .line 1002
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1003
    new-instance v1, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$6;

    invoke-direct {v1, p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$6;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1014
    :cond_2
    const-string v1, "STOP_RECORDING"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1016
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    const v2, 0x7f020165

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1017
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    const-string v2, "STOP_RECORDING"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 1018
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    const v2, 0x7f0801d3

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1021
    :cond_3
    const-string v1, "RECORD"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1023
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    const v2, 0x7f02013f

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1024
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    const-string v2, "RECORD"

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 1025
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    const v2, 0x7f0801b1

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1026
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private updatePlaybackProgressBarProgressUIComponent()V
    .locals 3

    .prologue
    .line 1552
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1553
    new-instance v2, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$15;

    invoke-direct {v2, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$15;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1574
    :goto_0
    return-void

    .line 1563
    :cond_0
    const/4 v1, 0x0

    .line 1564
    .local v1, "mediaDuration":I
    const/4 v0, 0x0

    .line 1565
    .local v0, "mediaCurrentPosition":I
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v2, :cond_1

    .line 1567
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getMediaDuration()I

    move-result v1

    .line 1568
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getCurrentPosition()I

    move-result v0

    .line 1573
    :cond_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackSeekBar:Landroid/widget/SeekBar;

    if-lez v1, :cond_2

    .end local v0    # "mediaCurrentPosition":I
    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .restart local v0    # "mediaCurrentPosition":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updatePlaybackProgressTextUIComponent(Z)V
    .locals 9
    .param p1, "backwardsProgress"    # Z

    .prologue
    .line 1602
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1603
    new-instance v6, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$16;

    invoke-direct {v6, p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$16;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Z)V

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1645
    :goto_0
    return-void

    .line 1613
    :cond_0
    const/4 v1, 0x0

    .line 1614
    .local v1, "mediaDuration":I
    const/4 v0, 0x0

    .line 1616
    .local v0, "currentPosition":I
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v6, :cond_1

    .line 1617
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v6}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getMediaDuration()I

    move-result v1

    .line 1618
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v6}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getCurrentPosition()I

    move-result v0

    .line 1623
    :goto_1
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->setTotalRecordingTime(I)V

    .line 1627
    if-eqz p1, :cond_2

    .line 1628
    sub-int v3, v1, v0

    .line 1629
    .local v3, "recordTimeLeft":I
    div-int/lit16 v5, v3, 0x3e8

    .line 1632
    .local v5, "recordingPlaybackSecondsProgress":I
    invoke-direct {p0, v3, v5}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->adjustRecordingPlaybackSecondsProgress(II)I

    move-result v5

    .line 1637
    .end local v3    # "recordTimeLeft":I
    :goto_2
    div-int/lit8 v4, v5, 0x3c

    .line 1638
    .local v4, "recordingPlaybackMinutesProgress":I
    rem-int/lit8 v5, v5, 0x3c

    .line 1640
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    .line 1641
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v6, 0xa

    if-ge v5, v6, :cond_3

    const-string v6, "0"

    :goto_3
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1642
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1643
    .local v2, "progressText":Ljava/lang/String;
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackProgressTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1644
    const-string v6, "AudioRecorderActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioRecorderActivity.updatePlaybackProgressTextUIComponent() - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1620
    .end local v2    # "progressText":Ljava/lang/String;
    .end local v4    # "recordingPlaybackMinutesProgress":I
    .end local v5    # "recordingPlaybackSecondsProgress":I
    :cond_1
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->currentRecordingProgress:I

    .line 1621
    const/4 v0, 0x0

    goto :goto_1

    .line 1634
    :cond_2
    div-int/lit16 v5, v0, 0x3e8

    .restart local v5    # "recordingPlaybackSecondsProgress":I
    goto :goto_2

    .line 1641
    .restart local v4    # "recordingPlaybackMinutesProgress":I
    :cond_3
    const-string v6, ""

    goto :goto_3
.end method

.method private updatePlaybackProgressUIComponents(Z)V
    .locals 1
    .param p1, "backwardsProgress"    # Z

    .prologue
    .line 1531
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1532
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$14;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$14;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Z)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1544
    :goto_0
    return-void

    .line 1542
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updatePlaybackProgressBarProgressUIComponent()V

    .line 1543
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updatePlaybackProgressTextUIComponent(Z)V

    goto :goto_0
.end method

.method private updateRecordingProgressBarProgressUIComponent()V
    .locals 2

    .prologue
    .line 1307
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1308
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$11;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$11;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1320
    :goto_0
    return-void

    .line 1319
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingProgressBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->currentRecordingProgress:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method private updateRecordingProgressUIComponents()V
    .locals 1

    .prologue
    .line 1286
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1287
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$10;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$10;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1299
    :goto_0
    return-void

    .line 1297
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updateRecordingProgressBarProgressUIComponent()V

    .line 1298
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updateRecordingProgressTextUIComponent()V

    goto :goto_0
.end method


# virtual methods
.method protected getRecFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1095
    const/4 v0, 0x0

    .line 1101
    :goto_0
    return-object v0

    .line 1097
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    sget-object v1, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->RECORD_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1098
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordNameFilePath:Ljava/lang/String;

    goto :goto_0

    .line 1101
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordCustomFilePath:Ljava/lang/String;

    goto :goto_0
.end method

.method public onAudioFocusChange(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 1760
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 506
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 507
    const-string v1, "AudioRecorderActivity"

    const-string v2, "AudioRecorderActivity.onCreate()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    .line 514
    new-instance v1, Ljava/lang/StringBuffer;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/VVMApplication;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 515
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 519
    .local v0, "pathPrefix":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".amr"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordNameFilePath:Ljava/lang/String;

    .line 520
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->CUSTOM:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".amr"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordCustomFilePath:Ljava/lang/String;

    .line 522
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "IMAP_RECORDING_GREETING_TYPE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    .line 524
    const v1, 0x7f040024

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->setContentView(I)V

    .line 526
    const v1, 0x7f0801b2

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->initActionBar(IZ)V

    .line 528
    const v1, 0x7f0f009c

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/vvm/screen/DotMeterView;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    .line 535
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->setVolumeControlStream(I)V

    .line 537
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->initSeekBars()V

    .line 538
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->setFontAndText()V

    .line 581
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 1730
    const-string v0, "AudioRecorderActivity"

    const-string v1, "AudioRecorderActivity.onDestroy()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1738
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    if-eqz v0, :cond_0

    .line 1739
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioRecorder;->stop()V

    .line 1742
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_1

    .line 1743
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->release()V

    .line 1744
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 1750
    :cond_1
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 1751
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setIsApplicationSpeakerOn(Z)V

    .line 1754
    :cond_2
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 1755
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 1689
    const-string v1, "AudioRecorderActivity"

    const-string v2, "AudioRecorderActivity.onPause()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1691
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->killRefreshingLoop()V

    .line 1698
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-virtual {v1}, Lcom/att/mobile/android/infra/media/AudioRecorder;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1700
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButtonOnClickCallback(Landroid/view/View;)V

    .line 1715
    :cond_0
    :goto_0
    :try_start_0
    const-string v1, "AudioRecorderActivity"

    const-string v2, "AudioRecorderActivity.onPause() -  sleeping for 50 milliseconds while pausing"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1716
    const-wide/16 v2, 0x32

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1721
    :goto_1
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onPause()V

    .line 1722
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 1723
    return-void

    .line 1701
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v1, :cond_0

    .line 1703
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1705
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButtonOnClickCallback(Landroid/view/View;)V

    .line 1709
    :cond_2
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->unregisterAudioPlayerEventsListener(Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;)V

    goto :goto_0

    .line 1717
    :catch_0
    move-exception v0

    .line 1718
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AudioRecorderActivity"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public onPlaybackEnd()V
    .locals 0

    .prologue
    .line 1512
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->onPlaybackStop()V

    .line 1513
    return-void
.end method

.method public onPlaybackPause()V
    .locals 3

    .prologue
    .line 1448
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1449
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1450
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->releaseWakeLock()V

    .line 1452
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->restoreDeviceAudioMode()V

    .line 1453
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/VVMApplication;->setIsApplicationSpeakerOn(Z)V

    .line 1456
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->stopObservingAudioPlayerPlaybackProgress()V

    .line 1459
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->stopUpdatingAudioEqualizer()V

    .line 1462
    const-string v1, "PLAY"

    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchPlayButtonState(Ljava/lang/String;)V

    .line 1465
    return-void
.end method

.method public onPlaybackStart(I)V
    .locals 1
    .param p1, "startingPosition"    # I

    .prologue
    .line 1430
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->startObservingAudioPlayerPlaybackProgress()V

    .line 1433
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->startUpdatingAudioEqualzierPlayback()V

    .line 1436
    const-string v0, "PAUSE_PLAYBACK"

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchPlayButtonState(Ljava/lang/String;)V

    .line 1437
    return-void
.end method

.method public onPlaybackStop()V
    .locals 3

    .prologue
    .line 1476
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1477
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1478
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->releaseWakeLock()V

    .line 1480
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->restoreDeviceAudioMode()V

    .line 1481
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/VVMApplication;->setIsApplicationSpeakerOn(Z)V

    .line 1484
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->stopObservingAudioPlayerPlaybackProgress()V

    .line 1487
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->stopUpdatingAudioEqualizer()V

    .line 1490
    const-string v1, "PLAY"

    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchPlayButtonState(Ljava/lang/String;)V

    .line 1494
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updatePlaybackProgressUIComponents(Z)V

    .line 1496
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v1, :cond_0

    .line 1497
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->release()V

    .line 1498
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 1503
    :cond_0
    return-void
.end method

.method public onPlayerError(I)V
    .locals 0
    .param p1, "errorCode"    # I

    .prologue
    .line 1520
    return-void
.end method

.method public onPlayerInitialization(I)V
    .locals 2
    .param p1, "mediaToBePlayedDuration"    # I

    .prologue
    const/4 v1, 0x1

    .line 1412
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1415
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchProgressAndSeekBars(Z)V

    .line 1418
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updatePlaybackProgressUIComponents(Z)V

    .line 1419
    return-void
.end method

.method public onRecorderError()V
    .locals 0

    .prologue
    .line 1278
    return-void
.end method

.method public onRecorderInitialization(I)V
    .locals 2
    .param p1, "maximumRecordDuration"    # I

    .prologue
    const/4 v1, 0x0

    .line 1161
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1162
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$8;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$8;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;I)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1187
    :goto_0
    return-void

    .line 1173
    :cond_0
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->currentRecordingProgress:I

    .line 1176
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1179
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchProgressAndSeekBars(Z)V

    .line 1182
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updateRecordingProgressUIComponents()V

    .line 1185
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1186
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->sendButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onRecordingEnd()V
    .locals 0

    .prologue
    .line 1269
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->onRecordingStop()V

    .line 1270
    return-void
.end method

.method public onRecordingStart()V
    .locals 1

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->acquireWakeLock()V

    .line 1201
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->startObservingAudioRecorderRecordingProgress(Z)V

    .line 1202
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->maximumRecordDuration:I

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->setTotalRecordingTime(I)V

    .line 1205
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->startUpdatingAudioEqualzierForRealRecording()V

    .line 1208
    const-string v0, "STOP_RECORDING"

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchRecordButtonState(Ljava/lang/String;)V

    .line 1209
    return-void
.end method

.method public onRecordingStop()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1218
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1219
    new-instance v0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$9;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$9;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1261
    :goto_0
    return-void

    .line 1233
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->releaseWakeLock()V

    .line 1236
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/infra/media/AudioRecorder;->unregisterAudioRecorderEventsListener(Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;)V

    .line 1239
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->stopObservingAudioRecorderRecordingProgress()V

    .line 1242
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->stopUpdatingAudioEqualizer()V

    .line 1245
    const-string v0, "RECORD"

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchRecordButtonState(Ljava/lang/String;)V

    .line 1248
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1249
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->sendButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1252
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->switchProgressAndSeekBars(Z)V

    .line 1255
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->updatePlaybackProgressUIComponents(Z)V

    .line 1257
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingPlaybackSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->currentRecordingProgress:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    goto :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .prologue
    .line 887
    const-string v0, "AudioRecorderActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRequestPermissionsResult requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    packed-switch p1, :pswitch_data_0

    .line 907
    invoke-super {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 909
    :cond_0
    :goto_0
    return-void

    .line 891
    :pswitch_0
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->isPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 892
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButtonOnClickCallback(Landroid/view/View;)V

    goto :goto_0

    .line 893
    :cond_1
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {v0, p0}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->wasNeverAskAgainChecked(Ljava/lang/String;Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 894
    const v1, 0x7f08016b

    const v2, 0x7f0800f1

    const v3, 0x7f080152

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$5;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$5;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    goto :goto_0

    .line 888
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 706
    const-string v0, "AudioRecorderActivity"

    const-string v1, "AudioRecorderActivity.onResume()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 707
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 708
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 710
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->meter:Lcom/att/mobile/android/vvm/screen/DotMeterView;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->startRefreshingLoop()V

    .line 713
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->screenStateBeforeGettingFocus:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->screenStateBeforeGettingFocus:I

    if-nez v0, :cond_1

    .line 716
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_1

    .line 717
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->registerAudioPlayerEventsListener(Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;)V

    .line 726
    :cond_1
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 697
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onStart()V

    .line 698
    const-string v0, "AudioRecorderActivity"

    const-string v1, "AudioRecorderActivity.onStart()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 737
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onWindowFocusChanged(Z)V

    .line 740
    if-nez p1, :cond_1

    .line 741
    const-string v0, "AudioRecorderActivity"

    const-string v1, "AudioRecorderActivity.onWindowFocusChanged() - activity is NOT visible"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    :cond_0
    :goto_0
    return-void

    .line 745
    :cond_1
    const-string v0, "AudioRecorderActivity"

    const-string v1, "AudioRecorderActivity.onWindowFocusChanged() - activity IS visible"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->screenStateBeforeGettingFocus:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 753
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->screenStateBeforeGettingFocus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->registerAudioPlayerEventsListener(Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;)V

    goto :goto_0
.end method

.method public playButtonOnClickCallback(Landroid/view/View;)V
    .locals 6
    .param p1, "buttonView"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1112
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "PLAY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1118
    invoke-direct {p0, v3}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->enableRecordButton(Z)V

    .line 1121
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-nez v1, :cond_0

    .line 1122
    new-instance v1, Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-direct {v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;-><init>()V

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    .line 1123
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->registerAudioPlayerEventsListener(Lcom/att/mobile/android/infra/media/MediaPlayerEventsListener;)V

    .line 1125
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getRecFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->initializeMedia(Ljava/lang/String;)V

    .line 1129
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->acquireWakeLock()V

    .line 1131
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setApplicationAudioMode()V

    .line 1132
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1, v4}, Lcom/att/mobile/android/vvm/VVMApplication;->setIsApplicationSpeakerOn(Z)V

    .line 1133
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1134
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1135
    const/4 v1, 0x3

    invoke-virtual {v0, p0, v1, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 1139
    :goto_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/infra/media/AudioPlayer;->start(I)V

    .line 1149
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :goto_1
    return-void

    .line 1137
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_1
    invoke-virtual {v0, p0, v3, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    goto :goto_0

    .line 1144
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_2
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1145
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->pause()V

    .line 1147
    :cond_3
    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->enableRecordButton(Z)V

    goto :goto_1
.end method

.method public recordButtonOnClickCallback(Landroid/view/View;)V
    .locals 4
    .param p1, "buttonView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 918
    const-string v0, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 919
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v2

    const/16 v1, 0xa

    invoke-static {p0, v0, v1}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    .line 973
    :cond_0
    :goto_0
    return-void

    .line 924
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "RECORD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 926
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 929
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    if-nez v0, :cond_2

    .line 930
    new-instance v0, Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getRecFilePath()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->maximumRecordDuration:I

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/infra/media/AudioRecorder;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    .line 934
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/infra/media/AudioRecorder;->registerAudioRecorderEventsListener(Lcom/att/mobile/android/infra/media/MediaRecorderEventsListener;)V

    .line 950
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_3

    .line 952
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->stop()V

    .line 962
    :cond_3
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioRecorder;->start()V

    goto :goto_0

    .line 967
    :cond_4
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioRecorder;->stop()V

    .line 968
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 969
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const-string v1, "PLAY"

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 970
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 971
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->playButton:Landroid/widget/ImageButton;

    const v1, 0x7f080181

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public sendButtonOnClickCallback(Landroid/view/View;)V
    .locals 1
    .param p1, "buttonView"    # Landroid/view/View;

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    if-eqz v0, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->audioPlayer:Lcom/att/mobile/android/infra/media/AudioPlayer;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->stop()V

    .line 1040
    :cond_0
    return-void
.end method

.method protected setTotalRecordingTime(I)V
    .locals 6
    .param p1, "totalRecordingTime"    # I

    .prologue
    .line 1579
    div-int/lit16 v2, p1, 0x3e8

    .line 1580
    .local v2, "recordingPlaybackSecondsProgress":I
    invoke-direct {p0, p1, v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->adjustRecordingPlaybackSecondsProgress(II)I

    move-result v2

    .line 1582
    div-int/lit8 v1, v2, 0x3c

    .line 1583
    .local v1, "recordingPlaybackMinutesProgress":I
    rem-int/lit8 v2, v2, 0x3c

    .line 1586
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    .line 1587
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    const-string v3, "0"

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1588
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1589
    .local v0, "progressText":Ljava/lang/String;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingTotalTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1590
    const-string v3, "AudioRecorderActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AudioRecorderActivity.updatePlaybackProgressTextUIComponent() - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1591
    return-void

    .line 1587
    .end local v0    # "progressText":Ljava/lang/String;
    :cond_0
    const-string v3, ""

    goto :goto_0
.end method

.method protected updateRecordingProgressTextUIComponent()V
    .locals 5

    .prologue
    .line 1328
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->isRunningFromUIThread()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1329
    new-instance v3, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$12;

    invoke-direct {v3, p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$12;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1355
    :goto_0
    return-void

    .line 1340
    :cond_0
    iget v3, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->currentRecordingProgress:I

    div-int/lit16 v0, v3, 0x3e8

    .line 1343
    .local v0, "backwardSecondsRecordingProgress":I
    iget v3, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->maximumRecordDuration:I

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->currentRecordingProgress:I

    sub-int/2addr v3, v4

    rem-int/lit16 v3, v3, 0x3e8

    if-lez v3, :cond_1

    .line 1344
    add-int/lit8 v0, v0, 0x1

    .line 1348
    :cond_1
    div-int/lit8 v1, v0, 0x3c

    .line 1349
    .local v1, "currentRecordingMinutesProgress":I
    rem-int/lit8 v0, v0, 0x3c

    .line 1351
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    .line 1352
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v3, 0xa

    if-ge v0, v3, :cond_2

    const-string v3, "0"

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1353
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1354
    .local v2, "progressText":Ljava/lang/String;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->recordingProgressTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1352
    .end local v2    # "progressText":Ljava/lang/String;
    :cond_2
    const-string v3, ""

    goto :goto_1
.end method

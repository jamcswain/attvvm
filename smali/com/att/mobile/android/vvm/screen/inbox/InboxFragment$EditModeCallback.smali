.class public Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;
.super Ljava/lang/Object;
.source "InboxFragment.java"

# interfaces
.implements Landroid/support/v7/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "EditModeCallback"
.end annotation


# instance fields
.field private mMenuRes:I

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

.field private titleAndButtonsLayout:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    .param p2, "menuRes"    # I

    .prologue
    .line 128
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput p2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->mMenuRes:I

    .line 130
    return-void
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->titleAndButtonsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method


# virtual methods
.method public onActionItemClicked(Landroid/support/v7/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 157
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActionItemClicked ItemId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 160
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v1

    .line 164
    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 197
    const/4 v1, 0x0

    goto :goto_0

    .line 166
    :sswitch_0
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 167
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->selectByIds()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getSelectedIdsItems()[Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;->onDeleteAction([Ljava/lang/Long;)V

    goto :goto_0

    .line 170
    :cond_2
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getSelectedPhonesItems()[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;->onDeleteAction([Ljava/lang/String;)V

    goto :goto_0

    .line 174
    :cond_3
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showNoNetworkConnectionDialog(Landroid/content/Context;)V

    goto :goto_0

    .line 179
    :sswitch_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->selectByIds()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 180
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getSelectedIdsItems()[Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;->onSaveAction([Ljava/lang/Long;)V

    .line 184
    :goto_1
    invoke-virtual {p1}, Landroid/support/v7/view/ActionMode;->finish()V

    goto :goto_0

    .line 182
    :cond_4
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getSelectedPhonesItems()[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;->onSaveAction([Ljava/lang/String;)V

    goto :goto_1

    .line 188
    :sswitch_2
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->selectByIds()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 189
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getSelectedIdsItems()[Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;->onUnsaveAction([Ljava/lang/Long;)V

    .line 193
    :goto_2
    invoke-virtual {p1}, Landroid/support/v7/view/ActionMode;->finish()V

    goto/16 :goto_0

    .line 191
    :cond_5
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getSelectedPhonesItems()[Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;->onUnsaveAction([Ljava/lang/String;)V

    goto :goto_2

    .line 164
    :sswitch_data_0
    .sparse-switch
        0x7f0f0187 -> :sswitch_1
        0x7f0f0188 -> :sswitch_0
        0x7f0f0190 -> :sswitch_2
    .end sparse-switch
.end method

.method public onCreateActionMode(Landroid/support/v7/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 135
    invoke-virtual {p1}, Landroid/support/v7/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 136
    .local v1, "inflater":Landroid/view/MenuInflater;
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->mMenuRes:I

    invoke-virtual {v1, v2, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 138
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AppCompatActivity;

    .line 139
    .local v0, "activity":Landroid/support/v7/app/AppCompatActivity;
    const v2, 0x7f0800ee

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/support/v7/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 140
    const v2, 0x7f0f010d

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->titleAndButtonsLayout:Landroid/widget/LinearLayout;

    .line 141
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->titleAndButtonsLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 142
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->access$100(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;Landroid/support/v7/app/AppCompatActivity;Z)V

    .line 143
    const v2, 0x7f0f010c

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/Menu;->clear()V

    .line 144
    const/4 v2, 0x1

    return v2
.end method

.method public onDestroyActionMode(Landroid/support/v7/view/ActionMode;)V
    .locals 6
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;

    .prologue
    .line 206
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDestroyActionMode"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AppCompatActivity;

    .line 209
    .local v0, "activity":Landroid/support/v7/app/AppCompatActivity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->turnEditModeOff()V

    .line 215
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->titleAndButtonsLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->titleAndButtonsLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;

    invoke-direct {v2, p0, v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;-><init>(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;Landroid/support/v7/app/AppCompatActivity;)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onPrepareActionMode(Landroid/support/v7/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

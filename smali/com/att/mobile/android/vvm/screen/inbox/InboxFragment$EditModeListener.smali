.class public Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;
.super Ljava/lang/Object;
.source "InboxFragment.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "EditModeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;


# direct methods
.method protected constructor <init>(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEmptySelection()V
    .locals 2

    .prologue
    .line 106
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EditModeListener.onEmptySelection"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    invoke-virtual {v0}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 109
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 111
    :cond_0
    return-void
.end method

.method public onItemLongClick(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
    .locals 4
    .param p1, "item"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 98
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EditModeListener.onItemLongClick item="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "null"

    :goto_0
    invoke-static {v2, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AppCompatActivity;

    .line 100
    .local v0, "activity":Landroid/support/v7/app/AppCompatActivity;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatActivity;->startSupportActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    move-result-object v2

    iput-object v2, v1, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 101
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Start EditMode"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return-void

    .line 98
    .end local v0    # "activity":Landroid/support/v7/app/AppCompatActivity;
    :cond_0
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

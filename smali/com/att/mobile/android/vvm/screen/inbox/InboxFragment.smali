.class public Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
.super Landroid/support/v4/app/Fragment;
.source "InboxFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;,
        Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;,
        Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;,
        Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field public static final BY_CONTACT:Ljava/lang/String; = "FOR_CONTACT"

.field public static final FILTER_TYPE:Ljava/lang/String; = "FILTER_TYPE"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "PHONE_NUMBER"

.field private static final TAG:Ljava/lang/String;

.field private static final URL_INBOX_LOADER:I = 0xc

.field private static final URL_SAVE_LOADER:I = 0xd


# instance fields
.field protected byContact:Z

.field protected filterType:I

.field protected groupedByContact:Z

.field protected mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

.field protected mActionMode:Landroid/support/v7/view/ActionMode;

.field protected mActionModeCallback:Landroid/support/v7/view/ActionMode$Callback;

.field protected mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

.field protected mEditModeListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;

.field protected mEmptyListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

.field protected mEmptyTextView:Landroid/widget/TextView;

.field protected mLayoutManager:Landroid/support/v7/widget/LinearLayoutManager;

.field private mLoaderId:I

.field protected mLoaderManager:Landroid/support/v4/app/LoaderManager;

.field private mLoadingContainer:Landroid/view/View;

.field protected mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

.field protected mRecyclerView:Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

.field protected modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field protected phoneNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 71
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;Landroid/support/v7/app/AppCompatActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    .param p1, "x1"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "x2"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->setBackArrowInToolbar(Landroid/support/v7/app/AppCompatActivity;Z)V

    return-void
.end method

.method private createCursorLoader()Landroid/support/v4/content/Loader;
    .locals 9
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 370
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    if-eqz v0, :cond_0

    sget-object v2, Lcom/att/mobile/android/vvm/model/db/VmContentProvider;->CONTENT_GROUPED_URI:Landroid/net/Uri;

    .line 371
    .local v2, "uri":Landroid/net/Uri;
    :goto_0
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    if-eqz v0, :cond_1

    sget-object v3, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AGGREGATED_VM:[Ljava/lang/String;

    .line 372
    .local v3, "projection":[Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->phoneNumber:Ljava/lang/String;

    if-nez v0, :cond_3

    iget v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    if-ne v0, v7, :cond_2

    sget-object v4, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_SAVED:Ljava/lang/String;

    .line 374
    .local v4, "where":Ljava/lang/String;
    :goto_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->phoneNumber:Ljava/lang/String;

    if-nez v0, :cond_5

    new-array v5, v6, [Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    .line 377
    .local v5, "selectArgs":[Ljava/lang/String;
    :goto_3
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createCursorLoader filterType="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v6, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " where="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    .line 380
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v6, "time desc"

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 370
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v3    # "projection":[Ljava/lang/String;
    .end local v4    # "where":Ljava/lang/String;
    .end local v5    # "selectArgs":[Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/att/mobile/android/vvm/model/db/VmContentProvider;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 371
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_1
    sget-object v3, Lcom/att/mobile/android/vvm/model/db/ModelManager;->COLUMNS_VM_LIST:[Ljava/lang/String;

    goto :goto_1

    .line 372
    .restart local v3    # "projection":[Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_SAVED_NOT:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    if-ne v0, v7, :cond_4

    sget-object v4, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_PHONE_NUMBER_AND_SAVED:Ljava/lang/String;

    goto :goto_2

    :cond_4
    sget-object v4, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_PHONE_NUMBER_AND_SAVED_NOT:Ljava/lang/String;

    goto :goto_2

    .line 374
    .restart local v4    # "where":Ljava/lang/String;
    :cond_5
    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->phoneNumber:Ljava/lang/String;

    .line 375
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    goto :goto_3
.end method

.method private getLoaderId(I)I
    .locals 1
    .param p1, "filterType"    # I

    .prologue
    .line 444
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const/16 v0, 0xd

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xc

    goto :goto_0
.end method

.method private initEmptyTextView(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 315
    const v0, 0x7f0f0123

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEmptyTextView:Landroid/widget/TextView;

    .line 316
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEmptyTextView:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 317
    return-void
.end method

.method private initLoadingUIElements(Landroid/view/View;)V
    .locals 1
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 310
    const v0, 0x7f0f0121

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoadingContainer:Landroid/view/View;

    .line 311
    const v0, 0x7f0f0122

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    .line 312
    return-void
.end method

.method private initRecyclerViewAndElements(Landroid/view/View;)V
    .locals 4
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 289
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initRecyclerViewAndElements groupedByContact="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const v0, 0x7f0f0124

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mRecyclerView:Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    .line 294
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mRecyclerView:Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->setHasFixedSize(Z)V

    .line 297
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLayoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    .line 298
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mRecyclerView:Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLayoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 300
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->initEmptyTextView(Landroid/view/View;)V

    .line 301
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->initLoadingUIElements(Landroid/view/View;)V

    .line 303
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mRecyclerView:Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoadingContainer:Landroid/view/View;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->setLoadingOrEmptyViewElements(Landroid/view/View;Lcom/att/mobile/android/infra/utils/LoadingProgressBar;Landroid/widget/TextView;)V

    .line 304
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->initInboxRecyclerAdapter(Z)V

    .line 306
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoaderId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 307
    return-void
.end method

.method public static newInstance(ILjava/lang/String;Z)Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    .locals 3
    .param p0, "filterType"    # I
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "byContact"    # Z

    .prologue
    .line 434
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 435
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "FILTER_TYPE"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 436
    const-string v2, "PHONE_NUMBER"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v2, "FOR_CONTACT"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 438
    new-instance v1, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-direct {v1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;-><init>()V

    .line 439
    .local v1, "fragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    invoke-virtual {v1, v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->setArguments(Landroid/os/Bundle;)V

    .line 440
    return-object v1
.end method

.method private setBackArrowInToolbar(Landroid/support/v7/app/AppCompatActivity;Z)V
    .locals 1
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "shouldShowArrow"    # Z

    .prologue
    .line 233
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 234
    .local v0, "supportActionBar":Landroid/support/v7/app/ActionBar;
    invoke-virtual {v0, p2}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 235
    invoke-virtual {v0, p2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 236
    return-void
.end method


# virtual methods
.method public closeEditMode()V
    .locals 2

    .prologue
    .line 426
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    const-string v1, "closeEditMode"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    invoke-virtual {v0}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 430
    :cond_0
    return-void
.end method

.method public getFilterType()I
    .locals 1

    .prologue
    .line 421
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    return v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getItemCount()I

    move-result v0

    .line 348
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initInboxRecyclerAdapter(Z)V
    .locals 4
    .param p1, "newGroupedByContact"    # Z

    .prologue
    const/4 v3, 0x0

    .line 323
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initInboxRecyclerAdapter mAdapter="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " groupedByContact="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newGroupedByContact="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    if-ne v0, p1, :cond_0

    .line 325
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    const-string v1, "NOT initInboxRecyclerAdapters."

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :goto_0
    return-void

    .line 329
    :cond_0
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    const-string v1, "initInboxRecyclerAdapters."

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    if-eqz p1, :cond_1

    .line 331
    new-instance v0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    invoke-direct {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    .line 338
    :goto_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEditModeListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->setActionModeListener(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;)V

    .line 339
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mRecyclerView:Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 340
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    goto :goto_0

    .line 332
    :cond_1
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->byContact:Z

    if-eqz v0, :cond_2

    .line 333
    new-instance v0, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    invoke-direct {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    goto :goto_1

    .line 335
    :cond_2
    new-instance v0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    invoke-direct {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1, "loaderID"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    sget-object v1, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateLoader loaderID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoaderId:I

    if-ne p1, v1, :cond_0

    .line 357
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->createCursorLoader()Landroid/support/v4/content/Loader;

    move-result-object v0

    .line 358
    .local v0, "lC":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->forceLoad()V

    .line 362
    .end local v0    # "lC":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 258
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 260
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 261
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "FILTER_TYPE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    .line 262
    const-string v2, "PHONE_NUMBER"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->phoneNumber:Ljava/lang/String;

    .line 263
    const-string v2, "FOR_CONTACT"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->byContact:Z

    .line 264
    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->byContact:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    .line 266
    new-instance v4, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_1

    const v2, 0x7f100002

    :goto_1
    invoke-direct {v4, p0, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;-><init>(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;I)V

    iput-object v4, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    .line 267
    new-instance v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;

    invoke-direct {v2, p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;-><init>(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;)V

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEditModeListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeListener;

    .line 268
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    .line 269
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    invoke-direct {p0, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getLoaderId(I)I

    move-result v2

    iput v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoaderId:I

    .line 270
    sget-object v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreateView filterType="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " groupedByContact="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->groupedByContact:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mLoaderId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoaderId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const v2, 0x7f040046

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 274
    .local v1, "rootView":Landroid/view/View;
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->initRecyclerViewAndElements(Landroid/view/View;)V

    .line 276
    return-object v1

    .line 264
    .end local v1    # "rootView":Landroid/view/View;
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isGroupByContact()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto :goto_0

    .line 266
    :cond_1
    const v2, 0x7f100004

    goto :goto_1
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 392
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadFinished cursor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v0, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->changeCursor(Landroid/database/Cursor;)V

    .line 396
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEmptyListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEmptyListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getItemCount()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;->onListUpdated(II)V

    .line 399
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 49
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 405
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mAdapter:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->changeCursor(Landroid/database/Cursor;)V

    .line 406
    return-void
.end method

.method public refreshList(Z)V
    .locals 3
    .param p1, "newGroupedByContact"    # Z

    .prologue
    .line 414
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "refreshList newGroupedByContact="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->initInboxRecyclerAdapter(Z)V

    .line 417
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mLoaderId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 418
    return-void
.end method

.method public setActionListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;)V
    .locals 0
    .param p1, "actionListener"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    .line 253
    return-void
.end method

.method public setListListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;)V
    .locals 0
    .param p1, "emptyListListener"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEmptyListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    .line 92
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 3
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 241
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->TAG:Ljava/lang/String;

    const-string v1, "setUserVisibleHint close EditMode"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    invoke-virtual {v0}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 245
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEmptyListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mEmptyListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->filterType:I

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getItemCount()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;->onListUpdated(II)V

    .line 248
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 249
    return-void
.end method

.method protected startLoadingProgressBar()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mRecyclerView:Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->startLoadingProgressBar()V

    .line 281
    return-void
.end method

.method protected stopLoadingProgressBar()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->mRecyclerView:Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->stopLoadingProgressBar()V

    .line 285
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;
.super Ljava/lang/Object;
.source "InboxFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->onDestroyActionMode(Landroid/support/v7/view/ActionMode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;

.field final synthetic val$activity:Landroid/support/v7/app/AppCompatActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;Landroid/support/v7/app/AppCompatActivity;)V
    .locals 0
    .param p1, "this$1"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;->this$1:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;->val$activity:Landroid/support/v7/app/AppCompatActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 219
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;->this$1:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->access$200(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;)Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 220
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;->this$1:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;->val$activity:Landroid/support/v7/app/AppCompatActivity;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;->val$activity:Landroid/support/v7/app/AppCompatActivity;

    instance-of v4, v4, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;

    invoke-static {v2, v3, v4}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->access$100(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;Landroid/support/v7/app/AppCompatActivity;Z)V

    .line 221
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback$1;->this$1:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$EditModeCallback;->this$0:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AppCompatActivity;

    .line 222
    .local v0, "activity":Landroid/support/v7/app/AppCompatActivity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 223
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 224
    .local v1, "inflater":Landroid/view/MenuInflater;
    const v3, 0x7f100001

    const v2, 0x7f0f010c

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 226
    .end local v1    # "inflater":Landroid/view/MenuInflater;
    :cond_0
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;
.super Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;
.source "InboxFragmentPagerAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final sAllInboxItemsTabIndex:I = 0x0

.field private static final sLastInboxTabIndex:I = 0x1

.field public static final sSavedInboxItemsTabIndex:I = 0x1

.field private static final sTotalTabsNum:I = 0x2


# instance fields
.field private mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

.field private mListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 40
    return-void
.end method


# virtual methods
.method public getContentDescription(I)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 94
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 95
    .local v1, "context":Landroid/content/Context;
    const/4 v0, 0x0

    .line 96
    .local v0, "Res":Ljava/lang/CharSequence;
    packed-switch p1, :pswitch_data_0

    .line 104
    :goto_0
    return-object v0

    .line 98
    :pswitch_0
    const v2, 0x7f080110

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 99
    goto :goto_0

    .line 101
    :pswitch_1
    const v2, 0x7f0801bd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x2

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 44
    const/4 v0, 0x0

    .line 46
    .local v0, "fragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    packed-switch p1, :pswitch_data_0

    .line 57
    :goto_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->setActionListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;)V

    .line 58
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->mListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->setListListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;)V

    .line 59
    return-object v0

    .line 49
    :pswitch_0
    const/4 v1, 0x1

    invoke-static {v1, v3, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->newInstance(ILjava/lang/String;Z)Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    move-result-object v0

    .line 50
    goto :goto_0

    .line 53
    :pswitch_1
    const/4 v1, 0x3

    invoke-static {v1, v3, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->newInstance(ILjava/lang/String;Z)Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    move-result-object v0

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 79
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 80
    .local v1, "context":Landroid/content/Context;
    const/4 v0, 0x0

    .line 81
    .local v0, "Res":Ljava/lang/CharSequence;
    packed-switch p1, :pswitch_data_0

    .line 89
    :goto_0
    return-object v0

    .line 83
    :pswitch_0
    const v2, 0x7f080111

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    goto :goto_0

    .line 86
    :pswitch_1
    const v2, 0x7f0801be

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/VVMFragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    .local v1, "itemObj":Ljava/lang/Object;
    move-object v0, v1

    .line 65
    check-cast v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    .line 66
    .local v0, "inboxFragmentObj":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    invoke-virtual {v0, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->setActionListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;)V

    .line 67
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->mListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    invoke-virtual {v0, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->setListListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;)V

    .line 68
    return-object v1
.end method

.method public refreshList(Z)V
    .locals 6
    .param p1, "groupedByContact"    # Z

    .prologue
    .line 138
    sget-object v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "refreshList groupedByContact="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->getFragments()Ljava/util/List;

    move-result-object v1

    .line 141
    .local v1, "fragmentList":Ljava/util/List;, "Ljava/util/List<Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;>;"
    sget-object v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "refreshList fragmentList size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 143
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    .line 144
    .local v0, "fragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->refreshList(Z)V

    .line 142
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 148
    .end local v0    # "fragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    :cond_1
    return-void
.end method

.method public setActionListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;)V
    .locals 2
    .param p1, "actionListener"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    .prologue
    .line 152
    sget-object v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->TAG:Ljava/lang/String;

    const-string v1, "setActionListener"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->mActionListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;

    .line 155
    return-void
.end method

.method public setListListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;)V
    .locals 0
    .param p1, "listListener"    # Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->mListListener:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;

    .line 160
    return-void
.end method

.method public startGauge()V
    .locals 5

    .prologue
    .line 110
    sget-object v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->TAG:Ljava/lang/String;

    const-string v4, "startGauge"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->getFragments()Ljava/util/List;

    move-result-object v1

    .line 113
    .local v1, "fragmentList":Ljava/util/List;, "Ljava/util/List<Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 115
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    .line 116
    .local v0, "fragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->startLoadingProgressBar()V

    .line 113
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 120
    .end local v0    # "fragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    :cond_1
    return-void
.end method

.method public stopGauge()V
    .locals 5

    .prologue
    .line 124
    sget-object v3, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->TAG:Ljava/lang/String;

    const-string v4, "startGauge"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->getFragments()Ljava/util/List;

    move-result-object v1

    .line 127
    .local v1, "fragmentList":Ljava/util/List;, "Ljava/util/List<Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 129
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    .line 130
    .local v0, "fragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->stopLoadingProgressBar()V

    .line 127
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 134
    .end local v0    # "fragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    :cond_1
    return-void
.end method

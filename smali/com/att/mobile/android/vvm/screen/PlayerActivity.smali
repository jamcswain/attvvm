.class public Lcom/att/mobile/android/vvm/screen/PlayerActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "PlayerActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/PlayerActivity$IntentExtraNames;,
        Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/att/mobile/android/vvm/screen/VVMActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/media/AudioManager$OnAudioFocusChangeListener;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PlayerActivity"

.field private static final URL_LOADER:I


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field private contactUri:Ljava/lang/String;

.field private contactsObserver:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

.field private cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

.field private displayName:Ljava/lang/String;

.field private field:I

.field private filterType:I

.field private goToInboxCalled:Z

.field private isAutoPlayMode:Z

.field private isProximityHeld:Z

.field private mCurrentPhoneNumber:Ljava/lang/String;

.field private mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private mProximity:Landroid/hardware/Sensor;

.field private mSensorEventListener:Landroid/hardware/SensorEventListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mesCursor:Landroid/database/Cursor;

.field private mespos:I

.field private message:Lcom/att/mobile/android/vvm/model/Message;

.field private messageId:J

.field private powerManager:Landroid/os/PowerManager;

.field private screenLaunchedByInbox:Z

.field private viewPager:Landroid/support/v4/view/ViewPager;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;

.field private wasSpeakerSwaped:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 51
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->contactUri:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->displayName:Ljava/lang/String;

    .line 58
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->isAutoPlayMode:Z

    .line 59
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->goToInboxCalled:Z

    .line 76
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->screenLaunchedByInbox:Z

    .line 181
    const/16 v0, 0x20

    iput v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->field:I

    .line 182
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->isProximityHeld:Z

    .line 183
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->wasSpeakerSwaped:Z

    .line 186
    new-instance v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->isProximityHeld:Z

    return v0
.end method

.method static synthetic access$002(Lcom/att/mobile/android/vvm/screen/PlayerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->isProximityHeld:Z

    return p1
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->wasSpeakerSwaped:Z

    return v0
.end method

.method static synthetic access$102(Lcom/att/mobile/android/vvm/screen/PlayerActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->wasSpeakerSwaped:Z

    return p1
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/screen/PlayerActivity;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->refreshCursorData(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$500(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private getMesCursor(ILcom/att/mobile/android/vvm/model/Message;)Landroid/database/Cursor;
    .locals 4
    .param p1, "filterType"    # I
    .param p2, "currmessage"    # Lcom/att/mobile/android/vvm/model/Message;

    .prologue
    .line 315
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isGroupByContact()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 316
    .local v0, "isGroupByContact":Z
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    .line 317
    .local v2, "messageByPhone":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3, p1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAllMessagesFromInbox(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 318
    .local v1, "mesCursor":Landroid/database/Cursor;
    return-object v1

    .line 316
    .end local v1    # "mesCursor":Landroid/database/Cursor;
    .end local v2    # "messageByPhone":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getMesCursor(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p1, "filterType"    # I
    .param p2, "messageByPhone"    # Ljava/lang/String;

    .prologue
    .line 322
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1, p1, p2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAllMessagesFromInbox(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 323
    .local v0, "mesCursor":Landroid/database/Cursor;
    return-object v0
.end method

.method private goToInbox()V
    .locals 3

    .prologue
    .line 412
    const-string v1, "PlayerActivity"

    const-string v2, "goToInbox()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->screenLaunchedByInbox:Z

    if-nez v1, :cond_1

    .line 418
    monitor-enter p0

    .line 420
    :try_start_0
    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->goToInboxCalled:Z

    if-nez v1, :cond_0

    .line 421
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->goToInboxCalled:Z

    .line 423
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 424
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 425
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->startActivity(Landroid/content/Intent;)V

    .line 427
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 429
    :cond_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->finish()V

    .line 431
    const-string v1, "PlayerActivity"

    const-string v2, "goToInbox() ended"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    return-void

    .line 427
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private loadIntentExtras(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v6, -0x1

    .line 286
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 287
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 288
    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    .line 289
    const-string v1, "filterType"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    .line 290
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    if-gez v1, :cond_0

    iget-wide v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    cmp-long v1, v4, v8

    if-lez v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    iget-wide v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    invoke-virtual {v1, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessageSaved(J)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    :goto_0
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    .line 293
    :cond_0
    const-string v1, "contactUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->contactUri:Ljava/lang/String;

    .line 294
    const-string v1, "senderDisplayName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->displayName:Ljava/lang/String;

    .line 295
    const-string v1, "messagePosition"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    .line 297
    const-string v1, "phoneNumber"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    .line 298
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    :goto_1
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    .line 299
    const-string v1, "isAutoPlayMode"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->isAutoPlayMode:Z

    .line 300
    const-string v1, "launchedFromInbox"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->screenLaunchedByInbox:Z

    .line 303
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 291
    goto :goto_0

    .line 298
    :cond_3
    iget-wide v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    cmp-long v1, v4, v8

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    iget-wide v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    invoke-virtual {v1, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSenderPhoneNumber(J)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    move-object v1, v3

    goto :goto_1
.end method

.method private refreshCursorData(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "data"    # Landroid/database/Cursor;

    .prologue
    .line 129
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    if-gez v1, :cond_0

    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->screenLaunchedByInbox:Z

    if-nez v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    invoke-virtual {v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessageSaved(J)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    :goto_0
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    .line 131
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getMesCursor(ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    .line 132
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessagePosition(JILjava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    .line 133
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    if-nez v1, :cond_4

    .line 136
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 137
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    new-instance v1, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->isAutoPlayMode:Z

    invoke-direct {v1, v0, p1, v2}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;Z)V

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    .line 138
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 139
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 144
    .end local v0    # "fm":Landroid/support/v4/app/FragmentManager;
    :goto_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createMessageFromCursor(Landroid/database/Cursor;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 145
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->displayName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 146
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/Message;->setSenderDisplayName(Ljava/lang/String;)V

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->contactUri:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 149
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->contactUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/Message;->setContactImageUri(Landroid/net/Uri;)V

    .line 151
    :cond_2
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 152
    return-void

    .line 130
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 141
    :cond_4
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_1
.end method


# virtual methods
.method public getCursoradapter()Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    return-object v0
.end method

.method protected goToGotItScreen()V
    .locals 2

    .prologue
    .line 514
    const-string v0, "PlayerActivity"

    const-string v1, "goToGotItScreen"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 516
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->finish()V

    .line 518
    :cond_1
    return-void
.end method

.method public onAudioFocusChange(I)V
    .locals 0
    .param p1, "focusChange"    # I

    .prologue
    .line 83
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->unregisterProximityListener()V

    .line 394
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->screenLaunchedByInbox:Z

    if-nez v0, :cond_0

    .line 395
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->goToInbox()V

    .line 399
    :goto_0
    return-void

    .line 397
    :cond_0
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 250
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 251
    const v2, 0x7f040051

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->setContentView(I)V

    .line 253
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 255
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 261
    .local v1, "intent":Landroid/content/Intent;
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    .line 263
    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->loadIntentExtras(Landroid/content/Intent;)V

    .line 265
    const v2, 0x7f0f0138

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 268
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 269
    const-string v2, "sensor"

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 270
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mProximity:Landroid/hardware/Sensor;

    .line 272
    :try_start_0
    const-class v2, Landroid/os/PowerManager;

    const-string v3, "PROXIMITY_SCREEN_OFF_WAKE_LOCK"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    iput v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->field:I

    .line 273
    const-string v2, "PlayerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate()  hidden field PROXIMITY_SCREEN_OFF_WAKE_LOCK = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->field:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :goto_0
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->powerManager:Landroid/os/PowerManager;

    .line 279
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->powerManager:Landroid/os/PowerManager;

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->field:I

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 280
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->audioManager:Landroid/media/AudioManager;

    .line 282
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 283
    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 275
    .local v0, "ignored":Ljava/lang/Throwable;
    const-string v2, "PlayerActivity"

    const-string v3, "onCreate() Exception while getting hidden field PROXIMITY_SCREEN_OFF_WAKE_LOCK"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8
    .param p1, "loaderID"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 104
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isGroupByContact()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v7, v0

    .line 105
    .local v7, "isGroupAndAggreg":Z
    :goto_0
    if-eqz v7, :cond_2

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    if-ne v2, v3, :cond_1

    sget-object v4, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_PHONE_NUMBER_AND_SAVED:Ljava/lang/String;

    .line 107
    .local v4, "where":Ljava/lang/String;
    :goto_1
    if-eqz v7, :cond_4

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    .line 110
    .local v5, "selectArgs":[Ljava/lang/String;
    :goto_2
    const-string v0, "PlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateLoader filterType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " where="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    sget-object v2, Lcom/att/mobile/android/vvm/model/db/VmContentProvider;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/att/mobile/android/vvm/model/db/ModelManager;->COLUMNS_VM_LIST:[Ljava/lang/String;

    const-string v6, "time desc"

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .end local v4    # "where":Ljava/lang/String;
    .end local v5    # "selectArgs":[Ljava/lang/String;
    .end local v7    # "isGroupAndAggreg":Z
    :cond_0
    move v7, v1

    .line 104
    goto :goto_0

    .line 105
    .restart local v7    # "isGroupAndAggreg":Z
    :cond_1
    sget-object v4, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_PHONE_NUMBER_AND_SAVED_NOT:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    if-ne v2, v3, :cond_3

    sget-object v4, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_SAVED:Ljava/lang/String;

    goto :goto_1

    :cond_3
    sget-object v4, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_SAVED_NOT:Ljava/lang/String;

    goto :goto_1

    .line 107
    .restart local v4    # "where":Ljava/lang/String;
    :cond_4
    new-array v5, v0, [Ljava/lang/String;

    .line 108
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->contactsObserver:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->contactsObserver:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    invoke-virtual {v0, p0, p0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->removeEventListener(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 373
    :cond_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->unregisterProximityListener()V

    .line 374
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 375
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 377
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    .line 379
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 380
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const-string v0, "PlayerActivity"

    const-string v1, "onLoadFinished"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-direct {p0, p2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->refreshCursorData(Landroid/database/Cursor;)V

    .line 126
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 159
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 403
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->loadIntentExtras(Landroid/content/Intent;)V

    .line 404
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 405
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 454
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->wasProximityHold()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v1, 0x4e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 457
    :cond_0
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onPause()V

    .line 458
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 307
    const-string v0, "PlayerActivity"

    const-string v1, "onPostCreate"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 309
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->handler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->createInstance(Landroid/os/Handler;)V

    .line 310
    invoke-static {}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->getInstance()Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->contactsObserver:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    .line 311
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->contactsObserver:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    invoke-virtual {v0, p0, p0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->addEventListener(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 312
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 384
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 386
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mustBackToSetupProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    :cond_0
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 10
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/16 v4, 0xa

    const/16 v3, 0x8

    const/4 v9, 0x6

    const/4 v8, 0x3

    .line 464
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->getCurrentMessage()Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v1

    .line 466
    .local v1, "currmessage":Lcom/att/mobile/android/vvm/model/Message;
    :goto_0
    if-eqz v1, :cond_9

    if-eq p1, v9, :cond_0

    if-eq p1, v8, :cond_0

    if-eq p1, v3, :cond_0

    if-ne p1, v4, :cond_9

    .line 468
    :cond_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->getCount()I

    move-result v0

    .line 469
    .local v0, "adapterCount":I
    :goto_1
    const-string v3, "PlayerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onUpdateListener()  currmessageID = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " messageId = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->messageId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " message.getRowId() = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " adapterCount = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "mespos = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    invoke-direct {p0, v2, v1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getMesCursor(ILcom/att/mobile/android/vvm/model/Message;)Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    .line 471
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_2

    :cond_1
    if-ne p1, v8, :cond_7

    .line 472
    :cond_2
    new-instance v2, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;

    invoke-direct {v2, p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerActivity;I)V

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 510
    .end local v0    # "adapterCount":I
    :goto_4
    return-void

    .line 464
    .end local v1    # "currmessage":Lcom/att/mobile/android/vvm/model/Message;
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 468
    .restart local v1    # "currmessage":Lcom/att/mobile/android/vvm/model/Message;
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 469
    .restart local v0    # "adapterCount":I
    :cond_5
    const-string v2, "null"

    goto :goto_2

    :cond_6
    const-string v2, "null"

    goto :goto_3

    .line 494
    :cond_7
    if-ne p1, v9, :cond_8

    .line 495
    new-instance v2, Lcom/att/mobile/android/vvm/screen/PlayerActivity$4;

    invoke-direct {v2, p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)V

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 502
    :cond_8
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->finish()V

    goto :goto_4

    .line 504
    .end local v0    # "adapterCount":I
    :cond_9
    if-nez v1, :cond_b

    if-eq p1, v9, :cond_a

    if-eq p1, v8, :cond_a

    if-eq p1, v3, :cond_a

    if-ne p1, v4, :cond_b

    .line 505
    :cond_a
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->finish()V

    goto :goto_4

    .line 508
    :cond_b
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    goto :goto_4
.end method

.method refreshAdapter(JI)V
    .locals 7
    .param p1, "messageId"    # J
    .param p3, "pos"    # I

    .prologue
    .line 331
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2, p1, p2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessage(J)Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createMessageFromCursor(Landroid/database/Cursor;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    .line 332
    const-string v1, "PlayerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "refreshAdapter() messageId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mesposition = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-direct {p0, v1, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->getMesCursor(ILcom/att/mobile/android/vvm/model/Message;)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    .line 334
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 335
    const-string v2, "PlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "refreshAdapter  messageId = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " message.getRowId() = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mesCursor count = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "mespos = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mModelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->message:Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v2

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->filterType:I

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mCurrentPhoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessagePosition(JILjava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    :goto_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    iget v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 342
    new-instance v1, Lcom/att/mobile/android/vvm/screen/PlayerActivity$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)V

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 349
    :cond_0
    return-void

    .line 335
    :cond_1
    const-string v1, "null"

    goto :goto_0

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/Exception;
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_2

    iget v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    :goto_2
    iput v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mespos:I

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mesCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_2
.end method

.method protected refreshContacts()V
    .locals 2

    .prologue
    .line 446
    const-string v0, "PlayerActivity"

    const-string v1, "refreshContacts()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->cursoradapter:Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/CursorPagerAdapter;->notifyDataSetChanged()V

    .line 450
    :cond_0
    return-void
.end method

.method registerProximityListener()V
    .locals 4

    .prologue
    .line 352
    const-string v0, "PlayerActivity"

    const-string v1, "registerProximityListener"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mProximity:Landroid/hardware/Sensor;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 354
    return-void
.end method

.method swapAudioSource()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 233
    const-string v0, "PlayerActivity"

    const-string v2, "swapAudioSource()"

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->setApplicationAudioMode()V

    .line 237
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v2

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/att/mobile/android/vvm/VVMApplication;->setIsApplicationSpeakerOn(Z)V

    .line 238
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 239
    const-string v0, "PlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "swapAudioSource() ended. audioManager.isSpeakerphoneOn() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    return-void

    :cond_1
    move v0, v1

    .line 237
    goto :goto_0
.end method

.method switchPage(I)V
    .locals 1
    .param p1, "nPage"    # I

    .prologue
    .line 327
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 328
    return-void
.end method

.method unregisterProximityListener()V
    .locals 2

    .prologue
    .line 357
    const-string v0, "PlayerActivity"

    const-string v1, "unregisterProximityListener"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->mSensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 359
    return-void
.end method

.method public updateMessageToMarkAsReadStatus(J)V
    .locals 3
    .param p1, "uid"    # J

    .prologue
    .line 437
    const-string v0, "PlayerActivity"

    const-string v1, "updateMessageToMarkAsReadStatus()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    new-instance v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerActivity;J)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity$RunUpdateReadAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 439
    sget-object v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->updateNotification()V

    .line 441
    const-string v0, "PlayerActivity"

    const-string v1, "updateMessageToMarkAsReadStatus() ended"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    return-void
.end method

.method wasProximityHold()Z
    .locals 1

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->isProximityHeld:Z

    return v0
.end method

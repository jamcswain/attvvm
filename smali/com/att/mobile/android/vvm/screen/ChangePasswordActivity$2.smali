.class Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;
.super Ljava/lang/Object;
.source "ChangePasswordActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 126
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 121
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const v7, 0x7f020117

    const v6, 0x7f080167

    const/4 v2, 0x0

    .line 93
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v1

    .line 94
    .local v1, "enterPasswordLength":I
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$000(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v0

    .line 95
    .local v0, "confirmPasswordLength":I
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget v3, v3, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->minDigit:I

    if-ge v1, v3, :cond_0

    .line 96
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    const v4, 0x7f080163

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v4, v4, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setError(Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 117
    :goto_0
    return-void

    .line 99
    :cond_0
    if-le v1, v0, :cond_2

    .line 100
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$000(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 101
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v3, v6}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setError(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 103
    :cond_1
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 104
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->clearError(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 106
    :cond_2
    if-ge v1, v0, :cond_3

    .line 107
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->btnContinue:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 108
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v3, v6}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setError(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 110
    :cond_3
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$000(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v5, v5, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    invoke-virtual {v3, v4, v5}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->isConfirmPasswordValid(Landroid/widget/EditText;Landroid/widget/EditText;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 111
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 112
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v5, v5, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget v6, v6, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->minDigit:I

    invoke-virtual {v4, v5, v6}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->isPasswordValid(Landroid/widget/EditText;I)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$000(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget v5, v5, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->minDigit:I

    if-lt v4, v5, :cond_4

    const/4 v2, 0x1

    :cond_4
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->clearError(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 114
    :cond_5
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->btnContinue:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 115
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v3, v6}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;->this$0:Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setError(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto/16 :goto_0
.end method

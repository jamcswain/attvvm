.class Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;
.super Ljava/lang/Object;
.source "SetupCompleteActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->onUpdateListener(ILjava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$eventId:I


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;ILandroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    iput p2, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->val$eventId:I

    iput-object p3, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 191
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->access$300(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->val$eventId:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 205
    :sswitch_0
    const-string v1, "SetupCompleteActivity"

    const-string v2, "SetupCompleteActivity.onUpdateListener() GET_METADATA_GREETING_DETAILS_FINISHED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMetadata()Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 209
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v1

    .line 210
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueGetExistingGreetingsOperation()V

    goto :goto_0

    .line 198
    :sswitch_1
    const-string v1, "SetupCompleteActivity"

    const-string v2, "SetupCompleteActivity.onUpdateListener() GET_METADATA_GREETING_FAILED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->access$400(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V

    goto :goto_0

    .line 216
    :sswitch_2
    const-string v1, "SetupCompleteActivity"

    const-string v2, "SetupCompleteActivity.onUpdateListener() GET_METADATA_EXISTING_GREETINGS_FINISHED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->access$400(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V

    .line 220
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v2

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->val$context:Landroid/content/Context;

    check-cast v1, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    invoke-virtual {v2, v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 223
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->val$context:Landroid/content/Context;

    const-class v2, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 225
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "NEXT_INTENT"

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->val$context:Landroid/content/Context;

    const-class v4, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 226
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 230
    .end local v0    # "i":Landroid/content/Intent;
    :sswitch_3
    const-string v1, "SetupCompleteActivity"

    const-string v2, "SetupCompleteActivity.onUpdateListener() LOGIN_FAILED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->access$400(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V

    .line 233
    const v1, 0x7f080099

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0

    .line 195
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_2
        0x1a -> :sswitch_1
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

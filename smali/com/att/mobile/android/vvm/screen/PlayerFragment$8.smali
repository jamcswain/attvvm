.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$8;
.super Ljava/lang/Object;
.source "PlayerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initButtonsListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 1251
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$8;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1253
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$8;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->swapAudioSource()V

    .line 1254
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$8;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->refreshSpeakerButton()V

    .line 1255
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/att/mobile/android/infra/utils/AccessibilityUtils;->sendEvent(Ljava/lang/String;Landroid/view/View;)V

    .line 1256
    return-void
.end method

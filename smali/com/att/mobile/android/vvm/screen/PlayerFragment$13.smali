.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;
.super Ljava/lang/Object;
.source "PlayerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;->increaseButtonHitArea(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

.field final synthetic val$btn:Landroid/view/View;

.field final synthetic val$parent:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 1294
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;->val$btn:Landroid/view/View;

    iput-object p3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;->val$parent:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1296
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1297
    .local v0, "rect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;->val$btn:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1298
    iget v1, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v1, -0x32

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1299
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x32

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1300
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v1, v1, 0x32

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1301
    iget v1, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v1, v1, 0x32

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1302
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;->val$parent:Landroid/view/View;

    new-instance v2, Landroid/view/TouchDelegate;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$13;->val$btn:Landroid/view/View;

    invoke-direct {v2, v0, v3}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 1303
    return-void
.end method

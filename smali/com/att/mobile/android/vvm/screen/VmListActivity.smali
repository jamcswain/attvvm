.class public Lcom/att/mobile/android/vvm/screen/VmListActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "VmListActivity.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;
.implements Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;,
        Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;
    }
.end annotation


# static fields
.field protected static final PREFERENCE_REQUEST_CODE:I = 0xa

.field private static final TAG:Ljava/lang/String; = "VmListActivity"


# instance fields
.field protected isCreate:Z

.field private isVVMOVerWifiShowing:Z

.field protected modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private refreshOperation:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

.field protected telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isCreate:Z

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/VmListActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/VmListActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isSavedTab()Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/att/mobile/android/vvm/screen/VmListActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/VmListActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isVVMOVerWifiShowing:Z

    return p1
.end method

.method private isSavedTab()Z
    .locals 2

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getCurrentFilterType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showOverWifiPopup()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    const-string v0, "VmListActivity"

    const-string v1, "InboxActivity.showOverWifiPopup() - context is not available "

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isVVMOVerWifiShowing:Z

    if-nez v0, :cond_0

    .line 183
    iput-boolean v5, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isVVMOVerWifiShowing:Z

    .line 184
    const v2, 0x7f0801fa

    const v3, 0x7f080151

    new-instance v6, Lcom/att/mobile/android/vvm/screen/VmListActivity$1;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;)V

    move-object v0, p0

    move v4, v1

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    goto :goto_0
.end method


# virtual methods
.method protected closeEditMode()V
    .locals 0

    .prologue
    .line 442
    return-void
.end method

.method public getCurrentFilterType()I
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method protected initInboxActionBar(Ljava/lang/String;ZZLcom/att/mobile/android/infra/utils/FontUtils$FontNames;)V
    .locals 4
    .param p1, "headerStr"    # Ljava/lang/String;
    .param p2, "shouldDisplayBackOption"    # Z
    .param p3, "playAllEnabled"    # Z
    .param p4, "fontNames"    # Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    .prologue
    .line 275
    const-string v1, "VmListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initActionBar shouldDisplayBackOption="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    const v1, 0x7f0f010c

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 277
    const v1, 0x7f0f00e9

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarTV:Landroid/widget/TextView;

    .line 278
    const v1, 0x7f0f010e

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    .line 279
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarTV:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarTV:Landroid/widget/TextView;

    invoke-static {p4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 282
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarTV:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 284
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 285
    .local v0, "supportActionBar":Landroid/support/v7/app/ActionBar;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 287
    invoke-virtual {v0, p2}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 288
    invoke-virtual {v0, p2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 292
    .end local v0    # "supportActionBar":Landroid/support/v7/app/ActionBar;
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 293
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    if-eqz p3, :cond_2

    const v1, 0x7f080182

    :goto_0
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 296
    :cond_1
    return-void

    .line 293
    :cond_2
    const v1, 0x7f080183

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 74
    invoke-super {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 75
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 134
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onBackPressed()V

    .line 135
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isCreate:Z

    .line 105
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 107
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 113
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 231
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f100001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 232
    const/4 v1, 0x1

    return v1
.end method

.method public onDeleteAction([Ljava/lang/Long;)V
    .locals 0
    .param p1, "ids"    # [Ljava/lang/Long;

    .prologue
    .line 379
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->showDeleteDialog([Ljava/lang/Long;)V

    .line 380
    return-void
.end method

.method public onDeleteAction([Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumbers"    # [Ljava/lang/String;

    .prologue
    .line 394
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->showDeleteDialog([Ljava/lang/String;)V

    .line 395
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 130
    return-void
.end method

.method public onListUpdated(II)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "size"    # I

    .prologue
    .line 447
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 238
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 249
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 240
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    .local v0, "intent":Landroid/content/Intent;
    const/16 v2, 0xa

    invoke-virtual {p0, v0, v2}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 245
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->refreshInbox()V

    goto :goto_0

    .line 238
    :pswitch_data_0
    .packed-switch 0x7f0f0185
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 154
    const-string v0, "VmListActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 159
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onPause()V

    .line 160
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 124
    const-string v1, "isRestart"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 125
    .local v0, "isRestart":Z
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 140
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isCreate:Z

    if-nez v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->refreshUi()V

    .line 144
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->isCreate:Z

    .line 147
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 148
    return-void
.end method

.method public onSaveAction([Ljava/lang/Long;)V
    .locals 1
    .param p1, "ids"    # [Ljava/lang/Long;

    .prologue
    .line 384
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->markMessagesAs([Ljava/lang/Long;I)V

    .line 385
    return-void
.end method

.method public onSaveAction([Ljava/lang/String;)V
    .locals 2
    .param p1, "phoneNumbers"    # [Ljava/lang/String;

    .prologue
    .line 399
    new-instance v0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;

    const/4 v1, 0x4

    invoke-direct {v0, p0, p1, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;-><init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/String;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 400
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 118
    const-string v0, "isRestart"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 119
    return-void
.end method

.method public onToolbarPlayAllClick(Landroid/view/View;)V
    .locals 4
    .param p1, "buttonView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 205
    const-string v1, "VmListActivity"

    const-string v2, "onToolbarPlayAllClick"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 211
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "filterType"

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getCurrentFilterType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 212
    const-string v1, "launchedFromInbox"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 213
    const-string v1, "isAutoPlayMode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 214
    const-string v1, "phoneNumber"

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 218
    return-void
.end method

.method public onUnsaveAction([Ljava/lang/Long;)V
    .locals 1
    .param p1, "ids"    # [Ljava/lang/Long;

    .prologue
    .line 389
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->markMessagesAs([Ljava/lang/Long;I)V

    .line 390
    return-void
.end method

.method public onUnsaveAction([Ljava/lang/String;)V
    .locals 2
    .param p1, "phoneNumbers"    # [Ljava/lang/String;

    .prologue
    .line 404
    new-instance v0, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;

    const/4 v1, 0x2

    invoke-direct {v0, p0, p1, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;-><init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/String;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$MarkMessageAsByPhoneNumbersAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 405
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 6
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 410
    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    .line 411
    const-string v1, "VmListActivity"

    const-string v2, "onUpdateListener() MARK_AS_DELETED_FINISHED/MESSAGE_MARKED_AS_SAVED "

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 415
    .local v0, "isMulipleMessages":Z
    :cond_0
    new-instance v1, Lcom/att/mobile/android/vvm/screen/VmListActivity$4;

    invoke-direct {v1, p0, v0}, Lcom/att/mobile/android/vvm/screen/VmListActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;Z)V

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 434
    .end local v0    # "isMulipleMessages":Z
    :goto_0
    return-void

    .line 424
    :cond_1
    const/4 v1, 0x6

    if-ne p1, v1, :cond_2

    .line 425
    new-instance v1, Lcom/att/mobile/android/vvm/screen/VmListActivity$5;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity$5;-><init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;)V

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 433
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method protected refreshInbox()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 258
    const-string v0, "VmListActivity"

    const-string v2, "InboxActivity.refreshInbox()"

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-static {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimManager;->validateSim()Lcom/att/mobile/android/infra/sim/SimValidationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->isSimPresentAndReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    const-string v0, "VmListActivity"

    const-string v2, "InboxActivity.refreshInbox(): no sim card / sim not ready. Cannot refresh inbox."

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const v2, 0x7f08014d

    const v3, 0x7f08014e

    const/4 v6, 0x0

    move-object v0, p0

    move v4, v1

    move v5, v1

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 271
    :goto_0
    return-void

    .line 266
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 267
    const v0, 0x7f080145

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    .line 269
    :cond_1
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueFetchHeadersAndBodiesOperation()V

    goto :goto_0
.end method

.method protected refreshUi()V
    .locals 0

    .prologue
    .line 438
    return-void
.end method

.method protected setToolBarPlayAllBtn(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    const v2, 0x7f0200ec

    .line 300
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 301
    const/4 p1, 0x0

    .line 303
    :cond_0
    if-eqz p1, :cond_1

    .line 304
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 305
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    const v1, 0x7f080182

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 314
    :goto_0
    return-void

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 310
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    const v1, 0x7f080183

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity;->toolBarPlayImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    goto :goto_0
.end method

.method protected showDeleteDialog([Ljava/lang/Long;)V
    .locals 6
    .param p1, "ids"    # [Ljava/lang/Long;

    .prologue
    .line 319
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v3, 0x1

    if-le v0, v3, :cond_0

    .line 320
    const v1, 0x7f0800e2

    .line 321
    .local v1, "titleID":I
    const v2, 0x7f0800df

    .line 327
    .local v2, "bodyId":I
    :goto_0
    const v3, 0x7f0800dd

    const v4, 0x7f0800b7

    new-instance v5, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;

    invoke-direct {v5, p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/Long;)V

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showRightAlignedDialog(Landroid/content/Context;IIIILcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 341
    return-void

    .line 323
    .end local v1    # "titleID":I
    .end local v2    # "bodyId":I
    :cond_0
    const v1, 0x7f0800e1

    .line 324
    .restart local v1    # "titleID":I
    const v2, 0x7f0800de

    .restart local v2    # "bodyId":I
    goto :goto_0
.end method

.method protected showDeleteDialog([Ljava/lang/String;)V
    .locals 6
    .param p1, "phoneNumbers"    # [Ljava/lang/String;

    .prologue
    .line 348
    const v1, 0x7f0800e2

    .line 349
    .local v1, "titleID":I
    const v2, 0x7f0800df

    .line 355
    .local v2, "bodyId":I
    const v3, 0x7f0800dd

    const v4, 0x7f0800b7

    new-instance v5, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;

    invoke-direct {v5, p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/String;)V

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showRightAlignedDialog(Landroid/content/Context;IIIILcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 370
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "NonAdminUserActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NonAdminUserActivity"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    return-void
.end method

.method private setContectDescription()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v2, 0x0

    const v1, 0x7f0f004d

    .line 71
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 72
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 73
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 74
    return-void
.end method


# virtual methods
.method public initTitleFontAndSize()V
    .locals 6

    .prologue
    const v5, 0x7f090005

    const/4 v4, 0x0

    const v3, 0x3fa66666    # 1.3f

    .line 51
    const v2, 0x7f0f0094

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 52
    .local v0, "title_att":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 53
    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_RegularItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 54
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 59
    :cond_0
    const v2, 0x7f0f0095

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 60
    .local v1, "title_visual":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 61
    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 62
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 63
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 66
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->setContectDescription()V

    .line 67
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    const-string v0, "NonAdminUserActivity"

    const-string v1, "NonAdminUserActivity::onCreate"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 24
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const v0, 0x7f040047

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->setContentView(I)V

    .line 27
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 31
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 32
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->initTitleFontAndSize()V

    .line 34
    const v1, 0x7f0f0126

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 35
    const v1, 0x7f0f0127

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 38
    const v1, 0x7f0f0128

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 39
    .local v0, "exitButton":Landroid/widget/Button;
    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 40
    new-instance v1, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/NonAdminUserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method

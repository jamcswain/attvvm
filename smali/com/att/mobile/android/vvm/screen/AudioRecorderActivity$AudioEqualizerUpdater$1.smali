.class Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;
.super Ljava/lang/Object;
.source "AudioRecorderActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 244
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$300(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 246
    monitor-enter p0

    .line 253
    :try_start_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$400(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)I

    move-result v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$500(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)[[I

    move-result-object v3

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 255
    const-string v2, "AudioEqualizerUpdater.run()"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current equalizer values position is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    .line 256
    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$400(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 255
    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$500(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)[[I

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$400(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)I

    move-result v3

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    .line 261
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$600(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Lcom/att/mobile/android/vvm/screen/DotMeterView;

    move-result-object v3

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$500(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)[[I

    move-result-object v2

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$400(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)I

    move-result v4

    aget-object v2, v2, v4

    .line 262
    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    .line 261
    invoke-virtual {v3, v2}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->setEqualizerValues([I)V

    .line 264
    :cond_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$404(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)I

    .line 266
    :cond_1
    monitor-exit p0

    .line 284
    :cond_2
    :goto_0
    return-void

    .line 266
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 271
    :cond_3
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$700(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Lcom/att/mobile/android/infra/media/AudioRecorder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/media/AudioRecorder;->getMaxAmplitude()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x44800000    # 1024.0f

    div-float/2addr v2, v3

    float-to-int v0, v2

    .line 273
    .local v0, "equalizerValue":I
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$600(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Lcom/att/mobile/android/vvm/screen/DotMeterView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/att/mobile/android/vvm/screen/DotMeterView;->setEqualizerValues(I)[I

    move-result-object v1

    .line 276
    .local v1, "recordingEqualizerValues":[I
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater$1;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;->access$800(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioEqualizerUpdater;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 280
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$900()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

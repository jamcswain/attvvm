.class public Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;
.super Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;
.source "EnterExistingPasswordActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field final enterPasswordTextWatcher:Landroid/text/TextWatcher;

.field passwordChangeRequiredStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;-><init>()V

    .line 50
    new-instance v0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->enterPasswordTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method


# virtual methods
.method protected changePassword(Ljava/lang/String;)V
    .locals 1
    .param p1, "pass"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPassword(Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueGetPasswordLengthOperation()V

    .line 76
    return-void
.end method

.method initConfirmPassword()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    const v1, 0x7f0800f3

    const v2, 0x7f08015f

    const v3, 0x7f080152

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$5;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$5;-><init>(Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 187
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f04003a

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->initUI(I)V

    .line 33
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/VVMApplication;

    check-cast v0, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->clearNotification()V

    .line 35
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getPasswordChangeRequiredStatus()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->passwordChangeRequiredStatus:I

    .line 37
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->setSoftKeyboardVisibility(Z)V

    .line 88
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->onPause()V

    .line 90
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->setSoftKeyboardVisibility(Z)V

    .line 81
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->onResume()V

    .line 83
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 3
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    sget-object v0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateListener eventId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    sparse-switch p1, :sswitch_data_0

    .line 168
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 107
    :sswitch_0
    new-instance v0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$2;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 128
    :sswitch_1
    new-instance v0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$3;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 148
    :sswitch_2
    new-instance v0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$4;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 162
    :sswitch_3
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    .line 163
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    goto :goto_0

    .line 104
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
        0x37 -> :sswitch_2
        0x40 -> :sswitch_3
    .end sparse-switch
.end method

.method setListeners()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->enterPasswordTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 43
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/VmListActivity$3;
.super Ljava/lang/Object;
.source "VmListActivity.java"

# interfaces
.implements Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/VmListActivity;->showDeleteDialog([Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

.field final synthetic val$phoneNumbers:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VmListActivity;

    .prologue
    .line 355
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;->val$phoneNumbers:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleNegativeButton(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 368
    return-void
.end method

.method public handlePositiveButton(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 358
    const-string v0, "VmListActivity"

    const-string v1, "onDeleteAction"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;->val$phoneNumbers:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;->val$phoneNumbers:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 360
    const-string v0, "VmListActivity"

    const-string v1, "deleteVMs()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    new-instance v0, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;->val$phoneNumbers:[Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;-><init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 362
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$3;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->closeEditMode()V

    .line 364
    :cond_0
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "SetupCompleteActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SetupCompleteActivity"

.field private static greetingsGauge:Landroid/app/ProgressDialog;


# instance fields
.field private btnNext:Landroid/widget/Button;

.field private final buttonClickListener:Landroid/view/View$OnClickListener;

.field private isCanceled:Ljava/lang/Boolean;

.field private personalGreeting:Landroid/widget/TextView;

.field private whatIsLayout:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->greetingsGauge:Landroid/app/ProgressDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->personalGreeting:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->whatIsLayout:Landroid/view/View;

    .line 39
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->btnNext:Landroid/widget/Button;

    .line 41
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->isCanceled:Ljava/lang/Boolean;

    .line 47
    new-instance v0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->buttonClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->btnNext:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->addShortcut()V

    return-void
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->getGreetingsDetails()V

    return-void
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->isCanceled:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$302(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->isCanceled:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->stopGauge()V

    return-void
.end method

.method private addShortcut()V
    .locals 5

    .prologue
    .line 249
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 250
    .local v1, "shortcutIntent":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 251
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 257
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 258
    const-string v2, "android.intent.extra.shortcut.NAME"

    const v3, 0x7f0800a3

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    const v3, 0x7f0200fe

    invoke-static {p0, v3}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 261
    const-string v2, "duplicate"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 263
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 264
    return-void
.end method

.method private getGreetingsDetails()V
    .locals 3

    .prologue
    .line 118
    const-string v0, "SetupCompleteActivity"

    const-string v1, "SetupCompleteActivity.getGreetingsDetails()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->startGauge()V

    .line 126
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueGetGreetingsDetailsOperation()V

    .line 135
    :goto_0
    return-void

    .line 128
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 129
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 131
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080147

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    .line 132
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private startGauge()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 141
    const-string v0, "SetupCompleteActivity"

    const-string v1, "SetupCompleteActivity.startGauge"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v0, 0x0

    const v1, 0x7f080186

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1, v3, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->greetingsGauge:Landroid/app/ProgressDialog;

    .line 143
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->isCanceled:Ljava/lang/Boolean;

    .line 144
    sget-object v0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->greetingsGauge:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 145
    sget-object v0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->greetingsGauge:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$3;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 155
    return-void
.end method

.method private stopGauge()V
    .locals 3

    .prologue
    .line 161
    const-string v1, "SetupCompleteActivity"

    const-string v2, "SetupCompleteActivity.stopGauge"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    sget-object v1, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->greetingsGauge:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->greetingsGauge:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    :try_start_0
    sget-object v1, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->greetingsGauge:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->greetingsGauge:Landroid/app/ProgressDialog;

    .line 170
    :cond_0
    return-void

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SetupCompleteActivity"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 174
    const-string v0, "SetupCompleteActivity"

    const-string v1, "onBackPressed()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const/16 v0, 0x3e

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->setResult(I)V

    .line 176
    invoke-static {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->exit(Landroid/app/Activity;)V

    .line 177
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->finish()V

    .line 178
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v2, 0x7f040029

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->setContentView(I)V

    .line 67
    const v2, 0x1020002

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    const v4, 0x7f080053

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 70
    const v2, 0x7f0f00c4

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->btnNext:Landroid/widget/Button;

    .line 71
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->btnNext:Landroid/widget/Button;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->buttonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMailBoxStatus()Ljava/lang/String;

    move-result-object v2

    const-string v4, "I"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 75
    .local v0, "isExistingMailBox":Z
    :goto_0
    const v2, 0x7f0f00c2

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->personalGreeting:Landroid/widget/TextView;

    .line 76
    const v2, 0x7f0f00c0

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->whatIsLayout:Landroid/view/View;

    .line 77
    const v2, 0x7f0f00bf

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 78
    .local v1, "setGreetings":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 79
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 85
    :goto_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->whatIsLayout:Landroid/view/View;

    new-instance v3, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$2;

    invoke-direct {v3, p0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void

    .end local v0    # "isExistingMailBox":Z
    .end local v1    # "setGreetings":Landroid/view/View;
    :cond_0
    move v0, v3

    .line 73
    goto :goto_0

    .line 81
    .restart local v0    # "isExistingMailBox":Z
    .restart local v1    # "setGreetings":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->personalGreeting:Landroid/widget/TextView;

    const v4, 0x7f08004f

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 82
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->whatIsLayout:Landroid/view/View;

    const v4, 0x7f080050

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 83
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 109
    const-string v0, "SetupCompleteActivity"

    const-string v1, "SetupCompleteActivity.onDestroy()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 111
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 112
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onPause()V

    .line 104
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 105
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 96
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 97
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 98
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 99
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 3
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object v0, p0

    .line 185
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;

    invoke-direct {v2, p0, p1, v0}, Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/SetupCompleteActivity;ILandroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 242
    return-void
.end method

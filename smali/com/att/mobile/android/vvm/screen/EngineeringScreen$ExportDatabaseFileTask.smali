.class Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;
.super Landroid/os/AsyncTask;
.source "EngineeringScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/EngineeringScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExportDatabaseFileTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final dialog:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;


# direct methods
.method private constructor <init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V
    .locals 2

    .prologue
    .line 231
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 232
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->dialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method synthetic constructor <init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;Lcom/att/mobile/android/vvm/screen/EngineeringScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/att/mobile/android/vvm/screen/EngineeringScreen;
    .param p2, "x1"    # Lcom/att/mobile/android/vvm/screen/EngineeringScreen$1;

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 19
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    move-object/from16 v16, v0

    const-string v17, "messages"

    invoke-virtual/range {v16 .. v17}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 247
    .local v3, "deviceDBFile":Ljava/io/File;
    const/4 v9, 0x0

    .line 248
    .local v9, "sdCardDBFile":Ljava/io/File;
    const/4 v4, 0x0

    .line 249
    .local v4, "deviceDBFileInputStream":Ljava/io/FileInputStream;
    const/4 v11, 0x0

    .line 250
    .local v11, "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    const/4 v13, 0x0

    .line 256
    .local v13, "success":Z
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    .line 257
    .local v6, "directory":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    if-nez v6, :cond_0

    new-instance v6, Ljava/io/File;

    .end local v6    # "directory":Ljava/io/File;
    const-string v16, "/mnt/sdcard"

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_0
    const-string v16, "/messagesCopy.db"

    move-object/from16 v0, v16

    invoke-direct {v10, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    .end local v9    # "sdCardDBFile":Ljava/io/File;
    .local v10, "sdCardDBFile":Ljava/io/File;
    :try_start_1
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_1

    .line 262
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    move-result v15

    .line 263
    .local v15, "wasDeleted":Z
    const-string v16, "EngineeringScreen"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "copyApplicationDBToSDCard - sdCardDBFile.delete() returned "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    .end local v15    # "wasDeleted":Z
    :cond_1
    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    move-result v14

    .line 268
    .local v14, "wasCreated":Z
    const-string v16, "EngineeringScreen"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "copyApplicationDBToSDCard - sdCardDBFile.createNewFile() returned "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 271
    .end local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .local v5, "deviceDBFileInputStream":Ljava/io/FileInputStream;
    :try_start_2
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 275
    .end local v11    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .local v12, "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    :goto_0
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileInputStream;->read()I

    move-result v2

    .local v2, "data":I
    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v2, v0, :cond_2

    .line 276
    invoke-virtual {v12, v2}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 277
    const/4 v13, 0x1

    goto :goto_0

    .line 284
    :cond_2
    if-eqz v5, :cond_3

    .line 285
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 287
    :cond_3
    if-eqz v12, :cond_4

    .line 288
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :cond_4
    move-object v11, v12

    .end local v12    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    move-object v9, v10

    .line 295
    .end local v2    # "data":I
    .end local v10    # "sdCardDBFile":Ljava/io/File;
    .end local v14    # "wasCreated":Z
    .restart local v9    # "sdCardDBFile":Ljava/io/File;
    :cond_5
    :goto_1
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    return-object v16

    .line 290
    .end local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .end local v9    # "sdCardDBFile":Ljava/io/File;
    .end local v11    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "data":I
    .restart local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v12    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v14    # "wasCreated":Z
    :catch_0
    move-exception v7

    .line 291
    .local v7, "e":Ljava/lang/Exception;
    const-string v16, "EngineeringScreen"

    const-string v17, "copyApplicationDBToSDCard() - InputOutputStream close failed."

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v11, v12

    .end local v12    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    move-object v9, v10

    .line 294
    .end local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v9    # "sdCardDBFile":Ljava/io/File;
    goto :goto_1

    .line 279
    .end local v2    # "data":I
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v14    # "wasCreated":Z
    :catch_1
    move-exception v8

    .line 280
    .local v8, "ex":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v16, "EngineeringScreen"

    const-string v17, "copyApplicationDBToSDCard() - Database file copy to SD card failed."

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 284
    if-eqz v4, :cond_6

    .line 285
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 287
    :cond_6
    if-eqz v11, :cond_5

    .line 288
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    .line 290
    :catch_2
    move-exception v7

    .line 291
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v16, "EngineeringScreen"

    const-string v17, "copyApplicationDBToSDCard() - InputOutputStream close failed."

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 282
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v8    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v16

    .line 284
    :goto_3
    if-eqz v4, :cond_7

    .line 285
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 287
    :cond_7
    if-eqz v11, :cond_8

    .line 288
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    .line 292
    :cond_8
    :goto_4
    throw v16

    .line 290
    :catch_3
    move-exception v7

    .line 291
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v17, "EngineeringScreen"

    const-string v18, "copyApplicationDBToSDCard() - InputOutputStream close failed."

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 282
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v9    # "sdCardDBFile":Ljava/io/File;
    .restart local v10    # "sdCardDBFile":Ljava/io/File;
    :catchall_1
    move-exception v16

    move-object v9, v10

    .end local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v9    # "sdCardDBFile":Ljava/io/File;
    goto :goto_3

    .end local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .end local v9    # "sdCardDBFile":Ljava/io/File;
    .restart local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v14    # "wasCreated":Z
    :catchall_2
    move-exception v16

    move-object v4, v5

    .end local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    move-object v9, v10

    .end local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v9    # "sdCardDBFile":Ljava/io/File;
    goto :goto_3

    .end local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .end local v9    # "sdCardDBFile":Ljava/io/File;
    .end local v11    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v12    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    :catchall_3
    move-exception v16

    move-object v11, v12

    .end local v12    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    move-object v9, v10

    .end local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v9    # "sdCardDBFile":Ljava/io/File;
    goto :goto_3

    .line 279
    .end local v9    # "sdCardDBFile":Ljava/io/File;
    .end local v14    # "wasCreated":Z
    .restart local v10    # "sdCardDBFile":Ljava/io/File;
    :catch_4
    move-exception v8

    move-object v9, v10

    .end local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v9    # "sdCardDBFile":Ljava/io/File;
    goto :goto_2

    .end local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .end local v9    # "sdCardDBFile":Ljava/io/File;
    .restart local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v14    # "wasCreated":Z
    :catch_5
    move-exception v8

    move-object v4, v5

    .end local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    move-object v9, v10

    .end local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v9    # "sdCardDBFile":Ljava/io/File;
    goto :goto_2

    .end local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .end local v9    # "sdCardDBFile":Ljava/io/File;
    .end local v11    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v12    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v8

    move-object v11, v12

    .end local v12    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v11    # "sdCardDBFileOutputStream":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "deviceDBFileInputStream":Ljava/io/FileInputStream;
    move-object v9, v10

    .end local v10    # "sdCardDBFile":Ljava/io/File;
    .restart local v9    # "sdCardDBFile":Ljava/io/File;
    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 231
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    const/4 v1, 0x0

    .line 300
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 303
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    const-string v0, "Database was copied to SD card root directory"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(Ljava/lang/String;I)V

    .line 309
    :goto_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->this$0:Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->access$000(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 310
    return-void

    .line 307
    :cond_1
    const-string v0, "Database wasn\'t copied to SD card root directory"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 231
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->dialog:Landroid/app/ProgressDialog;

    const-string v1, "Please Wait..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->dialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 239
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 241
    return-void
.end method

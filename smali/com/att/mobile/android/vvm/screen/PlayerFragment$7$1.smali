.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$7$1;
.super Ljava/lang/Object;
.source "PlayerFragment.java"

# interfaces
.implements Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;)V
    .locals 0
    .param p1, "this$1"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;

    .prologue
    .line 1230
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7$1;->this$1:Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleNegativeButton(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1242
    return-void
.end method

.method public handlePositiveButton(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 1233
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7$1;->this$1:Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .line 1234
    .local v0, "playerActivity":Lcom/att/mobile/android/vvm/screen/PlayerActivity;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7$1;->this$1:Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1235
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7$1;->this$1:Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Message;->getRowId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->deleteVMs([Ljava/lang/Long;)V

    .line 1236
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7$1;->this$1:Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1100(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1238
    :cond_0
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;
.super Ljava/lang/Object;
.source "EnterExistingPasswordActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget v2, v2, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->minDigit:I

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->isPasswordValid(Landroid/widget/EditText;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->enterPasswordImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020117

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 63
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget v3, v3, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->minDigit:I

    invoke-virtual {v1, v2, v3}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->isPasswordValid(Landroid/widget/EditText;I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->clearError(Ljava/lang/Boolean;)V

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    const v2, 0x7f080162

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget v4, v4, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->minDigit:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->enterPasswordImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/screen/EnterExistingPasswordActivity;->setError(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 57
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 54
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;
.super Ljava/lang/Object;
.source "PlayerActivity.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/PlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 225
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 190
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 191
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$000(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 192
    const-string v0, "PlayerActivity"

    const-string v1, "onSensorChanged event.values[0] == 0"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$002(Lcom/att/mobile/android/vvm/screen/PlayerActivity;Z)Z

    .line 194
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0, v2}, Lcom/att/mobile/android/vvm/VVMApplication;->setCurrentlyApplicationAudioMode(Z)V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->swapAudioSource()V

    .line 200
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0, v2}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$102(Lcom/att/mobile/android/vvm/screen/PlayerActivity;Z)Z

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$200(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 219
    :cond_2
    :goto_0
    return-void

    .line 206
    :cond_3
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$000(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    const-string v0, "PlayerActivity"

    const-string v1, "onSensorChanged event.values[0] != 0"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$200(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 209
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0, v3}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$002(Lcom/att/mobile/android/vvm/screen/PlayerActivity;Z)Z

    .line 210
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$100(Lcom/att/mobile/android/vvm/screen/PlayerActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 211
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-static {v0, v3}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->access$102(Lcom/att/mobile/android/vvm/screen/PlayerActivity;Z)Z

    .line 212
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode()Z

    move-result v0

    if-nez v0, :cond_4

    .line 213
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0, v2}, Lcom/att/mobile/android/vvm/VVMApplication;->setCurrentlyApplicationAudioMode(Z)V

    .line 215
    :cond_4
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PlayerActivity;->swapAudioSource()V

    goto :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/screen/InboxActivity;
.super Lcom/att/mobile/android/vvm/screen/VmListActivity;
.source "InboxActivity.java"


# static fields
.field private static final DIALOG_MESSAGES_ALLMOST_FULL:I = 0x5

.field private static final DIALOG_MESSAGE_FULL:I = 0x6

.field private static final PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:I = 0xb

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isErrorDownloadMessgesShown:Z

.field private mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

.field private mTabLayout:Landroid/support/design/widget/TabLayout;

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field private maxMessagesDialog:Landroid/app/Dialog;

.field private showAlmostMaxMessagesDialog:Z

.field private showMaxMessagesDialog:Z

.field private simValid:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;-><init>()V

    .line 45
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->isErrorDownloadMessgesShown:Z

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->maxMessagesDialog:Landroid/app/Dialog;

    .line 50
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showMaxMessagesDialog:Z

    .line 51
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showAlmostMaxMessagesDialog:Z

    .line 55
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->simValid:Z

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/InboxActivity;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->stopGauge()V

    return-void
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/InboxActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/InboxActivity;
    .param p1, "x1"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showMaxMessagesDialog(I)V

    return-void
.end method

.method private initTabLayout()V
    .locals 4

    .prologue
    .line 135
    sget-object v2, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v3, "initTabLayout"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    const v2, 0x7f0f00ff

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/TabLayout;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    .line 137
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 141
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout;->getTabCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 142
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2, v0}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v1

    .line 143
    .local v1, "tab":Landroid/support/design/widget/TabLayout$Tab;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {v2, v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->getContentDescription(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    .end local v1    # "tab":Landroid/support/design/widget/TabLayout$Tab;
    :cond_0
    return-void
.end method

.method private initUIElements()V
    .locals 4

    .prologue
    .line 118
    const/4 v0, 0x0

    .line 119
    .local v0, "showPlayAllBtn":Z
    const v1, 0x7f0801f9

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->initInboxActionBar(Ljava/lang/String;ZZLcom/att/mobile/android/infra/utils/FontUtils$FontNames;)V

    .line 120
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->initViewPager()V

    .line 121
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->initTabLayout()V

    .line 122
    return-void
.end method

.method private initViewPager()V
    .locals 2

    .prologue
    .line 126
    const v0, 0x7f0f0100

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 127
    new-instance v0, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    .line 128
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 129
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->setActionListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ActionListener;)V

    .line 130
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->setListListener(Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment$ListListener;)V

    .line 131
    return-void
.end method

.method private isGooglePlayServicesAvailable()Z
    .locals 5

    .prologue
    .line 110
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    .line 111
    .local v0, "apiAvailability":Lcom/google/android/gms/common/GoogleApiAvailability;
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    .line 112
    .local v1, "resultCode":I
    sget-object v2, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Google Play Services Availability result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/GoogleApiAvailability;->getErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private refreshFragments(Z)V
    .locals 1
    .param p1, "groupedByContact"    # Z

    .prologue
    .line 246
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->refreshList(Z)V

    .line 247
    return-void
.end method

.method private registerGcm()V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->isGooglePlayServicesAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/att/mobile/android/vvm/gcm/RegistrationIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 106
    :cond_0
    return-void
.end method

.method private showMaxMessagesDialog(I)V
    .locals 8
    .param p1, "eventId"    # I

    .prologue
    .line 482
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->maxMessagesDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 484
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->maxMessagesDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 488
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->maxMessagesDialog:Landroid/app/Dialog;

    .line 491
    :cond_0
    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    const v2, 0x7f080128

    .line 494
    .local v2, "messageStringId":I
    :goto_1
    const v1, 0x7f080129

    const v3, 0x7f080153

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/att/mobile/android/vvm/screen/InboxActivity$13;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$13;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 506
    return-void

    .line 485
    .end local v2    # "messageStringId":I
    :catch_0
    move-exception v7

    .line 486
    .local v7, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 491
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_1
    const v2, 0x7f080127

    goto :goto_1
.end method

.method private startGauge()V
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->startGauge()V

    .line 592
    return-void
.end method

.method private stopGauge()V
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->stopGauge()V

    .line 600
    return-void
.end method


# virtual methods
.method protected closeEditMode()V
    .locals 3

    .prologue
    .line 612
    sget-object v1, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v2, "closeEditMode "

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout;->getSelectedTabPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->getFragment(I)Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    move-result-object v0

    .line 614
    .local v0, "currentFragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->closeEditMode()V

    .line 615
    return-void
.end method

.method public getCurrentFilterType()I
    .locals 3

    .prologue
    .line 470
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout;->getSelectedTabPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->getFragment(I)Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    move-result-object v0

    .line 471
    .local v0, "currentFragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getFilterType()I

    move-result v1

    return v1
.end method

.method protected goToInboxSavedTab()V
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout$Tab;->select()V

    .line 607
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 203
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/16 v0, 0x190

    if-ne p1, v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->simManager:Lcom/att/mobile/android/infra/sim/SimManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimManager;->startSimValidation()V

    .line 208
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 209
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 562
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onBackPressed()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->setResult(I)V

    .line 566
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->finish()V

    .line 567
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 77
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "InboxActivity::onCreate"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 81
    const v0, 0x7f04003f

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->setContentView(I)V

    .line 83
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->initUIElements()V

    .line 89
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    if-nez v0, :cond_0

    .line 90
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 94
    :cond_0
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showMaxMessagesDialog:Z

    .line 95
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showAlmostMaxMessagesDialog:Z

    .line 97
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->registerGcm()V

    .line 98
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 543
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 547
    invoke-static {}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->getInstance()Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    move-result-object v0

    invoke-virtual {v0, p0, p0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->removeEventListener(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 549
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onDestroy()V

    .line 550
    return-void
.end method

.method public onListUpdated(II)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "size"    # I

    .prologue
    .line 620
    sget-object v1, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onListUpdated type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mInboxFragmentPagerAdapter:Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout;->getSelectedTabPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragmentPagerAdapter;->getFragment(I)Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;

    move-result-object v0

    .line 622
    .local v0, "currentFragment":Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/inbox/InboxFragment;->getFilterType()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 623
    if-eqz p2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->setToolBarPlayAllBtn(Z)V

    .line 625
    :cond_0
    return-void

    .line 623
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onNetworkFailure()V
    .locals 0

    .prologue
    .line 571
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "newIntent"    # Landroid/content/Intent;

    .prologue
    .line 242
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->setIntent(Landroid/content/Intent;)V

    .line 243
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 527
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 532
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onPause()V

    .line 533
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 150
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onPostCreate"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 153
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 155
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->createInstance(Landroid/os/Handler;)V

    .line 156
    invoke-static {}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->getInstance()Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    move-result-object v0

    invoke-virtual {v0, p0, p0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->addEventListener(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 157
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .prologue
    .line 214
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRequestPermissionsResult requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    packed-switch p1, :pswitch_data_0

    .line 232
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 234
    return-void

    .line 218
    :pswitch_0
    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->isPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v0, p0}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->wasNeverAskAgainChecked(Ljava/lang/String;Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    const v1, 0x7f08016b

    const v2, 0x7f0800f1

    const v3, 0x7f080152

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/att/mobile/android/vvm/screen/InboxActivity$1;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    goto :goto_0

    .line 215
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x100000

    const/4 v4, 0x0

    .line 162
    sget-object v2, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v3, "onResume"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onResume()V

    .line 165
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mustBackToSetupProcess()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 170
    .local v1, "inboxIntent":Landroid/content/Intent;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 173
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v2

    and-int/2addr v2, v5

    if-eq v2, v5, :cond_2

    .line 174
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v2, "com.att.mobile.android.vvm.LAUNCH_INBOX_FROM_NOTIFICATION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 177
    sget-object v2, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v3, "onResume() action = ACTION_LAUNCH_INBOX_FROM_NOTIFICATION"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2, v4}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout$Tab;->select()V

    .line 181
    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->setIntent(Landroid/content/Intent;)V

    .line 186
    .end local v0    # "action":Ljava/lang/String;
    :cond_2
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "go_to_saved"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 187
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_3

    .line 188
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout$Tab;->select()V

    .line 191
    :cond_3
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    :cond_4
    if-eqz v1, :cond_0

    const-string v2, "refresh_inbox"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->refreshInbox()V

    .line 195
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setNeedRefreshInbox(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 553
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 554
    const-string v0, "isRestart"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 555
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 538
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onStop()V

    .line 539
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 3
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v2, 0x0

    .line 258
    sparse-switch p1, :sswitch_data_0

    .line 452
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateListener() Event ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$12;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$12;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 464
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    .line 465
    :goto_1
    return-void

    .line 261
    :sswitch_0
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() LOGIN_FAILED_DUE_TO_WRONG_PASSWORD"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 273
    :sswitch_1
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() SIM_VALID"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->simValid:Z

    if-nez v0, :cond_0

    .line 277
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->simValid:Z

    .line 278
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->wasMailboxRefreshedOnStartup()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() SIM_VALID going to refresh inbox"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->refreshInbox()V

    goto :goto_0

    .line 286
    :sswitch_2
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() RETRIEVE_MESSAGES_STARTED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$3;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 305
    :sswitch_3
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() RETRIEVE_HEADERS_FINISHED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$4;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 320
    :sswitch_4
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() RETRIEVE_BODIES_FINISHED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$5;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$5;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 335
    :sswitch_5
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() SELECT_INBOX_FINISHED_WITH_NO_MESSAGES"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$6;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$6;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 350
    :sswitch_6
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() RETRIEVE_BODIES_FAILED_NOT_ENOUGH_SPACE"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 355
    :sswitch_7
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() MESSAGES_ALLMOST_FULL"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showAlmostMaxMessagesDialog:Z

    if-eqz v0, :cond_0

    .line 358
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showAlmostMaxMessagesDialog:Z

    .line 359
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$7;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$7;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 369
    :sswitch_8
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() MESSAGES_FULL"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showMaxMessagesDialog:Z

    if-eqz v0, :cond_0

    .line 371
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->showMaxMessagesDialog:Z

    .line 372
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$8;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$8;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 383
    :sswitch_9
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() DELETE_FINISHED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "InboxActivity.onUpdateListener() notifyListeners EVENTS.DELETE_FINISHED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 388
    :sswitch_a
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() CONTACTS_CHANGED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    goto/16 :goto_1

    .line 393
    :sswitch_b
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() CONNECTION_LOST"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$9;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$9;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 405
    :sswitch_c
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() FETCH_BODIES_MAX_RETRIES"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$10;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$10;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 420
    :sswitch_d
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() CONNECTION_CONNECTED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 432
    :sswitch_e
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() REFRESH_UI"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 437
    :sswitch_f
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() NEW_MESSAGE"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 442
    :sswitch_10
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateListener() LOGIN_FAILED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InboxActivity$11;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity$11;-><init>(Lcom/att/mobile/android/vvm/screen/InboxActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 258
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_f
        0xa -> :sswitch_9
        0xc -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x28 -> :sswitch_5
        0x29 -> :sswitch_6
        0x2a -> :sswitch_7
        0x2b -> :sswitch_8
        0x30 -> :sswitch_a
        0x31 -> :sswitch_0
        0x32 -> :sswitch_10
        0x35 -> :sswitch_b
        0x36 -> :sswitch_d
        0x39 -> :sswitch_e
        0x3f -> :sswitch_c
    .end sparse-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 576
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged() hasFocus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->wasMailboxRefreshedOnStartup()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 581
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "InboxActivity.onWindowFocusChanged() going to refresh inbox"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->refreshInbox()V

    .line 584
    :cond_0
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->onWindowFocusChanged(Z)V

    .line 585
    return-void
.end method

.method protected refreshContacts()V
    .locals 2

    .prologue
    .line 251
    sget-object v0, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    const-string v1, "refreshContacts()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    invoke-static {}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->clearContactNamesAndPhotoCashe()V

    .line 253
    invoke-static {p0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/PicassoTools;->clearCache(Lcom/squareup/picasso/Picasso;)V

    .line 254
    return-void
.end method

.method protected refreshUi()V
    .locals 4

    .prologue
    .line 511
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isGroupByContact()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 512
    .local v0, "isGroupedByContact":Z
    sget-object v1, Lcom/att/mobile/android/vvm/screen/InboxActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "refreshUi isGroupedByContact="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->refreshFragments(Z)V

    .line 515
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 516
    const v1, 0x7f080145

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    .line 517
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->stopGauge()V

    .line 519
    :cond_0
    return-void
.end method

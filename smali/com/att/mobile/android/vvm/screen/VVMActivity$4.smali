.class Lcom/att/mobile/android/vvm/screen/VVMActivity$4;
.super Ljava/lang/Object;
.source "VVMActivity.java"

# interfaces
.implements Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/VVMActivity;->showSavedDialog(Landroid/app/Activity;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VVMActivity;

    .prologue
    .line 606
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleNegativeButton(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 614
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->goToInboxSavedTab()V

    .line 615
    return-void
.end method

.method public handlePositiveButton(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 609
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity$4;->this$0:Lcom/att/mobile/android/vvm/screen/VVMActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->goToGotItScreen()V

    .line 610
    return-void
.end method

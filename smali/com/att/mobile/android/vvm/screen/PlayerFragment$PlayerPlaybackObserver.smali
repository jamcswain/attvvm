.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;
.super Ljava/util/TimerTask;
.source "PlayerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayerPlaybackObserver"
.end annotation


# static fields
.field private static final OBSERVING_INTERVAL:I = 0xc8


# instance fields
.field private playerProgressUIComponentsUpdater:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;


# direct methods
.method private constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 1

    .prologue
    .line 373
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 382
    new-instance v0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver$1;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;->playerProgressUIComponentsUpdater:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Lcom/att/mobile/android/vvm/screen/PlayerFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    .param p2, "x1"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment$1;

    .prologue
    .line 373
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 401
    const-string v1, "PlayerFragment"

    const-string v2, "PlayerPlaybackObserver.run()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 404
    .local v0, "act":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 405
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$PlayerPlaybackObserver;->playerProgressUIComponentsUpdater:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 407
    :cond_0
    const-string v1, "PlayerFragment"

    const-string v2, "PlayerPlaybackObserver.run() ended"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    return-void
.end method

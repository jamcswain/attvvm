.class public Lcom/att/mobile/android/vvm/screen/PermissionsActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "PermissionsActivity.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# static fields
.field public static final PERMISSIONS_REQUEST_CODE:I = 0xd7

.field public static final PERMISSION_ACTIVITY_RESULT:I = 0x190

.field public static final PERMISSION_DENIED_EXIT_APP:I = 0x12c


# instance fields
.field private TAG:Ljava/lang/String;

.field private initialPermissionsFragment:Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

.field private permissionsGoToSettingsFragment:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 23
    const-string v0, "PermissionsActivity"

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method private goToApp()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    const-string v1, "The User has granted permissions to continue with the app"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    const/16 v0, 0x4a

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->setResult(I)V

    .line 64
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->finish()V

    .line 65
    return-void
.end method

.method private static launchHome(Landroid/app/Activity;)V
    .locals 3
    .param p0, "act"    # Landroid/app/Activity;

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 121
    .local v0, "mHomeIntent":Landroid/content/Intent;
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 123
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 124
    return-void
.end method

.method private shouldShowPermissionsGoToSettingsFragment()Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 68
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    sget-object v6, Lcom/att/mobile/android/vvm/model/Constants$KEYS;->PERMISSION_EVER_REQUESTED:Ljava/lang/String;

    const-class v7, Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v3, v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 69
    .local v2, "permissionRequestedBefore":Z
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "shouldShowPermissionsGoToSettingsFragment() permissionRequestedBefore = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-static {}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->getNeededPermission()[Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "needePermissions":[Ljava/lang/String;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "shouldShowPermissionsGoToSettingsFragment() needePermissions = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 76
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_2

    .line 77
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "shouldShowPermissionsGoToSettingsFragment() needePermissions ["

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "]"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v7, v1, v0

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\n!shouldShowRequestPermissionRationale(needePermissions["

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "]) = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v3, v1, v0

    .line 78
    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    :goto_1
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 77
    invoke-static {v6, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    aget-object v3, v1, v0

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 85
    .end local v0    # "i":I
    :goto_2
    return v4

    .restart local v0    # "i":I
    :cond_0
    move v3, v5

    .line 78
    goto :goto_1

    .line 76
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v0    # "i":I
    :cond_2
    move v4, v5

    .line 85
    goto :goto_2
.end method


# virtual methods
.method public exitAppWithoutPermissions()V
    .locals 1

    .prologue
    .line 115
    const/16 v0, 0x12c

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->setResult(I)V

    .line 117
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->finish()V

    .line 118
    return-void
.end method

.method public goToAllowPermissionsFragment()V
    .locals 5

    .prologue
    .line 89
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    const-string v3, "goToInitialPermissionsFragment"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 92
    .local v1, "supportFragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v2, "InitialPermissionActivityFragment"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->initialPermissionsFragment:Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    .line 93
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->initialPermissionsFragment:Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    if-nez v2, :cond_0

    .line 94
    new-instance v2, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    invoke-direct {v2}, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;-><init>()V

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->initialPermissionsFragment:Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    .line 97
    :cond_0
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 98
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    const v2, 0x7f0f0136

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->initialPermissionsFragment:Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    const-string v4, "InitialPermissionActivityFragment"

    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 99
    return-void
.end method

.method public goToPermissionsGoToSettingsFragment()V
    .locals 5

    .prologue
    .line 102
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    const-string v3, "goToPermissionsGoToSettingsFragment"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 105
    .local v1, "supportFragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v2, "PermissionsGoToSettingsFragment"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->permissionsGoToSettingsFragment:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    .line 106
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->permissionsGoToSettingsFragment:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    if-nez v2, :cond_0

    .line 107
    new-instance v2, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    invoke-direct {v2}, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;-><init>()V

    iput-object v2, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->permissionsGoToSettingsFragment:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    .line 110
    :cond_0
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 111
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    const v2, 0x7f0f0136

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->permissionsGoToSettingsFragment:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    const-string v4, "PermissionsGoToSettingsFragment"

    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 112
    return-void
.end method

.method public initTitleFontAndSize()V
    .locals 6

    .prologue
    const v5, 0x7f090005

    const/4 v4, 0x0

    const v3, 0x3fa66666    # 1.3f

    .line 131
    const v2, 0x7f0f0094

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 132
    .local v0, "title_att":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 133
    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_RegularItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 134
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 139
    :cond_0
    const v2, 0x7f0f0095

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 140
    .local v1, "title_visual":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 141
    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 142
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 143
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 147
    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->exitAppWithoutPermissions()V

    .line 128
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v0, 0x7f040050

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->setContentView(I)V

    .line 40
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 45
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-static {}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->areRequiredPermissionsGranted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->shouldShowPermissionsGoToSettingsFragment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume() shouldShowPermissionsGoToSettingsFragment = true"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->goToPermissionsGoToSettingsFragment()V

    .line 59
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume() shouldShowPermissionsGoToSettingsFragment = false"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->goToAllowPermissionsFragment()V

    goto :goto_0

    .line 57
    :cond_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->goToApp()V

    goto :goto_0
.end method

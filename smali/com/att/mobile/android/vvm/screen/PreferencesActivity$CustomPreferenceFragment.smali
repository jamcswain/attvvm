.class public Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;
.super Landroid/support/v7/preference/PreferenceFragmentCompat;
.source "PreferencesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/PreferencesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CustomPreferenceFragment"
.end annotation


# instance fields
.field private changePassPref:Landroid/support/v7/preference/Preference;

.field private greetingTypePref:Landroid/support/v7/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Landroid/support/v7/preference/PreferenceFragmentCompat;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->greetingTypePref:Landroid/support/v7/preference/Preference;

    .line 52
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->changePassPref:Landroid/support/v7/preference/Preference;

    return-void
.end method


# virtual methods
.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 57
    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->addPreferencesFromResource(I)V

    .line 59
    const v0, 0x7f080191

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->greetingTypePref:Landroid/support/v7/preference/Preference;

    .line 60
    const v0, 0x7f08018f

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/support/v7/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->changePassPref:Landroid/support/v7/preference/Preference;

    .line 61
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->greetingTypePref:Landroid/support/v7/preference/Preference;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment$1;-><init>(Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 78
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->changePassPref:Landroid/support/v7/preference/Preference;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment$2;-><init>(Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/Preference;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 97
    return-void
.end method

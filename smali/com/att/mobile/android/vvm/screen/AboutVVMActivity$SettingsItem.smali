.class public Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;
.super Ljava/lang/Object;
.source "AboutVVMActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SettingsItem"
.end annotation


# instance fields
.field private header:Ljava/lang/String;

.field private summary:Ljava/lang/String;

.field private summarySecondText:Ljava/lang/String;

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;
    .param p2, "headerText"    # Ljava/lang/String;
    .param p3, "summaryText"    # Ljava/lang/String;
    .param p4, "termsSecondTextViewText"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->this$0:Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->header:Ljava/lang/String;

    .line 150
    iput-object p3, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->summary:Ljava/lang/String;

    .line 151
    iput-object p4, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->summarySecondText:Ljava/lang/String;

    .line 152
    return-void
.end method


# virtual methods
.method public getHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->header:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getSummarySecondText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->summarySecondText:Ljava/lang/String;

    return-object v0
.end method

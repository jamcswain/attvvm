.class public Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "ATTMLauncherActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ATTMLauncherActivity"


# instance fields
.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    return-void
.end method

.method private setFontAndButton()V
    .locals 7

    .prologue
    const v6, 0x7f090005

    const/4 v5, 0x0

    const v4, 0x3fa66666    # 1.3f

    .line 45
    const v3, 0x7f0f0094

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 46
    .local v1, "title_att":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 47
    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_RegularItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 48
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v1, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 53
    :cond_0
    const v3, 0x7f0f0095

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 54
    .local v2, "title_visual":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 55
    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_LightItalic:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 56
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    .line 57
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 61
    :cond_1
    const v3, 0x7f0f0096

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 62
    const v3, 0x7f0f0097

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 64
    const v3, 0x7f0f0099

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 65
    .local v0, "btnLaunchATTM":Landroid/widget/Button;
    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 66
    new-instance v3, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity$1;

    invoke-direct {v3, p0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->launchATTMApplication()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/16 v0, 0x42

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->setResult(I)V

    .line 82
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onBackPressed()V

    .line 84
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    const-string v0, "ATTMLauncherActivity"

    const-string v1, "ATTMLauncherActivity.onDestroy()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createInstance(Landroid/content/Context;)V

    .line 37
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 39
    const v0, 0x7f040023

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->setContentView(I)V

    .line 41
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->setFontAndButton()V

    .line 42
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 24
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 25
    const-string v0, "ATTMLauncherActivity"

    const-string v1, "ATTMLauncherActivity()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ATTMLauncherActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v1, "showLaunchAttmScreenOnlyOnce"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/WelcomeActivity$1;
.super Landroid/telephony/PhoneStateListener;
.source "WelcomeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/WelcomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 180
    const-string v0, "WelcomeActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ":phoneStateListener state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    packed-switch p1, :pswitch_data_0

    .line 199
    :cond_0
    :goto_0
    const-string v0, "WelcomeActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AccountSetupActivity::phoneStateListener => stateString="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-super {p0, p1, p2}, Landroid/telephony/PhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V

    .line 202
    return-void

    .line 183
    :pswitch_0
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Off Hook"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "Idle"

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 185
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$100(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 186
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$200(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    .line 187
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$1;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$300(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/control/SetupController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/SetupController;->handleSetup()V

    goto :goto_0

    .line 191
    :pswitch_1
    const-string v0, "Off Hook"

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$002(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 194
    :pswitch_2
    const-string v0, "Ringing"

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$002(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

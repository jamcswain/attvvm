.class public Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source "VVMRecyclerView.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

.field private mEmptyTextView:Landroid/widget/TextView;

.field private mLoadingOrEmptyViewContainer:Landroid/view/View;

.field private mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView$1;-><init>(Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView$1;-><init>(Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView$1;-><init>(Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    .line 35
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->updateEmptyView()V

    return-void
.end method

.method private updateEmptyView()V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 67
    sget-object v2, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    const-string v4, "updateEmptyView"

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->isStart()Z

    move-result v2

    if-nez v2, :cond_3

    .line 69
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v2

    if-nez v2, :cond_4

    const/4 v0, 0x1

    .line 71
    .local v0, "showEmptyView":Z
    :goto_0
    sget-object v2, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateEmptyView showEmptyView = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingOrEmptyViewContainer:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 73
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingOrEmptyViewContainer:Landroid/view/View;

    if-eqz v0, :cond_5

    move v2, v1

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 75
    :cond_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mEmptyTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 76
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mEmptyTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    move v2, v1

    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 78
    :cond_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    if-eqz v2, :cond_2

    .line 79
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->setVisibility(I)V

    .line 81
    :cond_2
    if-eqz v0, :cond_7

    :goto_3
    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->setVisibility(I)V

    .line 83
    .end local v0    # "showEmptyView":Z
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 69
    goto :goto_0

    .restart local v0    # "showEmptyView":Z
    :cond_5
    move v2, v3

    .line 73
    goto :goto_1

    :cond_6
    move v2, v3

    .line 76
    goto :goto_2

    :cond_7
    move v3, v1

    .line 81
    goto :goto_3
.end method


# virtual methods
.method public setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;

    .prologue
    .line 54
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    const-string v1, "setAdapter"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->unregisterAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 58
    :cond_0
    if-eqz p1, :cond_1

    .line 59
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mDataObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 61
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 62
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->updateEmptyView()V

    .line 63
    return-void
.end method

.method public setLoadingOrEmptyViewElements(Landroid/view/View;Lcom/att/mobile/android/infra/utils/LoadingProgressBar;Landroid/widget/TextView;)V
    .locals 0
    .param p1, "LoadingOrEmptyViewContainer"    # Landroid/view/View;
    .param p2, "loadingProgressBar"    # Lcom/att/mobile/android/infra/utils/LoadingProgressBar;
    .param p3, "emptyTextView"    # Landroid/widget/TextView;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingOrEmptyViewContainer:Landroid/view/View;

    .line 48
    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    .line 49
    iput-object p3, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mEmptyTextView:Landroid/widget/TextView;

    .line 50
    return-void
.end method

.method public startLoadingProgressBar()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 86
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    const-string v1, "startLoadingProgressBar"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 89
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    const-string v1, "startLoadingProgressBar - item count = 0"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingOrEmptyViewContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingOrEmptyViewContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mEmptyTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 94
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    const-string v1, "startLoadingProgressBar: setting empty text view visibility to GONE"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    :cond_1
    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    if-eqz v0, :cond_2

    .line 99
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    const-string v1, "startLoadingProgressBar starting progress bar"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0, v2}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->start()V

    .line 104
    :cond_2
    return-void
.end method

.method public stopLoadingProgressBar()V
    .locals 2

    .prologue
    .line 108
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    const-string v1, "stopLoadingProgressBar"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->TAG:Ljava/lang/String;

    const-string v1, "stopLoadingProgressBar: stopping progress bar"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->stop()V

    .line 112
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->mLoadingProgressBar:Lcom/att/mobile/android/infra/utils/LoadingProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/infra/utils/LoadingProgressBar;->setVisibility(I)V

    .line 114
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMRecyclerView;->updateEmptyView()V

    .line 115
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$6;
.super Ljava/lang/Object;
.source "PlayerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initButtonsListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 1208
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$6;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1211
    :try_start_0
    const-string v3, "PlayerFragment"

    const-string v4, "sendMsgButton.onClick"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$6;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$1000(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v2

    .line 1213
    .local v2, "phoneNumber":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SENDTO"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1214
    .local v1, "intent":Landroid/content/Intent;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sms:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1215
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1216
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$6;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$100(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "phoneNumber":Ljava/lang/String;
    :goto_0
    return-void

    .line 1217
    :catch_0
    move-exception v0

    .line 1218
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "PlayerFragment"

    const-string v4, "Failed to invoke SMS"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

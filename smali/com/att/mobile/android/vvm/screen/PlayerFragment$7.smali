.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;
.super Ljava/lang/Object;
.source "PlayerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initButtonsListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 1224
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1227
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$400(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/infra/media/AudioPlayer;->pause()V

    .line 1228
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1229
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1230
    const v1, 0x7f0800e1

    const v2, 0x7f0800de

    const v3, 0x7f0800dd

    const v4, 0x7f0800b7

    new-instance v5, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7$1;

    invoke-direct {v5, p0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$7$1;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment$7;)V

    invoke-static/range {v0 .. v5}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showRightAlignedDialog(Landroid/content/Context;IIIILcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 1249
    :goto_0
    return-void

    .line 1245
    :cond_0
    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showNoNetworkConnectionDialog(Landroid/content/Context;)V

    goto :goto_0
.end method

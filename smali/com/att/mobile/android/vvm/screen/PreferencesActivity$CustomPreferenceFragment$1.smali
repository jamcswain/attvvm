.class Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment$1;
.super Ljava/lang/Object;
.source "PreferencesActivity.java"

# interfaces
.implements Landroid/support/v7/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/support/v7/preference/Preference;)Z
    .locals 3
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;

    .prologue
    .line 65
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    const-string v1, "PreferencesActivity"

    const-string v2, "greetingTypePref.onPreferenceClick()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment$1;->this$0:Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    .line 68
    .local v0, "parent":Lcom/att/mobile/android/vvm/screen/PreferencesActivity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->access$000(Lcom/att/mobile/android/vvm/screen/PreferencesActivity;)V

    .line 74
    .end local v0    # "parent":Lcom/att/mobile/android/vvm/screen/PreferencesActivity;
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 72
    :cond_1
    const v1, 0x7f080145

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0
.end method

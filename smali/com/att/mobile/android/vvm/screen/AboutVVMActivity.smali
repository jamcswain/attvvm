.class public Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "AboutVVMActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;,
        Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field res:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private initArrayItems()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;>;"
    new-instance v3, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->res:Landroid/content/res/Resources;

    const v5, 0x7f080091

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->res:Landroid/content/res/Resources;

    const v7, 0x7f080090

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationVersion()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, p0, v4, v5, v6}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;-><init>(Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->res:Landroid/content/res/Resources;

    const v4, 0x7f0801dc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "first_link":Ljava/lang/String;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->res:Landroid/content/res/Resources;

    const v4, 0x7f0801df

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 129
    .local v2, "second_link":Ljava/lang/String;
    new-instance v3, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->res:Landroid/content/res/Resources;

    const v5, 0x7f0801db

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p0, v4, v0, v2}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;-><init>(Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    return-object v1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v4, 0x7f04001b

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->setContentView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->res:Landroid/content/res/Resources;

    .line 42
    const v4, 0x7f0f00e9

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 43
    .local v3, "title":Landroid/widget/TextView;
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 44
    const v4, 0x7f0801a6

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->initActionBar(IZ)V

    .line 46
    const v4, 0x7f0f0077

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 47
    .local v1, "list":Landroid/widget/ListView;
    new-instance v4, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$1;

    invoke-direct {v4, p0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;)V

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 54
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->initArrayItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 56
    .local v0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;>;"
    new-instance v2, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;

    invoke-direct {v2, p0, p0, v0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;-><init>(Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 57
    .local v2, "settingsAdapter":Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 58
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 134
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 139
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 136
    :pswitch_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->onBackPressed()V

    .line 137
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 64
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->mustBackToSetupProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    :cond_0
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "GreetingActionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GreetingsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
        ">;"
    }
.end annotation


# instance fields
.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 234
    .local p4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    .line 235
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 236
    iput-object p4, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;->items:Ljava/util/ArrayList;

    .line 237
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 246
    move-object v4, p2

    .line 247
    .local v4, "v":Landroid/view/View;
    if-nez v4, :cond_0

    .line 248
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 249
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v7, 0x7f04003d

    invoke-virtual {v5, v7, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 252
    .end local v5    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;->items:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    .line 253
    .local v0, "g":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    if-eqz v0, :cond_2

    .line 254
    const v7, 0x7f0f00f1

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 255
    .local v1, "headerTxt":Landroid/widget/TextView;
    const v7, 0x7f0f00f2

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 256
    .local v3, "subTxt":Landroid/widget/TextView;
    const v7, 0x7f0f00f3

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 258
    .local v2, "img":Landroid/widget/ImageView;
    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 259
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    sget-object v7, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v7}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 261
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDesc()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    sget-object v7, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v7}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 263
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getIsSelected()Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    :goto_0
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 266
    :cond_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 267
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getIsSelected()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 268
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    const v8, 0x7f0800c2

    invoke-virtual {v7, v8}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 275
    .end local v1    # "headerTxt":Landroid/widget/TextView;
    .end local v2    # "img":Landroid/widget/ImageView;
    .end local v3    # "subTxt":Landroid/widget/TextView;
    :cond_2
    :goto_1
    return-object v4

    .line 263
    .restart local v1    # "headerTxt":Landroid/widget/TextView;
    .restart local v2    # "img":Landroid/widget/ImageView;
    .restart local v3    # "subTxt":Landroid/widget/TextView;
    :cond_3
    const/4 v6, 0x4

    goto :goto_0

    .line 270
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    const v8, 0x7f0801e6

    invoke-virtual {v7, v8}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public updateItems(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$GreetingsAdapter;->items:Ljava/util/ArrayList;

    .line 241
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 242
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;
.super Landroid/support/v4/app/Fragment;
.source "InitialPermissionActivityFragment.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "InitialPermissionActivityFragment"


# instance fields
.field private nextButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private initFontAndButton()V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;->initTitleFontAndSize()V

    .line 44
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0111

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 45
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0112

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->nextButton:Landroid/widget/Button;

    .line 46
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->nextButton:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 47
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->nextButton:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment$1;-><init>(Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    const v0, 0x7f040042

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .prologue
    .line 61
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/model/Constants$KEYS;->PERMISSION_EVER_REQUESTED:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 63
    return-void
.end method

.method public onResume()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .prologue
    .line 36
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 37
    const-string v0, "InitialPermissionActivityFragment"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->initFontAndButton()V

    .line 40
    return-void
.end method

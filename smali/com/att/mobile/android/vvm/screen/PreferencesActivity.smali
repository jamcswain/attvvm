.class public Lcom/att/mobile/android/vvm/screen/PreferencesActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "PreferencesActivity.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/EventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PreferencesActivity"


# instance fields
.field private isCanceled:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 31
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->isCanceled:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/PreferencesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->getGreetingsDetails()V

    return-void
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/PreferencesActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->openChangePasswordScreen()V

    return-void
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/screen/PreferencesActivity;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/PreferencesActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->isCanceled:Ljava/lang/Boolean;

    return-object v0
.end method

.method private getGreetingsDetails()V
    .locals 2

    .prologue
    .line 104
    const-string v0, "PreferencesActivity"

    const-string v1, "getGreetingsDetails"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-static {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimManager;->validateSim()Lcom/att/mobile/android/infra/sim/SimValidationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->isSimPresentAndReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->showNoSimDialog()V

    .line 115
    :goto_0
    return-void

    .line 111
    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->showGauge(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueGetGreetingsDetailsOperation()V

    goto :goto_0
.end method

.method private openChangePasswordScreen()V
    .locals 4

    .prologue
    .line 118
    invoke-static {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/sim/SimManager;->validateSim()Lcom/att/mobile/android/infra/sim/SimValidationResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->isSimPresentAndReady()Z

    move-result v2

    if-nez v2, :cond_0

    .line 119
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->showNoSimDialog()V

    .line 129
    :goto_0
    return-void

    .line 123
    :cond_0
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 124
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "from_settings"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 125
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    .end local v1    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "PreferencesActivity"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private showNoSimDialog()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 132
    const-string v0, "PreferencesActivity"

    const-string v2, "openChangePasswordScreen(): no sim card / sim not ready. Cannot refresh inbox."

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const v2, 0x7f08014d

    const v3, 0x7f08014e

    const/4 v6, 0x0

    move-object v0, p0

    move v4, v1

    move v5, v1

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 135
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 176
    const-string v0, "PreferencesActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const/16 v0, 0x26

    if-ne p2, v0, :cond_1

    .line 181
    const v0, 0x7f080160

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    const/16 v0, 0x27

    if-ne p2, v0, :cond_0

    .line 186
    const v0, 0x7f080164

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 170
    const-string v0, "PreferencesActivity"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onBackPressed()V

    .line 172
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f0801c7

    .line 37
    const-string v0, "PreferencesActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f040062

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->setContentView(I)V

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->initActionBar(IZ)V

    .line 44
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 46
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0f015a

    new-instance v2, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;

    invoke-direct {v2}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$CustomPreferenceFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 47
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 163
    const-string v0, "PreferencesActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 166
    return-void
.end method

.method public onNetworkFailure()V
    .locals 0

    .prologue
    .line 268
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 154
    const-string v0, "PreferencesActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onPause()V

    .line 156
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/VVMApplication;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setVisible(Z)V

    .line 157
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 158
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 159
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 139
    const-string v0, "PreferencesActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onResume()V

    .line 143
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->mustBackToSetupProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/VVMApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setVisible(Z)V

    .line 148
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 149
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    goto :goto_0
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 3
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object v0, p0

    .line 196
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/PreferencesActivity;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/att/mobile/android/vvm/screen/PreferencesActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/PreferencesActivity;ILandroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 262
    return-void
.end method

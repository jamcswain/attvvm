.class Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;
.super Ljava/lang/Object;
.source "WelcomeActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->initWithMsisdnScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    .prologue
    .line 690
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 693
    const-string v0, "WelcomeActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "btnBeginSetup click.time saved  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$100(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSetupStarted(Ljava/lang/Boolean;)V

    .line 695
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v1, "mosmsTimePref"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 697
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$600(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$100(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 703
    :goto_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$200(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V

    .line 704
    return-void

    .line 700
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$100(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 701
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$12;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$300(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/control/SetupController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/SetupController;->handleSetup()V

    goto :goto_0
.end method

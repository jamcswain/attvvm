.class public Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;
.super Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;
.source "ChangePasswordActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private confirmPasswordImage:Landroid/widget/ImageView;

.field private confirmPasswordText:Landroid/widget/EditText;

.field final confirmPasswordTextWatcher:Landroid/text/TextWatcher;

.field final enterPasswordTextWatcher:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;-><init>()V

    .line 87
    new-instance v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordTextWatcher:Landroid/text/TextWatcher;

    .line 131
    new-instance v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordImage:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method protected changePassword(Ljava/lang/String;)V
    .locals 2
    .param p1, "pass"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getPasswordChangeRequiredStatus()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 79
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueXChangeTUIPasswordOperation(Ljava/lang/String;)V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueSetPasswordMetaDataOperation(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected initConfirmPassword()V
    .locals 5

    .prologue
    .line 69
    const v0, 0x7f0f00b8

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordText:Landroid/widget/EditText;

    .line 70
    const v0, 0x7f0f00b9

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordImage:Landroid/widget/ImageView;

    .line 71
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordText:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->maxDigit:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 72
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordText:Landroid/widget/EditText;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 73
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->confirmPasswordTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 74
    return-void
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->isFromSettings:Z

    if-nez v0, :cond_0

    .line 176
    const v1, 0x7f0800f3

    const v2, 0x7f080165

    const v3, 0x7f080152

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-instance v6, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$4;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 187
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f040028

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->initUI(I)V

    .line 38
    sget-object v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->TAG:Ljava/lang/String;

    const-string v1, "ChangePasswordActivity::onCreate"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 167
    sget-object v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->TAG:Ljava/lang/String;

    const-string v1, "ChangePasswordActivity::onPause"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->setSoftKeyboardVisibility(Z)V

    .line 169
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->onPause()V

    .line 170
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 195
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    new-instance v0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$5;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$5;-><init>(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;I)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 253
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    .line 254
    return-void
.end method

.method protected setListeners()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 45
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/ChangePasswordActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 66
    return-void
.end method

.class Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;
.super Ljava/util/TimerTask;
.source "AudioRecorderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioRecorderRecordingObserver"
.end annotation


# static fields
.field private static final OBSERVING_INTERVAL:I = 0xfa


# instance fields
.field private audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

.field private isObservingForDummyRecording:Z

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;


# direct methods
.method private constructor <init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/infra/media/AudioRecorder;Z)V
    .locals 1
    .param p2, "audioRecorder"    # Lcom/att/mobile/android/infra/media/AudioRecorder;
    .param p3, "isForDummyRecording"    # Z

    .prologue
    .line 113
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->isObservingForDummyRecording:Z

    .line 115
    iput-object p2, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    .line 118
    iput-boolean p3, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->isObservingForDummyRecording:Z

    .line 119
    return-void
.end method

.method synthetic constructor <init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/infra/media/AudioRecorder;ZLcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
    .param p2, "x1"    # Lcom/att/mobile/android/infra/media/AudioRecorder;
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$1;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;-><init>(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;Lcom/att/mobile/android/infra/media/AudioRecorder;Z)V

    return-void
.end method

.method private observeDummyRecording()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method private observeRealRecording()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)I

    move-result v1

    add-int/lit16 v1, v1, 0xfa

    invoke-static {v0, v1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$002(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;I)I

    .line 163
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$100(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)V

    .line 173
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->access$000(Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->this$0:Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;

    iget v1, v1, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->maximumRecordDuration:I

    if-le v0, v1, :cond_0

    .line 176
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->audioRecorder:Lcom/att/mobile/android/infra/media/AudioRecorder;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/media/AudioRecorder;->stop()V

    .line 178
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->isObservingForDummyRecording:Z

    if-eqz v0, :cond_0

    .line 128
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->observeDummyRecording()V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity$AudioRecorderRecordingObserver;->observeRealRecording()V

    goto :goto_0
.end method

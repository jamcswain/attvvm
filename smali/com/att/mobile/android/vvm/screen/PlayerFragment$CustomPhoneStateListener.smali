.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "PlayerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomPhoneStateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;


# direct methods
.method private constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;Lcom/att/mobile/android/vvm/screen/PlayerFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;
    .param p2, "x1"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment$1;

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;-><init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 364
    const-string v0, "PlayerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ":phoneStateListener state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$CustomPhoneStateListener;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$300(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    .line 366
    invoke-super {p0, p1, p2}, Landroid/telephony/PhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V

    .line 367
    return-void
.end method

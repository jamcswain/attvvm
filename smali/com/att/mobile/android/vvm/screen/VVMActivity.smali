.class public Lcom/att/mobile/android/vvm/screen/VVMActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "VVMActivity.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/EventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;,
        Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;,
        Lcom/att/mobile/android/vvm/screen/VVMActivity$ActivityStateBeforeGettingFocus;
    }
.end annotation


# static fields
.field public static final HIGH_DENSITY:F = 1.5f

.field public static final MID_DENSITY:F = 1.0f

.field private static final TAG:Ljava/lang/String; = "VVMActivity"

.field static vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;


# instance fields
.field protected contactsChanged:Z

.field protected editNameDialog:Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;

.field protected handler:Landroid/os/Handler;

.field protected isMenuButtonExists:Z

.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field protected screenStateBeforeGettingFocus:I

.field protected simManager:Lcom/att/mobile/android/infra/sim/SimManager;

.field protected toolBarPlayImage:Landroid/widget/ImageView;

.field protected toolBarTV:Landroid/widget/TextView;

.field protected toolbar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->isMenuButtonExists:Z

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->contactsChanged:Z

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/VVMActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/VVMActivity;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/VVMActivity;Ljava/lang/CharSequence;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/VVMActivity;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->initCall(Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private checkAttmStatus()V
    .locals 3

    .prologue
    .line 247
    move-object v0, p0

    .line 249
    .local v0, "thisActivity":Landroid/app/Activity;
    const-string v1, "VVMActivity"

    const-string v2, "VVMActivity.checkAttmStatus() check if need to check ATTM status"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->shouldCheckAttmStatusOnForeground()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    const-string v1, "VVMActivity"

    const-string v2, "VVMActivity.checkAttmStatus() going to check ATTM Status"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;Landroid/app/Activity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 280
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 282
    :cond_0
    return-void
.end method

.method public static exit(Landroid/app/Activity;)V
    .locals 0
    .param p0, "act"    # Landroid/app/Activity;

    .prologue
    .line 508
    invoke-static {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->launchHome(Landroid/app/Activity;)V

    .line 509
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 510
    return-void
.end method

.method private exitAppDueToMissingPermmision(II)Z
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I

    .prologue
    .line 576
    const/16 v0, 0x190

    if-ne p1, v0, :cond_0

    const/16 v0, 0x12c

    if-ne p2, v0, :cond_0

    .line 577
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->finish()V

    .line 578
    const/4 v0, 0x1

    .line 580
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initCall(Ljava/lang/CharSequence;)Landroid/content/Intent;
    .locals 4
    .param p1, "phoneNumber"    # Ljava/lang/CharSequence;

    .prologue
    .line 352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 353
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 354
    .local v0, "it":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 355
    return-object v0
.end method

.method private static launchHome(Landroid/app/Activity;)V
    .locals 3
    .param p0, "act"    # Landroid/app/Activity;

    .prologue
    .line 502
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 503
    .local v0, "mHomeIntent":Landroid/content/Intent;
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 504
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 505
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 506
    return-void
.end method

.method private setInitializeSetupState()V
    .locals 6

    .prologue
    .line 157
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v3, "first_login_4_0"

    const-class v4, Ljava/lang/Boolean;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 158
    .local v0, "isFirstTimeLogin4_0":Z
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 159
    .local v1, "setupComplete":Z
    if-eqz v0, :cond_0

    .line 160
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v3, "first_login_4_0"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    if-eqz v1, :cond_1

    .line 162
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_0
.end method

.method public static showAudioShareMenu(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileUri"    # Landroid/net/Uri;

    .prologue
    .line 469
    const-string v4, "VVMActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VvmActivity.showAudioShareMenu() Uri = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.SEND"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 476
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".mp3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 477
    const-string v4, "audio/mp3"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 481
    :goto_0
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 483
    const-string v4, "VVMActivity"

    const-string v5, "VvmActivity.showAudioShareMenu() add intent for HTC android.intent.action.SEND_MSG"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.SEND_MSG"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 487
    .local v1, "htcIntent1":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 488
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 490
    const-string v4, "VVMActivity"

    const-string v5, "VvmActivity.showAudioShareMenu() add intent for HTC com.htc.intent.action.SEND_MSG"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.htc.intent.action.SEND_MSG"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 493
    .local v2, "htcIntent2":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 496
    const v4, 0x7f0801c8

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 497
    .local v0, "chooser":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 498
    const-string v4, "android.intent.extra.INITIAL_INTENTS"

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/content/Intent;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 499
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 500
    return-void

    .line 479
    .end local v0    # "chooser":Landroid/content/Intent;
    .end local v1    # "htcIntent1":Landroid/content/Intent;
    .end local v2    # "htcIntent2":Landroid/content/Intent;
    :cond_0
    const-string v4, "audio/amr"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method protected deleteVMs([Ljava/lang/Long;)V
    .locals 2
    .param p1, "mesIDs"    # [Ljava/lang/Long;

    .prologue
    .line 585
    const-string v0, "VVMActivity"

    const-string v1, "deleteVMs()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    new-instance v0, Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;-><init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;[Ljava/lang/Long;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity$RunDeleteAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 587
    return-void
.end method

.method protected dismissGauge()V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->editNameDialog:Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->editNameDialog:Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;

    invoke-virtual {v0}, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->dismissAllowingStateLoss()V

    .line 532
    :cond_0
    return-void
.end method

.method public getBodyTextWithLink(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 6
    .param p1, "linkableText"    # Ljava/lang/String;

    .prologue
    .line 335
    const v4, 0x7f08005a

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 336
    .local v1, "link":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 337
    .local v3, "startLink":I
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int v0, v3, v4

    .line 338
    .local v0, "endLink":I
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 339
    .local v2, "sbuilder":Landroid/text/SpannableString;
    new-instance v4, Lcom/att/mobile/android/vvm/screen/VVMActivity$2;

    invoke-direct {v4, p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;)V

    const/16 v5, 0x21

    invoke-virtual {v2, v4, v3, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 347
    return-object v2
.end method

.method protected goToGotItScreen()V
    .locals 0

    .prologue
    .line 622
    return-void
.end method

.method protected goToInboxSavedTab()V
    .locals 2

    .prologue
    .line 626
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 627
    .local v0, "startActivityIntent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 628
    const-string v1, "go_to_saved"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 629
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->startActivity(Landroid/content/Intent;)V

    .line 630
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->finish()V

    .line 631
    return-void
.end method

.method protected initActionBar(IZ)V
    .locals 4
    .param p1, "headerId"    # I
    .param p2, "shouldDisplayBackOption"    # Z

    .prologue
    .line 536
    const-string v1, "VVMActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initActionBar shouldDisplayBackOption="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    const v1, 0x7f0f0079

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 538
    const v1, 0x7f0f00e9

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->toolBarTV:Landroid/widget/TextView;

    .line 539
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->toolBarTV:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 541
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->toolBarTV:Landroid/widget/TextView;

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 542
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->toolBarTV:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 543
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 544
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 545
    .local v0, "supportActionBar":Landroid/support/v7/app/ActionBar;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 547
    invoke-virtual {v0, p2}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 548
    invoke-virtual {v0, p2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 551
    .end local v0    # "supportActionBar":Landroid/support/v7/app/ActionBar;
    :cond_0
    return-void
.end method

.method protected isHighDensityScreen()Z
    .locals 4

    .prologue
    .line 379
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 380
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 381
    const-string v1, "VVMActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VVMActivity.isHighDensityScreen() - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x3fc00000    # 1.5f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected markMessagesAs([Ljava/lang/Long;I)V
    .locals 3
    .param p1, "mesUIDs"    # [Ljava/lang/Long;
    .param p2, "savedState"    # I

    .prologue
    .line 590
    const-string v0, "VVMActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "markMessagesAs() savedState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    new-instance v0, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;-><init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;[Ljava/lang/Long;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity$MarkMessageAsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 592
    return-void
.end method

.method protected mustBackToSetupProcess()Z
    .locals 3

    .prologue
    .line 234
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v1

    const/16 v2, 0xd

    if-eq v1, v2, :cond_0

    .line 236
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 237
    .local v0, "startActivityIntent":Landroid/content/Intent;
    const-class v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 238
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->startActivity(Landroid/content/Intent;)V

    .line 239
    const/4 v1, 0x1

    .line 241
    .end local v0    # "startActivityIntent":Landroid/content/Intent;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 567
    const-string v0, "VVMActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->exitAppDueToMissingPermmision(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    invoke-static {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->exit(Landroid/app/Activity;)V

    .line 573
    :goto_0
    return-void

    .line 571
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 132
    const-string v0, "VVMActivity"

    const-string v1, "VVMActivity.onCreate()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 136
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    .line 137
    invoke-static {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->simManager:Lcom/att/mobile/android/infra/sim/SimManager;

    .line 138
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->handler:Landroid/os/Handler;

    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->screenStateBeforeGettingFocus:I

    .line 143
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createInstance(Landroid/content/Context;)V

    .line 145
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 150
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->setInitializeSetupState()V

    .line 151
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->contactsChanged:Z

    if-eqz v0, :cond_0

    .line 313
    invoke-static {p0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/PicassoTools;->clearCache(Lcom/squareup/picasso/Picasso;)V

    .line 316
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 317
    return-void
.end method

.method public onNetworkFailure()V
    .locals 0

    .prologue
    .line 422
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 555
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 560
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 557
    :pswitch_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onBackPressed()V

    .line 558
    const/4 v0, 0x1

    goto :goto_0

    .line 555
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 289
    const-string v0, "VVMActivity"

    const-string v1, "VVMActivity.onPause()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->simManager:Lcom/att/mobile/android/infra/sim/SimManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/infra/sim/SimManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 293
    const/4 v0, 0x1

    iput v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->screenStateBeforeGettingFocus:I

    .line 296
    sget-object v0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/VVMApplication;->setVisible(Z)V

    .line 298
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 299
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 196
    const-string v1, "VVMActivity"

    const-string v2, "VVMActivity.onResume()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 199
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->simManager:Lcom/att/mobile/android/infra/sim/SimManager;

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/infra/sim/SimManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 201
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_2

    invoke-static {}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->areRequiredPermissionsGranted()Z

    move-result v1

    if-nez v1, :cond_2

    .line 203
    const-string v1, "VVMActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Permissions: Need permissions SetupState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v1

    const/16 v2, 0xd

    if-eq v1, v2, :cond_0

    .line 205
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->backToPrevState()V

    .line 207
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/att/mobile/android/vvm/screen/PermissionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 208
    .local v0, "newIntent":Landroid/content/Intent;
    const/16 v1, 0x190

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 230
    .end local v0    # "newIntent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 213
    :cond_2
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    const-string v2, "simSwap"

    const-class v3, Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 214
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->showSimSwappedDialog()V

    .line 218
    :cond_3
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->checkAttmStatus()V

    .line 220
    sget-object v1, Lcom/att/mobile/android/vvm/screen/VVMActivity;->vvmApplication:Lcom/att/mobile/android/vvm/VVMApplication;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/VVMApplication;->setVisible(Z)V

    .line 225
    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->contactsChanged:Z

    if-eqz v1, :cond_1

    .line 226
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->refreshContacts()V

    .line 227
    iput-boolean v5, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->contactsChanged:Z

    goto :goto_0
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 3
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 388
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    sparse-switch p1, :sswitch_data_0

    .line 415
    const-string v0, "VVMActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpdateListener() eventId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " no action."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 391
    :sswitch_0
    const-string v0, "VVMActivity"

    const-string v1, "VVMActivity.onUpdateListener() - START_WELCOME_ACTIVITY"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 399
    :sswitch_1
    const-string v0, "VVMActivity"

    const-string v1, "onUpdateListener() SIM_SWAPED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/VVMActivity$3;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity$3;-><init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 411
    :sswitch_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->contactsChanged:Z

    goto :goto_0

    .line 388
    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_1
        0x30 -> :sswitch_2
        0x40 -> :sswitch_0
    .end sparse-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 176
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onWindowFocusChanged(Z)V

    .line 179
    if-nez p1, :cond_0

    .line 181
    iget v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->screenStateBeforeGettingFocus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 183
    const/4 v0, 0x2

    iput v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->screenStateBeforeGettingFocus:I

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->screenStateBeforeGettingFocus:I

    goto :goto_0
.end method

.method protected refreshContacts()V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method protected showGauge(Ljava/lang/String;)V
    .locals 3
    .param p1, "loadingText"    # Ljava/lang/String;

    .prologue
    .line 517
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 518
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    new-instance v1, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;

    invoke-direct {v1}, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;-><init>()V

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->editNameDialog:Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;

    .line 519
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->editNameDialog:Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;

    invoke-virtual {v1, p1}, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->setBodyText(Ljava/lang/String;)V

    .line 520
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->editNameDialog:Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->setCancelable(Z)V

    .line 521
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/VVMActivity;->editNameDialog:Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;

    const-string v2, "fragment_edit_name"

    invoke-virtual {v1, v0, v2}, Lcom/att/mobile/android/infra/utils/SwirlDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 522
    return-void
.end method

.method protected showSavedDialog(Landroid/app/Activity;Z)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "isMulipleMessages"    # Z

    .prologue
    .line 597
    if-eqz p2, :cond_1

    .line 598
    const v1, 0x7f08012c

    .line 599
    .local v1, "titleId":I
    const v2, 0x7f08012b

    .line 604
    .local v2, "bodyId":I
    :goto_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v3, "doNotShowSavedDialogAgain"

    const-class v4, Ljava/lang/Boolean;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 606
    const v3, 0x7f0800eb

    const v4, 0x7f080153

    const v5, 0x7f0801bc

    new-instance v6, Lcom/att/mobile/android/vvm/screen/VVMActivity$4;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity$4;-><init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;)V

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialogWithCB(Landroid/content/Context;IIIIILcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 618
    :cond_0
    return-void

    .line 601
    .end local v1    # "titleId":I
    .end local v2    # "bodyId":I
    :cond_1
    const v1, 0x7f080125

    .line 602
    .restart local v1    # "titleId":I
    const v2, 0x7f080124

    .restart local v2    # "bodyId":I
    goto :goto_0
.end method

.method protected showSimSwappedDialog()V
    .locals 7

    .prologue
    .line 636
    const v1, 0x7f0801ca

    const v2, 0x7f0801c9

    const v3, 0x7f080153

    const v4, 0x7f0800b6

    const/4 v5, 0x0

    new-instance v6, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;

    invoke-direct {v6, p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity$5;-><init>(Lcom/att/mobile/android/vvm/screen/VVMActivity;)V

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/att/mobile/android/infra/utils/AlertDlgUtils;->showDialog(Landroid/content/Context;IIIIZLcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;)V

    .line 661
    return-void
.end method

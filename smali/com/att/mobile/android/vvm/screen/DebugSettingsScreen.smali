.class public Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "DebugSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$MyOnItemSelectedListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DebugSettingsScreen"


# instance fields
.field private hostName:Ljava/lang/String;

.field private manualHost:Landroid/widget/TextView;

.field private userName:Ljava/lang/String;

.field private userPass:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->saveDebugSettings()V

    return-void
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->manualHost:Landroid/widget/TextView;

    return-object v0
.end method

.method private saveDebugSettings()V
    .locals 9

    .prologue
    .line 59
    const v6, 0x7f0f00c6

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 60
    .local v3, "settingUserInput":Landroid/widget/TextView;
    const v6, 0x7f0f00c8

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 61
    .local v2, "settingPasswordInput":Landroid/widget/TextView;
    const v6, 0x7f0f00c9

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 62
    .local v5, "tempPassword":Landroid/widget/CheckBox;
    const v6, 0x7f0f00cc

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 63
    .local v4, "sslOn":Landroid/widget/CheckBox;
    const v6, 0x7f0f00cd

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 65
    .local v0, "fakeLogin":Landroid/widget/CheckBox;
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->userName:Ljava/lang/String;

    .line 66
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->userPass:Ljava/lang/String;

    .line 67
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->manualHost:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->hostName:Ljava/lang/String;

    .line 69
    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->userName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->userPass:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->hostName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 70
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    const-string v7, "userPref"

    iget-object v8, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->userName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->userPass:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPassword(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    const-string v7, "hostPref"

    iget-object v8, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->hostName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 79
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 84
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    const-string v7, "debugSslOnPref"

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 85
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 86
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    const-string v7, "tokenPref"

    const-string v8, "4042634387:A:OJUSAP46:ms18:IMSTTD:11069"

    invoke-virtual {v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 91
    :cond_1
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    const-string v7, "portPref"

    const-string v8, "143"

    invoke-virtual {v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    const-string v7, "sslPortPref"

    const-string v8, "993"

    invoke-virtual {v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 96
    const/16 v6, 0x1d

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->setResult(I)V

    .line 97
    new-instance v1, Landroid/content/Intent;

    const-class v6, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {v1, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v6, 0x4000000

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->startActivity(Landroid/content/Intent;)V

    .line 100
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->finish()V

    .line 102
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 106
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->setResult(I)V

    .line 107
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onBackPressed()V

    .line 108
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v3, 0x7f04002a

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->setContentView(I)V

    .line 36
    const v3, 0x7f0f00cb

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->manualHost:Landroid/widget/TextView;

    .line 38
    const v3, 0x7f0f00ce

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 39
    .local v1, "login":Landroid/widget/Button;
    new-instance v3, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$1;

    invoke-direct {v3, p0}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$1;-><init>(Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const v3, 0x7f0f00ca

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    .line 48
    .local v2, "spinner":Landroid/widget/Spinner;
    const v3, 0x7f0d0004

    const v4, 0x1090008

    .line 49
    invoke-static {p0, v3, v4}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 51
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v3, 0x1090009

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 52
    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 53
    new-instance v3, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$MyOnItemSelectedListener;

    invoke-direct {v3, p0}, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen$MyOnItemSelectedListener;-><init>(Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 54
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->manualHost:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;->manualHost:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->refreshDrawableState()V

    .line 56
    return-void
.end method

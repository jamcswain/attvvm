.class Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment$1;
.super Ljava/lang/Object;
.source "PermissionsGoToSettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;->setButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment$1;->this$0:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v5, 0x10008000

    .line 48
    :try_start_0
    const-string v3, "PermissionsGoToSettingsFragment"

    const-string v4, "go to vvm permissions settings screen"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    .local v1, "intent":Landroid/content/Intent;
    const v3, 0x10008000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 52
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "package:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment$1;->this$0:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 53
    .local v2, "selfPckageUri":Landroid/net/Uri;
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 54
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment$1;->this$0:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    invoke-virtual {v3, v1}, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .end local v2    # "selfPckageUri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 55
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.settings.MANAGE_APPLICATIONS_SETTINGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 58
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 59
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment$1;->this$0:Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;

    invoke-virtual {v3, v1}, Lcom/att/mobile/android/vvm/screen/PermissionsGoToSettingsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

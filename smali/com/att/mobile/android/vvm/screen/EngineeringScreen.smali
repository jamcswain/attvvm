.class public Lcom/att/mobile/android/vvm/screen/EngineeringScreen;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "EngineeringScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EngineeringScreen"


# instance fields
.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCopyDB:Landroid/widget/Button;

.field protected mMinConfidenceSpinner:Landroid/widget/Spinner;

.field protected mTokenSpinner:Landroid/widget/Spinner;

.field protected mTranslSpinner:Landroid/widget/Spinner;

.field private prefs:Landroid/content/SharedPreferences;

.field private prefsEditor:Landroid/content/SharedPreferences$Editor;

.field private retryTokenErrorCheckBox:Landroid/widget/CheckBox;

.field private retryTranslErrorCheckBox:Landroid/widget/CheckBox;

.field private simulateTokenErrorCheckBox:Landroid/widget/CheckBox;

.field private simulateTranslErrorCheckBox:Landroid/widget/CheckBox;

.field private timerDefault:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mCopyDB:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/screen/EngineeringScreen;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private getSelectedError(Ljava/lang/String;)I
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 210
    const-class v3, Ljava/lang/String;

    const-string v4, "400"

    invoke-direct {p0, p1, v3, v4}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 211
    .local v0, "code":Ljava/lang/String;
    const/4 v2, -0x1

    .line 212
    .local v2, "matchingIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 213
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 214
    move v2, v1

    .line 218
    :cond_0
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .end local v2    # "matchingIndex":I
    :cond_1
    return v2

    .line 212
    .restart local v2    # "matchingIndex":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private declared-synchronized getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 349
    .local p2, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "defaultValue":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 350
    .local v2, "val":Ljava/lang/Object;
    invoke-virtual {p2, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 352
    .local v1, "res":Ljava/lang/Object;, "TT;"
    if-nez v1, :cond_0

    .line 356
    .end local v1    # "res":Ljava/lang/Object;, "TT;"
    .end local v2    # "val":Ljava/lang/Object;
    .end local p3    # "defaultValue":Ljava/lang/Object;, "TT;"
    :goto_0
    monitor-exit p0

    return-object p3

    .restart local v1    # "res":Ljava/lang/Object;, "TT;"
    .restart local v2    # "val":Ljava/lang/Object;
    .restart local p3    # "defaultValue":Ljava/lang/Object;, "TT;"
    :cond_0
    move-object p3, v1

    .line 352
    goto :goto_0

    .line 353
    .end local v1    # "res":Ljava/lang/Object;, "TT;"
    .end local v2    # "val":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 354
    .local v0, "cce":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "EngineeringScreen"

    const-string v4, "EngineeringScreen.getSharedPreferenceValue() exception"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 349
    .end local v0    # "cce":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 316
    .local p2, "value":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 318
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 341
    :goto_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 344
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    monitor-exit p0

    return-void

    .line 320
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_0
    :try_start_1
    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 321
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 322
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_1
    :try_start_2
    instance-of v0, p2, Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/Float;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 324
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_2
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 325
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 326
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_3
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 327
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 329
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_4
    const-string v0, "EngineeringScreen"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EngineeringScreen.setSharedPreference() - [key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] [value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]. T can be the one of the following types: Boolean, Integer, Float, Long, String."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method protected copyApplicationDBToSDCard()V
    .locals 2

    .prologue
    .line 151
    new-instance v0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;Lcom/att/mobile/android/vvm/screen/EngineeringScreen$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$ExportDatabaseFileTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 152
    return-void
.end method

.method protected initSpinnersList()V
    .locals 9

    .prologue
    const v8, 0x7f0800f4

    const v7, 0x1090009

    const v6, 0x1090008

    .line 158
    const v4, 0x7f0f00e3

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTokenSpinner:Landroid/widget/Spinner;

    .line 159
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    .line 160
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    const-string v5, "400"

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    const-string v5, "401"

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    const-string v5, "500"

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    const-string v5, "503"

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    const-string v5, "Unknown"

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    invoke-direct {v2, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 168
    .local v2, "tokendataAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v2, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 169
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTokenSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 170
    const-string v4, "TokenErrorCode"

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getSelectedError(Ljava/lang/String;)I

    move-result v0

    .line 171
    .local v0, "selection1":I
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTokenSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 173
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTokenSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v8}, Landroid/widget/Spinner;->setPromptId(I)V

    .line 174
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTokenSpinner:Landroid/widget/Spinner;

    new-instance v5, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$7;

    invoke-direct {v5, p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$7;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 188
    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->list:Ljava/util/List;

    invoke-direct {v3, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 190
    .local v3, "transldataAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v3, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 191
    const v4, 0x7f0f00e7

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Spinner;

    iput-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTranslSpinner:Landroid/widget/Spinner;

    .line 192
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTranslSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 193
    const-string v4, "TranslErrorCode"

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getSelectedError(Ljava/lang/String;)I

    move-result v1

    .line 194
    .local v1, "selection2":I
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTranslSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 195
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTranslSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4, v8}, Landroid/widget/Spinner;->setPromptId(I)V

    .line 196
    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mTranslSpinner:Landroid/widget/Spinner;

    new-instance v5, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$8;

    invoke-direct {v5, p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$8;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 208
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->timerDefault:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v1, "timerDefaultPref"

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->timerDefault:Landroid/widget/EditText;

    .line 224
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 223
    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 226
    :cond_0
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onBackPressed()V

    .line 227
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    .line 59
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const v7, 0x7f040038

    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->setContentView(I)V

    .line 63
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "vvmPreferences"

    invoke-virtual {v7, v8, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->prefs:Landroid/content/SharedPreferences;

    .line 66
    const v7, 0x7f0f00dc

    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iput-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->timerDefault:Landroid/widget/EditText;

    .line 67
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v7

    const-string v8, "timerDefaultPref"

    const-class v9, Ljava/lang/Integer;

    const/16 v10, 0x1e

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 68
    .local v6, "timeout":I
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->timerDefault:Landroid/widget/EditText;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 69
    const v7, 0x7f0f00de

    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    iput-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mCopyDB:Landroid/widget/Button;

    .line 70
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mCopyDB:Landroid/widget/Button;

    new-instance v8, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$1;

    invoke-direct {v8, p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$1;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v7, 0x7f0f00e0

    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Spinner;

    iput-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mMinConfidenceSpinner:Landroid/widget/Spinner;

    .line 81
    const/high16 v7, 0x7f0d0000

    const v8, 0x1090008

    invoke-static {p0, v7, v8}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 84
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v7, 0x1090009

    invoke-virtual {v0, v7}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 86
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mMinConfidenceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 87
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mMinConfidenceSpinner:Landroid/widget/Spinner;

    new-instance v8, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$2;

    invoke-direct {v8, p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$2;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    invoke-virtual {v7, v8}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 101
    const-string v7, "MinConfidence"

    const-class v8, Ljava/lang/String;

    const-string v9, "0.0"

    invoke-direct {p0, v7, v8, v9}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v5

    .line 102
    .local v5, "spinnerPosition":I
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->mMinConfidenceSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 103
    const-string v7, "simulatetokenerror"

    const-class v8, Ljava/lang/Boolean;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {p0, v7, v8, v9}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 104
    .local v3, "simulateTokenError":Z
    const v7, 0x7f0f00e1

    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->simulateTokenErrorCheckBox:Landroid/widget/CheckBox;

    .line 105
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->simulateTokenErrorCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 107
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->simulateTokenErrorCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$3;

    invoke-direct {v8, p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$3;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 114
    const-string v7, "TokenRetryFail"

    const-class v8, Ljava/lang/Boolean;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {p0, v7, v8, v9}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 115
    .local v1, "retryTokenError":Z
    const v7, 0x7f0f00e4

    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->retryTokenErrorCheckBox:Landroid/widget/CheckBox;

    .line 116
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->retryTokenErrorCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 118
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->retryTokenErrorCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$4;

    invoke-direct {v8, p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$4;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 124
    const-string v7, "simulatetranslerror"

    const-class v8, Ljava/lang/Boolean;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {p0, v7, v8, v9}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 125
    .local v4, "simulateTranslError":Z
    const v7, 0x7f0f00e5

    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->simulateTranslErrorCheckBox:Landroid/widget/CheckBox;

    .line 126
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->simulateTranslErrorCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 128
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->simulateTranslErrorCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$5;

    invoke-direct {v8, p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$5;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 134
    const-string v7, "TranslRetryFail"

    const-class v8, Ljava/lang/Boolean;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {p0, v7, v8, v9}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 135
    .local v2, "retryTranslError":Z
    const v7, 0x7f0f00e8

    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->retryTranslErrorCheckBox:Landroid/widget/CheckBox;

    .line 136
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->retryTranslErrorCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 138
    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->retryTranslErrorCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$6;

    invoke-direct {v8, p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen$6;-><init>(Lcom/att/mobile/android/vvm/screen/EngineeringScreen;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 144
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;->initSpinnersList()V

    .line 145
    return-void
.end method

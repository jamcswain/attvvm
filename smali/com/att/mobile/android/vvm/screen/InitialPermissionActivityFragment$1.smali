.class Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment$1;
.super Ljava/lang/Object;
.source "InitialPermissionActivityFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->initFontAndButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment$1;->this$0:Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 50
    const-string v1, "InitialPermissionActivityFragment"

    const-string v2, "request permissions"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-static {}, Lcom/att/mobile/android/infra/utils/PermissionUtils;->getNeededPermission()[Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "neededPermissions":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment$1;->this$0:Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;

    const/16 v2, 0xd7

    invoke-virtual {v1, v0, v2}, Lcom/att/mobile/android/vvm/screen/InitialPermissionActivityFragment;->requestPermissions([Ljava/lang/String;I)V

    .line 55
    :cond_0
    return-void
.end method

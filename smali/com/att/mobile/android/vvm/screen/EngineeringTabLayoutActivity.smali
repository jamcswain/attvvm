.class public Lcom/att/mobile/android/vvm/screen/EngineeringTabLayoutActivity;
.super Landroid/app/TabActivity;
.source "EngineeringTabLayoutActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f0200e4

    .line 17
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/att/mobile/android/vvm/screen/EngineeringTabLayoutActivity;->requestWindowFeature(I)Z

    .line 19
    const v5, 0x7f040039

    invoke-virtual {p0, v5}, Lcom/att/mobile/android/vvm/screen/EngineeringTabLayoutActivity;->setContentView(I)V

    .line 21
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/EngineeringTabLayoutActivity;->getTabHost()Landroid/widget/TabHost;

    move-result-object v4

    .line 24
    .local v4, "tabHost":Landroid/widget/TabHost;
    const-string v5, "Engineering"

    invoke-virtual {v4, v5}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v3

    .line 26
    .local v3, "engineeringScreenSpec":Landroid/widget/TabHost$TabSpec;
    const-string v5, "Engineering"

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/EngineeringTabLayoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    .line 27
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/att/mobile/android/vvm/screen/EngineeringScreen;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 28
    .local v2, "engineeringScreenIntent":Landroid/content/Intent;
    invoke-virtual {v3, v2}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    .line 32
    const-string v5, "ALU"

    invoke-virtual {v4, v5}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    .line 33
    .local v1, "aluSpec":Landroid/widget/TabHost$TabSpec;
    const-string v5, "ALU"

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/EngineeringTabLayoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)Landroid/widget/TabHost$TabSpec;

    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/att/mobile/android/vvm/screen/DebugSettingsScreen;

    invoke-direct {v0, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    .local v0, "aluIntent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    .line 39
    invoke-virtual {v4, v3}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 40
    invoke-virtual {v4, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 41
    return-void
.end method

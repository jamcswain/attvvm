.class Lcom/att/mobile/android/vvm/screen/WelcomeActivity$7;
.super Ljava/lang/Object;
.source "WelcomeActivity.java"

# interfaces
.implements Lcom/att/mobile/android/infra/utils/AlertDlgUtils$AlertDlgInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->showNoVoiceMailDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    .prologue
    .line 515
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$7;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleNegativeButton(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 530
    return-void
.end method

.method public handlePositiveButton(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 519
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 520
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$7;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 525
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/WelcomeActivity$7;->this$0:Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;->access$100(Lcom/att/mobile/android/vvm/screen/WelcomeActivity;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const/16 v3, 0x12

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 526
    return-void

    .line 521
    :catch_0
    move-exception v0

    .line 522
    .local v0, "anfe":Landroid/content/ActivityNotFoundException;
    const-string v2, "WelcomeActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NoVoiceMailDialog.onClick ActivityNotFoundException="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

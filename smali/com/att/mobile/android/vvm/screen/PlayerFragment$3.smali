.class Lcom/att/mobile/android/vvm/screen/PlayerFragment$3;
.super Ljava/lang/Object;
.source "PlayerFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/screen/PlayerFragment;->initButtonsListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    .prologue
    .line 1136
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 1148
    const-string v3, "PlayerFragment"

    const-string v4, "onProgressChanged()"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    if-eqz p3, :cond_0

    .line 1152
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 1153
    .local v2, "seekBarProgress":I
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    .line 1154
    .local v1, "seekBarMax":I
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$400(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/infra/media/AudioPlayer;->getMediaDuration()I

    move-result v3

    mul-int/2addr v3, v2

    div-int v0, v3, v1

    .line 1158
    .local v0, "playbackPosition":I
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$400(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/att/mobile/android/infra/media/AudioPlayer;->seekTo(I)V

    .line 1162
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$400(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)Lcom/att/mobile/android/infra/media/AudioPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/infra/media/AudioPlayer;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1163
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/PlayerFragment$3;->this$0:Lcom/att/mobile/android/vvm/screen/PlayerFragment;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/screen/PlayerFragment;->access$500(Lcom/att/mobile/android/vvm/screen/PlayerFragment;)V

    .line 1166
    .end local v0    # "playbackPosition":I
    .end local v1    # "seekBarMax":I
    .end local v2    # "seekBarProgress":I
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1173
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1180
    return-void
.end method

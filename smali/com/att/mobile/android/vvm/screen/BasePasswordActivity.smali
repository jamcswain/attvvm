.class public abstract Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;
.super Lcom/att/mobile/android/vvm/screen/VVMActivity;
.source "BasePasswordActivity.java"


# static fields
.field public static final FROM_SETTINGS:Ljava/lang/String; = "from_settings"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected btnContinue:Landroid/widget/Button;

.field protected enterPasswordImage:Landroid/widget/ImageView;

.field protected enterPasswordText:Landroid/widget/EditText;

.field protected errorTV:Landroid/widget/TextView;

.field protected isFromSettings:Z

.field protected maxDigit:I

.field protected minDigit:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->isFromSettings:Z

    return-void
.end method


# virtual methods
.method abstract changePassword(Ljava/lang/String;)V
.end method

.method protected clearError(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "setContinueButtonEnable"    # Ljava/lang/Boolean;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->errorTV:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0041

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 81
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->errorTV:Landroid/widget/TextView;

    const v1, 0x7f080163

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->btnContinue:Landroid/widget/Button;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 84
    return-void
.end method

.method abstract initConfirmPassword()V
.end method

.method protected initContinueButton()V
    .locals 2

    .prologue
    .line 106
    const v0, 0x7f0f00ba

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->btnContinue:Landroid/widget/Button;

    .line 107
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->btnContinue:Landroid/widget/Button;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 108
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->btnContinue:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 109
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->btnContinue:Landroid/widget/Button;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$2;-><init>(Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    return-void
.end method

.method protected initEnterPassword()V
    .locals 5

    .prologue
    .line 98
    const v0, 0x7f0f00b6

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    .line 99
    const v0, 0x7f0f00b7

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordImage:Landroid/widget/ImageView;

    .line 100
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    iget v4, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->maxDigit:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 101
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 102
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->setListeners()V

    .line 103
    return-void
.end method

.method protected initErrorTv()V
    .locals 4

    .prologue
    .line 86
    const v0, 0x7f0f00bb

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->errorTV:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->errorTV:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 88
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->errorTV:Landroid/widget/TextView;

    new-instance v1, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity$1;-><init>(Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 94
    return-void
.end method

.method protected initUI(I)V
    .locals 2
    .param p1, "layoutId"    # I

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->setContentView(I)V

    .line 145
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->initErrorTv()V

    .line 146
    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->isFromSettings:Z

    if-eqz v1, :cond_0

    const v0, 0x7f0800ba

    .line 147
    .local v0, "titleId":I
    :goto_0
    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->isFromSettings:Z

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->initActionBar(IZ)V

    .line 149
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->initEnterPassword()V

    .line 150
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->initContinueButton()V

    .line 151
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->initConfirmPassword()V

    .line 152
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->setSoftKeyboardVisibility(Z)V

    .line 153
    return-void

    .line 146
    .end local v0    # "titleId":I
    :cond_0
    const v0, 0x7f080156

    goto :goto_0
.end method

.method protected isConfirmPasswordValid(Landroid/widget/EditText;Landroid/widget/EditText;)Z
    .locals 3
    .param p1, "newPasswordEt"    # Landroid/widget/EditText;
    .param p2, "confirmPasswordEt"    # Landroid/widget/EditText;

    .prologue
    .line 165
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "conPassword":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 167
    .local v1, "newPassword":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 168
    const/4 v2, 0x1

    .line 170
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected isPasswordValid(Landroid/widget/EditText;I)Z
    .locals 2
    .param p1, "et"    # Landroid/widget/EditText;
    .param p2, "minPasswordLenght"    # I

    .prologue
    .line 157
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "password":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, p2, :cond_0

    .line 159
    const/4 v1, 0x0

    .line 161
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 54
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    :goto_0
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->isFromSettings:Z

    .line 56
    const v1, 0x7f08012f

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->minDigit:I

    .line 57
    const v1, 0x7f08011d

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->maxDigit:I

    .line 58
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 59
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 60
    return-void

    .line 54
    :cond_0
    const-string v2, "from_settings"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->dismissGauge()V

    .line 176
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 177
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 178
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->onDestroy()V

    .line 179
    return-void
.end method

.method protected setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "iv"    # Landroid/widget/ImageView;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 73
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected setError(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "iv"    # Landroid/widget/ImageView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->errorTV:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 65
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->errorTV:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->btnContinue:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 67
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020118

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->setBackground(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 68
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->errorTV:Landroid/widget/TextView;

    invoke-static {p1, v0}, Lcom/att/mobile/android/infra/utils/AccessibilityUtils;->sendEvent(Ljava/lang/String;Landroid/view/View;)V

    .line 69
    return-void
.end method

.method abstract setListeners()V
.end method

.method protected setSoftKeyboardVisibility(Z)V
    .locals 3
    .param p1, "isVisible"    # Z

    .prologue
    .line 129
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 130
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 131
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 132
    if-eqz p1, :cond_1

    .line 133
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/BasePasswordActivity;->enterPasswordText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

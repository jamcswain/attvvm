.class Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AboutVVMActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SettingsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;

    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 74
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 76
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;

    .line 77
    .local v0, "item":Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;
    if-nez p2, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f04001a

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 81
    :cond_0
    const v4, 0x7f0f0074

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 82
    .local v1, "tvHeader":Landroid/widget/TextView;
    const v4, 0x7f0f0075

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 83
    .local v2, "tvSummaryFirst":Landroid/widget/TextView;
    const v4, 0x7f0f0076

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 85
    .local v3, "tvSummarySecond":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->getHeader()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->getSummary()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 89
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 90
    const/4 v4, 0x1

    if-ne p1, v4, :cond_1

    .line 91
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 92
    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsItem;->getSummarySecondText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 101
    :cond_1
    return-object p2
.end method

.method public goToLink(Ljava/lang/String;)V
    .locals 4
    .param p1, "link"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-static {}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SettingsAdapter goToLink() url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 108
    .local v0, "browserIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;

    invoke-virtual {v1, v0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->startActivity(Landroid/content/Intent;)V

    .line 109
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 121
    :goto_0
    return-void

    .line 115
    :pswitch_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->res:Landroid/content/res/Resources;

    const v1, 0x7f0801de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;->goToLink(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :pswitch_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity;->res:Landroid/content/res/Resources;

    const v1, 0x7f0801e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/AboutVVMActivity$SettingsAdapter;->goToLink(Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :pswitch_data_0
    .packed-switch 0x7f0f0075
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.class public Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;
.super Landroid/os/AsyncTask;
.source "VmListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/VmListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RunDeleteByPhonesAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field mPhoneNumbers:[Ljava/lang/String;

.field final synthetic this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/screen/VmListActivity;[Ljava/lang/String;)V
    .locals 1
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/screen/VmListActivity;
    .param p2, "phoneNumbers"    # [Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 57
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;->mPhoneNumbers:[Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 62
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;->mPhoneNumbers:[Ljava/lang/String;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/screen/VmListActivity;->access$000(Lcom/att/mobile/android/vvm/screen/VmListActivity;)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessagesAsDeleted([Ljava/lang/String;Z)I

    move-result v1

    .line 63
    .local v1, "numberOfUpdatedMessage":I
    if-lez v1, :cond_0

    .line 64
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageUIDsToDelete()[Ljava/lang/Long;

    move-result-object v0

    .line 65
    .local v0, "messagesUIDs":[Ljava/lang/Long;
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/VmListActivity$RunDeleteByPhonesAsyncTask;->this$0:Lcom/att/mobile/android/vvm/screen/VmListActivity;

    invoke-virtual {v2, v3, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueDeleteOperation(Landroid/content/Context;[Ljava/lang/Long;)V

    .line 67
    .end local v0    # "messagesUIDs":[Ljava/lang/Long;
    :cond_0
    const/4 v2, 0x0

    return-object v2
.end method

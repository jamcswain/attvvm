.class public interface abstract Lcom/att/mobile/android/vvm/screen/PlayerActivity$IntentExtraNames;
.super Ljava/lang/Object;
.source "PlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/PlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IntentExtraNames"
.end annotation


# static fields
.field public static final CONTACT_URI:Ljava/lang/String; = "contactUri"

.field public static final CURRENT_MESSAGE:Ljava/lang/String; = "currentMessage"

.field public static final EXTRA_ID:Ljava/lang/String; = "id"

.field public static final FILTER_TYPE:Ljava/lang/String; = "filterType"

.field public static final IS_AUTO_PLAY_MODE:Ljava/lang/String; = "isAutoPlayMode"

.field public static final LAUNCHED_FROM_INBOX:Ljava/lang/String; = "launchedFromInbox"

.field public static final MESSAGE_POSITION:Ljava/lang/String; = "messagePosition"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phoneNumber"

.field public static final SENDER_DISPLAY_NAME:Ljava/lang/String; = "senderDisplayName"

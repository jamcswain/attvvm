.class public Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;
.super Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;
.source "GreetingRecorderActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GreetingRecorderActivity"


# instance fields
.field private greetingType:Ljava/lang/String;

.field private greetingUniqueId:Ljava/lang/String;

.field private imapRecordingGreetingType:Ljava/lang/String;

.field private imapSelectionGreetingType:Ljava/lang/String;

.field private isErrorDialogShown:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingUniqueId:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->imapSelectionGreetingType:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingType:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->isErrorDialogShown:Z

    return-void
.end method

.method private setRes(ILjava/lang/String;)V
    .locals 2
    .param p1, "res"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 175
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 176
    .local v0, "dataIntent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 177
    const-string v1, "data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    invoke-virtual {p0, p1, v0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->setResult(ILandroid/content/Intent;)V

    .line 183
    :goto_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->finish()V

    .line 184
    return-void

    .line 180
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private setUploadRecordingUIMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 166
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->recordButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 167
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 168
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->sendButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 171
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->showGauge(Ljava/lang/String;)V

    .line 172
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 107
    const/16 v1, 0x23

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->setRes(ILjava/lang/String;)V

    .line 109
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 110
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 111
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->releaseWakeLock()V

    .line 113
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->restoreDeviceAudioMode()V

    .line 114
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/VVMApplication;->setIsApplicationSpeakerOn(Z)V

    .line 115
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 60
    .local v0, "intentExtras":Landroid/os/Bundle;
    const-string v1, "MAX_RECORDING_MILSEC_DURATION"

    .line 61
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->maximumRecordDuration:I

    .line 62
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->maximumRecordDuration:I

    if-nez v1, :cond_0

    .line 63
    const-string v1, "GreetingRecorderActivity"

    const-string v2, "GreetingRecorderActivity.onCreate() - MAX_RECORDING_MILSEC_DURATION extra is missing in the launching intent or is equal to 0"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    iget v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->maximumRecordDuration:I

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->setTotalRecordingTime(I)V

    .line 66
    const-string v1, "GREETING_UNIQUE_ID"

    .line 67
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingUniqueId:Ljava/lang/String;

    .line 68
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingUniqueId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 69
    const-string v1, "GreetingRecorderActivity"

    const-string v2, "GreetingRecorderActivity.onCreate() - GREETING_UNIQUE_ID extra is missing in the launching intent"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :cond_1
    const-string v1, "IMAP_SELECTION_GREETING_TYPE"

    .line 75
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->imapSelectionGreetingType:Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->imapSelectionGreetingType:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 77
    const-string v1, "GreetingRecorderActivity"

    const-string v2, "GreetingRecorderActivity.onCreate() - IMAP_SELECTION_GREETING_TYPE extra is missing in the launching intent"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_2
    const-string v1, "IMAP_RECORDING_GREETING_TYPE"

    .line 83
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 85
    const-string v1, "GreetingRecorderActivity"

    const-string v2, "GreetingRecorderActivity.onCreate() - IMAP_RECORDING_GREETING_TYPE extra is missing in the launching intent"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_3
    const-string v1, "GREETING_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingType:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingType:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 92
    const-string v1, "GreetingRecorderActivity"

    const-string v2, "GreetingRecorderActivity.onCreate() - GREETING_TYPE extra is missing in the launching intent"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_4
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->updateRecordingProgressTextUIComponent()V

    .line 101
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 102
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 244
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 246
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->onDestroy()V

    .line 247
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 233
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->onPause()V

    .line 234
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 235
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 222
    invoke-super {p0}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->onResume()V

    .line 224
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->mustBackToSetupProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 228
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    goto :goto_0
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 3
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    const/16 v2, 0x2d

    .line 193
    const/16 v1, 0x2c

    if-ne p1, v1, :cond_0

    .line 196
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    const-string v1, "/private/vendor/vendor.alu/messaging/GreetingType"

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingType:Ljava/lang/String;

    .line 197
    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueSetMetaDataGreetingTypeOperation(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :goto_0
    return-void

    .line 202
    :cond_0
    const/16 v1, 0x2e

    if-ne p1, v1, :cond_2

    .line 205
    const/16 v1, 0x22

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingUniqueId:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingUniqueId:Ljava/lang/String;

    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->setRes(ILjava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->dismissGauge()V

    goto :goto_0

    .line 210
    :cond_2
    if-eq p1, v2, :cond_3

    const/16 v1, 0x2f

    if-ne p1, v1, :cond_5

    .line 212
    :cond_3
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingUniqueId:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->greetingUniqueId:Ljava/lang/String;

    :cond_4
    invoke-direct {p0, v2, v0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->setRes(ILjava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->dismissGauge()V

    goto :goto_0

    .line 216
    :cond_5
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method public sendButtonOnClickCallback(Landroid/view/View;)V
    .locals 8
    .param p1, "buttonView"    # Landroid/view/View;

    .prologue
    .line 123
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 124
    invoke-super {p0, p1}, Lcom/att/mobile/android/vvm/screen/AudioRecorderActivity;->sendButtonOnClickCallback(Landroid/view/View;)V

    .line 126
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->setUploadRecordingUIMode()V

    .line 128
    const/4 v3, 0x0

    .line 132
    .local v3, "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->getRecFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 133
    .local v1, "recordingAudioFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v5, v6

    .line 134
    .local v5, "recordingAudioFileSize":I
    new-array v2, v5, [B

    .line 135
    .local v2, "recordingAudioFileData":[B
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    .end local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .local v4, "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v4, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    if-eq v6, v5, :cond_1

    .line 138
    const-string v6, "GreetingRecorderActivity"

    const-string v7, "recording greeting audio file data couldn\'t be read"

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 147
    if-eqz v4, :cond_0

    .line 149
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 159
    .end local v1    # "recordingAudioFile":Ljava/io/File;
    .end local v2    # "recordingAudioFileData":[B
    .end local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .end local v5    # "recordingAudioFileSize":I
    :cond_0
    :goto_0
    return-void

    .line 150
    .restart local v1    # "recordingAudioFile":Ljava/io/File;
    .restart local v2    # "recordingAudioFileData":[B
    .restart local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "recordingAudioFileSize":I
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 143
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_3
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v6

    iget-object v7, p0, Lcom/att/mobile/android/vvm/screen/GreetingRecorderActivity;->imapRecordingGreetingType:Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueSendGreetingOperation(Ljava/lang/String;[B)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 147
    if-eqz v4, :cond_4

    .line 149
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move-object v3, v4

    .line 152
    .end local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 150
    .end local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    .line 151
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 152
    .end local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_0

    .line 144
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "recordingAudioFile":Ljava/io/File;
    .end local v2    # "recordingAudioFileData":[B
    .end local v5    # "recordingAudioFileSize":I
    :catch_2
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_5
    const-string v6, "GreetingRecorderActivity"

    const-string v7, "recorded greeting audio file couldn\'t be sent to server - "

    invoke-static {v6, v7, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 147
    if-eqz v3, :cond_0

    .line 149
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 150
    :catch_3
    move-exception v0

    .line 151
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 147
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v3, :cond_2

    .line 149
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 152
    :cond_2
    :goto_3
    throw v6

    .line 150
    :catch_4
    move-exception v0

    .line 151
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 157
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :cond_3
    const v6, 0x7f080145

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(II)V

    goto :goto_0

    .line 147
    .restart local v1    # "recordingAudioFile":Ljava/io/File;
    .restart local v2    # "recordingAudioFileData":[B
    .restart local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "recordingAudioFileSize":I
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 144
    .end local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v0

    move-object v3, v4

    .end local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    :cond_4
    move-object v3, v4

    .end local v4    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "recordingAudioFileInputStream":Ljava/io/FileInputStream;
    goto :goto_0
.end method

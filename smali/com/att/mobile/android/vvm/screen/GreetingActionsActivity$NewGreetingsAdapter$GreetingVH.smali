.class public Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "GreetingActionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GreetingVH"
.end annotation


# instance fields
.field private headerTxt:Landroid/widget/TextView;

.field private img:Landroid/widget/ImageView;

.field private subTxt:Landroid/widget/TextView;

.field final synthetic this$1:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$1"    # Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->this$1:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;

    .line 191
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 192
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 193
    const v0, 0x7f0f00f1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->headerTxt:Landroid/widget/TextView;

    .line 194
    const v0, 0x7f0f00f2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->subTxt:Landroid/widget/TextView;

    .line 195
    const v0, 0x7f0f00f3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->img:Landroid/widget/ImageView;

    .line 197
    new-instance v0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH$1;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH$1;-><init>(Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    return-void
.end method


# virtual methods
.method public bind(Lcom/att/mobile/android/vvm/model/greeting/Greeting;)V
    .locals 4
    .param p1, "currentGreeting"    # Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->headerTxt:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->subTxt:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->img:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->headerTxt:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->headerTxt:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 210
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->subTxt:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDesc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->subTxt:Landroid/widget/TextView;

    sget-object v1, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 212
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->img:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getIsSelected()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->itemView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 216
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getIsSelected()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->itemView:Landroid/view/View;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->headerTxt:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->subTxt:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->this$1:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    const v3, 0x7f0800c2

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 221
    :goto_1
    return-void

    .line 212
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->itemView:Landroid/view/View;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->headerTxt:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->subTxt:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter$GreetingVH;->this$1:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity$NewGreetingsAdapter;->this$0:Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;

    const v3, 0x7f0801e6

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/screen/GreetingActionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

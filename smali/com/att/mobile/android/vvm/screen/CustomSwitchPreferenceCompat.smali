.class public Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;
.super Landroid/support/v7/preference/SwitchPreferenceCompat;
.source "CustomSwitchPreferenceCompat.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private summaryView:Landroid/widget/TextView;

.field private switchView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/support/v7/preference/SwitchPreferenceCompat;-><init>(Landroid/content/Context;)V

    .line 42
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->mContext:Landroid/content/Context;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/support/v7/preference/SwitchPreferenceCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/preference/SwitchPreferenceCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v7/preference/SwitchPreferenceCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 27
    iput-object p1, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method private setTextContentDescription()V
    .locals 4

    .prologue
    .line 74
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->summaryView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 75
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0801d5

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "switchText":Ljava/lang/String;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->summaryView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->summaryView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 78
    .end local v0    # "switchText":Ljava/lang/String;
    :cond_0
    return-void

    .line 75
    :cond_1
    const v1, 0x7f0801d4

    goto :goto_0
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v7/preference/PreferenceViewHolder;)V
    .locals 7
    .param p1, "holder"    # Landroid/support/v7/preference/PreferenceViewHolder;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 48
    invoke-super {p0, p1}, Landroid/support/v7/preference/SwitchPreferenceCompat;->onBindViewHolder(Landroid/support/v7/preference/PreferenceViewHolder;)V

    .line 50
    const v3, 0x1020016

    invoke-virtual {p1, v3}, Landroid/support/v7/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 51
    .local v2, "titleView":Landroid/widget/TextView;
    if-eqz v2, :cond_0

    .line 52
    sget-object v3, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v3}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 53
    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v6, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 56
    :cond_0
    const v3, 0x1020010

    invoke-virtual {p1, v3}, Landroid/support/v7/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 57
    .local v1, "summView":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 58
    iput-object v1, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->summaryView:Landroid/widget/TextView;

    .line 59
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->summaryView:Landroid/widget/TextView;

    sget-object v4, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v4}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 60
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->summaryView:Landroid/widget/TextView;

    const/high16 v4, 0x41600000    # 14.0f

    invoke-virtual {v3, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 63
    :cond_1
    const v3, 0x7f0f0159

    invoke-virtual {p1, v3}, Landroid/support/v7/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    .line 64
    .local v0, "sView":Landroid/support/v7/widget/SwitchCompat;
    if-eqz v0, :cond_2

    .line 65
    iput-object v0, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->switchView:Landroid/view/View;

    .line 66
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->switchView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 67
    iget-object v3, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->switchView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 70
    :cond_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->setTextContentDescription()V

    .line 71
    return-void
.end method

.method protected onClick()V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0}, Landroid/support/v7/preference/SwitchPreferenceCompat;->onClick()V

    .line 83
    invoke-static {}, Lcom/att/mobile/android/infra/utils/AccessibilityUtils;->isAccessibilityActivated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    iget-object v2, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0801d7

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "accessibilityText":Ljava/lang/String;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->switchView:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/AccessibilityUtils;->sendEvent(Ljava/lang/String;Landroid/view/View;)V

    .line 86
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/screen/CustomSwitchPreferenceCompat;->setTextContentDescription()V

    .line 88
    .end local v0    # "accessibilityText":Ljava/lang/String;
    :cond_0
    return-void

    .line 84
    :cond_1
    const v1, 0x7f0801d6

    goto :goto_0
.end method

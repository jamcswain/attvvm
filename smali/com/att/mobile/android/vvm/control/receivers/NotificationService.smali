.class public Lcom/att/mobile/android/vvm/control/receivers/NotificationService;
.super Landroid/app/IntentService;
.source "NotificationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;
    }
.end annotation


# static fields
.field private static final NOTIFICATION_ID:I = 0x1

.field private static final TAG:Ljava/lang/String; = "NotificationService"

.field private static final UPDATE_AVAILABLE_NOTIFICATION_ID:I = 0x2


# instance fields
.field private applicationContext:Landroid/content/Context;

.field private hostFound:Z

.field private mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private notificationManager:Landroid/app/NotificationManager;

.field private passwordFoundInSMS:Z

.field private userFound:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 67
    const-string v0, "NotificationService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 57
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 58
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    .line 59
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->applicationContext:Landroid/content/Context;

    .line 61
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->hostFound:Z

    .line 62
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->userFound:Z

    .line 63
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->passwordFoundInSMS:Z

    .line 64
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->UNKNOWN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    .line 68
    return-void
.end method

.method private clearNotification()V
    .locals 2

    .prologue
    .line 815
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 816
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setNotificationsBeforeReboot(Ljava/lang/Boolean;)V

    .line 817
    return-void
.end method

.method private clearUpdateNotification()V
    .locals 2

    .prologue
    .line 820
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 821
    return-void
.end method

.method private extractHost(Ljava/lang/String;)V
    .locals 6
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 692
    const-string v3, "?"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 694
    .local v2, "queryStringStart":I
    if-lez v2, :cond_1

    .line 695
    invoke-virtual {p1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 697
    .local v1, "hostPortArr":[Ljava/lang/String;
    aget-object v0, v1, v5

    .line 699
    .local v0, "host":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 701
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v4, "hostPref"

    invoke-virtual {v3, v4, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 703
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->hostFound:Z

    .line 704
    const-string v3, "NotificationService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NotificationService.extractHost() , host = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "hostPortArr":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    const-string v3, "NotificationService"

    const-string v4, "extractHost() SMS text is empty cannot get details!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private extractParameters(Ljava/lang/String;)V
    .locals 6
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 717
    const-string v4, "?"

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 719
    .local v3, "queryStringStart":I
    if-lez v3, :cond_1

    .line 720
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 724
    .local v2, "queryString":Ljava/lang/String;
    const-string v4, "&"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 727
    .local v1, "paramsArr":[Ljava/lang/String;
    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v1, v4

    .line 728
    .local v0, "param":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 731
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->handleParameter(Ljava/lang/String;)V

    .line 727
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 735
    .end local v0    # "param":Ljava/lang/String;
    .end local v1    # "paramsArr":[Ljava/lang/String;
    .end local v2    # "queryString":Ljava/lang/String;
    :cond_1
    const-string v4, "NotificationService"

    const-string v5, "extractParameters() SMS text is empty cannot get details!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    :cond_2
    return-void
.end method

.method private handleIncomingSMS(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 571
    const-string v2, "NotificationService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "#### NotificationService::handleIncomingSMS text = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    :try_start_0
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->extractHost(Ljava/lang/String;)V

    .line 577
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->extractParameters(Ljava/lang/String;)V

    .line 579
    const-string v2, "NotificationService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "#### NotificationService::handleIncomingSMS mailboxStatus="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->hostFound:Z

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->userFound:Z

    if-eqz v2, :cond_7

    .line 584
    sget-object v2, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$1;->$SwitchMap$com$att$mobile$android$vvm$control$receivers$NotificationService$MailboxStatus:[I

    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 673
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSetupStarted(Ljava/lang/Boolean;)V

    .line 674
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 676
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0x40

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 683
    :goto_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->turnFlagsOff()V

    .line 689
    :goto_1
    return-void

    .line 587
    :pswitch_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getPassword()Ljava/lang/String;

    move-result-object v1

    .line 589
    .local v1, "password":Ljava/lang/String;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isFirstTimeUse()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 593
    if-nez v1, :cond_1

    .line 594
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, 0x4

    .line 595
    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 613
    :cond_0
    :goto_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notifyMailboxSetupRequired()V

    .line 616
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0x40

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 684
    .end local v1    # "password":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 685
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "NotificationService"

    const-string v3, "handleIncomingSMS() Exception while proccesing SMS header/details"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 596
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "password":Ljava/lang/String;
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->passwordFoundInSMS:Z

    if-eqz v2, :cond_0

    .line 597
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, -0x1

    .line 598
    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    goto :goto_2

    .line 621
    :cond_2
    if-nez v1, :cond_3

    .line 622
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, 0x4

    .line 623
    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    goto :goto_0

    .line 625
    :cond_3
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v2

    .line 626
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueFetchHeadersAndBodiesOperation()V

    goto :goto_0

    .line 639
    .end local v1    # "password":Ljava/lang/String;
    :pswitch_1
    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->passwordFoundInSMS:Z

    if-eqz v2, :cond_4

    .line 640
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, 0x2

    .line 641
    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 645
    :cond_4
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 646
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 647
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setNeedRefreshInbox(Ljava/lang/Boolean;)V

    .line 652
    :goto_3
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible()Z

    move-result v2

    if-nez v2, :cond_6

    .line 654
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notifyMailboxSetupRequired()V

    goto/16 :goto_0

    .line 649
    :cond_5
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setFirstTimeUse(Ljava/lang/Boolean;)V

    goto :goto_3

    .line 657
    :cond_6
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0x40

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    goto/16 :goto_0

    .line 663
    :pswitch_2
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 667
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notifyMailboxSetupRequired()V

    .line 670
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0x40

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    goto/16 :goto_0

    .line 680
    :cond_7
    const-string v2, "NotificationService"

    const-string v3, "handleIncomingSMS() Host and Mailbox were not found in SMS."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 584
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleParameter(Ljava/lang/String;)V
    .locals 11
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 752
    const/4 v2, 0x0

    .line 753
    .local v2, "paramKey":Ljava/lang/String;
    const/4 v3, 0x0

    .line 754
    .local v3, "paramValue":Ljava/lang/String;
    const/4 v1, 0x0

    .line 756
    .local v1, "p_v":[Ljava/lang/String;
    const-string v6, "="

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 757
    array-length v6, v1

    if-ne v6, v7, :cond_0

    .line 758
    aget-object v2, v1, v9

    .line 759
    aget-object v3, v1, v10

    .line 762
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 764
    const-string v6, "S"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 765
    invoke-direct {p0, v3}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->setMailboxStatus(Ljava/lang/String;)V

    .line 812
    :cond_0
    :goto_0
    return-void

    .line 773
    :cond_1
    const-string v6, "P"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 774
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v7, "userPref"

    const-class v8, Ljava/lang/String;

    const-string v9, ""

    .line 775
    invoke-virtual {v6, v7, v8, v9}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 778
    .local v5, "telephoneNumber":Ljava/lang/String;
    invoke-static {v3, v5}, Lcom/att/mobile/android/infra/utils/Crypto$IMAP4;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 779
    .local v0, "decryptedPassword":Ljava/lang/String;
    const-string v6, "NotificationService"

    const-string v7, "NotificationService.handleParameter() \'p\'"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v6, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPassword(Ljava/lang/String;)V

    .line 781
    iput-boolean v10, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->passwordFoundInSMS:Z

    goto :goto_0

    .line 784
    .end local v0    # "decryptedPassword":Ljava/lang/String;
    .end local v5    # "telephoneNumber":Ljava/lang/String;
    :cond_2
    const-string v6, "m"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 785
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v7, "userPref"

    invoke-virtual {v6, v7, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 787
    iput-boolean v10, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->userFound:Z

    .line 788
    const-string v6, "NotificationService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "NotificationService.handleParameter() \'m\', PREFERENCE_MAILBOX_NUMBER = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 791
    :cond_3
    const-string v6, "t"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 792
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v7, "tokenPref"

    invoke-virtual {v6, v7, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 794
    const-string v6, "NotificationService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "NotificationService.handleParameter() \'t\', token = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    iput-boolean v10, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->userFound:Z

    goto :goto_0

    .line 799
    :cond_4
    const-string v6, "i"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 800
    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 801
    .local v4, "ports":[Ljava/lang/String;
    if-eqz v4, :cond_0

    array-length v6, v4

    if-ne v6, v7, :cond_0

    .line 802
    const-string v6, "NotificationService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "NotificationService.handleParameter() \'i\', port = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v4, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ssl port = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v4, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v7, "portPref"

    aget-object v8, v4, v9

    invoke-virtual {v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 806
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v7, "sslPortPref"

    aget-object v8, v4, v10

    invoke-virtual {v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private notifyMailboxSetupRequired()V
    .locals 12

    .prologue
    const v11, 0x7f08011c

    const v9, 0x7f0200fe

    const v8, 0x7f0200f5

    const/4 v10, 0x1

    .line 253
    const/4 v5, 0x0

    .line 254
    .local v5, "tickerText":Ljava/lang/String;
    const/4 v1, 0x0

    .line 255
    .local v1, "contentText":Ljava/lang/String;
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v6, v10}, Landroid/app/NotificationManager;->cancel(I)V

    .line 257
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setNotificationsBeforeReboot(Ljava/lang/Boolean;)V

    .line 261
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 264
    .local v4, "notificationIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    sget-object v7, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->INITIALIZED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    if-ne v6, v7, :cond_0

    .line 265
    const v6, 0x7f080070

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 269
    :goto_0
    const v6, 0x7f08011a

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 271
    const-string v6, "com.att.mobile.android.vvm.MAILBOX_CREATED"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    const-class v6, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-virtual {v4, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 277
    const/high16 v6, 0x4000000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 279
    const/4 v6, 0x0

    const/high16 v7, 0x8000000

    invoke-static {p0, v6, v4, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 289
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_1

    .line 290
    new-instance v6, Landroid/app/Notification$Builder;

    invoke-direct {v6, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 291
    invoke-virtual {p0, v11}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 292
    invoke-virtual {v6, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 293
    invoke-virtual {v6, v5}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 294
    invoke-virtual {v6, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 295
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 296
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 297
    invoke-virtual {v6, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 298
    invoke-virtual {v6}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 312
    .local v3, "notification":Landroid/app/Notification;
    :goto_1
    iget v6, v3, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v3, Landroid/app/Notification;->defaults:I

    .line 313
    iget v6, v3, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v3, Landroid/app/Notification;->defaults:I

    .line 316
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v6, v10}, Landroid/app/NotificationManager;->cancel(I)V

    .line 318
    :try_start_0
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    :goto_2
    const-string v6, "NotificationService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notifyMailboxCreated() Showing notification: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    return-void

    .line 267
    .end local v0    # "contentIntent":Landroid/app/PendingIntent;
    .end local v3    # "notification":Landroid/app/Notification;
    :cond_0
    const v6, 0x7f08011b

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 300
    .restart local v0    # "contentIntent":Landroid/app/PendingIntent;
    :cond_1
    new-instance v6, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v6, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 301
    invoke-virtual {p0, v11}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 302
    invoke-virtual {v6, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 303
    invoke-virtual {v6, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 304
    invoke-virtual {v6, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 305
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 307
    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 308
    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .restart local v3    # "notification":Landroid/app/Notification;
    goto :goto_1

    .line 319
    :catch_0
    move-exception v2

    .line 322
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "NotificationService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notifyMailboxCreated() Exception occured: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private notifyNewMessages(Ljava/lang/String;J)V
    .locals 26
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "messageId"    # J

    .prologue
    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getNewMessagesCount()I

    move-result v14

    .line 343
    .local v14, "newMessagesCount":I
    if-nez v14, :cond_0

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    move-object/from16 v19, v0

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v19, v0

    const/16 v22, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setNotificationsBeforeReboot(Ljava/lang/Boolean;)V

    .line 346
    const-string v19, "NotificationService"

    const-string v22, "Cancel notification: no new messages"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :goto_0
    return-void

    .line 350
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v19, v0

    .line 351
    invoke-virtual/range {v19 .. v19}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isNotifyOnNewMessagesEnabled()Ljava/lang/Boolean;

    move-result-object v17

    .line 355
    .local v17, "notifyOnNewMessage":Ljava/lang/Boolean;
    if-eqz v17, :cond_1

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_11

    .line 356
    :cond_1
    const-string v19, "NotificationService"

    const-string v22, "Going to notify the user about new message, because the setting is set to ON"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v19, v0

    const/16 v22, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setNotificationsBeforeReboot(Ljava/lang/Boolean;)V

    .line 367
    new-instance v16, Landroid/content/Intent;

    invoke-direct/range {v16 .. v16}, Landroid/content/Intent;-><init>()V

    .line 370
    .local v16, "notificationIntent":Landroid/content/Intent;
    const/4 v5, 0x0

    .line 373
    .local v5, "contentText":Ljava/lang/StringBuilder;
    const/16 v18, 0x0

    .line 377
    .local v18, "tickerText":Ljava/lang/StringBuilder;
    new-instance v18, Ljava/lang/StringBuilder;

    .end local v18    # "tickerText":Ljava/lang/StringBuilder;
    const v19, 0x7f080141

    .line 378
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 379
    .restart local v18    # "tickerText":Ljava/lang/StringBuilder;
    const/16 v19, 0x20

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 381
    const/4 v13, 0x0

    .line 382
    .local v13, "messageCursor":Landroid/database/Cursor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 386
    .local v20, "when":J
    const-wide/16 v22, -0x1

    cmp-long v19, p2, v22

    if-lez v19, :cond_5

    .line 387
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessage(J)Landroid/database/Cursor;

    move-result-object v13

    .line 396
    :goto_1
    if-eqz v13, :cond_2

    .line 397
    const-string v19, "time"

    .line 398
    move-object/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 397
    move/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 399
    const-string v19, "phone_number"

    .line 401
    move-object/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 400
    move/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 402
    .local v12, "lastCallerPhoneNumber":Ljava/lang/String;
    const-string v19, "delivery_status"

    .line 403
    move-object/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 402
    move/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const/16 v22, 0x1

    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    const/4 v9, 0x1

    .line 404
    .local v9, "isDSN":Z
    :goto_2
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContactDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 408
    .local v11, "lastCallerName":Ljava/lang/String;
    if-eqz v11, :cond_7

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v19

    if-lez v19, :cond_7

    .line 409
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 423
    .local v10, "isSetupCompleted":Z
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v14, v0, :cond_e

    .line 427
    move-object/from16 v5, v18

    .line 431
    const-string v19, "com.att.mobile.android.vvm.LAUNCH_PLAYER_FROM_NOTIFICATION"

    .line 432
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 433
    if-nez v10, :cond_c

    .line 434
    const-class v19, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 438
    :goto_4
    const/high16 v19, 0x4000000

    .line 439
    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 441
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessageSaved(J)Z

    move-result v19

    if-eqz v19, :cond_d

    const/4 v8, 0x3

    .line 442
    .local v8, "filtertype":I
    :goto_5
    const-string v19, "id"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-wide/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 443
    const-string v19, "filterType"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 444
    const-string v19, "launchedFromInbox"

    const/16 v22, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 486
    .end local v8    # "filtertype":I
    .end local v9    # "isDSN":Z
    .end local v10    # "isSetupCompleted":Z
    .end local v11    # "lastCallerName":Ljava/lang/String;
    .end local v12    # "lastCallerPhoneNumber":Ljava/lang/String;
    :cond_2
    :goto_6
    if-eqz v13, :cond_3

    .line 487
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 491
    :cond_3
    :goto_7
    const/16 v19, 0x0

    const/high16 v22, 0x8000000

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v16

    move/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 494
    .local v4, "contentIntent":Landroid/app/PendingIntent;
    const/4 v15, 0x0

    .line 509
    .local v15, "notification":Landroid/app/Notification;
    sget v19, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v22, 0x15

    move/from16 v0, v19

    move/from16 v1, v22

    if-lt v0, v1, :cond_10

    .line 510
    new-instance v19, Landroid/app/Notification$Builder;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v22, 0x7f08014f

    .line 511
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v19

    .line 512
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v19

    .line 513
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v19

    const v22, -0xffff01

    const/16 v23, 0x1f4

    const/16 v24, 0x1f4

    .line 514
    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Notification$Builder;->setLights(III)Landroid/app/Notification$Builder;

    move-result-object v19

    const v22, 0x7f0200f5

    .line 515
    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v19

    .line 516
    invoke-virtual/range {p0 .. p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0200fe

    invoke-static/range {v22 .. v23}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v19

    .line 517
    invoke-virtual/range {v19 .. v21}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v19

    .line 518
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v19

    .line 519
    invoke-virtual/range {v19 .. v19}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v15

    .line 534
    :goto_8
    const-string v19, "com.att.mobile.android.vvm.NEW_UNREAD_MESSAGE"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 536
    iget v0, v15, Landroid/app/Notification;->defaults:I

    move/from16 v19, v0

    or-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    iput v0, v15, Landroid/app/Notification;->defaults:I

    .line 537
    iget v0, v15, Landroid/app/Notification;->defaults:I

    move/from16 v19, v0

    or-int/lit8 v19, v19, 0x2

    move/from16 v0, v19

    iput v0, v15, Landroid/app/Notification;->defaults:I

    .line 541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    move-object/from16 v19, v0

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 545
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    move-object/from16 v19, v0

    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1, v15}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 551
    :goto_9
    const-string v19, "NotificationService"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "newMessageNotification() Showing notification: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 391
    .end local v4    # "contentIntent":Landroid/app/PendingIntent;
    .end local v15    # "notification":Landroid/app/Notification;
    :cond_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getLastUnreadMessage()Landroid/database/Cursor;

    move-result-object v13

    .line 392
    const-string v19, "_id"

    .line 393
    move-object/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 392
    move/from16 v0, v19

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p2

    goto/16 :goto_1

    .line 402
    .restart local v12    # "lastCallerPhoneNumber":Ljava/lang/String;
    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 410
    .restart local v9    # "isDSN":Z
    .restart local v11    # "lastCallerName":Ljava/lang/String;
    :cond_7
    if-eqz v12, :cond_8

    const v19, 0x7f0801fb

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 411
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    .line 476
    .end local v9    # "isDSN":Z
    .end local v11    # "lastCallerName":Ljava/lang/String;
    .end local v12    # "lastCallerPhoneNumber":Ljava/lang/String;
    :catch_0
    move-exception v7

    move-object v6, v5

    .line 477
    .end local v5    # "contentText":Ljava/lang/StringBuilder;
    .local v6, "contentText":Ljava/lang/StringBuilder;
    .local v7, "e":Ljava/lang/Throwable;
    :try_start_3
    invoke-direct/range {p0 .. p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->clearNotification()V

    .line 478
    new-instance v5, Ljava/lang/StringBuilder;

    const v19, 0x7f08013d

    .line 479
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 480
    .end local v6    # "contentText":Ljava/lang/StringBuilder;
    .restart local v5    # "contentText":Ljava/lang/StringBuilder;
    :try_start_4
    const-string v19, "com.att.mobile.android.vvm.LAUNCH_INBOX_FROM_NOTIFICATION"

    .line 481
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    const-class v19, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 483
    const/high16 v19, 0x4000000

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 486
    if-eqz v13, :cond_3

    .line 487
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_7

    .line 412
    .end local v7    # "e":Ljava/lang/Throwable;
    .restart local v9    # "isDSN":Z
    .restart local v11    # "lastCallerName":Ljava/lang/String;
    .restart local v12    # "lastCallerPhoneNumber":Ljava/lang/String;
    :cond_8
    if-eqz v12, :cond_a

    :try_start_5
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v19

    if-lez v19, :cond_a

    .line 413
    const v19, 0x7f0800d8

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v12, v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_3

    .line 486
    .end local v9    # "isDSN":Z
    .end local v11    # "lastCallerName":Ljava/lang/String;
    .end local v12    # "lastCallerPhoneNumber":Ljava/lang/String;
    :catchall_0
    move-exception v19

    :goto_a
    if-eqz v13, :cond_9

    .line 487
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v19

    .line 414
    .restart local v9    # "isDSN":Z
    .restart local v11    # "lastCallerName":Ljava/lang/String;
    .restart local v12    # "lastCallerPhoneNumber":Ljava/lang/String;
    :cond_a
    if-eqz v9, :cond_b

    .line 415
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->applicationContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v22, 0x7f0800e7

    .line 416
    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 415
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 418
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->applicationContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v22, 0x7f0801af

    .line 419
    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 418
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 436
    .restart local v10    # "isSetupCompleted":Z
    :cond_c
    const-class v19, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_4

    .line 441
    :cond_d
    const/4 v8, 0x1

    goto/16 :goto_5

    .line 449
    :cond_e
    new-instance v19, Ljava/lang/StringBuilder;

    const v22, 0x7f080140

    .line 450
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/16 v22, 0x20

    .line 451
    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 452
    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v22, 0x20

    .line 453
    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v19

    const v22, 0x7f08013f

    .line 454
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 460
    const-string v19, "com.att.mobile.android.vvm.LAUNCH_INBOX_FROM_NOTIFICATION"

    .line 461
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 462
    if-nez v10, :cond_f

    .line 463
    const-class v19, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 472
    :goto_b
    const/high16 v19, 0x4000000

    .line 473
    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_6

    .line 465
    :cond_f
    const-class v19, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_b

    .line 521
    .end local v9    # "isDSN":Z
    .end local v10    # "isSetupCompleted":Z
    .end local v11    # "lastCallerName":Ljava/lang/String;
    .end local v12    # "lastCallerPhoneNumber":Ljava/lang/String;
    .restart local v4    # "contentIntent":Landroid/app/PendingIntent;
    .restart local v15    # "notification":Landroid/app/Notification;
    :cond_10
    new-instance v19, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v22, 0x7f08014f

    .line 522
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 523
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 524
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    const v22, -0xffff01

    const/16 v23, 0x1f4

    const/16 v24, 0x1f4

    .line 525
    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setLights(III)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    const v22, 0x7f0200f5

    .line 526
    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 527
    invoke-virtual/range {p0 .. p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0200fe

    invoke-static/range {v22 .. v23}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 528
    invoke-virtual/range {v19 .. v21}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 529
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v19

    .line 530
    invoke-virtual/range {v19 .. v19}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v15

    goto/16 :goto_8

    .line 546
    :catch_1
    move-exception v7

    .line 547
    .local v7, "e":Ljava/lang/Exception;
    const-string v19, "NotificationService"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "notifyMailboxCreated() Exception occured: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    .line 554
    .end local v4    # "contentIntent":Landroid/app/PendingIntent;
    .end local v5    # "contentText":Ljava/lang/StringBuilder;
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v13    # "messageCursor":Landroid/database/Cursor;
    .end local v15    # "notification":Landroid/app/Notification;
    .end local v16    # "notificationIntent":Landroid/content/Intent;
    .end local v18    # "tickerText":Ljava/lang/StringBuilder;
    .end local v20    # "when":J
    :cond_11
    const-string v19, "NotificationService"

    const-string v22, "New message notification will not be shown, because the setting is OFF"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 486
    .restart local v6    # "contentText":Ljava/lang/StringBuilder;
    .local v7, "e":Ljava/lang/Throwable;
    .restart local v13    # "messageCursor":Landroid/database/Cursor;
    .restart local v16    # "notificationIntent":Landroid/content/Intent;
    .restart local v18    # "tickerText":Ljava/lang/StringBuilder;
    .restart local v20    # "when":J
    :catchall_1
    move-exception v19

    move-object v5, v6

    .end local v6    # "contentText":Ljava/lang/StringBuilder;
    .restart local v5    # "contentText":Ljava/lang/StringBuilder;
    goto/16 :goto_a
.end method

.method private notifyPasswordMissmatch()V
    .locals 13

    .prologue
    const v12, 0x7f0200fe

    const v11, 0x7f0200f5

    const v10, -0xffff01

    const/16 v9, 0x1f4

    const/4 v8, 0x1

    .line 175
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v6, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 177
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setNotificationsBeforeReboot(Ljava/lang/Boolean;)V

    .line 178
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setNeedRefreshInbox(Ljava/lang/Boolean;)V

    .line 182
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 186
    .local v4, "notificationIntent":Landroid/content/Intent;
    const v6, 0x7f08015c

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 190
    .local v5, "tickerText":Ljava/lang/String;
    const v6, 0x7f08015a

    invoke-virtual {p0, v6}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 192
    .local v1, "contentText":Ljava/lang/String;
    const-string v6, "com.att.mobile.android.vvm.MAILBOX_CREATED"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-class v6, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-virtual {v4, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 198
    const/high16 v6, 0x4000000

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 200
    const/4 v6, 0x0

    const/high16 v7, 0x8000000

    invoke-static {p0, v6, v4, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 210
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_0

    .line 211
    new-instance v6, Landroid/app/Notification$Builder;

    invoke-direct {v6, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f08015b

    .line 212
    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 213
    invoke-virtual {v6, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 214
    invoke-virtual {v6, v5}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 215
    invoke-virtual {v6, v10, v9, v9}, Landroid/app/Notification$Builder;->setLights(III)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 216
    invoke-virtual {v6, v11}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 217
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v12}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 218
    invoke-virtual {v6, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 219
    invoke-virtual {v6}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 232
    .local v3, "notification":Landroid/app/Notification;
    :goto_0
    iget v6, v3, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v3, Landroid/app/Notification;->defaults:I

    .line 233
    iget v6, v3, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v3, Landroid/app/Notification;->defaults:I

    .line 236
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v6, v8}, Landroid/app/NotificationManager;->cancel(I)V

    .line 238
    :try_start_0
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :goto_1
    const-string v6, "NotificationService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notifyPasswordMissmatch() Showing notification: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    return-void

    .line 221
    .end local v3    # "notification":Landroid/app/Notification;
    :cond_0
    new-instance v6, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v6, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f08015b

    .line 222
    invoke-virtual {p0, v7}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 223
    invoke-virtual {v6, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 224
    invoke-virtual {v6, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 225
    invoke-virtual {v6, v10, v9, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setLights(III)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 226
    invoke-virtual {v6, v11}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 227
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v12}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 228
    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 229
    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .restart local v3    # "notification":Landroid/app/Notification;
    goto :goto_0

    .line 239
    :catch_0
    move-exception v2

    .line 240
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "NotificationService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notifyMailboxCreated() Exception occured: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setMailboxStatus(Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 831
    const-string v0, "C"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 832
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->CREATED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    .line 844
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMailBoxStatus(Ljava/lang/String;)V

    .line 845
    return-void

    .line 833
    :cond_1
    const-string v0, "I"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 834
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->INITIALIZED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    goto :goto_0

    .line 835
    :cond_2
    const-string v0, "N"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 836
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->NO_SUCH_MAILBOX:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    goto :goto_0

    .line 837
    :cond_3
    const-string v0, "R"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 838
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->PASSWORD_RESET_BY_ADMIN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    goto :goto_0

    .line 839
    :cond_4
    const-string v0, "U"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 840
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->UNKNOWN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    goto :goto_0

    .line 841
    :cond_5
    const-string v0, "D"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 842
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->DEACTIVATED_ENHANCED_SERVICES:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->mailboxStatus:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    goto :goto_0
.end method

.method private turnFlagsOff()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 848
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->hostFound:Z

    .line 849
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->userFound:Z

    .line 850
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 167
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 73
    const-string v0, "NotificationService"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 75
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->applicationContext:Landroid/content/Context;

    .line 76
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notificationManager:Landroid/app/NotificationManager;

    .line 78
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createInstance(Landroid/content/Context;)V

    .line 79
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 81
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v8, -0x1

    const/4 v7, 0x2

    .line 85
    if-eqz p1, :cond_0

    .line 86
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "action":Ljava/lang/String;
    const-string v4, "NotificationService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onHandleIntent() action = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    if-eqz v0, :cond_0

    .line 90
    const-string v4, "com.att.mobile.android.vvm.ACTION_NEW_MESSAGE_NOTIFICATION"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 91
    const-string v4, "NotificationService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onHandleIntent() isSetupStarted = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupStarted()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " isSetupCompleted="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupStarted()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    .line 93
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notifyMailboxSetupRequired()V

    .line 163
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 95
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    const-string v4, "android.intent.extra.TEXT"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->handleIncomingSMS(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_2
    const-string v4, "com.att.mobile.android.vvm.BOOT_COMPLETED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 100
    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v4

    if-eq v4, v7, :cond_3

    .line 103
    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 104
    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->hadNotificationsBeforeReboot()Ljava/lang/Boolean;

    move-result-object v1

    .line 105
    .local v1, "hadNotificationsBeforeReboot":Ljava/lang/Boolean;
    const-string v4, "NotificationService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onHandleIntent() NOTIFICATIONS_EXIST_BEFORE_REBOOT = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 112
    invoke-static {p0}, Lcom/att/mobile/android/infra/sim/SimManager;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/att/mobile/android/infra/sim/SimManager;->validateSim()Lcom/att/mobile/android/infra/sim/SimValidationResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->isSimSwapped()Z

    move-result v4

    if-nez v4, :cond_0

    .line 113
    invoke-direct {p0, v0, v8, v9}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notifyNewMessages(Ljava/lang/String;J)V

    goto :goto_0

    .line 120
    .end local v1    # "hadNotificationsBeforeReboot":Ljava/lang/Boolean;
    :cond_3
    const-string v4, "com.att.mobile.android.vvm.INTENT_CLEAR_NOTIFICATION"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 121
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->clearNotification()V

    goto :goto_0

    .line 122
    :cond_4
    const-string v4, "com.att.mobile.android.vvm.INTENT_CLEAR_UPDATE_NOTIFICATION"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 123
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->clearUpdateNotification()V

    goto :goto_0

    .line 124
    :cond_5
    const-string v4, "com.att.mobile.android.vvm.INTENT_CLEAR_ALL_NOTIFICATIONS"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 125
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->clearNotification()V

    .line 126
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->clearUpdateNotification()V

    goto :goto_0

    .line 132
    :cond_6
    const-string v4, "com.att.mobile.android.vvm.NEW_UNREAD_MESSAGE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 133
    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v4

    if-eq v4, v7, :cond_7

    .line 134
    const-string v4, "extra_new_message_row_id"

    invoke-virtual {p1, v4, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 136
    .local v2, "messageId":J
    invoke-direct {p0, v0, v2, v3}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notifyNewMessages(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 139
    .end local v2    # "messageId":J
    :cond_7
    const-string v4, "com.att.mobile.android.vvm.ACTION_UPDATE_NOTIFICATION_AFTER_REFRESH"

    .line 140
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "com.att.mobile.android.vvm.ACTION_UPDATE_NOTIFICATION"

    .line 141
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_8
    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 142
    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v4

    if-eq v4, v7, :cond_9

    .line 143
    invoke-direct {p0, v0, v8, v9}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notifyNewMessages(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 146
    :cond_9
    const-string v4, "com.att.mobile.android.vvm.ACTION_PASSWORD_MISSMATCH"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 147
    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v4

    if-eq v4, v7, :cond_0

    .line 148
    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMailBoxStatus()Ljava/lang/String;

    move-result-object v4

    const-string v5, "C"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMailBoxStatus()Ljava/lang/String;

    move-result-object v4

    const-string v5, "R"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 149
    :cond_a
    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 154
    :goto_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible()Z

    move-result v4

    if-nez v4, :cond_c

    .line 155
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->notifyPasswordMissmatch()V

    goto/16 :goto_0

    .line 152
    :cond_b
    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_1

    .line 157
    :cond_c
    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v5, 0x40

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    goto/16 :goto_0
.end method

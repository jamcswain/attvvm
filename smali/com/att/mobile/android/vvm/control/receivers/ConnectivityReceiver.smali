.class public Lcom/att/mobile/android/vvm/control/receivers/ConnectivityReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConnectivityReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/att/mobile/android/vvm/control/receivers/ConnectivityReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/ConnectivityReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 25
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-nez v3, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    sget-object v3, Lcom/att/mobile/android/vvm/control/receivers/ConnectivityReceiver;->TAG:Ljava/lang/String;

    const-string v4, "ConnectivityActionReceiver started"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    .line 35
    .local v1, "modelManager":Lcom/att/mobile/android/vvm/model/db/ModelManager;
    invoke-static {}, Lcom/att/mobile/android/infra/utils/Utils;->isNetworkAvailable()Z

    move-result v0

    .line 36
    .local v0, "isConnected":Z
    const-string v3, "com.att.isconnectedtointernet"

    const-class v4, Ljava/lang/Boolean;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 37
    .local v2, "wasConnected":Z
    if-eqz v0, :cond_2

    .line 39
    if-nez v2, :cond_0

    .line 40
    const-string v3, "com.att.isconnectedtointernet"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 42
    const/16 v3, 0x47

    invoke-virtual {v1, v3, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 46
    sget-object v3, Lcom/att/mobile/android/vvm/control/receivers/ConnectivityReceiver;->TAG:Ljava/lang/String;

    const-string v4, "connectivity restored - refreshing from server"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 52
    :cond_2
    if-eqz v2, :cond_0

    .line 53
    const-string v3, "com.att.isconnectedtointernet"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    sget-object v3, Lcom/att/mobile/android/vvm/control/receivers/ConnectivityReceiver;->TAG:Ljava/lang/String;

    const-string v4, "connectivity lost"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/16 v3, 0x46

    invoke-virtual {v1, v3, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

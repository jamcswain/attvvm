.class public Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LowMemoryReceiver.java"


# static fields
.field private static DYNAMIC_REG_INSTANCE:Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver; = null

.field private static DYNAMIC_REG_INTENT_FILTER:Landroid/content/IntentFilter; = null

.field private static final TAG:Ljava/lang/String; = "LowMemoryReceiver"

.field private static isReceiverRegistered:Z


# instance fields
.field private action:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->DYNAMIC_REG_INSTANCE:Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;

    .line 28
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->DYNAMIC_REG_INTENT_FILTER:Landroid/content/IntentFilter;

    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->isReceiverRegistered:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->action:Ljava/lang/String;

    return-void
.end method

.method private handleMemoryFlag()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "LowMemoryReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LowMemoryReceiver.handleNoMemory() action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->action:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->action:Ljava/lang/String;

    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->setMemoryLow(Z)V

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->setMemoryLow(Z)V

    goto :goto_0
.end method

.method public static declared-synchronized registerReceiverDynamicly(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    const-class v1, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->isReceiverRegistered:Z

    if-nez v0, :cond_0

    .line 83
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->DYNAMIC_REG_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 84
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->DYNAMIC_REG_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 86
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->DYNAMIC_REG_INSTANCE:Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;

    sget-object v2, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->DYNAMIC_REG_INTENT_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 88
    const/4 v0, 0x1

    sput-boolean v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->isReceiverRegistered:Z

    .line 89
    const-string v0, "LowMemoryReceiver"

    const-string v2, "registerReceiverDynamicly() LowMemoryReceiver has been dinamicly registered."

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    :cond_0
    monitor-exit v1

    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized unregisterReceiverDynamicly(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    const-class v1, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->isReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 103
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->DYNAMIC_REG_INSTANCE:Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 105
    const/4 v0, 0x0

    sput-boolean v0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->isReceiverRegistered:Z

    .line 106
    const-string v0, "LowMemoryReceiver"

    const-string v2, "unregisterReceiverDynamicly() LowMemoryReceiver has been dinamicly unregistered."

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :cond_0
    monitor-exit v1

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 36
    const-string v0, "LowMemoryReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LowMemoryReceiver.onReceive() ACTION = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->action:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->action:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->action:Ljava/lang/String;

    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->action:Ljava/lang/String;

    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    .line 49
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    :cond_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->handleMemoryFlag()V

    goto :goto_0
.end method

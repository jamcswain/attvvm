.class public final enum Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;
.super Ljava/lang/Enum;
.source "NotificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/receivers/NotificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MailboxStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

.field public static final enum CREATED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

.field public static final enum DEACTIVATED_ENHANCED_SERVICES:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

.field public static final enum INITIALIZED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

.field public static final enum NO_SUCH_MAILBOX:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

.field public static final enum PASSWORD_RESET_BY_ADMIN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

.field public static final enum UNKNOWN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 827
    new-instance v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v3}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->CREATED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    new-instance v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v4}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->INITIALIZED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    new-instance v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->UNKNOWN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    new-instance v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    const-string v1, "NO_SUCH_MAILBOX"

    invoke-direct {v0, v1, v6}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->NO_SUCH_MAILBOX:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    new-instance v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    const-string v1, "PASSWORD_RESET_BY_ADMIN"

    invoke-direct {v0, v1, v7}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->PASSWORD_RESET_BY_ADMIN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    new-instance v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    const-string v1, "DEACTIVATED_ENHANCED_SERVICES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->DEACTIVATED_ENHANCED_SERVICES:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    .line 826
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->CREATED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->INITIALIZED:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->UNKNOWN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->NO_SUCH_MAILBOX:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->PASSWORD_RESET_BY_ADMIN:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->DEACTIVATED_ENHANCED_SERVICES:Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->$VALUES:[Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 826
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 826
    const-class v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    return-object v0
.end method

.method public static values()[Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;
    .locals 1

    .prologue
    .line 826
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->$VALUES:[Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    invoke-virtual {v0}, [Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/att/mobile/android/vvm/control/receivers/NotificationService$MailboxStatus;

    return-object v0
.end method

.class public Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;
.super Landroid/database/ContentObserver;
.source "ContactsContentObserver.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/IEventDispatcher;


# static fields
.field private static final TAG:Ljava/lang/String; = "ContactsContentObserver"

.field private static instance:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

.field private static lock:Ljava/lang/Object;


# instance fields
.field private dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

.field private isRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->lock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 18
    new-instance v0, Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->isRegistered:Z

    .line 42
    return-void
.end method

.method public static createInstance(Landroid/os/Handler;)V
    .locals 2
    .param p0, "handler"    # Landroid/os/Handler;

    .prologue
    .line 25
    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 26
    :try_start_0
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->instance:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;-><init>(Landroid/os/Handler;)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->instance:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    .line 29
    :cond_0
    monitor-exit v1

    .line 30
    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance()Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->instance:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    if-nez v0, :cond_0

    .line 34
    const-string v0, "ContactsContentObserver"

    const-string v1, "must call create instance before calling getinstance"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_0
    sget-object v0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->instance:Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    return-object v0
.end method


# virtual methods
.method public addEventListener(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 55
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 56
    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 57
    :try_start_0
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->isRegistered:Z

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    .line 60
    invoke-static {}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->getInstance()Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    move-result-object v4

    .line 58
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->isRegistered:Z

    .line 63
    :cond_0
    monitor-exit v1

    .line 64
    return-void

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/Dispatcher;->addListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 69
    return-void
.end method

.method public notifyListeners(ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 115
    return-void
.end method

.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 46
    const/16 v0, 0x30

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->notifyListeners(ILjava/util/ArrayList;)V

    .line 47
    return-void
.end method

.method public removeEventListener(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 77
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 78
    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 81
    invoke-static {}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->getInstance()Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    move-result-object v2

    .line 80
    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->isRegistered:Z

    .line 84
    :cond_0
    monitor-exit v1

    .line 85
    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/Dispatcher;->removeListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 90
    return-void
.end method

.method public removeEventListeners()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;->removeListeners()V

    .line 110
    return-void
.end method

.method public removeEventListeners(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->removeEventListeners()V

    .line 98
    sget-object v1, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 101
    invoke-static {}, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->getInstance()Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;

    move-result-object v2

    .line 100
    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/receivers/ContactsContentObserver;->isRegistered:Z

    .line 104
    :cond_0
    monitor-exit v1

    .line 105
    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

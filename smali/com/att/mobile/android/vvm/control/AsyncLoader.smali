.class public Lcom/att/mobile/android/vvm/control/AsyncLoader;
.super Ljava/lang/Object;
.source "AsyncLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;,
        Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;,
        Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;,
        Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private dataLoaderThread:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;

.field private dataQueue:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

.field private final defaultAvatar:Landroid/graphics/drawable/Drawable;

.field private lookupKeysCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private namesCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private photosCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->photosCache:Ljava/util/HashMap;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->namesCache:Ljava/util/HashMap;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->lookupKeysCache:Ljava/util/HashMap;

    .line 55
    new-instance v0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;-><init>(Lcom/att/mobile/android/vvm/control/AsyncLoader$1;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataQueue:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    .line 63
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->context:Landroid/content/Context;

    .line 65
    new-instance v0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;

    invoke-direct {v0, p0, p1}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;-><init>(Lcom/att/mobile/android/vvm/control/AsyncLoader;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataLoaderThread:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;

    .line 66
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataLoaderThread:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->setPriority(I)V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->defaultAvatar:Landroid/graphics/drawable/Drawable;

    .line 70
    return-void
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/AsyncLoader;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataQueue:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    return-object v0
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/control/AsyncLoader;Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/AsyncLoader;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->updateContactCaches(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/AsyncLoader;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->namesCache:Ljava/util/HashMap;

    return-object v0
.end method

.method private queueData(JLjava/lang/String;Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "phoneNumber"    # Ljava/lang/String;
    .param p4, "imageView"    # Landroid/widget/ImageView;
    .param p5, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 115
    new-instance v1, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;-><init>(JLjava/lang/String;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    .line 116
    .local v1, "p":Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataQueue:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v2

    monitor-enter v2

    .line 117
    :try_start_0
    const-string v0, "***AsyncLoader***"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pushed into queue id = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataQueue:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataQueue:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 121
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataLoaderThread:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v2, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v2, :cond_0

    .line 126
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataLoaderThread:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->start()V

    .line 127
    :cond_0
    return-void

    .line 121
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private updateContactCaches(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 20
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 130
    const/4 v9, 0x0

    .line 131
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 132
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    const/4 v15, 0x0

    .line 136
    .local v15, "is":Ljava/io/InputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 137
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-object v4, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 138
    .local v3, "uri":Landroid/net/Uri;
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "display_name"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "photo_id"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "lookup"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 140
    const-wide/16 v18, 0x0

    .line 141
    .local v18, "photoId":J
    const/4 v12, 0x0

    .line 144
    .local v12, "displayName":Ljava/lang/String;
    if-eqz v9, :cond_6

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 145
    const-string v4, "photo_id"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 146
    const-string v4, "display_name"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 147
    const-string v4, "_id"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 148
    .local v10, "contactID":J
    const-string v4, "lookup"

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 150
    .local v16, "lookupKey":Ljava/lang/String;
    const/4 v3, 0x0

    .line 151
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 154
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->lookupKeysCache:Ljava/util/HashMap;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    const/4 v15, 0x0

    .line 157
    const-wide/16 v4, 0x0

    cmp-long v4, v18, v4

    if-eqz v4, :cond_0

    .line 158
    invoke-static {v2, v3}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v15

    .line 160
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->namesCache:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    if-nez v15, :cond_3

    .line 165
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->photosCache:Ljava/util/HashMap;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    .end local v10    # "contactID":J
    .end local v16    # "lookupKey":Ljava/lang/String;
    :goto_0
    if-eqz v9, :cond_1

    .line 182
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 184
    :cond_1
    if-eqz v15, :cond_2

    .line 186
    :try_start_1
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 193
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v12    # "displayName":Ljava/lang/String;
    .end local v18    # "photoId":J
    :cond_2
    :goto_1
    return-object v8

    .line 168
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v10    # "contactID":J
    .restart local v12    # "displayName":Ljava/lang/String;
    .restart local v16    # "lookupKey":Ljava/lang/String;
    .restart local v18    # "photoId":J
    :cond_3
    :try_start_2
    invoke-static {v15}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 169
    if-lez p2, :cond_4

    if-lez p3, :cond_4

    .line 170
    const/4 v4, 0x1

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v8, v0, v1, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 172
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->photosCache:Ljava/util/HashMap;

    new-instance v5, Ljava/lang/ref/SoftReference;

    invoke-direct {v5, v8}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 177
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v10    # "contactID":J
    .end local v12    # "displayName":Ljava/lang/String;
    .end local v16    # "lookupKey":Ljava/lang/String;
    .end local v18    # "photoId":J
    :catch_0
    move-exception v14

    .line 178
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 181
    if-eqz v9, :cond_5

    .line 182
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 184
    :cond_5
    if-eqz v15, :cond_2

    .line 186
    :try_start_4
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 187
    :catch_1
    move-exception v13

    .line 189
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 175
    .end local v13    # "e":Ljava/io/IOException;
    .end local v14    # "ex":Ljava/lang/Exception;
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v12    # "displayName":Ljava/lang/String;
    .restart local v18    # "photoId":J
    :cond_6
    :try_start_5
    invoke-virtual/range {p0 .. p1}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->removeFromCache(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 181
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v12    # "displayName":Ljava/lang/String;
    .end local v18    # "photoId":J
    :catchall_0
    move-exception v4

    if-eqz v9, :cond_7

    .line 182
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 184
    :cond_7
    if-eqz v15, :cond_8

    .line 186
    :try_start_6
    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 190
    :cond_8
    :goto_2
    throw v4

    .line 187
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v12    # "displayName":Ljava/lang/String;
    .restart local v18    # "photoId":J
    :catch_2
    move-exception v13

    .line 189
    .restart local v13    # "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 187
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v12    # "displayName":Ljava/lang/String;
    .end local v13    # "e":Ljava/io/IOException;
    .end local v18    # "photoId":J
    :catch_3
    move-exception v13

    .line 189
    .restart local v13    # "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public displayData(JLjava/lang/String;Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "phoneNumber"    # Ljava/lang/String;
    .param p4, "imageView"    # Landroid/widget/ImageView;
    .param p5, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 74
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->photosCache:Ljava/util/HashMap;

    invoke-virtual {v2, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 75
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->photosCache:Ljava/util/HashMap;

    invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    .line 79
    .local v1, "photo":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    if-nez v1, :cond_1

    .line 80
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->defaultAvatar:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 95
    :goto_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->namesCache:Ljava/util/HashMap;

    invoke-virtual {v2, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 96
    .local v0, "cachedName":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 97
    invoke-virtual {p5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    .end local v0    # "cachedName":Ljava/lang/String;
    .end local v1    # "photo":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    :cond_0
    :goto_1
    return-void

    .line 84
    .restart local v1    # "photo":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    :cond_1
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 85
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p4, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 91
    :cond_2
    invoke-direct/range {p0 .. p5}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->queueData(JLjava/lang/String;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    goto :goto_1

    .line 100
    .end local v1    # "photo":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/Bitmap;>;"
    :cond_3
    invoke-direct/range {p0 .. p5}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->queueData(JLjava/lang/String;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    goto :goto_1
.end method

.method public getContactUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 197
    if-nez p1, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-object v0

    .line 201
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->lookupKeysCache:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->lookupKeysCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getDefaultAvatar()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->defaultAvatar:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public removeFromCache(Ljava/lang/String;)V
    .locals 1
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->photosCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->namesCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->lookupKeysCache:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    return-void
.end method

.method public stopThread()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->dataLoaderThread:Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->interrupt()V

    .line 245
    return-void
.end method

.method public updatedCaches()V
    .locals 8

    .prologue
    .line 214
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->lookupKeysCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 215
    .local v0, "cachePhoneNumbers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 218
    .local v7, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    new-instance v5, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->context:Landroid/content/Context;

    invoke-direct {v5, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 219
    .local v5, "defaultImage":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader;->defaultAvatar:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 223
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 224
    const-wide/16 v2, -0x1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->queueData(JLjava/lang/String;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    goto :goto_0

    .line 226
    :cond_0
    return-void
.end method

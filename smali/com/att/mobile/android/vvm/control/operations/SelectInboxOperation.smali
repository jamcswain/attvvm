.class public Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "SelectInboxOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation$Result;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SelectInboxOperation"


# instance fields
.field private serverNumberOfMessage:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;->serverNumberOfMessage:I

    .line 35
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 5

    .prologue
    .line 45
    const-string v3, "SelectInboxOperation"

    const-string v4, "SelectInboxOperation.execute()"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ATT_VVM__ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "SELECT INBOX\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 49
    .local v0, "command":[B
    const/4 v3, 0x2

    invoke-virtual {p0, v3, v0}, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v2

    .line 50
    .local v2, "selectInboxResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v1

    .line 53
    .local v1, "responseResult":I
    if-nez v1, :cond_1

    .line 55
    const-string v3, "SelectInboxOperation"

    const-string v4, "SelectInboxOperation.execute() completed successfully"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    check-cast v2, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;

    .end local v2    # "selectInboxResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->getExists()I

    move-result v3

    iput v3, p0, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;->serverNumberOfMessage:I

    .line 61
    iget v3, p0, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;->serverNumberOfMessage:I

    if-nez v3, :cond_0

    .line 63
    sget v3, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation$Result;->SUCCEED_NO_MESSAGES_EXIST:I

    .line 77
    :goto_0
    return v3

    .line 66
    :cond_0
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_0

    .line 70
    .restart local v2    # "selectInboxResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_1
    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    .line 72
    const-string v3, "SelectInboxOperation"

    const-string v4, "SelectInboxOperation.execute() failed"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 76
    :cond_2
    const-string v3, "SelectInboxOperation"

    const-string v4, "SelectInboxOperation.execute() failed due to a network error"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0
.end method

.method public getServerNumberOfMessage()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;->serverNumberOfMessage:I

    return v0
.end method

.method public setServerNumberOfMessage(I)V
    .locals 0
    .param p1, "serverNumberOfMessage"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;->serverNumberOfMessage:I

    .line 82
    return-void
.end method

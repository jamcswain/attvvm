.class public Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "GetQuotaOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation$Result;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GetQuotaOperation"


# instance fields
.field private serverQuota:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;->serverQuota:I

    .line 35
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 6

    .prologue
    .line 45
    const-string v3, "GetQuotaOperation"

    const-string v4, "GetQuotaOperation.execute()"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ATT_VVM__ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "GETQUOTA\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 49
    .local v0, "command":[B
    const/16 v3, 0x13

    invoke-virtual {p0, v3, v0}, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    .line 50
    .local v1, "getQuotaResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v2

    .line 53
    .local v2, "responseResult":I
    if-nez v2, :cond_0

    .line 56
    check-cast v1, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;

    .end local v1    # "getQuotaResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;->getQuota()I

    move-result v3

    iput v3, p0, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;->serverQuota:I

    .line 57
    const-string v3, "GetQuotaOperation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GetQuotaOperation.execute() completed successfully, serverQuota = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;->serverQuota:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 69
    :goto_0
    return v3

    .line 62
    .restart local v1    # "getQuotaResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_0
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 64
    const-string v3, "GetQuotaOperation"

    const-string v4, "GetQuotaOperation.execute() failed"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 68
    :cond_1
    const-string v3, "GetQuotaOperation"

    const-string v4, "GetQuotaOperation.execute() failed due to a network error"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0
.end method

.method public getServerQuota()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;->serverQuota:I

    return v0
.end method

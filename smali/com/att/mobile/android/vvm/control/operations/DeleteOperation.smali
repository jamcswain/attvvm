.class public Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "DeleteOperation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DeleteOperation"


# instance fields
.field private messageToDeleteUIDs:[Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 38
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->DELETE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/Long;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "messageToDeleteUIDs"    # [Ljava/lang/Long;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 30
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->DELETE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 31
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Long;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->messageToDeleteUIDs:[Ljava/lang/Long;

    .line 32
    return-void
.end method

.method private deleteMessages()I
    .locals 9

    .prologue
    .line 68
    const-string v5, "DeleteOperation"

    const-string v6, "DeleteOperation.deleteMessages()"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->messageToDeleteUIDs:[Ljava/lang/Long;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->messageToDeleteUIDs:[Ljava/lang/Long;

    array-length v5, v5

    if-nez v5, :cond_1

    .line 74
    :cond_0
    const-string v5, "DeleteOperation"

    const-string v6, "DeleteOperation.deleteMessages() completed successfully - no messages to delete"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 110
    :goto_0
    return v5

    .line 80
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ATT_VVM__ "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "UID STORE "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 81
    .local v0, "command":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->messageToDeleteUIDs:[Ljava/lang/Long;

    array-length v7, v6

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_2

    aget-object v8, v6, v5

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 82
    .local v2, "mUID":J
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 83
    const/16 v8, 0x2c

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 85
    .end local v2    # "mUID":J
    :cond_2
    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 86
    const-string v5, " +FLAGS (\\Deleted)\r\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const/4 v5, 0x3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    .line 90
    .local v1, "deleteOperationResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v4

    .line 93
    .local v4, "responseResult":I
    if-nez v4, :cond_3

    .line 97
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->deleteMessagesMarkedAsDeleted()V

    .line 98
    const-string v5, "DeleteOperation"

    const-string v6, "DeleteOperation.deleteMessages() completed successfully"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_0

    .line 103
    :cond_3
    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 105
    const-string v5, "DeleteOperation"

    const-string v6, "DeleteOperation.deleteMessages() failed"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 109
    :cond_4
    const-string v5, "DeleteOperation"

    const-string v6, "DeleteOperation.deleteMessages() failed due to a network error"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0
.end method

.method private expunge()I
    .locals 6

    .prologue
    .line 120
    const-string v3, "DeleteOperation"

    const-string v4, "DeleteOperation.expunge()"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ATT_VVM__ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "EXPUNGE\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 126
    .local v0, "command":[B
    const/4 v3, 0x5

    invoke-virtual {p0, v3, v0}, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    .line 127
    .local v1, "expungeOperationResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v2

    .line 128
    .local v2, "responseResult":I
    const-string v3, "DeleteOperation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DeleteOperation.expunge() - response is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    if-nez v2, :cond_0

    .line 133
    const-string v3, "DeleteOperation"

    const-string v4, "DeleteOperation.expunge()completed successfully"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 145
    :goto_0
    return v3

    .line 138
    :cond_0
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 140
    const-string v3, "DeleteOperation"

    const-string v4, "DeleteOperation.expunge() failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 144
    :cond_1
    const-string v3, "DeleteOperation"

    const-string v4, "DeleteOperation.expunge() failed due to a network error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0
.end method


# virtual methods
.method public execute()I
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->deleteMessages()I

    move-result v0

    .local v0, "operationResult":I
    sget v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-ne v0, v1, :cond_0

    .line 54
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;->expunge()I

    move-result v0

    .line 58
    :cond_0
    return v0
.end method

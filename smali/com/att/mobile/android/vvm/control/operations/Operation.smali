.class public abstract Lcom/att/mobile/android/vvm/control/operations/Operation;
.super Ljava/lang/Object;
.source "Operation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/operations/Operation$Result;,
        Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;
    }
.end annotation


# instance fields
.field protected context:Landroid/content/Context;

.field protected dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

.field private imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

.field protected type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->getInstance()Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/Operation;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/Operation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 76
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/operations/Operation;->context:Landroid/content/Context;

    .line 77
    return-void
.end method


# virtual methods
.method public abstract execute()I
.end method

.method public executeGetBodyTextCommand(Ljava/lang/String;Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 1
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/Operation;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    invoke-virtual {v0, p1, p2}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->executeGetGreetingFileCommand(Ljava/lang/String;Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v0

    return-object v0
.end method

.method public executeGetBodyTextCommand(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/vvm/model/db/MessageDo;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 1
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/Operation;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->executeGetBodyTextCommand(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/vvm/model/db/MessageDo;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v0

    return-object v0
.end method

.method protected executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 1
    .param p1, "transactionId"    # B
    .param p2, "command"    # [B

    .prologue
    .line 95
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/Operation;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    invoke-virtual {v0, p1, p2}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->executeImapCommand(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v0

    return-object v0
.end method

.method protected executeIMAP4CommandWithoutResponse(B[BI)V
    .locals 1
    .param p1, "transactionId"    # B
    .param p2, "command"    # [B
    .param p3, "chunkSize"    # I

    .prologue
    .line 107
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/Operation;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    invoke-virtual {v0, p1, p2, p3}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->executeImapCommandWithoutResponse(B[BI)V

    .line 108
    return-void
.end method

.method public getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/Operation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    return-object v0
.end method

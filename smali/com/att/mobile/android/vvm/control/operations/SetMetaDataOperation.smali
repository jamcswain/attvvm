.class public Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "SetMetaDataOperation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SetMetaDataOperation"


# instance fields
.field private variable:Ljava/lang/String;

.field private variableValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/vvm/control/Dispatcher;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operationType"    # Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;
    .param p3, "variable"    # Ljava/lang/String;
    .param p4, "variableValue"    # Ljava/lang/String;
    .param p5, "dispatcher"    # Lcom/att/mobile/android/vvm/control/Dispatcher;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 44
    iput-object p2, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 45
    iput-object p3, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->variable:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->variableValue:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 48
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 58
    const-string v3, "SetMetaDataOperation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SetMetaDataOperation.execute() - setting meta data variable "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->variable:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with its value "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->variableValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ATT_VVM__ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "SETMETADATA INBOX ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->variable:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \""

    .line 62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->variableValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\")\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 65
    .local v0, "command":[B
    const/16 v3, 0xc

    invoke-virtual {p0, v3, v0}, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    .line 66
    .local v1, "setMetaDataResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v2

    .line 69
    .local v2, "setMetaDataResponseResult":I
    if-nez v2, :cond_2

    .line 71
    const-string v3, "SetMetaDataOperation"

    const-string v4, "SetMetaDataOperation.execute() completed successfully"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v4, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v3, v4, :cond_1

    .line 77
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->variableValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPassword(Ljava/lang/String;)V

    .line 79
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v4, 0x25

    invoke-virtual {v3, v4, v6}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 87
    :cond_0
    :goto_0
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 104
    :goto_1
    return v3

    .line 81
    :cond_1
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v4, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_GREETING_TYPE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v3, v4, :cond_0

    .line 84
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v4, 0x2e

    invoke-virtual {v3, v4, v6}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    goto :goto_0

    .line 90
    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 92
    const-string v3, "SetMetaDataOperation.execute()"

    const-string v4, "set meta data operation failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v4, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_GREETING_TYPE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v3, v4, :cond_3

    .line 97
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v4, 0x2f

    invoke-virtual {v3, v4, v6}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 100
    :cond_3
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_1

    .line 103
    :cond_4
    const-string v3, "SetMetaDataOperation.execute()"

    const-string v4, "set meta data operation failed due to a network error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_1
.end method

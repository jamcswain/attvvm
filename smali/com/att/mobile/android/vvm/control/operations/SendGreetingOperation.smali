.class public Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "SendGreetingOperation.java"


# static fields
.field private static CHUNK_SIZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SendGreetingOperation"


# instance fields
.field private greetingAudioData:[B

.field greetingType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0x400

    sput v0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->CHUNK_SIZE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[BLcom/att/mobile/android/vvm/control/Dispatcher;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "greetingType"    # Ljava/lang/String;
    .param p3, "greetingAudioData"    # [B
    .param p4, "dispatcher"    # Lcom/att/mobile/android/vvm/control/Dispatcher;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 28
    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingType:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingAudioData:[B

    .line 42
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SEND_GREETING:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 43
    iput-object p2, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingType:Ljava/lang/String;

    .line 44
    array-length v0, p3

    invoke-static {p3, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingAudioData:[B

    .line 45
    iput-object p4, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 46
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/16 v11, 0xb

    const/4 v10, 0x1

    .line 54
    const-string v7, "SendGreetingOperation"

    const-string v8, "SendGreetingOperation.execute()"

    invoke-static {v7, v8}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const-string v1, "Content-Type: audio/amr\n\n"

    .line 61
    .local v1, "contentTypeStr":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ATT_VVM__ "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "SETMETADATA INBOX ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingType:Ljava/lang/String;

    .line 62
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " {"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingAudioData:[B

    array-length v8, v8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "}\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 63
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "command":Ljava/lang/String;
    const/16 v7, 0xa

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    const/4 v9, -0x1

    invoke-virtual {p0, v7, v8, v9}, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->executeIMAP4CommandWithoutResponse(B[BI)V

    .line 69
    iget-object v7, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingAudioData:[B

    sget v8, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->CHUNK_SIZE:I

    invoke-virtual {p0, v11, v7, v8}, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->executeIMAP4CommandWithoutResponse(B[BI)V

    .line 73
    const-string v7, ")\r\n"

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {p0, v11, v7}, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v4

    .line 74
    .local v4, "sendGreetingResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v5

    .line 77
    .local v5, "sendGreetingResponseResult":I
    if-nez v5, :cond_2

    .line 79
    const-string v7, "SendGreetingOperation"

    const-string v8, "SendGreetingOperation.execute() completed successfully"

    invoke-static {v7, v8}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v3

    .line 83
    .local v3, "greetingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    new-instance v6, Ljava/lang/String;

    iget-object v7, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingAudioData:[B

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    .line 84
    .local v6, "stream":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    .line 86
    .local v2, "greeting":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    iget-object v8, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingType:Ljava/lang/String;

    if-eqz v8, :cond_0

    if-eqz v2, :cond_0

    .line 87
    iget-object v8, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingType:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getImapRecordingVariable()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 88
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMetadata()Ljava/util/HashMap;

    move-result-object v8

    iget-object v9, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->greetingType:Ljava/lang/String;

    invoke-virtual {v8, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-virtual {v2, v10}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->setExistingRecording(Z)V

    goto :goto_0

    .line 95
    .end local v2    # "greeting":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    :cond_1
    iget-object v7, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v8, 0x2c

    invoke-virtual {v7, v8, v12}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 97
    sget v7, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 112
    .end local v3    # "greetingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    .end local v6    # "stream":Ljava/lang/String;
    :goto_1
    return v7

    .line 101
    :cond_2
    if-ne v5, v10, :cond_3

    .line 103
    const-string v7, "SendGreetingOperation"

    const-string v8, "SendGreetingOperation.execute() failed"

    invoke-static {v7, v8}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v7, p0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v8, 0x2d

    invoke-virtual {v7, v8, v12}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 108
    sget v7, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_1

    .line 111
    :cond_3
    const-string v7, "SendGreetingOperation"

    const-string v8, "SendGreetingOperation.execute() failed due to a network error"

    invoke-static {v7, v8}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    sget v7, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_1
.end method

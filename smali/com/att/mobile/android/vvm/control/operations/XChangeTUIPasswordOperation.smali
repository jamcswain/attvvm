.class public Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "XChangeTUIPasswordOperation.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "XChangeTUIPasswordOperation"


# instance fields
.field private newPassword:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/att/mobile/android/vvm/control/Dispatcher;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "newPassword"    # Ljava/lang/String;
    .param p3, "dispatcher"    # Lcom/att/mobile/android/vvm/control/Dispatcher;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 45
    iput-object p2, p0, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->newPassword:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 47
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 61
    const-string v5, "XChangeTUIPasswordOperation"

    const-string v6, "XChangeTUIPasswordOperation.execute()"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    const-string v6, "tokenPref"

    const-class v7, Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 68
    .local v4, "username":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 69
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    const-string v6, "userPref"

    const-class v7, Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "username":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 72
    .restart local v4    # "username":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getPassword()Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "password":Ljava/lang/String;
    if-eqz v4, :cond_2

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    if-eqz v2, :cond_2

    const-string v5, ""

    .line 76
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 78
    :cond_2
    const-string v5, "XChangeTUIPasswordOperation"

    const-string v6, "XChangeTUIPasswordOperation.execute() login operation failed, username or password is missing"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    .line 115
    :goto_0
    return v5

    .line 84
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ATT_VVM__ "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "XCHANGE_TUI_PWD USER="

    .line 85
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " PWD="

    .line 86
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->newPassword:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " OLD_PWD="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 87
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 90
    .local v0, "command":[B
    const/16 v5, 0xf

    invoke-virtual {p0, v5, v0}, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    .line 92
    .local v1, "imap4Response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v3

    .line 94
    .local v3, "responseResult":I
    if-nez v3, :cond_4

    .line 96
    const-string v5, "XChangeTUIPasswordOperation"

    const-string v6, "XChangeTUIPasswordOperation.execute() completed successfully"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->newPassword:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPassword(Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 102
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v6, 0x3b

    invoke-virtual {v5, v6, v8}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 104
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_0

    .line 107
    :cond_4
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v6, 0x3c

    invoke-virtual {v5, v6, v8}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 109
    const/4 v5, 0x1

    if-ne v3, v5, :cond_5

    .line 110
    const-string v5, "XChangeTUIPasswordOperation"

    const-string v6, "XChangeTUIPasswordOperation.execute() failed"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 114
    :cond_5
    const-string v5, "XChangeTUIPasswordOperation"

    const-string v6, "XChangeTUIPasswordOperation.execute() failed due to network error"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto/16 :goto_0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->execute()I

    .line 122
    return-void
.end method

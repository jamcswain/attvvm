.class public Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;
.super Ljava/lang/Object;
.source "RefreshOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;
    }
.end annotation


# static fields
.field private static final INTERVAL:I = 0x3

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private currentServiceState:I

.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private refreshInterface:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;

.field private scheduleTaskExecutor:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 51
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->scheduleTaskExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 52
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->context:Landroid/content/Context;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    .prologue
    .line 27
    iget v0, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->currentServiceState:I

    return v0
.end method

.method static synthetic access$002(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;I)I
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->currentServiceState:I

    return p1
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Lcom/att/mobile/android/vvm/model/db/ModelManager;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->refreshInterface:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;

    return-object v0
.end method


# virtual methods
.method public refreshVM()V
    .locals 9

    .prologue
    .line 56
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    const-string v1, "refreshVM()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    .line 58
    .local v8, "telMgr":Landroid/telephony/TelephonyManager;
    new-instance v7, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$1;

    invoke-direct {v7, p0}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$1;-><init>(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)V

    .line 66
    .local v7, "phoneStateListener":Landroid/telephony/PhoneStateListener;
    const/4 v0, 0x1

    invoke-virtual {v8, v7, v0}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 68
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->scheduleTaskExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;-><init>(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 114
    return-void
.end method

.method public setRefreshInterface(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;)V
    .locals 0
    .param p1, "refreshInterface"    # Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->refreshInterface:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;

    .line 41
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "FetchBodiesOperation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FetchBodiesOperation"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/Dispatcher;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dispatcher"    # Lcom/att/mobile/android/vvm/control/Dispatcher;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 60
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_BODIES:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 61
    iput-object p2, p0, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 62
    return-void
.end method

.method private fetchBodyStructureForNonTranscriptMessages([Lcom/att/mobile/android/vvm/model/db/MessageDo;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "messagesDos"    # [Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/att/mobile/android/vvm/model/db/MessageDo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/protocol/BodyStructure;",
            ">;"
        }
    .end annotation

    .prologue
    .line 275
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 276
    .local v3, "uids":Ljava/lang/StringBuilder;
    array-length v5, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, p1, v4

    .line 277
    .local v1, "message":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x2c

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 278
    const-string v6, "FetchBodiesOperation"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FetchBodiesOperation.fetchBodyStructureForNonTranscriptMessages() - fetching Nuance transcription for message with UID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 280
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 278
    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 283
    .end local v1    # "message":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 287
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ATT_VVM__ "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "UID FETCH "

    .line 288
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " BODYSTRUCTURE\r\n"

    .line 289
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 292
    .local v0, "command":Ljava/lang/String;
    const/16 v4, 0x12

    .line 293
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    .line 292
    invoke-virtual {p0, v4, v5}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v2

    .line 295
    .local v2, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v4

    if-nez v4, :cond_1

    instance-of v4, v2, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;

    if-eqz v4, :cond_1

    .line 296
    const-string v4, "FetchBodiesOperation"

    const-string v5, "FetchBodiesOperation.fetchBodyStructureForNonTranscriptMessages() - fetching Nuance transcription result OK "

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    check-cast v2, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;

    .end local v2    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;->getBodyStructureList()Ljava/util/ArrayList;

    move-result-object v4

    .line 300
    :goto_1
    return-object v4

    .restart local v2    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private fetchFileAndTranscription(Lcom/att/mobile/android/vvm/model/db/MessageDo;Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 6
    .param p1, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 254
    const-string v1, "FetchBodiesOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FetchBodiesOperation.fetchFileAndTranscription() - fetching audio file and its transcription for message with UID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 256
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 254
    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ATT_VVM__ "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "UID FETCH "

    .line 260
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (BODY.PEEK[TEXT])\r\n"

    .line 261
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "command":Ljava/lang/String;
    invoke-virtual {p0, v0, p2, p1}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->executeGetBodyTextCommand(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/vvm/model/db/MessageDo;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    return-object v1
.end method

.method private fetchMessage(Lcom/att/mobile/android/vvm/model/db/MessageDo;)I
    .locals 11
    .param p1, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .prologue
    .line 174
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v1, "userPref"

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 177
    .local v10, "userName":Ljava/lang/String;
    if-nez v10, :cond_0

    .line 178
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    .line 238
    :goto_0
    return v0

    .line 182
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 183
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".amr"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 187
    .local v8, "fileName":Ljava/lang/String;
    invoke-direct {p0, p1, v8}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->fetchFileAndTranscription(Lcom/att/mobile/android/vvm/model/db/MessageDo;Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v7

    .line 191
    .local v7, "fetchFileAndTranscriptionResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v9

    .line 195
    .local v9, "res":I
    if-nez v9, :cond_1

    .line 208
    const-string v0, "FetchBodiesOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FetchBodiesOperation.fetchMessage() completed successfully, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 210
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 208
    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->context:Landroid/content/Context;

    .line 213
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v2

    .line 214
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v4

    check-cast v7, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;

    .line 216
    .end local v7    # "fetchFileAndTranscriptionResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->getMessageTranscription()Ljava/lang/String;

    move-result-object v6

    .line 211
    invoke-virtual/range {v0 .. v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageDetailsFromBodyText(Landroid/content/Context;JJLjava/lang/String;)Z

    .line 217
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_0

    .line 222
    .restart local v7    # "fetchFileAndTranscriptionResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->context:Landroid/content/Context;

    invoke-static {v0, v8}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 224
    const/4 v0, 0x1

    if-ne v9, v0, :cond_2

    .line 225
    const-string v0, "FetchBodiesOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FetchBodiesOperation.fetchMessage() failed!, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 227
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 225
    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto/16 :goto_0

    .line 231
    :cond_2
    const/4 v0, 0x2

    if-ne v9, v0, :cond_3

    .line 232
    const-string v0, "FetchBodiesOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FetchBodiesOperation.fetchMessage() failed! CONNECTION_CLOSED, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 234
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 232
    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->CONNECTION_CLOSED:I

    goto/16 :goto_0

    .line 238
    :cond_3
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto/16 :goto_0
.end method

.method private fetchTextMessage(Lcom/att/mobile/android/vvm/model/db/MessageDo;Lcom/att/mobile/android/vvm/protocol/BodyStructure;)I
    .locals 13
    .param p1, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .param p2, "bodyStructure"    # Lcom/att/mobile/android/vvm/protocol/BodyStructure;

    .prologue
    const/4 v2, -0x1

    .line 316
    const/4 v12, -0x1

    .line 317
    .local v12, "transcriptionIndex":I
    const/4 v10, -0x1

    .line 318
    .local v10, "messageIndex":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->getBodyParts()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v9, v0, :cond_2

    .line 319
    invoke-virtual {p2, v9}, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->getBodyPart(I)Lcom/att/mobile/android/vvm/protocol/BodyPart;

    move-result-object v7

    .line 320
    .local v7, "bodyPart":Lcom/att/mobile/android/vvm/protocol/BodyPart;
    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/protocol/BodyPart;->getContentType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/protocol/BodyPart;->getIsVttAluReason()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    move v12, v9

    .line 318
    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 322
    :cond_1
    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/protocol/BodyPart;->getContentType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "message/rfc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    move v10, v9

    goto :goto_1

    .line 329
    .end local v7    # "bodyPart":Lcom/att/mobile/android/vvm/protocol/BodyPart;
    :cond_2
    if-eq v10, v2, :cond_3

    .line 330
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->fetchMessage(Lcom/att/mobile/android/vvm/model/db/MessageDo;)I

    move-result v0

    .line 394
    :goto_2
    return v0

    .line 334
    :cond_3
    if-ne v12, v2, :cond_4

    .line 335
    const-string v0, "FetchBodiesOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Nuance Transcription part is missing on messageUid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_2

    .line 341
    :cond_4
    invoke-direct {p0, p1, v12}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->fetchTranscription(Lcom/att/mobile/android/vvm/model/db/MessageDo;I)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v8

    .line 345
    .local v8, "fetchTranscriptionResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v11

    .line 348
    .local v11, "res":I
    if-nez v11, :cond_5

    .line 360
    const-string v1, "FetchBodiesOperation"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FetchBodiesOperation.fetchTextMessage() completed successfully, uid = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 362
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " got transcription = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object v0, v8

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;

    .line 363
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->getMessageTranscription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 360
    invoke-static {v1, v0}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->context:Landroid/content/Context;

    .line 366
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v2

    .line 367
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v4

    check-cast v8, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;

    .line 369
    .end local v8    # "fetchTranscriptionResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->getMessageTranscription()Ljava/lang/String;

    move-result-object v6

    .line 364
    invoke-virtual/range {v0 .. v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageDetailsFromBodyText(Landroid/content/Context;JJLjava/lang/String;)Z

    .line 370
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_2

    .line 373
    .restart local v8    # "fetchTranscriptionResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_5
    const/4 v0, 0x1

    if-ne v11, v0, :cond_6

    .line 374
    const-string v0, "FetchBodiesOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FetchBodiesOperation.fetchTextMessage() failed!, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 376
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 374
    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto/16 :goto_2

    .line 380
    :cond_6
    const/4 v0, 0x2

    if-ne v11, v0, :cond_7

    .line 381
    const-string v0, "FetchBodiesOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FetchBodiesOperation.fetchTextMessage() failed! CONNECTION_CLOSED, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 383
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 381
    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->CONNECTION_CLOSED:I

    goto/16 :goto_2

    .line 387
    :cond_7
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED_NO_MESSAGES_EXIST:I

    if-ne v11, v0, :cond_8

    .line 388
    const-string v0, "FetchBodiesOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FetchBodiesOperation.fetchTextMessage() skipped! Nuance transcription was not found, uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 390
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 388
    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED_NO_MESSAGES_EXIST:I

    goto/16 :goto_2

    .line 394
    :cond_8
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto/16 :goto_2
.end method

.method private fetchTranscription(Lcom/att/mobile/android/vvm/model/db/MessageDo;I)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 6
    .param p1, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .param p2, "transcriptionIndex"    # I

    .prologue
    .line 404
    const-string v1, "FetchBodiesOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FetchBodiesOperation.fetchTranscription() - fetching Nuance transcription for message with UID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 406
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 404
    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ATT_VVM__ "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "UID FETCH "

    .line 410
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " (BODY.PEEK["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "])\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 411
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 414
    .local v0, "command":Ljava/lang/String;
    const/16 v1, 0x11

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public execute()I
    .locals 18

    .prologue
    .line 71
    const-string v11, "FetchBodiesOperation"

    const-string v12, "FetchBodiesOperation.execute()"

    invoke-static {v11, v12}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    sget v6, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 78
    .local v6, "finalResult":I
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessagesToDownload(Z)[Lcom/att/mobile/android/vvm/model/db/MessageDo;

    move-result-object v8

    .line 79
    .local v8, "missingFileMessageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    if-eqz v8, :cond_2

    array-length v11, v8

    if-lez v11, :cond_2

    .line 80
    const/4 v10, -0x1

    .line 82
    .local v10, "result":I
    array-length v12, v8

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v12, :cond_2

    aget-object v7, v8, v11

    .line 85
    .local v7, "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v13

    .line 86
    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v14

    .line 85
    invoke-virtual {v13, v14, v15}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessagePendingForDelete(J)Z

    move-result v13

    if-nez v13, :cond_1

    .line 88
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->isMemoryLow()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 94
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v12, 0x29

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 98
    sget v10, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED_NOT_ENOUGH_SPACE:I

    .line 160
    .end local v7    # "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .end local v10    # "result":I
    :goto_1
    return v10

    .line 100
    .restart local v7    # "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .restart local v10    # "result":I
    :cond_0
    const-string v13, "FetchBodiesOperation"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FetchBodiesOperation.execute() fetching missedfile message Uid = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->fetchMessage(Lcom/att/mobile/android/vvm/model/db/MessageDo;)I

    move-result v10

    .line 104
    sget v13, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-eq v10, v13, :cond_1

    .line 105
    invoke-static {}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->resetToken()V

    goto :goto_1

    .line 82
    :cond_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 111
    .end local v7    # "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .end local v10    # "result":I
    :cond_2
    const-string v11, "FetchBodiesOperation"

    const-string v12, "FetchBodiesOperation: all missing files fetched"

    invoke-static {v11, v12}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessagesToDownload(Z)[Lcom/att/mobile/android/vvm/model/db/MessageDo;

    move-result-object v9

    .line 116
    .local v9, "missingTranscriptionMessageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    if-eqz v9, :cond_6

    array-length v11, v9

    if-lez v11, :cond_6

    .line 117
    const/4 v10, -0x1

    .line 120
    .restart local v10    # "result":I
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->fetchBodyStructureForNonTranscriptMessages([Lcom/att/mobile/android/vvm/model/db/MessageDo;)Ljava/util/ArrayList;

    move-result-object v2

    .line 121
    .local v2, "bodiesStructure":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/protocol/BodyStructure;>;"
    if-eqz v2, :cond_6

    .line 122
    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    .line 123
    .local v3, "bodiesStructureMap":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Lcom/att/mobile/android/vvm/protocol/BodyStructure;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/att/mobile/android/vvm/protocol/BodyStructure;

    .line 124
    .local v5, "bodyStructureItem":Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->getUid()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v3, v12, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 128
    .end local v5    # "bodyStructureItem":Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    :cond_3
    array-length v12, v9

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v12, :cond_5

    aget-object v7, v9, v11

    .line 130
    .restart local v7    # "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v13

    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessagePendingForDelete(J)Z

    move-result v13

    if-nez v13, :cond_4

    .line 131
    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v3, v13}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/att/mobile/android/vvm/protocol/BodyStructure;

    .line 135
    .local v4, "bodyStructure":Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    if-eqz v4, :cond_4

    .line 137
    const-string v13, "FetchBodiesOperation"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "FetchBodiesOperation.execute() fetching missing transcription message Uid = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v4}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->fetchTextMessage(Lcom/att/mobile/android/vvm/model/db/MessageDo;Lcom/att/mobile/android/vvm/protocol/BodyStructure;)I

    move-result v10

    .line 139
    sget v13, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-eq v10, v13, :cond_4

    .line 140
    move v6, v10

    .line 128
    .end local v4    # "bodyStructure":Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 145
    .end local v7    # "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :cond_5
    const-string v11, "FetchBodiesOperation"

    const-string v12, "FetchBodiesOperation: all missing transcriptions fetched"

    invoke-static {v11, v12}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    .end local v2    # "bodiesStructure":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/protocol/BodyStructure;>;"
    .end local v3    # "bodiesStructureMap":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Lcom/att/mobile/android/vvm/protocol/BodyStructure;>;"
    .end local v10    # "result":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v12, 0x17

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 152
    sget v11, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-eq v6, v11, :cond_7

    .line 153
    invoke-static {}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->resetToken()V

    move v10, v6

    .line 154
    goto/16 :goto_1

    .line 156
    :cond_7
    sget v11, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-ne v6, v11, :cond_8

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->wasMailboxRefreshedOnStartup()Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-nez v11, :cond_8

    .line 157
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v11

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setInboxRefreshed(Ljava/lang/Boolean;)V

    .line 160
    :cond_8
    sget v10, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto/16 :goto_1
.end method

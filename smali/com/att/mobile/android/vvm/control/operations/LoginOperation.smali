.class public Lcom/att/mobile/android/vvm/control/operations/LoginOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "LoginOperation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LoginOperation"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 41
    iget-object v9, p0, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    check-cast v9, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v9}, Lcom/att/mobile/android/vvm/VVMApplication;->getClientId()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "clientId":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ATT_VVM__ "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "SETMETADATA INBOX ("

    .line 44
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/private/vendor/vendor.alu/messaging/ClientID"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\")\r\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 46
    .local v1, "clientIdCommand":[B
    const-string v9, "LoginOperation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "LoginOperation.execute() set metadata client id command = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-virtual {p0, v12, v1}, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v2

    .line 50
    .local v2, "clientIdResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 51
    const-string v9, "LoginOperation"

    const-string v10, "LoginOperation.execute() set metadata client id failed due to network error"

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    sget v9, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    .line 108
    :goto_0
    return v9

    .line 57
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v9

    const-string v10, "tokenPref"

    const-class v11, Ljava/lang/String;

    invoke-virtual {v9, v10, v11, v14}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 58
    .local v8, "username":Ljava/lang/String;
    const-string v9, "LoginOperation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "####LoginOperation.execute() login operation  username from PREFERENCE_TOKEN = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    if-eqz v8, :cond_1

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 60
    :cond_1
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v9

    const-string v10, "userPref"

    const-class v11, Ljava/lang/String;

    invoke-virtual {v9, v10, v11, v14}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "username":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 61
    .restart local v8    # "username":Ljava/lang/String;
    const-string v9, "LoginOperation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "####LoginOperation.execute() login operation  username from PREFERENCE_MAILBOX_NUMBER = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_2
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getPassword()Ljava/lang/String;

    move-result-object v6

    .line 66
    .local v6, "password":Ljava/lang/String;
    if-eqz v8, :cond_3

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    if-eqz v6, :cond_3

    const-string v9, ""

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 69
    :cond_3
    const-string v9, "LoginOperation"

    const-string v10, "LoginOperation.execute() login operation failed, username or password is missing"

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    sget v9, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 74
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ATT_VVM__ "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "LOGIN "

    .line 75
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\r\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 76
    .local v3, "command":[B
    const-string v9, "LoginOperation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "####LoginOperation.execute() login operation  username  = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " password = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {p0, v12, v3}, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v5

    .line 80
    .local v5, "imap4Response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v7

    .line 83
    .local v7, "responseResult":I
    if-nez v7, :cond_5

    .line 85
    sget v9, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto/16 :goto_0

    .line 89
    :cond_5
    if-ne v7, v13, :cond_9

    .line 91
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getDescription()Ljava/lang/String;

    move-result-object v4

    .line 92
    .local v4, "desc":Ljava/lang/String;
    if-eqz v4, :cond_7

    iget-object v9, p0, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;->context:Landroid/content/Context;

    const v10, 0x7f080113

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    iget-object v9, p0, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;->context:Landroid/content/Context;

    const v10, 0x7f0800cb

    .line 93
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 94
    :cond_6
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v9

    invoke-virtual {v9, v13}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 95
    const-string v9, "LoginOperation"

    const-string v10, "####LoginOperation.execute() login operation failed,  password is CHANGED_IN_TUI"

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v9

    const/16 v10, 0x9

    invoke-virtual {v9, v10}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 97
    sget v9, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED_WRONG_PASSWORD:I

    goto/16 :goto_0

    .line 99
    :cond_7
    if-eqz v4, :cond_8

    iget-object v9, p0, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;->context:Landroid/content/Context;

    const v10, 0x7f080161

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 100
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v9

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 101
    const-string v9, "LoginOperation"

    const-string v10, "####LoginOperation.execute() login operation failed,  password is RESET_BY_ADMIN"

    invoke-static {v9, v10}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v9

    const/16 v10, 0xe

    invoke-virtual {v9, v10}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 103
    sget v9, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED_WRONG_PASSWORD:I

    goto/16 :goto_0

    .line 105
    :cond_8
    sget v9, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto/16 :goto_0

    .line 108
    .end local v4    # "desc":Ljava/lang/String;
    :cond_9
    sget v9, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto/16 :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "TuiSkipOperation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TuiSkipOperation"


# instance fields
.field private requestedMessageToTuiSkipUIDs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;->requestedMessageToTuiSkipUIDs:Ljava/util/Set;

    .line 38
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->SKIPPED:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 39
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 12

    .prologue
    .line 43
    const-string v6, "TuiSkipOperation"

    const-string v7, "TuiSkipOperation.execute()"

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getUnSkippedMessages()[Lcom/att/mobile/android/vvm/model/db/MessageDo;

    move-result-object v3

    .line 49
    .local v3, "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    if-eqz v3, :cond_0

    array-length v6, v3

    if-nez v6, :cond_1

    .line 52
    :cond_0
    const-string v6, "TuiSkipOperation"

    const-string v7, "TuiSkipOperation.execute() completed successfully - no messages to skip)"

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    sget v6, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 107
    :goto_0
    return v6

    .line 56
    :cond_1
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    iput-object v6, p0, Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;->requestedMessageToTuiSkipUIDs:Ljava/util/Set;

    .line 59
    array-length v7, v3

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_3

    aget-object v2, v3, v6

    .line 62
    .local v2, "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v8

    const-wide/32 v10, 0x7fffffff

    cmp-long v8, v8, v10

    if-gez v8, :cond_2

    .line 64
    iget-object v8, p0, Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;->requestedMessageToTuiSkipUIDs:Ljava/util/Set;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 68
    .end local v2    # "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ATT_VVM__ "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "UID STORE "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 70
    .local v0, "command":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;->requestedMessageToTuiSkipUIDs:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 71
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 72
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 74
    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 75
    const/16 v6, 0x2c

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 77
    :cond_5
    const-string v6, " +FLAGS (TUISkipped)\r\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const/16 v6, 0xe

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v5

    .line 81
    .local v5, "tuiSkipOperationResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v4

    .line 84
    .local v4, "responseResult":I
    if-nez v4, :cond_7

    .line 86
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    check-cast v5, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;

    .end local v5    # "tuiSkipOperationResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->getSkippedUids()Ljava/util/Set;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateTuiskipped(Ljava/util/Set;)V

    .line 89
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getUnSkippedMessages()[Lcom/att/mobile/android/vvm/model/db/MessageDo;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 91
    const-string v6, "TuiSkipOperation"

    const-string v7, "TuiSkipOperation.execute() NOT all requested messages were TUISkipped."

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    sget v6, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto/16 :goto_0

    .line 95
    :cond_6
    const-string v6, "TuiSkipOperation"

    const-string v7, "TuiSkipOperation.execute() all requested messages were TUISkipped."

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    sget v6, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto/16 :goto_0

    .line 100
    .restart local v5    # "tuiSkipOperationResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_7
    const/4 v6, 0x1

    if-ne v4, v6, :cond_8

    .line 102
    const-string v6, "TuiSkipOperation"

    const-string v7, "TuiSkipOperation.execute() failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    sget v6, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto/16 :goto_0

    .line 106
    :cond_8
    const-string v6, "TuiSkipOperation"

    const-string v7, "TuiSkipOperation.execute() failed due to a network error"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    sget v6, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto/16 :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/control/operations/MarkAsReadOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "MarkAsReadOperation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MarkAsReadOperation"


# instance fields
.field private messageUID:J


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mUID"    # J

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 30
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->MARK_AS_READ:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/MarkAsReadOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 31
    iput-wide p2, p0, Lcom/att/mobile/android/vvm/control/operations/MarkAsReadOperation;->messageUID:J

    .line 32
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 6

    .prologue
    .line 43
    const-string v3, "MarkAsReadOperation"

    const-string v4, "MarkAsReadOperation.execute()"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ATT_VVM__ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "UID STORE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 47
    .local v0, "command":Ljava/lang/StringBuilder;
    iget-wide v4, p0, Lcom/att/mobile/android/vvm/control/operations/MarkAsReadOperation;->messageUID:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 48
    const-string v3, " +FLAGS (\\Seen)\r\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const/4 v3, 0x7

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/att/mobile/android/vvm/control/operations/MarkAsReadOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    .line 52
    .local v1, "markAsReadOperationResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v2

    .line 55
    .local v2, "responseResult":I
    if-nez v2, :cond_0

    .line 58
    const-string v3, "MarkAsReadOperation"

    const-string v4, "MarkAsReadOperation.execute() completed successfully"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 71
    :goto_0
    return v3

    .line 64
    :cond_0
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 66
    const-string v3, "MarkAsReadOperation"

    const-string v4, "MarkAsReadOperation.execute() operation failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 70
    :cond_1
    const-string v3, "MarkAsReadOperation"

    const-string v4, "MarkAsReadOperation.execute() failed due to a network error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0
.end method

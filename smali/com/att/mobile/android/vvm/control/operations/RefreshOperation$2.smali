.class Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;
.super Ljava/lang/Object;
.source "RefreshOperation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->refreshVM()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 71
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$100(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/att/mobile/android/infra/sim/SimManager;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/infra/sim/SimManager;->validateSim()Lcom/att/mobile/android/infra/sim/SimValidationResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/infra/sim/SimValidationResult;->isSimPresentAndReady()Z

    move-result v3

    if-nez v3, :cond_1

    .line 72
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    const-string v4, "isSimPresentAndReady = false"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 77
    .local v1, "connManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 79
    .local v0, "activeNetwork":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 81
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "refreshVM() activeNetwork = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 83
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "refreshVM() Current service state is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$000(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". !!! RAN is available when the value is 0 (STATE_IN_SERVICE)."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$000(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)I

    move-result v3

    if-eqz v3, :cond_3

    .line 86
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$200(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isVVMOverWifi()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 87
    .local v2, "vvmOverWifiSwitch":Z
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "refreshVM() VVM over wifi switch is  = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    if-nez v2, :cond_2

    .line 90
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$300(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 91
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$300(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;->onRefresh()V

    goto/16 :goto_0

    .line 96
    :cond_2
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    const-string v4, "refreshVM() initiating a call to check if there are new voice mails(enqueueFetchHeadersAndBodiesOperation)."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueFetchHeadersAndBodiesOperation()V

    goto/16 :goto_0

    .line 99
    .end local v2    # "vvmOverWifiSwitch":Z
    :cond_3
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "refreshVM() RAN is enabled = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$000(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$300(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 101
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    const-string v4, "refreshVM() changing the VVM over WiFi switch to false."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$2;->this$0:Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->access$300(Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;)Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;

    move-result-object v3

    invoke-interface {v3}, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation$RefreshInterface;->onSIMChangeState()V

    goto/16 :goto_0

    .line 106
    :cond_4
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    const-string v4, "refreshVM() no need to refresh"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 109
    :cond_5
    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/RefreshOperation;->TAG:Ljava/lang/String;

    const-string v4, "refreshVM() network connection is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

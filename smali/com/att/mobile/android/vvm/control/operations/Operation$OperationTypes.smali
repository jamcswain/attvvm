.class public final enum Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;
.super Ljava/lang/Enum;
.source "Operation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/operations/Operation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OperationTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum DELETE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum LOGIN:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum MARK_AS_READ:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum SELECT_INBOX:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum SKIPPED:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_FETCH_BODIES:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_FETCH_HEADERS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_GET_META_DATA_EXISTING_GREETINGS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_GET_META_DATA_GREETINGS_DETAILS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_GET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_SEND_GREETING:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_SET_META_DATA_GENERAL:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_SET_META_DATA_GREETING_TYPE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

.field public static final enum TYPE_SET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "LOGIN"

    invoke-direct {v0, v1, v3}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->LOGIN:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 23
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "SELECT_INBOX"

    invoke-direct {v0, v1, v4}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->SELECT_INBOX:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 24
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_FETCH_HEADERS"

    invoke-direct {v0, v1, v5}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_HEADERS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 25
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_FETCH_BODIES"

    invoke-direct {v0, v1, v6}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_BODIES:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 26
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v7}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->DELETE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 27
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "MARK_AS_READ"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->MARK_AS_READ:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 28
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "SKIPPED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->SKIPPED:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 30
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_GET_META_DATA_GREETINGS_DETAILS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_GREETINGS_DETAILS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 31
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_GET_META_DATA_EXISTING_GREETINGS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_EXISTING_GREETINGS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 32
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_GET_META_DATA_PASSWORD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 34
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_SET_META_DATA_GENERAL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_GENERAL:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 35
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_SET_META_DATA_PASSWORD"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 36
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_SET_META_DATA_GREETING_TYPE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_GREETING_TYPE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 37
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v1, "TYPE_SEND_GREETING"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SEND_GREETING:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 20
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v1, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->LOGIN:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->SELECT_INBOX:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_HEADERS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_BODIES:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v1, v0, v6

    sget-object v1, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->DELETE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->MARK_AS_READ:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->SKIPPED:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_GREETINGS_DETAILS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_EXISTING_GREETINGS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_GENERAL:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_GREETING_TYPE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SEND_GREETING:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    aput-object v2, v0, v1

    sput-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->$VALUES:[Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    return-object v0
.end method

.method public static values()[Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->$VALUES:[Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    invoke-virtual {v0}, [Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    return-object v0
.end method

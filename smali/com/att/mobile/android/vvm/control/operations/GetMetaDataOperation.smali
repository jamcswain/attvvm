.class public Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "GetMetaDataOperation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GetMetaDataOperation"


# instance fields
.field private transactionType:B

.field private variables:Ljava/lang/String;

.field private variablesResponseValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;Lcom/att/mobile/android/vvm/control/Dispatcher;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operationType"    # Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;
    .param p3, "dispatcher"    # Lcom/att/mobile/android/vvm/control/Dispatcher;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 58
    iput-object p2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 59
    iput-object p3, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 61
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v1, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_GREETINGS_DETAILS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v0, v1, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->initializeGetGreetingsMetaData()V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v1, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_EXISTING_GREETINGS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v0, v1, :cond_1

    .line 66
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->initializeGetExistingGreetingsMetaData()V

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v1, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v0, v1, :cond_2

    .line 70
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->initializeGetPasswordLengthMetaData()V

    .line 72
    :cond_2
    return-void
.end method

.method private getMetaData()Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 4

    .prologue
    .line 251
    const-string v1, "GetMetaDataOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GetMetaDataOperation.getMetaData() - requested variables are "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variables:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ATT_VVM__ "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "GETMETADATA INBOX "

    .line 257
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variables:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    .line 258
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 261
    .local v0, "command":[B
    iget-byte v1, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->transactionType:B

    invoke-virtual {p0, v1, v0}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    return-object v1
.end method

.method private getMetaDataAttachedFile(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 266
    const-string v1, "GetMetaDataOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GetMetaDataOperation.getMetaData() - requested variables are "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variables:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ATT_VVM__ "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "GETMETADATA INBOX "

    .line 272
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variables:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    .line 273
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 276
    .local v0, "command":[B
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p0, v1, p1}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->executeGetBodyTextCommand(Ljava/lang/String;Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    return-object v1
.end method

.method private initializeGetExistingGreetingsMetaData()V
    .locals 8

    .prologue
    .line 111
    const/16 v0, 0xd

    iput-byte v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->transactionType:B

    .line 112
    const-string v0, "GetMetaDataOperation"

    const-string v1, "GetMetaDataOperation initializeGetExistingGreetingsMetaData"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMetadata()Ljava/util/HashMap;

    move-result-object v7

    .line 119
    .local v7, "greetingMetaData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v7, :cond_0

    .line 121
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->context:Landroid/content/Context;

    const-string v1, "/private/vendor/vendor.alu/messaging/GreetingTypesAllowed"

    .line 123
    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "/private/vendor/vendor.alu/messaging/ChangeableGreetings"

    .line 125
    invoke-virtual {v7, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "/private/vendor/vendor.alu/messaging/GreetingType"

    .line 126
    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "/private/vendor/vendor.alu/messaging/MaxGreetingLength"

    .line 127
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "/private/vendor/vendor.alu/messaging/MaxRecordedNameLength"

    .line 129
    invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 121
    invoke-static/range {v0 .. v5}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->createGreetings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 132
    .local v6, "greetingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setGreetingList(Ljava/util/ArrayList;)V

    .line 135
    invoke-static {v6}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 137
    .end local v6    # "greetingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    :cond_0
    return-void
.end method

.method private initializeGetGreetingsMetaData()V
    .locals 2

    .prologue
    .line 94
    const/16 v0, 0x9

    iput-byte v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->transactionType:B

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/private/vendor/vendor.alu/messaging/GreetingTypesAllowed"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/private/vendor/vendor.alu/messaging/ChangeableGreetings"

    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/private/vendor/vendor.alu/messaging/GreetingType"

    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/private/vendor/vendor.alu/messaging/MaxGreetingLength"

    .line 101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/private/vendor/vendor.alu/messaging/MaxRecordedNameLength"

    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variables:Ljava/lang/String;

    .line 103
    return-void
.end method

.method private initializeGetPasswordLengthMetaData()V
    .locals 2

    .prologue
    .line 80
    const/16 v0, 0x9

    iput-byte v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->transactionType:B

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "/private/vendor/vendor.alu/messaging/MaxPasswordDigits"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/private/vendor/vendor.alu/messaging/MinPasswordDigits"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variables:Ljava/lang/String;

    .line 86
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 8

    .prologue
    const/16 v7, 0x1a

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 147
    const/4 v0, 0x0

    .line 151
    .local v0, "getMetaDataResponse":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_EXISTING_GREETINGS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v2, v3, :cond_4

    .line 153
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getGreetingList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    .line 156
    .local v1, "greeting":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isChangeable()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 157
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getImapRecordingVariable()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variables:Ljava/lang/String;

    .line 158
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".amr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->getMetaDataAttachedFile(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v0

    .line 160
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v3

    if-nez v3, :cond_1

    .line 162
    invoke-virtual {v1, v6}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->setExistingRecording(Z)V

    goto :goto_0

    .line 164
    :cond_1
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v3

    if-ne v3, v6, :cond_2

    .line 165
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->setExistingRecording(Z)V

    goto :goto_0

    .line 167
    :cond_2
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 171
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v2, v7, v5}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 173
    const-string v2, "GetMetaDataOperation"

    const-string v3, "GetMetaDataOperation.execute() failed due to a network error!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    sget v2, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    .line 226
    .end local v1    # "greeting":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    :goto_1
    return v2

    .line 181
    :cond_3
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v3, 0x19

    invoke-virtual {v2, v3, v5}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 183
    sget v2, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_1

    .line 186
    :cond_4
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->getMetaData()Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v2

    if-nez v2, :cond_7

    move-object v2, v0

    .line 191
    check-cast v2, Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;

    .line 192
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;->getVariables()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variablesResponseValues:Ljava/util/HashMap;

    .line 194
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_GREETINGS_DETAILS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v2, v3, :cond_6

    .line 196
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variablesResponseValues:Ljava/util/HashMap;

    .line 197
    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->saveMetadata(Ljava/util/HashMap;)V

    .line 201
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v3, 0x18

    invoke-virtual {v2, v3, v5}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 213
    :cond_5
    :goto_2
    sget v2, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_1

    .line 203
    :cond_6
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v2, v3, :cond_5

    .line 205
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->variablesResponseValues:Ljava/util/HashMap;

    .line 206
    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->saveMetadata(Ljava/util/HashMap;)V

    .line 210
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v3, 0x1b

    invoke-virtual {v2, v3, v5}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    goto :goto_2

    .line 217
    :cond_7
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v2, v7, v5}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 220
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v2

    if-ne v2, v6, :cond_8

    .line 221
    const-string v2, "GetMetaDataOperation"

    const-string v3, "GetMetaDataOperation.execute() failed!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    sget v2, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_1

    .line 225
    :cond_8
    const-string v2, "GetMetaDataOperation"

    const-string v3, "GetMetaDataOperation.execute() failed due to a network error!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    sget v2, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_1
.end method

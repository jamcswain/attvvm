.class public Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;
.super Lcom/att/mobile/android/vvm/control/operations/Operation;
.source "FetchHeadersOperation.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FetchHeadersOperation"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/Dispatcher;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dispatcher"    # Lcom/att/mobile/android/vvm/control/Dispatcher;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/operations/Operation;-><init>(Landroid/content/Context;)V

    .line 28
    sget-object v0, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_HEADERS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;->type:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    .line 29
    iput-object p2, p0, Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 30
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 5

    .prologue
    .line 40
    const-string v3, "FetchHeadersOperation"

    const-string v4, "FetchHeadersOperation.execute()"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ATT_VVM__ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "FETCH 1:* (UID FLAGS BODY.PEEK[HEADER.FIELDS (DATE X-CLI_NUMBER X-ALU-PREVIOUS-UID)])\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 47
    .local v0, "command":[B
    const/4 v3, 0x6

    invoke-virtual {p0, v3, v0}, Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;->executeIMAP4Command(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    move-result-object v1

    .line 48
    .local v1, "imap4Response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->getResult()I

    move-result v2

    .line 51
    .local v2, "responseResult":I
    if-nez v2, :cond_0

    .line 53
    const-string v3, "FetchHeadersOperation"

    const-string v4, "FetchHeadersOperation.execute() completed successfully"

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 66
    :goto_0
    return v3

    .line 59
    :cond_0
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 61
    const-string v3, "FetchHeadersOperation"

    const-string v4, "FetchHeadersOperation.execute() failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 65
    :cond_1
    const-string v3, "FetchHeadersOperation"

    const-string v4, "FetchHeadersOperation.execute() failed due to a network error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    sget v3, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/control/operations/Operation$Result;
.super Ljava/lang/Object;
.source "Operation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/operations/Operation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Result"
.end annotation


# static fields
.field public static final ALREADY_LOGGED_IN:I

.field public static final CONNECTION_CLOSED:I

.field public static final FAILED:I

.field public static final FAILED_NOT_ENOUGH_SPACE:I

.field public static final FAILED_WRONG_PASSWORD:I

.field public static final NETWORK_ERROR:I

.field public static final SUCCEED:I

.field public static final SUCCEED_NO_MESSAGES_EXIST:I

.field static nextReusltID:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x0

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    .line 46
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    .line 47
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    .line 48
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    .line 49
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED_NO_MESSAGES_EXIST:I

    .line 50
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->ALREADY_LOGGED_IN:I

    .line 51
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED_WRONG_PASSWORD:I

    .line 52
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->CONNECTION_CLOSED:I

    .line 53
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->nextReusltID:I

    sput v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED_NOT_ENOUGH_SPACE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

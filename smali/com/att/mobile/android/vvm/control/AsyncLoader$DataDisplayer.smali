.class Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;
.super Ljava/lang/Object;
.source "AsyncLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/AsyncLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DataDisplayer"
.end annotation


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private imageView:Landroid/widget/ImageView;

.field private name:Ljava/lang/String;

.field private textView:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 0
    .param p1, "b"    # Landroid/graphics/Bitmap;
    .param p2, "i"    # Landroid/widget/ImageView;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "t"    # Landroid/widget/TextView;

    .prologue
    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->bitmap:Landroid/graphics/Bitmap;

    .line 332
    iput-object p2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->imageView:Landroid/widget/ImageView;

    .line 333
    iput-object p3, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->name:Ljava/lang/String;

    .line 334
    iput-object p4, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->textView:Landroid/widget/TextView;

    .line 336
    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/Bitmap;Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/TextView;Lcom/att/mobile/android/vvm/control/AsyncLoader$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/graphics/Bitmap;
    .param p2, "x1"    # Landroid/widget/ImageView;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Landroid/widget/TextView;
    .param p5, "x4"    # Lcom/att/mobile/android/vvm/control/AsyncLoader$1;

    .prologue
    .line 324
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;-><init>(Landroid/graphics/Bitmap;Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/TextView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 345
    :cond_1
    return-void
.end method

.class public interface abstract Lcom/att/mobile/android/vvm/control/IEventDispatcher;
.super Ljava/lang/Object;
.source "IEventDispatcher.java"


# virtual methods
.method public abstract addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
.end method

.method public abstract notifyListeners(ILjava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
.end method

.method public abstract removeEventListeners()V
.end method

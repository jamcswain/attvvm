.class public Lcom/att/mobile/android/vvm/control/Dispatcher;
.super Ljava/lang/Object;
.source "Dispatcher.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Dispatcher"


# instance fields
.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/att/mobile/android/vvm/control/EventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final addListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 15
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    monitor-enter v2

    .line 16
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/control/EventListener;

    .line 17
    .local v0, "currListener":Lcom/att/mobile/android/vvm/control/EventListener;
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 18
    monitor-exit v2

    .line 23
    .end local v0    # "currListener":Lcom/att/mobile/android/vvm/control/EventListener;
    :goto_0
    return-void

    .line 21
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 30
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 31
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final notifyListeners(ILjava/util/ArrayList;)V
    .locals 6
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    monitor-enter v2

    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/control/EventListener;

    .line 58
    .local v0, "listener":Lcom/att/mobile/android/vvm/control/EventListener;
    const-string v3, "Dispatcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifyListener "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const-string v3, "Dispatcher"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifyListener eventId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-interface {v0, p1, p2}, Lcom/att/mobile/android/vvm/control/EventListener;->onUpdateListener(ILjava/util/ArrayList;)V

    goto :goto_0

    .line 62
    .end local v0    # "listener":Lcom/att/mobile/android/vvm/control/EventListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    return-void
.end method

.method public final notifyListenersForNetworkFailure()V
    .locals 4

    .prologue
    .line 67
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    monitor-enter v2

    .line 69
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/control/EventListener;

    .line 71
    .local v0, "listener":Lcom/att/mobile/android/vvm/control/EventListener;
    invoke-interface {v0}, Lcom/att/mobile/android/vvm/control/EventListener;->onNetworkFailure()V

    goto :goto_0

    .line 73
    .end local v0    # "listener":Lcom/att/mobile/android/vvm/control/EventListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    return-void
.end method

.method public final removeListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 36
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 37
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 38
    monitor-exit v1

    .line 39
    return-void

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final removeListeners()V
    .locals 3

    .prologue
    .line 45
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    monitor-enter v2

    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/Dispatcher;->listeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 47
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/att/mobile/android/vvm/control/EventListener;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 49
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 51
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/att/mobile/android/vvm/control/EventListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/att/mobile/android/vvm/control/EventListener;>;"
    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/control/SetupController;
.super Ljava/lang/Object;
.source "SetupController.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/EventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;,
        Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SetupController"

.field private static final WAIT_FOR_SMS_TIMER:I = 0x1

.field private static _instance:Lcom/att/mobile/android/vvm/control/SetupController;

.field private static setupCallBackHandler:Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;

.field private static stateString:Ljava/lang/String;


# instance fields
.field private _callback:Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

.field private _context:Landroid/content/Context;

.field private isTC:Z

.field private mailboxNumber:Ljava/lang/String;

.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private telephonyManager:Landroid/telephony/TelephonyManager;

.field private timerStartTime:J

.field private waitMaxTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "N/A"

    sput-object v0, Lcom/att/mobile/android/vvm/control/SetupController;->stateString:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const v4, 0x7f080093

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v1, p0, Lcom/att/mobile/android/vvm/control/SetupController;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->isTC:Z

    .line 66
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_context:Landroid/content/Context;

    .line 67
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 68
    new-instance v0, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;-><init>(Lcom/att/mobile/android/vvm/control/SetupController;Lcom/att/mobile/android/vvm/control/SetupController$1;)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/SetupController;->setupCallBackHandler:Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;

    .line 70
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 72
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    const-string v2, "timerDefaultPref"

    const-class v3, Ljava/lang/Integer;

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 72
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->waitMaxTime:I

    .line 75
    return-void

    .line 73
    :cond_0
    const-string v0, "60"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/control/SetupController;)Lcom/att/mobile/android/vvm/model/db/ModelManager;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/SetupController;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/control/SetupController;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/SetupController;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/SetupController;->callUiCallback()V

    return-void
.end method

.method private callUiCallback()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_callback:Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_callback:Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

    invoke-interface {v0}, Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;->onSetupStateChange()V

    .line 123
    :cond_0
    return-void
.end method

.method private callVoicemail()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 192
    const-string v5, "SetupController"

    const-string v6, "callVoicemail"

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_context:Landroid/content/Context;

    const-string v6, "android.permission.READ_PHONE_STATE"

    invoke-static {v5, v6}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 195
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_context:Landroid/content/Context;

    const-string v6, "phone"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 196
    .local v1, "manager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getVoiceMailNumber()Ljava/lang/String;

    move-result-object v3

    .line 199
    .local v3, "voicemailNum":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 200
    const-string v4, "SetupController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "callVoicemail => calling number "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tel:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 203
    .local v2, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.CALL"

    invoke-direct {v0, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 204
    .local v0, "it":Landroid/content/Intent;
    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_context:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 205
    const/4 v4, 0x1

    .line 213
    .end local v0    # "it":Landroid/content/Intent;
    .end local v1    # "manager":Landroid/telephony/TelephonyManager;
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v3    # "voicemailNum":Ljava/lang/String;
    :cond_0
    :goto_0
    return v4

    .line 207
    .restart local v1    # "manager":Landroid/telephony/TelephonyManager;
    .restart local v3    # "voicemailNum":Ljava/lang/String;
    :cond_1
    const-string v5, "SetupController"

    const-string v6, "callVoicemail() voicemail number cannot be found"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/att/mobile/android/vvm/control/SetupController;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const-class v1, Lcom/att/mobile/android/vvm/control/SetupController;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/att/mobile/android/vvm/control/SetupController;->_instance:Lcom/att/mobile/android/vvm/control/SetupController;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/control/SetupController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/SetupController;->_instance:Lcom/att/mobile/android/vvm/control/SetupController;

    .line 61
    :cond_0
    sget-object v0, Lcom/att/mobile/android/vvm/control/SetupController;->_instance:Lcom/att/mobile/android/vvm/control/SetupController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private sendMOSMS()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 218
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/control/SetupController;->setTimerWorking(Z)V

    .line 219
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 220
    .local v0, "smsManager":Landroid/telephony/SmsManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GET?c="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/VVMApplication;->getClientId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "&v=1.0&l="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/SetupController;->mailboxNumber:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "&AD"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 221
    .local v3, "smsToSend":Ljava/lang/String;
    const-string v1, "SetupController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "before sending MO-SMS smstosend= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const-string v1, "94183567"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 223
    return-void
.end method


# virtual methods
.method public cancelTimerWorking()V
    .locals 2

    .prologue
    .line 246
    const-string v0, "SetupController"

    const-string v1, "cancelTimerWorking"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    sget-object v0, Lcom/att/mobile/android/vvm/control/SetupController;->setupCallBackHandler:Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->removeMessages(I)V

    .line 248
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 79
    sput-object v1, Lcom/att/mobile/android/vvm/control/SetupController;->setupCallBackHandler:Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;

    .line 80
    sput-object v1, Lcom/att/mobile/android/vvm/control/SetupController;->_instance:Lcom/att/mobile/android/vvm/control/SetupController;

    .line 81
    return-void
.end method

.method public declared-synchronized handleSetup()V
    .locals 5

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    .line 146
    .local v0, "currentState":I
    const/4 v1, 0x0

    .line 147
    .local v1, "needUiCallBack":Z
    const-string v2, "SetupController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleSetup currentSetupState="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/Constants;->getSetupStatusString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    sparse-switch v0, :sswitch_data_0

    .line 183
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 184
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/SetupController;->callUiCallback()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 151
    :sswitch_0
    :try_start_1
    const-string v2, "SetupController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mailboxNumber="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/SetupController;->mailboxNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/SetupController;->mailboxNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 153
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 145
    .end local v0    # "currentState":I
    .end local v1    # "needUiCallBack":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 155
    .restart local v0    # "currentState":I
    .restart local v1    # "needUiCallBack":Z
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_1

    .line 161
    :sswitch_1
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/SetupController;->sendMOSMS()V

    .line 162
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-le v2, v3, :cond_0

    .line 163
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_context:Landroid/content/Context;

    const v3, 0x7f080137

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Utils;->showToast(Ljava/lang/String;I)V

    goto :goto_0

    .line 169
    :sswitch_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/SetupController;->callVoicemail()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 170
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 171
    const/4 v1, 0x0

    goto :goto_0

    .line 173
    :cond_3
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 174
    const/4 v1, 0x1

    .line 176
    goto :goto_0

    .line 178
    :sswitch_3
    const/4 v1, 0x1

    goto :goto_0

    .line 149
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_3
        0xf -> :sswitch_2
    .end sparse-switch
.end method

.method public onNetworkFailure()V
    .locals 2

    .prologue
    .line 300
    const-string v0, "SetupController"

    const-string v1, "onNetworkFailure"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    return-void
.end method

.method public onUpdateListener(ILjava/util/ArrayList;)V
    .locals 10
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v9, 0x3

    const/4 v8, 0x4

    .line 253
    const-string v3, "SetupController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onUpdateListener() CurrentSetupState="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " eventId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " callback="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_callback:Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v3

    const/16 v4, 0xd

    if-ne v3, v4, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    const/16 v3, 0x40

    if-ne p1, v3, :cond_5

    .line 261
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMailBoxStatus()Ljava/lang/String;

    move-result-object v1

    .line 262
    .local v1, "mboxStatus":Ljava/lang/String;
    const-string v3, "SetupController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onUpdateListener() PROVISIONING_SMS mboxStatus="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "mosmsTimePref"

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 264
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    .line 266
    .local v0, "currentState":I
    const-string v3, "I"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-eq v0, v9, :cond_2

    if-eq v0, v8, :cond_2

    const/4 v3, 0x5

    if-ne v0, v3, :cond_3

    .line 267
    :cond_2
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "passwordPref"

    const-class v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 268
    .local v2, "passwordValue":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 269
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 274
    .end local v2    # "passwordValue":Ljava/lang/String;
    :cond_3
    :goto_1
    const-string v3, "C"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "R"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 275
    :cond_4
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 276
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v4, 0xe

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 290
    .end local v0    # "currentState":I
    .end local v1    # "mboxStatus":Ljava/lang/String;
    :cond_5
    :goto_2
    sget-object v3, Lcom/att/mobile/android/vvm/control/SetupController;->setupCallBackHandler:Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->removeMessages(I)V

    .line 291
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/control/SetupController;->setTimerWorking(Z)V

    .line 293
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_callback:Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

    if-eqz v3, :cond_0

    .line 294
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_callback:Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

    invoke-interface {v3}, Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;->onSetupStateChange()V

    goto/16 :goto_0

    .line 271
    .restart local v0    # "currentState":I
    .restart local v1    # "mboxStatus":Ljava/lang/String;
    .restart local v2    # "passwordValue":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSetupCompleted()V

    goto :goto_1

    .line 278
    .end local v2    # "passwordValue":Ljava/lang/String;
    :cond_7
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_2

    .line 281
    :cond_8
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    .line 282
    if-ne v0, v9, :cond_9

    .line 283
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v3, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_2

    .line 284
    :cond_9
    if-ne v0, v8, :cond_5

    .line 286
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/SetupController;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const/16 v4, 0x12

    invoke-virtual {v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_2
.end method

.method public registerCallback(Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;)V
    .locals 3
    .param p1, "callback"    # Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

    .prologue
    .line 134
    const-string v0, "SetupController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerCallback callback="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_callback:Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

    .line 136
    return-void
.end method

.method public setMailboxNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "mailboxNumber"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/SetupController;->mailboxNumber:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setTimerWorking(Z)V
    .locals 5
    .param p1, "isWorking"    # Z

    .prologue
    const/4 v4, 0x1

    .line 233
    const-string v0, "SetupController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTimerWorking="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    if-eqz p1, :cond_0

    .line 235
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->acquireWakeLock()V

    .line 236
    sget-object v0, Lcom/att/mobile/android/vvm/control/SetupController;->setupCallBackHandler:Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;

    iget v1, p0, Lcom/att/mobile/android/vvm/control/SetupController;->waitMaxTime:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 237
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->timerStartTime:J

    .line 242
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getInstance()Lcom/att/mobile/android/vvm/VVMApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/VVMApplication;->releaseWakeLock()V

    .line 240
    sget-object v0, Lcom/att/mobile/android/vvm/control/SetupController;->setupCallBackHandler:Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;

    invoke-virtual {v0, v4}, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->removeMessages(I)V

    goto :goto_0
.end method

.method public unregisterCallback()V
    .locals 2

    .prologue
    .line 139
    const-string v0, "SetupController"

    const-string v1, "unregisterCallback"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/SetupController;->_callback:Lcom/att/mobile/android/vvm/control/SetupController$OnSetupCallbackListener;

    .line 141
    return-void
.end method

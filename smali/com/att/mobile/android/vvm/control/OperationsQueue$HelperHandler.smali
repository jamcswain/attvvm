.class Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;
.super Landroid/os/Handler;
.source "OperationsQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/OperationsQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HelperHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/control/OperationsQueue;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/control/OperationsQueue;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1363
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->this$0:Lcom/att/mobile/android/vvm/control/OperationsQueue;

    .line 1364
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1365
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x2

    .line 1370
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$200()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 1372
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1373
    const-string v1, "OperationsQueue"

    const-string v2, "OperationsQueue.handleMessage() EVENT_RETRY_FETCH_BODIES"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1375
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->this$0:Lcom/att/mobile/android/vvm/control/OperationsQueue;

    new-instance v2, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;

    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$300()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->this$0:Lcom/att/mobile/android/vvm/control/OperationsQueue;

    .line 1376
    invoke-static {v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$400(Lcom/att/mobile/android/vvm/control/OperationsQueue;)Lcom/att/mobile/android/vvm/control/Dispatcher;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    .line 1375
    invoke-static {v1, v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$500(Lcom/att/mobile/android/vvm/control/OperationsQueue;Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1391
    :cond_0
    :goto_0
    return-void

    .line 1377
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v3, :cond_0

    .line 1378
    const-string v1, "OperationsQueue"

    const-string v2, "OperationsQueue.handleMessage() EVENT_XCHANGE_PASSWORD"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->this$0:Lcom/att/mobile/android/vvm/control/OperationsQueue;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$600(Lcom/att/mobile/android/vvm/control/OperationsQueue;)I

    move-result v1

    sget v2, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-ne v1, v2, :cond_0

    .line 1383
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;

    .line 1384
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$300()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->this$0:Lcom/att/mobile/android/vvm/control/OperationsQueue;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$400(Lcom/att/mobile/android/vvm/control/OperationsQueue;)Lcom/att/mobile/android/vvm/control/Dispatcher;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    .line 1385
    .local v0, "xChangeTUIPasswordOperation":Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/operations/XChangeTUIPasswordOperation;->execute()I

    .line 1386
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->this$0:Lcom/att/mobile/android/vvm/control/OperationsQueue;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$702(Lcom/att/mobile/android/vvm/control/OperationsQueue;Z)Z

    .line 1387
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->access$800()Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->close()V

    goto :goto_0
.end method

.class Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;
.super Landroid/os/Handler;
.source "SetupController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/SetupController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetupCallBackHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/control/SetupController;


# direct methods
.method private constructor <init>(Lcom/att/mobile/android/vvm/control/SetupController;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->this$0:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/att/mobile/android/vvm/control/SetupController;Lcom/att/mobile/android/vvm/control/SetupController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/att/mobile/android/vvm/control/SetupController;
    .param p2, "x1"    # Lcom/att/mobile/android/vvm/control/SetupController$1;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;-><init>(Lcom/att/mobile/android/vvm/control/SetupController;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0x12

    .line 88
    const-string v1, "SetupCallBackHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage msg.what="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 91
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->this$0:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/control/SetupController;->access$100(Lcom/att/mobile/android/vvm/control/SetupController;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v0

    .line 92
    .local v0, "currentState":I
    const-string v1, "SetupCallBackHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage currentState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/Constants;->getSetupStatusString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    packed-switch v0, :pswitch_data_0

    .line 109
    :goto_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->this$0:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/control/SetupController;->access$200(Lcom/att/mobile/android/vvm/control/SetupController;)V

    .line 110
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->this$0:Lcom/att/mobile/android/vvm/control/SetupController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/control/SetupController;->setTimerWorking(Z)V

    .line 115
    .end local v0    # "currentState":I
    :goto_1
    return-void

    .line 95
    .restart local v0    # "currentState":I
    :pswitch_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->this$0:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/control/SetupController;->access$100(Lcom/att/mobile/android/vvm/control/SetupController;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_0

    .line 99
    :pswitch_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->this$0:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/control/SetupController;->access$100(Lcom/att/mobile/android/vvm/control/SetupController;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_0

    .line 103
    :pswitch_2
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/SetupController$SetupCallBackHandler;->this$0:Lcom/att/mobile/android/vvm/control/SetupController;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/control/SetupController;->access$100(Lcom/att/mobile/android/vvm/control/SetupController;)Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    goto :goto_0

    .line 113
    .end local v0    # "currentState":I
    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_1

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

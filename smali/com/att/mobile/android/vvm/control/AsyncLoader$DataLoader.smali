.class Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;
.super Ljava/lang/Thread;
.source "AsyncLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/AsyncLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataLoader"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field inboxRefreshNeeded:Z

.field final synthetic this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/control/AsyncLoader;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 272
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->inboxRefreshNeeded:Z

    .line 275
    iput-object p2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->context:Landroid/content/Context;

    .line 276
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 283
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->access$200(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    move-result-object v2

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :try_start_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->access$200(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    move-result-object v2

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 286
    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->inboxRefreshNeeded:Z

    if-eqz v2, :cond_2

    .line 287
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->context:Landroid/content/Context;

    instance-of v2, v2, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    if-eqz v2, :cond_1

    .line 288
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->context:Landroid/content/Context;

    check-cast v2, Lcom/att/mobile/android/vvm/screen/InboxActivity;

    const/16 v4, 0x39

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/att/mobile/android/vvm/screen/InboxActivity;->onUpdateListener(ILjava/util/ArrayList;)V

    .line 290
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->inboxRefreshNeeded:Z

    .line 292
    :cond_2
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->access$200(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    move-result-object v2

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V

    .line 294
    :cond_3
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295
    :try_start_2
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->access$200(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    move-result-object v2

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-eqz v2, :cond_4

    .line 297
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->access$200(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    move-result-object v2

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v3

    monitor-enter v3
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 298
    :try_start_3
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->access$200(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;

    move-result-object v2

    invoke-static {v2}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;->access$100(Lcom/att/mobile/android/vvm/control/AsyncLoader$DataQueue;)Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;

    .line 299
    .local v6, "dataToLoad":Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 300
    :try_start_4
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    iget-object v3, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->phoneNumber:Ljava/lang/String;

    iget-object v4, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    move-result v4

    iget-object v5, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->access$300(Lcom/att/mobile/android/vvm/control/AsyncLoader;Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 303
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->imageView:Landroid/widget/ImageView;

    const v3, 0x7f080115

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    .line 304
    .local v7, "imageViewTag":Ljava/lang/Long;
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v4, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->id:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    .line 305
    const-string v2, "***AsyncLoader***"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "popped from queue id = ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->id:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    new-instance v0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;

    iget-object v2, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->imageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->this$0:Lcom/att/mobile/android/vvm/control/AsyncLoader;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/AsyncLoader;->access$400(Lcom/att/mobile/android/vvm/control/AsyncLoader;)Ljava/util/HashMap;

    move-result-object v3

    iget-object v4, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->textView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;-><init>(Landroid/graphics/Bitmap;Landroid/widget/ImageView;Ljava/lang/String;Landroid/widget/TextView;Lcom/att/mobile/android/vvm/control/AsyncLoader$1;)V

    .line 307
    .local v0, "dataDisplayerTask":Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;
    iget-object v2, v6, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    .line 308
    .local v8, "viewContextActivity":Landroid/app/Activity;
    invoke-virtual {v8, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 314
    .end local v0    # "dataDisplayerTask":Lcom/att/mobile/android/vvm/control/AsyncLoader$DataDisplayer;
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "dataToLoad":Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;
    .end local v7    # "imageViewTag":Ljava/lang/Long;
    .end local v8    # "viewContextActivity":Landroid/app/Activity;
    :cond_4
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 320
    :goto_1
    return-void

    .line 294
    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v2
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    .line 317
    :catch_0
    move-exception v2

    goto :goto_1

    .line 299
    :catchall_1
    move-exception v2

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2

    .line 311
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v6    # "dataToLoad":Lcom/att/mobile/android/vvm/control/AsyncLoader$DataToLoad;
    .restart local v7    # "imageViewTag":Ljava/lang/Long;
    :cond_5
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/control/AsyncLoader$DataLoader;->inboxRefreshNeeded:Z
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_0
.end method

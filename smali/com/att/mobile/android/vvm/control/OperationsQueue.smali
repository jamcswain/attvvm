.class public final Lcom/att/mobile/android/vvm/control/OperationsQueue;
.super Ljava/lang/Thread;
.source "OperationsQueue.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/IEventDispatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;,
        Lcom/att/mobile/android/vvm/control/OperationsQueue$OperationsQueueHolder;
    }
.end annotation


# static fields
.field private static final EVENT_RETRY_FETCH_BODIES:I = 0x1

.field private static final EVENT_XCHANGE_PASSWORD:I = 0x2

.field private static final TAG:Ljava/lang/String; = "OperationsQueue"

.field private static context:Landroid/content/Context;

.field private static imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

.field private static isnetworkFailureLockObjectNotified:Ljava/lang/Boolean;

.field private static maxRetriesConnect:I

.field private static maxRetriesFetchRequest:I

.field private static maxRetriesTransaction:I

.field private static modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private static retryFetchSeconds:I

.field private static retryIntervalSeconds:I


# instance fields
.field private currentFetchBodiesRetries:I

.field private currentNetworkRetries:I

.field private currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

.field private currentProtocolRetries:I

.field private dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

.field private fetchHeadersOperationLockObject:Ljava/lang/Object;

.field private getMetaDataOperationLockObject:Ljava/lang/Object;

.field private helperHandler:Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;

.field private helperHandlerThread:Landroid/os/HandlerThread;

.field private isAlive:Z

.field private isLoggedIn:Z

.field private isXChangeTUIPasswordOperationPending:Z

.field private networkFailureLockObject:Ljava/lang/Object;

.field private numberOfMessages:I

.field private operationsQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/att/mobile/android/vvm/control/operations/Operation;",
            ">;"
        }
    .end annotation
.end field

.field private pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

.field private pendingGetExistingGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

.field private pendingGetGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

.field private tuiSkipOperationLockObject:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isnetworkFailureLockObjectNotified:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 226
    const-class v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 89
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isAlive:Z

    .line 93
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->operationsQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 96
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isLoggedIn:Z

    .line 99
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->networkFailureLockObject:Ljava/lang/Object;

    .line 103
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    .line 112
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    .line 115
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetExistingGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    .line 121
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->fetchHeadersOperationLockObject:Ljava/lang/Object;

    .line 124
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getMetaDataOperationLockObject:Ljava/lang/Object;

    .line 125
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->tuiSkipOperationLockObject:Ljava/lang/Object;

    .line 168
    iput v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->numberOfMessages:I

    .line 183
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isXChangeTUIPasswordOperationPending:Z

    .line 229
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->operationsQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 235
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getDispatcher()Lcom/att/mobile/android/vvm/control/Dispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 246
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->start()V

    .line 248
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "OperationsQueueHelperHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->helperHandlerThread:Landroid/os/HandlerThread;

    .line 250
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->helperHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 255
    new-instance v0, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->helperHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;-><init>(Lcom/att/mobile/android/vvm/control/OperationsQueue;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->helperHandler:Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;

    .line 256
    return-void
.end method

.method synthetic constructor <init>(Lcom/att/mobile/android/vvm/control/OperationsQueue$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/att/mobile/android/vvm/control/OperationsQueue$1;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;-><init>()V

    return-void
.end method

.method static synthetic access$200()Lcom/att/mobile/android/vvm/model/db/ModelManager;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-object v0
.end method

.method static synthetic access$300()Landroid/content/Context;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/control/OperationsQueue;)Lcom/att/mobile/android/vvm/control/Dispatcher;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/OperationsQueue;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    return-object v0
.end method

.method static synthetic access$500(Lcom/att/mobile/android/vvm/control/OperationsQueue;Lcom/att/mobile/android/vvm/control/operations/Operation;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/OperationsQueue;
    .param p1, "x1"    # Lcom/att/mobile/android/vvm/control/operations/Operation;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    return-void
.end method

.method static synthetic access$600(Lcom/att/mobile/android/vvm/control/OperationsQueue;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/OperationsQueue;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->connect()I

    move-result v0

    return v0
.end method

.method static synthetic access$702(Lcom/att/mobile/android/vvm/control/OperationsQueue;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/OperationsQueue;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isXChangeTUIPasswordOperationPending:Z

    return p1
.end method

.method static synthetic access$800()Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    return-object v0
.end method

.method private connect()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 516
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    const-string v2, "hostPref"

    const-class v3, Ljava/lang/String;

    .line 517
    invoke-virtual {v1, v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 520
    .local v0, "hostAddress":Ljava/lang/String;
    if-eqz v0, :cond_0

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    if-nez v1, :cond_1

    .line 521
    :cond_0
    const-string v1, "OperationsQueue"

    const-string v2, "connect() No host is saved, connect has failed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    sget v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 523
    sget v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    .line 544
    :goto_0
    return v1

    .line 526
    :cond_1
    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->connect()Z

    .line 529
    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->isConnected()Z

    move-result v1

    if-nez v1, :cond_3

    .line 531
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryOnConnectError()Z

    move-result v1

    if-nez v1, :cond_2

    .line 533
    sget v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    invoke-direct {p0, v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 534
    const/16 v1, 0x32

    invoke-virtual {p0, v1, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyListeners(ILjava/util/ArrayList;)V

    .line 538
    :cond_2
    sget v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0

    .line 542
    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    .line 544
    sget v1, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_0
.end method

.method public static createInstance(Landroid/content/Context;)V
    .locals 6
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 192
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 193
    sput-object p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    .line 196
    :cond_0
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    const v1, 0x7f0801b8

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x1e

    .line 197
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 196
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryIntervalSeconds:I

    .line 198
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    const v1, 0x7f0801b7

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x3c

    .line 199
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 198
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryFetchSeconds:I

    .line 200
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    const v1, 0x7f08011e

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x2

    .line 201
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 200
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesConnect:I

    .line 202
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    const v1, 0x7f080120

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x5

    .line 203
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 202
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesTransaction:I

    .line 204
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    const v1, 0x7f08011f

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0xa

    .line 205
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 204
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesFetchRequest:I

    .line 207
    invoke-static {}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->getInstance()Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    .line 208
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->init(Landroid/content/Context;)V

    .line 210
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 212
    return-void
.end method

.method private enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V
    .locals 2
    .param p1, "operation"    # Lcom/att/mobile/android/vvm/control/operations/Operation;

    .prologue
    .line 896
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAttmStatus()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 897
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->operationsQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 899
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;
    .locals 2

    .prologue
    .line 215
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 216
    const-string v0, "OperationsQueue"

    const-string v1, "OperationsQueue.getInstance() must call create instance before calling getInstance()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue$OperationsQueueHolder;->access$100()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    return-object v0
.end method

.method public static getIsnetworkFailureLockObjectNotified()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isnetworkFailureLockObjectNotified:Ljava/lang/Boolean;

    return-object v0
.end method

.method private logoutAndTerminateConnection()V
    .locals 4

    .prologue
    .line 1244
    const-string v1, "OperationsQueue"

    const-string v2, "OperationsQueue.logoutAndTerminateConnection()"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1250
    const-string v1, "OperationsQueue"

    const-string v2, "OperationsQueue.logoutAndTerminateConnection() - connection not connected"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isLoggedIn:Z

    .line 1273
    return-void

    .line 1255
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ATT_VVM__ "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "LOGOUT\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1256
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1259
    .local v0, "command":Ljava/lang/String;
    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    const/4 v2, 0x1

    .line 1260
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 1259
    invoke-virtual {v1, v2, v3}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->executeImapCommand(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .line 1263
    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->imap4handler:Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->close()V

    .line 1265
    const-string v1, "OperationsQueue"

    const-string v2, "OperationsQueue.logoutAndTerminateConnection() - End of session, connection closed by client"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private resetQueueOnFailure(I)V
    .locals 1
    .param p1, "operationFailureType"    # I

    .prologue
    .line 848
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    if-ne p1, v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListenersForNetworkFailure()V

    .line 855
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueue()V

    .line 856
    return-void
.end method

.method private retryFetchBodies()V
    .locals 4

    .prologue
    .line 790
    iget v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentFetchBodiesRetries:I

    sget v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesFetchRequest:I

    if-gt v0, v1, :cond_0

    .line 792
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->helperHandler:Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;

    const/4 v1, 0x1

    sget v2, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryFetchSeconds:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 794
    iget v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentFetchBodiesRetries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentFetchBodiesRetries:I

    .line 796
    const-string v0, "OperationsQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationsQueue.retryFetchBodies() - retry fetch bodies num "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentFetchBodiesRetries:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryFetchSeconds:I

    mul-int/lit16 v2, v2, 0x3e8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " millis"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    :goto_0
    return-void

    .line 802
    :cond_0
    const-string v0, "OperationsQueue"

    const-string v1, "OperationsQueue.retryFetchBodies() - max retries of fetch bodies request has reached"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    const/4 v0, 0x0

    iput v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentFetchBodiesRetries:I

    .line 808
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v0

    const/16 v1, 0x35

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyListeners(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method private retryOnConnectError()Z
    .locals 4

    .prologue
    .line 721
    iget v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    sget v2, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesConnect:I

    if-lt v1, v2, :cond_0

    .line 722
    const-string v1, "OperationsQueue"

    const-string v2, "OperationsQueue.retryOnConnectError() - maximum number of socket connection retries has been reached!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    const/4 v1, 0x0

    .line 737
    :goto_0
    return v1

    .line 726
    :cond_0
    const-string v1, "OperationsQueue"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OperationsQueue.retryOnConnectError() - socket connection retry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    :try_start_0
    sget v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryIntervalSeconds:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 737
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 733
    :catch_0
    move-exception v0

    .line 734
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "OperationsQueue"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private retryOnNetworkError()Z
    .locals 4

    .prologue
    .line 748
    :try_start_0
    iget v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    sget v2, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesTransaction:I

    if-lt v1, v2, :cond_0

    .line 749
    const-string v1, "OperationsQueue"

    const-string v2, "OperationsQueue.retryOnNetworkError() - maximum number of network retries has been reached!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 751
    const/4 v1, 0x0

    .line 780
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    :goto_0
    return v1

    .line 764
    :cond_0
    :try_start_1
    const-string v1, "OperationsQueue"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OperationsQueue.retryOnNetworkError() - network retry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 770
    :try_start_2
    sget v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryIntervalSeconds:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 776
    :goto_1
    const/4 v1, 0x1

    .line 780
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    goto :goto_0

    .line 771
    :catch_0
    move-exception v0

    .line 772
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v1, "OperationsQueue"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 780
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    throw v1
.end method

.method private selectInbox()I
    .locals 7

    .prologue
    .line 634
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;

    sget-object v4, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;-><init>(Landroid/content/Context;)V

    .line 636
    .local v0, "getQuotaOperation":Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;->execute()I

    move-result v1

    .line 637
    .local v1, "getQuotaResult":I
    const-string v4, "OperationsQueue"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "OperationsQueue.selectInbox() - getQuotaResult "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-ne v1, v4, :cond_2

    .line 639
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v4

    const-string v5, "MaxMessages"

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/operations/GetQuotaOperation;->getServerQuota()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 640
    const-string v4, "OperationsQueue"

    const-string v5, "OperationsQueue.selectInbox() - getQuotaResult succeeded"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    :cond_0
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    if-ne v1, v4, :cond_4

    .line 654
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    .line 657
    iget v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    sget v5, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesTransaction:I

    if-lt v4, v5, :cond_1

    .line 658
    const-string v4, "OperationsQueue"

    const-string v5, "OperationsQueue.selectInbox() - maximum number of protocol retries has been reached!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 662
    :cond_1
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    .line 711
    :goto_0
    return v4

    .line 641
    :cond_2
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    if-ne v1, v4, :cond_0

    .line 643
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryOnNetworkError()Z

    move-result v4

    if-nez v4, :cond_3

    .line 645
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 648
    :cond_3
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0

    .line 666
    :cond_4
    new-instance v2, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;

    sget-object v4, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;-><init>(Landroid/content/Context;)V

    .line 668
    .local v2, "selectInboxOperation":Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;->execute()I

    move-result v3

    .line 669
    .local v3, "selectInboxResult":I
    const-string v4, "OperationsQueue"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "OperationsQueue.selectInbox() - selectInboxResult "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    if-ne v3, v4, :cond_6

    .line 677
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryOnNetworkError()Z

    move-result v4

    if-nez v4, :cond_5

    .line 679
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 682
    :cond_5
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0

    .line 687
    :cond_6
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    if-ne v3, v4, :cond_8

    .line 688
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    .line 691
    iget v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    sget v5, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesTransaction:I

    if-lt v4, v5, :cond_7

    .line 692
    const-string v4, "OperationsQueue"

    const-string v5, "OperationsQueue.getServerNumberOfMessages() - maximum number of protocol retries has been reached!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 697
    :cond_7
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 701
    :cond_8
    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    if-eqz v4, :cond_9

    .line 702
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation;->getServerNumberOfMessage()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->setNumberOfMessages(I)V

    .line 705
    :cond_9
    sget v4, Lcom/att/mobile/android/vvm/control/operations/SelectInboxOperation$Result;->SUCCEED_NO_MESSAGES_EXIST:I

    if-ne v3, v4, :cond_a

    .line 709
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED_NO_MESSAGES_EXIST:I

    goto :goto_0

    .line 711
    :cond_a
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto :goto_0
.end method

.method public static setIsnetworkFailureLockObjectNotified(Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "isnetworkFailureLockObjectNotified"    # Ljava/lang/Boolean;

    .prologue
    .line 85
    sput-object p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isnetworkFailureLockObjectNotified:Ljava/lang/Boolean;

    .line 86
    return-void
.end method

.method private verifyLogin()I
    .locals 8

    .prologue
    const/16 v7, 0x32

    const/4 v6, 0x0

    .line 554
    iget-boolean v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isLoggedIn:Z

    if-eqz v4, :cond_0

    .line 555
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->ALREADY_LOGGED_IN:I

    .line 623
    :goto_0
    return v4

    .line 559
    :cond_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->connect()I

    move-result v4

    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    if-ne v4, v5, :cond_1

    .line 560
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0

    .line 564
    :cond_1
    new-instance v2, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;

    sget-object v4, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;-><init>(Landroid/content/Context;)V

    .line 565
    .local v2, "loginOperation":Lcom/att/mobile/android/vvm/control/operations/LoginOperation;
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/operations/LoginOperation;->execute()I

    move-result v3

    .line 568
    .local v3, "loginOperationResult":I
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    if-ne v3, v4, :cond_3

    .line 569
    const-string v4, "OperationsQueue"

    const-string v5, "OperationsQueue.verifyLogin() - login failed."

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    .line 575
    iget v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    sget v5, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesTransaction:I

    if-lt v4, v5, :cond_2

    .line 576
    const-string v4, "OperationsQueue"

    const-string v5, "OperationsQueue.verifyLogin() - maximum number of protocol retries has been reached!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 579
    invoke-virtual {p0, v7, v6}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyListeners(ILjava/util/ArrayList;)V

    .line 589
    :goto_1
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    goto :goto_0

    .line 583
    :cond_2
    :try_start_0
    sget v4, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryIntervalSeconds:I

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "OperationsQueue"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 592
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    if-ne v3, v4, :cond_5

    .line 594
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryOnNetworkError()Z

    move-result v4

    if-nez v4, :cond_4

    .line 596
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 597
    invoke-virtual {p0, v7, v6}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyListeners(ILjava/util/ArrayList;)V

    .line 599
    :cond_4
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    goto :goto_0

    .line 600
    :cond_5
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED_WRONG_PASSWORD:I

    if-ne v3, v4, :cond_6

    .line 601
    const-string v4, "OperationsQueue"

    const-string v5, "OperationsQueue.verifyLogin() - login failed due to wrong password"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    .line 606
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    invoke-direct {p0, v4}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    .line 610
    const/16 v4, 0x31

    invoke-virtual {p0, v4, v6}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyListeners(ILjava/util/ArrayList;)V

    .line 612
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.att.mobile.android.vvm.ACTION_PASSWORD_MISSMATCH"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 613
    .local v1, "intent":Landroid/content/Intent;
    sget-object v4, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    const-class v5, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 614
    sget-object v4, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 616
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED_WRONG_PASSWORD:I

    goto/16 :goto_0

    .line 620
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_6
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isLoggedIn:Z

    .line 623
    sget v4, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    goto/16 :goto_0
.end method


# virtual methods
.method public addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/Dispatcher;->addListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 1316
    return-void
.end method

.method public enqueueDeleteOperation(Landroid/content/Context;[Ljava/lang/Long;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mesUIDs"    # [Ljava/lang/Long;

    .prologue
    .line 968
    if-eqz p2, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 969
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;

    invoke-direct {v0, p1, p2}, Lcom/att/mobile/android/vvm/control/operations/DeleteOperation;-><init>(Landroid/content/Context;[Ljava/lang/Long;)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 971
    :cond_0
    return-void
.end method

.method public enqueueFetchHeadersAndBodiesOperation()V
    .locals 3

    .prologue
    .line 907
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->fetchHeadersOperationLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 908
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    if-eqz v0, :cond_0

    .line 909
    const-string v0, "OperationsQueue"

    const-string v2, "OperationsQueue.enqueueFetchHeadersAndBodiesOperation() - fetch headers operation is already pending for execution"

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    monitor-exit v1

    .line 933
    :goto_0
    return-void

    .line 914
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 916
    const-string v0, "OperationsQueue"

    const-string v1, "OperationsQueue.enqueueFetchHeadersAndBodiesOperation() - fetch headers and bodies operations are being queued."

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    .line 923
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 924
    sget-object v0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isNotifyOnNewMessagesEnabled()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 926
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->tuiSkipOperationLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 927
    :try_start_1
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;

    sget-object v2, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/att/mobile/android/vvm/control/operations/TuiSkipOperation;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 928
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 931
    :cond_1
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/control/operations/FetchBodiesOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    goto :goto_0

    .line 914
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 928
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public enqueueGetExistingGreetingsOperation()V
    .locals 5

    .prologue
    .line 1078
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getMetaDataOperationLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 1083
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetExistingGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetExistingGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    .line 1084
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    move-result-object v0

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_EXISTING_GREETINGS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-eq v0, v2, :cond_1

    .line 1085
    :cond_0
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    sget-object v2, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_EXISTING_GREETINGS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0, v2, v3, v4}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetExistingGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    .line 1089
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetExistingGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1091
    :cond_1
    monitor-exit v1

    .line 1092
    return-void

    .line 1091
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public enqueueGetGreetingsDetailsOperation()V
    .locals 5

    .prologue
    .line 1041
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getMetaDataOperationLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 1045
    :try_start_0
    const-string v0, "OperationsQueue"

    const-string v2, "OperationsQueue::enqueueGetGreetingsDetailsOperation"

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1048
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    .line 1049
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;->getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    move-result-object v0

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_GREETINGS_DETAILS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-eq v0, v2, :cond_1

    .line 1050
    :cond_0
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    sget-object v2, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    sget-object v3, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_GREETINGS_DETAILS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0, v2, v3, v4}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    .line 1054
    const-string v0, "OperationsQueue"

    const-string v2, "OperationsQueue::enqueueGetGreetingsDetailsOperation::enqueue"

    invoke-static {v0, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1058
    :cond_1
    monitor-exit v1

    .line 1059
    return-void

    .line 1058
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public enqueueGetPasswordLengthOperation()V
    .locals 4

    .prologue
    .line 1068
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1070
    return-void
.end method

.method public enqueueMarkAsReadOperation(Landroid/content/Context;J)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mesUID"    # J

    .prologue
    .line 999
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-eqz v0, :cond_0

    .line 1000
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/MarkAsReadOperation;

    invoke-direct {v0, p1, p2, p3}, Lcom/att/mobile/android/vvm/control/operations/MarkAsReadOperation;-><init>(Landroid/content/Context;J)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1002
    :cond_0
    return-void
.end method

.method public enqueueSendGreetingOperation(Ljava/lang/String;[B)V
    .locals 3
    .param p1, "greetingType"    # Ljava/lang/String;
    .param p2, "greetingData"    # [B

    .prologue
    .line 1175
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;-><init>(Landroid/content/Context;Ljava/lang/String;[BLcom/att/mobile/android/vvm/control/Dispatcher;)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1177
    return-void
.end method

.method public enqueueSetMetaDataGreetingTypeOperation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "metaDataToSet"    # Ljava/lang/String;
    .param p2, "metaDataValue"    # Ljava/lang/String;

    .prologue
    .line 1121
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_GREETING_TYPE:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1124
    return-void
.end method

.method public enqueueSetMetaDataOperation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "metaDataToSet"    # Ljava/lang/String;
    .param p2, "metaDataValue"    # Ljava/lang/String;

    .prologue
    .line 1105
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_GENERAL:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1108
    return-void
.end method

.method public enqueueSetPasswordMetaDataOperation(Ljava/lang/String;)V
    .locals 6
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 1134
    new-instance v0, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;

    sget-object v1, Lcom/att/mobile/android/vvm/control/OperationsQueue;->context:Landroid/content/Context;

    sget-object v2, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_SET_META_DATA_PASSWORD:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    const-string v3, "/private/vendor/vendor.alu/messaging/TUIPassword"

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/vvm/control/Dispatcher;)V

    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->enqueueOperation(Lcom/att/mobile/android/vvm/control/operations/Operation;)V

    .line 1137
    return-void
.end method

.method public enqueueXChangeTUIPasswordOperation(Ljava/lang/String;)V
    .locals 3
    .param p1, "newPassword"    # Ljava/lang/String;

    .prologue
    .line 1157
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isXChangeTUIPasswordOperationPending:Z

    if-nez v0, :cond_0

    .line 1159
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueue()V

    .line 1160
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->helperHandler:Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->helperHandler:Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/OperationsQueue$HelperHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isXChangeTUIPasswordOperationPending:Z

    .line 1164
    :cond_0
    return-void
.end method

.method public declared-synchronized getNumberOfMessages()I
    .locals 1

    .prologue
    .line 1358
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->numberOfMessages:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public notifyDisconnect()V
    .locals 1

    .prologue
    .line 1281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isLoggedIn:Z

    .line 1282
    return-void
.end method

.method public notifyListeners(ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1346
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 1347
    return-void
.end method

.method public notifyMobileConnectionExists(I)V
    .locals 2
    .param p1, "mobileConnectionState"    # I

    .prologue
    .line 1298
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->networkFailureLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 1299
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->networkFailureLockObject:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1300
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->setIsnetworkFailureLockObjectNotified(Ljava/lang/Boolean;)V

    .line 1301
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->networkFailureLockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1303
    :cond_0
    monitor-exit v1

    .line 1304
    return-void

    .line 1303
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 1327
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/Dispatcher;->removeListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 1328
    return-void
.end method

.method public removeEventListeners()V
    .locals 1

    .prologue
    .line 1334
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;->removeListeners()V

    .line 1335
    return-void
.end method

.method public resetQueue()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 865
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    .line 872
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->operationsQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 875
    iput v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    .line 876
    iput v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    .line 877
    iput v1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentFetchBodiesRetries:I

    .line 881
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    .line 884
    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 886
    return-void
.end method

.method public run()V
    .locals 13

    .prologue
    const/16 v12, 0x28

    const/16 v11, 0x15

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 264
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isAlive:Z

    .line 267
    iput-object v9, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 270
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 273
    :cond_0
    :goto_0
    iget-boolean v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->isAlive:Z

    if-eqz v5, :cond_11

    .line 275
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-nez v5, :cond_1

    .line 279
    :try_start_0
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->operationsQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    const-wide/16 v6, 0xe10

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/att/mobile/android/vvm/control/operations/Operation;

    iput-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :cond_1
    const-string v5, "OperationsQueue"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "OperationsQueue.run() currentOperation = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->verifyLogin()I

    move-result v2

    .line 292
    .local v2, "loginResult":I
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->ALREADY_LOGGED_IN:I

    if-eq v2, v5, :cond_4

    .line 295
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-ne v2, v5, :cond_0

    .line 303
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v6, 0x37

    invoke-virtual {v5, v6, v9}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 307
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    if-eqz v5, :cond_2

    .line 308
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v5, v11, v9}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 312
    :cond_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->selectInbox()I

    move-result v4

    .line 315
    .local v4, "selectInboxResult":I
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-eq v4, v5, :cond_3

    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED_NO_MESSAGES_EXIST:I

    if-ne v4, v5, :cond_0

    .line 326
    :cond_3
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    instance-of v5, v5, Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    instance-of v5, v5, Lcom/att/mobile/android/vvm/control/operations/SetMetaDataOperation;

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    instance-of v5, v5, Lcom/att/mobile/android/vvm/control/operations/SendGreetingOperation;

    if-nez v5, :cond_4

    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED_NO_MESSAGES_EXIST:I

    if-ne v4, v5, :cond_4

    .line 333
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 334
    invoke-virtual {v5, v12, v9}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 339
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueue()V

    goto :goto_0

    .line 281
    .end local v2    # "loginResult":I
    .end local v4    # "selectInboxResult":I
    :catch_0
    move-exception v1

    .line 283
    .local v1, "e":Ljava/lang/InterruptedException;
    goto :goto_0

    .line 348
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v2    # "loginResult":I
    :cond_4
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 349
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/operations/Operation;->getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    move-result-object v5

    sget-object v6, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_HEADERS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v5, v6, :cond_7

    .line 352
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v5, v11, v9}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 389
    :cond_5
    :goto_1
    sget v0, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    .line 390
    .local v0, "currentOperationResult":I
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-eqz v5, :cond_6

    .line 391
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/operations/Operation;->execute()I

    move-result v0

    .line 395
    :cond_6
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    if-ne v0, v5, :cond_9

    .line 397
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryOnNetworkError()Z

    move-result v5

    if-nez v5, :cond_0

    .line 400
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->NETWORK_ERROR:I

    invoke-direct {p0, v5}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->resetQueueOnFailure(I)V

    goto/16 :goto_0

    .line 376
    .end local v0    # "currentOperationResult":I
    :cond_7
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 377
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/operations/Operation;->getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    move-result-object v5

    sget-object v6, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_GREETINGS_DETAILS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v5, v6, :cond_8

    .line 378
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getMetaDataOperationLockObject:Ljava/lang/Object;

    monitor-enter v6

    .line 379
    const/4 v5, 0x0

    :try_start_1
    iput-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    .line 380
    monitor-exit v6

    goto :goto_1

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 381
    :cond_8
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 382
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/operations/Operation;->getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    move-result-object v5

    sget-object v6, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_GET_META_DATA_EXISTING_GREETINGS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v5, v6, :cond_5

    .line 383
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getMetaDataOperationLockObject:Ljava/lang/Object;

    monitor-enter v6

    .line 384
    const/4 v5, 0x0

    :try_start_2
    iput-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingGetExistingGreetingsOperation:Lcom/att/mobile/android/vvm/control/operations/GetMetaDataOperation;

    .line 385
    monitor-exit v6

    goto :goto_1

    :catchall_1
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v5

    .line 411
    .restart local v0    # "currentOperationResult":I
    :cond_9
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED:I

    if-ne v0, v5, :cond_b

    .line 412
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    .line 415
    iget v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    sget v6, Lcom/att/mobile/android/vvm/control/OperationsQueue;->maxRetriesTransaction:I

    if-lt v5, v6, :cond_0

    .line 416
    const-string v5, "OperationsQueue"

    const-string v6, "OperationsQueue.run() - maximum number of protocol retries has been reached!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 422
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/operations/Operation;->getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    move-result-object v5

    sget-object v6, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_HEADERS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v5, v6, :cond_a

    .line 424
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->fetchHeadersOperationLockObject:Ljava/lang/Object;

    monitor-enter v6

    .line 425
    const/4 v5, 0x0

    :try_start_3
    iput-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    .line 426
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 428
    :cond_a
    iput-object v9, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    goto/16 :goto_0

    .line 426
    :catchall_2
    move-exception v5

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v5

    .line 439
    :cond_b
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->FAILED_NOT_ENOUGH_SPACE:I

    if-ne v0, v5, :cond_c

    .line 440
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    .line 442
    iput-object v9, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    goto/16 :goto_0

    .line 448
    :cond_c
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 449
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/operations/Operation;->getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    move-result-object v5

    sget-object v6, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_HEADERS:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v5, v6, :cond_f

    .line 451
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->fetchHeadersOperationLockObject:Ljava/lang/Object;

    monitor-enter v6

    .line 452
    const/4 v5, 0x0

    :try_start_5
    iput-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    .line 453
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 456
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    const/16 v6, 0x16

    invoke-virtual {v5, v6, v9}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 458
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    const-string v6, "MaxMessages"

    const-class v7, Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 459
    .local v3, "quota":I
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getNumberOfMessages()I

    move-result v5

    add-int/lit8 v6, v3, -0x3

    if-lt v5, v6, :cond_d

    .line 460
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getNumberOfMessages()I

    move-result v5

    if-lt v5, v3, :cond_e

    .line 461
    const/16 v5, 0x2b

    invoke-virtual {p0, v5, v9}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyListeners(ILjava/util/ArrayList;)V

    .line 490
    .end local v3    # "quota":I
    :cond_d
    :goto_2
    iput-object v9, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 491
    iput v10, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentNetworkRetries:I

    .line 492
    iput v10, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentProtocolRetries:I

    .line 496
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->operationsQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 499
    iget-object v6, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->fetchHeadersOperationLockObject:Ljava/lang/Object;

    monitor-enter v6

    .line 500
    const/4 v5, 0x0

    :try_start_6
    iput-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->pendingFetchHeadersOperation:Lcom/att/mobile/android/vvm/control/operations/FetchHeadersOperation;

    .line 501
    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 505
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->logoutAndTerminateConnection()V

    goto/16 :goto_0

    .line 453
    :catchall_3
    move-exception v5

    :try_start_7
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v5

    .line 463
    .restart local v3    # "quota":I
    :cond_e
    const/16 v5, 0x2a

    invoke-virtual {p0, v5, v9}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyListeners(ILjava/util/ArrayList;)V

    goto :goto_2

    .line 468
    .end local v3    # "quota":I
    :cond_f
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentOperation:Lcom/att/mobile/android/vvm/control/operations/Operation;

    .line 469
    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/operations/Operation;->getType()Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    move-result-object v5

    sget-object v6, Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;->TYPE_FETCH_BODIES:Lcom/att/mobile/android/vvm/control/operations/Operation$OperationTypes;

    if-ne v5, v6, :cond_d

    .line 471
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->CONNECTION_CLOSED:I

    if-ne v0, v5, :cond_10

    .line 473
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryOnNetworkError()Z

    move-result v5

    if-nez v5, :cond_0

    .line 479
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->retryFetchBodies()V

    goto :goto_2

    .line 483
    :cond_10
    sget v5, Lcom/att/mobile/android/vvm/control/operations/Operation$Result;->SUCCEED:I

    if-ne v0, v5, :cond_d

    .line 484
    iput v10, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->currentFetchBodiesRetries:I

    goto :goto_2

    .line 501
    :catchall_4
    move-exception v5

    :try_start_8
    monitor-exit v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    throw v5

    .line 507
    .end local v0    # "currentOperationResult":I
    .end local v2    # "loginResult":I
    :cond_11
    return-void
.end method

.method public declared-synchronized setNumberOfMessages(I)V
    .locals 1
    .param p1, "numberOfMessages"    # I

    .prologue
    .line 1350
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/att/mobile/android/vvm/control/OperationsQueue;->numberOfMessages:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1351
    monitor-exit p0

    return-void

    .line 1350
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setQuota(I)V
    .locals 3
    .param p1, "numberOfMessages"    # I

    .prologue
    .line 1354
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v1, "MaxMessages"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1355
    const-string v0, "OperationsQueue"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OperationsQueue.setQuota() server quota = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1356
    monitor-exit p0

    return-void

    .line 1354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

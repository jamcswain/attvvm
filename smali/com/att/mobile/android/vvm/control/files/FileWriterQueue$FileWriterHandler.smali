.class Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;
.super Landroid/os/Handler;
.source "FileWriterQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileWriterHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    .line 93
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 94
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 104
    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v3, v5, :cond_4

    .line 106
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;

    .line 110
    .local v1, "fileWriterTask":Lcom/att/mobile/android/vvm/control/files/FileWriterTask;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getMessageFileIndex()I

    move-result v3

    if-ne v3, v5, :cond_0

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getChunkNumber()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 113
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-static {v3, v4}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->access$002(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;I)I

    .line 117
    :cond_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->access$100(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getFileName()Ljava/lang/String;

    move-result-object v6

    .line 118
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getData()[B

    move-result-object v7

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getChunkNumber()I

    move-result v8

    .line 119
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->isSkipHeader()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x6

    .line 117
    :goto_0
    invoke-static {v5, v6, v7, v8, v3}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->saveChunk(Landroid/content/Context;Ljava/lang/String;[BII)Z

    move-result v2

    .line 122
    .local v2, "success":Z
    if-eqz v2, :cond_3

    .line 124
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-static {v4}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->access$000(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;)I

    move-result v4

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getData()[B

    move-result-object v5

    array-length v5, v5

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->access$002(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;I)I

    .line 125
    const-string v3, "FileWriterQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FileWriterHandler.handleMessage() - chunk #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 126
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getChunkNumber()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " saved for file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 127
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", total number of bytes saved till now is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    .line 128
    invoke-static {v5}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->access$000(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 125
    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    .end local v1    # "fileWriterTask":Lcom/att/mobile/android/vvm/control/files/FileWriterTask;
    .end local v2    # "success":Z
    :cond_1
    :goto_1
    return-void

    .restart local v1    # "fileWriterTask":Lcom/att/mobile/android/vvm/control/files/FileWriterTask;
    :cond_2
    move v3, v4

    .line 119
    goto :goto_0

    .line 130
    .restart local v2    # "success":Z
    :cond_3
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-static {v3, v4}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->access$002(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;I)I

    .line 131
    const-string v3, "FileWriterQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FileWriterHandler.handleMessage() - chunk #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 132
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getChunkNumber()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed to be saved for file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 133
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", file deleted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 131
    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 137
    .end local v1    # "fileWriterTask":Lcom/att/mobile/android/vvm/control/files/FileWriterTask;
    .end local v2    # "success":Z
    :cond_4
    iget v3, p1, Landroid/os/Message;->what:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 139
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;

    .line 142
    .local v0, "fileCloserTask":Lcom/att/mobile/android/vvm/control/files/FileCloserTask;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->access$000(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;)I

    move-result v3

    iget v4, v0, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;->messageFilesSize:I

    if-ne v3, v4, :cond_5

    .line 143
    const-string v3, "FileWriterQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FileWriterHandler.handleMessage() all chunks saved going to update DB with file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;->fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    iget-object v4, v0, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;->messageDo:Lcom/att/mobile/android/vvm/model/db/MessageDo;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v4

    iget-object v6, v0, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;->fileName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageFileName(JLjava/lang/String;)Z

    goto :goto_1

    .line 149
    :cond_5
    const-string v3, "FileWriterQueue"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FileWriterHandler.handleMessage() error saving all chunks going to delete file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;->fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    iget-object v4, v0, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;->messageDo:Lcom/att/mobile/android/vvm/model/db/MessageDo;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageErrorFile(J)Z

    .line 155
    iget-object v3, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->this$0:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->access$100(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;->fileName:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_1
.end method

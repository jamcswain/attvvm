.class Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;
.super Ljava/lang/Object;
.source "VvmFileUtils.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$OnScanCompletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OnVVMScanCompletedListener"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field fileDirUri:Landroid/net/Uri;

.field shareFile:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shareFile"    # Z
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->shareFile:Z

    .line 49
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->context:Landroid/content/Context;

    .line 50
    iput-boolean p2, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->shareFile:Z

    .line 51
    iput-object p3, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->fileDirUri:Landroid/net/Uri;

    .line 52
    return-void
.end method


# virtual methods
.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 58
    const-string v0, "VvmFileUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VvmFileUtils.OnVVMScanCompletedListener - media at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has been scanned, URI is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    if-nez p2, :cond_0

    .line 62
    iget-object p2, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->fileDirUri:Landroid/net/Uri;

    .line 65
    :cond_0
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->shareFile:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->context:Landroid/content/Context;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 66
    const-string v0, "VvmFileUtils"

    const-string v1, "VvmFileUtils.OnVVMScanCompletedListener going to show share menu"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->context:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/att/mobile/android/vvm/screen/VVMActivity;->showAudioShareMenu(Landroid/content/Context;Landroid/net/Uri;)V

    .line 73
    :goto_0
    return-void

    .line 70
    :cond_1
    const-string v0, "VvmFileUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VvmFileUtils.OnVVMScanCompletedListener do not show share menu context = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;->context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

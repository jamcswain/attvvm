.class public Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;
.super Ljava/lang/Object;
.source "VvmFileUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;
    }
.end annotation


# static fields
.field public static final APPEND:I = 0x2

.field public static final APPEND_AND_CLOSE:I = 0x3

.field public static final CLOSE:I = 0x4

.field public static final OPEN_FILE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VvmFileUtils"


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static copyFile(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 7
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "copiedFile"    # Ljava/io/File;

    .prologue
    .line 265
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 266
    .local v4, "out":Ljava/io/OutputStream;
    const/16 v5, 0x400

    new-array v0, v5, [B

    .line 268
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .local v3, "len":I
    if-lez v3, :cond_0

    .line 269
    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 275
    .end local v0    # "buf":[B
    .end local v3    # "len":I
    .end local v4    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v1

    .line 276
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "VvmFileUtils"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 280
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 272
    .restart local v0    # "buf":[B
    .restart local v3    # "len":I
    .restart local v4    # "out":Ljava/io/OutputStream;
    :cond_0
    :try_start_1
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 273
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 277
    .end local v0    # "buf":[B
    .end local v3    # "len":I
    .end local v4    # "out":Ljava/io/OutputStream;
    :catch_1
    move-exception v2

    .line 278
    .local v2, "ex":Ljava/lang/Exception;
    const-string v5, "VvmFileUtils"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static copyFile(Ljava/io/File;Ljava/io/File;I)Z
    .locals 12
    .param p0, "fileToCopy"    # Ljava/io/File;
    .param p1, "copiedFile"    # Ljava/io/File;
    .param p2, "chunkSize"    # I

    .prologue
    const/4 v9, 0x0

    .line 384
    const/4 v5, 0x0

    .line 387
    .local v5, "fileToCopyInputStream":Ljava/io/FileInputStream;
    const/4 v0, 0x0

    .line 390
    .local v0, "copiedFileOutputStream":Ljava/io/FileOutputStream;
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v10

    long-to-int v7, v10

    .line 393
    .local v7, "fileToCopySize":I
    const/4 v8, 0x0

    .line 397
    .local v8, "totalNumberOfCopiedBytes":I
    if-lez p2, :cond_0

    if-lt p2, v7, :cond_1

    .line 399
    :cond_0
    move p2, v7

    .line 403
    :cond_1
    new-array v4, p2, [B

    .line 407
    .local v4, "fileToCopyData":[B
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    .end local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .local v6, "fileToCopyInputStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 411
    .end local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .local v1, "copiedFileOutputStream":Ljava/io/FileOutputStream;
    :goto_0
    if-ge v8, v7, :cond_6

    .line 415
    const/4 v2, 0x0

    .line 416
    .local v2, "currentNumberOfReadBytes":I
    const/4 v10, 0x0

    :try_start_2
    invoke-virtual {v6, v4, v10, p2}, Ljava/io/FileInputStream;->read([BII)I

    move-result v2

    if-eq v2, p2, :cond_5

    .line 418
    const-string v10, "VvmFileUtils"

    const-string v11, "VvmFileUtils.copyFile() - could not perform file copy."

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 437
    if-eqz v6, :cond_2

    .line 438
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 440
    :cond_2
    if-eqz v1, :cond_3

    .line 441
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    :goto_1
    move-object v0, v1

    .end local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    move-object v5, v6

    .line 445
    .end local v2    # "currentNumberOfReadBytes":I
    .end local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    :cond_4
    :goto_2
    return v9

    .line 443
    .end local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .end local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "currentNumberOfReadBytes":I
    .restart local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v3

    .line 444
    .local v3, "e":Ljava/lang/Exception;
    const-string v10, "VvmFileUtils"

    const-string v11, "VvmFileUtils.copyFile() - error releasing resources."

    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 422
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v10, 0x0

    :try_start_4
    invoke-virtual {v1, v4, v10, p2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 423
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 426
    add-int/2addr v8, v2

    .line 427
    goto :goto_0

    .line 430
    .end local v2    # "currentNumberOfReadBytes":I
    :cond_6
    const/4 v9, 0x1

    .line 437
    if-eqz v6, :cond_7

    .line 438
    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 440
    :cond_7
    if-eqz v1, :cond_8

    .line 441
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :cond_8
    :goto_3
    move-object v0, v1

    .end local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    move-object v5, v6

    .line 445
    .end local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 443
    .end local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .end local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 444
    .restart local v3    # "e":Ljava/lang/Exception;
    const-string v10, "VvmFileUtils"

    const-string v11, "VvmFileUtils.copyFile() - error releasing resources."

    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 431
    .end local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    :catch_2
    move-exception v3

    .line 432
    .restart local v3    # "e":Ljava/lang/Exception;
    :goto_4
    :try_start_6
    const-string v10, "VvmFileUtils"

    const-string v11, "VvmFileUtils.copyFile() - could not perform file copy."

    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 437
    if-eqz v5, :cond_9

    .line 438
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 440
    :cond_9
    if-eqz v0, :cond_4

    .line 441
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    .line 443
    :catch_3
    move-exception v3

    .line 444
    const-string v10, "VvmFileUtils"

    const-string v11, "VvmFileUtils.copyFile() - error releasing resources."

    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 435
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    .line 437
    :goto_5
    if-eqz v5, :cond_a

    .line 438
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 440
    :cond_a
    if-eqz v0, :cond_b

    .line 441
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    .line 445
    :cond_b
    :goto_6
    throw v9

    .line 443
    :catch_4
    move-exception v3

    .line 444
    .restart local v3    # "e":Ljava/lang/Exception;
    const-string v10, "VvmFileUtils"

    const-string v11, "VvmFileUtils.copyFile() - error releasing resources."

    invoke-static {v10, v11, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 435
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v9

    move-object v5, v6

    .end local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    goto :goto_5

    .end local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .end local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "currentNumberOfReadBytes":I
    .restart local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v9

    move-object v0, v1

    .end local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    move-object v5, v6

    .end local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    goto :goto_5

    .line 431
    .end local v2    # "currentNumberOfReadBytes":I
    .end local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v3

    move-object v5, v6

    .end local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .end local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "currentNumberOfReadBytes":I
    .restart local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v3

    move-object v0, v1

    .end local v1    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "copiedFileOutputStream":Ljava/io/FileOutputStream;
    move-object v5, v6

    .end local v6    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    .restart local v5    # "fileToCopyInputStream":Ljava/io/FileInputStream;
    goto :goto_4
.end method

.method public static copyFileAsReadable(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sourceFileName"    # Ljava/lang/String;
    .param p2, "targetFileName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 293
    const/4 v3, 0x0

    .line 294
    .local v3, "sourceFileInputStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 296
    .local v4, "targetFileOutputStream":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    .line 297
    const/4 v7, 0x1

    invoke-virtual {p0, p2, v7}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 299
    const/16 v7, 0x400

    new-array v0, v7, [B

    .line 301
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .local v2, "len":I
    if-lez v2, :cond_2

    .line 302
    const/4 v7, 0x0

    invoke-virtual {v4, v0, v7, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 307
    .end local v0    # "buf":[B
    .end local v2    # "len":I
    :catch_0
    move-exception v1

    .line 308
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "VvmFileUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312
    if-eqz v4, :cond_0

    .line 313
    :try_start_2
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 315
    :cond_0
    if-eqz v3, :cond_1

    .line 316
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 324
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return v5

    .line 304
    .restart local v0    # "buf":[B
    .restart local v2    # "len":I
    :cond_2
    :try_start_3
    const-string v7, "VvmFileUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "copyFileAsReadable() file "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "copied successfully"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 312
    if-eqz v4, :cond_3

    .line 313
    :try_start_4
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 315
    :cond_3
    if-eqz v3, :cond_4

    .line 316
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    move v5, v6

    .line 324
    goto :goto_1

    .line 318
    :catch_1
    move-exception v1

    .line 319
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v6, "VvmFileUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 318
    .end local v0    # "buf":[B
    .end local v2    # "len":I
    :catch_2
    move-exception v1

    .line 319
    const-string v6, "VvmFileUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 311
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 312
    if-eqz v4, :cond_5

    .line 313
    :try_start_5
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 315
    :cond_5
    if-eqz v3, :cond_6

    .line 316
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 320
    :cond_6
    throw v6

    .line 318
    :catch_3
    move-exception v1

    .line 319
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v6, "VvmFileUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static copyFileToDeviceExternalStorage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileToCopyName"    # Ljava/lang/String;
    .param p2, "copiedFileName"    # Ljava/lang/String;
    .param p3, "fileType"    # Ljava/lang/String;
    .param p4, "shareFile"    # Z

    .prologue
    const/4 v5, 0x0

    .line 189
    :try_start_0
    const-string v6, "mounted"

    .line 190
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v7

    .line 189
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 223
    :cond_0
    :goto_0
    return v5

    .line 195
    :cond_1
    invoke-static {p3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 197
    .local v1, "copiedFileExternalStorageFolder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v4

    .line 198
    .local v4, "wasMade":Z
    const-string v6, "VvmFileUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "copyFileToDeviceExternalStorage() - copiedFileExternalStorageFolder.mkdirs() returned "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 204
    .local v0, "copiedFile":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-direct {v6, v7, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-static {v6, v0, v7}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->copyFile(Ljava/io/File;Ljava/io/File;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 207
    .local v3, "wasCopied":Z
    if-eqz v3, :cond_0

    .line 220
    const/4 v5, 0x1

    goto :goto_0

    .line 221
    .end local v0    # "copiedFile":Ljava/io/File;
    .end local v1    # "copiedFileExternalStorageFolder":Ljava/io/File;
    .end local v3    # "wasCopied":Z
    .end local v4    # "wasMade":Z
    :catch_0
    move-exception v2

    .line 222
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "VvmFileUtils"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static copyFileToMediaStore(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileToCopyName"    # Ljava/lang/String;
    .param p2, "copiedFileName"    # Ljava/lang/String;
    .param p3, "fileType"    # Ljava/lang/String;
    .param p4, "shareFile"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 234
    :try_start_0
    sget-object v7, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 236
    .local v2, "fileUri":Landroid/net/Uri;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 239
    .local v0, "copiedFile":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-direct {v7, v8, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-static {v7, v0, v8}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->copyFile(Ljava/io/File;Ljava/io/File;I)Z

    move-result v4

    .line 242
    .local v4, "wasCopied":Z
    if-nez v4, :cond_0

    .line 254
    .end local v0    # "copiedFile":Ljava/io/File;
    .end local v2    # "fileUri":Landroid/net/Uri;
    .end local v4    # "wasCopied":Z
    :goto_0
    return v5

    .line 246
    .restart local v0    # "copiedFile":Ljava/io/File;
    .restart local v2    # "fileUri":Landroid/net/Uri;
    .restart local v4    # "wasCopied":Z
    :cond_0
    new-instance v3, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;

    const/4 v7, 0x0

    invoke-direct {v3, p0, p4, v7}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;-><init>(Landroid/content/Context;ZLandroid/net/Uri;)V

    .line 247
    .local v3, "scanCompletedListener":Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    .line 248
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x0

    .line 247
    invoke-static {p0, v7, v8, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v5, v6

    .line 251
    goto :goto_0

    .line 252
    .end local v0    # "copiedFile":Ljava/io/File;
    .end local v2    # "fileUri":Landroid/net/Uri;
    .end local v3    # "scanCompletedListener":Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$OnVVMScanCompletedListener;
    .end local v4    # "wasCopied":Z
    :catch_0
    move-exception v1

    .line 253
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "VvmFileUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static deleteAllFilesFromFolder(Ljava/lang/String;)I
    .locals 7
    .param p0, "folderPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 593
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 594
    .local v2, "folder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    move v3, v4

    .line 604
    :cond_1
    return v3

    .line 597
    :cond_2
    const/4 v3, 0x0

    .line 598
    .local v3, "i":I
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 599
    .local v1, "files":[Ljava/io/File;
    array-length v5, v1

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v1, v4

    .line 600
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 601
    add-int/lit8 v3, v3, 0x1

    .line 599
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static deleteExternalFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 548
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 549
    :cond_0
    const/4 v0, 0x0

    .line 552
    :goto_0
    return v0

    :cond_1
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    goto :goto_0
.end method

.method public static deleteExternalFiles(Landroid/content/Context;[Ljava/lang/String;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileNames"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 567
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    :cond_0
    move v1, v3

    .line 581
    :cond_1
    return v1

    .line 572
    :cond_2
    const/4 v1, 0x0

    .line 575
    .local v1, "numberOfDeletedFiles":I
    array-length v5, p1

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, p1, v4

    .line 576
    .local v0, "fileName":Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteExternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    add-int/2addr v1, v2

    .line 575
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    :cond_3
    move v2, v3

    .line 576
    goto :goto_1
.end method

.method public static deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 500
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 501
    :cond_0
    const/4 v0, 0x0

    .line 504
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static deleteInternalFiles(Landroid/content/Context;[Ljava/lang/String;)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileNames"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 519
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    :cond_0
    move v1, v3

    .line 533
    :cond_1
    return v1

    .line 524
    :cond_2
    const/4 v1, 0x0

    .line 527
    .local v1, "numberOfDeletedFiles":I
    array-length v5, p1

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, p1, v4

    .line 528
    .local v0, "fileName":Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    add-int/2addr v1, v2

    .line 527
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    :cond_3
    move v2, v3

    .line 528
    goto :goto_1
.end method

.method public static getExternalFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 485
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getFileBytes(Ljava/lang/String;)[B
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 336
    const-string v5, "VvmFileUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "VvmFileUtils.fileBuffter() - filePath: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    const/4 v2, 0x0

    .line 338
    .local v2, "fileContent":[B
    const/4 v3, 0x0

    .line 339
    .local v3, "fin":Ljava/io/FileInputStream;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 341
    .local v1, "file":Ljava/io/File;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .local v4, "fin":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v5, v6

    new-array v2, v5, [B

    .line 343
    invoke-virtual {v4, v2}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 354
    if-eqz v4, :cond_0

    .line 355
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v3, v4

    .line 362
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    :cond_1
    :goto_0
    return-object v2

    .line 358
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "VvmFileUtils"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v3, v4

    .line 361
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    goto :goto_0

    .line 345
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 346
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string v5, "VvmFileUtils"

    const-string v6, "VvmFileUtils.fileBuffter() - file not found ."

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 354
    if-eqz v3, :cond_1

    .line 355
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 358
    :catch_2
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "VvmFileUtils"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 348
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 349
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    const-string v5, "VvmFileUtils"

    const-string v6, "VvmFileUtils.fileBuffter() Exception while reading the file "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 354
    if-eqz v3, :cond_1

    .line 355
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 358
    :catch_4
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "VvmFileUtils"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 352
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 354
    :goto_3
    if-eqz v3, :cond_2

    .line 355
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 360
    :cond_2
    :goto_4
    throw v5

    .line 358
    :catch_5
    move-exception v0

    .line 359
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v6, "VvmFileUtils"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 352
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    goto :goto_3

    .line 348
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_6
    move-exception v0

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    goto :goto_2

    .line 345
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_7
    move-exception v0

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static getFileCount(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0, "folder"    # Ljava/lang/String;
    .param p1, "fileType"    # Ljava/lang/String;

    .prologue
    .line 101
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 104
    :cond_0
    const/4 v2, 0x0

    .line 115
    :goto_0
    return v2

    .line 107
    :cond_1
    new-instance v1, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$1;

    invoke-direct {v1, p1}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils$1;-><init>(Ljava/lang/String;)V

    .line 115
    .local v1, "filter":Ljava/io/FilenameFilter;
    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    array-length v2, v2

    goto :goto_0
.end method

.method public static getInternalFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 471
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static isExists(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 88
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public static isExternalStorageExist()Z
    .locals 2

    .prologue
    .line 456
    const-string v0, "mounted"

    .line 457
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 456
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static loadSerializable(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 654
    invoke-virtual {p0, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 655
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    move-object v3, v6

    .line 674
    :goto_0
    return-object v3

    .line 659
    :cond_0
    const/4 v2, 0x0

    .line 660
    .local v2, "fis":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 661
    .local v4, "ois":Ljava/io/ObjectInputStream;
    const/4 v3, 0x0

    .line 663
    .local v3, "object":Ljava/lang/Object;
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    .line 664
    new-instance v5, Ljava/io/ObjectInputStream;

    invoke-direct {v5, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 665
    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .local v5, "ois":Ljava/io/ObjectInputStream;
    :try_start_1
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    .line 666
    invoke-virtual {v5}, Ljava/io/ObjectInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 667
    .end local v3    # "object":Ljava/lang/Object;
    :catch_0
    move-exception v0

    move-object v4, v5

    .line 668
    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    .local v0, "e":Ljava/io/IOException;
    .restart local v4    # "ois":Ljava/io/ObjectInputStream;
    :goto_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v6

    .line 669
    goto :goto_0

    .line 670
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v3    # "object":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 671
    .end local v3    # "object":Ljava/lang/Object;
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    :goto_2
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move-object v3, v6

    .line 672
    goto :goto_0

    .line 670
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    .end local v4    # "ois":Ljava/io/ObjectInputStream;
    .restart local v5    # "ois":Ljava/io/ObjectInputStream;
    :catch_2
    move-exception v0

    move-object v4, v5

    .end local v5    # "ois":Ljava/io/ObjectInputStream;
    .restart local v4    # "ois":Ljava/io/ObjectInputStream;
    goto :goto_2

    .line 667
    .restart local v3    # "object":Ljava/lang/Object;
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method public static renameFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "oldFileName"    # Ljava/lang/String;
    .param p1, "newFileName"    # Ljava/lang/String;

    .prologue
    .line 611
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 612
    .local v0, "file":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 613
    .local v1, "newFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 614
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    .line 616
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static saveChunk(Landroid/content/Context;Ljava/lang/String;[BII)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "data"    # [B
    .param p3, "chunkNum"    # I
    .param p4, "bytesToSkip"    # I

    .prologue
    const/4 v4, 0x0

    .line 131
    const/4 v3, 0x0

    .line 134
    .local v3, "fos":Ljava/io/FileOutputStream;
    if-eqz p0, :cond_2

    .line 135
    const v5, 0x8000

    :try_start_0
    invoke-virtual {p0, p1, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v3

    .line 136
    const/4 v5, 0x0

    invoke-static {p2, v5}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v0

    .line 137
    .local v0, "decodedData":[B
    if-eqz v3, :cond_1

    .line 138
    array-length v5, v0

    sub-int/2addr v5, p4

    invoke-virtual {v3, v0, p4, v5}, Ljava/io/FileOutputStream;->write([BII)V

    .line 139
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    const/4 v4, 0x1

    .line 157
    if-eqz v3, :cond_0

    .line 158
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 162
    .end local v0    # "decodedData":[B
    :cond_0
    :goto_0
    return v4

    .line 160
    .restart local v0    # "decodedData":[B
    :catch_0
    move-exception v2

    .line 161
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 157
    .end local v2    # "e1":Ljava/io/IOException;
    :cond_1
    if-eqz v3, :cond_0

    .line 158
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 160
    :catch_1
    move-exception v2

    .line 161
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 157
    .end local v0    # "decodedData":[B
    .end local v2    # "e1":Ljava/io/IOException;
    :cond_2
    if-eqz v3, :cond_0

    .line 158
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 160
    :catch_2
    move-exception v2

    .line 161
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 147
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 148
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v5, "VvmFileUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 149
    const-string v5, "VvmFileUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed chunk is:\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, p2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-static {p0, p1}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 152
    const-string v5, "VvmFileUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "VvmFileUtils.saveChunk() file deleted "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 157
    if-eqz v3, :cond_0

    .line 158
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    .line 160
    :catch_4
    move-exception v2

    .line 161
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 156
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 157
    if-eqz v3, :cond_3

    .line 158
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 162
    :cond_3
    :goto_1
    throw v4

    .line 160
    :catch_5
    move-exception v2

    .line 161
    .restart local v2    # "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static saveSerializable(Landroid/content/Context;Ljava/lang/Object;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 631
    const/4 v1, 0x0

    .line 632
    .local v1, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 634
    .local v2, "oos":Ljava/io/ObjectOutputStream;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, p2, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 635
    new-instance v3, Ljava/io/ObjectOutputStream;

    invoke-direct {v3, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 636
    .end local v2    # "oos":Ljava/io/ObjectOutputStream;
    .local v3, "oos":Ljava/io/ObjectOutputStream;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 637
    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 642
    const/4 v4, 0x1

    move-object v2, v3

    .end local v3    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v2    # "oos":Ljava/io/ObjectOutputStream;
    :goto_0
    return v4

    .line 638
    :catch_0
    move-exception v0

    .line 639
    .local v0, "e":Ljava/io/IOException;
    :goto_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 638
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v3    # "oos":Ljava/io/ObjectOutputStream;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "oos":Ljava/io/ObjectOutputStream;
    .restart local v2    # "oos":Ljava/io/ObjectOutputStream;
    goto :goto_1
.end method

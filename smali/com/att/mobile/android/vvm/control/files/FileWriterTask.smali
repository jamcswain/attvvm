.class public Lcom/att/mobile/android/vvm/control/files/FileWriterTask;
.super Ljava/lang/Object;
.source "FileWriterTask.java"


# instance fields
.field private chunkNumber:I

.field private data:[B

.field private fileName:Ljava/lang/String;

.field private messageDo:Lcom/att/mobile/android/vvm/model/db/MessageDo;

.field private messageFileIndex:I

.field private messageFilesSize:I

.field private skipHeader:Z


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/model/db/MessageDo;ILjava/lang/String;I[BI)V
    .locals 2
    .param p1, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .param p2, "messageFileIndex"    # I
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "messageFilesSize"    # I
    .param p5, "data"    # [B
    .param p6, "chunkNumber"    # I

    .prologue
    const/4 v1, 0x1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->skipHeader:Z

    .line 48
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageDo:Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .line 49
    iput p2, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageFileIndex:I

    .line 50
    iput-object p3, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->fileName:Ljava/lang/String;

    .line 51
    iput p4, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageFilesSize:I

    .line 52
    iput p6, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->chunkNumber:I

    .line 53
    array-length v0, p5

    invoke-static {p5, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->data:[B

    .line 56
    if-eq p2, v1, :cond_0

    if-ne p6, v1, :cond_0

    .line 58
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->skipHeader:Z

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public getChunkNumber()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->chunkNumber:I

    return v0
.end method

.method public getData()[B
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->data:[B

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->data:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageDo()Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageDo:Lcom/att/mobile/android/vvm/model/db/MessageDo;

    return-object v0
.end method

.method public getMessageFileIndex()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageFileIndex:I

    return v0
.end method

.method public getMessageFilesSize()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageFilesSize:I

    return v0
.end method

.method public isSkipHeader()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->skipHeader:Z

    return v0
.end method

.method public setChunkNumber(I)V
    .locals 0
    .param p1, "chunkNumber"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->chunkNumber:I

    .line 100
    return-void
.end method

.method public setData([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 107
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->data:[B

    .line 108
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->fileName:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setMessageDo(Lcom/att/mobile/android/vvm/model/db/MessageDo;)V
    .locals 0
    .param p1, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageDo:Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .line 68
    return-void
.end method

.method public setMessageFileIndex(I)V
    .locals 0
    .param p1, "messageFileIndex"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageFileIndex:I

    .line 76
    return-void
.end method

.method public setMessageFilesSize(I)V
    .locals 0
    .param p1, "messageFilesSize"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->messageFilesSize:I

    .line 92
    return-void
.end method

.method public setSkipHeader(Z)V
    .locals 0
    .param p1, "skipHeader"    # Z

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;->skipHeader:Z

    .line 116
    return-void
.end method

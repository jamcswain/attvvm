.class public Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;
.super Landroid/os/HandlerThread;
.source "FileWriterQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;
    }
.end annotation


# static fields
.field private static final AMR_HEADER_LENGTH:I = 0x6

.field private static final CLOSE_FILE:I = 0x2

.field private static final SAVE_CHUNK:I = 0x1

.field private static final TAG:Ljava/lang/String; = "FileWriterQueue"

.field private static instance:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

.field private static lock:Ljava/lang/Object;


# instance fields
.field private bytesStored:I

.field private context:Landroid/content/Context;

.field private handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->instance:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->lock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const-class v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->bytesStored:I

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->context:Landroid/content/Context;

    .line 59
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->context:Landroid/content/Context;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    .prologue
    .line 13
    iget v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->bytesStored:I

    return v0
.end method

.method static synthetic access$002(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;I)I
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;
    .param p1, "x1"    # I

    .prologue
    .line 13
    iput p1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->bytesStored:I

    return p1
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->context:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    sget-object v1, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 43
    :try_start_0
    sget-object v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->instance:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->instance:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    .line 47
    sget-object v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->instance:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->start()V

    .line 49
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    sget-object v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->instance:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    return-object v0

    .line 49
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public declared-synchronized enqueueFileCloserTask(Lcom/att/mobile/android/vvm/control/files/FileCloserTask;)V
    .locals 3
    .param p1, "fileCloserTask"    # Lcom/att/mobile/android/vvm/control/files/FileCloserTask;

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;-><init>(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    monitor-exit p0

    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized enqueueFileWriterTask(Lcom/att/mobile/android/vvm/control/files/FileWriterTask;)V
    .locals 3
    .param p1, "fileWriterTask"    # Lcom/att/mobile/android/vvm/control/files/FileWriterTask;

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;-><init>(Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->handler:Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue$FileWriterHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

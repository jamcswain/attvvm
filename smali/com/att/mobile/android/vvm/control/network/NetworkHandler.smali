.class public Lcom/att/mobile/android/vvm/control/network/NetworkHandler;
.super Ljava/lang/Object;
.source "NetworkHandler.java"


# static fields
.field private static final CHUNK_SIZE:I = 0x4b000

.field private static final TAG:Ljava/lang/String; = "NetworkHandler"

.field private static fileReadBuffer:[B


# instance fields
.field private connected:Z

.field private input:Ljava/io/InputStream;

.field private output:Ljava/io/OutputStream;

.field private readBuffer:[B

.field private secureSocket:Ljavax/net/ssl/SSLSocket;

.field private socket:Ljava/net/Socket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const v0, 0x4b000

    new-array v0, v0, [B

    sput-object v0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->fileReadBuffer:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z

    .line 36
    iput-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->input:Ljava/io/InputStream;

    .line 37
    iput-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    .line 38
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->readBuffer:[B

    .line 46
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 153
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    if-eqz v1, :cond_0

    .line 155
    :try_start_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->shutdownInput()V

    .line 156
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->input:Ljava/io/InputStream;

    .line 157
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->shutdownOutput()V

    .line 158
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    .line 160
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :goto_0
    iput-object v3, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    .line 168
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    if-eqz v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->close()V

    .line 170
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    .line 172
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    if-eqz v1, :cond_2

    .line 173
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 174
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 179
    :cond_2
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z

    .line 180
    return-void

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "NetworkHandler"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 176
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 177
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "NetworkHandler"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public connect(Ljava/lang/String;II)Z
    .locals 5
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "timeout"    # I

    .prologue
    const/4 v1, 0x1

    .line 57
    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z

    if-eqz v2, :cond_0

    .line 89
    :goto_0
    return v1

    .line 63
    :cond_0
    :try_start_0
    const-string v2, "NetworkHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NetworkHandler.connect() connecting to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", timeout = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-instance v2, Ljava/net/Socket;

    invoke-direct {v2}, Ljava/net/Socket;-><init>()V

    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    .line 70
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V

    .line 74
    if-gtz p3, :cond_1

    .line 75
    const/16 p3, 0x3c

    .line 78
    :cond_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    new-instance v3, Ljava/net/InetSocketAddress;

    invoke-direct {v3, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    mul-int/lit16 v4, p3, 0x3e8

    invoke-virtual {v2, v3, v4}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 81
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->input:Ljava/io/InputStream;

    .line 82
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    .line 83
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "NetworkHandler"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 87
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 89
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public connectSSL(Landroid/content/Context;Ljava/lang/String;II)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "port"    # I
    .param p4, "timeout"    # I

    .prologue
    const/4 v3, 0x1

    .line 100
    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z

    if-eqz v2, :cond_0

    move v2, v3

    .line 145
    :goto_0
    return v2

    .line 105
    :cond_0
    :try_start_0
    const-string v2, "NetworkHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NetworkHandler.connectSSL() connecting to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", timeout = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " seconds"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const-string v2, "NetworkHandler"

    const-string v4, "NetworkHandler.connectSSL() going to create secure socket with STRICT_HOSTNAME_VERIFIER"

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const-string v2, "TLSv1.2"

    invoke-static {v2}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v1

    .line 116
    .local v1, "sslContext":Ljavax/net/ssl/SSLContext;
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 117
    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v2

    check-cast v2, Ljavax/net/ssl/SSLSocket;

    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    .line 119
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    if-eqz v2, :cond_2

    .line 120
    const-string v2, "NetworkHandler"

    const-string v4, "NetworkHandler.connectSSL() socket created"

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljavax/net/ssl/SSLSocket;->bind(Ljava/net/SocketAddress;)V

    .line 127
    if-gtz p4, :cond_1

    .line 128
    const/16 p4, 0x3c

    .line 130
    :cond_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    new-instance v4, Ljava/net/InetSocketAddress;

    invoke-direct {v4, p2, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    mul-int/lit16 v5, p4, 0x3e8

    invoke-virtual {v2, v4, v5}, Ljavax/net/ssl/SSLSocket;->connect(Ljava/net/SocketAddress;I)V

    .line 132
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->input:Ljava/io/InputStream;

    .line 133
    iget-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    .line 134
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z

    move v2, v3

    .line 135
    goto/16 :goto_0

    .line 137
    :cond_2
    const-string v2, "NetworkHandler"

    const-string v3, "NetworkHandler.connectSSL() failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    .end local v1    # "sslContext":Ljavax/net/ssl/SSLContext;
    :goto_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "NetworkHandler"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 143
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    goto :goto_1
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z

    return v0
.end method

.method public receiveNextChunk(I)[B
    .locals 9
    .param p1, "fileSizeToRead"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 336
    const/4 v0, 0x0

    .local v0, "actual":I
    const/4 v3, 0x0

    .line 337
    .local v3, "totalRead":I
    sget-object v5, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->fileReadBuffer:[B

    array-length v5, v5

    invoke-static {v5, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 339
    .local v2, "maxBytesToRead":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 341
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->input:Ljava/io/InputStream;

    sget-object v6, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->fileReadBuffer:[B

    sub-int v7, v2, v3

    invoke-virtual {v5, v6, v3, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 344
    const/4 v5, -0x1

    if-ne v0, v5, :cond_0

    .line 346
    :try_start_0
    iget-object v5, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->input:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :goto_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 351
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Connection closed"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 347
    :catch_0
    move-exception v1

    .line 348
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "NetworkHandler"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 353
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    add-int/2addr v3, v0

    goto :goto_0

    .line 358
    :cond_1
    const/4 v4, 0x0

    .line 360
    .local v4, "wholeData":[B
    if-lez v3, :cond_2

    .line 361
    new-array v4, v3, [B

    .line 362
    sget-object v5, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->fileReadBuffer:[B

    invoke-static {v5, v8, v4, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 364
    :cond_2
    return-object v4
.end method

.method public receiveNextData()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 230
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData(Z)[B

    move-result-object v0

    return-object v0
.end method

.method public receiveNextData(Z)[B
    .locals 13
    .param p1, "plusLF"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 237
    const-wide/32 v8, 0xea60

    .line 238
    .local v8, "waitTime":J
    const/4 v5, 0x0

    .line 241
    .local v5, "stop":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 243
    .local v6, "start":J
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 249
    .local v2, "baos":Ljava/io/ByteArrayOutputStream;
    :goto_0
    if-nez v5, :cond_5

    .line 250
    const/4 v3, 0x0

    .line 257
    .local v3, "count":I
    :cond_0
    iget-object v10, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->input:Ljava/io/InputStream;

    iget-object v11, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->readBuffer:[B

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v3, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 263
    .local v0, "actual":I
    const/4 v10, -0x1

    if-ne v0, v10, :cond_1

    .line 265
    :try_start_0
    iget-object v10, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->input:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :goto_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 272
    new-instance v10, Ljava/io/IOException;

    const-string v11, "Connection closed"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 266
    :catch_0
    move-exception v4

    .line 267
    .local v4, "e":Ljava/io/IOException;
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IMAP4Protocol.receiveNextData() Exp :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 269
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 268
    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 279
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    if-nez v0, :cond_2

    .line 280
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 281
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v6

    cmp-long v10, v10, v8

    if-lez v10, :cond_0

    .line 282
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 283
    new-instance v10, Ljava/io/IOException;

    const-string v11, "TimeOut"

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 291
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 292
    iget-object v10, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->readBuffer:[B

    aget-byte v1, v10, v3

    .line 297
    .local v1, "b":B
    const/16 v10, 0xd

    if-eq v1, v10, :cond_0

    .line 303
    const/16 v10, 0xa

    if-ne v1, v10, :cond_4

    .line 305
    if-eqz p1, :cond_3

    .line 306
    add-int/lit8 v3, v3, 0x1

    .line 308
    :cond_3
    const/4 v5, 0x1

    .line 322
    :goto_2
    iget-object v10, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->readBuffer:[B

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 315
    :cond_4
    add-int/lit8 v3, v3, 0x1

    .line 316
    iget-object v10, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->readBuffer:[B

    array-length v10, v10

    if-ne v3, v10, :cond_0

    goto :goto_2

    .line 324
    .end local v0    # "actual":I
    .end local v1    # "b":B
    .end local v3    # "count":I
    :cond_5
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    return-object v10
.end method

.method public send([B)V
    .locals 2
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 194
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 195
    return-void

    .line 199
    :cond_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 200
    new-instance v0, Ljava/io/IOException;

    const-string v1, "connection closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public send([BII)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connected:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->socket:Ljava/net/Socket;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->secureSocket:Ljavax/net/ssl/SSLSocket;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 217
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->output:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 218
    return-void

    .line 222
    :cond_1
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 223
    new-instance v0, Ljava/io/IOException;

    const-string v1, "connection closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

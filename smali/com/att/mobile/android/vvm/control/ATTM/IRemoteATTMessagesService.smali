.class public interface abstract Lcom/att/mobile/android/vvm/control/ATTM/IRemoteATTMessagesService;
.super Ljava/lang/Object;
.source "IRemoteATTMessagesService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/ATTM/IRemoteATTMessagesService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getFileFromMessage(Ljava/lang/String;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getVVMConnectivityCredentials()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isLegacyVVMUibCompatible()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

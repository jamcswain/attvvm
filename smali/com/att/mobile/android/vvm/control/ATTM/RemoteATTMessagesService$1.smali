.class Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService$1;
.super Lcom/att/mobile/android/vvm/control/ATTM/IRemoteATTMessagesService$Stub;
.source "RemoteATTMessagesService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService$1;->this$0:Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;

    invoke-direct {p0}, Lcom/att/mobile/android/vvm/control/ATTM/IRemoteATTMessagesService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getFileFromMessage(Ljava/lang/String;)[B
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService$1;->this$0:Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->getFileBuffer(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getVVMConnectivityCredentials()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService$1;->this$0:Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->getSetupDetails()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isLegacyVVMUibCompatible()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService$1;->this$0:Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->isATTMEnabled()Z

    move-result v0

    return v0
.end method

.class public Lcom/att/mobile/android/vvm/control/ATTM/SavedMessagesContentProvider;
.super Landroid/content/ContentProvider;
.source "SavedMessagesContentProvider.java"


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final FILE_NAME:Ljava/lang/String; = "filename"

.field private static final MESSAGE_ID:Ljava/lang/String; = "messageid"

.field public static final PROVIDER_NAME:Ljava/lang/String; = "com.att.vvm.savedmessages"

.field private static final TAG:Ljava/lang/String; = "SavedMessagesContentProvider"


# instance fields
.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "content://com.att.vvm.savedmessages"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/control/ATTM/SavedMessagesContentProvider;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 145
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Read-only access is allowed"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "_uri"    # Landroid/net/Uri;

    .prologue
    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p1, "_uri"    # Landroid/net/Uri;
    .param p2, "_initialValues"    # Landroid/content/ContentValues;

    .prologue
    .line 136
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Read-only access is allowed"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 84
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createInstance(Landroid/content/Context;)V

    .line 85
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/ATTM/SavedMessagesContentProvider;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 87
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 44
    const-string v9, "SavedMessagesContentProvider.openFile"

    const-string v10, "Retrieve file according to message ID or file name"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 47
    .local v8, "segment":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x1

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 49
    .local v4, "id":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 50
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 51
    .local v2, "filePath":Ljava/lang/String;
    const-string v1, ""

    .line 52
    .local v1, "fileName":Ljava/lang/String;
    const-string v9, "messageid"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 54
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 55
    .local v6, "messageId":J
    const-string v9, "SavedMessagesContentProvider.openFile"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Retrieve file according to message ID "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v9, p0, Lcom/att/mobile/android/vvm/control/ATTM/SavedMessagesContentProvider;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v9, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessage(J)Landroid/database/Cursor;

    move-result-object v0

    .line 58
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 59
    const-string v9, "file_name"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 69
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v6    # "messageId":J
    :cond_0
    :goto_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 70
    .local v3, "fullFilePath":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 71
    .local v5, "messageFile":Ljava/io/File;
    const/high16 v9, 0x10000000

    invoke-static {v5, v9}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v9

    return-object v9

    .line 63
    .end local v3    # "fullFilePath":Ljava/lang/String;
    .end local v5    # "messageFile":Ljava/io/File;
    :cond_1
    const-string v9, "filename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 65
    move-object v1, v4

    .line 66
    const-string v9, "SavedMessagesContentProvider.openFile"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Retrieve file according to file name "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sort"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v1, "SavedMessagesContentProvider"

    const-string v2, "performing a query against the Messages table for saved messages"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/ATTM/SavedMessagesContentProvider;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    if-nez v1, :cond_0

    .line 114
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    iput-object v1, p0, Lcom/att/mobile/android/vvm/control/ATTM/SavedMessagesContentProvider;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/control/ATTM/SavedMessagesContentProvider;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAllMessagesFromInbox()Landroid/database/Cursor;

    move-result-object v0

    .line 121
    .local v0, "queryResults":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/ATTM/SavedMessagesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 124
    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 154
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Read-only access is allowed"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

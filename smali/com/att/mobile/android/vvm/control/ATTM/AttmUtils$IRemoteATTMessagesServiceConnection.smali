.class Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils$IRemoteATTMessagesServiceConnection;
.super Ljava/lang/Object;
.source "AttmUtils.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IRemoteATTMessagesServiceConnection"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "boundService"    # Landroid/os/IBinder;

    .prologue
    .line 35
    invoke-static {p2}, Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->access$002(Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;)Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;

    .line 36
    const-string v0, "AttmUtils"

    const-string v1, "IRemoteATTMessagesServiceConnection onServiceConnected()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils$IRemoteATTMessagesServiceConnection$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils$IRemoteATTMessagesServiceConnection$1;-><init>(Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils$IRemoteATTMessagesServiceConnection;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 46
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 48
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->access$002(Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;)Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;

    .line 53
    const-string v0, "AttmUtils"

    const-string v1, "IRemoteVvmService onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;
.super Ljava/lang/Object;
.source "AttmUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils$IRemoteATTMessagesServiceConnection;
    }
.end annotation


# static fields
.field public static final ATTM_PCKG_NAME:Ljava/lang/String; = "com.att.android.mobile.attmessages"

.field private static final TAG:Ljava/lang/String; = "AttmUtils"

.field public static final VVM_PCKG_NAME:Ljava/lang/String; = "com.att.mobile.android.vvm"

.field private static conn:Landroid/content/ServiceConnection;

.field private static remoteVvmService:Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;)Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;

    .prologue
    .line 22
    sput-object p0, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->remoteVvmService:Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;

    return-object p0
.end method

.method public static handlePackageInstall()V
    .locals 2

    .prologue
    .line 183
    const-string v0, "AttmUtils"

    const-string v1, "handlePackageUninstall() changing attm status to INSTALLED_NOT_PROVISIONED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setAttmStatus(I)V

    .line 186
    invoke-static {}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->initAttmService()V

    .line 187
    return-void
.end method

.method public static handlePackageUninstall()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    const-string v0, "AttmUtils"

    const-string v1, "handlePackageUninstall() changing attm status to NOT_INSTALLED"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setAttmStatus(I)V

    .line 176
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v1, "showLaunchAttmScreenOnlyOnce"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 177
    return-void
.end method

.method public static declared-synchronized initAttmService()V
    .locals 7

    .prologue
    .line 60
    const-class v4, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->remoteVvmService:Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;

    if-nez v3, :cond_0

    .line 61
    new-instance v3, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils$IRemoteATTMessagesServiceConnection;

    invoke-direct {v3}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils$IRemoteATTMessagesServiceConnection;-><init>()V

    sput-object v3, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->conn:Landroid/content/ServiceConnection;

    .line 62
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 63
    .local v1, "explicitIntent":Landroid/content/Intent;
    new-instance v3, Landroid/content/ComponentName;

    const-string v5, "com.att.android.mobile.attmessages"

    const-string v6, "com.att.mobile.android.vvm.control.ATTM.RemoteVvmService"

    invoke-direct {v3, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 64
    const-string v3, "AttmUtils"

    const-string v5, "bindService explicitIntent"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v5, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->conn:Landroid/content/ServiceConnection;

    const/4 v6, 0x1

    invoke-virtual {v3, v1, v5, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    .line 66
    .local v2, "isServiceBound":Z
    const-string v3, "AttmUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bindService isServiceBound="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    .end local v2    # "isServiceBound":Z
    :cond_0
    :goto_0
    monitor-exit v4

    return-void

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/SecurityException;
    :try_start_1
    const-string v3, "AttmUtils"

    const-string v5, "initAttmService() bind to ATTM service failed"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 60
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method private static isAttmInstalled()Ljava/lang/Boolean;
    .locals 5

    .prologue
    .line 128
    const/4 v2, 0x0

    .line 129
    .local v2, "result":Z
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 132
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.att.android.mobile.attmessages"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 133
    const-string v3, "AttmUtils"

    const-string v4, "isAttmInstalled() AT&T Messages is installed"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    const/4 v2, 0x1

    .line 139
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    return-object v3

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "AttmUtils"

    const-string v4, "isAttmInstalled() AT&T Messages is NOT installed"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isUibReadyToReplaceLegacyVvm()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 85
    const-string v2, "AttmUtils"

    const-string v4, "isUibReadyToReplaceLegacyVvm() going to check with AT&T Messages"

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 88
    .local v1, "isUibReadyToReplaceLegacyVvm":Ljava/lang/Boolean;
    invoke-static {}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->isAttmInstalled()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    sget-object v2, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->remoteVvmService:Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;

    if-eqz v2, :cond_1

    .line 91
    :try_start_0
    sget-object v2, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->remoteVvmService:Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;

    invoke-interface {v2}, Lcom/att/mobile/android/vvm/control/ATTM/IRemoteVvmService;->isUibReadyToReplaceLegacyVvm()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 93
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v4

    .line 94
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    .line 93
    :goto_0
    invoke-virtual {v4, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setAttmStatus(I)V

    .line 96
    const-string v2, "AttmUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IsUibReadyToReplaceLegacyVvm() remoteVvmService.IsUibReadyToReplaceLegacyVvm returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_1
    const-string v2, "AttmUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IsUibReadyToReplaceLegacyVvm() result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    return v2

    :cond_0
    move v2, v3

    .line 94
    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "AttmUtils"

    const-string v4, "IsUibReadyToReplaceLegacyVvm() call remoteVvmService.IsUibReadyToReplaceLegacyVvm() failed"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 103
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setAttmStatus(I)V

    goto :goto_1

    .line 106
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const-string v2, "AttmUtils"

    const-string v4, "isUibReadyToReplaceLegacyVvm() ATTM is not connected, VVM will run as usual"

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-static {}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->initAttmService()V

    .line 108
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 109
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setAttmStatus(I)V

    goto :goto_1

    .line 112
    :cond_2
    const-string v2, "AttmUtils"

    const-string v3, "isUibReadyToReplaceLegacyVvm() ATTM is not installed, VVM will run as usual"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 114
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setAttmStatus(I)V

    goto :goto_1
.end method

.method public static launchATTMApplication()Z
    .locals 6

    .prologue
    .line 150
    const-string v4, "AttmUtils"

    const-string v5, "launchATTMApplication() going to start AT&T Messages"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const/4 v3, 0x0

    .line 155
    .local v3, "result":Z
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.att.mobile.android.vvm.INTENT_CLEAR_ALL_NOTIFICATIONS"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 156
    .local v2, "intentService":Landroid/content/Intent;
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 157
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 159
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.att.android.ACTION_LAUNCH_ATTM"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 160
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    const/4 v3, 0x1

    .line 168
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "intentService":Landroid/content/Intent;
    :goto_0
    return v3

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "AttmUtils"

    const-string v5, "launchATTMApplication() - error starting AT&T Messages"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 166
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static releaseAttmService()V
    .locals 2

    .prologue
    .line 75
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->conn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 76
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->conn:Landroid/content/ServiceConnection;

    .line 77
    return-void
.end method

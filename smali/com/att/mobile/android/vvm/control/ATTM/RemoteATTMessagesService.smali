.class public Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;
.super Landroid/app/Service;
.source "RemoteATTMessagesService.java"


# static fields
.field public static final INTENT_BIND_ATTM_SERVICE:Ljava/lang/String; = "com.att.action.BIND_ATTM_SERVICE"

.field private static final TAG:Ljava/lang/String; = "RemoteATTMessagesService"


# instance fields
.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-void
.end method


# virtual methods
.method getFileBuffer(Ljava/lang/String;)[B
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 72
    const-string v1, "RemoteATTMessagesService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFileBuffer fileName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    .line 74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "fullFilePath":Ljava/lang/String;
    invoke-static {v0}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->getFileBytes(Ljava/lang/String;)[B

    move-result-object v1

    return-object v1
.end method

.method getSetupDetails()Ljava/lang/String;
    .locals 8

    .prologue
    .line 88
    const-string v3, "RemoteATTMessagesService"

    const-string v4, "getSetupDetails"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const/4 v2, 0x0

    .line 92
    .local v2, "setupDetails":Ljava/lang/String;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 96
    .local v0, "details":Lorg/json/JSONObject;
    :try_start_0
    const-string v3, "Password"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getPassword()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 97
    const-string v3, "Host"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v5, "hostPref"

    const-class v6, Ljava/lang/String;

    const-string v7, ""

    .line 98
    invoke-virtual {v4, v5, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 97
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 99
    const-string v3, "Token"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v5, "tokenPref"

    const-class v6, Ljava/lang/String;

    const-string v7, ""

    .line 100
    invoke-virtual {v4, v5, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 99
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 101
    const-string v3, "MailBox"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v5, "userPref"

    const-class v6, Ljava/lang/String;

    const-string v7, ""

    .line 102
    invoke-virtual {v4, v5, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 101
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 103
    const-string v3, "Port"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v5, "portPref"

    const-class v6, Ljava/lang/String;

    const-string v7, ""

    .line 104
    invoke-virtual {v4, v5, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 103
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 105
    const-string v3, "SSLPort"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v5, "sslPortPref"

    const-class v6, Ljava/lang/String;

    const-string v7, ""

    .line 106
    invoke-virtual {v4, v5, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 105
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 108
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-le v3, v4, :cond_0

    .line 109
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".ldSignature"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Crypto"

    invoke-static {v3, v4, v5}, Lcom/att/mobile/android/infra/utils/Crypto;->encrypt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 119
    :goto_0
    return-object v2

    .line 111
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".ldSignature"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/att/mobile/android/infra/utils/Crypto;->encrypt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_0

    .line 113
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Lorg/json/JSONException;
    const-string v3, "RemoteATTMessagesService"

    const-string v4, "Failed To Create JSON Object"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 116
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v1

    .line 117
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "RemoteATTMessagesService"

    const-string v4, "Failed to get setup data"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method isATTMEnabled()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 50
    new-instance v0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService$1;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService$1;-><init>(Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;)V

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 36
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createInstance(Landroid/content/Context;)V

    .line 37
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/control/ATTM/RemoteATTMessagesService;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 38
    const-string v0, "RemoteATTMessagesService"

    const-string v1, "The AIDLMessageService was created."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "RemoteATTMessagesService"

    const-string v1, "The AIDLMessageService was destroyed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 45
    return-void
.end method

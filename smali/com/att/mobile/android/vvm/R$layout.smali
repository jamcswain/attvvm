.class public final Lcom/att/mobile/android/vvm/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f040000

.field public static final abc_action_bar_up_container:I = 0x7f040001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f040002

.field public static final abc_action_menu_item_layout:I = 0x7f040003

.field public static final abc_action_menu_layout:I = 0x7f040004

.field public static final abc_action_mode_bar:I = 0x7f040005

.field public static final abc_action_mode_close_item_material:I = 0x7f040006

.field public static final abc_activity_chooser_view:I = 0x7f040007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f040008

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f040009

.field public static final abc_alert_dialog_material:I = 0x7f04000a

.field public static final abc_dialog_title_material:I = 0x7f04000b

.field public static final abc_expanded_menu_layout:I = 0x7f04000c

.field public static final abc_list_menu_item_checkbox:I = 0x7f04000d

.field public static final abc_list_menu_item_icon:I = 0x7f04000e

.field public static final abc_list_menu_item_layout:I = 0x7f04000f

.field public static final abc_list_menu_item_radio:I = 0x7f040010

.field public static final abc_popup_menu_header_item_layout:I = 0x7f040011

.field public static final abc_popup_menu_item_layout:I = 0x7f040012

.field public static final abc_screen_content_include:I = 0x7f040013

.field public static final abc_screen_simple:I = 0x7f040014

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f040015

.field public static final abc_screen_toolbar:I = 0x7f040016

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f040017

.field public static final abc_search_view:I = 0x7f040018

.field public static final abc_select_dialog_material:I = 0x7f040019

.field public static final about_item:I = 0x7f04001a

.field public static final about_vvm:I = 0x7f04001b

.field public static final account_setup_error:I = 0x7f04001c

.field public static final account_setup_loading:I = 0x7f04001d

.field public static final account_setup_unknownbox:I = 0x7f04001e

.field public static final activity_setup_and_transfer:I = 0x7f04001f

.field public static final aggregated:I = 0x7f040020

.field public static final alert_dialog:I = 0x7f040021

.field public static final att_visual_voicemail_title:I = 0x7f040022

.field public static final attm_launcher:I = 0x7f040023

.field public static final audio_recorder:I = 0x7f040024

.field public static final audio_recorder_control_panel:I = 0x7f040025

.field public static final call_again:I = 0x7f040026

.field public static final call_voice_mail:I = 0x7f040027

.field public static final change_password:I = 0x7f040028

.field public static final configure_all_done:I = 0x7f040029

.field public static final debug_settings:I = 0x7f04002a

.field public static final design_bottom_sheet_dialog:I = 0x7f04002b

.field public static final design_layout_snackbar:I = 0x7f04002c

.field public static final design_layout_snackbar_include:I = 0x7f04002d

.field public static final design_layout_tab_icon:I = 0x7f04002e

.field public static final design_layout_tab_text:I = 0x7f04002f

.field public static final design_menu_item_action_area:I = 0x7f040030

.field public static final design_navigation_item:I = 0x7f040031

.field public static final design_navigation_item_header:I = 0x7f040032

.field public static final design_navigation_item_separator:I = 0x7f040033

.field public static final design_navigation_item_subheader:I = 0x7f040034

.field public static final design_navigation_menu:I = 0x7f040035

.field public static final design_navigation_menu_item:I = 0x7f040036

.field public static final design_text_input_password_icon:I = 0x7f040037

.field public static final engineering_screen:I = 0x7f040038

.field public static final engineering_tab_layout:I = 0x7f040039

.field public static final enter_existing_password:I = 0x7f04003a

.field public static final fragment_dialog_with_swirly:I = 0x7f04003b

.field public static final goto_settings_permission_fragment:I = 0x7f04003c

.field public static final greeting_item:I = 0x7f04003d

.field public static final greeting_types:I = 0x7f04003e

.field public static final inbox:I = 0x7f04003f

.field public static final inbox_delete:I = 0x7f040040

.field public static final inbox_toolbar:I = 0x7f040041

.field public static final initial_permission_fragment:I = 0x7f040042

.field public static final message_aggregated_item:I = 0x7f040043

.field public static final message_item:I = 0x7f040044

.field public static final message_item_delete:I = 0x7f040045

.field public static final message_list_fragment:I = 0x7f040046

.field public static final non_admin_user:I = 0x7f040047

.field public static final notification_media_action:I = 0x7f040048

.field public static final notification_media_cancel_action:I = 0x7f040049

.field public static final notification_template_big_media:I = 0x7f04004a

.field public static final notification_template_big_media_narrow:I = 0x7f04004b

.field public static final notification_template_lines:I = 0x7f04004c

.field public static final notification_template_media:I = 0x7f04004d

.field public static final notification_template_part_chronometer:I = 0x7f04004e

.field public static final notification_template_part_time:I = 0x7f04004f

.field public static final permission_activity:I = 0x7f040050

.field public static final player:I = 0x7f040051

.field public static final player_action_bar:I = 0x7f040052

.field public static final playerpage:I = 0x7f040053

.field public static final preference:I = 0x7f040054

.field public static final preference_category:I = 0x7f040055

.field public static final preference_category_material:I = 0x7f040056

.field public static final preference_dialog_edittext:I = 0x7f040057

.field public static final preference_dropdown:I = 0x7f040058

.field public static final preference_dropdown_material:I = 0x7f040059

.field public static final preference_information:I = 0x7f04005a

.field public static final preference_information_material:I = 0x7f04005b

.field public static final preference_list_fragment:I = 0x7f04005c

.field public static final preference_material:I = 0x7f04005d

.field public static final preference_recyclerview:I = 0x7f04005e

.field public static final preference_widget_checkbox:I = 0x7f04005f

.field public static final preference_widget_switch:I = 0x7f040060

.field public static final preference_widget_switch_compat:I = 0x7f040061

.field public static final preferences:I = 0x7f040062

.field public static final prefeulalink:I = 0x7f040063

.field public static final prefpolicylink:I = 0x7f040064

.field public static final quickaction:I = 0x7f040065

.field public static final select_dialog_item_material:I = 0x7f040066

.field public static final select_dialog_multichoice_material:I = 0x7f040067

.field public static final select_dialog_singlechoice_material:I = 0x7f040068

.field public static final settings:I = 0x7f040069

.field public static final setup_error_missing_msisdn:I = 0x7f04006a

.field public static final support_simple_spinner_dropdown_item:I = 0x7f04006b

.field public static final tab_inbox_indicator:I = 0x7f04006c

.field public static final tab_saved_indicator:I = 0x7f04006d

.field public static final tc:I = 0x7f04006e

.field public static final text_with_bullet:I = 0x7f04006f

.field public static final tool_bar:I = 0x7f040070

.field public static final two_vertical_buttons_layout:I = 0x7f040071

.field public static final vvm_tab_indicator:I = 0x7f040072

.field public static final welcome:I = 0x7f040073

.field public static final welcome_item:I = 0x7f040074

.field public static final widget_layout:I = 0x7f040075


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

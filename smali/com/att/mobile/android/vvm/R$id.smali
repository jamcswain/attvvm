.class public final Lcom/att/mobile/android/vvm/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final AccountVerificationFailedText2:I = 0x7f0f00b3

.field public static final EULAtextView:I = 0x7f0f015c

.field public static final ErrorView:I = 0x7f0f00f7

.field public static final LinearLayout0:I = 0x7f0f0093

.field public static final LinearLayout01:I = 0x7f0f0110

.field public static final LinearLayout02:I = 0x7f0f0119

.field public static final LinearLayout2:I = 0x7f0f00aa

.field public static final MessagesTxt:I = 0x7f0f0142

.field public static final PrPtextView:I = 0x7f0f015d

.field public static final RelativeLayout01:I = 0x7f0f016a

.field public static final RelativeLayout1:I = 0x7f0f0078

.field public static final SettingAccountText:I = 0x7f0f0081

.field public static final TextView06:I = 0x7f0f00ae

.field public static final TextView07:I = 0x7f0f00af

.field public static final TextView08:I = 0x7f0f00ad

.field public static final TextView09:I = 0x7f0f00ab

.field public static final TranscriptionText:I = 0x7f0f0116

.field public static final action0:I = 0x7f0f0129

.field public static final action_bar:I = 0x7f0f0065

.field public static final action_bar_activity_content:I = 0x7f0f0000

.field public static final action_bar_container:I = 0x7f0f0064

.field public static final action_bar_root:I = 0x7f0f0060

.field public static final action_bar_spinner:I = 0x7f0f0001

.field public static final action_bar_subtitle:I = 0x7f0f0045

.field public static final action_bar_title:I = 0x7f0f0044

.field public static final action_context_bar:I = 0x7f0f0066

.field public static final action_divider:I = 0x7f0f012d

.field public static final action_menu_divider:I = 0x7f0f0002

.field public static final action_menu_presenter:I = 0x7f0f0003

.field public static final action_mode_bar:I = 0x7f0f0062

.field public static final action_mode_bar_stub:I = 0x7f0f0061

.field public static final action_mode_close_button:I = 0x7f0f0046

.field public static final actionnextButton:I = 0x7f0f013c

.field public static final actionnumberOfMessages:I = 0x7f0f013b

.field public static final actionpreviousButton:I = 0x7f0f013a

.field public static final activity_chooser_view_content:I = 0x7f0f0047

.field public static final add:I = 0x7f0f001a

.field public static final adjust_height:I = 0x7f0f0036

.field public static final adjust_width:I = 0x7f0f0037

.field public static final agg_user_image:I = 0x7f0f008c

.field public static final alertTitle:I = 0x7f0f0053

.field public static final all:I = 0x7f0f0032

.field public static final always:I = 0x7f0f0038

.field public static final arrow_down:I = 0x7f0f0162

.field public static final arrow_up:I = 0x7f0f015f

.field public static final attm_launcher_paragraph1:I = 0x7f0f0097

.field public static final attm_launcher_paragraph2:I = 0x7f0f0098

.field public static final attm_launcher_title:I = 0x7f0f0096

.field public static final audioEqualizer:I = 0x7f0f009c

.field public static final audioRecordedPlayButton:I = 0x7f0f00a2

.field public static final audioRecordedRecordButton:I = 0x7f0f009d

.field public static final audioRecordedSendButton:I = 0x7f0f00a3

.field public static final audioRecordingProgressBar:I = 0x7f0f00a5

.field public static final audio_recorder_buttons_panel:I = 0x7f0f00a6

.field public static final audio_recorder_control_panel:I = 0x7f0f00a4

.field public static final auto:I = 0x7f0f0021

.field public static final avatarImage:I = 0x7f0f0118

.field public static final awesomepager:I = 0x7f0f0138

.field public static final backBtnST:I = 0x7f0f0089

.field public static final baseLayout:I = 0x7f0f00b4

.field public static final beginning:I = 0x7f0f0034

.field public static final bottom:I = 0x7f0f0022

.field public static final bottomButtonsLayout:I = 0x7f0f007a

.field public static final bottomLinksLayout:I = 0x7f0f015b

.field public static final bottomShadow:I = 0x7f0f010b

.field public static final bottomlayout:I = 0x7f0f014e

.field public static final btnAccept:I = 0x7f0f016d

.field public static final btnBeginsetup:I = 0x7f0f017a

.field public static final btnCallAgain:I = 0x7f0f00b0

.field public static final btnContinue:I = 0x7f0f00ba

.field public static final btnDecline:I = 0x7f0f016c

.field public static final btnNext:I = 0x7f0f00c4

.field public static final btnQuit1:I = 0x7f0f00b1

.field public static final btnSetupCancel:I = 0x7f0f0082

.field public static final btnSetupExit:I = 0x7f0f0088

.field public static final bullet:I = 0x7f0f016f

.field public static final bulleted_text:I = 0x7f0f0170

.field public static final bulleted_text_layout:I = 0x7f0f016e

.field public static final buttonFinishSetup:I = 0x7f0f00fc

.field public static final buttonNegative:I = 0x7f0f0091

.field public static final buttonPanel:I = 0x7f0f004e

.field public static final buttonPositive:I = 0x7f0f0092

.field public static final callBack:I = 0x7f0f0146

.field public static final call_failed:I = 0x7f0f00b2

.field public static final cancel_action:I = 0x7f0f012a

.field public static final center:I = 0x7f0f0023

.field public static final center_horizontal:I = 0x7f0f0024

.field public static final center_vertical:I = 0x7f0f0025

.field public static final changePasswordExplain:I = 0x7f0f00c3

.field public static final checkbox:I = 0x7f0f005c

.field public static final chkDeleteMessage:I = 0x7f0f011f

.field public static final chronometer:I = 0x7f0f0130

.field public static final clip_horizontal:I = 0x7f0f002e

.field public static final clip_vertical:I = 0x7f0f002f

.field public static final cm_save:I = 0x7f0f0187

.field public static final cm_trash:I = 0x7f0f0188

.field public static final cm_unsave:I = 0x7f0f0190

.field public static final collapseActionView:I = 0x7f0f0039

.field public static final confidenceLayout:I = 0x7f0f00df

.field public static final confirmPasswordEditText:I = 0x7f0f00b8

.field public static final confirmPasswordImage:I = 0x7f0f00b9

.field public static final contentPanel:I = 0x7f0f0054

.field public static final content_frame:I = 0x7f0f015a

.field public static final current_permission_fragment:I = 0x7f0f0136

.field public static final custom:I = 0x7f0f005a

.field public static final customPanel:I = 0x7f0f0059

.field public static final dark:I = 0x7f0f0040

.field public static final date:I = 0x7f0f0114

.field public static final decor_content_parent:I = 0x7f0f0063

.field public static final defaultConfidence:I = 0x7f0f00e0

.field public static final default_activity_button:I = 0x7f0f004a

.field public static final defaulttimer:I = 0x7f0f00dc

.field public static final delButton:I = 0x7f0f0106

.field public static final delete:I = 0x7f0f0144

.field public static final design_bottom_sheet:I = 0x7f0f00d0

.field public static final design_menu_item_action_area:I = 0x7f0f00d7

.field public static final design_menu_item_action_area_stub:I = 0x7f0f00d6

.field public static final design_menu_item_text:I = 0x7f0f00d5

.field public static final design_navigation_view:I = 0x7f0f00d4

.field public static final detail_headerBar:I = 0x7f0f00fe

.field public static final dialogBody:I = 0x7f0f008f

.field public static final dialogHeader:I = 0x7f0f008e

.field public static final dialog_checkBox:I = 0x7f0f0090

.field public static final disableHome:I = 0x7f0f000e

.field public static final dots:I = 0x7f0f009a

.field public static final downloadErrorImage:I = 0x7f0f011e

.field public static final downloadFileImage:I = 0x7f0f011d

.field public static final downloadingGauge:I = 0x7f0f0151

.field public static final edit_query:I = 0x7f0f0067

.field public static final end:I = 0x7f0f0026

.field public static final end_padder:I = 0x7f0f0135

.field public static final engScreen_copyDBButton:I = 0x7f0f00de

.field public static final enterAlways:I = 0x7f0f0015

.field public static final enterAlwaysCollapsed:I = 0x7f0f0016

.field public static final enterPasswordEditText:I = 0x7f0f00b6

.field public static final errorLayout:I = 0x7f0f0108

.field public static final errorText:I = 0x7f0f0109

.field public static final errorTextView:I = 0x7f0f00bb

.field public static final exitUntilCollapsed:I = 0x7f0f0017

.field public static final exit_button:I = 0x7f0f0128

.field public static final expand_activities_button:I = 0x7f0f0048

.field public static final expanded_menu:I = 0x7f0f005b

.field public static final fakeLogin:I = 0x7f0f00cd

.field public static final fill:I = 0x7f0f0030

.field public static final fill_horizontal:I = 0x7f0f0031

.field public static final fill_vertical:I = 0x7f0f0027

.field public static final fixed:I = 0x7f0f0042

.field public static final footer:I = 0x7f0f0161

.field public static final fragmentPlayerBack:I = 0x7f0f013f

.field public static final fragmentPlayerMenu:I = 0x7f0f0145

.field public static final fragment_container:I = 0x7f0f008d

.field public static final frmLayout:I = 0x7f0f017c

.field public static final gaugeProgress:I = 0x7f0f0105

.field public static final gaugeSetupProgress:I = 0x7f0f0080

.field public static final greetingRV:I = 0x7f0f00f6

.field public static final greeting_error_sub1:I = 0x7f0f00f9

.field public static final greeting_error_sub2:I = 0x7f0f00fa

.field public static final header2:I = 0x7f0f015e

.field public static final headerShadow1:I = 0x7f0f010a

.field public static final headerTextItem:I = 0x7f0f00f1

.field public static final header_title:I = 0x7f0f00e9

.field public static final home:I = 0x7f0f0004

.field public static final homeAsUp:I = 0x7f0f000f

.field public static final icon:I = 0x7f0f004c

.field public static final icon_frame:I = 0x7f0f0157

.field public static final icon_only:I = 0x7f0f003d

.field public static final ifRoom:I = 0x7f0f003a

.field public static final image:I = 0x7f0f0049

.field public static final imageButton:I = 0x7f0f017d

.field public static final imageItem:I = 0x7f0f00f3

.field public static final imageView1:I = 0x7f0f00c1

.field public static final imgWelcome:I = 0x7f0f010f

.field public static final inboxHeaderLayout:I = 0x7f0f0101

.field public static final inboxTitle:I = 0x7f0f0104

.field public static final inboxTitleLayout:I = 0x7f0f0102

.field public static final inboxTitleLayout1:I = 0x7f0f0103

.field public static final inbox_context_menu_copy_transcript:I = 0x7f0f0183

.field public static final inbox_context_menu_delete:I = 0x7f0f0184

.field public static final inbox_context_menu_export:I = 0x7f0f0182

.field public static final inbox_context_menu_message_details:I = 0x7f0f017f

.field public static final inbox_context_menu_save:I = 0x7f0f0181

.field public static final inbox_context_menu_share:I = 0x7f0f0180

.field public static final inbox_recycler_view:I = 0x7f0f0124

.field public static final inbox_tab_indicator:I = 0x7f0f0168

.field public static final inbox_toolbar:I = 0x7f0f010c

.field public static final info:I = 0x7f0f0134

.field public static final itemSummary:I = 0x7f0f0075

.field public static final itemSummarySecondView:I = 0x7f0f0076

.field public static final itemTitle:I = 0x7f0f0074

.field public static final item_touch_helper_previous_elevation:I = 0x7f0f0005

.field public static final left:I = 0x7f0f0028

.field public static final light:I = 0x7f0f0041

.field public static final line1:I = 0x7f0f012e

.field public static final line3:I = 0x7f0f0132

.field public static final linearLayout:I = 0x7f0f00b5

.field public static final linearLayoutItem:I = 0x7f0f00f0

.field public static final list:I = 0x7f0f0158

.field public static final listMode:I = 0x7f0f000b

.field public static final listView:I = 0x7f0f00f5

.field public static final list_item:I = 0x7f0f004b

.field public static final ll3:I = 0x7f0f0086

.field public static final loadspeaker:I = 0x7f0f0152

.field public static final login:I = 0x7f0f00ce

.field public static final lowerButton:I = 0x7f0f0172

.field public static final mainLayout:I = 0x7f0f00fd

.field public static final manualHost:I = 0x7f0f00cb

.field public static final media_actions:I = 0x7f0f012c

.field public static final menu_add_to_contacts:I = 0x7f0f018f

.field public static final menu_copy_message_text:I = 0x7f0f018d

.field public static final menu_export:I = 0x7f0f018c

.field public static final menu_refresh:I = 0x7f0f0185

.field public static final menu_save:I = 0x7f0f018b

.field public static final menu_settings:I = 0x7f0f0186

.field public static final menu_share:I = 0x7f0f0189

.field public static final menu_unsave:I = 0x7f0f018a

.field public static final menu_view_caller_details:I = 0x7f0f018e

.field public static final messageStatus:I = 0x7f0f0115

.field public static final messageTitleLayout:I = 0x7f0f0141

.field public static final message_list_fragment__empty_inbox_message:I = 0x7f0f0123

.field public static final message_list_fragment__loading_pb:I = 0x7f0f0122

.field public static final message_list_fragment_loading_container:I = 0x7f0f0121

.field public static final middle:I = 0x7f0f0035

.field public static final mini:I = 0x7f0f0033

.field public static final missing_msisdn_final_text:I = 0x7f0f0167

.field public static final missing_msisdn_line1:I = 0x7f0f0164

.field public static final missing_msisdn_line2:I = 0x7f0f0165

.field public static final missing_msisdn_line3:I = 0x7f0f0166

.field public static final missing_msisdn_sub_text:I = 0x7f0f0163

.field public static final multiply:I = 0x7f0f001b

.field public static final name:I = 0x7f0f011a

.field public static final navigationBarLayout:I = 0x7f0f0139

.field public static final navigation_header_container:I = 0x7f0f00d3

.field public static final never:I = 0x7f0f003b

.field public static final newPasswordImage:I = 0x7f0f00b7

.field public static final nextButton:I = 0x7f0f0112

.field public static final non_admin_user_text:I = 0x7f0f0127

.field public static final none:I = 0x7f0f0010

.field public static final normal:I = 0x7f0f000c

.field public static final numberOfMessages:I = 0x7f0f0143

.field public static final open_att_button:I = 0x7f0f0099

.field public static final parallax:I = 0x7f0f002c

.field public static final parentPanel:I = 0x7f0f0050

.field public static final passwordLabel:I = 0x7f0f00c7

.field public static final permission_fragment_title:I = 0x7f0f00ec

.field public static final permission_framgent_goto_settings_text:I = 0x7f0f00ee

.field public static final permission_framgent_text:I = 0x7f0f00ed

.field public static final personalGreeting:I = 0x7f0f00c2

.field public static final phoneType:I = 0x7f0f014b

.field public static final phoneTypeIcon:I = 0x7f0f014a

.field public static final pin:I = 0x7f0f002d

.field public static final playButton:I = 0x7f0f0150

.field public static final playPauseButton:I = 0x7f0f00a8

.field public static final play_btn:I = 0x7f0f010e

.field public static final playerTotalTime:I = 0x7f0f0153

.field public static final player_bar:I = 0x7f0f014f

.field public static final player_container:I = 0x7f0f013e

.field public static final player_user_bg_image:I = 0x7f0f013d

.field public static final previousButton:I = 0x7f0f0140

.field public static final primaryLinearLayoutItem:I = 0x7f0f00ef

.field public static final progressTime:I = 0x7f0f0154

.field public static final progress_circular:I = 0x7f0f0006

.field public static final progress_horizontal:I = 0x7f0f0007

.field public static final radio:I = 0x7f0f005e

.field public static final recordStopButton:I = 0x7f0f00a7

.field public static final recordingProgressBar:I = 0x7f0f009e

.field public static final recordingProgressText:I = 0x7f0f009f

.field public static final recordingTotalTime:I = 0x7f0f00a1

.field public static final rellayout2:I = 0x7f0f0083

.field public static final retryBodyId:I = 0x7f0f007b

.field public static final right:I = 0x7f0f0029

.field public static final root_layout:I = 0x7f0f0137

.field public static final savedOrDownloadFlipper:I = 0x7f0f011c

.field public static final savedStatus:I = 0x7f0f0148

.field public static final saved_tab_indicator:I = 0x7f0f0169

.field public static final screen:I = 0x7f0f001c

.field public static final scroll:I = 0x7f0f0018

.field public static final scrollIndicatorDown:I = 0x7f0f0058

.field public static final scrollIndicatorUp:I = 0x7f0f0055

.field public static final scrollView:I = 0x7f0f0056

.field public static final scroll_screen:I = 0x7f0f0175

.field public static final scrollable:I = 0x7f0f0043

.field public static final scview:I = 0x7f0f0085

.field public static final search_badge:I = 0x7f0f0069

.field public static final search_bar:I = 0x7f0f0068

.field public static final search_button:I = 0x7f0f006a

.field public static final search_close_btn:I = 0x7f0f006f

.field public static final search_edit_frame:I = 0x7f0f006b

.field public static final search_go_btn:I = 0x7f0f0071

.field public static final search_mag_icon:I = 0x7f0f006c

.field public static final search_plate:I = 0x7f0f006d

.field public static final search_src_text:I = 0x7f0f006e

.field public static final search_voice_btn:I = 0x7f0f0072

.field public static final seekBarDefault:I = 0x7f0f014d

.field public static final selectAllChk:I = 0x7f0f0107

.field public static final select_dialog_listview:I = 0x7f0f0073

.field public static final selectedImage:I = 0x7f0f0113

.field public static final sendButton:I = 0x7f0f00a9

.field public static final sendMessage:I = 0x7f0f014c

.field public static final senderImage:I = 0x7f0f0147

.field public static final senderImageFrame:I = 0x7f0f0120

.field public static final sendrImageLayout:I = 0x7f0f0117

.field public static final settingPasswordInput:I = 0x7f0f00c8

.field public static final settingUserInput:I = 0x7f0f00c6

.field public static final settingsList:I = 0x7f0f0077

.field public static final setupBtnST:I = 0x7f0f008b

.field public static final setupGreetingsLayout:I = 0x7f0f00bf

.field public static final shortcut:I = 0x7f0f005d

.field public static final showCustom:I = 0x7f0f0011

.field public static final showHome:I = 0x7f0f0012

.field public static final showTitle:I = 0x7f0f0013

.field public static final simulateTokenErrorCheckBox:I = 0x7f0f00e1

.field public static final simulateTranslErrCheckBox:I = 0x7f0f00e5

.field public static final skipBtnST:I = 0x7f0f008a

.field public static final sliding_tabs:I = 0x7f0f00ff

.field public static final snackbar_action:I = 0x7f0f00d2

.field public static final snackbar_text:I = 0x7f0f00d1

.field public static final snap:I = 0x7f0f0019

.field public static final spacer:I = 0x7f0f004f

.field public static final speaker_text:I = 0x7f0f0155

.field public static final spinner:I = 0x7f0f00ca

.field public static final split_action_bar:I = 0x7f0f0008

.field public static final src_atop:I = 0x7f0f001d

.field public static final src_in:I = 0x7f0f001e

.field public static final src_over:I = 0x7f0f001f

.field public static final sslOn:I = 0x7f0f00cc

.field public static final standard:I = 0x7f0f003e

.field public static final start:I = 0x7f0f002a

.field public static final status_bar_latest_event_content:I = 0x7f0f012b

.field public static final subTextItem:I = 0x7f0f00f2

.field public static final submenuarrow:I = 0x7f0f005f

.field public static final submit_area:I = 0x7f0f0070

.field public static final swirlView:I = 0x7f0f00fb

.field public static final switchWidget:I = 0x7f0f0159

.field public static final tabMode:I = 0x7f0f000d

.field public static final tab_contacs:I = 0x7f0f0174

.field public static final tab_inbox:I = 0x7f0f0173

.field public static final temp:I = 0x7f0f009b

.field public static final tempPassword:I = 0x7f0f00c9

.field public static final text:I = 0x7f0f0133

.field public static final text2:I = 0x7f0f0131

.field public static final textItem:I = 0x7f0f017b

.field public static final textSpacerNoButtons:I = 0x7f0f0057

.field public static final textTranscriptionBody:I = 0x7f0f0156

.field public static final textView:I = 0x7f0f017e

.field public static final textView1:I = 0x7f0f00db

.field public static final textView2:I = 0x7f0f00dd

.field public static final textView3:I = 0x7f0f00d9

.field public static final text_input_password_toggle:I = 0x7f0f00d8

.field public static final time:I = 0x7f0f012f

.field public static final timeDelim:I = 0x7f0f00a0

.field public static final timerLayout:I = 0x7f0f00da

.field public static final title:I = 0x7f0f004d

.field public static final titleLayout:I = 0x7f0f00bc

.field public static final titleText:I = 0x7f0f00bd

.field public static final title_att:I = 0x7f0f0094

.field public static final title_template:I = 0x7f0f0052

.field public static final title_visual:I = 0x7f0f0095

.field public static final tokenErrorLayout:I = 0x7f0f00e2

.field public static final tokenErrorSpinner:I = 0x7f0f00e3

.field public static final tokenRetryFailcheckBox:I = 0x7f0f00e4

.field public static final toolbar:I = 0x7f0f0079

.field public static final toolbar_title_and_buttons:I = 0x7f0f010d

.field public static final top:I = 0x7f0f002b

.field public static final topPanel:I = 0x7f0f0051

.field public static final touch_outside:I = 0x7f0f00cf

.field public static final tracks:I = 0x7f0f0160

.field public static final translErrorLayout:I = 0x7f0f00e6

.field public static final translErrorSpinner:I = 0x7f0f00e7

.field public static final translRetryFailcheckBox:I = 0x7f0f00e8

.field public static final tvTC:I = 0x7f0f0176

.field public static final tvTC1:I = 0x7f0f0177

.field public static final tvTC2:I = 0x7f0f0178

.field public static final tvTC3:I = 0x7f0f0179

.field public static final txtHeader:I = 0x7f0f00ea

.field public static final txtOops:I = 0x7f0f00f8

.field public static final txtPersonalGreeting:I = 0x7f0f00be

.field public static final txtSetupLoading:I = 0x7f0f00eb

.field public static final txtSetupRetryHeader:I = 0x7f0f007c

.field public static final txtSetupRetrySubHeader:I = 0x7f0f007d

.field public static final txtSetupRetrySubHeader2:I = 0x7f0f00ac

.field public static final txtSetupRetry_Sub_1:I = 0x7f0f007e

.field public static final txtSetupRetry_Sub_2:I = 0x7f0f007f

.field public static final txtSetupUnknownBox:I = 0x7f0f0087

.field public static final txtSubDesc4:I = 0x7f0f0111

.field public static final txtTCBody:I = 0x7f0f016b

.field public static final txtTitle:I = 0x7f0f0084

.field public static final up:I = 0x7f0f0009

.field public static final upperButton:I = 0x7f0f0171

.field public static final urgentStatus:I = 0x7f0f011b

.field public static final useLogo:I = 0x7f0f0014

.field public static final userLabel:I = 0x7f0f00c5

.field public static final userPhoneTypeLayout:I = 0x7f0f0149

.field public static final viewFlipper:I = 0x7f0f00f4

.field public static final view_offset_helper:I = 0x7f0f000a

.field public static final view_pager:I = 0x7f0f0100

.field public static final welcome_to:I = 0x7f0f0126

.field public static final welcome_to_title:I = 0x7f0f0125

.field public static final whatIsLayout:I = 0x7f0f00c0

.field public static final wide:I = 0x7f0f003f

.field public static final withText:I = 0x7f0f003c

.field public static final wrap_content:I = 0x7f0f0020


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public interface abstract Lcom/att/mobile/android/vvm/model/Constants$PasswordChangeRequiredStatus;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PasswordChangeRequiredStatus"
.end annotation


# static fields
.field public static final CHANGED_IN_TUI:I = 0x1

.field public static final NONE:I = -0x1

.field public static final PASSWORD_MISSING:I = 0x4

.field public static final RESET_BY_ADMIN:I = 0x3

.field public static final TEMPORARY_PASSWORD:I = 0x2

.class public interface abstract Lcom/att/mobile/android/vvm/model/Constants$SETUP_STATUS;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SETUP_STATUS"
.end annotation


# static fields
.field public static final CALL_VOICE_MAIL:I = 0x7

.field public static final ENTER_EXISTING_PWD:I = 0x9

.field public static final ENTER_PASSWORD:I = 0xa

.field public static final INIT_CALL_VOICE_MAIL:I = 0xf

.field public static final INIT_GREETINGS:I = 0xc

.field public static final INIT_WITH_MSISDN:I = 0x1

.field public static final NO_VOICE_MAIL_NUMBER:I = 0x8

.field public static final RESET_PASSWORD:I = 0xe

.field public static final SHOW_CALL_VOICE_MAIL:I = 0x12

.field public static final SUCCESS:I = 0xd

.field public static final TRY_MO_SMS_AGAIN:I = 0x6

.field public static final UNKNOWN:I = -0x1

.field public static final UNKNOWN_MAILBOX:I = 0x11

.field public static final WAIT_BINARY_SMS1:I = 0x3

.field public static final WAIT_BINARY_SMS2:I = 0x4

.field public static final WAIT_CALL_BINARY_SMS:I = 0x5

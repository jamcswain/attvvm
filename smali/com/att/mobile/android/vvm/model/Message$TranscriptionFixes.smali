.class public interface abstract Lcom/att/mobile/android/vvm/model/Message$TranscriptionFixes;
.super Ljava/lang/Object;
.source "Message.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Message;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TranscriptionFixes"
.end annotation


# static fields
.field public static final NO_TRANSCRIPTION_POSTFIX:Ljava/lang/String; = "cannot be processed into text."

.field public static final NO_TRANSCRIPTION_POSTFIX_1:Ljava/lang/String; = "could not be processed to text."

.field public static final NO_TRANSCRIPTION_PREFIX:Ljava/lang/String; = "Audio message from"

.field public static final NO_TRANSCRIPTION_PREFIX_1:Ljava/lang/String; = "A voicemail from"

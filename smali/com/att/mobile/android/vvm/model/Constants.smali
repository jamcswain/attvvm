.class public Lcom/att/mobile/android/vvm/model/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;,
        Lcom/att/mobile/android/vvm/model/Constants$ATTM;,
        Lcom/att/mobile/android/vvm/model/Constants$MessageFilter;,
        Lcom/att/mobile/android/vvm/model/Constants$PasswordChangeRequiredStatus;,
        Lcom/att/mobile/android/vvm/model/Constants$ATTM_STATUS;,
        Lcom/att/mobile/android/vvm/model/Constants$SETUP_STATUS;,
        Lcom/att/mobile/android/vvm/model/Constants$KEYS;,
        Lcom/att/mobile/android/vvm/model/Constants$METADATA_VARIABLES;,
        Lcom/att/mobile/android/vvm/model/Constants$EXTRAS;,
        Lcom/att/mobile/android/vvm/model/Constants$EVENTS;,
        Lcom/att/mobile/android/vvm/model/Constants$ACTIONS;,
        Lcom/att/mobile/android/vvm/model/Constants$TRANSACTIONS;
    }
.end annotation


# static fields
.field public static final ACTION_GOTO_SAVED:Ljava/lang/String; = "go_to_saved"

.field public static final ALU_LINK_TEXT:Ljava/lang/String; = "Additional information on .lvp audio files can be found at http://www.alcatel-lucent.com/mvp"

.field public static AVATAR_FOR_LAST_NUMBER_ARR:[I = null

.field public static final BACKUP_DATE_FORMAT:Ljava/lang/String; = "MM/dd"

.field public static DEFAULT_AVATAR_COLORS:[I = null

.field public static DEFAULT_AVATAR_IDs:[I = null

.field public static final DEFAULT_MAX_MESSAGES:I = 0x28

.field public static final DEFAULT_MAX_MESSAGES_WARNING:I = 0x3

.field public static final DEFAULT_MIN_CONFIDENCE_LEVEL:Ljava/lang/String; = "0.0"

.field public static final DO_NOT_SHOW_SAVED_DIALOG_AGAIN:Ljava/lang/String; = "doNotShowSavedDialogAgain"

.field public static final EXACT_DAY_OF_WEEK_FORMAT:Ljava/lang/String; = "EEEE"

.field public static final IMAP4_TAG_STR:Ljava/lang/String; = "ATT_VVM__ "

.field public static final INTENT_DATA_FILTER_TYPE:Ljava/lang/String; = "intent.data.filter.type"

.field public static final INTENT_DATA_USER_NAME:Ljava/lang/String; = "intent.data.user.name"

.field public static final INTENT_DATA_USER_PHONE:Ljava/lang/String; = "intent.data.user.phone"

.field public static final INTENT_DATA_USER_PHOTO_URI:Ljava/lang/String; = "intent.data.user.photo.uri"

.field public static final IS_DEMO_VERSION:Z = false

.field public static final SHARED_FILE_TIME_FORMAT:Ljava/lang/String; = "%Y-%m-%d.%H.%M.%S"

.field public static final SHORTCODE_FOR_MO_SMS:Ljava/lang/String; = "94183567"

.field public static final SP_KEY_IS_CONNECTED_TO_INTERNET:Ljava/lang/String; = "com.att.isconnectedtointernet"

.field public static final TIME_FORMAT_12:Ljava/lang/String; = "hh:mm AA"

.field public static final TIME_FORMAT_24:Ljava/lang/String; = "kk:mm"

.field public static final WELCOME_MESSAGE_FILE_NAME:Ljava/lang/String; = "welcome_amr_new.amr"

.field public static final WELCOME_MESSAGE_ID:I = 0x7fffffff


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 450
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants;->DEFAULT_AVATAR_IDs:[I

    .line 458
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants;->DEFAULT_AVATAR_COLORS:[I

    .line 469
    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ORANGE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 470
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ORANGE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 471
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ORANGE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 472
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->BLUE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 473
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->BLUE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 474
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sget-object v1, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->BLUE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 475
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v1

    aput v1, v0, v3

    const/4 v1, 0x6

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->GREEN:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 476
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->GREEN:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 477
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->PURPLE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 478
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->PURPLE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    .line 479
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants;->AVATAR_FOR_LAST_NUMBER_ARR:[I

    .line 469
    return-void

    .line 450
    nop

    :array_0
    .array-data 4
        0x7f020116
        0x7f020113
        0x7f020114
        0x7f020115
        0x7f020115
    .end array-data

    .line 458
    :array_1
    .array-data 4
        0x7f0e0061
        0x7f0e000a
        0x7f0e0038
        0x7f0e0053
        0x7f0e0053
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSetupStatusString(I)Ljava/lang/String;
    .locals 1
    .param p0, "setupStatus"    # I

    .prologue
    .line 338
    const-string v0, "NULL"

    .line 340
    .local v0, "setupStatusString":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 387
    :pswitch_0
    const-string v0, "UNKNOWN"

    .line 389
    :goto_0
    return-object v0

    .line 342
    :pswitch_1
    const-string v0, "UNKNOWN"

    .line 343
    goto :goto_0

    .line 345
    :pswitch_2
    const-string v0, "INIT_WITH_MSISDN"

    .line 346
    goto :goto_0

    .line 348
    :pswitch_3
    const-string v0, "WAIT_BINARY_SMS1"

    .line 349
    goto :goto_0

    .line 351
    :pswitch_4
    const-string v0, "WAIT_BINARY_SMS2"

    .line 352
    goto :goto_0

    .line 354
    :pswitch_5
    const-string v0, "WAIT_CALL_BINARY_SMS"

    .line 355
    goto :goto_0

    .line 357
    :pswitch_6
    const-string v0, "TRY_MO_SMS_AGAIN"

    .line 358
    goto :goto_0

    .line 360
    :pswitch_7
    const-string v0, "CALL_VOICE_MAIL"

    .line 361
    goto :goto_0

    .line 363
    :pswitch_8
    const-string v0, "INIT_CALL_VOICE_MAIL"

    .line 364
    goto :goto_0

    .line 366
    :pswitch_9
    const-string v0, "NO_VOICE_MAIL_NUMBER"

    .line 367
    goto :goto_0

    .line 369
    :pswitch_a
    const-string v0, "ENTER_PASSWORD"

    .line 370
    goto :goto_0

    .line 372
    :pswitch_b
    const-string v0, "ENTER_EXISTING_PWD"

    .line 373
    goto :goto_0

    .line 375
    :pswitch_c
    const-string v0, "SUCCESS"

    .line 376
    goto :goto_0

    .line 378
    :pswitch_d
    const-string v0, "RESET_PASSWORD"

    .line 379
    goto :goto_0

    .line 381
    :pswitch_e
    const-string v0, "UNKNOWN_MAILBOX"

    .line 382
    goto :goto_0

    .line 384
    :pswitch_f
    const-string v0, "SHOW_CALL_VOICE_MAIL"

    .line 385
    goto :goto_0

    .line 340
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_8
        :pswitch_0
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

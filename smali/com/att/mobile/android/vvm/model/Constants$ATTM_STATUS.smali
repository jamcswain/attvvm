.class public interface abstract Lcom/att/mobile/android/vvm/model/Constants$ATTM_STATUS;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ATTM_STATUS"
.end annotation


# static fields
.field public static final INSTALLED_NOT_PROVISIONED:I = 0x1

.field public static final NOT_INSTALLED:I = 0x0

.field public static final PROVISIONED:I = 0x2

.field public static final UNKNOWN:I = -0x1

.class public Lcom/att/mobile/android/vvm/model/Constants$KEYS;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KEYS"
.end annotation


# static fields
.field public static final ALTERNATIVE_PORT:Ljava/lang/String; = "altPort"

.field public static final ATTM_JSON_HOST_KEY:Ljava/lang/String; = "Host"

.field public static final ATTM_JSON_MAILBOX_KEY:Ljava/lang/String; = "MailBox"

.field public static final ATTM_JSON_PASSWORD_KEY:Ljava/lang/String; = "Password"

.field public static final ATTM_JSON_PORT_KEY:Ljava/lang/String; = "Port"

.field public static final ATTM_JSON_SSL_PORT_KEY:Ljava/lang/String; = "SSLPort"

.field public static final ATTM_JSON_TOKEN_KEY:Ljava/lang/String; = "Token"

.field public static final ATTM_STATUS:Ljava/lang/String; = "isAttmInstalled"

.field public static final AVAILABLE_VERSION_ON_MARKET:Ljava/lang/String; = "availableVersionOnMarket"

.field public static final CREATE_SHORTCUT_CHECKBOX:Ljava/lang/String; = "createShortcutCheckbox"

.field public static final CURRENT_SETUP_STATE:Ljava/lang/String; = "currentSetupState"

.field public static final DID_NOTIFICATIONS_EXIST_BEFORE_REBOOT:Ljava/lang/String; = "didNotificationsExistBeforeReboot"

.field public static final DO_NOT_SHOW_LAUNCH_ATTM_SCREEN:Ljava/lang/String; = "showLaunchAttmScreenOnlyOnce"

.field public static final FIRST_LOGIN_4_0:Ljava/lang/String; = "first_login_4_0"

.field public static final IS_FIRST_USE:Ljava/lang/String; = "isFirstUse"

.field public static final IS_SETUP_COMPLETED:Ljava/lang/String; = "isSetupCompleted"

.field public static final IS_SETUP_STARTED:Ljava/lang/String; = "isSetupStarted"

.field public static final IS_WELCOME_MESSAGE_INSERTED:Ljava/lang/String; = "isWelcomeMessageInserted"

.field public static final LAST_UPDATE_CHECK_DATE:Ljava/lang/String; = "lastUpdateCheckDate"

.field public static final MAX_MESSAGES:Ljava/lang/String; = "MaxMessages"

.field public static final MAX_MESSAGES_WARNING:Ljava/lang/String; = "MaxMessagesWarning"

.field public static final MinConfidence:Ljava/lang/String; = "MinConfidence"

.field public static final NEED_REFRESH_INBOX:Ljava/lang/String; = "needRefreshInbox"

.field public static final NOTIFY_SETUP_REQUIRED_COUNTER:Ljava/lang/String; = "notifySetupRequiredCounter"

.field public static final PASSWORD_CHANGE_REQUIRED_STATUS:Ljava/lang/String; = "PasswordChangeRequiredStatus"

.field public static PERMISSION_EVER_REQUESTED:Ljava/lang/String; = null

.field public static final PKEY:Ljava/lang/String; = ".ldSignature"

.field public static final PREFERENCE_BEGIN_SETUP_TIME:Ljava/lang/String; = "mosmsTimePref"

.field public static final PREFERENCE_CUSTOM_GREETING:Ljava/lang/String; = "customGreetingPref"

.field public static final PREFERENCE_DEBUG_SSL_ON:Ljava/lang/String; = "debugSslOnPref"

.field public static final PREFERENCE_DEFAULT_GREETING:Ljava/lang/String; = "defaultGreetingPref"

.field public static final PREFERENCE_FILE_NAME:Ljava/lang/String; = "vvmPreferences"

.field public static final PREFERENCE_HOST:Ljava/lang/String; = "hostPref"

.field public static final PREFERENCE_MAILBOX_NUMBER:Ljava/lang/String; = "userPref"

.field public static final PREFERENCE_MAILBOX_STATUS:Ljava/lang/String; = "mailboxStatus"

.field public static final PREFERENCE_NAME_GREETING:Ljava/lang/String; = "nameGreetingPref"

.field public static final PREFERENCE_PASSWORD:Ljava/lang/String; = "passwordPref"

.field public static final PREFERENCE_PORT:Ljava/lang/String; = "portPref"

.field public static final PREFERENCE_SSL_PORT:Ljava/lang/String; = "sslPortPref"

.field public static final PREFERENCE_TIMER_DEFAULT:Ljava/lang/String; = "timerDefaultPref"

.field public static final PREFERENCE_TOKEN:Ljava/lang/String; = "tokenPref"

.field public static final SHOULD_CHECK_ATTM_STATUS_ON_FOREGROUND:Ljava/lang/String; = "shouldCheckAttmStatusOnForeground"

.field public static final SIMULATE_TOKEN_ERROR:Ljava/lang/String; = "simulatetokenerror"

.field public static final SIMULATE_TRANSL_ERROR:Ljava/lang/String; = "simulatetranslerror"

.field public static final SIM_ID:Ljava/lang/String; = "simId"

.field public static final SIM_SWAP:Ljava/lang/String; = "simSwap"

.field public static final TokenErrorCode:Ljava/lang/String; = "TokenErrorCode"

.field public static final TokenRetryFail:Ljava/lang/String; = "TokenRetryFail"

.field public static final TranslErrorCode:Ljava/lang/String; = "TranslErrorCode"

.field public static final TranslRetryFail:Ljava/lang/String; = "TranslRetryFail"

.field public static final WAS_INBOX_REFRESHED:Ljava/lang/String; = "wasInboxRefreshed"

.field public static final WATSON_TOKEN:Ljava/lang/String; = "watsonToken"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 314
    const-string v0, "pref_permissions_ever_requested"

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants$KEYS;->PERMISSION_EVER_REQUESTED:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

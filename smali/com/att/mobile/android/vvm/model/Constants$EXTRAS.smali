.class public interface abstract Lcom/att/mobile/android/vvm/model/Constants$EXTRAS;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EXTRAS"
.end annotation


# static fields
.field public static final EXTRA_NEW_MESSAGE_ROW_ID:Ljava/lang/String; = "extra_new_message_row_id"

.field public static final EXTRA_REFRESH_INBOX:Ljava/lang/String; = "refresh_inbox"

.field public static final EXTRA_SHORTCUT_DUPLICATE:Ljava/lang/String; = "duplicate"

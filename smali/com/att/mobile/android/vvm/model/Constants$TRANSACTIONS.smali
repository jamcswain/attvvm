.class public interface abstract Lcom/att/mobile/android/vvm/model/Constants$TRANSACTIONS;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TRANSACTIONS"
.end annotation


# static fields
.field public static final TRANSACTION_DELETE:B = 0x3t

.field public static final TRANSACTION_EXPUNGE:B = 0x5t

.field public static final TRANSACTION_FETCH_AUDIO_ATTACHMENT:B = 0x4t

.field public static final TRANSACTION_FETCH_BODYSTRUCTURE_LIST:B = 0x12t

.field public static final TRANSACTION_FETCH_GREETIGNS_BODIES:B = 0xdt

.field public static final TRANSACTION_FETCH_HEADERS:B = 0x6t

.field public static final TRANSACTION_FETCH_TRANSCRIPTION:B = 0x11t

.field public static final TRANSACTION_GETQUOTA:B = 0x13t

.field public static final TRANSACTION_GET_METADATA:B = 0x9t

.field public static final TRANSACTION_LOGIN:B = 0x0t

.field public static final TRANSACTION_LOGOUT:B = 0x1t

.field public static final TRANSACTION_NOOP:B = 0x8t

.field public static final TRANSACTION_READ:B = 0x7t

.field public static final TRANSACTION_SELECT_INBOX:B = 0x2t

.field public static final TRANSACTION_SEND_GREETING_DATA:B = 0xbt

.field public static final TRANSACTION_SEND_GREETING_REQUEST:B = 0xat

.field public static final TRANSACTION_SET_METADATA:B = 0xct

.field public static final TRANSACTION_SET_METADATA_CLIENT_ID:B = 0x10t

.field public static final TRANSACTION_TUI_SKIP:B = 0xet

.field public static final TRANSACTION_XCHANGE_TUI_PASSWORD:B = 0xft

.class public Lcom/att/mobile/android/vvm/model/Contact;
.super Ljava/lang/Object;
.source "Contact.java"


# instance fields
.field public contactId:J

.field public displayName:Ljava/lang/String;

.field public email:Ljava/lang/String;

.field public exchangeType:I

.field public firstName:Ljava/lang/String;

.field public hasManyTypes:Z

.field public label:Ljava/lang/String;

.field public lastName:Ljava/lang/String;

.field public middleName:Ljava/lang/String;

.field public next:Lcom/att/mobile/android/vvm/model/Contact;

.field public phoneNumber:Ljava/lang/String;

.field public phoneType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->next:Lcom/att/mobile/android/vvm/model/Contact;

    return-void
.end method

.method public static copy(Lcom/att/mobile/android/vvm/model/Contact;)Lcom/att/mobile/android/vvm/model/Contact;
    .locals 3
    .param p0, "c"    # Lcom/att/mobile/android/vvm/model/Contact;

    .prologue
    .line 77
    new-instance v0, Lcom/att/mobile/android/vvm/model/Contact;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/model/Contact;-><init>()V

    .line 78
    .local v0, "tmp":Lcom/att/mobile/android/vvm/model/Contact;
    move-object v1, v0

    .line 79
    .local v1, "tmpCopy":Lcom/att/mobile/android/vvm/model/Contact;
    invoke-static {p0, v0}, Lcom/att/mobile/android/vvm/model/Contact;->copyFields(Lcom/att/mobile/android/vvm/model/Contact;Lcom/att/mobile/android/vvm/model/Contact;)V

    .line 81
    :goto_0
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/Contact;->next:Lcom/att/mobile/android/vvm/model/Contact;

    if-eqz v2, :cond_0

    .line 82
    new-instance v2, Lcom/att/mobile/android/vvm/model/Contact;

    invoke-direct {v2}, Lcom/att/mobile/android/vvm/model/Contact;-><init>()V

    iput-object v2, v0, Lcom/att/mobile/android/vvm/model/Contact;->next:Lcom/att/mobile/android/vvm/model/Contact;

    .line 83
    iget-object v0, v0, Lcom/att/mobile/android/vvm/model/Contact;->next:Lcom/att/mobile/android/vvm/model/Contact;

    .line 84
    iget-object p0, p0, Lcom/att/mobile/android/vvm/model/Contact;->next:Lcom/att/mobile/android/vvm/model/Contact;

    .line 85
    invoke-static {p0, v0}, Lcom/att/mobile/android/vvm/model/Contact;->copyFields(Lcom/att/mobile/android/vvm/model/Contact;Lcom/att/mobile/android/vvm/model/Contact;)V

    goto :goto_0

    .line 88
    :cond_0
    move-object v0, v1

    .line 89
    return-object v0
.end method

.method private static copyFields(Lcom/att/mobile/android/vvm/model/Contact;Lcom/att/mobile/android/vvm/model/Contact;)V
    .locals 2
    .param p0, "src"    # Lcom/att/mobile/android/vvm/model/Contact;
    .param p1, "dest"    # Lcom/att/mobile/android/vvm/model/Contact;

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->contactId:J

    iput-wide v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->contactId:J

    .line 96
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->displayName:Ljava/lang/String;

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->displayName:Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->firstName:Ljava/lang/String;

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->firstName:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->middleName:Ljava/lang/String;

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->middleName:Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->lastName:Ljava/lang/String;

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->lastName:Ljava/lang/String;

    .line 100
    iget v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->phoneType:I

    iput v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->phoneType:I

    .line 101
    iget v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->exchangeType:I

    iput v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->exchangeType:I

    .line 102
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->phoneNumber:Ljava/lang/String;

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->phoneNumber:Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->email:Ljava/lang/String;

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->email:Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->label:Ljava/lang/String;

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->label:Ljava/lang/String;

    .line 105
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Contact;->hasManyTypes:Z

    iput-boolean v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->hasManyTypes:Z

    .line 106
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/Contact;->next:Lcom/att/mobile/android/vvm/model/Contact;

    .line 107
    return-void
.end method

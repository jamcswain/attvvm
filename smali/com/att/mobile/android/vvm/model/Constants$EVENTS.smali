.class public interface abstract Lcom/att/mobile/android/vvm/model/Constants$EVENTS;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EVENTS"
.end annotation


# static fields
.field public static final ATTM_SERVICE_CONNECTED:I = 0x41

.field public static final BACK_FROM_ATTM_LAUNCHAR:I = 0x42

.field public static final BACK_FROM_CONFIGURE_ALL_DONE:I = 0x3e

.field public static final BACK_FROM_GREETINGS:I = 0x49

.field public static final BACK_FROM_PASSWORD_CHANGE:I = 0x3a

.field public static final CONNECTION_CONNECTED:I = 0x36

.field public static final CONNECTION_LOST:I = 0x35

.field public static final CONTACTS_CHANGED:I = 0x30

.field public static final DELETE_FINISHED:I = 0xa

.field public static final ENTER_PASSWORD_CANCELED:I = 0x38

.field public static final ENTER_PASSWORD_FINISHED:I = 0x35

.field public static final EXIT_APP:I = 0x4b

.field public static final FETCH_BODIES_MAX_RETRIES:I = 0x3f

.field public static final GET_METADATA_EXISTING_GREETINGS_FINISHED:I = 0x19

.field public static final GET_METADATA_GREETING_DETAILS_FINISHED:I = 0x18

.field public static final GET_METADATA_GREETING_FAILED:I = 0x1a

.field public static final GET_METADATA_PASSWORD_FINISHED:I = 0x1b

.field public static final GREETING_INIT_FINISHED:I = 0x48

.field public static final GREETING_UPLOAD_FAILED:I = 0x2d

.field public static final GREETING_UPLOAD_SUCCEED:I = 0x2c

.field public static final IDENTIFY_USER_FAILED:I = 0x1e

.field public static final IDENTIFY_USER_FINISHED:I = 0x1d

.field public static final IDENTIFY_USER_STARTED:I = 0x1c

.field public static final INBOX_FINISHED:I = 0x20

.field public static final INBOX_STARTED:I = 0x1f

.field public static final LOGIN_FAILED:I = 0x32

.field public static final LOGIN_FAILED_DUE_TO_WRONG_PASSWORD:I = 0x31

.field public static final LOGIN_SUCCEEDED:I = 0x37

.field public static final MARK_AS_DELETED_FINISHED:I = 0x6

.field public static final MESSAGES_ALLMOST_FULL:I = 0x2a

.field public static final MESSAGES_FULL:I = 0x2b

.field public static final MESSAGE_CONTACT_INFO_UPDATED:I = 0x4c

.field public static final MESSAGE_FILE_DOWNLOADED:I = 0x4

.field public static final MESSAGE_FILE_DOWNLOAD_ERROR:I = 0x7

.field public static final MESSAGE_MARKED_AS_SAVED:I = 0x3

.field public static final MESSAGE_MARKED_AS_UNSAVED:I = 0x8

.field public static final MESSAGE_READ_STATE_CHANGED:I = 0x2

.field public static final MESSAGE_TRANSCRIPTION_DOWNLOADED:I = 0x5

.field public static final NETWORK_CONNECTION_LOST:I = 0x46

.field public static final NETWORK_CONNECTION_RESTORED:I = 0x47

.field public static final NEW_MESSAGE:I = 0x1

.field public static final NON_ADMIN_USER:I = 0x43

.field public static final PASSWORD_ASYNC_SET_CANCELED:I = 0x34

.field public static final PASSWORD_CHANGE_FAILED:I = 0x27

.field public static final PASSWORD_CHANGE_FINISHED:I = 0x26

.field public static final PERMISSIONS_GRANTED:I = 0x4a

.field public static final PLAYER_PAUSED:I = 0x4e

.field public static final REFRESH_UI:I = 0x39

.field public static final RETRIEVE_BODIES_FAILED_NOT_ENOUGH_SPACE:I = 0x29

.field public static final RETRIEVE_BODIES_FINISHED:I = 0x17

.field public static final RETRIEVE_HEADERS_FINISHED:I = 0x16

.field public static final RETRIEVE_MESSAGES_STARTED:I = 0x15

.field public static final SELECT_INBOX_FINISHED_WITH_NO_MESSAGES:I = 0x28

.field public static final SET_METADATA_FAILED:I = 0x23

.field public static final SET_METADATA_FINISHED:I = 0x22

.field public static final SET_METADATA_GREETING_FAILED:I = 0x2f

.field public static final SET_METADATA_GREETING_FINISHED:I = 0x2e

.field public static final SET_METADATA_PASSWORD_FINISHED:I = 0x25

.field public static final SET_METADATA_STARTED:I = 0x21

.field public static final SIM_STATED_CHANGED:I = 0x24

.field public static final SIM_SWAPED:I = 0xb

.field public static final SIM_VALID:I = 0xc

.field public static final START_WELCOME_ACTIVITY:I = 0x40

.field public static final UI_UPDATED:I = 0x4d

.field public static final WELCOME_WIZARD_FINISHED:I = 0x3d

.field public static final XCHANGE_TUI_PASSWORD_FAILED:I = 0x3c

.field public static final XCHANGE_TUI_PASSWORD_FINISHED_SUCCESSFULLY:I = 0x3b

.class public abstract Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;
.super Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;
.source "ListItemCursorRecyclerAdapterBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;,
        Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;,
        Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;,
        Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;,
        Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter",
        "<",
        "Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field protected static final CONTACT_CASHE_DEF_RES_ID:I = 0x2

.field protected static final CONTACT_CASHE_DISPLAY_NAME:I = 0x0

.field protected static final CONTACT_CASHE_IMAGE_URL:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field protected static addToContactContentDescription:Ljava/lang/String;

.field protected static contactDesabledContentDescription:Ljava/lang/String;

.field protected static contactInfoContentDescription:Ljava/lang/String;

.field protected static mContactNamePhotoCashe:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static welcomeMessageContentDescription:Ljava/lang/String;


# instance fields
.field protected mActionModeListener:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;

.field protected mContext:Landroid/content/Context;

.field protected mFilterType:I

.field protected mIsActionModeActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->TAG:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContactNamePhotoCashe:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filterType"    # I
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 126
    invoke-direct {p0, p3}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;-><init>(Landroid/database/Cursor;)V

    .line 127
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    .line 128
    iput p2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mFilterType:I

    .line 130
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const v1, 0x7f0800c8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->contactInfoContentDescription:Ljava/lang/String;

    .line 131
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const v1, 0x7f08009d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->addToContactContentDescription:Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const v1, 0x7f0800c7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->contactDesabledContentDescription:Ljava/lang/String;

    .line 133
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const v1, 0x7f0801fb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->welcomeMessageContentDescription:Ljava/lang/String;

    .line 134
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p1, "x1"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->updateDisplayNameFromVoicemailItem(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    return-void
.end method

.method public static clearContactNamesAndPhotoCashe()V
    .locals 2

    .prologue
    .line 68
    sget-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->TAG:Ljava/lang/String;

    const-string v1, "clearContactNamesAndPhotoCashe"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    sget-object v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContactNamePhotoCashe:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 70
    return-void
.end method

.method protected static setDefaultAvatarImage(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;Z)V
    .locals 9
    .param p0, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p1, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;
    .param p2, "saveToChache"    # Z

    .prologue
    .line 322
    sget-object v5, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->DEFAULT:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ordinal()I

    move-result v0

    .line 323
    .local v0, "avatarIndex":I
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    .line 324
    .local v4, "phoneNumber":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 325
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .line 326
    .local v1, "ind":I
    if-ltz v1, :cond_0

    .line 327
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 328
    .local v2, "lastChar":C
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 329
    invoke-static {v2}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v3

    .line 330
    .local v3, "lastCharInt":I
    sget-object v5, Lcom/att/mobile/android/vvm/model/Constants;->AVATAR_FOR_LAST_NUMBER_ARR:[I

    aget v0, v5, v3

    .line 335
    .end local v1    # "ind":I
    .end local v2    # "lastChar":C
    .end local v3    # "lastCharInt":I
    :cond_0
    sget-object v5, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setDefaultAvatarImage for phoneNumber="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " avatarIndex="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " saveToChache="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    sget-object v6, Lcom/att/mobile/android/vvm/model/Constants;->DEFAULT_AVATAR_IDs:[I

    aget v6, v6, v0

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 338
    if-eqz p2, :cond_1

    .line 339
    sget-object v5, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContactNamePhotoCashe:Ljava/util/HashMap;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v7, 0x1

    const-string v8, ""

    aput-object v8, v6, v7

    const/4 v7, 0x2

    sget-object v8, Lcom/att/mobile/android/vvm/model/Constants;->DEFAULT_AVATAR_IDs:[I

    aget v8, v8, v0

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    :cond_1
    return-void
.end method

.method private static updateDisplayNameFromVoicemailItem(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
    .locals 4
    .param p0, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p1, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 394
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getFormattedDisplayName()Ljava/lang/String;

    move-result-object v0

    .line 395
    .local v0, "displayName":Ljava/lang/String;
    sget-object v1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateDisplayNameFromVoicemailItem displayName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p0, :cond_0

    .line 397
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 399
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract getSelectedCount()I
.end method

.method public getSelectedIdsItems()[Ljava/lang/Long;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectedPhonesItems()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSubTitleTypeFaceForReadState(Z)Landroid/graphics/Typeface;
    .locals 1
    .param p1, "read"    # Z

    .prologue
    .line 427
    if-eqz p1, :cond_0

    sget-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Light:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0
.end method

.method protected getTextColorForReadState(Z)I
    .locals 2
    .param p1, "read"    # Z

    .prologue
    .line 435
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_0

    const v0, 0x7f0e0034

    :goto_0
    invoke-static {v1, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    return v0

    :cond_0
    const v0, 0x7f0e0035

    goto :goto_0
.end method

.method protected getTitleTypeFaceForReadState(Z)Landroid/graphics/Typeface;
    .locals 1
    .param p1, "read"    # Z

    .prologue
    .line 431
    if-eqz p1, :cond_0

    sget-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->Roboto_Medium:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract isSelectedItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040044

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 141
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    invoke-direct {v1, v0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;-><init>(Landroid/view/View;)V

    .line 143
    .local v1, "vh":Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    return-object v1
.end method

.method public selectByIds()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method public setActionModeListener(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;)V
    .locals 0
    .param p1, "actionModeListener"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mActionModeListener:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;

    .line 79
    return-void
.end method

.method protected setDefaultMessage(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z
    .locals 4
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 305
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getUid()J

    move-result-wide v0

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 306
    invoke-static {p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->updateDisplayNameFromVoicemailItem(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    .line 307
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    const v1, 0x7f020100

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    new-instance v1, Lcom/att/mobile/android/infra/utils/PicassoUtils$CircleTransform;

    invoke-direct {v1}, Lcom/att/mobile/android/infra/utils/PicassoUtils$CircleTransform;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 310
    :cond_0
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setEditModeItems(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
    .locals 5
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f0e0036

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 276
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mIsActionModeActive:Z

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 279
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 280
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->isSelectedItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    const v1, 0x7f0200be

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 282
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    const v1, 0x7f0200c9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 283
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const v2, 0x7f0801c3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 302
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 287
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    const v1, 0x7f0200bf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 288
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 289
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 293
    :cond_1
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->isSelectedItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 294
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 295
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 296
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 298
    :cond_2
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 299
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected abstract toggleSelection(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
.end method

.method public turnEditModeOff()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mIsActionModeActive:Z

    .line 154
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->notifyDataSetChanged()V

    .line 155
    return-void
.end method

.method protected updatePhotoAndDisplayName(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;I)Z
    .locals 11
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;
    .param p3, "position"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 346
    sget-object v8, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "updatePhotoAndDisplayName position="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " voicemailItem="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    iget-object v8, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->name:Landroid/widget/TextView;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 349
    sget-object v8, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContactNamePhotoCashe:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 350
    .local v0, "contactNamePhotoArr":[Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 351
    aget-object v3, v0, v7

    .line 352
    .local v3, "displayName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 353
    iget-object v8, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 355
    :cond_0
    aget-object v5, v0, v6

    .line 356
    .local v5, "photoUriStr":Ljava/lang/String;
    const/4 v8, 0x2

    aget-object v2, v0, v8

    .line 357
    .local v2, "defAvatarStr":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 359
    .local v1, "defAvatarId":I
    sget-object v8, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "updatePhotoAndDisplayName displayName="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " photoUriStr="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " defAvatarId="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const/4 v4, 0x0

    .line 361
    .local v4, "photoUri":Landroid/net/Uri;
    iget-object v8, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    if-eqz v8, :cond_1

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 362
    iget-object v7, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v7

    new-instance v8, Lcom/att/mobile/android/infra/utils/PicassoUtils$CircleTransform;

    invoke-direct {v8}, Lcom/att/mobile/android/infra/utils/PicassoUtils$CircleTransform;-><init>()V

    invoke-virtual {v7, v8}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v7

    iget-object v8, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    new-instance v9, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$1;

    invoke-direct {v9, p0, v5, p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$1;-><init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Ljava/lang/String;Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    invoke-virtual {v7, v8, v9}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 374
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 381
    :goto_0
    new-instance v7, Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-direct {v7, v4, v3}, Lcom/att/mobile/android/vvm/model/ContactObject;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {p2, v7}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->setContact(Lcom/att/mobile/android/vvm/model/ContactObject;)V

    .line 390
    .end local v1    # "defAvatarId":I
    .end local v2    # "defAvatarStr":Ljava/lang/String;
    .end local v3    # "displayName":Ljava/lang/String;
    .end local v4    # "photoUri":Landroid/net/Uri;
    .end local v5    # "photoUriStr":Ljava/lang/String;
    :goto_1
    return v6

    .line 375
    .restart local v1    # "defAvatarId":I
    .restart local v2    # "defAvatarStr":Ljava/lang/String;
    .restart local v3    # "displayName":Ljava/lang/String;
    .restart local v4    # "photoUri":Landroid/net/Uri;
    .restart local v5    # "photoUriStr":Ljava/lang/String;
    :cond_1
    iget-object v8, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    if-eqz v8, :cond_2

    if-lez v1, :cond_2

    .line 376
    iget-object v7, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 378
    :cond_2
    invoke-static {p1, p2, v7}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->setDefaultAvatarImage(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;Z)V

    goto :goto_0

    .line 386
    .end local v1    # "defAvatarId":I
    .end local v2    # "defAvatarStr":Ljava/lang/String;
    .end local v3    # "displayName":Ljava/lang/String;
    .end local v4    # "photoUri":Landroid/net/Uri;
    .end local v5    # "photoUriStr":Ljava/lang/String;
    :cond_3
    invoke-static {p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->updateDisplayNameFromVoicemailItem(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    .line 388
    new-instance v6, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;

    invoke-direct {v6, p2, p1, p3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;-><init>(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;I)V

    sget-object v8, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v9, v7, [Ljava/lang/Void;

    invoke-virtual {v6, v8, v9}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v6, v7

    .line 390
    goto :goto_1
.end method

.method protected updateUIAttributesAccordingToReadState(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Z)V
    .locals 6
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "read"    # Z

    .prologue
    .line 403
    sget-object v3, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateUIAttributesAccordingToReadState read="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getTextColorForReadState(Z)I

    move-result v1

    .line 405
    .local v1, "textColor":I
    iget-object v4, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->messageStatusView:Landroid/widget/ImageView;

    if-eqz p2, :cond_3

    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 407
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getTitleTypeFaceForReadState(Z)Landroid/graphics/Typeface;

    move-result-object v2

    .line 408
    .local v2, "titleTypeFace":Landroid/graphics/Typeface;
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getSubTitleTypeFaceForReadState(Z)Landroid/graphics/Typeface;

    move-result-object v0

    .line 410
    .local v0, "subTitleTypeFace":Landroid/graphics/Typeface;
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->name:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 411
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 412
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 415
    :cond_0
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->transcriptionText:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    .line 416
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->transcriptionText:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 417
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->transcriptionText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 420
    :cond_1
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->date:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    .line 421
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 422
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 424
    :cond_2
    return-void

    .line 405
    .end local v0    # "subTitleTypeFace":Landroid/graphics/Typeface;
    .end local v2    # "titleTypeFace":Landroid/graphics/Typeface;
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

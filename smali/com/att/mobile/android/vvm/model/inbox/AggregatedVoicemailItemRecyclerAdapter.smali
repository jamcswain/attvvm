.class public Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;
.super Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;
.source "AggregatedVoicemailItemRecyclerAdapter.java"


# static fields
.field private static final SPACE:Ljava/lang/String; = " "

.field private static final TAG:Ljava/lang/String;


# instance fields
.field message_str:Ljava/lang/String;

.field messages_str:Ljava/lang/String;

.field new_str:Ljava/lang/String;

.field old_str:Ljava/lang/String;

.field protected selectedItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filterType"    # I
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    .line 24
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->new_str:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->old_str:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->message_str:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->messages_str:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    .line 46
    const v0, 0x7f080142

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->new_str:Ljava/lang/String;

    .line 47
    const v0, 0x7f080154

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->old_str:Ljava/lang/String;

    .line 48
    const v0, 0x7f080126

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->message_str:Ljava/lang/String;

    .line 49
    const v0, 0x7f08012d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->messages_str:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private buildMessageCountDescription(II)Ljava/lang/String;
    .locals 4
    .param p1, "newMsgCount"    # I
    .param p2, "oldMsgCount"    # I

    .prologue
    const/4 v3, 0x1

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    .local v0, "buf":Ljava/lang/StringBuilder;
    if-lez p1, :cond_0

    .line 128
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->new_str:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    if-le p1, v3, :cond_2

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->messages_str:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    if-lez p2, :cond_0

    .line 131
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_0
    if-lez p2, :cond_1

    .line 135
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->old_str:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    if-le p2, v3, :cond_3

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->messages_str:Ljava/lang/String;

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_1
    sget-object v1, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "buildMessageCountDescription newMsgCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " oldMsgCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " description="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 129
    :cond_2
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->message_str:Ljava/lang/String;

    goto :goto_0

    .line 136
    :cond_3
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->message_str:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method protected getSelectedCount()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedPhonesItems()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method protected isSelectedItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z
    .locals 2
    .param p1, "item"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 85
    if-nez p1, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 88
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->onBindViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V

    return-void
.end method

.method public onBindViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 64
    new-instance v2, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;

    invoke-direct {v2, p2}, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;-><init>(Landroid/database/Cursor;)V

    .line 65
    .local v2, "voicemailItem":Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 67
    .local v1, "position":I
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->setEditModeItems(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    .line 69
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->setDefaultMessage(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 70
    invoke-virtual {p0, p1, v2, v1}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->updatePhotoAndDisplayName(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 71
    invoke-static {p1, v2, v0}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->setDefaultAvatarImage(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;Z)V

    .line 74
    :cond_0
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->updateTranscription(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;)V

    .line 76
    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->getNewInboxMessagesCount()I

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x1

    .line 77
    .local v0, "isRead":Z
    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->updateUIAttributesAccordingToReadState(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Z)V

    .line 79
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    new-instance v4, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;

    invoke-direct {v4, p0, v2, v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;-><init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    new-instance v4, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;

    invoke-direct {v4, p0, v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;-><init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 81
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/16 v2, 0x8

    .line 54
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    move-result-object v0

    .line 55
    .local v0, "holder":Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    iget-object v1, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 56
    iget-object v1, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->urgentStatus:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 57
    iget-object v1, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    .line 58
    return-object v0
.end method

.method public selectByIds()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method protected toggleSelection(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
    .locals 2
    .param p1, "item"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 99
    :goto_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->notifyDataSetChanged()V

    .line 100
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public turnEditModeOff()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 111
    invoke-super {p0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->turnEditModeOff()V

    .line 112
    return-void
.end method

.method protected updateTranscription(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;)V
    .locals 5
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "aggregatedVoicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;

    .prologue
    .line 116
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->getOldInboxMessagesCount()I

    move-result v1

    .line 117
    .local v1, "oldVMCount":I
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->getNewInboxMessagesCount()I

    move-result v0

    .line 118
    .local v0, "newVMCount":I
    iget-object v2, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->transcriptionText:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1}, Lcom/att/mobile/android/vvm/model/inbox/AggregatedVoicemailItemRecyclerAdapter;->buildMessageCountDescription(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    if-lez v0, :cond_0

    .line 120
    iget-object v2, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->transcriptionText:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 122
    :cond_0
    return-void
.end method

.class public abstract Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "CursorRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<TVH;>;"
    }
.end annotation


# instance fields
.field protected mCursor:Landroid/database/Cursor;

.field protected mDataValid:Z

.field protected mRowIDColumn:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 37
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 38
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->init(Landroid/database/Cursor;)V

    .line 39
    return-void
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 96
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 97
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 98
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 100
    :cond_0
    return-void
.end method

.method public convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 142
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 64
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 69
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mDataValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1, "position"    # I

    .prologue
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    const-wide/16 v0, -0x1

    .line 78
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->hasStableIds()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mDataValid:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    .line 79
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mRowIDColumn:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 85
    :cond_0
    return-wide v0
.end method

.method init(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    const/4 v1, 0x1

    .line 42
    if-eqz p1, :cond_0

    move v0, v1

    .line 43
    .local v0, "cursorPresent":Z
    :goto_0
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    .line 44
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mDataValid:Z

    .line 45
    if-eqz v0, :cond_1

    const-string v2, "_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    :goto_1
    iput v2, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mRowIDColumn:I

    .line 46
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->setHasStableIds(Z)V

    .line 47
    return-void

    .line 42
    .end local v0    # "cursorPresent":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 45
    .restart local v0    # "cursorPresent":Z
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public final onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 3
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    .local p1, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;, "TVH;"
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mDataValid:Z

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this should only be called when the cursor is valid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "couldn\'t move cursor to position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, p1, v0}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V

    .line 59
    return-void
.end method

.method public abstract onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .local p0, "this":Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;, "Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter<TVH;>;"
    const/4 v2, 0x0

    .line 113
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    if-ne p1, v1, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    .line 117
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mCursor:Landroid/database/Cursor;

    .line 118
    if-eqz p1, :cond_1

    .line 119
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mRowIDColumn:I

    .line 120
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mDataValid:Z

    .line 122
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 124
    :cond_1
    const/4 v1, -0x1

    iput v1, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mRowIDColumn:I

    .line 125
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->mDataValid:Z

    .line 127
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->getItemCount()I

    move-result v1

    invoke-virtual {p0, v2, v1}, Lcom/att/mobile/android/vvm/model/inbox/CursorRecyclerAdapter;->notifyItemRangeRemoved(II)V

    goto :goto_0
.end method

.class Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;
.super Ljava/lang/Object;
.source "ListItemCursorRecyclerAdapterBase.java"

# interfaces
.implements Lcom/squareup/picasso/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->onPostExecute(Lcom/att/mobile/android/vvm/model/ContactObject;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;

.field final synthetic val$holder:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

.field final synthetic val$photoUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;Landroid/net/Uri;Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;

    .prologue
    .line 488
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;->val$photoUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;->val$holder:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 3

    .prologue
    .line 496
    invoke-static {}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Picasso onError photoUri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;->val$photoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;->val$holder:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->access$100(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;)Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->setDefaultAvatarImage(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;Z)V

    .line 498
    return-void
.end method

.method public onSuccess()V
    .locals 3

    .prologue
    .line 491
    invoke-static {}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Picasso onSuccess photoUri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;->val$photoUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    return-void
.end method

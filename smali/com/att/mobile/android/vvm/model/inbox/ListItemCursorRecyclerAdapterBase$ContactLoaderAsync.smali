.class public Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;
.super Landroid/os/AsyncTask;
.source "ListItemCursorRecyclerAdapterBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ContactLoaderAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/att/mobile/android/vvm/model/ContactObject;",
        ">;"
    }
.end annotation


# instance fields
.field private imageViewWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mVoiceMailObject:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

.field private position:I


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;I)V
    .locals 1
    .param p1, "voiceMailObject"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;
    .param p2, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p3, "pos"    # I

    .prologue
    .line 444
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 445
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->mVoiceMailObject:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .line 446
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->imageViewWeakReference:Ljava/lang/ref/WeakReference;

    .line 447
    iput p3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->position:I

    .line 448
    return-void
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;)Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->mVoiceMailObject:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    return-object v0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/att/mobile/android/vvm/model/ContactObject;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 453
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->mVoiceMailObject:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    .line 454
    .local v1, "phoneNumber":Ljava/lang/String;
    invoke-static {v1}, Lcom/att/mobile/android/infra/utils/ContactUtils;->getContact(Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v0

    .line 456
    .local v0, "contact":Lcom/att/mobile/android/vvm/model/ContactObject;
    const-string v2, "ContactLoaderAsync"

    const-string v3, "doInBackground()"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 438
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->doInBackground([Ljava/lang/Void;)Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/att/mobile/android/vvm/model/ContactObject;)V
    .locals 8
    .param p1, "contactObject"    # Lcom/att/mobile/android/vvm/model/ContactObject;

    .prologue
    .line 464
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 466
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->imageViewWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    .line 468
    .local v0, "holder":Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    if-eqz p1, :cond_0

    .line 470
    const-string v3, "ContactLoaderAsync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPostExecute contactObject="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhotoUri()Landroid/net/Uri;

    move-result-object v1

    .line 472
    .local v1, "photoUri":Landroid/net/Uri;
    sget-object v4, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContactNamePhotoCashe:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->mVoiceMailObject:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x1

    if-nez v1, :cond_1

    const-string v3, ""

    :goto_0
    aput-object v3, v6, v7

    const/4 v3, 0x2

    const-string v7, "0"

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    iget-object v3, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 475
    .local v2, "positionTag":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->position:I

    if-eq v3, v4, :cond_2

    .line 477
    const-string v3, "ContactLoaderAsync"

    const-string v4, "Object was reused. Don\'t set values."

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    .end local v1    # "photoUri":Landroid/net/Uri;
    .end local v2    # "positionTag":Ljava/lang/Integer;
    :cond_0
    :goto_1
    return-void

    .line 472
    .restart local v1    # "photoUri":Landroid/net/Uri;
    :cond_1
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhotoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 481
    .restart local v2    # "positionTag":Ljava/lang/Integer;
    :cond_2
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->mVoiceMailObject:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v3, p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->setContact(Lcom/att/mobile/android/vvm/model/ContactObject;)V

    .line 483
    const-string v3, "ContactLoaderAsync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "holder="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    if-eqz v0, :cond_0

    .line 485
    iget-object v3, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    if-eqz v3, :cond_3

    if-eqz v1, :cond_3

    .line 486
    const-string v3, "ContactLoaderAsync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "load photoUri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    iget-object v3, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/squareup/picasso/Picasso;->load(Landroid/net/Uri;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v3

    new-instance v4, Lcom/att/mobile/android/infra/utils/PicassoUtils$CircleTransform;

    invoke-direct {v4}, Lcom/att/mobile/android/infra/utils/PicassoUtils$CircleTransform;-><init>()V

    invoke-virtual {v3, v4}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v3

    iget-object v4, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->photo:Landroid/widget/ImageView;

    new-instance v5, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;

    invoke-direct {v5, p0, v1, v0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync$1;-><init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;Landroid/net/Uri;Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;)V

    invoke-virtual {v3, v4, v5}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 501
    :cond_3
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->mVoiceMailObject:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-static {v0, v3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->access$200(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 438
    check-cast p1, Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ContactLoaderAsync;->onPostExecute(Lcom/att/mobile/android/vvm/model/ContactObject;)V

    return-void
.end method

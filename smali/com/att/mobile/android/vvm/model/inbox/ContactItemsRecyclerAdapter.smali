.class public Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;
.super Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;
.source "ContactItemsRecyclerAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filterType"    # I
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    .line 26
    return-void
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->onBindViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V

    return-void
.end method

.method public onBindViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 42
    new-instance v2, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;

    invoke-direct {v2, p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;-><init>(Landroid/database/Cursor;)V

    .line 43
    .local v2, "voicemailItem":Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;
    sget-object v3, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onBindViewHolder voicemailItem="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 46
    .local v0, "position":I
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->setEditModeItems(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    .line 48
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->updateTranscription(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V

    .line 49
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->updateDate(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V

    .line 51
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->isReadListItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)Z

    move-result v1

    .line 52
    .local v1, "readState":Z
    invoke-virtual {p0, p1, v1}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->updateUIAttributesAccordingToReadState(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Z)V

    .line 54
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    new-instance v4, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;

    invoke-direct {v4, p0, v2, v0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;-><init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    new-instance v4, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;

    invoke-direct {v4, p0, v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;-><init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 57
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040043

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    invoke-direct {v1, v0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;-><init>(Landroid/view/View;)V

    .line 35
    .local v1, "vh":Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    return-object v1
.end method

.method protected setEditModeItems(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
    .locals 3
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 62
    iget-object v1, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->mIsActionModeActive:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 63
    invoke-virtual {p0, p2}, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->isSelectedItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    const v1, 0x7f0200be

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 65
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    const v1, 0x7f0200c9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 66
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ContactItemsRecyclerAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0036

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 72
    :goto_1
    return-void

    .line 62
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    const v1, 0x7f0200bf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 69
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->selectedImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 70
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1
.end method

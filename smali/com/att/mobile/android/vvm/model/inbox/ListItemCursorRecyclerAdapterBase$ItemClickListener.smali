.class public Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;
.super Ljava/lang/Object;
.source "ListItemCursorRecyclerAdapterBase.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ItemClickListener"
.end annotation


# instance fields
.field private itemPosition:I

.field private mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

.field final synthetic this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;
    .param p3, "position"    # I

    .prologue
    .line 194
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .line 196
    iput p3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->itemPosition:I

    .line 197
    return-void
.end method

.method private openAggregatedScreen()V
    .locals 6

    .prologue
    .line 234
    invoke-static {}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "openAggregatedScreen() displayName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getFormattedDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " phoneNumber="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const-class v4, Lcom/att/mobile/android/vvm/screen/AggregatedActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    .local v2, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getFormattedDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 237
    const-string v3, "intent.data.user.name"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getFormattedDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    :cond_0
    const-string v3, "intent.data.user.phone"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    const-string v3, "intent.data.filter.type"

    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget v4, v4, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mFilterType:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 242
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getContact()Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v0

    .line 243
    .local v0, "contact":Lcom/att/mobile/android/vvm/model/ContactObject;
    if-eqz v0, :cond_1

    .line 244
    invoke-static {}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "openAggregatedScreen() contact="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/ContactObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhotoUri()Landroid/net/Uri;

    move-result-object v1

    .line 246
    .local v1, "contactPhoto":Landroid/net/Uri;
    if-eqz v1, :cond_1

    .line 247
    const-string v3, "intent.data.user.photo.uri"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 251
    .end local v1    # "contactPhoto":Landroid/net/Uri;
    :cond_1
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 252
    return-void
.end method

.method private openPlayerScreen()V
    .locals 6

    .prologue
    .line 256
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const-class v3, Lcom/att/mobile/android/vvm/screen/PlayerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 258
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getContact()Lcom/att/mobile/android/vvm/model/ContactObject;

    move-result-object v0

    .line 259
    .local v0, "contact":Lcom/att/mobile/android/vvm/model/ContactObject;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhotoUri()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 260
    const-string v2, "contactUri"

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/ContactObject;->getPhotoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    const-string v2, "senderDisplayName"

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/ContactObject;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    :cond_0
    const-string v2, "id"

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getId()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 265
    const-string v2, "filterType"

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget v3, v3, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mFilterType:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 266
    const-string v2, "launchedFromInbox"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 267
    const-string v2, "messagePosition"

    iget v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->itemPosition:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 269
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 271
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 201
    invoke-static {}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick() displayName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getFormattedDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " phoneNumber="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getPhoneNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-boolean v1, v1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mIsActionModeActive:Z

    if-eqz v1, :cond_2

    .line 205
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->toggleSelection(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    .line 207
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->isSelectedItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const v2, 0x7f0801c3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/att/mobile/android/infra/utils/AccessibilityUtils;->sendEvent(Ljava/lang/String;Landroid/view/View;)V

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const v2, 0x7f0801ec

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/att/mobile/android/infra/utils/AccessibilityUtils;->sendEvent(Ljava/lang/String;Landroid/view/View;)V

    .line 212
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->getSelectedCount()I

    move-result v1

    if-gtz v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v1, v1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mActionModeListener:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;

    invoke-interface {v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;->onEmptySelection()V

    goto :goto_0

    .line 219
    :cond_2
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    instance-of v1, v1, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;

    if-eqz v1, :cond_3

    .line 220
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    check-cast v0, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;

    .line 221
    .local v0, "aggregatedData":Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->getNewInboxMessagesCount()I

    move-result v1

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->getOldInboxMessagesCount()I

    move-result v2

    add-int/2addr v1, v2

    const/4 v2, 0x1

    if-le v1, v2, :cond_3

    .line 222
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->openAggregatedScreen()V

    goto :goto_0

    .line 227
    .end local v0    # "aggregatedData":Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;
    :cond_3
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;->openPlayerScreen()V

    goto :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;
.super Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;
.source "VoicemailItemRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter$fileStatusDisplay;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private rotateAnimation:Landroid/view/animation/RotateAnimation;

.field protected selectedItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "filterType"    # I
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    .line 45
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->initRotateAnimation()V

    .line 46
    return-void
.end method

.method private initRotateAnimation()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    .line 49
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    .line 50
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    const-wide/16 v2, 0x4b0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 51
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 52
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 53
    return-void
.end method

.method private startGauge(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;)V
    .locals 2
    .param p1, "viewHolder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    .prologue
    .line 149
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    const v1, 0x7f0f011d

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->downloadFileGauge:Landroid/widget/ImageView;

    .line 150
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->downloadFileGauge:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 152
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->downloadFileGauge:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->rotateAnimation:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 153
    return-void
.end method

.method private stopGauge(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;)V
    .locals 3
    .param p1, "viewHolder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    .prologue
    const/4 v2, 0x0

    .line 160
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->downloadFileGauge:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->downloadFileGauge:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 162
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->downloadFileGauge:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 163
    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->downloadFileGauge:Landroid/widget/ImageView;

    .line 165
    :cond_0
    iput-object v2, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->downloadFileGauge:Landroid/widget/ImageView;

    .line 166
    return-void
.end method


# virtual methods
.method protected getSelectedCount()I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedIdsItems()[Ljava/lang/Long;
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Long;

    return-object v0
.end method

.method protected isReadListItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)Z
    .locals 2
    .param p1, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;

    .prologue
    const/4 v0, 0x1

    .line 102
    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->getReadState()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 105
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isSelectedItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z
    .locals 4
    .param p1, "item"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 170
    if-nez p1, :cond_0

    .line 171
    const/4 v0, 0x0

    .line 173
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->onBindViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V

    return-void
.end method

.method public onBindViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 58
    new-instance v2, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;

    invoke-direct {v2, p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;-><init>(Landroid/database/Cursor;)V

    .line 59
    .local v2, "voicemailItem":Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;
    sget-object v3, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onBindViewHolder voicemailItem="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 62
    .local v0, "position":I
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->setEditModeItems(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    .line 64
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->setDefaultMessage(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    invoke-virtual {p0, p1, v2, v0}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->updatePhotoAndDisplayName(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 66
    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->setDefaultAvatarImage(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;Z)V

    .line 69
    :cond_0
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->updateTranscription(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V

    .line 70
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->updateDate(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V

    .line 72
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->updateSaveOrDownloadFlipperAndAnimation(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V

    .line 73
    invoke-virtual {p0, p1, v2}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->updateUrgentStatus(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V

    .line 75
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->isReadListItem(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)Z

    move-result v1

    .line 76
    .local v1, "readState":Z
    invoke-virtual {p0, p1, v1}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->updateUIAttributesAccordingToReadState(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Z)V

    .line 78
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    new-instance v4, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;

    invoke-direct {v4, p0, v2, v0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemClickListener;-><init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->parent:Landroid/view/View;

    new-instance v4, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;

    invoke-direct {v4, p0, v2}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;-><init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 80
    return-void
.end method

.method protected toggleSelection(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
    .locals 4
    .param p1, "item"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 184
    :goto_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->notifyDataSetChanged()V

    .line 185
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public turnEditModeOff()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->selectedItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 196
    invoke-super {p0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->turnEditModeOff()V

    .line 197
    return-void
.end method

.method protected updateDate(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V
    .locals 5
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;

    .prologue
    .line 96
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->getTime()J

    move-result-wide v2

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v2, v3, v1, v4}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->getFriendlyDate(JLandroid/content/Context;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "time":Ljava/lang/String;
    iget-object v1, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    return-void
.end method

.method protected updateSaveOrDownloadFlipperAndAnimation(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V
    .locals 6
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;

    .prologue
    const/4 v2, 0x1

    const/16 v5, 0x8

    const/4 v3, 0x0

    .line 120
    iget-object v4, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v4, v5}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    .line 124
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->getSavedState()I

    move-result v4

    if-ne v4, v5, :cond_2

    move v1, v2

    .line 125
    .local v1, "isError":Z
    :goto_0
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->getFileName()Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "fileName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    .line 128
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->isMemoryLow()Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v1, :cond_3

    .line 129
    :cond_1
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->stopGauge(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;)V

    .line 130
    iget-object v4, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v4, v3}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    .line 131
    iget-object v3, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 146
    :goto_1
    return-void

    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "isError":Z
    :cond_2
    move v1, v3

    .line 124
    goto :goto_0

    .line 133
    .restart local v0    # "fileName":Ljava/lang/String;
    .restart local v1    # "isError":Z
    :cond_3
    iget-object v2, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2, v3}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    .line 134
    iget-object v2, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2, v3}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 138
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->startGauge(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;)V

    goto :goto_1

    .line 142
    :cond_4
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->stopGauge(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;)V

    .line 144
    iget-object v2, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->savedOrDownloadFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2, v5}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    goto :goto_1
.end method

.method protected updateTranscription(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V
    .locals 3
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;

    .prologue
    .line 85
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->getTranscription()Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "transcription":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/VoicemailItemRecyclerAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f08014b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 90
    :cond_1
    iget-object v1, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->transcriptionText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method

.method protected updateUrgentStatus(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;)V
    .locals 2
    .param p1, "holder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "voicemailItem"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;

    .prologue
    .line 110
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->getIsUrgent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->urgentStatus:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p1, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;->urgentStatus:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;
.super Ljava/lang/Object;
.source "ListItemCursorRecyclerAdapterBase.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ItemLongClickListener"
.end annotation


# instance fields
.field mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

.field final synthetic this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;
    .param p2, "item"    # Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    .line 163
    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 168
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-boolean v0, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mIsActionModeActive:Z

    if-eqz v0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return v2

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iput-boolean v2, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mIsActionModeActive:Z

    .line 174
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mActionModeListener:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->toggleSelection(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    .line 178
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mActionModeListener:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->mItem:Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;

    invoke-interface {v0, v1}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ActionModeListener;->onItemLongClick(Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;)V

    .line 180
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    iget-object v0, v0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->mContext:Landroid/content/Context;

    const v1, 0x7f0801c3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/att/mobile/android/infra/utils/AccessibilityUtils;->sendEvent(Ljava/lang/String;Landroid/view/View;)V

    .line 182
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ItemLongClickListener;->this$0:Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase;->notifyDataSetChanged()V

    goto :goto_0
.end method

.class public interface abstract Lcom/att/mobile/android/vvm/model/Constants$METADATA_VARIABLES;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "METADATA_VARIABLES"
.end annotation


# static fields
.field public static final AudioAcceptTypes:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/AudioAcceptTypes"

.field public static final ChangeableGreetings:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/ChangeableGreetings"

.field public static final ClientID:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/ClientID"

.field public static final EAGStatus:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/EAGStatus"

.field public static final GreetingType:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/GreetingType"

.field public static final GreetingTypesAllowed:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/GreetingTypesAllowed"

.field public static final GreetingsExtendedAbsence:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/Greetings/ExtendedAbsence"

.field public static final GreetingsPersonal:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/Greetings/Personal"

.field public static final GreetingsPersonalBusy:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/Greetings/PersonalBusy"

.field public static final GreetingsPersonalNoAnswer:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/Greetings/PersonalNoAnswer"

.field public static final GreetingsStandartWithName:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/Greetings/StandartWithName"

.field public static final GreetingsStandartWithNumber:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/Greetings/StandartWithNumber"

.field public static final MaxGreetingLength:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/MaxGreetingLength"

.field public static final MaxMessageLength:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/MaxMessageLength"

.field public static final MaxPasswordDigits:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/MaxPasswordDigits"

.field public static final MaxRecordedNameLength:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/MaxRecordedNameLength"

.field public static final MinMessageLength:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/MinMessageLength"

.field public static final MinPasswordDigits:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/MinPasswordDigits"

.field public static final RecordedName:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/RecordedName"

.field public static final TUIPassword:Ljava/lang/String; = "/private/vendor/vendor.alu/messaging/TUIPassword"

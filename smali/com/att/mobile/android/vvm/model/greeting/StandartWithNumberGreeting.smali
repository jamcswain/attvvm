.class public Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;
.super Lcom/att/mobile/android/vvm/model/greeting/Greeting;
.source "StandartWithNumberGreeting.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;Ljava/lang/Boolean;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isChangeable"    # Ljava/lang/Boolean;
    .param p3, "isSelected"    # Ljava/lang/Boolean;
    .param p4, "maxRecordTime"    # I

    .prologue
    const/4 v1, 0x0

    .line 12
    invoke-direct {p0, p2, p3, p4}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;I)V

    .line 14
    sget-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->StandardWithNumber:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    .line 15
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->convertOriginalTypeToSimpleType(Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;->displayName:Ljava/lang/String;

    .line 16
    const v0, 0x7f08005d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;->description:Ljava/lang/String;

    .line 17
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;->isChangeable()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "/private/vendor/vendor.alu/messaging/Greetings/StandartWithNumber"

    :goto_0
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;->imapSelectionVariable:Ljava/lang/String;

    .line 18
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;->isChangeable()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "/private/vendor/vendor.alu/messaging/Greetings/StandartWithNumber"

    :cond_0
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;->imapRecordingVariable:Ljava/lang/String;

    .line 19
    return-void

    :cond_1
    move-object v0, v1

    .line 17
    goto :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;
.super Ljava/lang/Object;
.source "GreetingFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;
    }
.end annotation


# static fields
.field public static CUSTOM:Ljava/lang/String; = null

.field public static NAME:Ljava/lang/String; = null

.field public static PERSONAL:Ljava/lang/String; = null

.field public static RECORD_NAME:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "GreetingFactory"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "Personal"

    sput-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->PERSONAL:Ljava/lang/String;

    .line 25
    const-string v0, "RecordedName"

    sput-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->RECORD_NAME:Ljava/lang/String;

    .line 26
    const-string v0, "Custom"

    sput-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->CUSTOM:Ljava/lang/String;

    .line 27
    const-string v0, "Name"

    sput-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertOriginalTypeToSimpleType(Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;)Ljava/lang/String;
    .locals 3
    .param p0, "originalType"    # Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    .prologue
    .line 149
    sget-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$1;->$SwitchMap$com$att$mobile$android$vvm$model$greeting$GreetingFactory$SUPPORTED_GREETING_TYPES:[I

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 157
    const-string v0, "GreetingFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The following greeting type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 151
    :pswitch_0
    const-string v0, "Default"

    goto :goto_0

    .line 153
    :pswitch_1
    const-string v0, "Name"

    goto :goto_0

    .line 155
    :pswitch_2
    const-string v0, "Custom"

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static convertSimpleTypeToOriginalType(Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;
    .locals 3
    .param p0, "originalType"    # Ljava/lang/String;

    .prologue
    .line 163
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->Personal:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    sget-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->Personal:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    .line 171
    :goto_0
    return-object v0

    .line 165
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->StandardWithName:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    sget-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->StandardWithName:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    goto :goto_0

    .line 167
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->StandardWithNumber:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 168
    sget-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->StandardWithNumber:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    goto :goto_0

    .line 170
    :cond_2
    const-string v0, "GreetingFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The following greeting type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static createGreeting(Landroid/content/Context;Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;Ljava/lang/Boolean;Ljava/lang/Boolean;I)Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "originalType"    # Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;
    .param p2, "isChangeable"    # Ljava/lang/Boolean;
    .param p3, "isSelected"    # Ljava/lang/Boolean;
    .param p4, "maxRecordTime"    # I

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 87
    .local v0, "retVal":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    sget-object v1, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$1;->$SwitchMap$com$att$mobile$android$vvm$model$greeting$GreetingFactory$SUPPORTED_GREETING_TYPES:[I

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 100
    :goto_0
    return-object v0

    .line 89
    :pswitch_0
    new-instance v0, Lcom/att/mobile/android/vvm/model/greeting/PersonalGreeting;

    .end local v0    # "retVal":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    invoke-direct {v0, p0, p2, p3, p4}, Lcom/att/mobile/android/vvm/model/greeting/PersonalGreeting;-><init>(Landroid/content/Context;Ljava/lang/Boolean;Ljava/lang/Boolean;I)V

    .line 90
    .restart local v0    # "retVal":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    goto :goto_0

    .line 92
    :pswitch_1
    new-instance v0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;

    .end local v0    # "retVal":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    invoke-direct {v0, p0, p3, p4}, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;-><init>(Landroid/content/Context;Ljava/lang/Boolean;I)V

    .line 93
    .restart local v0    # "retVal":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    goto :goto_0

    .line 95
    :pswitch_2
    new-instance v0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;

    .end local v0    # "retVal":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    invoke-direct {v0, p0, p2, p3, p4}, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNumberGreeting;-><init>(Landroid/content/Context;Ljava/lang/Boolean;Ljava/lang/Boolean;I)V

    .line 96
    .restart local v0    # "retVal":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static createGreetings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "allowedGreetings"    # Ljava/lang/String;
    .param p2, "changeableGreetings"    # Ljava/lang/String;
    .param p3, "selectedGreeting"    # Ljava/lang/String;
    .param p4, "maxCustomRecording"    # Ljava/lang/String;
    .param p5, "maxRecordedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v2, "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    const-string v6, "GreetingFactory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GreetingFactory::createGreetings - Allowed Greetings => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    const-string v6, "GreetingFactory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GreetingFactory::createGreetings - Changeable Greetings => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    const-string v6, "GreetingFactory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GreetingFactory::createGreetings - Selected Greeting => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const-string v6, "GreetingFactory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GreetingFactory::createGreetings - Max Custom Record Time => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const-string v6, "GreetingFactory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GreetingFactory::createGreetings - Max Name Record Time => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    if-eqz p1, :cond_2

    .line 51
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v6, ","

    invoke-direct {v3, p1, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .local v3, "st":Ljava/util/StringTokenizer;
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p3

    .line 55
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 56
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 58
    .local v4, "tmpVal":Ljava/lang/String;
    :try_start_0
    invoke-static {v4}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->convertSimpleTypeToOriginalType(Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    move-result-object v5

    .line 59
    .local v5, "type":Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;
    const/4 v1, 0x0

    .line 60
    .local v1, "g":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    sget-object v6, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->StandardWithName:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    if-ne v5, v6, :cond_1

    .line 61
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 62
    invoke-virtual {p5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 61
    invoke-static {p0, v5, v6, v7, v8}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->createGreeting(Landroid/content/Context;Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;Ljava/lang/Boolean;Ljava/lang/Boolean;I)Lcom/att/mobile/android/vvm/model/greeting/Greeting;

    move-result-object v1

    .line 68
    :goto_1
    if-eqz v1, :cond_0

    .line 69
    const-string v6, "GreetingFactory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Greeting of type "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " was added to list."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 72
    .end local v1    # "g":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    .end local v5    # "type":Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "GreetingFactory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported greeting type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 64
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "g":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    .restart local v5    # "type":Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;
    :cond_1
    :try_start_1
    invoke-static {v4, p2}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->isGreetingChangeable(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 65
    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 64
    invoke-static {p0, v5, v6, v7, v8}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->createGreeting(Landroid/content/Context;Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;Ljava/lang/Boolean;Ljava/lang/Boolean;I)Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1

    .line 79
    .end local v1    # "g":Lcom/att/mobile/android/vvm/model/greeting/Greeting;
    .end local v3    # "st":Ljava/util/StringTokenizer;
    .end local v4    # "tmpVal":Ljava/lang/String;
    .end local v5    # "type":Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;
    :cond_2
    return-object v2
.end method

.method private static isGreetingChangeable(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 7
    .param p0, "greetingSimple"    # Ljava/lang/String;
    .param p1, "changeableGreetings"    # Ljava/lang/String;

    .prologue
    .line 113
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v1, "changeableGreetingsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 116
    .local v2, "isChangeable":Ljava/lang/Boolean;
    if-eqz p0, :cond_3

    if-eqz p1, :cond_3

    .line 117
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, ","

    invoke-direct {v3, p1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .local v3, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 119
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 126
    .local v0, "cGreeting":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 128
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 136
    .end local v0    # "cGreeting":Ljava/lang/String;
    :cond_2
    return-object v2

    .line 133
    .end local v3    # "st":Ljava/util/StringTokenizer;
    :cond_3
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "Greeting type cannot be null. You must specify a valid value of greeting types."

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

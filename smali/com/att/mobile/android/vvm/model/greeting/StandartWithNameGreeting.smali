.class public Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;
.super Lcom/att/mobile/android/vvm/model/greeting/Greeting;
.source "StandartWithNameGreeting.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isSelected"    # Ljava/lang/Boolean;
    .param p3, "maxRecordTime"    # I

    .prologue
    .line 13
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;I)V

    .line 15
    sget-object v0, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->StandardWithName:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    .line 16
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory;->convertOriginalTypeToSimpleType(Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;->displayName:Ljava/lang/String;

    .line 17
    const v0, 0x7f08005e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;->description:Ljava/lang/String;

    .line 18
    const-string v0, "/private/vendor/vendor.alu/messaging/Greetings/StandartWithName"

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;->imapSelectionVariable:Ljava/lang/String;

    .line 19
    const-string v0, "/private/vendor/vendor.alu/messaging/RecordedName"

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/StandartWithNameGreeting;->imapRecordingVariable:Ljava/lang/String;

    .line 20
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/model/greeting/Greeting;
.super Ljava/lang/Object;
.source "Greeting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected description:Ljava/lang/String;

.field protected displayName:Ljava/lang/String;

.field private hasExistingRecording:Z

.field protected imapRecordingVariable:Ljava/lang/String;

.field protected imapSelectionVariable:Ljava/lang/String;

.field private isChangeable:Ljava/lang/Boolean;

.field private isSelected:Ljava/lang/Boolean;

.field private maxAllowedRecordTime:I

.field protected originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

.field protected uniqueId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    new-instance v0, Lcom/att/mobile/android/vvm/model/greeting/Greeting$1;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/model/greeting/Greeting$1;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    .line 17
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->displayName:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->description:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->hasExistingRecording:Z

    .line 37
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->readFromParcel(Landroid/os/Parcel;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;I)V
    .locals 2
    .param p1, "isChangeable"    # Ljava/lang/Boolean;
    .param p2, "isSelected"    # Ljava/lang/Boolean;
    .param p3, "maxRecordTime"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    .line 17
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->displayName:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->description:Ljava/lang/String;

    .line 24
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->hasExistingRecording:Z

    .line 28
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isChangeable:Ljava/lang/Boolean;

    .line 29
    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isSelected:Ljava/lang/Boolean;

    .line 32
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isChangeable:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .end local p3    # "maxRecordTime":I
    :goto_0
    iput p3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->maxAllowedRecordTime:I

    .line 33
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->uniqueId:Ljava/lang/String;

    .line 34
    return-void

    .restart local p3    # "maxRecordTime":I
    :cond_0
    move p3, v0

    .line 32
    goto :goto_0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 112
    const/4 v3, 0x2

    new-array v0, v3, [Z

    .line 113
    .local v0, "boolsArr":[Z
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 115
    aget-boolean v3, v0, v2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isChangeable:Ljava/lang/Boolean;

    .line 116
    aget-boolean v3, v0, v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isSelected:Ljava/lang/Boolean;

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->valueOf(Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->displayName:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->description:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->maxAllowedRecordTime:I

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->uniqueId:Ljava/lang/String;

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->imapSelectionVariable:Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->imapRecordingVariable:Ljava/lang/String;

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v1, :cond_0

    :goto_0
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->hasExistingRecording:Z

    .line 125
    return-void

    :cond_0
    move v1, v2

    .line 124
    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public getDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getImapRecordingVariable()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->imapRecordingVariable:Ljava/lang/String;

    return-object v0
.end method

.method public getImapSelectionVariable()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->imapSelectionVariable:Ljava/lang/String;

    return-object v0
.end method

.method public getIsSelected()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isSelected:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getMaxAllowedRecordTime()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->maxAllowedRecordTime:I

    return v0
.end method

.method public getOriginalType()Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    return-object v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->uniqueId:Ljava/lang/String;

    return-object v0
.end method

.method public hasExistingRecording()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->hasExistingRecording:Z

    return v0
.end method

.method public isChangeable()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isChangeable:Ljava/lang/Boolean;

    return-object v0
.end method

.method public setExistingRecording(Z)V
    .locals 0
    .param p1, "hasExistingRecording"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->hasExistingRecording:Z

    .line 82
    return-void
.end method

.method public setIsSelected(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "isSelected"    # Ljava/lang/Boolean;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isSelected:Ljava/lang/Boolean;

    .line 50
    return-void
.end method

.method public setMaxAllowedRecordTime(I)V
    .locals 0
    .param p1, "maxAllowedRecordTime"    # I

    .prologue
    .line 89
    iput p1, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->maxAllowedRecordTime:I

    .line 90
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 100
    const/4 v2, 0x2

    new-array v2, v2, [Z

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isChangeable:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    aput-boolean v3, v2, v1

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->isSelected:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    aput-boolean v3, v2, v0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 101
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->originalType:Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/model/greeting/GreetingFactory$SUPPORTED_GREETING_TYPES;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->description:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget v2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->maxAllowedRecordTime:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->uniqueId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->imapSelectionVariable:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->imapRecordingVariable:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/model/greeting/Greeting;->hasExistingRecording:Z

    if-eqz v2, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    return-void

    :cond_0
    move v0, v1

    .line 108
    goto :goto_0
.end method

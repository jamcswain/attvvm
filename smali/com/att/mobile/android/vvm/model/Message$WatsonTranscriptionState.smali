.class public interface abstract Lcom/att/mobile/android/vvm/model/Message$WatsonTranscriptionState;
.super Ljava/lang/Object;
.source "Message.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Message;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WatsonTranscriptionState"
.end annotation


# static fields
.field public static final DEFAULT:I = 0x0

.field public static final PENDING_RETRY:I = 0x7

.field public static final PENDING_WAIT_FOR_RETRY:I = 0x6

.field public static final RETRY:I = 0x2

.field public static final TRANSCRIPTION_FAILED:I = 0x4

.field public static final TRANSCRIPTION_RECEIVED:I = 0x3

.field public static final WAIT_FOR_RETRY:I = 0x5

.field public static final WAIT_FOR_TRANSCRIPTION:I = 0x1

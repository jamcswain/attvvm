.class public Lcom/att/mobile/android/vvm/model/db/ModelManager;
.super Ljava/lang/Object;
.source "ModelManager.java"

# interfaces
.implements Lcom/att/mobile/android/vvm/control/IEventDispatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;,
        Lcom/att/mobile/android/vvm/model/db/ModelManager$Inbox;
    }
.end annotation


# static fields
.field public static final AGGREGATED_VM:[Ljava/lang/String;

.field public static final AND_NOT_DELETED:Ljava/lang/String;

.field public static final COLUMNS_VM_LIST:[Ljava/lang/String;

.field private static final DATABASE_NAME:Ljava/lang/String; = "messages"

.field public static final DATABASE_TABLE_INBOX:Ljava/lang/String; = "inbox"

.field public static final DB_VERSION_1:I = 0x1

.field public static final DB_VERSION_2:I = 0x2

.field public static final IND_AGGREGATED_READ_COUNT:I = 0x3

.field public static final IND_AGGREGATED_UNREAD_COUNT:I = 0x4

.field public static final NO_TRANSCRIPTION_STRING:Ljava/lang/String; = " "

.field private static final TABLE_INBOX_CREATE_STATEMENT:Ljava/lang/String;

.field private static final TABLE_INBOX_QUERY_AGGREGATED:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ModelManager"

.field public static final WHERE_MARKED_AS_DELETE:Ljava/lang/String;

.field public static final WHERE_PHONE_NUMBER_AND_SAVED:Ljava/lang/String;

.field public static final WHERE_PHONE_NUMBER_AND_SAVED_NOT:Ljava/lang/String;

.field public static final WHERE_SAVED:Ljava/lang/String;

.field public static final WHERE_SAVED_NOT:Ljava/lang/String;

.field private static final allMessagesCountQuery:Ljava/lang/String; = "SELECT COUNT(*) FROM inbox"

.field private static instance:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private static lock:Ljava/lang/Object;


# instance fields
.field private context:Landroid/content/Context;

.field private db:Landroid/database/sqlite/SQLiteDatabase;

.field private dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

.field private greetingList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;"
        }
    .end annotation
.end field

.field private helper:Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;

.field private messageToMarkAsReadUIDs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private metadataHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public pendingDeletesFilename:Ljava/lang/String;

.field public pendingReadsFilename:Ljava/lang/String;

.field private prefs:Landroid/content/SharedPreferences;

.field private prefsEditor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE TABLE IF NOT EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "inbox"

    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INTEGER PRIMARY KEY AUTOINCREMENT, "

    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "uid"

    .line 91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " BIGINT UNIQUE NOT NULL, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "time"

    .line 92
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INTEGER NOT NULL, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "phone_number"

    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " VARCHAR(20), "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "transcription"

    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "file_name"

    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "saved_state"

    .line 96
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT DEFAULT 0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "forward_state"

    .line 97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT DEFAULT 0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "tuiskipped"

    .line 98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT DEFAULT 0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "urgent"

    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT DEFAULT 0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "delivery_status"

    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT DEFAULT 0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "was_downloaded"

    .line 101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT DEFAULT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 102
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "can_overwrite"

    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT DEFAULT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 104
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "read_state"

    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT NOT NULL,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "watson_transcription"

    .line 106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INT DEFAULT 0);"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->TABLE_INBOX_CREATE_STATEMENT:Ljava/lang/String;

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT t.*, ss.count AS count FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "inbox"

    .line 119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " t "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "INNER JOIN (SELECT "

    .line 120
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    .line 121
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "uid"

    .line 122
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "time"

    .line 123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "phone_number"

    .line 124
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "transcription"

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "file_name"

    .line 126
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "read_state"

    .line 127
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "saved_state"

    .line 128
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "forward_state"

    .line 129
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "urgent"

    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "delivery_status"

    .line 131
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MAX("

    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS maxdate, COUNT("

    .line 133
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS count "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    FROM "

    .line 134
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "inbox"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    GROUP BY "

    .line 135
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) ss ON t."

    .line 136
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ss."

    .line 137
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND t."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ss.maxdate "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "GROUP BY t."

    .line 138
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY t."

    .line 139
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->TABLE_INBOX_QUERY_AGGREGATED:Ljava/lang/String;

    .line 143
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "uid"

    aput-object v1, v0, v5

    const-string v1, "time"

    aput-object v1, v0, v4

    const-string v1, "phone_number"

    aput-object v1, v0, v6

    const-string v1, "transcription"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "file_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "read_state"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "saved_state"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "forward_state"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "urgent"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "delivery_status"

    aput-object v2, v0, v1

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->COLUMNS_VM_LIST:[Ljava/lang/String;

    .line 152
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "uid"

    aput-object v1, v0, v5

    const-string v1, "phone_number"

    aput-object v1, v0, v4

    const-string v1, "count_read"

    aput-object v1, v0, v6

    const-string v1, "count_unread"

    aput-object v1, v0, v7

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AGGREGATED_VM:[Ljava/lang/String;

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "forward_state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "saved_state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_PHONE_NUMBER_AND_SAVED:Ljava/lang/String;

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "saved_state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_PHONE_NUMBER_AND_SAVED_NOT:Ljava/lang/String;

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "saved_state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_SAVED:Ljava/lang/String;

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "saved_state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_SAVED_NOT:Ljava/lang/String;

    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "forward_state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_MARKED_AS_DELETE:Ljava/lang/String;

    .line 173
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->lock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->metadataHashMap:Ljava/util/HashMap;

    .line 171
    new-instance v0, Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    .line 177
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 183
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->messageToMarkAsReadUIDs:Ljava/util/Set;

    .line 186
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->greetingList:Ljava/util/ArrayList;

    .line 215
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    .line 216
    new-instance v0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;

    invoke-direct {v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->helper:Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;

    .line 217
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->metadataHashMap:Ljava/util/HashMap;

    .line 218
    const-string v0, "vvmPreferences"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefs:Landroid/content/SharedPreferences;

    .line 219
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->open()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 221
    const v0, 0x7f080169

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "deletes.ser"

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->pendingDeletesFilename:Ljava/lang/String;

    .line 223
    const v0, 0x7f08016a

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "reads.ser"

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->pendingReadsFilename:Ljava/lang/String;

    .line 231
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->TABLE_INBOX_CREATE_STATEMENT:Ljava/lang/String;

    return-object v0
.end method

.method private addShortcut()V
    .locals 8

    .prologue
    .line 3140
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 3141
    .local v0, "context":Landroid/content/Context;
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->uninstallShortcutBrute(Landroid/content/Context;)V

    .line 3146
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3147
    .local v2, "shortcutIntent":Landroid/content/Intent;
    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/att/mobile/android/vvm/screen/WelcomeActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 3148
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 3153
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 3154
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3155
    const-string v3, "android.intent.extra.shortcut.NAME"

    const v4, 0x7f0800a3

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3156
    const-string v3, "android.intent.extra.shortcut.ICON_RESOURCE"

    const v4, 0x7f0200fe

    invoke-static {v0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3158
    const-string v3, "duplicate"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3161
    const-string v3, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3162
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3163
    const-string v3, "ModelManager"

    const-string v4, "uninstallShortcut - ours"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3166
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/att/mobile/android/vvm/model/db/ModelManager$1;

    invoke-direct {v4, p0, v1, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager$1;-><init>(Lcom/att/mobile/android/vvm/model/db/ModelManager;Landroid/content/Intent;Landroid/content/Context;)V

    const-wide/16 v6, 0x1388

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3174
    return-void
.end method

.method private buildAndNotSaved()Ljava/lang/String;
    .locals 1

    .prologue
    .line 737
    const-string v0, " And saved_state <> 4"

    return-object v0
.end method

.method private buildAndSaved()Ljava/lang/String;
    .locals 1

    .prologue
    .line 740
    const-string v0, " And saved_state = 4"

    return-object v0
.end method

.method private buildWhere([J)Ljava/lang/String;
    .locals 7
    .param p1, "longArr"    # [J

    .prologue
    .line 709
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " IN ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 710
    .local v2, "whereClause":Ljava/lang/StringBuilder;
    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-wide v0, p1, v3

    .line 711
    .local v0, "next":J
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 710
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 713
    .end local v0    # "next":J
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const/16 v4, 0x29

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 714
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private buildWhere([Ljava/lang/Long;)Ljava/lang/String;
    .locals 7
    .param p1, "longArr"    # [Ljava/lang/Long;

    .prologue
    .line 719
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " IN ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 720
    .local v2, "whereClause":Ljava/lang/StringBuilder;
    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, p1, v3

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 721
    .local v0, "next":J
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 720
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 723
    .end local v0    # "next":J
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const/16 v4, 0x29

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 724
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private buildWhere([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "strArr"    # [Ljava/lang/String;

    .prologue
    .line 728
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " IN ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 729
    .local v1, "whereClause":Ljava/lang/StringBuilder;
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, p1, v2

    .line 730
    .local v0, "next":Ljava/lang/String;
    const-string v4, "\'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\',"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 729
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 732
    .end local v0    # "next":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x29

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 733
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static createInstance(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    sget-object v1, Lcom/att/mobile/android/vvm/model/db/ModelManager;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 197
    :try_start_0
    sget-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->instance:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 198
    new-instance v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->instance:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 199
    invoke-static {p0}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->createInstance(Landroid/content/Context;)V

    .line 201
    :cond_0
    monitor-exit v1

    .line 202
    return-void

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized deleteMessageMarkedAsOverwrite(J)Z
    .locals 11
    .param p1, "messageUID"    # J

    .prologue
    const/4 v5, 0x1

    .line 409
    monitor-enter p0

    const/4 v4, 0x0

    .line 410
    .local v4, "oldMessageDeleted":Z
    const/4 v1, 0x0

    .line 413
    .local v1, "cr":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageByUid(J)Landroid/database/Cursor;

    move-result-object v1

    .line 414
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 415
    const-string v6, "can_overwrite"

    .line 416
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 415
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-ne v6, v5, :cond_2

    move v0, v5

    .line 418
    .local v0, "canOverwrite":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 419
    const-string v6, "file_name"

    .line 420
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 419
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 422
    .local v3, "fileName":Ljava/lang/String;
    :try_start_1
    iget-object v6, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v7, "inbox"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "uid="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    if-ne v6, v5, :cond_0

    .line 425
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-static {v5, v3}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 427
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-static {v5, v3}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteExternalFile(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 431
    const/4 v4, 0x1

    .line 442
    .end local v0    # "canOverwrite":Z
    .end local v3    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 443
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 446
    :cond_1
    :goto_2
    monitor-exit p0

    return v4

    .line 415
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 433
    .restart local v0    # "canOverwrite":Z
    .restart local v3    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 434
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v5, "ModelManager"

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 439
    .end local v0    # "canOverwrite":Z
    .end local v2    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v3    # "fileName":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 440
    .local v2, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v5, "ModelManager"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 442
    if-eqz v1, :cond_1

    .line 443
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 409
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 442
    :catchall_1
    move-exception v5

    if-eqz v1, :cond_3

    .line 443
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private deleteMessagesFiles([Lcom/att/mobile/android/vvm/model/db/MessageDo;)V
    .locals 5
    .param p1, "messageDos"    # [Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .prologue
    .line 2732
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    .line 2734
    .local v0, "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2737
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteExternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2732
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2739
    .end local v0    # "messageDo":Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->instance:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    if-nez v0, :cond_0

    .line 206
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createInstance(Landroid/content/Context;)V

    .line 210
    :cond_0
    sget-object v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->instance:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    return-object v0
.end method

.method private setMessageDetails(Landroid/database/Cursor;Lcom/att/mobile/android/vvm/model/Message;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "message"    # Lcom/att/mobile/android/vvm/model/Message;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1823
    const-string v4, "time"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Lcom/att/mobile/android/vvm/model/Message;->setDateLong(J)V

    .line 1824
    const-string v4, "file_name"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/att/mobile/android/vvm/model/Message;->setFileName(Ljava/lang/String;)V

    .line 1825
    const-string v4, "read_state"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v4, v2, :cond_0

    move v0, v2

    .line 1826
    .local v0, "isRead":Z
    :goto_0
    invoke-virtual {p2, v0}, Lcom/att/mobile/android/vvm/model/Message;->setRead(Z)V

    .line 1827
    const-string v4, "saved_state"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p2, v4}, Lcom/att/mobile/android/vvm/model/Message;->setSavedState(I)V

    .line 1828
    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Lcom/att/mobile/android/vvm/model/Message;->setRowId(J)V

    .line 1829
    const-string v4, "uid"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Lcom/att/mobile/android/vvm/model/Message;->setUid(J)V

    .line 1831
    const-string v4, "urgent"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p2, v4}, Lcom/att/mobile/android/vvm/model/Message;->setUrgentStatus(I)V

    .line 1832
    const-string v4, "delivery_status"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v4, v2, :cond_1

    :goto_1
    invoke-virtual {p2, v2}, Lcom/att/mobile/android/vvm/model/Message;->setDeliveryStatus(Z)V

    .line 1834
    const-string v2, "phone_number"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1836
    .local v1, "senderPhoneNumber":Ljava/lang/String;
    invoke-virtual {p2, v1}, Lcom/att/mobile/android/vvm/model/Message;->setSenderPhoneNumber(Ljava/lang/String;)V

    .line 1839
    invoke-virtual {p2}, Lcom/att/mobile/android/vvm/model/Message;->isDeliveryStatus()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1840
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v3, 0x7f0800e7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/att/mobile/android/vvm/model/Message;->setSenderDisplayName(Ljava/lang/String;)V

    .line 1847
    :goto_2
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const-string v3, "transcription"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/att/mobile/android/vvm/model/Message;->setTranscription(Landroid/content/Context;Ljava/lang/String;)V

    .line 1848
    return-void

    .end local v0    # "isRead":Z
    .end local v1    # "senderPhoneNumber":Ljava/lang/String;
    :cond_0
    move v0, v3

    .line 1825
    goto :goto_0

    .restart local v0    # "isRead":Z
    :cond_1
    move v2, v3

    .line 1832
    goto :goto_1

    .line 1841
    .restart local v1    # "senderPhoneNumber":Ljava/lang/String;
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 1842
    invoke-virtual {p2, v1}, Lcom/att/mobile/android/vvm/model/Message;->setSenderDisplayName(Ljava/lang/String;)V

    goto :goto_2

    .line 1844
    :cond_3
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v3, 0x7f0801af

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/att/mobile/android/vvm/model/Message;->setSenderDisplayName(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private uninstallShortcutBrute(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3181
    const-string v4, "ModelManager"

    const-string v5, "uninstallShortcutBrute - Google play shortcut"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3182
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 3184
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "#Intent;action=android.intent.action.MAIN;category=android.intent.category.LAUNCHER;launchFlags=0x10000000;package=com.att.mobile.android.vvm;component=com.att.mobile.android.vvm/.screen.WelcomeActivity;end"

    .line 3186
    .local v3, "oldShortcutUri":Ljava/lang/String;
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v3, v4}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 3187
    .local v0, "altShortcutIntent":Landroid/content/Intent;
    const-string v4, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3188
    const-string v4, "android.intent.extra.shortcut.NAME"

    const v5, 0x7f0800a3

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3192
    .end local v0    # "altShortcutIntent":Landroid/content/Intent;
    :goto_0
    const-string v4, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3193
    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3194
    const-string v4, "ModelManager"

    const-string v5, "uninstallShortcutBrute - Google play shortcut. Uninstall sent"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3195
    return-void

    .line 3189
    :catch_0
    move-exception v1

    .line 3190
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "ModelManager"

    const-string v5, ""

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private updateWidgets()V
    .locals 4

    .prologue
    .line 307
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const-class v3, Lcom/att/mobile/android/vvm/widget/VVMWidgetUpdateService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 308
    return-void
.end method


# virtual methods
.method public addEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 3388
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/Dispatcher;->addListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 3389
    return-void
.end method

.method public declared-synchronized clearPreferences()V
    .locals 2

    .prologue
    .line 3355
    monitor-enter p0

    :try_start_0
    const-string v0, "ModelManager"

    const-string v1, "ModelManager::clearPreferences()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3356
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 3357
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 3358
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3360
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3362
    monitor-exit p0

    return-void

    .line 3355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 245
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->helper:Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    monitor-exit p0

    return-void

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createMessageFromCursor(Landroid/database/Cursor;)Lcom/att/mobile/android/vvm/model/Message;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1810
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1811
    new-instance v0, Lcom/att/mobile/android/vvm/model/Message;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/model/Message;-><init>()V

    .line 1813
    .local v0, "message":Lcom/att/mobile/android/vvm/model/Message;
    invoke-direct {p0, p1, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageDetails(Landroid/database/Cursor;Lcom/att/mobile/android/vvm/model/Message;)V

    .line 1819
    .end local v0    # "message":Lcom/att/mobile/android/vvm/model/Message;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized deleteMessageFromDB([Lcom/att/mobile/android/vvm/model/db/MessageDo;)Z
    .locals 10
    .param p1, "messageDos"    # [Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .prologue
    const/4 v6, 0x0

    .line 2589
    monitor-enter p0

    :try_start_0
    array-length v7, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v7, :cond_1

    .line 2627
    :cond_0
    :goto_0
    monitor-exit p0

    return v6

    .line 2593
    :cond_1
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2594
    .local v5, "whereBuilder":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2595
    .local v2, "messageIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v7, p1

    if-ge v1, v7, :cond_2

    .line 2596
    iget-object v7, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    aget-object v8, p1, v1

    .line 2597
    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageFileName(J)Ljava/lang/String;

    move-result-object v8

    .line 2596
    invoke-static {v7, v8}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2598
    iget-object v7, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    aget-object v8, p1, v1

    .line 2599
    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageFileName(J)Ljava/lang/String;

    move-result-object v8

    .line 2598
    invoke-static {v7, v8}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteExternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2600
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, p1, v1

    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " OR "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2602
    aget-object v7, p1, v1

    invoke-virtual {v7}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2595
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2605
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2608
    .local v4, "where":Ljava/lang/String;
    const/4 v7, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x4

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 2611
    const/4 v3, 0x0

    .line 2613
    .local v3, "numOfDeleted":I
    :try_start_2
    iget-object v7, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "inbox"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v4, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    .line 2618
    if-lez v3, :cond_0

    .line 2619
    const/16 v6, 0xa

    :try_start_3
    invoke-virtual {p0, v6, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 2620
    const-string v6, "ModelManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ModelManager.deleteMessageFromInbox() - message with IDs "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 2622
    invoke-virtual {v2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " deleted"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2620
    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2623
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateWidgets()V

    .line 2624
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 2614
    :catch_0
    move-exception v0

    .line 2615
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v7, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 2589
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v1    # "i":I
    .end local v2    # "messageIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v3    # "numOfDeleted":I
    .end local v4    # "where":Ljava/lang/String;
    .end local v5    # "whereBuilder":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public deleteMessagesMarkedAsDeleted()V
    .locals 3

    .prologue
    .line 895
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAllMarkedAsDeletedMessages()[Lcom/att/mobile/android/vvm/model/db/MessageDo;

    move-result-object v0

    .line 896
    .local v0, "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 897
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->deleteMessagesFiles([Lcom/att/mobile/android/vvm/model/db/MessageDo;)V

    .line 899
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->deleteMessageFromDB([Lcom/att/mobile/android/vvm/model/db/MessageDo;)Z

    .line 901
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/vvm/VVMApplication;

    check-cast v1, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->updateNotification()V

    .line 902
    const-string v1, "ModelManager"

    const-string v2, "ModelManager.deleteMessages() notifyListeners EVENTS.DELETE_FINISHED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 905
    :cond_0
    return-void
.end method

.method public declared-synchronized deleteMessagesNotOnServer(Ljava/util/ArrayList;)I
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2399
    .local p1, "serverExistingMessageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    monitor-enter p0

    :try_start_0
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    .line 2401
    .local v25, "serverExistingMessageIDsSB":Ljava/lang/StringBuilder;
    const-string v2, "2147483647"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2404
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2405
    const-string v2, ","

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2407
    new-instance v25, Ljava/lang/StringBuilder;

    .end local v25    # "serverExistingMessageIDsSB":Ljava/lang/StringBuilder;
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    .line 2408
    .restart local v25    # "serverExistingMessageIDsSB":Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v22

    .line 2409
    .local v22, "numberOfServerExistingMessages":I
    const/16 v24, 0x0

    .local v24, "position":I
    :goto_0
    move/from16 v0, v24

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    .line 2410
    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2409
    add-int/lit8 v24, v24, 0x1

    goto :goto_0

    .line 2412
    :cond_0
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 2419
    .end local v22    # "numberOfServerExistingMessages":I
    .end local v24    # "position":I
    :cond_1
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const-string v3, "userPref"

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 2421
    .local v27, "username":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file_name"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " LIKE \'"

    .line 2422
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%\' AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uid"

    .line 2423
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " NOT IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2424
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v28

    .line 2430
    .local v28, "whereClauseSB":Ljava/lang/StringBuilder;
    const/16 v16, 0x0

    .line 2431
    .local v16, "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v12, 0x0

    .line 2437
    .local v12, "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v18, 0x0

    .line 2441
    .local v18, "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/16 v26, 0x0

    .line 2445
    .local v26, "serverNonExistingMessageDetails":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "inbox"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "saved_state"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "file_name"

    aput-object v6, v4, v5

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 2449
    if-eqz v26, :cond_9

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2451
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v23

    .line 2452
    .local v23, "numberOfServerNonExistingMessages":I
    new-instance v17, Ljava/util/ArrayList;

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2453
    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .local v17, "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :try_start_2
    new-instance v13, Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2454
    .end local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v13, "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_3
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2459
    .end local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .local v19, "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_2
    :try_start_4
    const-string v2, "_id"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 2462
    .local v14, "messageID":J
    const-string v2, "saved_state"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 2466
    .local v20, "messageSavedState":I
    const/4 v2, 0x4

    move/from16 v0, v20

    if-ne v0, v2, :cond_8

    .line 2469
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2480
    :goto_1
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result v2

    if-eqz v2, :cond_2

    .line 2489
    :cond_3
    if-eqz v26, :cond_c

    .line 2490
    :try_start_5
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    move-object/from16 v18, v19

    .end local v19    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object v12, v13

    .end local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v16, v17

    .line 2495
    .end local v14    # "messageID":J
    .end local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v20    # "messageSavedState":I
    .end local v23    # "numberOfServerNonExistingMessages":I
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_4
    :goto_2
    if-eqz v18, :cond_5

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_5
    if-eqz v16, :cond_6

    .line 2496
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2497
    :cond_6
    const-string v2, "ModelManager"

    const-string v3, "ModelManager.delteMessagesNotOnServer() - all application\'s messages exist on server"

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2498
    const/16 v21, 0x0

    .line 2547
    :cond_7
    :goto_3
    monitor-exit p0

    return v21

    .line 2473
    .end local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14    # "messageID":J
    .restart local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v19    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "messageSavedState":I
    .restart local v23    # "numberOfServerNonExistingMessages":I
    :cond_8
    :try_start_6
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2477
    const-string v2, "file_name"

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v26

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2478
    .local v11, "fileName":Ljava/lang/String;
    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_1

    .line 2485
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v14    # "messageID":J
    .end local v20    # "messageSavedState":I
    :catch_0
    move-exception v10

    move-object/from16 v18, v19

    .end local v19    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object v12, v13

    .end local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v16, v17

    .line 2486
    .end local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v23    # "numberOfServerNonExistingMessages":I
    .local v10, "e":Ljava/lang/Exception;
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :goto_4
    :try_start_7
    const-string v2, "ModelManager"

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v10}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2489
    if-eqz v26, :cond_4

    .line 2490
    :try_start_8
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 2399
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v25    # "serverExistingMessageIDsSB":Ljava/lang/StringBuilder;
    .end local v26    # "serverNonExistingMessageDetails":Landroid/database/Cursor;
    .end local v27    # "username":Ljava/lang/String;
    .end local v28    # "whereClauseSB":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 2483
    .restart local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v25    # "serverExistingMessageIDsSB":Ljava/lang/StringBuilder;
    .restart local v26    # "serverNonExistingMessageDetails":Landroid/database/Cursor;
    .restart local v27    # "username":Ljava/lang/String;
    .restart local v28    # "whereClauseSB":Ljava/lang/StringBuilder;
    :cond_9
    const/16 v21, 0x0

    .line 2489
    if-eqz v26, :cond_7

    .line 2490
    :try_start_9
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 2489
    :catchall_1
    move-exception v2

    :goto_5
    if-eqz v26, :cond_a

    .line 2490
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v2

    .line 2503
    :cond_b
    const/16 v21, 0x0

    .line 2544
    .local v21, "numberOfDeletedRecords":I
    invoke-direct/range {p0 .. p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateWidgets()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    .line 2489
    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v21    # "numberOfDeletedRecords":I
    .restart local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v23    # "numberOfServerNonExistingMessages":I
    :catchall_2
    move-exception v2

    move-object/from16 v16, v17

    .end local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    goto :goto_5

    .end local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_3
    move-exception v2

    move-object v12, v13

    .end local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v16, v17

    .end local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    goto :goto_5

    .end local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v19    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_4
    move-exception v2

    move-object/from16 v18, v19

    .end local v19    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object v12, v13

    .end local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v16, v17

    .end local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    goto :goto_5

    .line 2485
    .end local v23    # "numberOfServerNonExistingMessages":I
    :catch_1
    move-exception v10

    goto :goto_4

    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v23    # "numberOfServerNonExistingMessages":I
    :catch_2
    move-exception v10

    move-object/from16 v16, v17

    .end local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    goto :goto_4

    .end local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catch_3
    move-exception v10

    move-object v12, v13

    .end local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v16, v17

    .end local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    goto :goto_4

    .end local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14    # "messageID":J
    .restart local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v19    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "messageSavedState":I
    :cond_c
    move-object/from16 v18, v19

    .end local v19    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v18    # "messageIDsToUpdateSavedStateArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object v12, v13

    .end local v13    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "messageFileNamesToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v16, v17

    .end local v17    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v16    # "messageIDsToDeleteArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    goto/16 :goto_2
.end method

.method public deleteUnsavedMessages(Z)[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .locals 3
    .param p1, "deleteWelcomeMessage"    # Z

    .prologue
    .line 2661
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getAllMessages(ZZ)[Lcom/att/mobile/android/vvm/model/db/MessageDo;

    move-result-object v0

    .line 2663
    .local v0, "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    array-length v1, v0

    if-lez v1, :cond_0

    .line 2665
    invoke-direct {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->deleteMessagesFiles([Lcom/att/mobile/android/vvm/model/db/MessageDo;)V

    .line 2667
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->deleteMessageFromDB([Lcom/att/mobile/android/vvm/model/db/MessageDo;)Z

    .line 2670
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/att/mobile/android/vvm/VVMApplication;

    check-cast v1, Lcom/att/mobile/android/vvm/VVMApplication;

    .line 2671
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->updateNotification()V

    .line 2672
    const-string v1, "ModelManager"

    const-string v2, "ModelManager.deleteUnsavedMessages() notifyListeners EVENTS.DELETE_FINISHED"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2673
    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 2677
    :cond_0
    return-object v0
.end method

.method public declared-synchronized getAllMarkedAsDeletedMessages()[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .locals 14

    .prologue
    .line 909
    monitor-enter p0

    const/4 v13, 0x0

    .line 912
    .local v13, "queryResults":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "uid"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "time"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "phone_number"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "file_name"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "read_state"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "transcription"

    aput-object v4, v2, v3

    sget-object v3, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_MARKED_AS_DELETE:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "time desc"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 916
    if-nez v13, :cond_1

    .line 917
    const-string v0, "ModelManager"

    const-string v1, "getAllMarkedAsDeletedMessages() - No MarkedAsDeleted messages in the application"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    const/4 v0, 0x0

    new-array v9, v0, [Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 950
    if-eqz v13, :cond_0

    .line 951
    :try_start_1
    invoke-interface {v13}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 955
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v9

    .line 921
    :cond_1
    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 922
    .local v10, "numberOfExistingMessages":I
    if-lez v10, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 923
    new-array v9, v10, [Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .line 924
    .local v9, "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    const/4 v11, 0x0

    .local v11, "position":I
    move v12, v11

    .line 926
    .end local v11    # "position":I
    .local v12, "position":I
    :goto_1
    new-instance v0, Lcom/att/mobile/android/vvm/model/db/MessageDo;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/model/db/MessageDo;-><init>()V

    aput-object v0, v9, v12

    .line 927
    aget-object v0, v9, v12

    const-string v1, "file_name"

    .line 929
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 928
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 927
    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setFileName(Ljava/lang/String;)V

    .line 930
    aget-object v0, v9, v12

    const-string v1, "_id"

    .line 931
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 930
    invoke-virtual {v0, v2, v3}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setId(J)V

    .line 932
    aget-object v0, v9, v12

    const-string v1, "phone_number"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setPhoneNumber(Ljava/lang/String;)V

    .line 933
    aget-object v0, v9, v12

    const-string v1, "read_state"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setReadState(Ljava/lang/String;)V

    .line 934
    aget-object v0, v9, v12

    const-string v1, "time"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setTime(Ljava/lang/String;)V

    .line 935
    aget-object v0, v9, v12

    const-string v1, "transcription"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setTranscription(Ljava/lang/String;)V

    .line 936
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "position":I
    .restart local v11    # "position":I
    aget-object v0, v9, v12

    const-string v1, "uid"

    .line 938
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 937
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setUid(J)V

    .line 941
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 942
    invoke-interface {v13}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_6

    .line 950
    :cond_2
    if-eqz v13, :cond_0

    .line 951
    :try_start_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 909
    .end local v9    # "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .end local v10    # "numberOfExistingMessages":I
    .end local v11    # "position":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 950
    .restart local v10    # "numberOfExistingMessages":I
    :cond_3
    if-eqz v13, :cond_4

    .line 951
    :try_start_4
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 955
    .end local v10    # "numberOfExistingMessages":I
    :cond_4
    :goto_2
    const/4 v0, 0x0

    new-array v9, v0, [Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 946
    :catch_0
    move-exception v8

    .line 947
    .local v8, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v0, "ModelManager"

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 950
    if-eqz v13, :cond_4

    .line 951
    :try_start_6
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 950
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v0

    if-eqz v13, :cond_5

    .line 951
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .restart local v9    # "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .restart local v10    # "numberOfExistingMessages":I
    .restart local v11    # "position":I
    :cond_6
    move v12, v11

    .end local v11    # "position":I
    .restart local v12    # "position":I
    goto/16 :goto_1
.end method

.method public declared-synchronized getAllMessages(ZZ)[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .locals 14
    .param p1, "saved"    # Z
    .param p2, "getWelcomeMessage"    # Z

    .prologue
    .line 2748
    monitor-enter p0

    const/4 v13, 0x0

    .line 2750
    .local v13, "queryResults":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 2754
    .local v3, "whereClause":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 2755
    if-eqz p1, :cond_1

    const/4 v3, 0x0

    .line 2767
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "uid"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "time"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "phone_number"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "file_name"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "read_state"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "transcription"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "time desc"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 2771
    if-nez v13, :cond_4

    .line 2772
    const-string v0, "ModelManager"

    const-string v1, "ModelManager.deleteAllMessages() - couldn\'t delete all messages in the application"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2776
    const/4 v0, 0x0

    new-array v9, v0, [Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2808
    if-eqz v13, :cond_0

    .line 2809
    :try_start_1
    invoke-interface {v13}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2813
    :cond_0
    :goto_1
    monitor-exit p0

    return-object v9

    .line 2755
    :cond_1
    :try_start_2
    const-string v3, "saved_state != 4"

    goto :goto_0

    .line 2761
    :cond_2
    if-eqz p1, :cond_3

    const-string v3, ""

    :goto_2
    goto :goto_0

    :cond_3
    const-string v3, "saved_state != 4 AND uid != 2147483647"

    goto :goto_2

    .line 2779
    :cond_4
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 2780
    .local v10, "numberOfExistingMessages":I
    if-lez v10, :cond_6

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2781
    new-array v9, v10, [Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .line 2782
    .local v9, "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    const/4 v11, 0x0

    .local v11, "position":I
    move v12, v11

    .line 2784
    .end local v11    # "position":I
    .local v12, "position":I
    :goto_3
    new-instance v0, Lcom/att/mobile/android/vvm/model/db/MessageDo;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/model/db/MessageDo;-><init>()V

    aput-object v0, v9, v12

    .line 2785
    aget-object v0, v9, v12

    const-string v1, "file_name"

    .line 2787
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 2786
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2785
    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setFileName(Ljava/lang/String;)V

    .line 2788
    aget-object v0, v9, v12

    const-string v1, "_id"

    .line 2789
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 2788
    invoke-virtual {v0, v4, v5}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setId(J)V

    .line 2790
    aget-object v0, v9, v12

    const-string v1, "phone_number"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setPhoneNumber(Ljava/lang/String;)V

    .line 2791
    aget-object v0, v9, v12

    const-string v1, "read_state"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setReadState(Ljava/lang/String;)V

    .line 2792
    aget-object v0, v9, v12

    const-string v1, "time"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setTime(Ljava/lang/String;)V

    .line 2793
    aget-object v0, v9, v12

    const-string v1, "transcription"

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setTranscription(Ljava/lang/String;)V

    .line 2794
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "position":I
    .restart local v11    # "position":I
    aget-object v0, v9, v12

    const-string v1, "uid"

    .line 2796
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 2795
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setUid(J)V

    .line 2799
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2800
    invoke-interface {v13}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    if-eqz v0, :cond_9

    .line 2808
    :cond_5
    if-eqz v13, :cond_0

    .line 2809
    :try_start_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 2748
    .end local v9    # "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .end local v10    # "numberOfExistingMessages":I
    .end local v11    # "position":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2808
    .restart local v10    # "numberOfExistingMessages":I
    :cond_6
    if-eqz v13, :cond_7

    .line 2809
    :try_start_4
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 2813
    .end local v10    # "numberOfExistingMessages":I
    :cond_7
    :goto_4
    const/4 v0, 0x0

    new-array v9, v0, [Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 2804
    :catch_0
    move-exception v8

    .line 2805
    .local v8, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v0, "ModelManager"

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2808
    if-eqz v13, :cond_7

    .line 2809
    :try_start_6
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 2808
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v0

    if-eqz v13, :cond_8

    .line 2809
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .restart local v9    # "messageDos":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .restart local v10    # "numberOfExistingMessages":I
    .restart local v11    # "position":I
    :cond_9
    move v12, v11

    .end local v11    # "position":I
    .restart local v12    # "position":I
    goto/16 :goto_3
.end method

.method public declared-synchronized getAllMessagesFromInbox()Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 1165
    monitor-enter p0

    :try_start_0
    const-string v3, "saved_state!=?"

    .line 1166
    .local v3, "whereClause":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1168
    .local v4, "whereArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v2, v5

    const/4 v5, 0x1

    const-string v6, "uid"

    aput-object v6, v2, v5

    const/4 v5, 0x2

    const-string v6, "time"

    aput-object v6, v2, v5

    const/4 v5, 0x3

    const-string v6, "phone_number"

    aput-object v6, v2, v5

    const/4 v5, 0x4

    const-string v6, "transcription"

    aput-object v6, v2, v5

    const/4 v5, 0x5

    const-string v6, "file_name"

    aput-object v6, v2, v5

    const/4 v5, 0x6

    const-string v6, "read_state"

    aput-object v6, v2, v5

    const/4 v5, 0x7

    const-string v6, "saved_state"

    aput-object v6, v2, v5

    const/16 v5, 0x8

    const-string v6, "urgent"

    aput-object v6, v2, v5

    const/16 v5, 0x9

    const-string v6, "delivery_status"

    aput-object v6, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "time desc"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1165
    .end local v3    # "whereClause":Ljava/lang/String;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAllMessagesFromInbox(ILjava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "filterType"    # I
    .param p2, "phoneNumber"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x3

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 1132
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isGroupByContact()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    .line 1133
    .local v9, "isGroupAndAggreg":Z
    :goto_0
    if-eqz v9, :cond_3

    if-ne p1, v2, :cond_2

    sget-object v11, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_PHONE_NUMBER_AND_SAVED:Ljava/lang/String;

    .line 1135
    .local v11, "where":Ljava/lang/String;
    :goto_1
    if-eqz v9, :cond_5

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1137
    .local v4, "selectArgs":[Ljava/lang/String;
    :goto_2
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAllMessagesFromInbox whereClause="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1139
    const/4 v10, 0x0

    .line 1143
    .local v10, "messages":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    sget-object v2, Lcom/att/mobile/android/vvm/model/db/ModelManager;->COLUMNS_VM_LIST:[Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "time desc"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1146
    if-eqz v10, :cond_0

    .line 1147
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1152
    :cond_0
    :goto_3
    monitor-exit p0

    return-object v10

    .end local v4    # "selectArgs":[Ljava/lang/String;
    .end local v9    # "isGroupAndAggreg":Z
    .end local v10    # "messages":Landroid/database/Cursor;
    .end local v11    # "where":Ljava/lang/String;
    :cond_1
    move v9, v0

    .line 1132
    goto :goto_0

    .line 1133
    .restart local v9    # "isGroupAndAggreg":Z
    :cond_2
    :try_start_2
    sget-object v11, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_PHONE_NUMBER_AND_SAVED_NOT:Ljava/lang/String;

    goto :goto_1

    :cond_3
    if-ne p1, v2, :cond_4

    sget-object v11, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_SAVED:Ljava/lang/String;

    goto :goto_1

    :cond_4
    sget-object v11, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_SAVED_NOT:Ljava/lang/String;

    goto :goto_1

    .line 1135
    .restart local v11    # "where":Ljava/lang/String;
    :cond_5
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x4

    .line 1136
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1132
    .end local v9    # "isGroupAndAggreg":Z
    .end local v11    # "where":Ljava/lang/String;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1149
    .restart local v4    # "selectArgs":[Ljava/lang/String;
    .restart local v9    # "isGroupAndAggreg":Z
    .restart local v10    # "messages":Landroid/database/Cursor;
    .restart local v11    # "where":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 1150
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v0, "ModelManager"

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3
.end method

.method public declared-synchronized getAttmStatus()I
    .locals 3

    .prologue
    .line 3325
    monitor-enter p0

    :try_start_0
    const-string v0, "isAttmInstalled"

    const-class v1, Ljava/lang/Integer;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getClientSideTranscription()Z
    .locals 3

    .prologue
    .line 2924
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v1, 0x7f0801a3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCurrentSetupState()I
    .locals 4

    .prologue
    .line 3071
    monitor-enter p0

    :try_start_0
    const-string v1, "currentSetupState"

    const-class v2, Ljava/lang/Integer;

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3072
    .local v0, "currentState":I
    monitor-exit p0

    return v0

    .line 3071
    .end local v0    # "currentState":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getDb()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public getDispatcher()Lcom/att/mobile/android/vvm/control/Dispatcher;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    return-object v0
.end method

.method public getGreetingList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3747
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->greetingList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public declared-synchronized getLastUnreadMessage()Landroid/database/Cursor;
    .locals 12

    .prologue
    .line 2179
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "read_state"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    .line 2180
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "time"

    .line 2181
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=(SELECT MAX("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "time"

    .line 2182
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "inbox"

    .line 2183
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "read_state"

    .line 2184
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    .line 2185
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 2191
    .local v4, "where":Ljava/lang/String;
    const/4 v10, 0x0

    .line 2193
    .local v10, "c":Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "inbox"

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "uid"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "time"

    aput-object v6, v3, v5

    const/4 v5, 0x3

    const-string v6, "phone_number"

    aput-object v6, v3, v5

    const/4 v5, 0x4

    const-string v6, "transcription"

    aput-object v6, v3, v5

    const/4 v5, 0x5

    const-string v6, "file_name"

    aput-object v6, v3, v5

    const/4 v5, 0x6

    const-string v6, "read_state"

    aput-object v6, v3, v5

    const/4 v5, 0x7

    const-string v6, "saved_state"

    aput-object v6, v3, v5

    const/16 v5, 0x8

    const-string v6, "forward_state"

    aput-object v6, v3, v5

    const/16 v5, 0x9

    const-string v6, "delivery_status"

    aput-object v6, v3, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v10

    .line 2206
    :goto_0
    if-eqz v10, :cond_0

    .line 2207
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2209
    :cond_0
    monitor-exit p0

    return-object v10

    .line 2203
    :catch_0
    move-exception v11

    .line 2204
    .local v11, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v0, "ModelManager"

    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v11}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2179
    .end local v4    # "where":Ljava/lang/String;
    .end local v10    # "c":Landroid/database/Cursor;
    .end local v11    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMailBoxStatus()Ljava/lang/String;
    .locals 3

    .prologue
    .line 3067
    monitor-enter p0

    :try_start_0
    const-string v0, "mailboxStatus"

    const-class v1, Ljava/lang/String;

    const-string v2, "U"

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMessage(J)Landroid/database/Cursor;
    .locals 11
    .param p1, "messageId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1722
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "inbox"

    const/16 v3, 0xb

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "uid"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "time"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "phone_number"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "transcription"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "file_name"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "read_state"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "saved_state"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "urgent"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "delivery_status"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "watson_transcription"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1730
    .local v10, "mCursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 1731
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1734
    :cond_0
    monitor-exit p0

    return-object v10

    .line 1722
    .end local v10    # "mCursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMessageByUid(J)Landroid/database/Cursor;
    .locals 11
    .param p1, "messageUid"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1852
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "inbox"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "uid"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "can_overwrite"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "file_name"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "uid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMessageFileName(J)Ljava/lang/String;
    .locals 13
    .param p1, "messageID"    # J

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 2251
    monitor-enter p0

    const/4 v8, 0x0

    .line 2255
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "file_name"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    .line 2259
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2260
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2255
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2263
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v12, :cond_1

    .line 2264
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2266
    const-string v0, "file_name"

    .line 2267
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2266
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2268
    .local v10, "fileName":Ljava/lang/String;
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    .line 2279
    if-eqz v8, :cond_0

    .line 2280
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v10    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v10

    .line 2279
    :cond_1
    if-eqz v8, :cond_2

    .line 2280
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_2
    move-object v10, v11

    goto :goto_0

    .line 2274
    :catch_0
    move-exception v9

    .line 2275
    .local v9, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "ModelManager"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2279
    if-eqz v8, :cond_3

    .line 2280
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v10, v11

    goto :goto_0

    .line 2279
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 2280
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2251
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMessagePosition(JILjava/lang/String;)I
    .locals 11
    .param p1, "messageId"    # J
    .param p3, "filterType"    # I
    .param p4, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 1933
    monitor-enter p0

    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "SELECT COUNT(*) FROM "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "inbox"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " WHERE "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1934
    .local v6, "sqlQuery":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isGroupByContact()Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_2

    if-eqz p4, :cond_2

    const/4 v3, 0x1

    .line 1935
    .local v3, "isGroupAndAggreg":Z
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageTimeStamp(J)J

    move-result-wide v4

    .line 1936
    .local v4, "messageDate":J
    const-string v8, "ModelManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " getMessagePosition()  messageId = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " filterType = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " phoneNumber = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " messageDate = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1937
    const-string v8, "time"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " >= "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1938
    if-eqz v3, :cond_4

    const/4 v8, 0x3

    if-ne p3, v8, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "phone_number"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " == \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' AND "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "saved_state"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " == "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1942
    .local v7, "wher":Ljava/lang/StringBuilder;
    :goto_1
    const-string v8, " AND "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1943
    const-string v8, "ModelManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " getMessagePosition sqlQuery="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1945
    const/4 v0, 0x0

    .line 1947
    .local v0, "c":Landroid/database/Cursor;
    :try_start_1
    iget-object v8, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1948
    const/4 v1, 0x1

    .line 1949
    .local v1, "count":I
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1950
    const/4 v8, 0x0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1952
    :cond_0
    const-string v8, "ModelManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " getMessagePosition()  messageId = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " filterType = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " phoneNumber = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " returns count = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1959
    if-eqz v0, :cond_1

    .line 1960
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v1    # "count":I
    :cond_1
    :goto_2
    monitor-exit p0

    return v1

    .line 1934
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v3    # "isGroupAndAggreg":Z
    .end local v4    # "messageDate":J
    .end local v7    # "wher":Ljava/lang/StringBuilder;
    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1938
    .restart local v3    # "isGroupAndAggreg":Z
    .restart local v4    # "messageDate":J
    :cond_3
    :try_start_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "phone_number"

    .line 1939
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " == \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' AND "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "saved_state"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " != "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/16 :goto_1

    :cond_4
    const/4 v8, 0x3

    if-ne p3, v8, :cond_5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saved_state"

    .line 1940
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " == "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/16 :goto_1

    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saved_state"

    .line 1941
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " != "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/att/mobile/android/vvm/model/db/ModelManager;->AND_NOT_DELETED:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    goto/16 :goto_1

    .line 1954
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v7    # "wher":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v2

    .line 1955
    .local v2, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v8, "ModelManager"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v2}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1956
    const/4 v1, -0x1

    .line 1959
    if-eqz v0, :cond_1

    .line 1960
    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 1933
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "isGroupAndAggreg":Z
    .end local v4    # "messageDate":J
    .end local v6    # "sqlQuery":Ljava/lang/StringBuilder;
    .end local v7    # "wher":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 1959
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v3    # "isGroupAndAggreg":Z
    .restart local v4    # "messageDate":J
    .restart local v6    # "sqlQuery":Ljava/lang/StringBuilder;
    .restart local v7    # "wher":Ljava/lang/StringBuilder;
    :catchall_1
    move-exception v8

    if-eqz v0, :cond_6

    .line 1960
    :try_start_6
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public getMessageTimeStamp(J)J
    .locals 17
    .param p1, "messageId"    # J

    .prologue
    .line 1759
    const/4 v12, 0x0

    .line 1760
    .local v12, "c":Landroid/database/Cursor;
    const-wide/16 v14, 0x0

    .line 1762
    .local v14, "timestamp":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "inbox"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "time"

    aput-object v7, v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p1

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1765
    if-eqz v12, :cond_0

    .line 1766
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1767
    const-string v2, "time"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v14

    .line 1774
    :cond_0
    if-eqz v12, :cond_1

    .line 1775
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_1
    move-wide v2, v14

    :cond_2
    :goto_0
    return-wide v2

    .line 1770
    :catch_0
    move-exception v13

    .line 1771
    .local v13, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v2, "ModelManager"

    invoke-virtual {v13}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v13}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1772
    const-wide/16 v2, -0x1

    .line 1774
    if-eqz v12, :cond_2

    .line 1775
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1774
    .end local v13    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v12, :cond_3

    .line 1775
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method public declared-synchronized getMessageTranscription(J)Ljava/lang/String;
    .locals 13
    .param p1, "messageID"    # J

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2328
    monitor-enter p0

    const/4 v8, 0x0

    .line 2332
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "transcription"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    .line 2336
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2337
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2332
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2339
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v11, :cond_1

    .line 2340
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2342
    const-string v0, "transcription"

    .line 2343
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2342
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    .line 2353
    .local v10, "tirgum":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 2354
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v10    # "tirgum":Ljava/lang/String;
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v10

    .line 2353
    :cond_1
    if-eqz v8, :cond_0

    .line 2354
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2348
    :catch_0
    move-exception v9

    .line 2349
    .local v9, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "ModelManager"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2353
    if-eqz v8, :cond_0

    .line 2354
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2353
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v0

    if-eqz v8, :cond_2

    .line 2354
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public declared-synchronized getMessageUID(J)J
    .locals 13
    .param p1, "messageID"    # J

    .prologue
    const-wide/16 v10, -0x1

    const/4 v12, 0x1

    .line 2291
    monitor-enter p0

    const/4 v8, 0x0

    .line 2295
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "uid"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    .line 2297
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2298
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2295
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2300
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v12, :cond_1

    .line 2301
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2303
    const-string v0, "uid"

    .line 2304
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2303
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v10

    .line 2314
    .local v10, "uid":J
    if-eqz v8, :cond_0

    .line 2315
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v10    # "uid":J
    :cond_0
    :goto_0
    monitor-exit p0

    return-wide v10

    .line 2314
    :cond_1
    if-eqz v8, :cond_0

    .line 2315
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2309
    :catch_0
    move-exception v9

    .line 2310
    .local v9, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "ModelManager"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2314
    if-eqz v8, :cond_0

    .line 2315
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2314
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v0

    if-eqz v8, :cond_2

    .line 2315
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public getMessageUIDsToDelete()[Ljava/lang/Long;
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 3552
    const/4 v8, 0x0

    .line 3554
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "uid"

    aput-object v4, v2, v3

    sget-object v3, Lcom/att/mobile/android/vvm/model/db/ModelManager;->WHERE_MARKED_AS_DELETE:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3555
    if-eqz v8, :cond_2

    .line 3557
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v12, v0, [Ljava/lang/Long;

    .line 3558
    .local v12, "uids":[Ljava/lang/Long;
    const/4 v10, 0x0

    .local v10, "i":I
    move v11, v10

    .line 3559
    .end local v10    # "i":I
    .local v11, "i":I
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3560
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "i":I
    .restart local v10    # "i":I
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v12, v11
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v11, v10

    .end local v10    # "i":I
    .restart local v11    # "i":I
    goto :goto_0

    .line 3568
    :cond_0
    if-eqz v8, :cond_1

    .line 3569
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3572
    .end local v11    # "i":I
    .end local v12    # "uids":[Ljava/lang/Long;
    :cond_1
    :goto_1
    return-object v12

    .line 3568
    :cond_2
    if-eqz v8, :cond_3

    .line 3569
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_2
    move-object v12, v13

    .line 3572
    goto :goto_1

    .line 3565
    :catch_0
    move-exception v9

    .line 3566
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ModelManager"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3568
    if-eqz v8, :cond_3

    .line 3569
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 3568
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 3569
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public declared-synchronized getMessageWatsonStatus(J)I
    .locals 13
    .param p1, "messageID"    # J

    .prologue
    const/4 v12, 0x1

    const/4 v11, -0x1

    .line 1048
    monitor-enter p0

    const/4 v8, 0x0

    .line 1052
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "watson_transcription"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    .line 1053
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1054
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1052
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1056
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v12, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1058
    const-string v0, "watson_transcription"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1059
    .local v10, "wStatus":I
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ModelManager.getInstance() getMessageWatsonStatus("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") wStatus = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1069
    if-eqz v8, :cond_0

    .line 1070
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v10    # "wStatus":I
    :cond_0
    :goto_0
    monitor-exit p0

    return v10

    .line 1069
    :cond_1
    if-eqz v8, :cond_2

    .line 1070
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_2
    move v10, v11

    goto :goto_0

    .line 1064
    :catch_0
    move-exception v9

    .line 1065
    .local v9, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "ModelManager"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1069
    if-eqz v8, :cond_3

    .line 1070
    :try_start_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    move v10, v11

    goto :goto_0

    .line 1069
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 1070
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1048
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMessagesToDownload(Z)[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .locals 18
    .param p1, "filterByTranscription"    # Z

    .prologue
    .line 1973
    monitor-enter p0

    const/4 v14, 0x0

    .line 1974
    .local v14, "mCursor":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 1977
    .local v16, "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    const/4 v15, 0x0

    .line 1978
    .local v15, "selection":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    .line 1979
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file_name"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " IS NOT NULL AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "file_name"

    .line 1980
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<>\'\' AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "trim("

    .line 1981
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "transcription"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")=\'\' "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 1989
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "inbox"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "uid"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "file_name"

    aput-object v7, v5, v6

    .line 1991
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "time desc"

    const/4 v11, 0x0

    .line 1989
    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1994
    if-eqz v14, :cond_1

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1995
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v2

    new-array v0, v2, [Lcom/att/mobile/android/vvm/model/db/MessageDo;

    move-object/from16 v16, v0

    .line 1996
    const/4 v13, 0x0

    .line 1997
    .local v13, "i":I
    :goto_1
    invoke-interface {v14}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1998
    new-instance v2, Lcom/att/mobile/android/vvm/model/db/MessageDo;

    invoke-direct {v2}, Lcom/att/mobile/android/vvm/model/db/MessageDo;-><init>()V

    aput-object v2, v16, v13

    .line 1999
    aget-object v2, v16, v13

    const-string v3, "uid"

    .line 2000
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 1999
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setUid(J)V

    .line 2001
    aget-object v2, v16, v13

    const-string v3, "_id"

    .line 2002
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 2001
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setId(J)V

    .line 2003
    aget-object v2, v16, v13

    const-string v3, "file_name"

    .line 2004
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 2003
    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setFileName(Ljava/lang/String;)V

    .line 2005
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    .line 2006
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 1984
    .end local v13    # "i":I
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file_name"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " IS NULL OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "file_name"

    .line 1985
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\'\' AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "was_downloaded"

    .line 1986
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v15

    goto/16 :goto_0

    .line 2016
    :cond_1
    if-eqz v14, :cond_2

    .line 2017
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    move-object/from16 v17, v16

    .end local v16    # "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .local v17, "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :goto_2
    monitor-exit p0

    return-object v17

    .line 2011
    .end local v17    # "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .restart local v16    # "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :catch_0
    move-exception v12

    .line 2012
    .local v12, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "ModelManager"

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v12}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2016
    if-eqz v14, :cond_3

    .line 2017
    :try_start_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object/from16 v17, v16

    .end local v16    # "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .restart local v17    # "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    goto :goto_2

    .line 2016
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v17    # "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .restart local v16    # "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :catchall_0
    move-exception v2

    if-eqz v14, :cond_4

    .line 2017
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1973
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getMetadata()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2378
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->metadataHashMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public declared-synchronized getNewMessagesCount()I
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2152
    monitor-enter p0

    const/4 v0, 0x0

    .line 2154
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "select count(*) from inbox where read_state=0"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2157
    const/4 v1, 0x1

    .line 2158
    .local v1, "count":I
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2159
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 2168
    :cond_0
    if-eqz v0, :cond_1

    .line 2169
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v1    # "count":I
    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    .line 2163
    :catch_0
    move-exception v2

    .line 2164
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "ModelManager"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2168
    if-eqz v0, :cond_2

    .line 2169
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    move v1, v3

    goto :goto_0

    .line 2168
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_3

    .line 2169
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2152
    :catchall_1
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized getPassword()Ljava/lang/String;
    .locals 8

    .prologue
    .line 3238
    monitor-enter p0

    const/4 v2, 0x0

    .line 3239
    .local v2, "password":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v4

    const-string v5, "passwordPref"

    const-class v6, Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 3241
    .local v3, "passwordValue":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3245
    const-string v4, ".ldSignature"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3247
    const-string v4, ".ldSignature"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 3249
    .local v1, "encryptedPassword":Ljava/lang/String;
    :try_start_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-le v4, v5, :cond_1

    .line 3250
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".ldSignature"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Crypto"

    invoke-static {v4, v1, v5}, Lcom/att/mobile/android/infra/utils/Crypto;->decrypt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 3268
    .end local v1    # "encryptedPassword":Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_2
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "####ModelManager.getPassword() returns password = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3269
    monitor-exit p0

    return-object v2

    .line 3253
    .restart local v1    # "encryptedPassword":Ljava/lang/String;
    :cond_1
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".ldSignature"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v1, v5}, Lcom/att/mobile/android/infra/utils/Crypto;->decrypt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 3256
    :catch_0
    move-exception v0

    .line 3257
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v4, "ModelManager"

    const-string v5, "#### ModelManager.getPassword() failed"

    invoke-static {v4, v5, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 3238
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "encryptedPassword":Ljava/lang/String;
    .end local v3    # "passwordValue":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 3263
    .restart local v3    # "passwordValue":Ljava/lang/String;
    :cond_2
    move-object v2, v3

    .line 3265
    :try_start_5
    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPassword(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized getPasswordChangeRequiredStatus()Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 3010
    monitor-enter p0

    :try_start_0
    const-string v1, "PasswordChangeRequiredStatus"

    const-class v2, Ljava/lang/Integer;

    const/4 v3, -0x1

    .line 3012
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 3010
    invoke-virtual {p0, v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 3014
    .local v0, "ans":Ljava/lang/Integer;
    const-string v1, "ModelManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ModelManager.getPasswordChangeRequiredStatus() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3017
    monitor-exit p0

    return-object v0

    .line 3010
    .end local v0    # "ans":Ljava/lang/Integer;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getProximitySwitcher()Z
    .locals 3

    .prologue
    .line 2928
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v1, 0x7f08019b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSenderPhoneNumber(J)Ljava/lang/String;
    .locals 13
    .param p1, "messageId"    # J

    .prologue
    .line 1737
    const/4 v10, 0x0

    .line 1738
    .local v10, "c":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 1740
    .local v12, "phonenumber":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "inbox"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "phone_number"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1743
    if-eqz v10, :cond_0

    .line 1744
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1745
    const-string v0, "phone_number"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    .line 1752
    :cond_0
    if-eqz v10, :cond_1

    .line 1753
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v12

    :cond_2
    :goto_0
    return-object v0

    .line 1748
    :catch_0
    move-exception v11

    .line 1749
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ModelManager"

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v11}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1750
    const/4 v0, 0x0

    .line 1752
    if-eqz v10, :cond_2

    .line 1753
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1752
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_3

    .line 1753
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public declared-synchronized getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 2893
    .local p2, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "defaultValue":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 2895
    .local v2, "val":Ljava/lang/Object;
    if-nez v2, :cond_0

    .line 2896
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 2897
    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 2899
    :cond_0
    invoke-virtual {p2, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2901
    .local v1, "res":Ljava/lang/Object;, "TT;"
    if-nez v1, :cond_1

    .line 2905
    .end local v1    # "res":Ljava/lang/Object;, "TT;"
    .end local v2    # "val":Ljava/lang/Object;
    .end local p3    # "defaultValue":Ljava/lang/Object;, "TT;"
    :goto_0
    monitor-exit p0

    return-object p3

    .restart local v1    # "res":Ljava/lang/Object;, "TT;"
    .restart local v2    # "val":Ljava/lang/Object;
    .restart local p3    # "defaultValue":Ljava/lang/Object;, "TT;"
    :cond_1
    move-object p3, v1

    .line 2901
    goto :goto_0

    .line 2902
    .end local v1    # "res":Ljava/lang/Object;, "TT;"
    .end local v2    # "val":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 2903
    .local v0, "cce":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "ModelManager"

    const-string v4, "ModelManager.getSharedPreferenceValue() exception"

    invoke-static {v3, v4, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2893
    .end local v0    # "cce":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized getUnSkippedMessages()[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .locals 11

    .prologue
    .line 2080
    monitor-enter p0

    const/4 v4, 0x0

    .line 2081
    .local v4, "mCursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 2084
    .local v8, "uids":[Lcom/att/mobile/android/vvm/model/db/MessageDo;
    :try_start_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "select _id, uid from "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "inbox"

    .line 2085
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " where "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "tuiskipped"

    .line 2086
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " = 0"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " AND "

    .line 2087
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "uid"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " < "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7fffffff

    .line 2088
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2090
    .local v5, "query":Ljava/lang/String;
    iget-object v9, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v10, 0x0

    invoke-virtual {v9, v5, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 2095
    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2096
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v9

    new-array v8, v9, [Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .line 2097
    const/4 v1, 0x0

    .line 2098
    .local v1, "i":I
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_0

    .line 2099
    const-string v9, "_id"

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2100
    .local v2, "id":J
    const-string v9, "uid"

    .line 2101
    invoke-interface {v4, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 2102
    .local v6, "uid":J
    new-instance v9, Lcom/att/mobile/android/vvm/model/db/MessageDo;

    invoke-direct {v9}, Lcom/att/mobile/android/vvm/model/db/MessageDo;-><init>()V

    aput-object v9, v8, v1

    .line 2103
    aget-object v9, v8, v1

    invoke-virtual {v9, v6, v7}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setUid(J)V

    .line 2104
    aget-object v9, v8, v1

    invoke-virtual {v9, v2, v3}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->setId(J)V

    .line 2105
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2113
    .end local v1    # "i":I
    .end local v2    # "id":J
    .end local v6    # "uid":J
    :cond_0
    if-eqz v4, :cond_1

    .line 2114
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117
    .end local v5    # "query":Ljava/lang/String;
    :cond_1
    :goto_1
    monitor-exit p0

    return-object v8

    .line 2109
    :catch_0
    move-exception v0

    .line 2110
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v9, "ModelManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2113
    if-eqz v4, :cond_1

    .line 2114
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2080
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 2113
    :catchall_1
    move-exception v9

    if-eqz v4, :cond_2

    .line 2114
    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public declared-synchronized hadNotificationsBeforeReboot()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 2970
    monitor-enter p0

    :try_start_0
    const-string v0, "didNotificationsExistBeforeReboot"

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized insertWelcomeMessage()V
    .locals 11

    .prologue
    .line 258
    monitor-enter p0

    :try_start_0
    const-string v5, "ModelManager"

    const-string v8, "insertWelcomeMessage"

    invoke-static {v5, v8}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const-string v5, "isWelcomeMessageInserted"

    const-class v8, Ljava/lang/Boolean;

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {p0, v5, v8, v9}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_0

    .line 264
    const-string v3, "welcome_amr_new.amr"

    .line 265
    .local v3, "fileName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-direct {v1, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 268
    .local v1, "copiedFile":Ljava/io/File;
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    .line 269
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f070006

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v5

    .line 268
    invoke-static {v5, v1}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->copyFile(Ljava/io/InputStream;Ljava/io/File;)V

    .line 272
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 275
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v5, "uid"

    const v8, 0x7fffffff

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 276
    const-string v5, "time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 277
    const-string v5, "phone_number"

    iget-object v8, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v9, 0x7f0801fb

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v5, "read_state"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 279
    const-string v5, "transcription"

    iget-object v8, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v9, 0x7f0801fc

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const-string v5, "file_name"

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v5, "saved_state"

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 284
    const-string v5, "was_downloaded"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    :try_start_1
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "inbox"

    const/4 v9, 0x0

    const/4 v10, 0x4

    invoke-virtual {v5, v8, v9, v0, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v6

    .line 290
    .local v6, "rowID":J
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 293
    const-string v5, "isWelcomeMessageInserted"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 295
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.att.mobile.android.vvm.NEW_UNREAD_MESSAGE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 296
    .local v4, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const-class v8, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 297
    const-string v5, "extra_new_message_row_id"

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 298
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-virtual {v5, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 304
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "copiedFile":Ljava/io/File;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v6    # "rowID":J
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 300
    .restart local v0    # "contentValues":Landroid/content/ContentValues;
    .restart local v1    # "copiedFile":Ljava/io/File;
    .restart local v3    # "fileName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 301
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v5, "ModelManager"

    const-string v8, ""

    invoke-static {v5, v8, v2}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 258
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "copiedFile":Ljava/io/File;
    .end local v2    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v3    # "fileName":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized isFirstTimeUse()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 3030
    monitor-enter p0

    :try_start_0
    const-string v0, "isFirstUse"

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isGroupByContact()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 2943
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v1, 0x7f080194

    .line 2944
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    .line 2945
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2943
    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public isMessageHasTranscription(J)Z
    .locals 3
    .param p1, "messageID"    # J

    .prologue
    .line 2360
    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageTranscription(J)Ljava/lang/String;

    move-result-object v0

    .line 2361
    .local v0, "transcription":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isTranscriptionNotEmpty(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public isMessagePendingForDelete(J)Z
    .locals 13
    .param p1, "messageUID"    # J

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 3515
    const/4 v8, 0x0

    .line 3519
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "forward_state"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "uid"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    .line 3523
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3524
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 3519
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3526
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v10, :cond_3

    .line 3527
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3529
    const-string v0, "forward_state"

    .line 3530
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 3529
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v0, v10

    .line 3539
    :goto_0
    if-eqz v8, :cond_0

    .line 3540
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    move v11, v0

    :cond_1
    :goto_1
    return v11

    :cond_2
    move v0, v11

    .line 3529
    goto :goto_0

    .line 3539
    :cond_3
    if-eqz v8, :cond_1

    .line 3540
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 3534
    :catch_0
    move-exception v9

    .line 3535
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ModelManager"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3539
    if-eqz v8, :cond_1

    .line 3540
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 3539
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 3540
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public isMessagePendingForMarkAsRead(J)Z
    .locals 13
    .param p1, "messageUID"    # J

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 3582
    const/4 v8, 0x0

    .line 3586
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "read_state"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "uid"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    .line 3590
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 3591
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 3586
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3593
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v10, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3595
    const-string v0, "read_state"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, v10, :cond_2

    move v0, v10

    .line 3604
    :goto_0
    if-eqz v8, :cond_0

    .line 3605
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    move v11, v0

    :cond_1
    :goto_1
    return v11

    :cond_2
    move v0, v11

    .line 3595
    goto :goto_0

    .line 3604
    :cond_3
    if-eqz v8, :cond_1

    .line 3605
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 3599
    :catch_0
    move-exception v9

    .line 3600
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ModelManager"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3604
    if-eqz v8, :cond_1

    .line 3605
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 3604
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 3605
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public isMessageSaved(J)Z
    .locals 13
    .param p1, "messageId"    # J

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1781
    const/4 v8, 0x0

    .line 1785
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "inbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "saved_state"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "="

    .line 1789
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1790
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1785
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1792
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v10, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1793
    const-string v0, "saved_state"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    move v0, v10

    .line 1802
    :goto_0
    if-eqz v8, :cond_0

    .line 1803
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    move v11, v0

    :cond_1
    :goto_1
    return v11

    :cond_2
    move v0, v11

    .line 1793
    goto :goto_0

    .line 1802
    :cond_3
    if-eqz v8, :cond_1

    .line 1803
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1797
    :catch_0
    move-exception v9

    .line 1798
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ModelManager"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1802
    if-eqz v8, :cond_1

    .line 1803
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1802
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 1803
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public declared-synchronized isNeedRefreshInbox()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 3100
    monitor-enter p0

    :try_start_0
    const-string v0, "needRefreshInbox"

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isNotifyOnNewMessagesEnabled()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 2937
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v1, 0x7f080196

    .line 2938
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x1

    .line 2939
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2937
    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public declared-synchronized isSetupCompleted()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 3113
    monitor-enter p0

    :try_start_0
    const-string v0, "isSetupCompleted"

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isSetupStarted()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 3086
    monitor-enter p0

    :try_start_0
    const-string v0, "isSetupStarted"

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isTranscriptionNotEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1, "transcription"    # Ljava/lang/String;

    .prologue
    .line 2365
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVVMOverWifi()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 2950
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2951
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v2, 0x7f0801ad

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1
.end method

.method public declared-synchronized markAllMessagesAsOverwrite()V
    .locals 6

    .prologue
    .line 3756
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3757
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v2, "can_overwrite"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3759
    :try_start_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "inbox"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3763
    :goto_0
    monitor-exit p0

    return-void

    .line 3760
    :catch_0
    move-exception v1

    .line 3761
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v2, "ModelManager"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3756
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public notifyListeners(ILjava/util/ArrayList;)V
    .locals 1
    .param p1, "eventId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3408
    .local p2, "messageIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1, p2}, Lcom/att/mobile/android/vvm/control/Dispatcher;->notifyListeners(ILjava/util/ArrayList;)V

    .line 3409
    return-void
.end method

.method public declared-synchronized open()Lcom/att/mobile/android/vvm/model/db/ModelManager;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 235
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->helper:Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    monitor-exit p0

    return-object p0

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public removeEventListener(Lcom/att/mobile/android/vvm/control/EventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/control/EventListener;

    .prologue
    .line 3398
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0, p1}, Lcom/att/mobile/android/vvm/control/Dispatcher;->removeListener(Lcom/att/mobile/android/vvm/control/EventListener;)V

    .line 3399
    return-void
.end method

.method public removeEventListeners()V
    .locals 1

    .prologue
    .line 3413
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->dispatcher:Lcom/att/mobile/android/vvm/control/Dispatcher;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/Dispatcher;->removeListeners()V

    .line 3414
    return-void
.end method

.method public resetVVMOverWifi()V
    .locals 5

    .prologue
    .line 2957
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2958
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2959
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v3, 0x7f0801ad

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2960
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2961
    const-string v2, "ModelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resetVVMOverWifi() is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isVVMOverWifi()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2962
    return-void
.end method

.method public saveMetadata(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2372
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 2373
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->metadataHashMap:Ljava/util/HashMap;

    .line 2375
    :cond_0
    return-void
.end method

.method public declared-synchronized setAttmStatus(I)V
    .locals 2
    .param p1, "attmStatus"    # I

    .prologue
    .line 3321
    monitor-enter p0

    :try_start_0
    const-string v0, "isAttmInstalled"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3322
    monitor-exit p0

    return-void

    .line 3321
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setBooleanSharedPreference(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 2817
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 2819
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2821
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2824
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 2825
    return-void
.end method

.method public declared-synchronized setCheckAttmStatusOnForeground(Z)V
    .locals 3
    .param p1, "shouldCheck"    # Z

    .prologue
    .line 3335
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v1, "shouldCheckAttmStatusOnForeground"

    .line 3336
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 3335
    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3337
    monitor-exit p0

    return-void

    .line 3335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCurrentSetupState(I)V
    .locals 2
    .param p1, "mCurrentState"    # I

    .prologue
    .line 3076
    monitor-enter p0

    :try_start_0
    const-string v0, "currentSetupState"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3077
    monitor-exit p0

    return-void

    .line 3076
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setFirstTimeUse(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "firstTime"    # Ljava/lang/Boolean;

    .prologue
    .line 3039
    monitor-enter p0

    :try_start_0
    const-string v0, "isFirstUse"

    invoke-virtual {p0, v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3040
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "First time use flag was set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3041
    monitor-exit p0

    return-void

    .line 3039
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setGreetingList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/greeting/Greeting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3738
    .local p1, "greetingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/greeting/Greeting;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->greetingList:Ljava/util/ArrayList;

    .line 3739
    return-void
.end method

.method public declared-synchronized setInboxRefreshed(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "inboxRefreshed"    # Ljava/lang/Boolean;

    .prologue
    .line 3057
    monitor-enter p0

    :try_start_0
    const-string v0, "wasInboxRefreshed"

    invoke-virtual {p0, v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3058
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inbox refreshed was set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3059
    monitor-exit p0

    return-void

    .line 3057
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setMailBoxStatus(Ljava/lang/String;)V
    .locals 3
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 3062
    monitor-enter p0

    :try_start_0
    const-string v0, "mailboxStatus"

    invoke-virtual {p0, v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3063
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mailbox status was set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3064
    monitor-exit p0

    return-void

    .line 3062
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setMessageAsRead(J)I
    .locals 9
    .param p1, "UID"    # J

    .prologue
    .line 793
    monitor-enter p0

    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 794
    .local v3, "updatedValues":Landroid/content/ContentValues;
    const-string v5, "read_state"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 795
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 796
    .local v0, "arrIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 798
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "uid"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 801
    .local v4, "whereClauseSB":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 803
    .local v2, "numberOfUpdatedMessage":I
    :try_start_1
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "inbox"

    .line 804
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    .line 803
    invoke-virtual {v5, v6, v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 805
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessagesReadState() - true. read state was set for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messages"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    const/4 v5, 0x2

    invoke-virtual {p0, v5, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 811
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessagesReadState() - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messages were marked as saved"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 815
    :goto_0
    :try_start_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateWidgets()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 817
    monitor-exit p0

    return v2

    .line 812
    :catch_0
    move-exception v1

    .line 813
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v5, "ModelManager"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 793
    .end local v0    # "arrIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "numberOfUpdatedMessage":I
    .end local v3    # "updatedValues":Landroid/content/ContentValues;
    .end local v4    # "whereClauseSB":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized setMessageDetailsFromBodyText(Landroid/content/Context;JJLjava/lang/String;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowID"    # J
    .param p4, "UID"    # J
    .param p6, "transcription"    # Ljava/lang/String;

    .prologue
    .line 492
    monitor-enter p0

    if-nez p6, :cond_1

    .line 495
    :try_start_0
    const-string p6, " "

    .line 510
    :cond_0
    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 511
    .local v2, "updatedValues":Landroid/content/ContentValues;
    const-string v4, "uid"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 512
    const-string v4, "transcription"

    invoke-virtual {v2, v4, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const-string v4, "watson_transcription"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 514
    const-string v4, "was_downloaded"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    :try_start_1
    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "inbox"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "_id"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " = "

    .line 518
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    .line 517
    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    const/4 v3, 0x1

    .line 521
    .local v3, "wasUpdate":Z
    :goto_1
    if-eqz v3, :cond_5

    .line 522
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 523
    .local v1, "rowIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 527
    const/4 v4, 0x5

    invoke-virtual {p0, v4, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 528
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ModelManager.setMessageDetailsFromBodyText() - transcription updated for message with row ID "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529
    const/4 v4, 0x1

    .line 536
    .end local v1    # "rowIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v3    # "wasUpdate":Z
    :goto_2
    monitor-exit p0

    return v4

    .line 497
    .end local v2    # "updatedValues":Landroid/content/ContentValues;
    :cond_1
    :try_start_2
    invoke-virtual {p6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p6

    .line 498
    const-string v4, "Audio message from"

    invoke-virtual {p6, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "cannot be processed into text."

    .line 499
    invoke-virtual {p6, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    const-string v4, "A voicemail from"

    .line 500
    invoke-virtual {p6, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 501
    invoke-virtual {p6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "could not be processed to text."

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 504
    :cond_3
    const v4, 0x7f0801e4

    .line 505
    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p6

    goto/16 :goto_0

    .line 517
    .restart local v2    # "updatedValues":Landroid/content/ContentValues;
    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    .line 531
    :catch_0
    move-exception v0

    .line 532
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v4, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 535
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_5
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ModelManager.setMessageDetailsFromBodyText() - transcription was not updated for message with row ID "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 536
    const/4 v4, 0x0

    goto :goto_2

    .line 492
    .end local v2    # "updatedValues":Landroid/content/ContentValues;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized setMessageErrorFile(J)Z
    .locals 9
    .param p1, "messageID"    # J

    .prologue
    const/4 v4, 0x1

    .line 1003
    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1005
    .local v2, "updatedValues":Landroid/content/ContentValues;
    const-string v5, "saved_state"

    const/16 v6, 0x8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009
    const/4 v3, 0x0

    .line 1011
    .local v3, "wasMessageSet":Z
    :try_start_1
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "inbox"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "_id"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " = "

    .line 1012
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1013
    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    .line 1011
    invoke-virtual {v5, v6, v2, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-ne v5, v4, :cond_1

    move v3, v4

    .line 1016
    :goto_0
    if-eqz v3, :cond_0

    .line 1017
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1018
    .local v1, "messageIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1022
    const/4 v4, 0x7

    invoke-virtual {p0, v4, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1029
    .end local v1    # "messageIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_0
    :goto_1
    monitor-exit p0

    return v3

    .line 1011
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1024
    :catch_0
    move-exception v0

    .line 1025
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v4, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1003
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "updatedValues":Landroid/content/ContentValues;
    .end local v3    # "wasMessageSet":Z
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized setMessageFileName(JLjava/lang/String;)Z
    .locals 9
    .param p1, "messageID"    # J
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 968
    monitor-enter p0

    :try_start_0
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setMessageFileName messageID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " fileName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 970
    .local v2, "updatedValues":Landroid/content/ContentValues;
    const-string v5, "file_name"

    invoke-virtual {v2, v5, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    const-string v5, "saved_state"

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 976
    const/4 v3, 0x0

    .line 978
    .local v3, "wasMessageSet":Z
    :try_start_1
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "inbox"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "_id"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " = "

    .line 979
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 980
    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    .line 978
    invoke-virtual {v5, v6, v2, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-ne v5, v4, :cond_2

    move v3, v4

    .line 983
    :goto_0
    if-eqz v3, :cond_1

    .line 984
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 985
    .local v1, "messageIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 989
    invoke-static {p1, p2}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->needRequestWatson(J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 990
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setMessageFileName need request Watson transcription message Uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fileName = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "callWatsonTranscriptionTask"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    invoke-static {}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->getInstance()Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->callWatsonTranscriptionTask(JLjava/lang/String;)V

    .line 993
    :cond_0
    const/4 v4, 0x4

    invoke-virtual {p0, v4, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 999
    .end local v1    # "messageIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_1
    :goto_1
    monitor-exit p0

    return v3

    .line 978
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 995
    :catch_0
    move-exception v0

    .line 996
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v4, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 968
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "updatedValues":Landroid/content/ContentValues;
    .end local v3    # "wasMessageSet":Z
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized setMessageFileNotFound(J)V
    .locals 7
    .param p1, "messageId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 1862
    monitor-enter p0

    :try_start_0
    const-string v3, "ModelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setMessageFileNotFound messageId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1863
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1865
    .local v2, "where":Ljava/lang/StringBuilder;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1866
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v3, "file_name"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1867
    const-string v3, "saved_state"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1868
    const-string v3, "watson_transcription"

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1871
    :try_start_1
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "inbox"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1872
    const-string v3, "ModelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ModelManager.getInstance() setMessageFileNotFound("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") Finished"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1876
    :goto_0
    monitor-exit p0

    return-void

    .line 1873
    :catch_0
    move-exception v1

    .line 1874
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v3, "ModelManager"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1862
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "where":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized setMessageTranscription(JLjava/lang/String;)Z
    .locals 11
    .param p1, "messageID"    # J
    .param p3, "transcription"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 548
    monitor-enter p0

    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p3

    .line 551
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 552
    .local v2, "updatedValues":Landroid/content/ContentValues;
    const-string v6, "transcription"

    invoke-virtual {v2, v6, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 555
    :try_start_1
    iget-object v6, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v7, "inbox"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "_id"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " = "

    .line 556
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 557
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 555
    invoke-virtual {v6, v7, v2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    if-ne v6, v4, :cond_0

    move v3, v4

    .line 560
    .local v3, "wasUpdate":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 561
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 562
    .local v1, "rowIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    const/4 v6, 0x5

    invoke-virtual {p0, v6, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 567
    const-string v6, "ModelManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ModelManager.setMessageTranscription() - transcription updated for message with message ID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 581
    .end local v1    # "rowIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v3    # "wasUpdate":Z
    :goto_1
    monitor-exit p0

    return v4

    :cond_0
    move v3, v5

    .line 555
    goto :goto_0

    .line 573
    :catch_0
    move-exception v0

    .line 574
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v4, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 577
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    const-string v4, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessageTranscription() - transcription was not updated for message with message ID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v4, v5

    .line 581
    goto :goto_1

    .line 548
    .end local v2    # "updatedValues":Landroid/content/ContentValues;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized setMessageTranscriptionError(J)Z
    .locals 11
    .param p1, "messageID"    # J

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 593
    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 594
    .local v2, "updatedValues":Landroid/content/ContentValues;
    invoke-virtual {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessageHasTranscription(J)Z

    move-result v6

    if-nez v6, :cond_0

    .line 595
    const-string v6, "transcription"

    iget-object v7, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    const v8, 0x7f0801e4

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_0
    const-string v6, "watson_transcription"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 600
    :try_start_1
    iget-object v6, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v7, "inbox"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "_id"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " = "

    .line 601
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 602
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    .line 600
    invoke-virtual {v6, v7, v2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    if-ne v6, v4, :cond_1

    move v3, v4

    .line 605
    .local v3, "wasUpdate":Z
    :goto_0
    if-eqz v3, :cond_2

    .line 606
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 607
    .local v1, "rowIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    const-string v6, "ModelManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ModelManager.setMessageTranscriptionError() - transcription error updated for message with message ID = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " status set to - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618
    .end local v1    # "rowIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v3    # "wasUpdate":Z
    :goto_1
    monitor-exit p0

    return v4

    :cond_1
    move v3, v5

    .line 600
    goto :goto_0

    .line 613
    :catch_0
    move-exception v0

    .line 614
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v4, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 617
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_2
    const-string v4, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessageTranscriptionError() - transcription error was not updated for message with message ID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v4, v5

    .line 618
    goto :goto_1

    .line 593
    .end local v2    # "updatedValues":Landroid/content/ContentValues;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized setMessageWatsonStatus(JI)Z
    .locals 9
    .param p1, "messageID"    # J
    .param p3, "watsonStatus"    # I

    .prologue
    const/4 v3, 0x1

    .line 1034
    monitor-enter p0

    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1035
    .local v1, "updatedValues":Landroid/content/ContentValues;
    const-string v4, "watson_transcription"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1036
    const/4 v2, 0x0

    .line 1038
    .local v2, "wasMessageSet":Z
    :try_start_1
    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "inbox"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "_id"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-ne v4, v3, :cond_0

    move v2, v3

    .line 1039
    :goto_0
    const-string v3, "ModelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ModelManager.getInstance() setMessageFileNotFound("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") Finished"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1043
    :goto_1
    monitor-exit p0

    return v2

    .line 1038
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1040
    :catch_0
    move-exception v0

    .line 1041
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v3, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1034
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v1    # "updatedValues":Landroid/content/ContentValues;
    .end local v2    # "wasMessageSet":Z
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized setMessagesAsDeleted([Ljava/lang/Long;)I
    .locals 8
    .param p1, "iDs"    # [Ljava/lang/Long;

    .prologue
    .line 822
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    array-length v4, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_1

    .line 823
    :cond_0
    const/4 v1, 0x0

    .line 852
    :goto_0
    monitor-exit p0

    return v1

    .line 827
    :cond_1
    :try_start_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 828
    .local v2, "updatedValues":Landroid/content/ContentValues;
    const-string v4, "forward_state"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 831
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->buildWhere([Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 832
    .local v3, "whereClauseSB":Ljava/lang/StringBuilder;
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setMessagesAsDeleted() whereClauseSB="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 835
    const/4 v1, 0x0

    .line 837
    .local v1, "numberOfUpdatedMessage":I
    :try_start_2
    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "inbox"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 838
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setMessagesAsDeleted() - true. deleted state was set for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " messages"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    if-lez v1, :cond_2

    .line 842
    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 843
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ModelManager.setMessagesReadState() - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " messages were marked as saved"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 850
    :goto_1
    :try_start_3
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateWidgets()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 822
    .end local v1    # "numberOfUpdatedMessage":I
    .end local v2    # "updatedValues":Landroid/content/ContentValues;
    .end local v3    # "whereClauseSB":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 845
    .restart local v1    # "numberOfUpdatedMessage":I
    .restart local v2    # "updatedValues":Landroid/content/ContentValues;
    .restart local v3    # "whereClauseSB":Ljava/lang/StringBuilder;
    :cond_2
    :try_start_4
    const-string v4, "ModelManager"

    const-string v5, "ModelManager.setMessagesReadState() - true. read state was set for 0 messages."

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 847
    :catch_0
    move-exception v0

    .line 848
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_5
    const-string v4, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public setMessagesAsDeleted([Ljava/lang/String;Z)I
    .locals 8
    .param p1, "phoneNumbers"    # [Ljava/lang/String;
    .param p2, "deleteSaved"    # Z

    .prologue
    .line 862
    if-eqz p1, :cond_0

    array-length v4, p1

    if-nez v4, :cond_1

    .line 863
    :cond_0
    const/4 v1, 0x0

    .line 889
    :goto_0
    return v1

    .line 867
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 868
    .local v2, "updatedValues":Landroid/content/ContentValues;
    const-string v4, "forward_state"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 871
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "phone_number"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->buildWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 872
    .local v3, "whereClauseSB":Ljava/lang/StringBuilder;
    if-eqz p2, :cond_2

    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->buildAndSaved()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 873
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setMessagesAsDeleted() whereClauseSB="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    const/4 v1, 0x0

    .line 878
    .local v1, "numberOfUpdatedMessage":I
    :try_start_0
    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "inbox"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 879
    const-string v4, "ModelManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setMessagesAsDeleted() - true. deleted state was set for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " messages"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 887
    :goto_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateWidgets()V

    goto :goto_0

    .line 872
    .end local v1    # "numberOfUpdatedMessage":I
    :cond_2
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->buildAndNotSaved()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 883
    .restart local v1    # "numberOfUpdatedMessage":I
    :catch_0
    move-exception v0

    .line 884
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const-string v4, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public declared-synchronized setMessagesAsRead([J)I
    .locals 9
    .param p1, "rowIDs"    # [J

    .prologue
    .line 750
    monitor-enter p0

    :try_start_0
    array-length v5, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_0

    .line 751
    const/4 v1, 0x0

    .line 782
    :goto_0
    monitor-exit p0

    return v1

    .line 755
    :cond_0
    :try_start_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 756
    .local v3, "updatedValues":Landroid/content/ContentValues;
    const-string v5, "read_state"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 759
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 760
    .local v2, "rowIDsArr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "_id"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->buildWhere([J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 763
    .local v4, "whereClauseSB":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 765
    .local v1, "numberOfUpdatedMessage":I
    :try_start_2
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "inbox"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 766
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessagesReadState() - true. read state was set for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messages"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    array-length v5, p1

    if-ne v1, v5, :cond_1

    .line 772
    const/4 v5, 0x2

    invoke-virtual {p0, v5, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 773
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessagesReadState() - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messages were marked as saved"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 780
    :goto_1
    :try_start_3
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateWidgets()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 750
    .end local v1    # "numberOfUpdatedMessage":I
    .end local v2    # "rowIDsArr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v3    # "updatedValues":Landroid/content/ContentValues;
    .end local v4    # "whereClauseSB":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 775
    .restart local v1    # "numberOfUpdatedMessage":I
    .restart local v2    # "rowIDsArr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v3    # "updatedValues":Landroid/content/ContentValues;
    .restart local v4    # "whereClauseSB":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_4
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessagesReadState() - true. read state was set for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messages while total number of messages to set their saved status was "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 777
    :catch_0
    move-exception v0

    .line 778
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_5
    const-string v5, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized setMessagesSavedState([Ljava/lang/Long;I)I
    .locals 9
    .param p1, "messageIDs"    # [Ljava/lang/Long;
    .param p2, "savedState"    # I

    .prologue
    .line 633
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    array-length v5, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_2

    .line 634
    :cond_0
    const/4 v1, 0x0

    .line 665
    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    .line 638
    :cond_2
    :try_start_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 639
    .local v3, "updatedValues":Landroid/content/ContentValues;
    const-string v5, "saved_state"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 641
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "_id"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->buildWhere([Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 644
    .local v4, "whereClauseSB":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 646
    .local v1, "numberOfUpdatedMessage":I
    :try_start_2
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "inbox"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 647
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessageSavedState() savedState="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messages"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    if-lez v1, :cond_1

    .line 650
    const/4 v5, 0x4

    if-ne p2, v5, :cond_3

    .line 651
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 652
    .local v2, "params":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    int-to-long v6, v1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    const/4 v5, 0x3

    invoke-virtual {p0, v5, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 660
    .end local v2    # "params":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catch_0
    move-exception v0

    .line 661
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v5, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 633
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v1    # "numberOfUpdatedMessage":I
    .end local v3    # "updatedValues":Landroid/content/ContentValues;
    .end local v4    # "whereClauseSB":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 654
    .restart local v1    # "numberOfUpdatedMessage":I
    .restart local v3    # "updatedValues":Landroid/content/ContentValues;
    .restart local v4    # "whereClauseSB":Ljava/lang/StringBuilder;
    :cond_3
    const/4 v5, 0x2

    if-ne p2, v5, :cond_1

    .line 655
    const/16 v5, 0x8

    const/4 v6, 0x0

    :try_start_4
    invoke-virtual {p0, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized setMessagesSavedState([Ljava/lang/String;I)I
    .locals 9
    .param p1, "phoneNumbers"    # [Ljava/lang/String;
    .param p2, "savedState"    # I

    .prologue
    .line 671
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    array-length v5, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v5, :cond_2

    .line 672
    :cond_0
    const/4 v1, 0x0

    .line 703
    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    .line 676
    :cond_2
    :try_start_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 677
    .local v3, "updatedValues":Landroid/content/ContentValues;
    const-string v5, "saved_state"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 679
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "phone_number"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->buildWhere([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 681
    .local v4, "whereClauseSB":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 683
    .local v1, "numberOfUpdatedMessage":I
    :try_start_2
    iget-object v5, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "inbox"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 684
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessageSavedState() - saved state was set for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " messages"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    if-lez v1, :cond_1

    .line 687
    const/4 v5, 0x4

    if-ne p2, v5, :cond_4

    .line 688
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 689
    .local v2, "params":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    int-to-long v6, v1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    const/4 v5, 0x3

    invoke-virtual {p0, v5, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 694
    .end local v2    # "params":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_3
    :goto_1
    const-string v5, "ModelManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ModelManager.setMessagesSavedState() - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " savedState="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v5, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 671
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v1    # "numberOfUpdatedMessage":I
    .end local v3    # "updatedValues":Landroid/content/ContentValues;
    .end local v4    # "whereClauseSB":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 691
    .restart local v1    # "numberOfUpdatedMessage":I
    .restart local v3    # "updatedValues":Landroid/content/ContentValues;
    .restart local v4    # "whereClauseSB":Ljava/lang/StringBuilder;
    :cond_4
    const/4 v5, 0x2

    if-ne p2, v5, :cond_3

    .line 692
    const/16 v5, 0x8

    const/4 v6, 0x0

    :try_start_4
    invoke-virtual {p0, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized setNeedRefreshInbox(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "needRefresh"    # Ljava/lang/Boolean;

    .prologue
    .line 3104
    monitor-enter p0

    :try_start_0
    const-string v0, "needRefreshInbox"

    invoke-virtual {p0, v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3105
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is need refresh inbox flag was set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3106
    monitor-exit p0

    return-void

    .line 3104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setNotificationsBeforeReboot(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "haveNotifications"    # Ljava/lang/Boolean;

    .prologue
    .line 2981
    monitor-enter p0

    :try_start_0
    const-string v0, "didNotificationsExistBeforeReboot"

    invoke-virtual {p0, v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2983
    monitor-exit p0

    return-void

    .line 2981
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setPassword(Ljava/lang/String;)V
    .locals 6
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 3215
    monitor-enter p0

    :try_start_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-le v3, v4, :cond_0

    .line 3216
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".ldSignature"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Crypto"

    invoke-static {v3, p1, v4}, Lcom/att/mobile/android/infra/utils/Crypto;->encrypt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3224
    .local v1, "encryptedPassword":Ljava/lang/String;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ".ldSignature"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3225
    .local v2, "signedPassword":Ljava/lang/String;
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "passwordPref"

    invoke-virtual {v3, v4, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3226
    const-string v3, "ModelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "####ModelManager.setPassword() password = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " saved"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3230
    .end local v1    # "encryptedPassword":Ljava/lang/String;
    .end local v2    # "signedPassword":Ljava/lang/String;
    :goto_1
    monitor-exit p0

    return-void

    .line 3219
    :cond_0
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".ldSignature"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, p1, v4}, Lcom/att/mobile/android/infra/utils/Crypto;->encrypt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .restart local v1    # "encryptedPassword":Ljava/lang/String;
    goto :goto_0

    .line 3227
    .end local v1    # "encryptedPassword":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3228
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "ModelManager"

    const-string v4, "ModelManager.setPassword() failed"

    invoke-static {v3, v4, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3215
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized setPasswordChangeRequired(I)V
    .locals 2
    .param p1, "passwordChangeRequiredStatus"    # I

    .prologue
    .line 3279
    monitor-enter p0

    :try_start_0
    const-string v0, "PasswordChangeRequiredStatus"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3280
    monitor-exit p0

    return-void

    .line 3279
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSetupCompleted()V
    .locals 2

    .prologue
    .line 3118
    monitor-enter p0

    :try_start_0
    const-string v0, "ModelManager"

    const-string v1, "setSetupCompleted"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3120
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setPasswordChangeRequired(I)V

    .line 3121
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setCurrentSetupState(I)V

    .line 3122
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isSetupCompleted()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3123
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSetupCompleted(Ljava/lang/Boolean;)V

    .line 3124
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setFirstTimeUse(Ljava/lang/Boolean;)V

    .line 3125
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->insertWelcomeMessage()V

    .line 3127
    const-string v0, "createShortcutCheckbox"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3129
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->addShortcut()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3131
    :cond_0
    monitor-exit p0

    return-void

    .line 3118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSetupCompleted(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "setupCompleted"    # Ljava/lang/Boolean;

    .prologue
    .line 3203
    monitor-enter p0

    :try_start_0
    const-string v0, "isSetupCompleted"

    invoke-virtual {p0, v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3204
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is setup completed flag was set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3205
    monitor-exit p0

    return-void

    .line 3203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSetupStarted(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "setupStarted"    # Ljava/lang/Boolean;

    .prologue
    .line 3095
    monitor-enter p0

    :try_start_0
    const-string v0, "isSetupStarted"

    invoke-virtual {p0, v0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3096
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "is setup started flag was set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3097
    monitor-exit p0

    return-void

    .line 3095
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 2836
    .local p2, "value":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    .line 2838
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2839
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2861
    :goto_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2864
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2865
    monitor-exit p0

    return-void

    .line 2840
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_0
    :try_start_1
    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2841
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2836
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2842
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_1
    :try_start_2
    instance-of v0, p2, Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 2843
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/Float;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 2844
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_2
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2845
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 2846
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_3
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2847
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->prefsEditor:Landroid/content/SharedPreferences$Editor;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;, "TT;"
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 2849
    .restart local p2    # "value":Ljava/lang/Object;, "TT;"
    :cond_4
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ModelManager.setSharedPreference() - [key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] [value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]. T can be the one of the following types: Boolean, Integer, Float, Long, String."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public shouldCheckAttmStatusOnForeground()Z
    .locals 4

    .prologue
    .line 3345
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    const-string v1, "shouldCheckAttmStatusOnForeground"

    const-class v2, Ljava/lang/Boolean;

    const/4 v3, 0x0

    .line 3346
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 3345
    invoke-virtual {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized updateOrInsertMessageToInbox(Lcom/att/mobile/android/vvm/model/Message;)Z
    .locals 24
    .param p1, "message"    # Lcom/att/mobile/android/vvm/model/Message;

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/att/mobile/android/vvm/model/Message;->getUid()J

    move-result-wide v12

    .line 319
    .local v12, "messageUID":J
    invoke-virtual/range {p1 .. p1}, Lcom/att/mobile/android/vvm/model/Message;->getDateMillis()J

    move-result-wide v18

    .line 320
    .local v18, "timeStamp":J
    invoke-virtual/range {p1 .. p1}, Lcom/att/mobile/android/vvm/model/Message;->getSenderPhoneNumber()Ljava/lang/String;

    move-result-object v14

    .line 321
    .local v14, "phoneNumber":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/att/mobile/android/vvm/model/Message;->isRead()Z

    move-result v7

    .line 322
    .local v7, "isRead":Z
    invoke-virtual/range {p1 .. p1}, Lcom/att/mobile/android/vvm/model/Message;->isTuiskipped()Z

    move-result v8

    .line 323
    .local v8, "isTuiskipped":Z
    invoke-virtual/range {p1 .. p1}, Lcom/att/mobile/android/vvm/model/Message;->isUrgent()Z

    move-result v9

    .line 324
    .local v9, "isUrgent":Z
    invoke-virtual/range {p1 .. p1}, Lcom/att/mobile/android/vvm/model/Message;->isDeliveryStatus()Z

    move-result v10

    .line 328
    .local v10, "isdeliveryStatus":Z
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->deleteMessageMarkedAsOverwrite(J)Z

    move-result v11

    .line 331
    .local v11, "oldFileDeleted":Z
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 333
    .local v4, "contentValues":Landroid/content/ContentValues;
    const-wide/16 v16, -0x1

    .line 334
    .local v16, "rowID":J
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 341
    .local v15, "rowIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const-string v21, "read_state"

    if-eqz v7, :cond_2

    const/16 v20, 0x1

    :goto_0
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 342
    const-string v21, "tuiskipped"

    if-eqz v8, :cond_3

    const/16 v20, 0x1

    :goto_1
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 343
    const-string v21, "urgent"

    if-eqz v9, :cond_4

    const/16 v20, 0x1

    :goto_2
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 344
    const-string v21, "delivery_status"

    if-eqz v10, :cond_5

    const/16 v20, 0x1

    :goto_3
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    if-nez v11, :cond_0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v20, v0

    const-string v21, "inbox"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "uid"

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v23, " = "

    .line 349
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    .line 350
    move-object/from16 v0, v22

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    .line 348
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_7

    .line 356
    :cond_0
    const-string v20, "uid"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 357
    const-string v20, "time"

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 358
    const-string v20, "phone_number"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v20, "saved_state"

    const/16 v21, 0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v20, v0

    const-string v21, "inbox"

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v16

    const-wide/16 v20, -0x1

    cmp-long v20, v16, v20

    if-eqz v20, :cond_6

    .line 366
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    if-nez v7, :cond_1

    .line 375
    invoke-direct/range {p0 .. p0}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateWidgets()V

    .line 376
    new-instance v6, Landroid/content/Intent;

    const-string v20, "com.att.mobile.android.vvm.NEW_UNREAD_MESSAGE"

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 377
    .local v6, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    move-object/from16 v20, v0

    const-class v21, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 378
    const-string v20, "extra_new_message_row_id"

    move-object/from16 v0, v20

    move-wide/from16 v1, v16

    invoke-virtual {v6, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->context:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 381
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_1
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1, v15}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 382
    const-string v20, "ModelManager"

    const-string v21, "ModelManager.updateOrInsertMessageToInbox() - message was inserted"

    invoke-static/range {v20 .. v21}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 383
    const/16 v20, 0x1

    .line 400
    :goto_4
    monitor-exit p0

    return v20

    .line 341
    :cond_2
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 342
    :cond_3
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 343
    :cond_4
    const/16 v20, 0x0

    goto/16 :goto_2

    .line 344
    :cond_5
    const/16 v20, 0x0

    goto/16 :goto_3

    .line 387
    :cond_6
    :try_start_2
    const-string v20, "ModelManager"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "ModelManager.updateOrInsertMessageToInbox() - new message couldn\'t be inserted.  messageUID = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " timeStamp = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 388
    const/16 v20, 0x0

    goto :goto_4

    .line 390
    :catch_0
    move-exception v5

    .line 391
    .local v5, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v20, "ModelManager"

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v5}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 392
    const/16 v20, 0x0

    goto :goto_4

    .line 397
    .end local v5    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_7
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    const/16 v20, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1, v15}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->notifyListeners(ILjava/util/ArrayList;)V

    .line 399
    const-string v20, "ModelManager"

    const-string v21, "ModelManager.updateOrInsertMessageToInbox() - message read state was updated"

    invoke-static/range {v20 .. v21}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 400
    const/16 v20, 0x1

    goto :goto_4

    .line 318
    .end local v4    # "contentValues":Landroid/content/ContentValues;
    .end local v7    # "isRead":Z
    .end local v8    # "isTuiskipped":Z
    .end local v9    # "isUrgent":Z
    .end local v10    # "isdeliveryStatus":Z
    .end local v11    # "oldFileDeleted":Z
    .end local v12    # "messageUID":J
    .end local v14    # "phoneNumber":Ljava/lang/String;
    .end local v15    # "rowIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v16    # "rowID":J
    .end local v18    # "timeStamp":J
    :catchall_0
    move-exception v20

    monitor-exit p0

    throw v20
.end method

.method public declared-synchronized updateTuiskipped(Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2127
    .local p1, "tuiskippedUIs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    monitor-enter p0

    if-eqz p1, :cond_2

    :try_start_0
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2128
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2129
    .local v3, "where":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2130
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2131
    const-string v4, "uid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2132
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2133
    const-string v4, " OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2127
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .end local v3    # "where":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 2136
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .restart local v3    # "where":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2137
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v4, "tuiskipped"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2139
    :try_start_2
    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "inbox"

    .line 2140
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    .line 2139
    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2145
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .end local v3    # "where":Ljava/lang/StringBuilder;
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 2141
    .restart local v0    # "contentValues":Landroid/content/ContentValues;
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .restart local v3    # "where":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v1

    .line 2142
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    const-string v4, "ModelManager"

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized updateUid(JJ)Z
    .locals 9
    .param p1, "oldUID"    # J
    .param p3, "newUID"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 458
    monitor-enter p0

    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 459
    .local v1, "updatedValues":Landroid/content/ContentValues;
    const-string v4, "uid"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 462
    const-string v4, "was_downloaded"

    const/4 v5, 0x0

    .line 463
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 462
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 465
    :try_start_1
    iget-object v4, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "inbox"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "uid"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " = "

    .line 468
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 469
    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    .line 465
    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-ne v4, v2, :cond_0

    .line 477
    :goto_0
    monitor-exit p0

    return v2

    .line 473
    :catch_0
    move-exception v0

    .line 474
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    const-string v2, "ModelManager"

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    move v2, v3

    .line 477
    goto :goto_0

    .line 458
    .end local v1    # "updatedValues":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized wasMailboxRefreshedOnStartup()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 3048
    monitor-enter p0

    :try_start_0
    const-string v0, "wasInboxRefreshed"

    const-class v1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

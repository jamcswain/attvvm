.class public Lcom/att/mobile/android/vvm/model/db/MessageDo;
.super Ljava/lang/Object;
.source "MessageDo.java"


# instance fields
.field private fileName:Ljava/lang/String;

.field private forwardState:Ljava/lang/String;

.field private id:J

.field private isCheckedForDelete:Z

.field private phoneNumber:Ljava/lang/String;

.field private readState:Ljava/lang/String;

.field private savedState:Ljava/lang/String;

.field private time:Ljava/lang/String;

.field private transcription:Ljava/lang/String;

.field private uid:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getForwardState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->forwardState:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->id:J

    return-wide v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getReadState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->readState:Ljava/lang/String;

    return-object v0
.end method

.method public getSavedState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->savedState:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->time:Ljava/lang/String;

    return-object v0
.end method

.method public getTranscription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->transcription:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()J
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->uid:J

    return-wide v0
.end method

.method public isCheckedForDelete()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->isCheckedForDelete:Z

    return v0
.end method

.method public setCheckedForDelete(Z)V
    .locals 0
    .param p1, "isCheckedForDelete"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->isCheckedForDelete:Z

    .line 26
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->fileName:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setForwardState(Ljava/lang/String;)V
    .locals 0
    .param p1, "forwardState"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->forwardState:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 127
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->id:J

    .line 128
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->phoneNumber:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setReadState(Ljava/lang/String;)V
    .locals 0
    .param p1, "readState"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->readState:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setSavedState(Ljava/lang/String;)V
    .locals 0
    .param p1, "savedState"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->savedState:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->time:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setTranscription(Ljava/lang/String;)V
    .locals 0
    .param p1, "transcription"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->transcription:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setUid(J)V
    .locals 1
    .param p1, "uid"    # J

    .prologue
    .line 115
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/model/db/MessageDo;->uid:J

    .line 116
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/model/db/upgrade/AddWatsonStatusColumnToInboxTable;
.super Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
.source "AddWatsonStatusColumnToInboxTable.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AddWatsonStatusColumnToInboxTable"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;-><init>()V

    return-void
.end method


# virtual methods
.method public upgrade(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    const-string v0, "ALTER TABLE inbox ADD watson_transcription  INT(1) DEFAULT 0 "

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 20
    const-string v0, "AddWatsonStatusColumnToInboxTable"

    const-string v1, "Watson transcription column was added to threads table after upgrade"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    return-void
.end method

.class public abstract Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
.super Ljava/lang/Object;
.source "DBUpgrade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;
    }
.end annotation


# instance fields
.field private _listener:Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;->_listener:Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;

    return-void
.end method


# virtual methods
.method final notifyDBUpgradeEnded(Z)V
    .locals 2
    .param p1, "success"    # Z

    .prologue
    .line 19
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;->_listener:Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;

    if-eqz v1, :cond_0

    .line 20
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;->_listener:Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 21
    .local v0, "payload":Landroid/os/Message;
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 22
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 24
    .end local v0    # "payload":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public final setUpgradeListener(Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;->_listener:Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;

    .line 16
    return-void
.end method

.method public abstract upgrade(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
.end method

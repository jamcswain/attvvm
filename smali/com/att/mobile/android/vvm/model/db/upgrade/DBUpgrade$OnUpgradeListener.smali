.class public abstract Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;
.super Landroid/os/Handler;
.source "DBUpgrade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OnUpgradeListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 29
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;->onDBUpgradeEnded(Z)V

    .line 30
    return-void
.end method

.method protected abstract onDBUpgradeEnded(Z)V
.end method

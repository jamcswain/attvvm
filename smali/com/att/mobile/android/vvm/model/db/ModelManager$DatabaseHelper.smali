.class Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/db/ModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# static fields
.field private static final DB_PREFERENCES:Ljava/lang/String; = "db_pref"

.field private static final NEW_DB_VER:Ljava/lang/String; = "new_db_ver"

.field private static final OLD_DB_VER:Ljava/lang/String; = "old_db_ver"

.field private static final TAG:Ljava/lang/String; = "DatabaseHelper"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3424
    const-string v0, "messages"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 3426
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;->mContext:Landroid/content/Context;

    .line 3427
    return-void
.end method


# virtual methods
.method public declared-synchronized getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 9

    .prologue
    .line 3449
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3454
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v6, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;->mContext:Landroid/content/Context;

    const-string v7, "db_pref"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 3455
    .local v2, "local":Landroid/content/SharedPreferences;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v0

    .line 3456
    .local v0, "currentVersion":I
    const-string v6, "old_db_ver"

    invoke-interface {v2, v6, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 3457
    .local v4, "oldVer":I
    const-string v6, "new_db_ver"

    invoke-interface {v2, v6, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 3459
    .local v3, "newVer":I
    if-ne v3, v4, :cond_0

    .line 3485
    :goto_0
    monitor-exit p0

    return-object v1

    .line 3463
    :cond_0
    :try_start_1
    invoke-static {v4, v3}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgradeFactory;->getDBUpgradeScript(II)Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;

    move-result-object v5

    .line 3464
    .local v5, "upgradeScript":Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
    new-instance v6, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;

    invoke-direct {v6, p0, v2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;-><init>(Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;Landroid/content/SharedPreferences;I)V

    invoke-virtual {v5, v6}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;->setUpgradeListener(Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;)V

    .line 3481
    iget-object v6, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v1, v6}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;->upgrade(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 3483
    const-string v6, "DatabaseHelper"

    const-string v7, "getWritableDatabase - returned DB"

    invoke-static {v6, v7}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3449
    .end local v0    # "currentVersion":I
    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "local":Landroid/content/SharedPreferences;
    .end local v3    # "newVer":I
    .end local v4    # "oldVer":I
    .end local v5    # "upgradeScript":Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 3431
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3432
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 3436
    const-string v2, "DatabaseHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onUpgrade oldVesrion="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newVersion="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3438
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;->mContext:Landroid/content/Context;

    const-string v3, "db_pref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 3440
    .local v1, "local":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3441
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "old_db_ver"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 3442
    const-string v2, "new_db_ver"

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 3443
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3445
    return-void
.end method

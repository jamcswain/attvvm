.class public interface abstract Lcom/att/mobile/android/vvm/model/db/ModelManager$Inbox;
.super Ljava/lang/Object;
.source "ModelManager.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/db/ModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Inbox"
.end annotation


# static fields
.field public static final KEY_CAN_OVERWRITE:Ljava/lang/String; = "can_overwrite"

.field public static final KEY_DELIVERY_STATUS:Ljava/lang/String; = "delivery_status"

.field public static final KEY_FILE_NAME:Ljava/lang/String; = "file_name"

.field public static final KEY_IS_DELETED:Ljava/lang/String; = "forward_state"

.field public static final KEY_IS_READ:Ljava/lang/String; = "read_state"

.field public static final KEY_IS_TUISKIPPED:Ljava/lang/String; = "tuiskipped"

.field public static final KEY_PHONE_NUMBER:Ljava/lang/String; = "phone_number"

.field public static final KEY_SAVED_STATE:Ljava/lang/String; = "saved_state"

.field public static final KEY_TIME_STAMP:Ljava/lang/String; = "time"

.field public static final KEY_TRANSCRIPTION:Ljava/lang/String; = "transcription"

.field public static final KEY_UID:Ljava/lang/String; = "uid"

.field public static final KEY_URGENT_STATUS:Ljava/lang/String; = "urgent"

.field public static final KEY_WAS_DOWNLOADED:Ljava/lang/String; = "was_downloaded"

.field public static final KEY_WATSON_TRANSCRIPTION:Ljava/lang/String; = "watson_transcription"

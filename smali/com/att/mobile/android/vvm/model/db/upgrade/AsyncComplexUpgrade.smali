.class Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;
.super Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;
.source "AsyncComplexUpgrade.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private _context:Landroid/content/Context;

.field private _db:Landroid/database/sqlite/SQLiteDatabase;

.field private _isTransactionBegun:Z

.field private final _lock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_lock:Ljava/lang/Object;

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_isTransactionBegun:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 42
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 44
    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_lock:Ljava/lang/Object;

    monitor-enter v3

    .line 45
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_isTransactionBegun:Z

    .line 46
    sget-object v2, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->TAG:Ljava/lang/String;

    const-string v4, "db transaction begun notifying waiting threads"

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_lock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 48
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    const/4 v1, 0x0

    .line 53
    .local v1, "success":Z
    :try_start_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_db:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_context:Landroid/content/Context;

    invoke-super {p0, v2, v3}, Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;->upgrade(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 54
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 55
    const/4 v1, 0x1

    .line 59
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 60
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->notifyDBUpgradeEnded(Z)V

    .line 62
    :goto_0
    return-void

    .line 48
    .end local v1    # "success":Z
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 56
    .restart local v1    # "success":Z
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v2, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->TAG:Ljava/lang/String;

    const-string v3, "transaction faild on upgrade - rolling db back!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 59
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 60
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->notifyDBUpgradeEnded(Z)V

    goto :goto_0

    .line 59
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 60
    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->notifyDBUpgradeEnded(Z)V

    throw v2
.end method

.method public upgrade(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_db:Landroid/database/sqlite/SQLiteDatabase;

    .line 24
    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_context:Landroid/content/Context;

    .line 26
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 28
    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_lock:Ljava/lang/Object;

    monitor-enter v2

    .line 30
    :try_start_0
    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_isTransactionBegun:Z

    if-nez v1, :cond_0

    .line 31
    sget-object v1, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->TAG:Ljava/lang/String;

    const-string v3, "thread waiting for DB transaction to begin"

    invoke-static {v1, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->_lock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 38
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;->TAG:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 37
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

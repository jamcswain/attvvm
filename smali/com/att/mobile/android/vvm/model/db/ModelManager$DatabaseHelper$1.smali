.class Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;
.super Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;
.source "ModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;

.field final synthetic val$local:Landroid/content/SharedPreferences;

.field final synthetic val$newVer:I


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;Landroid/content/SharedPreferences;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;

    .prologue
    .line 3464
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;->this$0:Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper;

    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;->val$local:Landroid/content/SharedPreferences;

    iput p3, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;->val$newVer:I

    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade$OnUpgradeListener;-><init>()V

    return-void
.end method


# virtual methods
.method protected onDBUpgradeEnded(Z)V
    .locals 4
    .param p1, "success"    # Z

    .prologue
    .line 3468
    const-string v1, "DatabaseHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upgrade ended isSuccess = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3470
    if-eqz p1, :cond_0

    .line 3471
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;->val$local:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3472
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "old_db_ver"

    iget v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;->val$newVer:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 3473
    const-string v1, "new_db_ver"

    iget v2, p0, Lcom/att/mobile/android/vvm/model/db/ModelManager$DatabaseHelper$1;->val$newVer:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 3474
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3478
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

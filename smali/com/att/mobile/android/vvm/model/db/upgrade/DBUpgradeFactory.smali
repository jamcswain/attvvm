.class public final Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgradeFactory;
.super Ljava/lang/Object;
.source "DBUpgradeFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgradeFactory$NullObjectHolder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DBUpgradeFactory"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getDBUpgradeScript(II)Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
    .locals 6
    .param p0, "oldVersion"    # I
    .param p1, "newVersion"    # I

    .prologue
    const/4 v5, 0x2

    .line 23
    const-string v2, "DBUpgradeFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDBUpgradeScript() oldVersion = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newVersion = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    new-instance v0, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/model/db/upgrade/AsyncComplexUpgrade;-><init>()V

    .line 27
    .local v0, "complexUpgrader":Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;
    iget-object v1, v0, Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;->innerUpgradeScripts:Ljava/util/ArrayList;

    .line 30
    .local v1, "scripts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;>;"
    if-ge p0, v5, :cond_0

    if-lt p1, v5, :cond_0

    .line 32
    const-string v2, "DBUpgradeFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDBUpgradeScript() upgrade from version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    new-instance v2, Lcom/att/mobile/android/vvm/model/db/upgrade/AddWatsonStatusColumnToInboxTable;

    invoke-direct {v2}, Lcom/att/mobile/android/vvm/model/db/upgrade/AddWatsonStatusColumnToInboxTable;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    const-string v2, "DBUpgradeFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDBUpgradeScript() upgrade from version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgradeFactory$NullObjectHolder;->NULL:Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;

    .end local v0    # "complexUpgrader":Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;
    :cond_1
    return-object v0
.end method

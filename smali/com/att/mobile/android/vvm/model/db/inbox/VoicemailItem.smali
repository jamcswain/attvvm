.class public Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;
.super Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;
.source "VoicemailItem.java"


# instance fields
.field private deliveryFailure:Z

.field private fileName:Ljava/lang/String;

.field private forwardState:Ljava/lang/String;

.field private isCheckedForDelete:Z

.field private isUrgent:Z

.field private readState:I

.field private savedState:I

.field private time:J

.field private transcription:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;-><init>(Landroid/database/Cursor;)V

    .line 30
    const-string v0, "time"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->time:J

    .line 32
    const-string v0, "transcription"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->transcription:Ljava/lang/String;

    .line 34
    const-string v0, "read_state"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->readState:I

    .line 35
    const-string v0, "saved_state"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->savedState:I

    .line 37
    const-string v0, "urgent"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->isUrgent:Z

    .line 38
    const-string v0, "saved_state"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->deliveryFailure:Z

    .line 39
    const-string v0, "file_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->fileName:Ljava/lang/String;

    .line 40
    return-void

    :cond_0
    move v0, v2

    .line 37
    goto :goto_0

    :cond_1
    move v1, v2

    .line 38
    goto :goto_1
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getForwardState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->forwardState:Ljava/lang/String;

    return-object v0
.end method

.method public getIsUrgent()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->isUrgent:Z

    return v0
.end method

.method public getIsdeliveryFailure()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->deliveryFailure:Z

    return v0
.end method

.method public getReadState()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->readState:I

    return v0
.end method

.method public getSavedState()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->savedState:I

    return v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->time:J

    return-wide v0
.end method

.method public getTranscription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->transcription:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->uid:J

    return-wide v0
.end method

.method public isCheckedForDelete()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->isCheckedForDelete:Z

    return v0
.end method

.method public setCheckedForDelete(Z)V
    .locals 0
    .param p1, "isCheckedForDelete"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->isCheckedForDelete:Z

    .line 48
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->fileName:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setForwardState(Ljava/lang/String;)V
    .locals 0
    .param p1, "forwardState"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->forwardState:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setIsDeliveryFailure(Z)V
    .locals 0
    .param p1, "deliveryFailure"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->deliveryFailure:Z

    .line 167
    return-void
.end method

.method public setReadState(I)V
    .locals 0
    .param p1, "readState"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->readState:I

    .line 125
    return-void
.end method

.method public setSavedState(I)V
    .locals 0
    .param p1, "savedState"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->savedState:I

    .line 97
    return-void
.end method

.method public setTime(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->time:J

    .line 55
    return-void
.end method

.method public setTranscription(Ljava/lang/String;)V
    .locals 0
    .param p1, "transcription"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->transcription:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setUid(J)V
    .locals 1
    .param p1, "uid"    # J

    .prologue
    .line 138
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->uid:J

    .line 139
    return-void
.end method

.method public setUrgent(Z)V
    .locals 0
    .param p1, "urgent"    # Z

    .prologue
    .line 152
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->isUrgent:Z

    .line 153
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->uid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " transcription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->transcription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " fileName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " savedState="

    .line 185
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItem;->savedState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "viewHolder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 179
    invoke-super {p0, p1, p2}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->updateViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V

    .line 180
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;
.super Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;
.source "AggregatedVoicemailItem.java"


# instance fields
.field private newInboxMessagesCount:I

.field private oldInboxMessagesCount:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;-><init>(Landroid/database/Cursor;)V

    .line 27
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->oldInboxMessagesCount:I

    .line 28
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->newInboxMessagesCount:I

    .line 29
    return-void
.end method


# virtual methods
.method public getNewInboxMessagesCount()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->newInboxMessagesCount:I

    return v0
.end method

.method public getOldInboxMessagesCount()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/AggregatedVoicemailItem;->oldInboxMessagesCount:I

    return v0
.end method

.method public updateViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "viewHolder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 48
    return-void
.end method

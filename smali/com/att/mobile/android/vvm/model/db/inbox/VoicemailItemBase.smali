.class public abstract Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;
.super Ljava/lang/Object;
.source "VoicemailItemBase.java"


# instance fields
.field protected contact:Lcom/att/mobile/android/vvm/model/ContactObject;

.field protected id:J

.field protected phoneNumber:Ljava/lang/String;

.field protected uid:J


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->id:J

    .line 34
    const-string v0, "uid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->uid:J

    .line 35
    const-string v0, "phone_number"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->setPhoneNumber(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method private getPrivateNumberString()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 87
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0801af

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContact()Lcom/att/mobile/android/vvm/model/ContactObject;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->contact:Lcom/att/mobile/android/vvm/model/ContactObject;

    return-object v0
.end method

.method public getFormattedDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->phoneNumber:Ljava/lang/String;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->contact:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Utils;->getFormattedDisplayName(Ljava/lang/String;Lcom/att/mobile/android/vvm/model/ContactObject;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->id:J

    return-wide v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->uid:J

    return-wide v0
.end method

.method public setContact(Lcom/att/mobile/android/vvm/model/ContactObject;)V
    .locals 0
    .param p1, "contact"    # Lcom/att/mobile/android/vvm/model/ContactObject;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->contact:Lcom/att/mobile/android/vvm/model/ContactObject;

    .line 55
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->phoneNumber:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uid="

    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->uid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " phoneNumber="

    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " contact="

    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->contact:Lcom/att/mobile/android/vvm/model/ContactObject;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/db/inbox/VoicemailItemBase;->contact:Lcom/att/mobile/android/vvm/model/ContactObject;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/model/ContactObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public updateViewHolder(Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "viewHolder"    # Lcom/att/mobile/android/vvm/model/inbox/ListItemCursorRecyclerAdapterBase$ViewHolder;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 40
    return-void
.end method

.class Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;
.super Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
.source "ComplexUpgrade.java"


# instance fields
.field final innerUpgradeScripts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;-><init>()V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;->innerUpgradeScripts:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public upgrade(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/db/upgrade/ComplexUpgrade;->innerUpgradeScripts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;

    .line 15
    .local v0, "upgrader":Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
    invoke-virtual {v0, p1, p2}, Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;->upgrade(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    goto :goto_0

    .line 17
    .end local v0    # "upgrader":Lcom/att/mobile/android/vvm/model/db/upgrade/DBUpgrade;
    :cond_0
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/model/BaseMessage;
.super Ljava/lang/Object;
.source "BaseMessage.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public attachmentCount:I

.field public attachments:[Lcom/att/mobile/android/vvm/model/Attachment;

.field public backendIDIndex:J

.field public backendIDPrefix:Ljava/lang/String;

.field public conversationId:I

.field public created:Landroid/text/format/Time;

.field public errorType:I

.field public favourite:Z

.field public messageId:I

.field public messageStatus:I

.field public messageType:I

.field public modified:Landroid/text/format/Time;

.field public nativeId:I

.field public recipients:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public sender:Ljava/lang/String;

.field public senderName:Ljava/lang/String;

.field public subType:I

.field public syncStatus:I

.field public text:Ljava/lang/String;

.field public thumbnail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

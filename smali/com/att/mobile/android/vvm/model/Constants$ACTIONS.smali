.class public interface abstract Lcom/att/mobile/android/vvm/model/Constants$ACTIONS;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ACTIONS"
.end annotation


# static fields
.field public static final ACTION_BOOT_COMPLETED:Ljava/lang/String; = "com.att.mobile.android.vvm.BOOT_COMPLETED"

.field public static final ACTION_CLEAR_ALL_NOTIFICATIONS:Ljava/lang/String; = "com.att.mobile.android.vvm.INTENT_CLEAR_ALL_NOTIFICATIONS"

.field public static final ACTION_CLEAR_NOTIFICATION:Ljava/lang/String; = "com.att.mobile.android.vvm.INTENT_CLEAR_NOTIFICATION"

.field public static final ACTION_CLEAR_UPDATE_NOTIFICATION:Ljava/lang/String; = "com.att.mobile.android.vvm.INTENT_CLEAR_UPDATE_NOTIFICATION"

.field public static final ACTION_LAUNCH_ATTM:Ljava/lang/String; = "com.att.android.ACTION_LAUNCH_ATTM"

.field public static final ACTION_LAUNCH_INBOX_FROM_NOTIFICATION:Ljava/lang/String; = "com.att.mobile.android.vvm.LAUNCH_INBOX_FROM_NOTIFICATION"

.field public static final ACTION_LAUNCH_PLAYER_FROM_NOTIFICATION:Ljava/lang/String; = "com.att.mobile.android.vvm.LAUNCH_PLAYER_FROM_NOTIFICATION"

.field public static final ACTION_MAILBOX_CREATED:Ljava/lang/String; = "com.att.mobile.android.vvm.MAILBOX_CREATED"

.field public static final ACTION_NEW_SMS_NOTIFICATION:Ljava/lang/String; = "com.att.mobile.android.vvm.ACTION_NEW_MESSAGE_NOTIFICATION"

.field public static final ACTION_NEW_UNREAD_MESSAGE:Ljava/lang/String; = "com.att.mobile.android.vvm.NEW_UNREAD_MESSAGE"

.field public static final ACTION_PASSWORD_MISSMATCH:Ljava/lang/String; = "com.att.mobile.android.vvm.ACTION_PASSWORD_MISSMATCH"

.field public static final ACTION_UPDATE_NOTIFICATION:Ljava/lang/String; = "com.att.mobile.android.vvm.ACTION_UPDATE_NOTIFICATION"

.field public static final ACTION_UPDATE_NOTIFICATION_AFTER_REFRESH:Ljava/lang/String; = "com.att.mobile.android.vvm.ACTION_UPDATE_NOTIFICATION_AFTER_REFRESH"

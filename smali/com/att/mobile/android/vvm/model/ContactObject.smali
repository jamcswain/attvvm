.class public Lcom/att/mobile/android/vvm/model/ContactObject;
.super Ljava/lang/Object;
.source "ContactObject.java"


# static fields
.field private static final DUMMY_PHOTO_ID:I = 0x1


# instance fields
.field private mContactId:J

.field private mContactLookup:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;

.field private mPhoneLabel:Ljava/lang/String;

.field private mPhoneType:I

.field private mPhotoId:J

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "displayName"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mUri:Landroid/net/Uri;

    .line 28
    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mDisplayName:Ljava/lang/String;

    .line 29
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 30
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhotoId:J

    .line 32
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;JILjava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "displayName"    # Ljava/lang/String;
    .param p3, "contactId"    # J
    .param p5, "contactLookup"    # Ljava/lang/String;
    .param p6, "photoId"    # J
    .param p8, "phoneType"    # I
    .param p9, "phoneLabel"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mUri:Landroid/net/Uri;

    .line 18
    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mDisplayName:Ljava/lang/String;

    .line 19
    iput-wide p3, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mContactId:J

    .line 20
    iput-object p5, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mContactLookup:Ljava/lang/String;

    .line 21
    iput-wide p6, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhotoId:J

    .line 22
    iput p8, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhoneType:I

    .line 23
    iput-object p9, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhoneLabel:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public getContactId()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mContactId:J

    return-wide v0
.end method

.method public getContactLookup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mContactLookup:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhoneLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneType()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhoneType:I

    return v0
.end method

.method public getPhotoId()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhotoId:J

    return-wide v0
.end method

.method public getPhotoUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhotoId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 38
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " ContactObject mUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mDisplayName="

    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mContactId="

    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mContactId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mContactLookup="

    .line 70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mContactLookup:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mPhotoId="

    .line 71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhotoId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mPhoneType="

    .line 72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhoneType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mPhoneLabel="

    .line 73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mPhoneLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/ContactObject;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

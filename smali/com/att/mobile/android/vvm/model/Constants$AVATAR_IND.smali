.class public final enum Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/model/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AVATAR_IND"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

.field public static final enum BLUE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

.field public static final enum DEFAULT:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

.field public static final enum GREEN:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

.field public static final enum ORANGE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

.field public static final enum PURPLE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 467
    new-instance v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    const-string v1, "PURPLE"

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->PURPLE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    new-instance v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    const-string v1, "BLUE"

    invoke-direct {v0, v1, v3}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->BLUE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    new-instance v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    const-string v1, "GREEN"

    invoke-direct {v0, v1, v4}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->GREEN:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    new-instance v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    const-string v1, "ORANGE"

    invoke-direct {v0, v1, v5}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ORANGE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    new-instance v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v6}, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->DEFAULT:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    sget-object v1, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->PURPLE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    aput-object v1, v0, v2

    sget-object v1, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->BLUE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    aput-object v1, v0, v3

    sget-object v1, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->GREEN:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    aput-object v1, v0, v4

    sget-object v1, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->ORANGE:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    aput-object v1, v0, v5

    sget-object v1, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->DEFAULT:Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    aput-object v1, v0, v6

    sput-object v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->$VALUES:[Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 467
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 467
    const-class v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    return-object v0
.end method

.method public static values()[Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;
    .locals 1

    .prologue
    .line 467
    sget-object v0, Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->$VALUES:[Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    invoke-virtual {v0}, [Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/att/mobile/android/vvm/model/Constants$AVATAR_IND;

    return-object v0
.end method

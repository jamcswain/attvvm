.class public Lcom/att/mobile/android/vvm/model/Message;
.super Ljava/lang/Object;
.source "Message.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/model/Message$CAN_OVERWRITE_STATE;,
        Lcom/att/mobile/android/vvm/model/Message$WAS_DOWNLOADED_STATE;,
        Lcom/att/mobile/android/vvm/model/Message$UrgentState;,
        Lcom/att/mobile/android/vvm/model/Message$WatsonTranscriptionState;,
        Lcom/att/mobile/android/vvm/model/Message$ReadDeletedState;,
        Lcom/att/mobile/android/vvm/model/Message$SavedStates;,
        Lcom/att/mobile/android/vvm/model/Message$TranscriptionFixes;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/att/mobile/android/vvm/model/Message;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/att/mobile/android/vvm/model/Message;",
            ">;"
        }
    .end annotation
.end field

.field public static final FOLDER_INBOX:Ljava/lang/String; = "INBOX"

.field public static final MAX_CHUNK_SIZE:I = 0x400

.field public static final PRIORITY_HIGH:B = 0x1t

.field public static final PRIORITY_LOW:B = -0x1t

.field public static final PRIORITY_NONE:B = 0x0t

.field private static final SERVER_DATE_TIME_FORMAT:Ljava/lang/String; = "E, dd MMM yyyy HH:mm:ss Z"

.field private static final TAG:Ljava/lang/String; = "Message"


# instance fields
.field private contactImageUri:Landroid/net/Uri;

.field private contactLookupKey:Ljava/lang/String;

.field private dateMillis:J

.field private volatile fileName:Ljava/lang/String;

.field private isDeliveryStatus:Z

.field private phoneNumberLabel:Ljava/lang/String;

.field private phoneNumberType:I

.field private previousUid:J

.field private privateStatus:Z

.field private readStatus:Z

.field private rowId:J

.field private savedState:I

.field private senderBitmap:Landroid/graphics/Bitmap;

.field private senderDisplayName:Ljava/lang/String;

.field private senderPhoneNumber:Ljava/lang/String;

.field private transcription:Ljava/lang/String;

.field private tuiskipped:Z

.field private uid:J

.field private urgentStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576
    new-instance v0, Lcom/att/mobile/android/vvm/model/Message$1;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/model/Message$1;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/model/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/Message;->senderDisplayName:Ljava/lang/String;

    .line 136
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->readStatus:Z

    .line 141
    iput v0, p0, Lcom/att/mobile/android/vvm/model/Message;->urgentStatus:I

    .line 145
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->isDeliveryStatus:Z

    .line 150
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->privateStatus:Z

    .line 155
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->tuiskipped:Z

    .line 158
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/Message;->senderBitmap:Landroid/graphics/Bitmap;

    .line 169
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/Message;->contactLookupKey:Ljava/lang/String;

    .line 171
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/Message;->contactImageUri:Landroid/net/Uri;

    .line 178
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    .line 188
    iput-object v1, p0, Lcom/att/mobile/android/vvm/model/Message;->senderPhoneNumber:Ljava/lang/String;

    .line 192
    iput-wide v2, p0, Lcom/att/mobile/android/vvm/model/Message;->previousUid:J

    .line 213
    iput-wide v2, p0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    .line 214
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->readStatus:Z

    .line 215
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderDisplayName:Ljava/lang/String;

    .line 136
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/model/Message;->readStatus:Z

    .line 141
    iput v2, p0, Lcom/att/mobile/android/vvm/model/Message;->urgentStatus:I

    .line 145
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/model/Message;->isDeliveryStatus:Z

    .line 150
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/model/Message;->privateStatus:Z

    .line 155
    iput-boolean v2, p0, Lcom/att/mobile/android/vvm/model/Message;->tuiskipped:Z

    .line 158
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderBitmap:Landroid/graphics/Bitmap;

    .line 169
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->contactLookupKey:Ljava/lang/String;

    .line 171
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->contactImageUri:Landroid/net/Uri;

    .line 178
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    .line 188
    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderPhoneNumber:Ljava/lang/String;

    .line 192
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/att/mobile/android/vvm/model/Message;->previousUid:J

    .line 555
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    .line 556
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderDisplayName:Ljava/lang/String;

    .line 557
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/att/mobile/android/vvm/model/Message;->dateMillis:J

    .line 558
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->readStatus:Z

    .line 559
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/model/Message;->urgentStatus:I

    .line 560
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->isDeliveryStatus:Z

    .line 561
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->privateStatus:Z

    .line 562
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/att/mobile/android/vvm/model/Message;->tuiskipped:Z

    .line 563
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/model/Message;->savedState:I

    .line 564
    const-class v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderBitmap:Landroid/graphics/Bitmap;

    .line 565
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->transcription:Ljava/lang/String;

    .line 566
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->contactLookupKey:Ljava/lang/String;

    .line 567
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->contactImageUri:Landroid/net/Uri;

    .line 568
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    .line 569
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderPhoneNumber:Ljava/lang/String;

    .line 570
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->previousUid:J

    .line 571
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->rowId:J

    .line 572
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->phoneNumberLabel:Ljava/lang/String;

    .line 573
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/att/mobile/android/vvm/model/Message;->phoneNumberType:I

    .line 574
    return-void

    :cond_0
    move v0, v2

    .line 558
    goto :goto_0

    :cond_1
    move v0, v2

    .line 560
    goto :goto_1

    :cond_2
    move v0, v2

    .line 561
    goto :goto_2

    :cond_3
    move v1, v2

    .line 562
    goto :goto_3
.end method


# virtual methods
.method public compareTo(Lcom/att/mobile/android/vvm/model/Message;)I
    .locals 4
    .param p1, "another"    # Lcom/att/mobile/android/vvm/model/Message;

    .prologue
    .line 436
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->dateMillis:J

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/Message;->getDateMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 437
    const/4 v0, 0x1

    .line 441
    :goto_0
    return v0

    .line 438
    :cond_0
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->dateMillis:J

    invoke-virtual {p1}, Lcom/att/mobile/android/vvm/model/Message;->getDateMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 439
    const/4 v0, -0x1

    goto :goto_0

    .line 441
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lcom/att/mobile/android/vvm/model/Message;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/model/Message;->compareTo(Lcom/att/mobile/android/vvm/model/Message;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 590
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 220
    if-ne p0, p1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return v1

    .line 222
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 223
    goto :goto_0

    .line 224
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 225
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 226
    check-cast v0, Lcom/att/mobile/android/vvm/model/Message;

    .line 227
    .local v0, "other":Lcom/att/mobile/android/vvm/model/Message;
    iget-wide v4, p0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    iget-wide v6, v0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 228
    goto :goto_0
.end method

.method public getContactImageUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->contactImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getContactLookupKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->contactLookupKey:Ljava/lang/String;

    return-object v0
.end method

.method public getDateMillis()J
    .locals 2

    .prologue
    .line 405
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->dateMillis:J

    return-wide v0
.end method

.method public getFile(Landroid/content/Context;)Ljava/io/File;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 361
    iget v0, p0, Lcom/att/mobile/android/vvm/model/Message;->savedState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/att/mobile/android/vvm/model/Message;->savedState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->getInternalFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 374
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFileFullPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 322
    const/4 v0, 0x0

    .line 325
    .local v0, "fileFullPath":Ljava/lang/String;
    iget v1, p0, Lcom/att/mobile/android/vvm/model/Message;->savedState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/att/mobile/android/vvm/model/Message;->savedState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 329
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 330
    const-string v1, "Message"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Message.getFileFullPath() - file\'s full path is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 344
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumberLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->phoneNumberLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumberType()I
    .locals 1

    .prologue
    .line 505
    iget v0, p0, Lcom/att/mobile/android/vvm/model/Message;->phoneNumberType:I

    return v0
.end method

.method public getPreviousUid()J
    .locals 2

    .prologue
    .line 410
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->previousUid:J

    return-wide v0
.end method

.method public getRowId()J
    .locals 2

    .prologue
    .line 423
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->rowId:J

    return-wide v0
.end method

.method public getSavedIndex()I
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x0

    return v0
.end method

.method public getSavedState()I
    .locals 1

    .prologue
    .line 449
    iget v0, p0, Lcom/att/mobile/android/vvm/model/Message;->savedState:I

    return v0
.end method

.method public getSenderBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSenderDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getSenderPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getTranscription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->transcription:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()J
    .locals 2

    .prologue
    .line 237
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 181
    const/16 v0, 0x11

    .line 182
    .local v0, "result":I
    const/16 v1, 0x20f

    int-to-long v2, v1

    iget-wide v4, p0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 183
    return v0
.end method

.method public isDeliveryStatus()Z
    .locals 1

    .prologue
    .line 548
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->isDeliveryStatus:Z

    return v0
.end method

.method public isMessageLoaded()Z
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x0

    return v0
.end method

.method public isPrivateStatus()Z
    .locals 1

    .prologue
    .line 539
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->privateStatus:Z

    return v0
.end method

.method public isRead()Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->readStatus:Z

    return v0
.end method

.method public isTuiskipped()Z
    .locals 1

    .prologue
    .line 521
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->tuiskipped:Z

    return v0
.end method

.method public isUrgent()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 529
    iget v1, p0, Lcom/att/mobile/android/vvm/model/Message;->urgentStatus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContactImageUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "contactImageUri"    # Landroid/net/Uri;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/Message;->contactImageUri:Landroid/net/Uri;

    .line 33
    return-void
.end method

.method public setContactLookupKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "contactLookupKey"    # Ljava/lang/String;

    .prologue
    .line 517
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/Message;->contactLookupKey:Ljava/lang/String;

    .line 518
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 4
    .param p1, "dateStr"    # Ljava/lang/String;

    .prologue
    .line 254
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "E, dd MMM yyyy HH:mm:ss Z"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 256
    .local v1, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/att/mobile/android/vvm/model/Message;->dateMillis:J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    :goto_0
    return-void

    .line 257
    :catch_0
    move-exception v0

    .line 258
    .local v0, "e":Ljava/text/ParseException;
    const-string v2, "Message"

    invoke-virtual {v0}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/att/mobile/android/infra/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setDateLong(J)V
    .locals 1
    .param p1, "date"    # J

    .prologue
    .line 268
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/model/Message;->dateMillis:J

    .line 269
    return-void
.end method

.method public setDeliveryStatus(Z)V
    .locals 0
    .param p1, "isDeliveryStatus"    # Z

    .prologue
    .line 552
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/Message;->isDeliveryStatus:Z

    .line 553
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 379
    monitor-enter p0

    .line 380
    :try_start_0
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    .line 381
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 382
    monitor-exit p0

    .line 383
    return-void

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPhoneNumberLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumberLabel"    # Ljava/lang/String;

    .prologue
    .line 501
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/Message;->phoneNumberLabel:Ljava/lang/String;

    .line 502
    return-void
.end method

.method public setPhoneNumberType(I)V
    .locals 0
    .param p1, "phoneNumberType"    # I

    .prologue
    .line 509
    iput p1, p0, Lcom/att/mobile/android/vvm/model/Message;->phoneNumberType:I

    .line 510
    return-void
.end method

.method public setPreviousUid(J)V
    .locals 1
    .param p1, "previousUid"    # J

    .prologue
    .line 414
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/model/Message;->previousUid:J

    .line 415
    return-void
.end method

.method public setPrivateStatus(Z)V
    .locals 0
    .param p1, "privateStatus"    # Z

    .prologue
    .line 544
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/Message;->privateStatus:Z

    .line 545
    return-void
.end method

.method public setRead(Z)V
    .locals 0
    .param p1, "read"    # Z

    .prologue
    .line 285
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/Message;->readStatus:Z

    .line 286
    return-void
.end method

.method public setRowId(J)V
    .locals 1
    .param p1, "rowId"    # J

    .prologue
    .line 430
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/model/Message;->rowId:J

    .line 431
    return-void
.end method

.method public setSavedState(I)V
    .locals 0
    .param p1, "savedState"    # I

    .prologue
    .line 456
    iput p1, p0, Lcom/att/mobile/android/vvm/model/Message;->savedState:I

    .line 457
    return-void
.end method

.method public setSenderBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "senderBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/Message;->senderBitmap:Landroid/graphics/Bitmap;

    .line 471
    return-void
.end method

.method public setSenderDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "senderServerDisplayName"    # Ljava/lang/String;

    .prologue
    .line 493
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/Message;->senderDisplayName:Ljava/lang/String;

    .line 494
    return-void
.end method

.method public setSenderPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneString"    # Ljava/lang/String;

    .prologue
    .line 480
    iput-object p1, p0, Lcom/att/mobile/android/vvm/model/Message;->senderPhoneNumber:Ljava/lang/String;

    .line 481
    return-void
.end method

.method public setTranscription(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "transcription"    # Ljava/lang/String;

    .prologue
    .line 393
    if-nez p2, :cond_0

    .line 394
    const-string v0, " "

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->transcription:Ljava/lang/String;

    .line 400
    :goto_0
    return-void

    .line 395
    :cond_0
    const-string v0, "Audio message from"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "cannot be processed into text."

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    const v0, 0x7f0801e4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->transcription:Ljava/lang/String;

    goto :goto_0

    .line 398
    :cond_1
    iput-object p2, p0, Lcom/att/mobile/android/vvm/model/Message;->transcription:Ljava/lang/String;

    goto :goto_0
.end method

.method public setTuiskipped(Z)V
    .locals 0
    .param p1, "tuiskipped"    # Z

    .prologue
    .line 525
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/model/Message;->tuiskipped:Z

    .line 526
    return-void
.end method

.method public setUid(J)V
    .locals 1
    .param p1, "uid"    # J

    .prologue
    .line 245
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    .line 246
    return-void
.end method

.method public setUrgentStatus(I)V
    .locals 0
    .param p1, "urgentStatus"    # I

    .prologue
    .line 534
    iput p1, p0, Lcom/att/mobile/android/vvm/model/Message;->urgentStatus:I

    .line 535
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 299
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 300
    .local v0, "result":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Message[uid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 301
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",read="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/att/mobile/android/vvm/model/Message;->readStatus:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 302
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",date="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/model/Message;->dateMillis:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 303
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ",fileName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 305
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 595
    iget-wide v4, p0, Lcom/att/mobile/android/vvm/model/Message;->uid:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 596
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 597
    iget-wide v4, p0, Lcom/att/mobile/android/vvm/model/Message;->dateMillis:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 598
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->readStatus:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 599
    iget v0, p0, Lcom/att/mobile/android/vvm/model/Message;->urgentStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 600
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->isDeliveryStatus:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 601
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->privateStatus:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 602
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/model/Message;->tuiskipped:Z

    if-eqz v0, :cond_3

    :goto_3
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 603
    iget v0, p0, Lcom/att/mobile/android/vvm/model/Message;->savedState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 604
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 605
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->transcription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 606
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->contactLookupKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->contactImageUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 608
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->fileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 609
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->senderPhoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 610
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->previousUid:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 611
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/model/Message;->rowId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 612
    iget-object v0, p0, Lcom/att/mobile/android/vvm/model/Message;->phoneNumberLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 613
    iget v0, p0, Lcom/att/mobile/android/vvm/model/Message;->phoneNumberType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 614
    return-void

    :cond_0
    move v0, v2

    .line 598
    goto :goto_0

    :cond_1
    move v0, v2

    .line 600
    goto :goto_1

    :cond_2
    move v0, v2

    .line 601
    goto :goto_2

    :cond_3
    move v1, v2

    .line 602
    goto :goto_3
.end method

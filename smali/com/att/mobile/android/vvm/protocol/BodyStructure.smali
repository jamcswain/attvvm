.class public Lcom/att/mobile/android/vvm/protocol/BodyStructure;
.super Ljava/lang/Object;
.source "BodyStructure.java"


# instance fields
.field private bodyParts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/protocol/BodyPart;",
            ">;"
        }
    .end annotation
.end field

.field private uid:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->bodyParts:Ljava/util/ArrayList;

    .line 19
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "uid"    # J

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->bodyParts:Ljava/util/ArrayList;

    .line 22
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->uid:J

    .line 23
    return-void
.end method


# virtual methods
.method public addBodyPart(Lcom/att/mobile/android/vvm/protocol/BodyPart;)V
    .locals 1
    .param p1, "bodyPart"    # Lcom/att/mobile/android/vvm/protocol/BodyPart;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->bodyParts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public getBodyPart(I)Lcom/att/mobile/android/vvm/protocol/BodyPart;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 30
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->bodyParts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/BodyPart;

    return-object v0
.end method

.method public getBodyParts()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/protocol/BodyPart;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->bodyParts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getUid()J
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->uid:J

    return-wide v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->bodyParts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.class public Lcom/att/mobile/android/vvm/protocol/BodyPart;
.super Ljava/lang/Object;
.source "BodyPart.java"


# instance fields
.field bodyPartStr:Ljava/lang/String;

.field contentType:Ljava/lang/String;

.field duration:I

.field isVttAluReason:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "bodyPartStr"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->bodyPartStr:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getBodyPartStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->bodyPartStr:Ljava/lang/String;

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->duration:I

    return v0
.end method

.method public getIsVttAluReason()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->isVttAluReason:Z

    return v0
.end method

.method public setBodyPartStr(Ljava/lang/String;)V
    .locals 0
    .param p1, "bodyPartStr"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->bodyPartStr:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->contentType:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->duration:I

    .line 50
    return-void
.end method

.method public setIsVttAluReason(Z)V
    .locals 0
    .param p1, "isVttAluReason"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/protocol/BodyPart;->isVttAluReason:Z

    .line 42
    return-void
.end method

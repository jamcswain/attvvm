.class public Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;
.super Ljava/lang/Object;
.source "IMAP4Parser.java"


# static fields
.field private static final CHAR_CLOSE_BRACKET:C = ')'

.field private static final CHAR_OPEN_BRACKET:C = '('

.field private static final CHAR_QUESTION_MARK:C = '?'

.field private static final NULL_STRING:Ljava/lang/String; = ""

.field private static final SPACE:B = 0x20t

.field private static final STR_DATE:Ljava/lang/String; = "date"

.field private static final STR_ENCODING_INDICATOR:Ljava/lang/String; = "=?"

.field private static final STR_FETCH:Ljava/lang/String; = "fetch"

.field private static final STR_FLAG_DELETED:Ljava/lang/String; = "\\deleted"

.field private static final STR_FLAG_DSN:Ljava/lang/String; = "dsn"

.field private static final STR_FLAG_SEEN:Ljava/lang/String; = "\\seen"

.field private static final STR_FLAG_TUISKIPPED:Ljava/lang/String; = "tuiskipped"

.field private static final STR_FLAG_URGENT:Ljava/lang/String; = "priority"

.field private static final STR_HEADER_CLI_NUMBER:Ljava/lang/String; = "x-cli_number"

.field private static final STR_HEADER_X_ALU_COMP_REASON:Ljava/lang/String; = "x-alu-comp-reason"

.field private static final STR_HEADER_X_ALU_PREVIOUS_UID:Ljava/lang/String; = "x-alu-previous-uid"

.field private static final STR_HEADER_X_ALU_PREVIOUS_UID_REASON:Ljava/lang/String; = "x-alu-previous-uid-reason"

.field private static final STR_UID:Ljava/lang/String; = "uid"

.field private static final TAG:Ljava/lang/String; = "IMAP4Parser"


# instance fields
.field private networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/network/NetworkHandler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkHandler"    # Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    .line 59
    return-void
.end method

.method private createImapCommandStreamsHashMap(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541
    .local p1, "imapCommands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "streams":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 542
    .local v1, "localHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 543
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 546
    :cond_0
    return-object v1
.end method

.method private getImapCommands(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "pattern"    # Ljava/util/regex/Pattern;
    .param p2, "vars"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561
    const-string v4, "/private/vendor/vendor.alu/messaging/Greetings/[A-Za-z]+"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    .line 562
    invoke-virtual {p1, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 563
    .local v2, "imapCommandMatcher":Ljava/util/regex/Matcher;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 564
    .local v3, "imapCommands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 565
    .local v0, "currentGroup":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 567
    :try_start_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 568
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 569
    :catch_0
    move-exception v1

    .line 570
    .local v1, "e":Ljava/lang/IllegalStateException;
    const-string v4, "IMAP4Parser"

    const-string v5, "IMAP4Parser getImapCommands unsuccesful grouping"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 575
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :cond_0
    const-string v4, "/private/vendor/vendor.alu/messaging/RecordedName"

    invoke-virtual {p2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 576
    const-string v4, "/private/vendor/vendor.alu/messaging/RecordedName"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    :cond_1
    return-object v3
.end method

.method private getStreamsStrings(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "pattern"    # Ljava/util/regex/Pattern;
    .param p2, "vars"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 593
    const-string v7, "/private/vendor/vendor.alu/messaging/Greetings/.*AMR"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 594
    .local v5, "streamsPattern":Ljava/util/regex/Pattern;
    invoke-virtual {v5, p2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    .line 595
    .local v0, "localStrArr":[Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 596
    .local v3, "retArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    array-length v8, v0

    move v7, v6

    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v4, v0, v7

    .line 597
    .local v4, "stream":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v9, ""

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 598
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 602
    .end local v4    # "stream":Ljava/lang/String;
    :cond_1
    const-string v7, "/private/vendor/vendor.alu/messaging/RecordedName/.*AMR"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 603
    .local v2, "nameStreamsPattern":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v1

    .line 605
    .local v1, "nameLocalStrArr":[Ljava/lang/String;
    array-length v7, v1

    :goto_1
    if-ge v6, v7, :cond_3

    aget-object v4, v1, v6

    .line 606
    .restart local v4    # "stream":Ljava/lang/String;
    if-eqz v4, :cond_2

    const-string v8, ""

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 607
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 605
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 611
    .end local v4    # "stream":Ljava/lang/String;
    :cond_3
    return-object v3
.end method

.method private stripQuotedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "responseStr"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x22

    .line 637
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 638
    .local v1, "start":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 639
    .local v0, "end":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-eq v1, v0, :cond_0

    .line 640
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 642
    .end local p1    # "responseStr":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private stripResponseString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "responseStr"    # Ljava/lang/String;

    .prologue
    .line 622
    const/16 v2, 0x28

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 623
    .local v1, "start":I
    const/16 v2, 0x29

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 624
    .local v0, "end":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-eq v1, v0, :cond_0

    .line 625
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 627
    .end local p1    # "responseStr":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method protected static toLowerCase(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "genCase"    # Ljava/lang/String;

    .prologue
    const/4 v7, -0x1

    .line 199
    const/4 v4, 0x0

    .line 200
    .local v4, "retVal":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 201
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 204
    const/4 v0, 0x0

    .line 205
    .local v0, "currReplacementIndex":I
    const/4 v3, 0x0

    .line 206
    .local v3, "replacementStartPoint":I
    const/4 v2, 0x0

    .line 208
    .local v2, "replacementEndPoint":I
    :try_start_0
    const-string v5, "=?"

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 210
    :goto_0
    if-eq v0, v7, :cond_2

    .line 211
    add-int/lit8 v0, v0, 0x2

    .line 212
    const/16 v5, 0x3f

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 214
    if-le v3, v7, :cond_2

    .line 215
    const/16 v5, 0x3f

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 216
    const/16 v5, 0x3f

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 218
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 219
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 221
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 223
    if-ne v2, v7, :cond_1

    .line 224
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 234
    :cond_0
    :goto_1
    const-string v5, "=?"

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 226
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 227
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_1

    .line 236
    :catch_0
    move-exception v1

    .line 237
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "IMAP4Parser"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 240
    .end local v0    # "currReplacementIndex":I
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "replacementEndPoint":I
    .end local v3    # "replacementStartPoint":I
    :cond_2
    return-object v4
.end method

.method private unfoldHeader(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .param p1, "firstLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 162
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 163
    .local v2, "headerData":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 164
    .local v1, "count":I
    move-object v0, p1

    .line 165
    .local v0, "buffer":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v3, v4, 0x2

    .line 170
    .local v3, "octets":I
    :goto_0
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "\t"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 171
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 179
    :cond_1
    :goto_1
    const/16 v4, 0x29

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    if-nez v4, :cond_3

    .line 188
    const/4 v0, 0x0

    .line 189
    return-object v2

    .line 174
    :cond_2
    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 175
    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 183
    :cond_3
    new-instance v0, Ljava/lang/String;

    .end local v0    # "buffer":Ljava/lang/String;
    iget-object v4, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v4}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>([B)V

    .line 184
    .restart local v0    # "buffer":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v3

    add-int/lit8 v3, v4, 0x2

    .line 186
    const-string v4, "IMAP4Parser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unfoldHeader() received <- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected parseBodiesStructure(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;
    .locals 8
    .param p1, "responseStr"    # Ljava/lang/String;

    .prologue
    .line 341
    new-instance v3, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;

    invoke-direct {v3}, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;-><init>()V

    .line 343
    .local v3, "response":Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;
    const-string v4, "\\*"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 344
    .local v0, "bodiesStructureStr":[Ljava/lang/String;
    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    .line 345
    .local v2, "bodyStructureStr":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v6, "BODYSTRUCTURE"

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 346
    invoke-virtual {p0, v2}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->parseBodyStructure(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/BodyStructure;

    move-result-object v1

    .line 347
    .local v1, "bodyStructure":Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;->getBodyStructureList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    .end local v1    # "bodyStructure":Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 350
    .end local v2    # "bodyStructureStr":Ljava/lang/String;
    :cond_1
    return-object v3
.end method

.method protected parseBodyPart(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/BodyPart;
    .locals 14
    .param p1, "bodyPartStr"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 405
    const/4 v7, 0x0

    .local v7, "variable":Ljava/lang/String;
    const/4 v6, 0x0

    .line 407
    .local v6, "value":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripResponseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 408
    .local v4, "partStr":Ljava/lang/String;
    new-instance v0, Lcom/att/mobile/android/vvm/protocol/BodyPart;

    invoke-direct {v0, v4}, Lcom/att/mobile/android/vvm/protocol/BodyPart;-><init>(Ljava/lang/String;)V

    .line 410
    .local v0, "bodyPart":Lcom/att/mobile/android/vvm/protocol/BodyPart;
    const/16 v11, 0x28

    invoke-virtual {v4, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 411
    .local v9, "xHeaderStartIndex":I
    const/16 v11, 0x29

    invoke-virtual {v4, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    .line 414
    .local v8, "xHeaderEndIndex":I
    invoke-virtual {v4, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 415
    .local v1, "contentType":Ljava/lang/String;
    const-string v11, "\\s+"

    invoke-virtual {v1, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 416
    .local v2, "contentTypeParts":[Ljava/lang/String;
    aget-object v11, v2, v12

    invoke-direct {p0, v11}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripQuotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 417
    array-length v11, v2

    if-le v11, v13, :cond_0

    .line 418
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget-object v12, v2, v13

    invoke-direct {p0, v12}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripQuotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 420
    :cond_0
    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/protocol/BodyPart;->setContentType(Ljava/lang/String;)V

    .line 423
    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v4, v9, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripResponseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 424
    .local v10, "xHeaderStr":Ljava/lang/String;
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v11, " "

    invoke-direct {v5, v10, v11}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    .local v5, "st":Ljava/util/StringTokenizer;
    :cond_1
    :goto_0
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 427
    :try_start_0
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 428
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 429
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 434
    :cond_2
    :goto_1
    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    .line 435
    invoke-direct {p0, v7}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripQuotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "x-alu-comp-reason"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 436
    invoke-direct {p0, v6}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripQuotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 437
    const-string v11, "vtt"

    invoke-virtual {v6, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    invoke-virtual {v0, v11}, Lcom/att/mobile/android/vvm/protocol/BodyPart;->setIsVttAluReason(Z)V

    goto :goto_0

    .line 431
    :catch_0
    move-exception v3

    .line 432
    .local v3, "e":Ljava/util/NoSuchElementException;
    const-string v11, "IMAP4Parser"

    invoke-virtual {v3}, Ljava/util/NoSuchElementException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 442
    .end local v3    # "e":Ljava/util/NoSuchElementException;
    :cond_3
    return-object v0
.end method

.method protected parseBodyStructure(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    .locals 14
    .param p1, "bodyStructureStr"    # Ljava/lang/String;

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripResponseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 365
    .local v4, "fetchStr":Ljava/lang/String;
    const-string v11, "BODYSTRUCTURE"

    invoke-virtual {v4, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 366
    .local v2, "bodyStructureIndex":I
    const/4 v11, 0x0

    invoke-virtual {v4, v11, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 367
    .local v10, "uidPair":Ljava/lang/String;
    const-string v11, "\\s+"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    aget-object v11, v11, v12

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 368
    .local v9, "uid":Ljava/lang/String;
    new-instance v1, Lcom/att/mobile/android/vvm/protocol/BodyStructure;

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-direct {v1, v12, v13}, Lcom/att/mobile/android/vvm/protocol/BodyStructure;-><init>(J)V

    .line 370
    .local v1, "bodyStructure":Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripResponseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 371
    .local v8, "partsStr":Ljava/lang/String;
    const/4 v6, 0x0

    .line 372
    .local v6, "numOfOpenBrackets":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 374
    .local v7, "part":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    if-ge v5, v11, :cond_3

    .line 375
    invoke-virtual {v8, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 376
    .local v3, "c":C
    const/16 v11, 0x28

    if-ne v3, v11, :cond_2

    .line 377
    add-int/lit8 v6, v6, 0x1

    .line 382
    :cond_0
    :goto_1
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 385
    if-nez v6, :cond_1

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x28

    invoke-static {v12}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 386
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->parseBodyPart(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/BodyPart;

    move-result-object v0

    .line 387
    .local v0, "bodyPart":Lcom/att/mobile/android/vvm/protocol/BodyPart;
    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/protocol/BodyStructure;->getBodyParts()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    new-instance v7, Ljava/lang/StringBuilder;

    .end local v7    # "part":Ljava/lang/StringBuilder;
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 374
    .end local v0    # "bodyPart":Lcom/att/mobile/android/vvm/protocol/BodyPart;
    .restart local v7    # "part":Ljava/lang/StringBuilder;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 379
    :cond_2
    const/16 v11, 0x29

    if-ne v3, v11, :cond_0

    .line 380
    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    .line 394
    .end local v3    # "c":C
    :cond_3
    return-object v1
.end method

.method protected parseGreetingMetaData(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;
    .locals 9
    .param p1, "responseStr"    # Ljava/lang/String;

    .prologue
    .line 500
    new-instance v4, Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;

    invoke-direct {v4}, Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;-><init>()V

    .line 501
    .local v4, "response":Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;
    const/4 v3, 0x0

    .line 503
    .local v3, "pattern":Ljava/util/regex/Pattern;
    if-eqz p1, :cond_0

    const-string v7, ""

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 504
    const-string v7, "IMAP4Parser"

    const-string v8, "IMAP4Parser::parseGreetingMetaData => response string exists!"

    invoke-static {v7, v8}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :try_start_0
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->stripResponseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 510
    .local v6, "vars":Ljava/lang/String;
    invoke-direct {p0, v3, v6}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->getStreamsStrings(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 513
    .local v5, "streams":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v3, v6}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->getImapCommands(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 516
    .local v2, "imapCommands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v2, v5}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->createImapCommandStreamsHashMap(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v1

    .line 519
    .local v1, "imapCommandStreamHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v4, v1}, Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;->addVariablesRange(Ljava/util/HashMap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 526
    .end local v1    # "imapCommandStreamHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "imapCommands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "streams":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "vars":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v4

    .line 520
    :catch_0
    move-exception v0

    .line 521
    .local v0, "e":Ljava/lang/Exception;
    const-string v7, "IMAP4Parser"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public parseHeader(Lcom/att/mobile/android/vvm/protocol/response/FetchHeadersResponse;Ljava/lang/String;Landroid/content/Context;)Lcom/att/mobile/android/vvm/model/Message;
    .locals 16
    .param p1, "response"    # Lcom/att/mobile/android/vvm/protocol/response/FetchHeadersResponse;
    .param p2, "firstLine"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 81
    new-instance v8, Lcom/att/mobile/android/vvm/model/Message;

    invoke-direct {v8}, Lcom/att/mobile/android/vvm/model/Message;-><init>()V

    .line 83
    .local v8, "resultMessage":Lcom/att/mobile/android/vvm/model/Message;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->unfoldHeader(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v2

    .line 86
    .local v2, "headerData":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    const/4 v4, -0x1

    .line 87
    .local v4, "index":I
    const/4 v10, 0x0

    .line 89
    .local v10, "tempStr":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v11

    if-ge v3, v11, :cond_b

    .line 90
    invoke-virtual {v2, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 91
    .local v5, "nextHeaderLine":Ljava/lang/String;
    invoke-static {v5}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 92
    .local v6, "nextHeaderLineLower":Ljava/lang/String;
    const-string v11, "IMAP4Parser"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "IMAP4Parser.parseHeader() received header = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_5

    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    const/16 v12, 0x2a

    if-ne v11, v12, :cond_5

    const-string v11, "fetch"

    invoke-virtual {v6, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_5

    .line 94
    const-string v11, "uid"

    invoke-virtual {v6, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 95
    if-ltz v4, :cond_0

    .line 96
    const-string v11, "uid"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    add-int/2addr v4, v11

    .line 98
    const/16 v11, 0x20

    invoke-virtual {v5, v11, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v11

    invoke-virtual {v5, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v8, v12, v13}, Lcom/att/mobile/android/vvm/model/Message;->setUid(J)V

    .line 102
    :cond_0
    const-string v11, "\\seen"

    invoke-virtual {v6, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 103
    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Lcom/att/mobile/android/vvm/model/Message;->setRead(Z)V

    .line 106
    :cond_1
    const-string v11, "tuiskipped"

    invoke-virtual {v6, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 107
    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Lcom/att/mobile/android/vvm/model/Message;->setTuiskipped(Z)V

    .line 110
    :cond_2
    const-string v11, "priority"

    invoke-virtual {v6, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 111
    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Lcom/att/mobile/android/vvm/model/Message;->setUrgentStatus(I)V

    .line 113
    :cond_3
    const-string v11, "dsn"

    invoke-virtual {v6, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 114
    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Lcom/att/mobile/android/vvm/model/Message;->setDeliveryStatus(Z)V

    .line 146
    :cond_4
    :goto_1
    const/4 v5, 0x0

    .line 147
    const/4 v6, 0x0

    .line 89
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 116
    :cond_5
    const-string v11, "date"

    invoke-virtual {v6, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 117
    const-string v11, "date"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/att/mobile/android/vvm/model/Message;->setDate(Ljava/lang/String;)V

    goto :goto_1

    .line 118
    :cond_6
    const-string v11, "x-cli_number"

    invoke-virtual {v6, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 119
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    const-string v12, "x-cli_number"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-le v11, v12, :cond_4

    .line 120
    const-string v11, "x-cli_number"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 122
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_7

    const-string v7, ""

    .line 125
    .local v7, "phoneNumber":Ljava/lang/String;
    :goto_2
    invoke-static {v7}, Lcom/att/mobile/android/infra/utils/Utils;->convertPhoneNumberToE164(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 126
    invoke-virtual {v8, v7}, Lcom/att/mobile/android/vvm/model/Message;->setSenderPhoneNumber(Ljava/lang/String;)V

    goto :goto_1

    .end local v7    # "phoneNumber":Ljava/lang/String;
    :cond_7
    move-object v7, v10

    .line 122
    goto :goto_2

    .line 128
    :cond_8
    const-string v11, "x-alu-previous-uid-reason"

    invoke-virtual {v6, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 129
    const-string v11, "IMAP4Parser"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "IMAP4Parser.parseHeader() STR_HEADER_X_ALU_PREVIOUS_UID_REASON = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 130
    :cond_9
    const-string v11, "x-alu-comp-reason"

    invoke-virtual {v6, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 131
    const-string v11, "IMAP4Parser"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "IMAP4Parser.parseHeader() STR_HEADER_X_ALU_COMP_REASON = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 133
    :cond_a
    const-string v11, "x-alu-previous-uid"

    invoke-virtual {v6, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 135
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v11

    const-string v12, "x-alu-previous-uid"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-le v11, v12, :cond_4

    .line 136
    const-string v11, "-"

    invoke-virtual {v5, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 137
    .local v9, "start":I
    if-lez v9, :cond_4

    add-int/lit8 v11, v9, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v11, v12, :cond_4

    .line 138
    add-int/lit8 v11, v9, 0x1

    invoke-virtual {v5, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 139
    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v8, v12, v13}, Lcom/att/mobile/android/vvm/model/Message;->setPreviousUid(J)V

    .line 140
    const-string v11, "IMAP4Parser"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "IMAP4Parser.parseHeader() set message previous uid "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/model/Message;->getPreviousUid()J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " to message "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 141
    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/model/Message;->getUid()J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 140
    invoke-static {v11, v12}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 149
    .end local v5    # "nextHeaderLine":Ljava/lang/String;
    .end local v6    # "nextHeaderLineLower":Ljava/lang/String;
    .end local v9    # "start":I
    :cond_b
    invoke-virtual {v2}, Ljava/util/Vector;->removeAllElements()V

    .line 150
    const/4 v2, 0x0

    .line 152
    return-object v8
.end method

.method protected parseMetaData(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;
    .locals 10
    .param p1, "responseStr"    # Ljava/lang/String;

    .prologue
    .line 459
    new-instance v2, Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;

    invoke-direct {v2}, Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;-><init>()V

    .line 460
    .local v2, "response":Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;
    const/4 v6, 0x0

    .local v6, "variable":Ljava/lang/String;
    const/4 v5, 0x0

    .line 463
    .local v5, "value":Ljava/lang/String;
    const/16 v8, 0x28

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 464
    .local v4, "start":I
    const/16 v8, 0x29

    invoke-virtual {p1, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 465
    .local v1, "end":I
    add-int/lit8 v8, v4, 0x1

    invoke-virtual {p1, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 467
    .local v7, "vars":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v8, " "

    invoke-direct {v3, v7, v8}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    .local v3, "st":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 471
    :try_start_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 472
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 473
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x2

    if-le v8, v9, :cond_1

    .line 474
    const/4 v8, 0x1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 479
    :cond_1
    :goto_1
    if-eqz v6, :cond_0

    if-eqz v5, :cond_0

    .line 480
    invoke-virtual {v2, v6, v5}, Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;->addVariable(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 476
    :catch_0
    move-exception v0

    .line 477
    .local v0, "e":Ljava/util/NoSuchElementException;
    const-string v8, "IMAP4Parser"

    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 483
    .end local v0    # "e":Ljava/util/NoSuchElementException;
    :cond_2
    return-object v2
.end method

.method public parseStore(Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;Ljava/lang/String;)V
    .locals 9
    .param p1, "response"    # Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;
    .param p2, "firstLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 259
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 260
    .local v2, "nextHeaderLineLower":Ljava/lang/String;
    const/4 v1, -0x1

    .line 262
    .local v1, "index":I
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v6, 0x2a

    if-ne v3, v6, :cond_3

    const-string v3, "fetch"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_3

    .line 263
    const-wide/16 v4, -0x1

    .line 264
    .local v4, "uid":J
    const-string v3, "uid"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 265
    if-ltz v1, :cond_0

    .line 266
    const-string v3, "uid"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    .line 268
    const/16 v3, 0x29

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 271
    :cond_0
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 274
    const-string v3, "\\seen"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 275
    invoke-virtual {p1, v4, v5}, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->addRead(J)V

    .line 279
    :cond_1
    const-string v3, "\\deleted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 280
    invoke-virtual {p1, v4, v5}, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->addDeleted(J)V

    .line 284
    :cond_2
    const-string v3, "tuiskipped"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 285
    invoke-virtual {p1, v4, v5}, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->addSkipped(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    .end local v4    # "uid":J
    :cond_3
    return-void

    .line 293
    .end local v1    # "index":I
    .end local v2    # "nextHeaderLineLower":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "IMAP4Parser"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 295
    new-instance v3, Ljava/text/ParseException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6, v8}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v3
.end method

.method protected parseTranscription(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;
    .locals 4
    .param p1, "responseStr"    # Ljava/lang/String;

    .prologue
    .line 307
    new-instance v1, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;

    invoke-direct {v1}, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;-><init>()V

    .line 311
    .local v1, "response":Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;
    const-string v3, "Additional information on .lvp audio files can be found at http://www.alcatel-lucent.com/mvp"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 316
    const/16 v3, 0x7d

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 317
    .local v2, "start":I
    const/16 v3, 0x29

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 318
    .local v0, "end":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 319
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .line 321
    :cond_0
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->setMessageTranscription(Ljava/lang/String;)V

    .line 324
    .end local v0    # "end":I
    .end local v2    # "start":I
    :cond_1
    return-object v1
.end method

.class public Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;
.super Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
.source "StoreResponse.java"


# instance fields
.field private deletedUids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private readUids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private skippedUids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->deletedUids:Ljava/util/Set;

    .line 26
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->readUids:Ljava/util/Set;

    .line 27
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->skippedUids:Ljava/util/Set;

    .line 28
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "result"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->deletedUids:Ljava/util/Set;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->readUids:Ljava/util/Set;

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->skippedUids:Ljava/util/Set;

    .line 47
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "result"    # I
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->deletedUids:Ljava/util/Set;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->readUids:Ljava/util/Set;

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->skippedUids:Ljava/util/Set;

    .line 38
    return-void
.end method


# virtual methods
.method public addDeleted(J)V
    .locals 3
    .param p1, "uid"    # J

    .prologue
    .line 96
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->deletedUids:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    return-void
.end method

.method public addRead(J)V
    .locals 3
    .param p1, "uid"    # J

    .prologue
    .line 92
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->readUids:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 93
    return-void
.end method

.method public addSkipped(J)V
    .locals 3
    .param p1, "uid"    # J

    .prologue
    .line 100
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->skippedUids:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 101
    return-void
.end method

.method public getDeletedUids()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->deletedUids:Ljava/util/Set;

    return-object v0
.end method

.method public getReadUids()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->readUids:Ljava/util/Set;

    return-object v0
.end method

.method public getSkippedUids()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->skippedUids:Ljava/util/Set;

    return-object v0
.end method

.method public setDeletedUids(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "deletedUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->deletedUids:Ljava/util/Set;

    .line 62
    return-void
.end method

.method public setReadUids(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "readUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->readUids:Ljava/util/Set;

    .line 82
    return-void
.end method

.method public setSkippedUids(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "skippedUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;->skippedUids:Ljava/util/Set;

    .line 89
    return-void
.end method

.class public Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;
.super Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
.source "FetchBodiesStructureResponse.java"


# instance fields
.field protected bodyStructureList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/protocol/BodyStructure;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;->bodyStructureList:Ljava/util/ArrayList;

    .line 34
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "result"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;->bodyStructureList:Ljava/util/ArrayList;

    .line 30
    return-void
.end method


# virtual methods
.method public getBodyStructureList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/protocol/BodyStructure;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;->bodyStructureList:Ljava/util/ArrayList;

    return-object v0
.end method

.class public Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;
.super Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
.source "GetQuotaResponse.java"


# instance fields
.field protected exists:I

.field protected quota:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V

    .line 15
    return-void
.end method


# virtual methods
.method public getExists()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;->exists:I

    return v0
.end method

.method public getQuota()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;->quota:I

    return v0
.end method

.method public setExists(I)V
    .locals 0
    .param p1, "exists"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;->exists:I

    .line 23
    return-void
.end method

.method public setQuota(I)V
    .locals 0
    .param p1, "quota"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;->quota:I

    .line 31
    return-void
.end method

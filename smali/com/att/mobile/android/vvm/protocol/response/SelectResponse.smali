.class public Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;
.super Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
.source "SelectResponse.java"


# instance fields
.field protected exists:I

.field protected resents:I

.field protected uidNext:I

.field protected uidValidity:J

.field protected unseen:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V

    .line 18
    return-void
.end method


# virtual methods
.method public getExists()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->exists:I

    return v0
.end method

.method public getResents()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->resents:I

    return v0
.end method

.method public getUidNext()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->uidNext:I

    return v0
.end method

.method public getUidValidity()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->uidValidity:J

    return-wide v0
.end method

.method public getUnseen()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->unseen:I

    return v0
.end method

.method public setExists(I)V
    .locals 0
    .param p1, "exists"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->exists:I

    .line 26
    return-void
.end method

.method public setResents(I)V
    .locals 0
    .param p1, "resents"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->resents:I

    .line 34
    return-void
.end method

.method public setUidNext(I)V
    .locals 0
    .param p1, "uidNext"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->uidNext:I

    .line 58
    return-void
.end method

.method public setUidValidity(J)V
    .locals 1
    .param p1, "uidValidity"    # J

    .prologue
    .line 49
    iput-wide p1, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->uidValidity:J

    .line 50
    return-void
.end method

.method public setUnseen(I)V
    .locals 0
    .param p1, "unseen"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->unseen:I

    .line 42
    return-void
.end method

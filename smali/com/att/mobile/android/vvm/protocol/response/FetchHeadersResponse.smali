.class public Lcom/att/mobile/android/vvm/protocol/response/FetchHeadersResponse;
.super Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
.source "FetchHeadersResponse.java"


# instance fields
.field protected messagesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/model/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V

    .line 21
    return-void
.end method

.method public constructor <init>(ILcom/att/mobile/android/vvm/model/Message;)V
    .locals 0
    .param p1, "result"    # I
    .param p2, "message"    # Lcom/att/mobile/android/vvm/model/Message;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V

    .line 29
    return-void
.end method

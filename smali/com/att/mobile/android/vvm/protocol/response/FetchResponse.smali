.class public Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;
.super Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
.source "FetchResponse.java"


# instance fields
.field protected bodyStructure:Lcom/att/mobile/android/vvm/protocol/BodyStructure;

.field protected isDeliveryStatusMessage:Z

.field protected messageTranscription:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->isDeliveryStatusMessage:Z

    .line 24
    return-void
.end method

.method public constructor <init>(ILjava/lang/StringBuffer;)V
    .locals 1
    .param p1, "result"    # I
    .param p2, "attachmentBuffer"    # Ljava/lang/StringBuffer;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->isDeliveryStatusMessage:Z

    .line 29
    if-eqz p2, :cond_0

    .line 30
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->messageTranscription:Ljava/lang/String;

    .line 31
    :cond_0
    return-void
.end method


# virtual methods
.method public getBodyStructure()Lcom/att/mobile/android/vvm/protocol/BodyStructure;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->bodyStructure:Lcom/att/mobile/android/vvm/protocol/BodyStructure;

    return-object v0
.end method

.method public getIsDeliveryStatusMessage()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->isDeliveryStatusMessage:Z

    return v0
.end method

.method public getMessageTranscription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->messageTranscription:Ljava/lang/String;

    return-object v0
.end method

.method public setBodyStructure(Lcom/att/mobile/android/vvm/protocol/BodyStructure;)V
    .locals 0
    .param p1, "bodyStructure"    # Lcom/att/mobile/android/vvm/protocol/BodyStructure;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->bodyStructure:Lcom/att/mobile/android/vvm/protocol/BodyStructure;

    .line 47
    return-void
.end method

.method public setIsDeliveryStatusMessage(Z)V
    .locals 0
    .param p1, "isDeliveryStatusMessage"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->isDeliveryStatusMessage:Z

    .line 55
    return-void
.end method

.method public setMessageTranscription(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageTranscription"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->messageTranscription:Ljava/lang/String;

    .line 39
    return-void
.end method

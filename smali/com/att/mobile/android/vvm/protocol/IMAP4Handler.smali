.class public Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;
.super Ljava/lang/Object;
.source "IMAP4Handler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/protocol/IMAP4Handler$IMAP4HandlerHolder;
    }
.end annotation


# static fields
.field private static final CHAR_OPEN_BRACKET:C = '('

.field private static final PARSE_STATE_CHUNCKING_AMR:I = 0x2

.field private static final PARSE_STATE_END:I = 0x4

.field private static final PARSE_STATE_FOUND_AMR:I = 0x1

.field private static final PARSE_STATE_NONE:I = 0x0

.field private static final PARSE_STATE_PARSING_TEXT_ITEM:I = 0x3

.field private static final SPACE:B = 0x20t

.field private static final STR_BAD:Ljava/lang/String; = " bad"

.field private static final STR_BYE:Ljava/lang/String; = "* bye"

.field private static final STR_EXISTS:Ljava/lang/String; = " exists"

.field private static final STR_MESSAGE:Ljava/lang/String; = " message"

.field private static final STR_METADATA:Ljava/lang/String; = "metadata"

.field private static final STR_NO:Ljava/lang/String; = " no"

.field private static final STR_OK:Ljava/lang/String; = " ok"

.field private static final TAG:Ljava/lang/String; = "IMAP4Handler"


# instance fields
.field private chunkingTimeStamp:J

.field private context:Landroid/content/Context;

.field private init:Z

.field private modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

.field private networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

.field private parser:Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->init:Z

    .line 51
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    .line 78
    return-void
.end method

.method synthetic constructor <init>(Lcom/att/mobile/android/vvm/protocol/IMAP4Handler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/att/mobile/android/vvm/protocol/IMAP4Handler$1;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;-><init>()V

    return-void
.end method

.method private extractDataLength(Ljava/lang/String;)I
    .locals 3
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 466
    const-string v2, "{"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 467
    .local v1, "start":I
    const-string v2, "}"

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 468
    .local v0, "end":I
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public static getInstance()Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler$IMAP4HandlerHolder;->access$100()Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized receiveBodyText(Ljava/lang/String;Lcom/att/mobile/android/vvm/model/db/MessageDo;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 36
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 602
    monitor-enter p0

    const/4 v8, 0x0

    .line 603
    .local v8, "messageFilesBytesSize":I
    const/4 v12, 0x0

    .line 606
    .local v12, "bytesStillToRead":I
    const/4 v10, 0x0

    .line 608
    .local v10, "currentChunkNumber":I
    const/16 v27, 0x0

    .line 609
    .local v27, "status":I
    :try_start_0
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 610
    .local v29, "textAttachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v15, 0x0

    .line 611
    .local v15, "currentTextAttachment":Ljava/lang/StringBuffer;
    const/16 v22, 0x0

    .line 612
    .local v22, "isDeliveryStatus":Z
    const/16 v24, 0x0

    .line 613
    .local v24, "nextStr":Ljava/lang/String;
    const/4 v11, 0x0

    .line 617
    .local v11, "actualRead":[B
    const/4 v14, 0x0

    .line 618
    .local v14, "currentChunkBuffer":Ljava/io/ByteArrayOutputStream;
    const/16 v25, 0x0

    .line 619
    .local v25, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v6, 0x0

    .line 623
    .local v6, "numOfAmrFilesFound":I
    invoke-virtual/range {p2 .. p2}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getFileName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    const/16 v26, 0x1

    .line 626
    .local v26, "skipFileFetching":Z
    :goto_0
    new-instance v24, Ljava/lang/String;

    .end local v24    # "nextStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v5

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([B)V

    .line 627
    .restart local v24    # "nextStr":Ljava/lang/String;
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() received: "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    if-eqz v24, :cond_1

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "* bye"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 632
    new-instance v5, Ljava/io/IOException;

    const-string v7, "connection closed"

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 602
    .end local v6    # "numOfAmrFilesFound":I
    .end local v11    # "actualRead":[B
    .end local v14    # "currentChunkBuffer":Ljava/io/ByteArrayOutputStream;
    .end local v15    # "currentTextAttachment":Ljava/lang/StringBuffer;
    .end local v22    # "isDeliveryStatus":Z
    .end local v24    # "nextStr":Ljava/lang/String;
    .end local v25    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v26    # "skipFileFetching":Z
    .end local v29    # "textAttachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    .line 623
    .restart local v6    # "numOfAmrFilesFound":I
    .restart local v11    # "actualRead":[B
    .restart local v14    # "currentChunkBuffer":Ljava/io/ByteArrayOutputStream;
    .restart local v15    # "currentTextAttachment":Ljava/lang/StringBuffer;
    .restart local v22    # "isDeliveryStatus":Z
    .restart local v24    # "nextStr":Ljava/lang/String;
    .restart local v25    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v29    # "textAttachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    const/16 v26, 0x0

    goto :goto_0

    .line 635
    .restart local v26    # "skipFileFetching":Z
    :cond_1
    if-eqz v24, :cond_2

    :try_start_1
    const-string v5, "ATT_VVM__ "

    .line 636
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v7, -0x1

    if-le v5, v7, :cond_2

    .line 637
    const/16 v27, 0x4

    .line 640
    :cond_2
    :goto_1
    if-eqz v24, :cond_25

    .line 641
    packed-switch v27, :pswitch_data_0

    .line 904
    :cond_3
    :goto_2
    const/4 v5, 0x2

    move/from16 v0, v27

    if-eq v0, v5, :cond_2

    .line 905
    new-instance v24, Ljava/lang/String;

    .end local v24    # "nextStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v5

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([B)V

    .line 907
    .restart local v24    # "nextStr":Ljava/lang/String;
    if-eqz v26, :cond_4

    const/4 v5, 0x3

    move/from16 v0, v27

    if-ne v0, v5, :cond_5

    .line 909
    :cond_4
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() received: "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    :cond_5
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "* bye"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 915
    new-instance v5, Ljava/io/IOException;

    const-string v7, "connection closed"

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 644
    :pswitch_0
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v23

    .line 646
    .local v23, "lowerCaseNextStr":Ljava/lang/String;
    const-string v5, "content-type: text/plain"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 647
    const/16 v27, 0x3

    goto :goto_2

    .line 652
    :cond_6
    if-nez v26, :cond_7

    const-string v5, "content-type: audio/amr"

    .line 653
    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 654
    const/16 v27, 0x1

    .line 658
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 663
    :cond_7
    const-string v5, "content-type: multipart/report"

    .line 664
    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v5, "report-type=\"delivery-status\""

    .line 666
    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    :cond_8
    const-string v5, "content-type: message/delivery-status"

    .line 668
    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 669
    :cond_9
    const/16 v22, 0x1

    .line 671
    const-string v5, "IMAP4Handler"

    const-string v7, "IMAP4Handler.receiveBodyText() message type is delivery-status"

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 680
    .end local v23    # "lowerCaseNextStr":Ljava/lang/String;
    :pswitch_1
    const-string v5, ""

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 681
    const/16 v27, 0x2

    goto/16 :goto_2

    .line 684
    :cond_a
    if-nez v12, :cond_3

    .line 686
    const-string v5, "Content-Length:"

    .line 687
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    .line 688
    .local v13, "contentLenghtPosition":I
    if-ltz v13, :cond_3

    .line 691
    const-string v5, "Content-Length:"

    .line 693
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v13

    add-int/lit8 v5, v5, 0x1

    .line 691
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 694
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 691
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 695
    add-int/2addr v8, v12

    .line 699
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 707
    .end local v13    # "contentLenghtPosition":I
    :pswitch_2
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() PARSE_STATE_CHUNCKING_AMR bytesStillToRead = "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    :cond_b
    if-lez v12, :cond_11

    .line 713
    if-eqz v11, :cond_e

    array-length v5, v11

    if-lez v5, :cond_e

    .line 715
    array-length v5, v11

    sub-int/2addr v12, v5

    .line 717
    if-nez v14, :cond_c

    .line 718
    new-instance v14, Ljava/io/ByteArrayOutputStream;

    .end local v14    # "currentChunkBuffer":Ljava/io/ByteArrayOutputStream;
    invoke-direct {v14}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 720
    .restart local v14    # "currentChunkBuffer":Ljava/io/ByteArrayOutputStream;
    :cond_c
    invoke-virtual {v14, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 721
    invoke-virtual {v14}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    .line 725
    .local v9, "currentChunk":[B
    if-eqz v12, :cond_d

    array-length v5, v9

    add-int/lit8 v5, v5, -0x1

    aget-byte v5, v9, v5

    const/16 v7, 0xa

    if-ne v5, v7, :cond_10

    .line 728
    :cond_d
    add-int/lit8 v10, v10, 0x1

    .line 732
    new-instance v4, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;

    move-object/from16 v5, p2

    move-object/from16 v7, p1

    invoke-direct/range {v4 .. v10}, Lcom/att/mobile/android/vvm/control/files/FileWriterTask;-><init>(Lcom/att/mobile/android/vvm/model/db/MessageDo;ILjava/lang/String;I[BI)V

    .line 736
    .local v4, "task":Lcom/att/mobile/android/vvm/control/files/FileWriterTask;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    move-result-object v5

    .line 737
    invoke-virtual {v5, v4}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->enqueueFileWriterTask(Lcom/att/mobile/android/vvm/control/files/FileWriterTask;)V

    .line 740
    const/4 v14, 0x0

    .line 741
    const/4 v11, 0x0

    .line 742
    const/4 v9, 0x0

    .line 750
    .end local v4    # "task":Lcom/att/mobile/android/vvm/control/files/FileWriterTask;
    .end local v9    # "currentChunk":[B
    :cond_e
    :goto_3
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() check if more bytes to read = "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    if-lez v12, :cond_b

    .line 757
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 758
    .local v16, "currentTime":J
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() get chunk took "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    move-wide/from16 v32, v0

    sub-long v32, v16, v32

    move-wide/from16 v0, v32

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v31, " millis"

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    move-wide/from16 v32, v0

    sub-long v32, v16, v32

    const-wide/16 v34, 0x4e20

    cmp-long v5, v32, v34

    if-ltz v5, :cond_f

    .line 765
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    .line 766
    const-string v5, "IMAP4Handler"

    const-string v7, "IMAP4Handler.receiveBodyText() send: NOOP_TAG NOOP"

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    const-string v7, "NOOP_TAG NOOP\r\n"

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->send([B)V

    .line 772
    :cond_f
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() going to get next chunk bytesStillToRead = "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    .line 778
    invoke-virtual {v5, v12}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextChunk(I)[B

    move-result-object v11

    .line 780
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() actual read = "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v0, v11

    move/from16 v31, v0

    move/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v11}, Ljava/lang/String;-><init>([B)V

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "* bye"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 789
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/att/mobile/android/vvm/control/files/VvmFileUtils;->deleteInternalFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 790
    new-instance v5, Ljava/io/IOException;

    const-string v7, "connection closed"

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 744
    .end local v16    # "currentTime":J
    .restart local v9    # "currentChunk":[B
    :cond_10
    const-string v5, "IMAP4Handler"

    const-string v7, "IMAP4Handler.receiveBodyText() chunk not ending with LF or not last chunk! wait to append next chunk..."

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 795
    .end local v9    # "currentChunk":[B
    :cond_11
    const/16 v27, 0x0

    .line 796
    goto/16 :goto_2

    .line 800
    :cond_12
    :pswitch_3
    const-string v5, ""

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_13

    .line 801
    new-instance v24, Ljava/lang/String;

    .end local v24    # "nextStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v5

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([B)V

    .line 802
    .restart local v24    # "nextStr":Ljava/lang/String;
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() received: "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "* bye"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 806
    new-instance v5, Ljava/io/IOException;

    const-string v7, "connection closed"

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 809
    :cond_13
    new-instance v15, Ljava/lang/StringBuffer;

    .end local v15    # "currentTextAttachment":Ljava/lang/StringBuffer;
    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    .line 810
    .restart local v15    # "currentTextAttachment":Ljava/lang/StringBuffer;
    :cond_14
    const-string v5, "--"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 811
    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 812
    new-instance v24, Ljava/lang/String;

    .end local v24    # "nextStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v5

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([B)V

    .line 813
    .restart local v24    # "nextStr":Ljava/lang/String;
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() received: "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "* bye"

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 817
    new-instance v5, Ljava/io/IOException;

    const-string v7, "connection closed"

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 824
    :cond_15
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "Additional information on .lvp audio files can be found at http://www.alcatel-lucent.com/mvp"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_16

    .line 826
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 828
    :cond_16
    const/16 v27, 0x0

    .line 829
    goto/16 :goto_2

    .line 844
    :pswitch_4
    const/16 v19, -0x1

    .line 845
    .local v19, "errIndex":I
    const/16 v5, 0x28

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v18

    .line 846
    .local v18, "endOfMsgIndex":I
    const-string v5, " no"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    if-gez v19, :cond_17

    const-string v5, " no"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 847
    invoke-virtual {v5, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    if-ltz v19, :cond_1b

    .line 848
    :cond_17
    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1a

    .line 850
    :goto_4
    new-instance v25, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .end local v25    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v5, 0x1

    const-string v7, " no"

    .line 851
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int v7, v7, v19

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v25

    invoke-direct {v0, v5, v7}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .line 892
    .restart local v25    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_18
    :goto_5
    if-lez v8, :cond_19

    .line 895
    new-instance v20, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v8}, Lcom/att/mobile/android/vvm/control/files/FileCloserTask;-><init>(Lcom/att/mobile/android/vvm/model/db/MessageDo;Ljava/lang/String;I)V

    .line 897
    .local v20, "fileCloserTask":Lcom/att/mobile/android/vvm/control/files/FileCloserTask;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/att/mobile/android/vvm/control/files/FileWriterQueue;->enqueueFileCloserTask(Lcom/att/mobile/android/vvm/control/files/FileCloserTask;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v20    # "fileCloserTask":Lcom/att/mobile/android/vvm/control/files/FileCloserTask;
    :cond_19
    move-object/from16 v5, v25

    .line 929
    .end local v18    # "endOfMsgIndex":I
    .end local v19    # "errIndex":I
    :goto_6
    monitor-exit p0

    return-object v5

    .line 849
    .restart local v18    # "endOfMsgIndex":I
    .restart local v19    # "errIndex":I
    :cond_1a
    :try_start_2
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v18

    goto :goto_4

    .line 854
    :cond_1b
    const-string v5, " bad"

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    if-gez v19, :cond_1c

    const-string v5, " bad"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 855
    invoke-virtual {v5, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v19

    if-ltz v19, :cond_1e

    .line 856
    :cond_1c
    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_1d

    .line 858
    :goto_7
    new-instance v25, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .end local v25    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v5, 0x1

    const-string v7, " bad"

    .line 859
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int v7, v7, v19

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v25

    invoke-direct {v0, v5, v7}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .restart local v25    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_5

    .line 857
    :cond_1d
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v18

    goto :goto_7

    .line 862
    :cond_1e
    new-instance v25, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;

    .end local v25    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    invoke-direct/range {v25 .. v25}, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;-><init>()V

    .line 863
    .restart local v25    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v5, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->setResult(I)V

    .line 864
    const/16 v30, 0x0

    .line 866
    .local v30, "transcription":Ljava/lang/String;
    if-eqz v22, :cond_22

    .line 867
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 868
    .local v21, "fullTrascription":Ljava/lang/StringBuilder;
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_20

    .line 869
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    .line 870
    .local v28, "str":Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v31, "\n"

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 872
    .end local v28    # "str":Ljava/lang/String;
    :cond_1f
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 874
    :cond_20
    move-object/from16 v0, v25

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;

    move-object v5, v0

    const/4 v7, 0x1

    .line 875
    invoke-virtual {v5, v7}, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->setIsDeliveryStatusMessage(Z)V

    .line 882
    .end local v21    # "fullTrascription":Ljava/lang/StringBuilder;
    :cond_21
    :goto_9
    if-eqz v30, :cond_18

    .line 883
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "receiveBodyText() fetch Nuance transcription completed successfully, uid = "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 884
    invoke-virtual/range {p2 .. p2}, Lcom/att/mobile/android/vvm/model/db/MessageDo;->getUid()J

    move-result-wide v32

    move-wide/from16 v0, v32

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v31, " Nuance transcription = "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 883
    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    move-object/from16 v0, v25

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;

    move-object v5, v0

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;->setMessageTranscription(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 877
    :cond_22
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_21

    .line 878
    const/4 v5, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    .end local v30    # "transcription":Ljava/lang/String;
    check-cast v30, Ljava/lang/String;

    .restart local v30    # "transcription":Ljava/lang/String;
    goto :goto_9

    .line 919
    .end local v18    # "endOfMsgIndex":I
    .end local v19    # "errIndex":I
    .end local v30    # "transcription":Ljava/lang/String;
    :cond_23
    const-string v5, "ATT_VVM__ "

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const/4 v7, -0x1

    if-le v5, v7, :cond_2

    .line 920
    if-eqz v26, :cond_24

    .line 921
    const-string v5, "IMAP4Handler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "IMAP4Handler.receiveBodyText() received: "

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    :cond_24
    const/16 v27, 0x4

    goto/16 :goto_1

    .line 929
    :cond_25
    new-instance v5, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/4 v7, 0x1

    const/16 v31, 0x0

    move-object/from16 v0, v31

    invoke-direct {v5, v7, v0}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_6

    .line 641
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private declared-synchronized receiveGreetingFile(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 18
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 474
    monitor-enter p0

    const/4 v12, 0x0

    .line 475
    .local v12, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v10, 0x0

    .line 476
    .local v10, "outputStream":Ljava/io/BufferedOutputStream;
    const/4 v2, 0x0

    .line 477
    .local v2, "actualRead":[B
    const/4 v3, 0x0

    .line 478
    .local v3, "dataLengthRemained":I
    :try_start_0
    const-string v7, ""

    .line 479
    .local v7, "nextStr":Ljava/lang/String;
    const/4 v13, 0x0

    .line 481
    .local v13, "status":I
    const-string v14, "IMAP4Handler"

    const-string v15, "receiveGreetingFile started"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-object v8, v7

    .end local v7    # "nextStr":Ljava/lang/String;
    .local v8, "nextStr":Ljava/lang/String;
    move-object v11, v10

    .line 483
    .end local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .local v11, "outputStream":Ljava/io/BufferedOutputStream;
    :goto_0
    if-eqz v8, :cond_c

    .line 484
    packed-switch v13, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object v7, v8

    .end local v8    # "nextStr":Ljava/lang/String;
    .restart local v7    # "nextStr":Ljava/lang/String;
    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    :goto_1
    move-object v8, v7

    .end local v7    # "nextStr":Ljava/lang/String;
    .restart local v8    # "nextStr":Ljava/lang/String;
    move-object v11, v10

    .line 579
    .end local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    goto :goto_0

    .line 488
    :pswitch_1
    :try_start_1
    new-instance v7, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v14}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v14

    invoke-direct {v7, v14}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 490
    .end local v8    # "nextStr":Ljava/lang/String;
    .restart local v7    # "nextStr":Ljava/lang/String;
    if-eqz v7, :cond_2

    :try_start_2
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v14}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "* bye"

    invoke-virtual {v14, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 491
    new-instance v14, Ljava/io/IOException;

    const-string v15, "connection closed"

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 578
    .end local v12    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :catchall_0
    move-exception v14

    :goto_2
    if-eqz v11, :cond_1

    .line 579
    :try_start_3
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V

    :cond_1
    throw v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 474
    .end local v7    # "nextStr":Ljava/lang/String;
    :catchall_1
    move-exception v14

    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .end local v13    # "status":I
    .restart local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    :goto_3
    monitor-exit p0

    throw v14

    .line 494
    .end local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v7    # "nextStr":Ljava/lang/String;
    .restart local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v12    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v13    # "status":I
    :cond_2
    if-eqz v7, :cond_5

    :try_start_4
    const-string v14, "ATT_VVM__ "

    .line 495
    invoke-virtual {v7, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    const/4 v15, -0x1

    if-le v14, v15, :cond_5

    .line 496
    const-string v14, " GETMETADATA failed"

    invoke-virtual {v7, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    if-lez v14, :cond_4

    .line 497
    new-instance v12, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .end local v12    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v14, 0x1

    invoke-direct {v12, v14}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 578
    if-eqz v11, :cond_3

    .line 579
    :try_start_5
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 583
    :cond_3
    :goto_4
    monitor-exit p0

    return-object v12

    .line 499
    .restart local v12    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_4
    const/4 v13, 0x4

    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    goto :goto_1

    .line 501
    .end local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    :cond_5
    :try_start_6
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v14}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    .line 503
    .local v6, "lowerCaseNextStr":Ljava/lang/String;
    const-string v14, "~{"

    invoke-virtual {v6, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 504
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->extractDataLength(Ljava/lang/String;)I

    move-result v3

    .line 509
    :goto_5
    const-string v14, "content-type: audio/amr"

    invoke-virtual {v6, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 510
    const/4 v13, 0x1

    :cond_6
    move-object v10, v11

    .line 513
    .end local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    goto :goto_1

    .line 506
    .end local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    :cond_7
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v14

    const-string v15, "\r\n"

    invoke-virtual {v15}, Ljava/lang/String;->length()I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v15

    add-int/2addr v14, v15

    sub-int/2addr v3, v14

    goto :goto_5

    .line 517
    .end local v6    # "lowerCaseNextStr":Ljava/lang/String;
    .end local v7    # "nextStr":Ljava/lang/String;
    .restart local v8    # "nextStr":Ljava/lang/String;
    :pswitch_2
    :try_start_7
    new-instance v7, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v14}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v14

    invoke-direct {v7, v14}, Ljava/lang/String;-><init>([B)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 519
    .end local v8    # "nextStr":Ljava/lang/String;
    .restart local v7    # "nextStr":Ljava/lang/String;
    if-eqz v7, :cond_8

    :try_start_8
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v14}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "* bye"

    invoke-virtual {v14, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 520
    new-instance v14, Ljava/io/IOException;

    const-string v15, "connection closed"

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 523
    :cond_8
    if-eqz v7, :cond_9

    const-string v14, "ATT_VVM__ "

    .line 524
    invoke-virtual {v7, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    const/4 v15, -0x1

    if-le v14, v15, :cond_9

    .line 525
    const/4 v13, 0x4

    .line 528
    :cond_9
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v14

    const-string v15, "\r\n"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    add-int/2addr v14, v15

    sub-int/2addr v3, v14

    .line 532
    const-string v14, ""

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_e

    .line 533
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v15}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v9

    .line 534
    .local v9, "openFileOutput":Ljava/io/FileOutputStream;
    new-instance v10, Ljava/io/BufferedOutputStream;

    invoke-direct {v10, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 536
    .end local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    const/4 v13, 0x2

    .line 537
    goto/16 :goto_1

    .line 543
    .end local v7    # "nextStr":Ljava/lang/String;
    .end local v9    # "openFileOutput":Ljava/io/FileOutputStream;
    .end local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v8    # "nextStr":Ljava/lang/String;
    .restart local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    :pswitch_3
    :try_start_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    const/16 v15, 0x64

    invoke-static {v15, v3}, Ljava/lang/Math;->min(II)I

    move-result v15

    invoke-virtual {v14, v15}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextChunk(I)[B

    move-result-object v2

    .line 544
    array-length v14, v2

    sub-int/2addr v3, v14

    .line 545
    const-string v14, "IMAP4Handler"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "receiveGreetingFile PARSE_STATE_CHUNCKING_AMR dataLengthRemained = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    invoke-virtual {v11, v2}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 549
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->flush()V

    .line 551
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 552
    .local v4, "currentTime":J
    const-string v14, "IMAP4Handler"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "receiveBodyText() get chunk took "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    move-wide/from16 v16, v0

    sub-long v16, v4, v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " millis"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    sub-long v14, v4, v14

    const-wide/16 v16, 0x4e20

    cmp-long v14, v14, v16

    if-ltz v14, :cond_a

    .line 557
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    .line 558
    const-string v14, "IMAP4Handler"

    const-string v15, "receiveBodyText() send: NOOP_TAG NOOP"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    const-string v15, "NOOP_TAG NOOP\r\n"

    invoke-virtual {v15}, Ljava/lang/String;->getBytes()[B

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->send([B)V

    .line 563
    :cond_a
    if-nez v3, :cond_0

    .line 565
    const-string v14, "IMAP4Handler"

    const-string v15, "receiveGreetingFile ended"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    const/4 v13, 0x0

    move-object v7, v8

    .end local v8    # "nextStr":Ljava/lang/String;
    .restart local v7    # "nextStr":Ljava/lang/String;
    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    goto/16 :goto_1

    .line 574
    .end local v4    # "currentTime":J
    .end local v7    # "nextStr":Ljava/lang/String;
    .end local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v8    # "nextStr":Ljava/lang/String;
    .restart local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    :pswitch_4
    new-instance v12, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .end local v12    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v14, 0x0

    invoke-direct {v12, v14}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(I)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 578
    if-eqz v11, :cond_b

    .line 579
    :try_start_a
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V

    :cond_b
    move-object v7, v8

    .end local v8    # "nextStr":Ljava/lang/String;
    .restart local v7    # "nextStr":Ljava/lang/String;
    goto/16 :goto_4

    .line 578
    .end local v7    # "nextStr":Ljava/lang/String;
    .restart local v8    # "nextStr":Ljava/lang/String;
    .restart local v12    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_c
    if-eqz v11, :cond_d

    .line 579
    invoke-virtual {v11}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_d
    move-object v7, v8

    .line 583
    .end local v8    # "nextStr":Ljava/lang/String;
    .restart local v7    # "nextStr":Ljava/lang/String;
    goto/16 :goto_4

    .line 578
    .end local v7    # "nextStr":Ljava/lang/String;
    .end local v12    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v8    # "nextStr":Ljava/lang/String;
    :catchall_2
    move-exception v14

    move-object v7, v8

    .end local v8    # "nextStr":Ljava/lang/String;
    .restart local v7    # "nextStr":Ljava/lang/String;
    goto/16 :goto_2

    .line 474
    .end local v7    # "nextStr":Ljava/lang/String;
    .end local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .end local v13    # "status":I
    .restart local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v12    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :catchall_3
    move-exception v14

    goto/16 :goto_3

    .end local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v7    # "nextStr":Ljava/lang/String;
    .restart local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v13    # "status":I
    :cond_e
    move-object v10, v11

    .end local v11    # "outputStream":Ljava/io/BufferedOutputStream;
    .restart local v10    # "outputStream":Ljava/io/BufferedOutputStream;
    goto/16 :goto_1

    .line 484
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private declared-synchronized receiveImapResponse(BLjava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 29
    .param p1, "transaction"    # B
    .param p2, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/net/ssl/SSLException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 225
    monitor-enter p0

    const/4 v11, 0x0

    .line 226
    .local v11, "messageExists":I
    const/4 v15, 0x0

    .line 227
    .local v15, "quota":I
    const/16 v18, 0x0

    .line 228
    .local v18, "textBuf":Ljava/lang/StringBuffer;
    const/4 v8, -0x1

    .line 229
    .local v8, "index":I
    const/4 v9, 0x0

    .line 231
    .local v9, "isEnd":Z
    const/4 v12, 0x0

    .line 232
    .local v12, "nextLine":Ljava/lang/String;
    const/4 v14, 0x0

    .line 233
    .local v14, "nextLineLower":Ljava/lang/String;
    const/16 v16, 0x0

    .line 234
    .local v16, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/16 v20, 0x0

    .line 237
    .local v20, "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :try_start_0
    new-instance v13, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v13, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    .end local v12    # "nextLine":Ljava/lang/String;
    .local v13, "nextLine":Ljava/lang/String;
    :try_start_1
    const-string v22, "IMAP4Handler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "IMAP4Handler.receiveImapResponse() received: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-static {v13}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v14

    move-object/from16 v21, v20

    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .local v21, "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .local v17, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v19, v18

    .line 242
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .local v19, "textBuf":Ljava/lang/StringBuffer;
    :goto_0
    if-nez v9, :cond_17

    .line 243
    :try_start_2
    const-string v22, "* bye"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 244
    const/16 v22, 0x1

    move/from16 v0, p1

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 245
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x0

    const-string v23, " ok"

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v28, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .local v28, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v17, v16

    move-object/from16 v16, v28

    .line 461
    .end local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :goto_1
    monitor-exit p0

    return-object v17

    .line 247
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_0
    :try_start_3
    const-string v22, "IMAP4Handler"

    const-string v23, "BYE recieved - Connection closed by server"

    invoke-static/range {v22 .. v23}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-direct {v0, v1, v14}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v28, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v17, v16

    move-object/from16 v16, v28

    .end local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_1

    .line 254
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_1
    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    .line 267
    if-eqz v9, :cond_5

    .line 268
    const/4 v7, -0x1

    .line 269
    .local v7, "errIndex":I
    const/16 v22, 0x28

    move/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 270
    .local v6, "endOfMsgIndex":I
    const-string v22, " no"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-ltz v7, :cond_3

    .line 271
    if-le v6, v7, :cond_2

    .line 273
    :goto_2
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x1

    const-string v23, " no"

    .line 274
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    add-int v23, v23, v7

    move/from16 v0, v23

    invoke-virtual {v14, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .line 276
    goto :goto_1

    .line 272
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_2
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v6

    goto :goto_2

    .line 277
    :cond_3
    const-string v22, " bad"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-ltz v7, :cond_5

    .line 278
    if-le v6, v7, :cond_4

    .line 280
    :goto_3
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x1

    const-string v23, " bad"

    .line 282
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    add-int v23, v23, v7

    .line 281
    move/from16 v0, v23

    invoke-virtual {v14, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .line 283
    goto/16 :goto_1

    .line 279
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_4
    invoke-virtual {v14}, Ljava/lang/String;->length()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v6

    goto :goto_3

    .line 287
    .end local v6    # "endOfMsgIndex":I
    .end local v7    # "errIndex":I
    :cond_5
    packed-switch p1, :pswitch_data_0

    :cond_6
    :pswitch_0
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v18, v19

    .line 451
    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    :goto_4
    if-nez v9, :cond_18

    .line 453
    :try_start_4
    new-instance v12, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->receiveNextData()[B

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 454
    .end local v13    # "nextLine":Ljava/lang/String;
    .restart local v12    # "nextLine":Ljava/lang/String;
    :try_start_5
    const-string v22, "IMAP4Handler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "IMAP4Handler.receiveImapResponse() received: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    invoke-static {v12}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->toLowerCase(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v14

    move-object/from16 v21, v20

    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object v13, v12

    .end local v12    # "nextLine":Ljava/lang/String;
    .restart local v13    # "nextLine":Ljava/lang/String;
    move-object/from16 v19, v18

    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    goto/16 :goto_0

    .line 290
    :pswitch_1
    :try_start_6
    const-string v22, " exists"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-ltz v8, :cond_7

    .line 291
    const/16 v22, 0x20

    add-int/lit8 v23, v8, -0x1

    .line 293
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    .line 292
    move/from16 v0, v22

    invoke-virtual {v13, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    .line 294
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    .line 292
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 296
    :cond_7
    if-eqz v9, :cond_6

    .line 297
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;

    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;-><init>(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 298
    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :try_start_7
    move-object/from16 v0, v16

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Lcom/att/mobile/android/vvm/protocol/response/SelectResponse;->setExists(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .line 299
    goto/16 :goto_1

    .line 305
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_2
    :try_start_8
    const-string v22, " message"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-ltz v8, :cond_8

    .line 306
    const/16 v22, 0x20

    add-int/lit8 v23, v8, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    const/16 v23, 0x20

    add-int/lit8 v24, v8, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v23

    add-int/lit8 v23, v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 307
    const/16 v22, 0x20

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    const/16 v23, 0x20

    move/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v23

    add-int/lit8 v23, v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 309
    :cond_8
    if-eqz v9, :cond_6

    .line 310
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;

    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;-><init>(I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 311
    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :try_start_9
    move-object/from16 v0, v16

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;->setExists(I)V

    .line 312
    move-object/from16 v0, v16

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Lcom/att/mobile/android/vvm/protocol/response/GetQuotaResponse;->setQuota(I)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .line 313
    goto/16 :goto_1

    .line 318
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_3
    if-nez v21, :cond_1b

    .line 319
    :try_start_a
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 321
    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :goto_5
    if-eqz v9, :cond_9

    .line 327
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->deleteMessagesNotOnServer(Ljava/util/ArrayList;)I

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    check-cast v22, Lcom/att/mobile/android/vvm/VVMApplication;

    check-cast v22, Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-virtual/range {v22 .. v22}, Lcom/att/mobile/android/vvm/VVMApplication;->updateNotificationAfterRefresh()V

    .line 333
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->setResult(I)V

    move-object/from16 v16, v17

    .line 334
    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_1

    .line 336
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_9
    if-nez v17, :cond_1a

    .line 337
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/FetchHeadersResponse;

    invoke-direct/range {v16 .. v16}, Lcom/att/mobile/android/vvm/protocol/response/FetchHeadersResponse;-><init>()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 339
    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :goto_6
    :try_start_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->parser:Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;

    move-object/from16 v23, v0

    move-object/from16 v0, v16

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/response/FetchHeadersResponse;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v13, v2}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->parseHeader(Lcom/att/mobile/android/vvm/protocol/response/FetchHeadersResponse;Ljava/lang/String;Landroid/content/Context;)Lcom/att/mobile/android/vvm/model/Message;

    move-result-object v10

    .line 342
    .local v10, "message":Lcom/att/mobile/android/vvm/model/Message;
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/Message;->getPreviousUid()J

    move-result-wide v22

    const-wide/16 v24, -0x1

    cmp-long v22, v22, v24

    if-eqz v22, :cond_a

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v22, v0

    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/Message;->getPreviousUid()J

    move-result-wide v24

    .line 344
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/Message;->getUid()J

    move-result-wide v26

    .line 343
    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    move-wide/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateUid(JJ)Z

    .line 353
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v22, v0

    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/Message;->getUid()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessagePendingForDelete(J)Z

    move-result v22

    if-nez v22, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v22, v0

    .line 355
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/Message;->getUid()J

    move-result-wide v24

    .line 354
    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessagePendingForMarkAsRead(J)Z

    move-result v22

    if-nez v22, :cond_b

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->updateOrInsertMessageToInbox(Lcom/att/mobile/android/vvm/model/Message;)Z

    .line 359
    :cond_b
    invoke-virtual {v10}, Lcom/att/mobile/android/vvm/model/Message;->getUid()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    move-object/from16 v18, v19

    .line 360
    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto/16 :goto_4

    .line 365
    .end local v10    # "message":Lcom/att/mobile/android/vvm/model/Message;
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_4
    if-nez v17, :cond_19

    .line 366
    :try_start_d
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;

    invoke-direct/range {v16 .. v16}, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;-><init>()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 369
    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :goto_7
    if-eqz v9, :cond_c

    .line 370
    const/16 v22, 0x0

    :try_start_e
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->setResult(I)V

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .line 371
    goto/16 :goto_1

    .line 373
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->parser:Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;

    move-object/from16 v23, v0

    move-object/from16 v0, v16

    check-cast v0, Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;

    move-object/from16 v22, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v13}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->parseStore(Lcom/att/mobile/android/vvm/protocol/response/StoreResponse;Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v18, v19

    .line 374
    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto/16 :goto_4

    .line 384
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_5
    if-eqz v9, :cond_6

    .line 385
    :try_start_f
    const-string v22, " ok"

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    if-ltz v22, :cond_d

    .line 386
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x0

    const-string v23, " ok"

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v28, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v17, v16

    move-object/from16 v16, v28

    .end local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_1

    .line 389
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_d
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x1

    const-string v23, " bad"

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v28, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v17, v16

    move-object/from16 v16, v28

    .end local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_1

    .line 396
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_6
    if-eqz v9, :cond_f

    .line 397
    if-eqz v19, :cond_e

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->parser:Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;

    move-object/from16 v22, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->parseMetaData(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/MetaDataResponse;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    move-result-object v16

    .line 399
    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/16 v22, 0x0

    :try_start_10
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->setResult(I)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :goto_8
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .line 403
    goto/16 :goto_1

    .line 401
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_e
    :try_start_11
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x1

    const-string v23, " bad"

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_8

    .line 405
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_f
    if-nez v19, :cond_10

    const-string v22, "metadata"

    .line 406
    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v22

    if-lez v22, :cond_10

    .line 407
    new-instance v18, Ljava/lang/StringBuffer;

    move-object/from16 v0, v18

    invoke-direct {v0, v13}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_4

    .line 408
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_10
    if-eqz v19, :cond_6

    .line 409
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v18, v19

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto/16 :goto_4

    .line 414
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_7
    if-eqz v9, :cond_12

    .line 415
    if-eqz v19, :cond_11

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->parser:Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;

    move-object/from16 v22, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->parseTranscription(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/FetchResponse;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    move-result-object v16

    .line 417
    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/16 v22, 0x0

    :try_start_12
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->setResult(I)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :goto_9
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .line 421
    goto/16 :goto_1

    .line 419
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_11
    :try_start_13
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x1

    const-string v23, " bad"

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_9

    .line 423
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_12
    if-nez v19, :cond_13

    .line 424
    new-instance v18, Ljava/lang/StringBuffer;

    move-object/from16 v0, v18

    invoke-direct {v0, v13}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_4

    .line 425
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_13
    const-string v22, "--"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_6

    .line 426
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v18, v19

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto/16 :goto_4

    .line 431
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_8
    if-eqz v9, :cond_15

    .line 432
    if-eqz v19, :cond_14

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->parser:Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;

    move-object/from16 v22, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;->parseBodiesStructure(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/FetchBodiesStructureResponse;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    move-result-object v16

    .line 434
    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/16 v22, 0x0

    :try_start_14
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;->setResult(I)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    :goto_a
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .line 438
    goto/16 :goto_1

    .line 436
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_14
    :try_start_15
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x1

    const-string v23, " bad"

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_a

    .line 440
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :cond_15
    if-nez v19, :cond_16

    .line 441
    new-instance v18, Ljava/lang/StringBuffer;

    move-object/from16 v0, v18

    invoke-direct {v0, v13}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_4

    .line 443
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_16
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v18, v19

    .line 445
    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto/16 :goto_4

    .line 461
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_17
    new-instance v16, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    const/16 v22, 0x1

    const-string v23, " bad"

    move-object/from16 v0, v16

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v28, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v17, v16

    move-object/from16 v16, v28

    .end local v28    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_1

    .line 225
    .end local v13    # "nextLine":Ljava/lang/String;
    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v12    # "nextLine":Ljava/lang/String;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v22

    :goto_b
    monitor-exit p0

    throw v22

    .end local v12    # "nextLine":Ljava/lang/String;
    .restart local v13    # "nextLine":Ljava/lang/String;
    :catchall_1
    move-exception v22

    move-object v12, v13

    .end local v13    # "nextLine":Ljava/lang/String;
    .restart local v12    # "nextLine":Ljava/lang/String;
    goto :goto_b

    .end local v12    # "nextLine":Ljava/lang/String;
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v13    # "nextLine":Ljava/lang/String;
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_2
    move-exception v22

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object v12, v13

    .end local v13    # "nextLine":Ljava/lang/String;
    .restart local v12    # "nextLine":Ljava/lang/String;
    move-object/from16 v18, v19

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto :goto_b

    .end local v12    # "nextLine":Ljava/lang/String;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v13    # "nextLine":Ljava/lang/String;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_3
    move-exception v22

    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object v12, v13

    .end local v13    # "nextLine":Ljava/lang/String;
    .restart local v12    # "nextLine":Ljava/lang/String;
    move-object/from16 v18, v19

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto :goto_b

    .end local v12    # "nextLine":Ljava/lang/String;
    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v13    # "nextLine":Ljava/lang/String;
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    :catchall_4
    move-exception v22

    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object v12, v13

    .end local v13    # "nextLine":Ljava/lang/String;
    .restart local v12    # "nextLine":Ljava/lang/String;
    move-object/from16 v18, v19

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto :goto_b

    .end local v12    # "nextLine":Ljava/lang/String;
    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v13    # "nextLine":Ljava/lang/String;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    :catchall_5
    move-exception v22

    move-object v12, v13

    .end local v13    # "nextLine":Ljava/lang/String;
    .restart local v12    # "nextLine":Ljava/lang/String;
    move-object/from16 v18, v19

    .end local v19    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v18    # "textBuf":Ljava/lang/StringBuffer;
    goto :goto_b

    .end local v12    # "nextLine":Ljava/lang/String;
    .restart local v13    # "nextLine":Ljava/lang/String;
    :cond_18
    move-object/from16 v21, v20

    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v17, v16

    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    move-object/from16 v19, v18

    .end local v18    # "textBuf":Ljava/lang/StringBuffer;
    .restart local v19    # "textBuf":Ljava/lang/StringBuffer;
    goto/16 :goto_0

    :cond_19
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_7

    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_1a
    move-object/from16 v16, v17

    .end local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto/16 :goto_6

    .end local v16    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .end local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v17    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .restart local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_1b
    move-object/from16 v20, v21

    .end local v21    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v20    # "uids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    goto/16 :goto_5

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 988
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 989
    monitor-exit p0

    return-void

    .line 988
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized connect()Z
    .locals 12

    .prologue
    .line 936
    monitor-enter p0

    const/4 v2, 0x0

    .line 937
    .local v2, "res":Z
    :try_start_0
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v9, "hostPref"

    const-class v10, Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 940
    .local v0, "host":Ljava/lang/String;
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    const v9, 0x7f0801f5

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 941
    .local v7, "useSslStr":Ljava/lang/String;
    if-eqz v7, :cond_2

    const-string v8, "true"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v5, 0x1

    .line 944
    .local v5, "useSSL":Z
    :goto_0
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->isDebugMode()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 945
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v9, "debugSslOnPref"

    const-class v10, Ljava/lang/Boolean;

    .line 946
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 945
    invoke-virtual {v8, v9, v10, v11}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 947
    .local v6, "useSslDebug":Z
    move v5, v6

    .line 950
    .end local v6    # "useSslDebug":Z
    :cond_0
    if-eqz v0, :cond_6

    .line 951
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    const v9, 0x7f0801cc

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 952
    .local v4, "timeout":I
    if-eqz v5, :cond_3

    .line 953
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v9, "sslPortPref"

    const-class v10, Ljava/lang/String;

    const-string v11, "0"

    invoke-virtual {v8, v9, v10, v11}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 956
    .local v3, "sslPort":I
    if-nez v3, :cond_1

    .line 957
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    const v9, 0x7f0801d2

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 959
    :cond_1
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    iget-object v9, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    invoke-virtual {v8, v9, v0, v3, v4}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connectSSL(Landroid/content/Context;Ljava/lang/String;II)Z

    move-result v2

    .line 971
    .end local v3    # "sslPort":I
    :goto_1
    if-eqz v2, :cond_5

    .line 972
    const-string v8, "IMAP4Handler"

    const-string v9, "IMAP4Handler.connect() connected successfully"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 980
    .end local v4    # "timeout":I
    :goto_2
    monitor-exit p0

    return v2

    .line 941
    .end local v5    # "useSSL":Z
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 963
    .restart local v4    # "timeout":I
    .restart local v5    # "useSSL":Z
    :cond_3
    :try_start_1
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    const-string v9, "portPref"

    const-class v10, Ljava/lang/String;

    const-string v11, "0"

    invoke-virtual {v8, v9, v10, v11}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 966
    .local v1, "port":I
    if-nez v1, :cond_4

    .line 967
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    const v9, 0x7f080187

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 969
    :cond_4
    iget-object v8, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v8, v0, v1, v4}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->connect(Ljava/lang/String;II)Z

    move-result v2

    goto :goto_1

    .line 974
    .end local v1    # "port":I
    :cond_5
    const-string v8, "IMAP4Handler"

    const-string v9, "IMAP4Handler.connect() connection failed"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 936
    .end local v0    # "host":Ljava/lang/String;
    .end local v4    # "timeout":I
    .end local v5    # "useSSL":Z
    .end local v7    # "useSslStr":Ljava/lang/String;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 977
    .restart local v0    # "host":Ljava/lang/String;
    .restart local v5    # "useSSL":Z
    .restart local v7    # "useSslStr":Ljava/lang/String;
    :cond_6
    :try_start_2
    const-string v8, "IMAP4Handler"

    const-string v9, "Host is not set!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized executeGetBodyTextCommand(Ljava/lang/String;Ljava/lang/String;Lcom/att/mobile/android/vvm/model/db/MessageDo;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 5
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "messageDo"    # Lcom/att/mobile/android/vvm/model/db/MessageDo;

    .prologue
    .line 194
    monitor-enter p0

    const/4 v1, 0x0

    .line 197
    .local v1, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :try_start_0
    const-string v2, "IMAP4Handler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IMAP4Handler.getAttachment() send:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->send([B)V

    .line 201
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    .line 203
    invoke-direct {p0, p2, p3}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->receiveBodyText(Ljava/lang/String;Lcom/att/mobile/android/vvm/model/db/MessageDo;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 212
    :goto_0
    monitor-exit p0

    return-object v1

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 206
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyDisconnect()V

    .line 207
    const-string v2, "IMAP4Handler"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 208
    new-instance v1, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .end local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v2, 0x2

    .line 209
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_0

    .line 194
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized executeGetGreetingFileCommand(Ljava/lang/String;Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 5
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 171
    monitor-enter p0

    const/4 v1, 0x0

    .line 174
    .local v1, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :try_start_0
    const-string v2, "IMAP4Handler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IMAP4Handler.getAttachment() send:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->send([B)V

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->chunkingTimeStamp:J

    .line 179
    invoke-direct {p0, p2}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->receiveGreetingFile(Ljava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 188
    :goto_0
    monitor-exit p0

    return-object v1

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 182
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyDisconnect()V

    .line 183
    const-string v2, "IMAP4Handler"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 184
    new-instance v1, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .end local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v2, 0x2

    .line 185
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_0

    .line 171
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized executeImapCommand(B[B)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    .locals 5
    .param p1, "transaction"    # B
    .param p2, "command"    # [B

    .prologue
    .line 103
    monitor-enter p0

    const/4 v1, 0x0

    .line 106
    .local v1, "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :try_start_0
    const-string v2, "IMAP4Handler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IMAP4Handler.executeImapCommand() send:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v2, p2}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->send([B)V

    .line 112
    const-string v2, "ATT_VVM__ "

    invoke-direct {p0, p1, v2}, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->receiveImapResponse(BLjava/lang/String;)Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 124
    :goto_0
    monitor-exit p0

    return-object v1

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/text/ParseException;
    :try_start_1
    const-string v2, "IMAP4Handler"

    invoke-virtual {v0}, Ljava/text/ParseException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 115
    new-instance v1, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .end local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v2, 0x1

    .line 116
    invoke-virtual {v0}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V

    .line 123
    .restart local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_0

    .line 117
    .end local v0    # "e":Ljava/text/ParseException;
    :catch_1
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 119
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyDisconnect()V

    .line 120
    const-string v2, "IMAP4Handler"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 121
    new-instance v1, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;

    .end local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    const/4 v2, 0x2

    .line 122
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;-><init>(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    goto :goto_0

    .line 103
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "response":Lcom/att/mobile/android/vvm/protocol/response/IMAP4Response;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized executeImapCommandWithoutResponse(B[BI)V
    .locals 6
    .param p1, "transaction"    # B
    .param p2, "command"    # [B
    .param p3, "chunkSize"    # I

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    const-string v3, "IMAP4Handler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "executeImapCommandWithoutResponse() send: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, p2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v3, -0x1

    if-ne p3, v3, :cond_1

    .line 139
    iget-object v3, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v3, p2}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->send([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 144
    :cond_1
    const/4 v1, 0x0

    .line 145
    .local v1, "numberOfBytesSent":I
    :try_start_1
    array-length v2, p2

    .line 149
    .local v2, "numberOfBytesStillToSend":I
    :goto_1
    if-ge p3, v2, :cond_2

    .line 151
    iget-object v3, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v3, p2, v1, p3}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->send([BII)V

    .line 152
    add-int/2addr v1, p3

    .line 153
    sub-int/2addr v2, p3

    goto :goto_1

    .line 159
    :cond_2
    if-lez v2, :cond_0

    .line 160
    iget-object v3, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v3, p2, v1, v2}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->send([BII)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    .end local v1    # "numberOfBytesSent":I
    .end local v2    # "numberOfBytesStillToSend":I
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    iget-object v3, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->close()V

    .line 165
    invoke-static {}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->getInstance()Lcom/att/mobile/android/vvm/control/OperationsQueue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->notifyDisconnect()V

    .line 166
    const-string v3, "IMAP4Handler"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 135
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    monitor-enter p0

    .line 83
    :try_start_0
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->init:Z

    if-nez v0, :cond_0

    .line 84
    iput-object p1, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->context:Landroid/content/Context;

    .line 85
    new-instance v0, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;-><init>()V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    .line 87
    new-instance v0, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-direct {v0, p1, v1}, Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;-><init>(Landroid/content/Context;Lcom/att/mobile/android/vvm/control/network/NetworkHandler;)V

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->parser:Lcom/att/mobile/android/vvm/protocol/IMAP4Parser;

    .line 88
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v0

    iput-object v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->modelManager:Lcom/att/mobile/android/vvm/model/db/ModelManager;

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->init:Z

    .line 92
    :cond_0
    monitor-exit p0

    .line 93
    return-void

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized isConnected()Z
    .locals 1

    .prologue
    .line 984
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/att/mobile/android/vvm/protocol/IMAP4Handler;->networkHandler:Lcom/att/mobile/android/vvm/control/network/NetworkHandler;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/control/network/NetworkHandler;->isConnected()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.class public Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;
.super Landroid/widget/LinearLayout;
.source "CompoundTextWithBullet.java"


# instance fields
.field private rowFrame:Landroid/widget/RelativeLayout;

.field private text:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    :goto_0
    return-void

    .line 32
    :cond_0
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 33
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f04006f

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 35
    const v1, 0x7f0f016e

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->rowFrame:Landroid/widget/RelativeLayout;

    .line 36
    const v1, 0x7f0f0170

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->text:Landroid/widget/TextView;

    .line 37
    iget-object v1, p0, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->text:Landroid/widget/TextView;

    sget-object v2, Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;->OmnesATT_Regular:Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/FontUtils;->getTypeface(Lcom/att/mobile/android/infra/utils/FontUtils$FontNames;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->initializeAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_0
.end method

.method private initializeAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget-object v4, Lcom/att/mobile/android/vvm/R$styleable;->CompoundTextWithBullet:[I

    invoke-virtual {v3, p2, v4, v5, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 45
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 46
    .local v2, "textSrc":I
    if-eqz v2, :cond_0

    .line 47
    iget-object v3, p0, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->text:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 49
    :cond_0
    const/4 v3, 0x2

    const/high16 v4, -0x1000000

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 50
    .local v1, "textColor":I
    iget-object v3, p0, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->text:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 55
    return-void

    .line 53
    .end local v1    # "textColor":I
    .end local v2    # "textSrc":I
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v3
.end method


# virtual methods
.method public setText(I)V
    .locals 1
    .param p1, "strigId"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lcom/att/mobile/android/vvm/ui_components/CompoundTextWithBullet;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 59
    return-void
.end method

.class public final Lcom/att/mobile/android/vvm/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ATTtermsAndCondistionBody:I = 0x7f080026

.field public static final ATTtermsAndCondistionTitle:I = 0x7f080027

.field public static final ATTtitle:I = 0x7f080028

.field public static final AccountSetupErrorText:I = 0x7f080029

.field public static final AccountSetupMoSmsRetryHeader:I = 0x7f08002a

.field public static final AccountSetupMoSmsRetrySubTextLine1:I = 0x7f08002b

.field public static final AccountSetupMoSmsRetrySubTextLine2:I = 0x7f08002c

.field public static final AccountSetupMoSmsRetryTextSub:I = 0x7f08002d

.field public static final AccountSetupRetrySubTextLine1:I = 0x7f08002e

.field public static final AccountSetupRetrySubTextLine2:I = 0x7f08002f

.field public static final AccountSetupRetryTextSub:I = 0x7f080030

.field public static final AccountSetupText:I = 0x7f080031

.field public static final AccountSetup_1:I = 0x7f080032

.field public static final AccountSetup_2:I = 0x7f080033

.field public static final AccountSetup_3_BeginSetup:I = 0x7f080034

.field public static final AccountSetup_3_CallVoicemail:I = 0x7f080035

.field public static final AccountSetup_SubDesc_1:I = 0x7f080036

.field public static final AccountSetup_SubDesc_2:I = 0x7f080037

.field public static final AccountSetup_SubDesc_3:I = 0x7f080038

.field public static final AccountVerificationFailedSub1Text:I = 0x7f080039

.field public static final AccountVerificationFailedSub2Text1:I = 0x7f08003a

.field public static final AccountVerificationFailedSub2Text2:I = 0x7f08003b

.field public static final AccountVerificationFailedSub3Text:I = 0x7f08003c

.field public static final AccountVerificationFailedSub3Text1:I = 0x7f08003d

.field public static final AccountVerificationFailedSub3Text2:I = 0x7f08003e

.field public static final AccountVerificationFailedSub4Text1:I = 0x7f08003f

.field public static final AccountVerificationFailedSub4TextCallAgain:I = 0x7f080040

.field public static final AccountVerificationFailedSub4TextCallVoicemail:I = 0x7f080041

.field public static final AccountVerificationFailedSub4TextTryAgain:I = 0x7f080042

.field public static final AccountVerificationFailedText:I = 0x7f080043

.field public static final AccountVerificationFailedText1:I = 0x7f080044

.field public static final AccountVerificationFailedText1Sub:I = 0x7f080045

.field public static final AccountVerificationFailedText2:I = 0x7f080046

.field public static final AccountVerificationFailedTextSub:I = 0x7f080047

.field public static final AccountVerificationFailedTxt:I = 0x7f080048

.field public static final ActivateVisual_Voicemail:I = 0x7f080049

.field public static final BackText:I = 0x7f08004a

.field public static final BeginSetupText:I = 0x7f08004b

.field public static final CallAgainText:I = 0x7f08004c

.field public static final CallVoicemailText:I = 0x7f08004d

.field public static final CancelText:I = 0x7f08004e

.field public static final ConfigureAllDoneGreetingLink:I = 0x7f08004f

.field public static final ConfigureAllDoneGreetingLinkButton:I = 0x7f080050

.field public static final ConfigureAllDoneGreetingLinkExisting:I = 0x7f080051

.field public static final ConfigureAllDoneGreetingLinkExistingButton:I = 0x7f080052

.field public static final ConfigureAllDoneHeader:I = 0x7f080053

.field public static final ConfigureAllDonePart1:I = 0x7f080054

.field public static final ConfigureAllDonePart2:I = 0x7f080055

.field public static final Confirm_password:I = 0x7f080056

.field public static final EULA:I = 0x7f080057

.field public static final Enter_password:I = 0x7f080058

.field public static final ForgotPasswordText:I = 0x7f080059

.field public static final ForgotPswTextLink:I = 0x7f08005a

.field public static final GreetingOptionsTitleText:I = 0x7f08005b

.field public static final Greeting_Custom_Desc:I = 0x7f08005c

.field public static final Greeting_Default_Desc:I = 0x7f08005d

.field public static final Greeting_Name_Desc:I = 0x7f08005e

.field public static final NextMessageText:I = 0x7f08005f

.field public static final PreviousMessageText:I = 0x7f080060

.field public static final PrivacyPolicyLink:I = 0x7f080061

.field public static final QuitText:I = 0x7f080062

.field public static final RecordNewText:I = 0x7f080063

.field public static final RegisterText:I = 0x7f080064

.field public static final ServerPingIntervalSeconds:I = 0x7f080065

.field public static final SetGreetingText:I = 0x7f080066

.field public static final SetPasswordText:I = 0x7f080067

.field public static final SettingAccountText:I = 0x7f080068

.field public static final SkipText:I = 0x7f080069

.field public static final StartText:I = 0x7f08006a

.field public static final TermsOfService:I = 0x7f08006b

.field public static final TodayText:I = 0x7f08006c

.field public static final TryAgainText:I = 0x7f08006d

.field public static final UnknownAccountText:I = 0x7f08006e

.field public static final UseExistingText:I = 0x7f08006f

.field public static final WelcomeATTServiceText:I = 0x7f080070

.field public static final WelcomeATTServiceToCompleteText:I = 0x7f080071

.field public static final WelcomeATTServiceToCompleteTextExisting:I = 0x7f080072

.field public static final WelcomeHeaderText:I = 0x7f080073

.field public static final WelcomeText:I = 0x7f080074

.field public static final Welcome_with_per_1:I = 0x7f080075

.field public static final Welcome_with_per_4:I = 0x7f080076

.field public static final Welcome_with_per_SubDesc_2:I = 0x7f080077

.field public static final Welcome_with_per_SubDesc_3:I = 0x7f080078

.field public static final WhatIsButton:I = 0x7f080079

.field public static final WhatIsText:I = 0x7f08007a

.field public static final WhatIs_Desc1:I = 0x7f08007b

.field public static final WhatIs_Desc2:I = 0x7f08007c

.field public static final WhatIs_SubDesc_1:I = 0x7f08007d

.field public static final WhatIs_SubDesc_2:I = 0x7f08007e

.field public static final WhatIs_SubDesc_3:I = 0x7f08007f

.field public static final WhatIs_SubDesc_4:I = 0x7f080080

.field public static final WhatIs_SubDesc_5:I = 0x7f080081

.field public static final WhatIs_SubDesc_Bullet:I = 0x7f080082

.field public static final YesterdayText:I = 0x7f080083

.field public static final abc_action_bar_home_description:I = 0x7f080000

.field public static final abc_action_bar_home_description_format:I = 0x7f080001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f080002

.field public static final abc_action_bar_up_description:I = 0x7f080003

.field public static final abc_action_menu_overflow_description:I = 0x7f080004

.field public static final abc_action_mode_done:I = 0x7f080005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f080006

.field public static final abc_activitychooserview_choose_application:I = 0x7f080007

.field public static final abc_capital_off:I = 0x7f080008

.field public static final abc_capital_on:I = 0x7f080009

.field public static final abc_font_family_body_1_material:I = 0x7f080084

.field public static final abc_font_family_body_2_material:I = 0x7f080085

.field public static final abc_font_family_button_material:I = 0x7f080086

.field public static final abc_font_family_caption_material:I = 0x7f080087

.field public static final abc_font_family_display_1_material:I = 0x7f080088

.field public static final abc_font_family_display_2_material:I = 0x7f080089

.field public static final abc_font_family_display_3_material:I = 0x7f08008a

.field public static final abc_font_family_display_4_material:I = 0x7f08008b

.field public static final abc_font_family_headline_material:I = 0x7f08008c

.field public static final abc_font_family_menu_material:I = 0x7f08008d

.field public static final abc_font_family_subhead_material:I = 0x7f08008e

.field public static final abc_font_family_title_material:I = 0x7f08008f

.field public static final abc_search_hint:I = 0x7f08000a

.field public static final abc_searchview_description_clear:I = 0x7f08000b

.field public static final abc_searchview_description_query:I = 0x7f08000c

.field public static final abc_searchview_description_search:I = 0x7f08000d

.field public static final abc_searchview_description_submit:I = 0x7f08000e

.field public static final abc_searchview_description_voice:I = 0x7f08000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f080010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f080011

.field public static final abc_toolbar_collapse_description:I = 0x7f080012

.field public static final about_menu_summary:I = 0x7f080090

.field public static final about_menu_title:I = 0x7f080091

.field public static final accept:I = 0x7f080092

.field public static final accountSetupWaitTime:I = 0x7f080093

.field public static final account_setup_call_voicemail_content_description:I = 0x7f080094

.field public static final account_setup_content_description:I = 0x7f080095

.field public static final account_setup_text_for_marshmellow:I = 0x7f080096

.field public static final account_setup_text_for_marshmellow_call:I = 0x7f080097

.field public static final account_setup_try_again_content_description:I = 0x7f080098

.field public static final actionNotCompletedError:I = 0x7f080099

.field public static final activate_visual_voicemail:I = 0x7f08009a

.field public static final addToContactsText:I = 0x7f08009b

.field public static final addToContactsTitle:I = 0x7f08009c

.field public static final addToContactsTxt:I = 0x7f08009d

.field public static final add_home_screen_shortcut:I = 0x7f08009e

.field public static final add_to_contacts_menu_title:I = 0x7f08009f

.field public static final always:I = 0x7f0800a0

.field public static final and:I = 0x7f0800a1

.field public static final androidIssueToast:I = 0x7f0800a2

.field public static final app_name:I = 0x7f0800a3

.field public static final appbar_scrolling_view_behavior:I = 0x7f0800a4

.field public static final att:I = 0x7f0800a5

.field public static final att_visual_voicemail:I = 0x7f0800a6

.field public static final attentionText:I = 0x7f0800a7

.field public static final attm_launcher_call_attm_text:I = 0x7f0800a8

.field public static final attm_launcher_text_paragraph_1:I = 0x7f0800a9

.field public static final attm_launcher_text_paragraph_2:I = 0x7f0800aa

.field public static final attm_launcher_title:I = 0x7f0800ab

.field public static final audio_recorder_send_button_text:I = 0x7f0800ac

.field public static final autoPlay:I = 0x7f0800ad

.field public static final autoSaveMessagesToastText:I = 0x7f0800ae

.field public static final auto_play_menu_title:I = 0x7f0800af

.field public static final auto_save_menu_title:I = 0x7f0800b0

.field public static final bottom_sheet_behavior:I = 0x7f0800b1

.field public static final by_clicking_activate_vv_you_agree:I = 0x7f0800b2

.field public static final call:I = 0x7f0800b3

.field public static final callBack:I = 0x7f0800b4

.field public static final call_failed:I = 0x7f0800b5

.field public static final cancel:I = 0x7f0800b6

.field public static final cancelCAPS:I = 0x7f0800b7

.field public static final cantDownloadMessagesError:I = 0x7f0800b8

.field public static final cato_select_greeting:I = 0x7f0800b9

.field public static final change_password:I = 0x7f0800ba

.field public static final change_password_title_1:I = 0x7f0800bb

.field public static final change_password_title_2:I = 0x7f0800bc

.field public static final change_password_title_2a:I = 0x7f0800bd

.field public static final change_password_title_3:I = 0x7f0800be

.field public static final change_password_title_4:I = 0x7f0800bf

.field public static final character_counter_pattern:I = 0x7f0800c0

.field public static final checkForUpdatesIntervalInDays:I = 0x7f0800c1

.field public static final checked:I = 0x7f0800c2

.field public static final clearAll:I = 0x7f0800c3

.field public static final common_google_play_services_enable_button:I = 0x7f080013

.field public static final common_google_play_services_enable_text:I = 0x7f080014

.field public static final common_google_play_services_enable_title:I = 0x7f080015

.field public static final common_google_play_services_install_button:I = 0x7f080016

.field public static final common_google_play_services_install_text:I = 0x7f080017

.field public static final common_google_play_services_install_title:I = 0x7f080018

.field public static final common_google_play_services_notification_ticker:I = 0x7f080019

.field public static final common_google_play_services_unknown_issue:I = 0x7f08001a

.field public static final common_google_play_services_unsupported_text:I = 0x7f08001b

.field public static final common_google_play_services_update_button:I = 0x7f08001c

.field public static final common_google_play_services_update_text:I = 0x7f08001d

.field public static final common_google_play_services_update_title:I = 0x7f08001e

.field public static final common_google_play_services_updating_text:I = 0x7f08001f

.field public static final common_google_play_services_wear_update_text:I = 0x7f080020

.field public static final common_open_on_phone:I = 0x7f080021

.field public static final common_signin_button_text:I = 0x7f080022

.field public static final common_signin_button_text_long:I = 0x7f080023

.field public static final confirmPassword:I = 0x7f0800c4

.field public static final confirmPasswordTitle:I = 0x7f0800c5

.field public static final connectedbtFile:I = 0x7f0800c6

.field public static final contactDisabledTxt:I = 0x7f0800c7

.field public static final contactInfoTxt:I = 0x7f0800c8

.field public static final continue_change_password_button:I = 0x7f0800c9

.field public static final copy_message_text_menu_title:I = 0x7f0800ca

.field public static final couldNotLocateError:I = 0x7f0800cb

.field public static final customGreeting:I = 0x7f0800cc

.field public static final custom_greeting_change:I = 0x7f0800cd

.field public static final debug_screen_Fake_Login:I = 0x7f0800ce

.field public static final debug_screen_SSL_on:I = 0x7f0800cf

.field public static final debug_screen_choose_host:I = 0x7f0800d0

.field public static final debug_screen_host_prompt:I = 0x7f0800d1

.field public static final debug_screen_insert_host:I = 0x7f0800d2

.field public static final debug_screen_mailbox:I = 0x7f0800d3

.field public static final debug_screen_password:I = 0x7f0800d4

.field public static final debug_screen_save:I = 0x7f0800d5

.field public static final debug_screen_temp_password:I = 0x7f0800d6

.field public static final decline:I = 0x7f0800d7

.field public static final defaultCountryIso:I = 0x7f0800d8

.field public static final delete:I = 0x7f0800d9

.field public static final deleteAllButton:I = 0x7f0800da

.field public static final deleteAllDialogMessage:I = 0x7f0800db

.field public static final deleteAllDialogTitle:I = 0x7f0800dc

.field public static final deleteCAPS:I = 0x7f0800dd

.field public static final deleteDialogMessage:I = 0x7f0800de

.field public static final deleteDialogMessages:I = 0x7f0800df

.field public static final deleteDialogTitle:I = 0x7f0800e0

.field public static final deleteMessageDialogTitle:I = 0x7f0800e1

.field public static final deleteMessagesDialogTitle:I = 0x7f0800e2

.field public static final deleteSelected:I = 0x7f0800e3

.field public static final deleteUnsavedButton:I = 0x7f0800e4

.field public static final delete_all_menu_title:I = 0x7f0800e5

.field public static final delete_menu_title:I = 0x7f0800e6

.field public static final deliveryStatusSenderName:I = 0x7f0800e7

.field public static final demoWatermark:I = 0x7f0800e8

.field public static final digits_only_password:I = 0x7f0800e9

.field public static final disabledText:I = 0x7f0800ea

.field public static final do_not_show_again:I = 0x7f0800eb

.field public static final dontSave:I = 0x7f0800ec

.field public static final dont_show_this_again:I = 0x7f0800ed

.field public static final edit_message:I = 0x7f0800ee

.field public static final empty_inbox_message:I = 0x7f0800ef

.field public static final enable_permissions:I = 0x7f0800f0

.field public static final enable_permissions_dialog:I = 0x7f0800f1

.field public static final engScreen:I = 0x7f0800f2

.field public static final error:I = 0x7f0800f3

.field public static final errorcode:I = 0x7f0800f4

.field public static final existingpasswordHintText:I = 0x7f0800f5

.field public static final exit:I = 0x7f0800f6

.field public static final export_menu_title:I = 0x7f0800f7

.field public static final exported:I = 0x7f0800f8

.field public static final finishSetup:I = 0x7f0800f9

.field public static final finish_change_password_button:I = 0x7f0800fa

.field public static final gcm_defaultSenderId:I = 0x7f0800fb

.field public static final go_to_settings_permissions_redirect_text:I = 0x7f0800fc

.field public static final go_to_settings_permissions_text:I = 0x7f0800fd

.field public static final google_api_key:I = 0x7f0800fe

.field public static final google_app_id:I = 0x7f0800ff

.field public static final google_crash_reporting_api_key:I = 0x7f080100

.field public static final greeting:I = 0x7f080101

.field public static final greetingError1:I = 0x7f080102

.field public static final greetingError2:I = 0x7f080103

.field public static final greetingErrorDesc:I = 0x7f080104

.field public static final greeting_changed:I = 0x7f080105

.field public static final greeting_error:I = 0x7f080106

.field public static final greeting_menu_title:I = 0x7f080107

.field public static final greeting_not_changed:I = 0x7f080108

.field public static final hello:I = 0x7f080109

.field public static final inbox_context_menu_delete:I = 0x7f08010a

.field public static final inbox_context_menu_read_message_details:I = 0x7f08010b

.field public static final inbox_context_menu_save:I = 0x7f08010c

.field public static final inbox_context_menu_share:I = 0x7f08010d

.field public static final inbox_context_menu_transcript:I = 0x7f08010e

.field public static final inbox_context_menu_unread_message_details:I = 0x7f08010f

.field public static final inbox_tab_content_description:I = 0x7f080110

.field public static final inbox_tab_title:I = 0x7f080111

.field public static final inbox_title:I = 0x7f080112

.field public static final invalidPassword:I = 0x7f080113

.field public static final just_a_momunt:I = 0x7f080114

.field public static final key_one:I = 0x7f080115

.field public static final key_two:I = 0x7f080116

.field public static final loading:I = 0x7f080117

.field public static final login:I = 0x7f080118

.field public static final looks_like_something:I = 0x7f080119

.field public static final mailboxSetupRequiredNotificationContentText:I = 0x7f08011a

.field public static final mailboxSetupRequiredNotificationText:I = 0x7f08011b

.field public static final mailboxSetupRequiredNotificationTitle:I = 0x7f08011c

.field public static final maxPasswordLenght:I = 0x7f08011d

.field public static final maxRetriesConnect:I = 0x7f08011e

.field public static final maxRetriesFetchRequest:I = 0x7f08011f

.field public static final maxRetriesTransaction:I = 0x7f080120

.field public static final menu:I = 0x7f080121

.field public static final messageText:I = 0x7f080122

.field public static final message_deleted:I = 0x7f080123

.field public static final message_saved_dialog_body:I = 0x7f080124

.field public static final message_saved_dialog_header:I = 0x7f080125

.field public static final message_str:I = 0x7f080126

.field public static final messagesAlmostFull:I = 0x7f080127

.field public static final messagesFull:I = 0x7f080128

.field public static final messagesFullTitle:I = 0x7f080129

.field public static final messages_deleted:I = 0x7f08012a

.field public static final messages_saved_dialog_body:I = 0x7f08012b

.field public static final messages_saved_dialog_header:I = 0x7f08012c

.field public static final messages_str:I = 0x7f08012d

.field public static final minConfidence:I = 0x7f08012e

.field public static final minPasswordLenght:I = 0x7f08012f

.field public static final missingVoicemailNumber:I = 0x7f080130

.field public static final missing_msisdn_final_text:I = 0x7f080131

.field public static final missing_msisdn_line1:I = 0x7f080132

.field public static final missing_msisdn_line2:I = 0x7f080133

.field public static final missing_msisdn_line3:I = 0x7f080134

.field public static final missing_msisdn_sub_text:I = 0x7f080135

.field public static final missing_msisdn_welcome_title:I = 0x7f080136

.field public static final mosmsToast:I = 0x7f080137

.field public static final msgReplyAll:I = 0x7f080138

.field public static final nameGreeting:I = 0x7f080139

.field public static final name_greeting_change:I = 0x7f08013a

.field public static final network_is_not_available:I = 0x7f08013b

.field public static final never:I = 0x7f08013c

.field public static final newMessageNotification:I = 0x7f08013d

.field public static final newMessageNotificationLastCaller:I = 0x7f08013e

.field public static final newMessageNotificationTextEnd:I = 0x7f08013f

.field public static final newMessageNotificationTextStart:I = 0x7f080140

.field public static final newSingleMessageNotificationText:I = 0x7f080141

.field public static final new_str:I = 0x7f080142

.field public static final newpasswordHintText:I = 0x7f080143

.field public static final next:I = 0x7f080144

.field public static final noConnectionToast:I = 0x7f080145

.field public static final noDataConnectionInboxBadge:I = 0x7f080146

.field public static final noDataConnectionToast:I = 0x7f080147

.field public static final noMemoryDialogText:I = 0x7f080148

.field public static final noMemoryDialogTitle:I = 0x7f080149

.field public static final noMemoryError:I = 0x7f08014a

.field public static final noTranscriptionMessage:I = 0x7f08014b

.field public static final no_connectivity:I = 0x7f08014c

.field public static final no_sim_dialog_body_text:I = 0x7f08014d

.field public static final no_sim_dialog_positive_button_text:I = 0x7f08014e

.field public static final notificationTitle:I = 0x7f08014f

.field public static final notifications_menu_title:I = 0x7f080150

.field public static final ok:I = 0x7f080151

.field public static final ok_got_it:I = 0x7f080152

.field public static final ok_got_it_caps:I = 0x7f080153

.field public static final old_str:I = 0x7f080154

.field public static final only_when_silent:I = 0x7f080155

.field public static final password:I = 0x7f080156

.field public static final passwordChangedInTUI:I = 0x7f080157

.field public static final passwordHintText:I = 0x7f080158

.field public static final passwordMissmatchMissingPasswordScreenTitle:I = 0x7f080159

.field public static final passwordMissmatchNotificationContentText:I = 0x7f08015a

.field public static final passwordMissmatchNotificationContentTitle:I = 0x7f08015b

.field public static final passwordMissmatchNotificationTickerText:I = 0x7f08015c

.field public static final passwordMissmatchScreenText:I = 0x7f08015d

.field public static final passwordMissmatchScreenTitle:I = 0x7f08015e

.field public static final passwordMissmatchScreenWarning:I = 0x7f08015f

.field public static final password_changed:I = 0x7f080160

.field public static final password_expired:I = 0x7f080161

.field public static final password_length:I = 0x7f080162

.field public static final password_must_be:I = 0x7f080163

.field public static final password_not_changed:I = 0x7f080164

.field public static final password_not_changed_force:I = 0x7f080165

.field public static final password_not_saved:I = 0x7f080166

.field public static final passwords_not_match:I = 0x7f080167

.field public static final pause:I = 0x7f080168

.field public static final pendingDeletesFile:I = 0x7f080169

.field public static final pendingReadsFile:I = 0x7f08016a

.field public static final permissions:I = 0x7f08016b

.field public static final phoneTypeAssistant:I = 0x7f08016c

.field public static final phoneTypeCallback:I = 0x7f08016d

.field public static final phoneTypeCar:I = 0x7f08016e

.field public static final phoneTypeCompanyMain:I = 0x7f08016f

.field public static final phoneTypeCustom:I = 0x7f080170

.field public static final phoneTypeFaxHome:I = 0x7f080171

.field public static final phoneTypeFaxWork:I = 0x7f080172

.field public static final phoneTypeHome:I = 0x7f080173

.field public static final phoneTypeIsdn:I = 0x7f080174

.field public static final phoneTypeMain:I = 0x7f080175

.field public static final phoneTypeMms:I = 0x7f080176

.field public static final phoneTypeMobile:I = 0x7f080177

.field public static final phoneTypeOther:I = 0x7f080178

.field public static final phoneTypeOtherFax:I = 0x7f080179

.field public static final phoneTypePager:I = 0x7f08017a

.field public static final phoneTypeRadio:I = 0x7f08017b

.field public static final phoneTypeTelex:I = 0x7f08017c

.field public static final phoneTypeTtyTdd:I = 0x7f08017d

.field public static final phoneTypeWork:I = 0x7f08017e

.field public static final phoneTypeWorkMobile:I = 0x7f08017f

.field public static final phoneTypeWorkPager:I = 0x7f080180

.field public static final play:I = 0x7f080181

.field public static final playAllTxt:I = 0x7f080182

.field public static final playAllTxtDisabled:I = 0x7f080183

.field public static final playerError:I = 0x7f080184

.field public static final playerScreenDowbloadFileError:I = 0x7f080185

.field public static final pleaseWait:I = 0x7f080186

.field public static final port:I = 0x7f080187

.field public static final pref_AboutKey:I = 0x7f080188

.field public static final pref_About_AppVer_key:I = 0x7f080189

.field public static final pref_About_AppVer_title:I = 0x7f08018a

.field public static final pref_About_Category_title:I = 0x7f08018b

.field public static final pref_About_Reset_title:I = 0x7f08018c

.field public static final pref_Autosave_Toggle_summary:I = 0x7f08018d

.field public static final pref_Autosave_Toggle_title:I = 0x7f08018e

.field public static final pref_ChangePwdKey:I = 0x7f08018f

.field public static final pref_General_Category_title:I = 0x7f080190

.field public static final pref_GreetingsKey:I = 0x7f080191

.field public static final pref_Greetings_Toggle_summary:I = 0x7f080192

.field public static final pref_Greetings_Toggle_title:I = 0x7f080193

.field public static final pref_GroupByContact_key:I = 0x7f080194

.field public static final pref_Notifications_Category_title:I = 0x7f080195

.field public static final pref_Notifications_Toggle_key:I = 0x7f080196

.field public static final pref_Notifications_Toggle_summary:I = 0x7f080197

.field public static final pref_Notifications_Toggle_title:I = 0x7f080198

.field public static final pref_Password_summary:I = 0x7f080199

.field public static final pref_Password_title:I = 0x7f08019a

.field public static final pref_Proximity_Toggle_key:I = 0x7f08019b

.field public static final pref_Proximity_Toggle_summary:I = 0x7f08019c

.field public static final pref_Proximity_Toggle_title:I = 0x7f08019d

.field public static final pref_ResetAccount_key:I = 0x7f08019e

.field public static final pref_SecureConnection_key:I = 0x7f08019f

.field public static final pref_Secure_Connection_Toggle_summary:I = 0x7f0801a0

.field public static final pref_Secure_Connection_Toggle_title:I = 0x7f0801a1

.field public static final pref_Terms_Category_title:I = 0x7f0801a2

.field public static final pref_Transcription_Toggle_key:I = 0x7f0801a3

.field public static final pref_Transcription_Toggle_summary:I = 0x7f0801a4

.field public static final pref_Transcription_Toggle_title:I = 0x7f0801a5

.field public static final pref_about_terms_title:I = 0x7f0801a6

.field public static final pref_change_pwd:I = 0x7f0801a7

.field public static final pref_greetings:I = 0x7f0801a8

.field public static final pref_group_by_contact_summary:I = 0x7f0801a9

.field public static final pref_group_by_contact_title:I = 0x7f0801aa

.field public static final pref_transcriptions:I = 0x7f0801ab

.field public static final pref_vvm_over_wifi_desc:I = 0x7f0801ac

.field public static final pref_vvm_over_wifi_key:I = 0x7f0801ad

.field public static final pref_vvm_over_wifi_title:I = 0x7f0801ae

.field public static final privateNumber:I = 0x7f0801af

.field public static final prpolicy:I = 0x7f0801b0

.field public static final recordTxt:I = 0x7f0801b1

.field public static final record_greeting:I = 0x7f0801b2

.field public static final refresh_menu_title:I = 0x7f0801b3

.field public static final request_permissions_text:I = 0x7f0801b4

.field public static final restricted_user:I = 0x7f0801b5

.field public static final retrieveError:I = 0x7f0801b6

.field public static final retryFetchSeconds:I = 0x7f0801b7

.field public static final retryIntervalSeconds:I = 0x7f0801b8

.field public static final retypepasswordHintText:I = 0x7f0801b9

.field public static final save_menu_title:I = 0x7f0801ba

.field public static final saved:I = 0x7f0801bb

.field public static final saved_dialog_go_to_saved:I = 0x7f0801bc

.field public static final saved_tab_content_description:I = 0x7f0801bd

.field public static final saved_tab_title:I = 0x7f0801be

.field public static final searchBoxHint:I = 0x7f0801bf

.field public static final searchModeHint:I = 0x7f0801c0

.field public static final search_menu_title:I = 0x7f080024

.field public static final secondsText:I = 0x7f0801c1

.field public static final selectAllTxt:I = 0x7f0801c2

.field public static final selected:I = 0x7f0801c3

.field public static final send_message:I = 0x7f0801c4

.field public static final send_text_menu_title:I = 0x7f0801c5

.field public static final send_voice_menu_title:I = 0x7f0801c6

.field public static final settings_menu_title:I = 0x7f0801c7

.field public static final share_menu_title:I = 0x7f0801c8

.field public static final simSwapDialogText:I = 0x7f0801c9

.field public static final simSwapDialogTitle:I = 0x7f0801ca

.field public static final smsText_defText:I = 0x7f0801cb

.field public static final socketConnectionTimeout:I = 0x7f0801cc

.field public static final somethingIsntRight:I = 0x7f0801cd

.field public static final speaker:I = 0x7f0801ce

.field public static final speakerOff:I = 0x7f0801cf

.field public static final speaker_off:I = 0x7f0801d0

.field public static final speaker_on:I = 0x7f0801d1

.field public static final sslPort:I = 0x7f0801d2

.field public static final status_bar_notification_info_overflow:I = 0x7f080025

.field public static final stopText:I = 0x7f0801d3

.field public static final switch_off_desc:I = 0x7f0801d4

.field public static final switch_on_desc:I = 0x7f0801d5

.field public static final switch_turn_off_desc:I = 0x7f0801d6

.field public static final switch_turn_on_desc:I = 0x7f0801d7

.field public static final tcTitle:I = 0x7f0801d8

.field public static final tc_body:I = 0x7f0801d9

.field public static final tc_bodyTxt:I = 0x7f0801da

.field public static final terms_menu:I = 0x7f0801db

.field public static final terms_menu_summary_first_link:I = 0x7f0801dc

.field public static final terms_menu_summary_first_name:I = 0x7f0801dd

.field public static final terms_menu_summary_first_url:I = 0x7f0801de

.field public static final terms_menu_summary_second_link:I = 0x7f0801df

.field public static final terms_menu_summary_second_name:I = 0x7f0801e0

.field public static final terms_menu_summary_second_url:I = 0x7f0801e1

.field public static final text_copied_toast:I = 0x7f0801e2

.field public static final timerDefault:I = 0x7f0801e3

.field public static final trascriptionErrorText:I = 0x7f0801e4

.field public static final txtSetupLoading:I = 0x7f0801e5

.field public static final unchecked:I = 0x7f0801e6

.field public static final unknown:I = 0x7f0801e7

.field public static final unknownNumber:I = 0x7f0801e8

.field public static final unreadMessageText:I = 0x7f0801e9

.field public static final unreadMessagesTextEnd:I = 0x7f0801ea

.field public static final unsave_menu_title:I = 0x7f0801eb

.field public static final unselected:I = 0x7f0801ec

.field public static final updateDialogButton:I = 0x7f0801ed

.field public static final updateDialogText:I = 0x7f0801ee

.field public static final updateNotificationContentText:I = 0x7f0801ef

.field public static final updateNotificationTickerText:I = 0x7f0801f0

.field public static final updateNotificationTitleText:I = 0x7f0801f1

.field public static final updates_menu_title:I = 0x7f0801f2

.field public static final updatingAccountText:I = 0x7f0801f3

.field public static final updating_your_account:I = 0x7f0801f4

.field public static final useSSL:I = 0x7f0801f5

.field public static final v7_preference_off:I = 0x7f0801f6

.field public static final v7_preference_on:I = 0x7f0801f7

.field public static final view_caller_details_menu_title:I = 0x7f0801f8

.field public static final visual_voicemail:I = 0x7f0801f9

.field public static final vvm_over_wifi_popup_message:I = 0x7f0801fa

.field public static final welcomeMessagePhoneNumber:I = 0x7f0801fb

.field public static final welcomeMessageTranscription:I = 0x7f0801fc

.field public static final welcomeScreenChangePasswordExplanation:I = 0x7f0801fd

.field public static final welcomeScreenSetupExplanation:I = 0x7f0801fe

.field public static final widget_name:I = 0x7f0801ff

.field public static final wrong_passwords:I = 0x7f080200

.field public static final you_dont_have_enough_space:I = 0x7f080201

.field public static final your_voicemail_at_a_glance:I = 0x7f080202


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/att/mobile/android/vvm/widget/VVMWidgetUpdateService;
.super Landroid/app/IntentService;
.source "VVMWidgetUpdateService.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "VVMWidgetUpdateService"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "VVMWidgetUpdateService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 28
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 29
    .local v1, "context":Landroid/content/Context;
    new-instance v7, Landroid/widget/RemoteViews;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f040075

    invoke-direct {v7, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 30
    .local v7, "views":Landroid/widget/RemoteViews;
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getNewMessagesCount()I

    move-result v3

    .line 31
    .local v3, "messageCount":I
    const-string v8, "VVMWidgetUpdateService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onHandleIntent messageCount = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    if-lez v3, :cond_1

    .line 33
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 34
    .local v5, "strUnReadMessagesNum":Ljava/lang/String;
    const v8, 0x7f0f017e

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 35
    const v8, 0x7f0f017e

    invoke-virtual {v7, v8, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 36
    const v9, 0x7f0f017d

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v10, 0x7f0800a3

    invoke-virtual {p0, v10}, Lcom/att/mobile/android/vvm/widget/VVMWidgetUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v8, 0x1

    if-ne v3, v8, :cond_0

    const v8, 0x7f0801e9

    invoke-virtual {p0, v8}, Lcom/att/mobile/android/vvm/widget/VVMWidgetUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v8

    :goto_0
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v9, v8}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 42
    .end local v5    # "strUnReadMessagesNum":Ljava/lang/String;
    :goto_1
    const/4 v8, 0x0

    invoke-static {v1, v8}, Lcom/att/mobile/android/infra/utils/Utils;->getLaunchingIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 43
    .local v0, "confintent":Landroid/content/Intent;
    const/4 v8, 0x1

    const/high16 v9, 0x8000000

    invoke-static {v1, v8, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 45
    .local v4, "pendingIntent":Landroid/app/PendingIntent;
    const v8, 0x7f0f017d

    invoke-virtual {v7, v8, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 46
    new-instance v6, Landroid/content/ComponentName;

    const-class v8, Lcom/att/mobile/android/vvm/widget/VVMWidget;

    invoke-direct {v6, v1, v8}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v6, "thisWidget":Landroid/content/ComponentName;
    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 48
    .local v2, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v2, v6, v7}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 49
    return-void

    .line 36
    .end local v0    # "confintent":Landroid/content/Intent;
    .end local v2    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v4    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v6    # "thisWidget":Landroid/content/ComponentName;
    .restart local v5    # "strUnReadMessagesNum":Ljava/lang/String;
    :cond_0
    const v8, 0x7f0801ea

    invoke-virtual {p0, v8}, Lcom/att/mobile/android/vvm/widget/VVMWidgetUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 39
    .end local v5    # "strUnReadMessagesNum":Ljava/lang/String;
    :cond_1
    const v8, 0x7f0f017e

    const/16 v9, 0x8

    invoke-virtual {v7, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 40
    const v8, 0x7f0f017d

    const v9, 0x7f0800a3

    invoke-virtual {p0, v9}, Lcom/att/mobile/android/vvm/widget/VVMWidgetUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_1
.end method

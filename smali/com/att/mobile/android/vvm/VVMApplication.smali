.class public Lcom/att/mobile/android/vvm/VVMApplication;
.super Landroid/app/Application;
.source "VVMApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;
    }
.end annotation


# static fields
.field private static final CHECK_IF_ON_BACKGROUND:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VVMApplication"

.field private static appcontext:Landroid/content/Context;

.field private static applicationVersion:Ljava/lang/String;

.field private static isDebugMode:Z

.field private static isMemoryLow:Z

.field private static vvmapplication:Lcom/att/mobile/android/vvm/VVMApplication;


# instance fields
.field private appHelperHandler:Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;

.field private applicationVersionCode:I

.field private clientId:Ljava/lang/String;

.field private deviceAudioMode:I

.field private deviceMusicVolume:I

.field private isApplicationSpeakerOn:Z

.field private isCurrentlyApplicationAudioMode:Z

.field private isDeviceSpeakerOn:Z

.field private isVisible:Z

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeLockLocked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    sput-boolean v1, Lcom/att/mobile/android/vvm/VVMApplication;->isMemoryLow:Z

    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/vvm/VVMApplication;->applicationVersion:Ljava/lang/String;

    .line 84
    sput-boolean v1, Lcom/att/mobile/android/vvm/VVMApplication;->isDebugMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 42
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible:Z

    .line 45
    iput-object v2, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 48
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLockLocked:Z

    .line 51
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode:Z

    .line 64
    iput v1, p0, Lcom/att/mobile/android/vvm/VVMApplication;->deviceAudioMode:I

    .line 66
    iput v1, p0, Lcom/att/mobile/android/vvm/VVMApplication;->deviceMusicVolume:I

    .line 69
    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn:Z

    .line 76
    iput-object v2, p0, Lcom/att/mobile/android/vvm/VVMApplication;->clientId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/VVMApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/VVMApplication;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode:Z

    return v0
.end method

.method static synthetic access$102(Lcom/att/mobile/android/vvm/VVMApplication;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/VVMApplication;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode:Z

    return p1
.end method

.method static synthetic access$200(Lcom/att/mobile/android/vvm/VVMApplication;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/VVMApplication;

    .prologue
    .line 38
    iget v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->deviceAudioMode:I

    return v0
.end method

.method static synthetic access$300(Lcom/att/mobile/android/vvm/VVMApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/VVMApplication;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isDeviceSpeakerOn:Z

    return v0
.end method

.method static synthetic access$400(Lcom/att/mobile/android/vvm/VVMApplication;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/VVMApplication;

    .prologue
    .line 38
    iget v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->deviceMusicVolume:I

    return v0
.end method

.method static synthetic access$500()Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/att/mobile/android/vvm/VVMApplication;->appcontext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/att/mobile/android/vvm/VVMApplication;)Z
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/VVMApplication;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible:Z

    return v0
.end method

.method public static getApplicationVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 500
    sget-object v0, Lcom/att/mobile/android/vvm/VVMApplication;->applicationVersion:Ljava/lang/String;

    return-object v0
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/att/mobile/android/vvm/VVMApplication;->appcontext:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance()Lcom/att/mobile/android/vvm/VVMApplication;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/att/mobile/android/vvm/VVMApplication;->vvmapplication:Lcom/att/mobile/android/vvm/VVMApplication;

    return-object v0
.end method

.method public static getVvmapplication()Lcom/att/mobile/android/vvm/VVMApplication;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/att/mobile/android/vvm/VVMApplication;->vvmapplication:Lcom/att/mobile/android/vvm/VVMApplication;

    return-object v0
.end method

.method public static isAdminUser(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 113
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    .line 114
    .local v0, "uh":Landroid/os/UserHandle;
    const-string v5, "user"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    .line 115
    .local v1, "um":Landroid/os/UserManager;
    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {v1, v0}, Landroid/os/UserManager;->getSerialNumberForUser(Landroid/os/UserHandle;)J

    move-result-wide v2

    .line 118
    .local v2, "userSerialNumber":J
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "userSerialNumber = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const-wide/16 v6, 0x0

    cmp-long v5, v6, v2

    if-nez v5, :cond_0

    const/4 v4, 0x1

    .line 123
    .end local v2    # "userSerialNumber":J
    :cond_0
    return v4
.end method

.method public static isDebugMode()Z
    .locals 1

    .prologue
    .line 575
    sget-boolean v0, Lcom/att/mobile/android/vvm/VVMApplication;->isDebugMode:Z

    return v0
.end method

.method public static declared-synchronized isMemoryLow()Z
    .locals 2

    .prologue
    .line 429
    const-class v0, Lcom/att/mobile/android/vvm/VVMApplication;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/att/mobile/android/vvm/VVMApplication;->isMemoryLow:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static setAppcontext(Landroid/content/Context;)V
    .locals 0
    .param p0, "appcontext"    # Landroid/content/Context;

    .prologue
    .line 87
    sput-object p0, Lcom/att/mobile/android/vvm/VVMApplication;->appcontext:Landroid/content/Context;

    .line 88
    return-void
.end method

.method public static setApplicationVersion(Ljava/lang/String;)V
    .locals 0
    .param p0, "applicationVersion"    # Ljava/lang/String;

    .prologue
    .line 105
    sput-object p0, Lcom/att/mobile/android/vvm/VVMApplication;->applicationVersion:Ljava/lang/String;

    .line 106
    return-void
.end method

.method private setClientId()V
    .locals 6

    .prologue
    .line 545
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "ATTV:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 548
    .local v1, "clientIdBuilder":Ljava/lang/StringBuilder;
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 549
    .local v2, "phoneModel":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 550
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    :cond_0
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 556
    .local v0, "AndroidVersion":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 557
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 558
    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    :cond_1
    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/att/mobile/android/vvm/VVMApplication;->clientId:Ljava/lang/String;

    .line 566
    const-string v3, "VVMApplication"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "VVMApplication.setClientId() - clientId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/att/mobile/android/vvm/VVMApplication;->clientId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    return-void
.end method

.method public static setDebugMode(Z)V
    .locals 0
    .param p0, "isDebugMode"    # Z

    .prologue
    .line 109
    sput-boolean p0, Lcom/att/mobile/android/vvm/VVMApplication;->isDebugMode:Z

    .line 110
    return-void
.end method

.method public static declared-synchronized setMemoryLow(Z)V
    .locals 2
    .param p0, "isMemoryLow"    # Z

    .prologue
    .line 425
    const-class v0, Lcom/att/mobile/android/vvm/VVMApplication;

    monitor-enter v0

    :try_start_0
    sput-boolean p0, Lcom/att/mobile/android/vvm/VVMApplication;->isMemoryLow:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    monitor-exit v0

    return-void

    .line 425
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static setVvmapplication(Lcom/att/mobile/android/vvm/VVMApplication;)V
    .locals 0
    .param p0, "vvmapplication"    # Lcom/att/mobile/android/vvm/VVMApplication;

    .prologue
    .line 101
    sput-object p0, Lcom/att/mobile/android/vvm/VVMApplication;->vvmapplication:Lcom/att/mobile/android/vvm/VVMApplication;

    .line 102
    return-void
.end method


# virtual methods
.method public acquireWakeLock()V
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLockLocked:Z

    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLockLocked:Z

    .line 221
    :cond_0
    return-void
.end method

.method public clearNotification()V
    .locals 2

    .prologue
    .line 470
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.att.mobile.android.vvm.INTENT_CLEAR_NOTIFICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 471
    .local v0, "intentService":Landroid/content/Intent;
    const-class v1, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 472
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/VVMApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 473
    return-void
.end method

.method public getApplicationVersionCode()I
    .locals 1

    .prologue
    .line 508
    iget v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->applicationVersionCode:I

    return v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->clientId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 538
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->setClientId()V

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public isAccesibilityOn()Z
    .locals 4

    .prologue
    .line 409
    const-string v3, "accessibility"

    invoke-virtual {p0, v3}, Lcom/att/mobile/android/vvm/VVMApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 410
    .local v0, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    .line 411
    .local v1, "isAccessibilityEnabled":Z
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    .line 412
    .local v2, "isExploreByTouchEnabled":Z
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isApplicationSpeakerOn()Z
    .locals 1

    .prologue
    .line 421
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn:Z

    return v0
.end method

.method public isCurrentlyApplicationAudioMode()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible:Z

    return v0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 130
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/att/mobile/android/vvm/VVMApplication;->setAppcontext(Landroid/content/Context;)V

    .line 131
    sget-object v5, Lcom/att/mobile/android/vvm/VVMApplication;->vvmapplication:Lcom/att/mobile/android/vvm/VVMApplication;

    if-nez v5, :cond_0

    .line 132
    invoke-static {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->setVvmapplication(Lcom/att/mobile/android/vvm/VVMApplication;)V

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->createInstance(Landroid/content/Context;)V

    .line 139
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/att/mobile/android/vvm/control/OperationsQueue;->createInstance(Landroid/content/Context;)V

    .line 142
    const-string v5, "power"

    invoke-virtual {p0, v5}, Lcom/att/mobile/android/vvm/VVMApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 143
    .local v2, "mPowerManager":Landroid/os/PowerManager;
    const/16 v5, 0x1a

    const-string v6, "Flashlight"

    invoke-virtual {v2, v5, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    iput-object v5, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 146
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/att/mobile/android/infra/utils/FontUtils;->initFonts(Landroid/content/Context;)V

    .line 150
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/att/mobile/android/infra/sim/SimManager;->getInstance(Landroid/content/Context;)Lcom/att/mobile/android/infra/sim/SimManager;

    move-result-object v3

    .line 151
    .local v3, "simManager":Lcom/att/mobile/android/infra/sim/SimManager;
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getCurrentSetupState()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 152
    invoke-virtual {v3}, Lcom/att/mobile/android/infra/sim/SimManager;->startListening()V

    .line 153
    invoke-virtual {v3}, Lcom/att/mobile/android/infra/sim/SimManager;->startSimValidation()V

    .line 156
    :cond_1
    new-instance v5, Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;-><init>(Lcom/att/mobile/android/vvm/VVMApplication$1;)V

    iput-object v5, p0, Lcom/att/mobile/android/vvm/VVMApplication;->appHelperHandler:Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;

    .line 158
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->setApplicationVersionFromPackgeInfo()V

    .line 160
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->setClientId()V

    .line 165
    :try_start_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v0, v5, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 167
    .local v0, "b":Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 168
    const-string v5, "debugMode"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 169
    .local v4, "val":Ljava/lang/Object;
    if-eqz v4, :cond_2

    .line 170
    check-cast v4, Ljava/lang/Boolean;

    .end local v4    # "val":Ljava/lang/Object;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-static {v5}, Lcom/att/mobile/android/vvm/VVMApplication;->setDebugMode(Z)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_2
    :goto_0
    invoke-static {}, Lcom/att/mobile/android/vvm/control/ATTM/AttmUtils;->initAttmService()V

    .line 179
    invoke-static {p0}, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->registerReceiverDynamicly(Landroid/content/Context;)V

    .line 181
    sget-object v5, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    invoke-virtual {v5, p0}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->loadConnectedDevices(Landroid/content/Context;)V

    .line 182
    return-void

    .line 173
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 189
    invoke-static {p0}, Lcom/att/mobile/android/vvm/control/receivers/LowMemoryReceiver;->unregisterReceiverDynamicly(Landroid/content/Context;)V

    .line 190
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 191
    return-void
.end method

.method public releaseWakeLock()V
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLockLocked:Z

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->mWakeLockLocked:Z

    .line 231
    :cond_0
    return-void
.end method

.method public restoreDeviceAudioMode()V
    .locals 2

    .prologue
    .line 322
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/att/mobile/android/vvm/VVMApplication$1;

    invoke-direct {v1, p0}, Lcom/att/mobile/android/vvm/VVMApplication$1;-><init>(Lcom/att/mobile/android/vvm/VVMApplication;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 352
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 353
    return-void
.end method

.method public setApplicationAudioMode()V
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x1

    .line 240
    iget-boolean v4, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode:Z

    if-nez v4, :cond_0

    .line 245
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/VVMApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 246
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v4

    iput v4, p0, Lcom/att/mobile/android/vvm/VVMApplication;->deviceAudioMode:I

    .line 247
    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v4

    iput-boolean v4, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isDeviceSpeakerOn:Z

    .line 248
    const-string v4, "VVMApplication"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VVMApplication.setApplicationAudioMode() - device\'s audio mode is :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/att/mobile/android/vvm/VVMApplication;->deviceAudioMode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const-string v5, "VVMApplication"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VVMApplication.setApplicationAudioMode() - device\'s speaker is "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v4, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isDeviceSpeakerOn:Z

    if-eqz v4, :cond_2

    const-string v4, "ON"

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 257
    const-string v4, "VVMApplication"

    const-string v5, "VVMApplication.setApplicationAudioMode() - music is currently active in the device"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const/4 v4, 0x3

    .line 264
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v4

    iput v4, p0, Lcom/att/mobile/android/vvm/VVMApplication;->deviceMusicVolume:I

    .line 265
    const-string v4, "VVMApplication"

    const-string v5, "VVMApplication.setApplicationAudioMode() - music turned OFF"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :goto_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v4, v8, :cond_4

    .line 278
    :try_start_0
    const-class v4, Landroid/media/AudioManager;

    const-string v5, "MODE_IN_COMMUNICATION"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 279
    .local v2, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    .line 281
    .local v3, "mode":I
    const-string v4, "VVMApplication"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "VVMApplication.setApplicationAudioMode() - going to set audio mode "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    .end local v2    # "field":Ljava/lang/reflect/Field;
    .end local v3    # "mode":I
    :goto_2
    iput-boolean v7, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode:Z

    .line 302
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->isAccesibilityOn()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 303
    iput-boolean v7, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn:Z

    .line 305
    :cond_1
    iget-boolean v4, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn:Z

    invoke-virtual {p0, v4}, Lcom/att/mobile/android/vvm/VVMApplication;->setIsApplicationSpeakerOn(Z)V

    .line 312
    const-string v4, "VVMApplication"

    const-string v5, "VVMApplication.setApplicationAudioMode() - application\'s audio mode is set (including last speaker state)"

    invoke-static {v4, v5}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    return-void

    .line 251
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_2
    const-string v4, "OFF"

    goto :goto_0

    .line 269
    :cond_3
    const/4 v4, -0x1

    iput v4, p0, Lcom/att/mobile/android/vvm/VVMApplication;->deviceMusicVolume:I

    goto :goto_1

    .line 285
    :catch_0
    move-exception v1

    .line 286
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "VVMApplication"

    const-string v5, "VVMApplication.setApplicationAudioMode() Error setting MODE_IN_COMMUNICATION"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 289
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v4, v8, :cond_5

    .line 290
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->setMode(I)V

    .line 291
    const-string v4, "VVMApplication"

    const-string v5, "VVMApplication.setPlayerAudioMode() - going to set audio mode IN_CALL"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 294
    :cond_5
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->setMode(I)V

    goto :goto_2
.end method

.method public setApplicationVersionFromPackgeInfo()V
    .locals 5

    .prologue
    .line 514
    :try_start_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 515
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 514
    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 516
    .local v1, "manager":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 517
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v2}, Lcom/att/mobile/android/vvm/VVMApplication;->setApplicationVersion(Ljava/lang/String;)V

    .line 518
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v2, p0, Lcom/att/mobile/android/vvm/VVMApplication;->applicationVersionCode:I

    .line 528
    .end local v1    # "manager":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 520
    .restart local v1    # "manager":Landroid/content/pm/PackageInfo;
    :cond_0
    const-string v2, ""

    invoke-static {v2}, Lcom/att/mobile/android/vvm/VVMApplication;->setApplicationVersion(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 523
    .end local v1    # "manager":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 524
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "VVMApplication"

    const-string v3, "PreferencesActivity.setApplicationVersion() An exception was thrown while getting package info."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setCurrentlyApplicationAudioMode(Z)V
    .locals 0
    .param p1, "isCurrentlyApplicationAudioMode"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode:Z

    .line 59
    return-void
.end method

.method public setIsApplicationSpeakerOn(Z)V
    .locals 3
    .param p1, "isApplicationSpeakerOn"    # Z

    .prologue
    .line 364
    if-eqz p1, :cond_3

    .line 365
    const-string v1, "VVMApplication"

    const-string v2, "VVMApplication.setIsApplicationSpeakerOn() - going to set application speaker state to ON"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    :goto_0
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isApplicationSpeakerOn:Z

    .line 378
    if-eqz p1, :cond_0

    .line 379
    sget-object v1, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->stopRouteAudioToBluetooth(Landroid/content/Context;)V

    .line 383
    :cond_0
    iget-boolean v1, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isCurrentlyApplicationAudioMode:Z

    if-eqz v1, :cond_2

    .line 384
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/att/mobile/android/vvm/VVMApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 385
    .local v0, "audioManager":Landroid/media/AudioManager;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_1

    .line 386
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 388
    :cond_1
    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 389
    if-eqz p1, :cond_4

    .line 390
    const-string v1, "VVMApplication"

    const-string v2, "VVMApplication.setIsApplicationSpeakerOn() - application speaker state was set ON"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :goto_1
    if-nez p1, :cond_2

    .line 402
    sget-object v1, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    invoke-virtual {v1, p0}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->startRouteAudioToBluetooth(Landroid/content/Context;)V

    .line 406
    .end local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_2
    return-void

    .line 369
    :cond_3
    const-string v1, "VVMApplication"

    const-string v2, "VVMApplication.setIsApplicationSpeakerOn() - going to set application speaker state to OFF"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 394
    .restart local v0    # "audioManager":Landroid/media/AudioManager;
    :cond_4
    const-string v1, "VVMApplication"

    const-string v2, "VVMApplication.setIsApplicationSpeakerOn() - application speaker state was set OFF"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setVisible(Z)V
    .locals 4
    .param p1, "isVisible"    # Z

    .prologue
    .line 198
    iput-boolean p1, p0, Lcom/att/mobile/android/vvm/VVMApplication;->isVisible:Z

    .line 200
    if-nez p1, :cond_0

    .line 204
    iget-object v0, p0, Lcom/att/mobile/android/vvm/VVMApplication;->appHelperHandler:Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;

    iget-object v1, p0, Lcom/att/mobile/android/vvm/VVMApplication;->appHelperHandler:Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;

    const/4 v2, 0x0

    .line 205
    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2bc

    .line 204
    invoke-virtual {v0, v1, v2, v3}, Lcom/att/mobile/android/vvm/VVMApplication$AppHelperHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/att/mobile/android/vvm/VVMApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/att/mobile/android/infra/utils/TimeDateUtils;->refreshDateFormat(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public updateNotification()V
    .locals 2

    .prologue
    .line 479
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.att.mobile.android.vvm.ACTION_UPDATE_NOTIFICATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 480
    .local v0, "intentService":Landroid/content/Intent;
    const-class v1, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 481
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/VVMApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 482
    return-void
.end method

.method public updateNotificationAfterRefresh()V
    .locals 2

    .prologue
    .line 489
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.att.mobile.android.vvm.ACTION_UPDATE_NOTIFICATION_AFTER_REFRESH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 491
    .local v0, "intentService":Landroid/content/Intent;
    const-class v1, Lcom/att/mobile/android/vvm/control/receivers/NotificationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 492
    invoke-virtual {p0, v0}, Lcom/att/mobile/android/vvm/VVMApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 493
    return-void
.end method

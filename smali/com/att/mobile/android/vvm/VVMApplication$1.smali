.class Lcom/att/mobile/android/vvm/VVMApplication$1;
.super Ljava/lang/Object;
.source "VVMApplication.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/VVMApplication;->restoreDeviceAudioMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/att/mobile/android/vvm/VVMApplication;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/VVMApplication;)V
    .locals 0
    .param p1, "this$0"    # Lcom/att/mobile/android/vvm/VVMApplication;

    .prologue
    .line 322
    iput-object p1, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 324
    iget-object v1, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/VVMApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 325
    .local v0, "audioManager":Landroid/media/AudioManager;
    iget-object v1, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->access$100(Lcom/att/mobile/android/vvm/VVMApplication;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 327
    iget-object v1, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->access$200(Lcom/att/mobile/android/vvm/VVMApplication;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 328
    iget-object v1, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->access$300(Lcom/att/mobile/android/vvm/VVMApplication;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 332
    iget-object v1, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-static {v1}, Lcom/att/mobile/android/vvm/VVMApplication;->access$400(Lcom/att/mobile/android/vvm/VVMApplication;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 333
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    .line 334
    invoke-static {v2}, Lcom/att/mobile/android/vvm/VVMApplication;->access$400(Lcom/att/mobile/android/vvm/VVMApplication;)I

    move-result v2

    .line 333
    invoke-virtual {v0, v1, v2, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 337
    :cond_0
    const-string v1, "VVMApplication"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VVMApplication.restoreDeviceAudioMode() isBluetoothScoOn = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 339
    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  deviceAudioMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/VVMApplication;->access$200(Lcom/att/mobile/android/vvm/VVMApplication;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isDeviceSpeakerOn = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-static {v3}, Lcom/att/mobile/android/vvm/VVMApplication;->access$300(Lcom/att/mobile/android/vvm/VVMApplication;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 337
    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    sget-object v1, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->INSTANCE:Lcom/att/mobile/android/infra/utils/BluetoothRouter;

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->access$500()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/infra/utils/BluetoothRouter;->stopRouteAudioToBluetooth(Landroid/content/Context;)V

    .line 344
    :cond_1
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->setMode(I)V

    .line 348
    iget-object v1, p0, Lcom/att/mobile/android/vvm/VVMApplication$1;->this$0:Lcom/att/mobile/android/vvm/VVMApplication;

    invoke-static {v1, v4}, Lcom/att/mobile/android/vvm/VVMApplication;->access$102(Lcom/att/mobile/android/vvm/VVMApplication;Z)Z

    .line 349
    const-string v1, "VVMApplication"

    const-string v2, "VVMApplication.restoreDeviceAudioMode() - device\'s audio mode state restored"

    invoke-static {v1, v2}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    return-void
.end method

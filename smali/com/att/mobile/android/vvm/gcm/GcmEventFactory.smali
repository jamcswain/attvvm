.class Lcom/att/mobile/android/vvm/gcm/GcmEventFactory;
.super Ljava/lang/Object;
.source "GcmEventFactory.java"


# static fields
.field private static final msEventsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/att/mobile/android/vvm/gcm/GcmEventFactory$1;

    invoke-direct {v0}, Lcom/att/mobile/android/vvm/gcm/GcmEventFactory$1;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/gcm/GcmEventFactory;->msEventsMap:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getEventCommand(Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "eventType"    # Ljava/lang/String;

    .prologue
    .line 28
    sget-object v0, Lcom/att/mobile/android/vvm/gcm/GcmEventFactory;->msEventsMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    return-object v0
.end method

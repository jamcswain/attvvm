.class Lcom/att/mobile/android/vvm/gcm/GcmMessage;
.super Ljava/lang/Object;
.source "GcmMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mEventList:Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "eventList"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/att/mobile/android/vvm/gcm/GcmMessage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/att/mobile/android/vvm/gcm/GcmMessage;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/att/mobile/android/vvm/gcm/GcmMessage;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method getEventTypes()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lcom/att/mobile/android/vvm/gcm/GcmMessage;->mEventList:Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;

    if-nez v0, :cond_0

    .line 20
    sget-object v0, Lcom/att/mobile/android/vvm/gcm/GcmMessage;->TAG:Ljava/lang/String;

    const-string v1, "event list is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 24
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/att/mobile/android/vvm/gcm/GcmMessage;->mEventList:Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;

    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;->getEvents()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.class Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;
.super Ljava/lang/Object;
.source "GcmMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/gcm/GcmMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList$Event;
    }
.end annotation


# instance fields
.field private mDate:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "date"
    .end annotation
.end field

.field private mEvents:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "events"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList$Event;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/att/mobile/android/vvm/gcm/GcmMessage;


# direct methods
.method private constructor <init>(Lcom/att/mobile/android/vvm/gcm/GcmMessage;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;->this$0:Lcom/att/mobile/android/vvm/gcm/GcmMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEvents()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;->mEvents:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 41
    invoke-static {}, Lcom/att/mobile/android/vvm/gcm/GcmMessage;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "events is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_0
    return-object v1

    .line 45
    :cond_1
    iget-object v2, p0, Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList;->mEvents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList$Event;

    .line 47
    .local v0, "event":Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList$Event;
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList$Event;->getType()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 49
    invoke-static {}, Lcom/att/mobile/android/vvm/gcm/GcmMessage;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "event type is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {v0}, Lcom/att/mobile/android/vvm/gcm/GcmMessage$EventList$Event;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.class public Lcom/att/mobile/android/vvm/watson/WatsonHandler;
.super Ljava/lang/Object;
.source "WatsonHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;,
        Lcom/att/mobile/android/vvm/watson/WatsonHandler$WiFiConnectivityInterface;
    }
.end annotation


# static fields
.field private static final GET_TOKEN_URL:Ljava/lang/String; = "https://api.att.com/oauth/token"

.field private static final GET_TRANSCRIPTION_URL:Ljava/lang/String; = "https://api.att.com/speech/v3/speechToText"

.field private static final HTTP_EXPECTATION_FAILED:I = 0x1a1

.field private static final PhoneTagEnd:Ljava/lang/String; = "_END_PHONE"

.field private static final PhoneTagStart:Ljava/lang/String; = "_PHONE"

.field private static final RETRY_TIME:I = 0xea60

.field private static final TAG:Ljava/lang/String; = "WatsonHandler"

.field private static final TIMEOUT_CONNECTION:I = 0x36ee80

.field private static final TIMEOUT_RETRY_TIME1:I = 0x493e0

.field private static final TIMEOUT_RETRY_TIME2:I = 0x1b7740

.field private static accessToken:Ljava/lang/String; = null

.field private static final client_ID:Ljava/lang/String; = "dxngpbidevxl7zdxox7wducydooqd6q0"

.field private static final client_Secret:Ljava/lang/String; = "eaawymjxawvdqjsia96huqr3bpilprpl"

.field static instance:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

.field private static lock:Ljava/lang/Object;


# instance fields
.field public context:Landroid/content/Context;

.field private is:Ljava/io/InputStream;

.field private jObj:Lorg/json/JSONObject;

.field private json:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->accessToken:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->lock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/att/mobile/android/vvm/watson/WatsonHandler;JLjava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/watson/WatsonHandler;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->requestTranscription(JLjava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/att/mobile/android/vvm/watson/WatsonHandler;IJLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/att/mobile/android/vvm/watson/WatsonHandler;
    .param p1, "x1"    # I
    .param p2, "x2"    # J
    .param p4, "x3"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->onHttpError(IJLjava/lang/String;)V

    return-void
.end method

.method public static createInstance(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    sget-object v1, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 73
    :try_start_0
    sget-object v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->instance:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    .line 74
    new-instance v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    invoke-direct {v0, p0}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->instance:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    .line 76
    :cond_0
    monitor-exit v1

    .line 77
    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance()Lcom/att/mobile/android/vvm/watson/WatsonHandler;
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->instance:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    if-nez v0, :cond_0

    .line 81
    const-string v0, "WatsonHandler"

    const-string v1, "WatsonHandler.getInstance() must call create instance before calling getInstance()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    sget-object v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->instance:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    return-object v0
.end method

.method private getToken(JLjava/lang/String;)I
    .locals 23
    .param p1, "messageID"    # J
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    .line 333
    const-string v19, "WatsonHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getToken messageID="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " fileName="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const/16 v16, 0x0

    .line 336
    .local v16, "urlConnection":Ljava/net/HttpURLConnection;
    const/4 v6, 0x0

    .line 337
    .local v6, "os":Ljava/io/OutputStream;
    const/16 v17, 0x0

    .line 339
    .local v17, "writer":Ljava/io/BufferedWriter;
    const/16 v12, 0xc8

    .line 344
    .local v12, "status_code":I
    :try_start_0
    new-instance v15, Ljava/net/URL;

    const-string v19, "https://api.att.com/oauth/token"

    move-object/from16 v0, v19

    invoke-direct {v15, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 345
    .local v15, "url":Ljava/net/URL;
    invoke-virtual {v15}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v19

    move-object/from16 v0, v19

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object/from16 v16, v0

    .line 347
    move-object/from16 v0, v16

    instance-of v0, v0, Ljavax/net/ssl/HttpsURLConnection;

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 349
    move-object/from16 v0, v16

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object/from16 v19, v0

    new-instance v20, Lcom/att/mobile/android/infra/network/TLSSocketFactory;

    invoke-direct/range {v20 .. v20}, Lcom/att/mobile/android/infra/network/TLSSocketFactory;-><init>()V

    invoke-virtual/range {v19 .. v20}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 352
    :cond_0
    const-string v19, "POST"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 353
    const/16 v19, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 356
    new-instance v19, Landroid/net/Uri$Builder;

    invoke-direct/range {v19 .. v19}, Landroid/net/Uri$Builder;-><init>()V

    const-string v20, "client_id"

    const-string v21, "dxngpbidevxl7zdxox7wducydooqd6q0"

    invoke-virtual/range {v19 .. v21}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v19

    const-string v20, "client_secret"

    const-string v21, "eaawymjxawvdqjsia96huqr3bpilprpl"

    invoke-virtual/range {v19 .. v21}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v19

    const-string v20, "grant_type"

    const-string v21, "client_credentials"

    invoke-virtual/range {v19 .. v21}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v19

    const-string v20, "scope"

    const-string v21, "SPEECH"

    invoke-virtual/range {v19 .. v21}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 357
    .local v3, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v7

    .line 358
    .local v7, "query":Ljava/lang/String;
    const-string v19, "WatsonHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "WatsonHandler.getToken() query="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    .line 361
    new-instance v18, Ljava/io/BufferedWriter;

    new-instance v19, Ljava/io/OutputStreamWriter;

    const-string v20, "UTF-8"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v6, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct/range {v18 .. v19}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 362
    .end local v17    # "writer":Ljava/io/BufferedWriter;
    .local v18, "writer":Ljava/io/BufferedWriter;
    :try_start_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 363
    invoke-virtual/range {v18 .. v18}, Ljava/io/BufferedWriter;->flush()V

    .line 366
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->connect()V

    .line 368
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_18
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_17
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_16
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result v12

    .line 386
    if-eqz v18, :cond_1

    .line 387
    :try_start_2
    invoke-virtual/range {v18 .. v18}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 389
    :cond_1
    :goto_0
    if-eqz v6, :cond_2

    .line 390
    :try_start_3
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    .line 394
    :cond_2
    :goto_1
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v19

    const-string v20, "simulatetokenerror"

    const-class v21, Ljava/lang/Boolean;

    const/16 v22, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    invoke-virtual/range {v19 .. v22}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 395
    .local v11, "simulateTokenError":Z
    if-eqz v11, :cond_4

    .line 396
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v19

    const-string v20, "TokenErrorCode"

    const-class v21, Ljava/lang/String;

    const-string v22, "501"

    invoke-virtual/range {v19 .. v22}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 397
    .local v14, "status_code_string":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    const-string v20, "Unknown"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 398
    const-string v14, "501"

    .line 400
    :cond_3
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 401
    const-string v19, "WatsonHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "WatsonHandler.getToken() simulate Token error Error code - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    .end local v14    # "status_code_string":Ljava/lang/String;
    :cond_4
    const/16 v19, 0xc8

    move/from16 v0, v19

    if-ne v12, v0, :cond_8

    .line 406
    const/4 v8, 0x0

    .line 408
    .local v8, "reader":Ljava/io/BufferedReader;
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->is:Ljava/io/InputStream;

    .line 409
    new-instance v9, Ljava/io/BufferedReader;

    new-instance v19, Ljava/io/InputStreamReader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->is:Ljava/io/InputStream;

    move-object/from16 v20, v0

    const-string v21, "iso-8859-1"

    invoke-direct/range {v19 .. v21}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    const/16 v20, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v9, v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_14
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 410
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .local v9, "reader":Ljava/io/BufferedReader;
    :try_start_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 411
    .local v10, "sb":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .line 412
    .local v5, "line":Ljava/lang/String;
    :goto_2
    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_13

    .line 413
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_2

    .line 418
    .end local v5    # "line":Ljava/lang/String;
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v4

    move-object v8, v9

    .line 419
    .end local v9    # "reader":Ljava/io/BufferedReader;
    .local v4, "e":Ljava/lang/Exception;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :goto_3
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/att/mobile/android/infra/utils/Utils;->isWiFiOn(Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_17

    const/16 v12, 0x1f8

    .line 420
    :goto_4
    const-string v19, "Buffer Error"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Error converting result "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->is:Ljava/io/InputStream;

    move-object/from16 v19, v0

    if-eqz v19, :cond_5

    .line 423
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->is:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_13

    .line 425
    :cond_5
    :goto_5
    if-eqz v8, :cond_6

    .line 426
    :try_start_8
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_10

    .line 428
    :cond_6
    :goto_6
    if-eqz v16, :cond_7

    .line 429
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 433
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->json:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1b

    .line 435
    :try_start_9
    new-instance v19, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->json:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->jObj:Lorg/json/JSONObject;

    .line 436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->jObj:Lorg/json/JSONObject;

    move-object/from16 v19, v0

    const-string v20, "access_token"

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    sput-object v19, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->accessToken:Ljava/lang/String;

    .line 437
    const-string v19, "WatsonHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "WatsonHandler.getToken() succeeded accessToken - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->accessToken:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_4

    .end local v8    # "reader":Ljava/io/BufferedReader;
    :cond_8
    :goto_8
    move-object/from16 v17, v18

    .end local v18    # "writer":Ljava/io/BufferedWriter;
    .restart local v17    # "writer":Ljava/io/BufferedWriter;
    move v13, v12

    .line 446
    .end local v3    # "builder":Landroid/net/Uri$Builder;
    .end local v7    # "query":Ljava/lang/String;
    .end local v11    # "simulateTokenError":Z
    .end local v12    # "status_code":I
    .end local v15    # "url":Ljava/net/URL;
    .local v13, "status_code":I
    :goto_9
    return v13

    .line 370
    .end local v13    # "status_code":I
    .restart local v12    # "status_code":I
    :catch_1
    move-exception v4

    .line 371
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    :goto_a
    :try_start_a
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 372
    const/16 v12, 0x1a1

    .line 373
    const-string v19, "WatsonHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "WatsonHandler.getToken() UnsupportedEncodingException status_code - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 386
    if-eqz v17, :cond_9

    .line 387
    :try_start_b
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedWriter;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 389
    :cond_9
    :goto_b
    if-eqz v6, :cond_a

    .line 390
    :try_start_c
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    :cond_a
    :goto_c
    move v13, v12

    .end local v12    # "status_code":I
    .restart local v13    # "status_code":I
    goto :goto_9

    .line 375
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v13    # "status_code":I
    .restart local v12    # "status_code":I
    :catch_2
    move-exception v4

    .line 376
    .local v4, "e":Ljava/io/IOException;
    :goto_d
    :try_start_d
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/att/mobile/android/infra/utils/Utils;->isWiFiOn(Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_d

    const/16 v12, 0x1f8

    .line 378
    :goto_e
    const-string v19, "WatsonHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "WatsonHandler.getToken() IOException status_code - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 386
    if-eqz v17, :cond_b

    .line 387
    :try_start_e
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedWriter;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_9

    .line 389
    :cond_b
    :goto_f
    if-eqz v6, :cond_c

    .line 390
    :try_start_f
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_a

    :cond_c
    :goto_10
    move v13, v12

    .end local v12    # "status_code":I
    .restart local v13    # "status_code":I
    goto :goto_9

    .line 377
    .end local v13    # "status_code":I
    .restart local v12    # "status_code":I
    :cond_d
    const/16 v12, 0x1a1

    goto :goto_e

    .line 380
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 381
    .local v4, "e":Ljava/lang/Exception;
    :goto_11
    :try_start_10
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/att/mobile/android/infra/utils/Utils;->isWiFiOn(Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_10

    const/16 v12, 0x1f8

    .line 383
    :goto_12
    const-string v19, "WatsonHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "WatsonHandler.getToken() Exception status_code - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 386
    if-eqz v17, :cond_e

    .line 387
    :try_start_11
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedWriter;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_b

    .line 389
    :cond_e
    :goto_13
    if-eqz v6, :cond_f

    .line 390
    :try_start_12
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_c

    :cond_f
    :goto_14
    move v13, v12

    .end local v12    # "status_code":I
    .restart local v13    # "status_code":I
    goto/16 :goto_9

    .line 382
    .end local v13    # "status_code":I
    .restart local v12    # "status_code":I
    :cond_10
    const/16 v12, 0x1a1

    goto :goto_12

    .line 386
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v19

    :goto_15
    if-eqz v17, :cond_11

    .line 387
    :try_start_13
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedWriter;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_d

    .line 389
    :cond_11
    :goto_16
    if-eqz v6, :cond_12

    .line 390
    :try_start_14
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_e

    :cond_12
    :goto_17
    throw v19

    .line 415
    .end local v17    # "writer":Ljava/io/BufferedWriter;
    .restart local v3    # "builder":Landroid/net/Uri$Builder;
    .restart local v5    # "line":Ljava/lang/String;
    .restart local v7    # "query":Ljava/lang/String;
    .restart local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v10    # "sb":Ljava/lang/StringBuilder;
    .restart local v11    # "simulateTokenError":Z
    .restart local v15    # "url":Ljava/net/URL;
    .restart local v18    # "writer":Ljava/io/BufferedWriter;
    :cond_13
    if-eqz v10, :cond_14

    .line 416
    :try_start_15
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->json:Ljava/lang/String;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_0
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 422
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->is:Ljava/io/InputStream;

    move-object/from16 v19, v0

    if-eqz v19, :cond_15

    .line 423
    :try_start_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->is:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_15

    .line 425
    :cond_15
    :goto_18
    if-eqz v9, :cond_16

    .line 426
    :try_start_17
    invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_f

    .line 428
    :cond_16
    :goto_19
    if-eqz v16, :cond_1c

    .line 429
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v8, v9

    .end local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_7

    .line 419
    .end local v5    # "line":Ljava/lang/String;
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    .restart local v4    # "e":Ljava/lang/Exception;
    :cond_17
    const/16 v12, 0x1a1

    goto/16 :goto_4

    .line 422
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v19

    :goto_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->is:Ljava/io/InputStream;

    move-object/from16 v20, v0

    if-eqz v20, :cond_18

    .line 423
    :try_start_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->is:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_12

    .line 425
    :cond_18
    :goto_1b
    if-eqz v8, :cond_19

    .line 426
    :try_start_19
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_11

    .line 428
    :cond_19
    :goto_1c
    if-eqz v16, :cond_1a

    .line 429
    invoke-virtual/range {v16 .. v16}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1a
    throw v19

    .line 438
    :catch_4
    move-exception v4

    .line 439
    .local v4, "e":Lorg/json/JSONException;
    const-string v19, "JSON Parser"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Error parsing data "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 442
    .end local v4    # "e":Lorg/json/JSONException;
    :cond_1b
    const-string v19, "JSON Parser"

    const-string v20, "Error parsing data, json = null "

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 387
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .end local v11    # "simulateTokenError":Z
    :catch_5
    move-exception v19

    goto/16 :goto_0

    .line 390
    :catch_6
    move-exception v19

    goto/16 :goto_1

    .line 387
    .end local v3    # "builder":Landroid/net/Uri$Builder;
    .end local v7    # "query":Ljava/lang/String;
    .end local v15    # "url":Ljava/net/URL;
    .end local v18    # "writer":Ljava/io/BufferedWriter;
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    .restart local v17    # "writer":Ljava/io/BufferedWriter;
    :catch_7
    move-exception v19

    goto/16 :goto_b

    .line 390
    :catch_8
    move-exception v19

    goto/16 :goto_c

    .line 387
    .local v4, "e":Ljava/io/IOException;
    :catch_9
    move-exception v19

    goto/16 :goto_f

    .line 390
    :catch_a
    move-exception v19

    goto/16 :goto_10

    .line 387
    .local v4, "e":Ljava/lang/Exception;
    :catch_b
    move-exception v19

    goto/16 :goto_13

    .line 390
    :catch_c
    move-exception v19

    goto/16 :goto_14

    .line 387
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_d
    move-exception v20

    goto/16 :goto_16

    .line 390
    :catch_e
    move-exception v20

    goto/16 :goto_17

    .line 426
    .end local v17    # "writer":Ljava/io/BufferedWriter;
    .restart local v3    # "builder":Landroid/net/Uri$Builder;
    .restart local v5    # "line":Ljava/lang/String;
    .restart local v7    # "query":Ljava/lang/String;
    .restart local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v10    # "sb":Ljava/lang/StringBuilder;
    .restart local v11    # "simulateTokenError":Z
    .restart local v15    # "url":Ljava/net/URL;
    .restart local v18    # "writer":Ljava/io/BufferedWriter;
    :catch_f
    move-exception v19

    goto :goto_19

    .end local v5    # "line":Ljava/lang/String;
    .end local v9    # "reader":Ljava/io/BufferedReader;
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    .restart local v4    # "e":Ljava/lang/Exception;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :catch_10
    move-exception v19

    goto/16 :goto_6

    .end local v4    # "e":Ljava/lang/Exception;
    :catch_11
    move-exception v20

    goto :goto_1c

    .line 423
    :catch_12
    move-exception v20

    goto :goto_1b

    .line 422
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v9    # "reader":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v19

    move-object v8, v9

    .end local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    goto :goto_1a

    .line 423
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_13
    move-exception v19

    goto/16 :goto_5

    .line 418
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_14
    move-exception v4

    goto/16 :goto_3

    .line 423
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "line":Ljava/lang/String;
    .restart local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v10    # "sb":Ljava/lang/StringBuilder;
    :catch_15
    move-exception v19

    goto/16 :goto_18

    .line 386
    .end local v5    # "line":Ljava/lang/String;
    .end local v9    # "reader":Ljava/io/BufferedReader;
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    .end local v11    # "simulateTokenError":Z
    :catchall_3
    move-exception v19

    move-object/from16 v17, v18

    .end local v18    # "writer":Ljava/io/BufferedWriter;
    .restart local v17    # "writer":Ljava/io/BufferedWriter;
    goto/16 :goto_15

    .line 380
    .end local v17    # "writer":Ljava/io/BufferedWriter;
    .restart local v18    # "writer":Ljava/io/BufferedWriter;
    :catch_16
    move-exception v4

    move-object/from16 v17, v18

    .end local v18    # "writer":Ljava/io/BufferedWriter;
    .restart local v17    # "writer":Ljava/io/BufferedWriter;
    goto/16 :goto_11

    .line 375
    .end local v17    # "writer":Ljava/io/BufferedWriter;
    .restart local v18    # "writer":Ljava/io/BufferedWriter;
    :catch_17
    move-exception v4

    move-object/from16 v17, v18

    .end local v18    # "writer":Ljava/io/BufferedWriter;
    .restart local v17    # "writer":Ljava/io/BufferedWriter;
    goto/16 :goto_d

    .line 370
    .end local v17    # "writer":Ljava/io/BufferedWriter;
    .restart local v18    # "writer":Ljava/io/BufferedWriter;
    :catch_18
    move-exception v4

    move-object/from16 v17, v18

    .end local v18    # "writer":Ljava/io/BufferedWriter;
    .restart local v17    # "writer":Ljava/io/BufferedWriter;
    goto/16 :goto_a

    .end local v17    # "writer":Ljava/io/BufferedWriter;
    .restart local v5    # "line":Ljava/lang/String;
    .restart local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v10    # "sb":Ljava/lang/StringBuilder;
    .restart local v11    # "simulateTokenError":Z
    .restart local v18    # "writer":Ljava/io/BufferedWriter;
    :cond_1c
    move-object v8, v9

    .end local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_7
.end method

.method private getTranscription(JLjava/lang/String;)I
    .locals 35
    .param p1, "messageID"    # J
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    .line 98
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTranscription() messageID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fileName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/16 v31, 0x0

    .line 101
    .local v31, "urlConnection":Ljava/net/HttpURLConnection;
    const/4 v13, 0x0

    .line 102
    .local v13, "fileIs":Ljava/io/FileInputStream;
    const/16 v18, 0x0

    .line 103
    .local v18, "is":Ljava/io/InputStream;
    const/16 v22, 0x0

    .line 105
    .local v22, "out":Ljava/io/OutputStream;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/att/mobile/android/vvm/VVMApplication;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 106
    .local v15, "filePath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 107
    .local v16, "fullFilePath":Ljava/lang/String;
    const/4 v8, 0x0

    .line 108
    .local v8, "transcription":Ljava/lang/String;
    const/16 v28, 0xc8

    .line 109
    .local v28, "status_code":I
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 111
    .local v12, "file":Ljava/io/File;
    sget-object v2, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->accessToken:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessagePendingForDelete(J)Z

    move-result v2

    if-nez v2, :cond_8

    .line 114
    :try_start_0
    new-instance v30, Ljava/net/URL;

    const-string v2, "https://api.att.com/speech/v3/speechToText"

    move-object/from16 v0, v30

    invoke-direct {v0, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 115
    .local v30, "url":Ljava/net/URL;
    invoke-virtual/range {v30 .. v30}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object/from16 v31, v0

    .line 117
    move-object/from16 v0, v31

    instance-of v2, v0, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v2, :cond_0

    .line 119
    move-object/from16 v0, v31

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object v2, v0

    new-instance v3, Lcom/att/mobile/android/infra/network/TLSSocketFactory;

    invoke-direct {v3}, Lcom/att/mobile/android/infra/network/TLSSocketFactory;-><init>()V

    invoke-virtual {v2, v3}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 122
    :cond_0
    const v2, 0x36ee80

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 123
    const v2, 0x36ee80

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 124
    const-string v2, "POST"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 125
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 126
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 127
    const/4 v2, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 128
    const/4 v2, 0x0

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 130
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bearer "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->accessToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v2, "Content-type"

    const-string v3, "audio/amr"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v2, "Accept"

    const-string v3, "application/json"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v2, "X-SpeechContext"

    const-string v3, "VoiceMail"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v2, "X-Arg"

    const-string v3, "Language=en-us,NBest=0,FormatFlag=1"

    move-object/from16 v0, v31

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual/range {v31 .. v31}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v22

    .line 138
    new-instance v14, Ljava/io/FileInputStream;

    invoke-direct {v14, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    .end local v13    # "fileIs":Ljava/io/FileInputStream;
    .local v14, "fileIs":Ljava/io/FileInputStream;
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v14}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->writeStream(Ljava/io/OutputStream;Ljava/io/FileInputStream;)V

    .line 141
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->flush()V

    .line 144
    invoke-virtual/range {v31 .. v31}, Ljava/net/HttpURLConnection;->connect()V

    .line 146
    invoke-virtual/range {v31 .. v31}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v28

    .line 147
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WatsonHandler.getTranscription()  status_code - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_13
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_12
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 156
    if-eqz v14, :cond_1

    .line 157
    :try_start_2
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 159
    :cond_1
    :goto_0
    if-eqz v22, :cond_16

    .line 160
    :try_start_3
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v13, v14

    .line 164
    .end local v14    # "fileIs":Ljava/io/FileInputStream;
    .end local v30    # "url":Ljava/net/URL;
    .restart local v13    # "fileIs":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const-string v3, "simulatetranslerror"

    const-class v4, Ljava/lang/Boolean;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27

    .line 165
    .local v27, "simulateTranslError":Z
    if-eqz v27, :cond_4

    .line 166
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const-string v3, "TranslErrorCode"

    const-class v4, Ljava/lang/String;

    const-string v5, "501"

    invoke-virtual {v2, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/lang/String;

    .line 167
    .local v29, "status_code_string":Ljava/lang/String;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Unknown"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 168
    const-string v29, "501"

    .line 170
    :cond_3
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    .line 171
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WatsonHandler.getTranscription() simulate Transcription error Error code - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    .end local v29    # "status_code_string":Ljava/lang/String;
    :cond_4
    const/16 v2, 0xc8

    move/from16 v0, v28

    if-ne v0, v2, :cond_8

    .line 176
    const/16 v23, 0x0

    .line 178
    .local v23, "reader":Ljava/io/BufferedReader;
    :try_start_4
    invoke-virtual/range {v31 .. v31}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v18

    .line 180
    new-instance v24, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    const-string v3, "iso-8859-1"

    move-object/from16 v0, v18

    invoke-direct {v2, v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    const/16 v3, 0x8

    move-object/from16 v0, v24

    invoke-direct {v0, v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_11
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 181
    .end local v23    # "reader":Ljava/io/BufferedReader;
    .local v24, "reader":Ljava/io/BufferedReader;
    :try_start_5
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    .local v25, "sb":Ljava/lang/StringBuilder;
    const/16 v19, 0x0

    .line 183
    .local v19, "line":Ljava/lang/String;
    :goto_2
    invoke-virtual/range {v24 .. v24}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_e

    .line 184
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_2

    .line 206
    .end local v19    # "line":Ljava/lang/String;
    .end local v25    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v9

    move-object/from16 v23, v24

    .line 207
    .end local v24    # "reader":Ljava/io/BufferedReader;
    .local v9, "e":Ljava/lang/Exception;
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    :goto_3
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/Utils;->isWiFiOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v28, 0x1f8

    .line 208
    :goto_4
    const-string v2, "Buffer Error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error converting result "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 210
    if-eqz v18, :cond_5

    .line 211
    :try_start_7
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_d

    .line 213
    :cond_5
    :goto_5
    if-eqz v23, :cond_6

    .line 214
    :try_start_8
    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_e

    .line 217
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_6
    invoke-static/range {p1 .. p2}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->needRequestWatson(J)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 218
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    const v3, 0x7f0801e4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 221
    :cond_7
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageUID(J)J

    move-result-wide v6

    .line 222
    .local v6, "uid":J
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    sget-object v3, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->instance:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    move-wide/from16 v4, p1

    invoke-virtual/range {v2 .. v8}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageDetailsFromBodyText(Landroid/content/Context;JJLjava/lang/String;)Z

    .line 227
    .end local v6    # "uid":J
    .end local v23    # "reader":Ljava/io/BufferedReader;
    .end local v27    # "simulateTranslError":Z
    :cond_8
    return v28

    .line 160
    .end local v13    # "fileIs":Ljava/io/FileInputStream;
    .restart local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v30    # "url":Ljava/net/URL;
    :catch_1
    move-exception v2

    move-object v13, v14

    .end local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v13    # "fileIs":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .line 149
    .end local v30    # "url":Ljava/net/URL;
    :catch_2
    move-exception v26

    .line 150
    .local v26, "se":Ljava/net/SocketTimeoutException;
    :goto_7
    const/16 v28, 0x1f8

    .line 151
    :try_start_9
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WatsonHandler.getTranscription() ConnectTimeoutException status_code - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 156
    if-eqz v13, :cond_9

    .line 157
    :try_start_a
    invoke-virtual {v13}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 159
    :cond_9
    :goto_8
    if-eqz v22, :cond_2

    .line 160
    :try_start_b
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    goto/16 :goto_1

    :catch_3
    move-exception v2

    goto/16 :goto_1

    .line 152
    .end local v26    # "se":Ljava/net/SocketTimeoutException;
    :catch_4
    move-exception v9

    .line 153
    .restart local v9    # "e":Ljava/lang/Exception;
    :goto_9
    :try_start_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/att/mobile/android/infra/utils/Utils;->isWiFiOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v28, 0x1f8

    .line 154
    :goto_a
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WatsonHandler.getTranscription() Exception status_code - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 156
    if-eqz v13, :cond_a

    .line 157
    :try_start_d
    invoke-virtual {v13}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    .line 159
    :cond_a
    :goto_b
    if-eqz v22, :cond_2

    .line 160
    :try_start_e
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    goto/16 :goto_1

    :catch_5
    move-exception v2

    goto/16 :goto_1

    .line 153
    :cond_b
    const/16 v28, 0x190

    goto :goto_a

    .line 156
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    :goto_c
    if-eqz v13, :cond_c

    .line 157
    :try_start_f
    invoke-virtual {v13}, Ljava/io/FileInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_a

    .line 159
    :cond_c
    :goto_d
    if-eqz v22, :cond_d

    .line 160
    :try_start_10
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b

    :cond_d
    :goto_e
    throw v2

    .line 187
    .restart local v19    # "line":Ljava/lang/String;
    .restart local v24    # "reader":Ljava/io/BufferedReader;
    .restart local v25    # "sb":Ljava/lang/StringBuilder;
    .restart local v27    # "simulateTranslError":Z
    :cond_e
    :try_start_11
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->json:Ljava/lang/String;

    .line 188
    new-instance v2, Lorg/json/JSONObject;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->json:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "Recognition"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->jObj:Lorg/json/JSONObject;

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->jObj:Lorg/json/JSONObject;

    const-string v3, "NBest"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v33

    .line 191
    .local v33, "venues":Lorg/json/JSONArray;
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const-string v3, "MinConfidence"

    const-class v4, Ljava/lang/String;

    const-string v5, "0.0"

    invoke-virtual {v2, v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v20

    .line 192
    .local v20, "minConf":D
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WatsonHandler.getTranscription() minimal shown Confidence - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_f
    invoke-virtual/range {v33 .. v33}, Lorg/json/JSONArray;->length()I

    move-result v2

    move/from16 v0, v17

    if-ge v0, v2, :cond_10

    .line 194
    move-object/from16 v0, v33

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v32

    .line 195
    .local v32, "venueObject":Lorg/json/JSONObject;
    const-string v2, "Confidence"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 196
    .local v10, "conf":D
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WatsonHandler.getTranscription() got transcription - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ResultText"

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with Confidence - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    cmpl-double v2, v10, v20

    if-ltz v2, :cond_f

    .line 198
    const-string v2, "ResultText"

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 199
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WatsonHandler.getTranscription() succeeded transcription - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->parsePhoneFromTranscription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 201
    const-string v2, "WatsonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WatsonHandler.getTranscription() after parsing transcription - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :goto_10
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_f

    .line 203
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    const v3, 0x7f0801e4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    move-result-object v8

    goto :goto_10

    .line 210
    .end local v10    # "conf":D
    .end local v32    # "venueObject":Lorg/json/JSONObject;
    :cond_10
    if-eqz v18, :cond_11

    .line 211
    :try_start_12
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_c

    .line 213
    :cond_11
    :goto_11
    if-eqz v24, :cond_15

    .line 214
    :try_start_13
    invoke-virtual/range {v24 .. v24}, Ljava/io/BufferedReader;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_6

    move-object/from16 v23, v24

    .end local v24    # "reader":Ljava/io/BufferedReader;
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_6

    .end local v23    # "reader":Ljava/io/BufferedReader;
    .restart local v24    # "reader":Ljava/io/BufferedReader;
    :catch_6
    move-exception v2

    move-object/from16 v23, v24

    .end local v24    # "reader":Ljava/io/BufferedReader;
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_6

    .line 207
    .end local v17    # "i":I
    .end local v19    # "line":Ljava/lang/String;
    .end local v20    # "minConf":D
    .end local v25    # "sb":Ljava/lang/StringBuilder;
    .end local v33    # "venues":Lorg/json/JSONArray;
    .restart local v9    # "e":Ljava/lang/Exception;
    :cond_12
    const/16 v28, 0x1a1

    goto/16 :goto_4

    .line 210
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    :goto_12
    if-eqz v18, :cond_13

    .line 211
    :try_start_14
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_f

    .line 213
    :cond_13
    :goto_13
    if-eqz v23, :cond_14

    .line 214
    :try_start_15
    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedReader;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_10

    :cond_14
    :goto_14
    throw v2

    .line 157
    .end local v13    # "fileIs":Ljava/io/FileInputStream;
    .end local v23    # "reader":Ljava/io/BufferedReader;
    .end local v27    # "simulateTranslError":Z
    .restart local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v30    # "url":Ljava/net/URL;
    :catch_7
    move-exception v2

    goto/16 :goto_0

    .end local v14    # "fileIs":Ljava/io/FileInputStream;
    .end local v30    # "url":Ljava/net/URL;
    .restart local v13    # "fileIs":Ljava/io/FileInputStream;
    .restart local v26    # "se":Ljava/net/SocketTimeoutException;
    :catch_8
    move-exception v2

    goto/16 :goto_8

    .end local v26    # "se":Ljava/net/SocketTimeoutException;
    .restart local v9    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v2

    goto/16 :goto_b

    .end local v9    # "e":Ljava/lang/Exception;
    :catch_a
    move-exception v3

    goto/16 :goto_d

    .line 160
    :catch_b
    move-exception v3

    goto/16 :goto_e

    .line 211
    .restart local v17    # "i":I
    .restart local v19    # "line":Ljava/lang/String;
    .restart local v20    # "minConf":D
    .restart local v24    # "reader":Ljava/io/BufferedReader;
    .restart local v25    # "sb":Ljava/lang/StringBuilder;
    .restart local v27    # "simulateTranslError":Z
    .restart local v33    # "venues":Lorg/json/JSONArray;
    :catch_c
    move-exception v2

    goto :goto_11

    .end local v17    # "i":I
    .end local v19    # "line":Ljava/lang/String;
    .end local v20    # "minConf":D
    .end local v24    # "reader":Ljava/io/BufferedReader;
    .end local v25    # "sb":Ljava/lang/StringBuilder;
    .end local v33    # "venues":Lorg/json/JSONArray;
    .restart local v9    # "e":Ljava/lang/Exception;
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    :catch_d
    move-exception v2

    goto/16 :goto_5

    .line 214
    :catch_e
    move-exception v2

    goto/16 :goto_6

    .line 211
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_f
    move-exception v3

    goto :goto_13

    .line 214
    :catch_10
    move-exception v3

    goto :goto_14

    .line 210
    .end local v23    # "reader":Ljava/io/BufferedReader;
    .restart local v24    # "reader":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v2

    move-object/from16 v23, v24

    .end local v24    # "reader":Ljava/io/BufferedReader;
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    goto :goto_12

    .line 206
    :catch_11
    move-exception v9

    goto/16 :goto_3

    .line 156
    .end local v13    # "fileIs":Ljava/io/FileInputStream;
    .end local v23    # "reader":Ljava/io/BufferedReader;
    .end local v27    # "simulateTranslError":Z
    .restart local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v30    # "url":Ljava/net/URL;
    :catchall_3
    move-exception v2

    move-object v13, v14

    .end local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v13    # "fileIs":Ljava/io/FileInputStream;
    goto/16 :goto_c

    .line 152
    .end local v13    # "fileIs":Ljava/io/FileInputStream;
    .restart local v14    # "fileIs":Ljava/io/FileInputStream;
    :catch_12
    move-exception v9

    move-object v13, v14

    .end local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v13    # "fileIs":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .line 149
    .end local v13    # "fileIs":Ljava/io/FileInputStream;
    .restart local v14    # "fileIs":Ljava/io/FileInputStream;
    :catch_13
    move-exception v26

    move-object v13, v14

    .end local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v13    # "fileIs":Ljava/io/FileInputStream;
    goto/16 :goto_7

    .end local v30    # "url":Ljava/net/URL;
    .restart local v17    # "i":I
    .restart local v19    # "line":Ljava/lang/String;
    .restart local v20    # "minConf":D
    .restart local v24    # "reader":Ljava/io/BufferedReader;
    .restart local v25    # "sb":Ljava/lang/StringBuilder;
    .restart local v27    # "simulateTranslError":Z
    .restart local v33    # "venues":Lorg/json/JSONArray;
    :cond_15
    move-object/from16 v23, v24

    .end local v24    # "reader":Ljava/io/BufferedReader;
    .restart local v23    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_6

    .end local v13    # "fileIs":Ljava/io/FileInputStream;
    .end local v17    # "i":I
    .end local v19    # "line":Ljava/lang/String;
    .end local v20    # "minConf":D
    .end local v23    # "reader":Ljava/io/BufferedReader;
    .end local v25    # "sb":Ljava/lang/StringBuilder;
    .end local v27    # "simulateTranslError":Z
    .end local v33    # "venues":Lorg/json/JSONArray;
    .restart local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v30    # "url":Ljava/net/URL;
    :cond_16
    move-object v13, v14

    .end local v14    # "fileIs":Ljava/io/FileInputStream;
    .restart local v13    # "fileIs":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method private isCanConnect()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 240
    iget-object v5, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->context:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 242
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 243
    .local v2, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 245
    .local v1, "mobile":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v3, v4

    :cond_1
    return v3
.end method

.method public static needRequestWatson(J)Z
    .locals 6
    .param p0, "messageID"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 478
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageTranscription(J)Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "transcription":Ljava/lang/String;
    const-string v1, "WatsonHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "needRequestWatson messageID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " existing transcription = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    const-string v1, "WatsonHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "needRequestWatson ModelManager.getInstance().getClientSideTranscription() = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 482
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getClientSideTranscription()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 481
    invoke-static {v1, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    const-string v4, "WatsonHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "needRequestWatson returning  "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 484
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getClientSideTranscription()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessageHasTranscription(J)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 483
    invoke-static {v4, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getClientSideTranscription()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->isMessageHasTranscription(J)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    return v2

    :cond_0
    move v1, v3

    .line 484
    goto :goto_0

    :cond_1
    move v2, v3

    .line 486
    goto :goto_1
.end method

.method private onHttpError(IJLjava/lang/String;)V
    .locals 16
    .param p1, "status_code"    # I
    .param p2, "messageID"    # J
    .param p4, "fileName"    # Ljava/lang/String;

    .prologue
    .line 305
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageWatsonStatus(J)I

    move-result v2

    .line 306
    .local v2, "watson_status":I
    invoke-direct/range {p0 .. p0}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->isCanConnect()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 307
    const/16 v3, 0x190

    move/from16 v0, p1

    if-eq v0, v3, :cond_0

    const/16 v3, 0x191

    move/from16 v0, p1

    if-ne v0, v3, :cond_1

    :cond_0
    const/4 v3, 0x5

    if-ne v2, v3, :cond_2

    :cond_1
    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    .line 310
    :cond_2
    const-string v3, "WatsonHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WatsonHandler.onHttpError() messageID - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " reset Token "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-static {}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->resetToken()V

    .line 312
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageTranscriptionError(J)Z

    .line 329
    :cond_3
    :goto_0
    return-void

    .line 313
    :cond_4
    const/16 v3, 0x1f8

    move/from16 v0, p1

    if-ne v0, v3, :cond_7

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    .line 314
    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    const v7, 0x493e0

    .line 315
    .local v7, "rt":I
    :goto_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    const/4 v8, 0x6

    .line 316
    .local v8, "mesSt":I
    :goto_2
    const-string v3, "WatsonHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WatsonHandler.onHttpError() messageID - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " error = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " scheduling retry in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms. Status = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v3, p0

    move-wide/from16 v4, p2

    move-object/from16 v6, p4

    .line 317
    invoke-direct/range {v3 .. v8}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->scheduleRetry(JLjava/lang/String;II)V

    goto :goto_0

    .line 314
    .end local v7    # "rt":I
    .end local v8    # "mesSt":I
    :cond_5
    const v7, 0x1b7740

    goto :goto_1

    .line 315
    .restart local v7    # "rt":I
    :cond_6
    const/4 v8, 0x7

    goto :goto_2

    .line 318
    .end local v7    # "rt":I
    :cond_7
    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    const/4 v3, -0x1

    move/from16 v0, p1

    if-eq v0, v3, :cond_3

    const/16 v3, 0x1a1

    move/from16 v0, p1

    if-eq v0, v3, :cond_3

    .line 319
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "TokenRetryFail"

    const-class v5, Ljava/lang/Boolean;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_8

    .line 320
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "simulatetokenerror"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 322
    :cond_8
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "TranslRetryFail"

    const-class v5, Ljava/lang/Boolean;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getSharedPreferenceValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_9

    .line 323
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v3

    const-string v4, "simulatetranslerror"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setSharedPreference(Ljava/lang/String;Ljava/lang/Object;)V

    .line 326
    :cond_9
    const v13, 0xea60

    const/4 v14, 0x5

    move-object/from16 v9, p0

    move-wide/from16 v10, p2

    move-object/from16 v12, p4

    invoke-direct/range {v9 .. v14}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->scheduleRetry(JLjava/lang/String;II)V

    goto/16 :goto_0
.end method

.method private parsePhoneFromTranscription(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "transcription"    # Ljava/lang/String;

    .prologue
    .line 282
    const-string v5, "_END_PHONE"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 283
    .local v1, "endposition":I
    const/4 v5, -0x1

    if-ne v1, v5, :cond_0

    .line 284
    const-string v5, "WatsonHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WatsonHandler.parsePhoneFromTranscription() before final transcription - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const-string v5, "_\\p{L}+"

    const-string v6, ""

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 286
    const-string v5, "WatsonHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WatsonHandler.parsePhoneFromTranscription() final transcription - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, p1

    .line 301
    .end local p1    # "transcription":Ljava/lang/String;
    .local v4, "transcription":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 289
    .end local v4    # "transcription":Ljava/lang/String;
    .restart local p1    # "transcription":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 290
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v5, "_PHONE"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 291
    .local v3, "startposition":I
    const-string v5, "_PHONE"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v3

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 292
    .local v2, "phonenumber":Ljava/lang/String;
    const-string v5, "WatsonHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WatsonHandler.parsePhoneFromTranscription() phonenumber - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 294
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_END_PHONE"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 295
    const-string v5, "WatsonHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WatsonHandler.parsePhoneFromTranscription() parsing transcription - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    invoke-direct {p0, p1}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->parsePhoneFromTranscription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 298
    const-string v5, "WatsonHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WatsonHandler.parsePhoneFromTranscription() before final transcription - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    const-string v5, "_\\p{L}+"

    const-string v6, ""

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 300
    const-string v5, "WatsonHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WatsonHandler.parsePhoneFromTranscription() final transcription - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, p1

    .line 301
    .end local p1    # "transcription":Ljava/lang/String;
    .restart local v4    # "transcription":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private requestTranscription(JLjava/lang/String;)I
    .locals 5
    .param p1, "messageID"    # J
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0xc8

    const/4 v3, 0x7

    .line 250
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getMessageWatsonStatus(J)I

    move-result v1

    .line 251
    .local v1, "watson_status":I
    const/16 v0, 0xc8

    .line 252
    .local v0, "status_code":I
    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    if-ne v1, v3, :cond_3

    .line 253
    :cond_0
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, p1, p2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageWatsonStatus(JI)Z

    .line 260
    :goto_0
    invoke-direct {p0}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->isCanConnect()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 261
    sget-object v2, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->accessToken:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 262
    invoke-static {}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->getInstance()Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    move-result-object v2

    invoke-direct {v2, p1, p2, p3}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->getToken(JLjava/lang/String;)I

    move-result v0

    .line 264
    :cond_1
    if-ne v0, v4, :cond_2

    sget-object v2, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->accessToken:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 265
    invoke-direct {p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->getTranscription(JLjava/lang/String;)I

    move-result v0

    .line 267
    if-eq v0, v4, :cond_2

    .line 268
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, p1, p2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageWatsonStatus(JI)Z

    .line 279
    :cond_2
    :goto_1
    return v0

    .line 254
    :cond_3
    const/4 v2, 0x6

    if-ne v1, v2, :cond_4

    .line 255
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageWatsonStatus(JI)Z

    goto :goto_0

    .line 257
    :cond_4
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, p1, p2, v3}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageWatsonStatus(JI)Z

    goto :goto_0

    .line 277
    :cond_5
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static resetToken()V
    .locals 2

    .prologue
    .line 473
    const/4 v0, 0x0

    sput-object v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->accessToken:Ljava/lang/String;

    .line 474
    const-string v0, "WatsonHandler"

    const-string v1, "WatsonHandler.resetToken()"

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    return-void
.end method

.method private scheduleRetry(JLjava/lang/String;II)V
    .locals 7
    .param p1, "messageID"    # J
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "retryTime"    # I
    .param p5, "messageStatus"    # I

    .prologue
    .line 450
    invoke-static {}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->getInstance()Lcom/att/mobile/android/vvm/model/db/ModelManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p5}, Lcom/att/mobile/android/vvm/model/db/ModelManager;->setMessageWatsonStatus(JI)Z

    .line 451
    const-string v2, "WatsonHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WatsonHandler.scheduleRetry() messageID - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " retry time = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "message status = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 453
    .local v3, "handler":Landroid/os/Handler;
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 454
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;

    move-object v2, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;-><init>(Lcom/att/mobile/android/vvm/watson/WatsonHandler;Landroid/os/Handler;JLjava/lang/String;)V

    .line 469
    .local v1, "doAsynchronousTask":Ljava/util/TimerTask;
    int-to-long v4, p4

    invoke-virtual {v0, v1, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 470
    return-void
.end method

.method private writeStream(Ljava/io/OutputStream;Ljava/io/FileInputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "fileIs"    # Ljava/io/FileInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    const/16 v2, 0x400

    new-array v0, v2, [B

    .line 233
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {p2, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v1

    .local v1, "count":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 234
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 236
    :cond_0
    return-void
.end method


# virtual methods
.method public callWatsonTranscriptionTask(JLjava/lang/String;)V
    .locals 3
    .param p1, "messageID"    # J
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    .line 515
    const-string v0, "WatsonHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WatsonHandler.callWatsonTranscriptionTask messageID - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fileName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    new-instance v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;-><init>(Lcom/att/mobile/android/vvm/watson/WatsonHandler;JLjava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 517
    return-void
.end method

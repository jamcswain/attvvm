.class Lcom/att/mobile/android/vvm/watson/WatsonHandler$1$1;
.super Ljava/lang/Object;
.source "WatsonHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;


# direct methods
.method constructor <init>(Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;

    .prologue
    .line 457
    iput-object p1, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1$1;->this$1:Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 460
    :try_start_0
    new-instance v1, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;

    iget-object v2, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1$1;->this$1:Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;

    iget-object v2, v2, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;->this$0:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    iget-object v3, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1$1;->this$1:Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;

    iget-wide v4, v3, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;->val$messageID:J

    iget-object v3, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1$1;->this$1:Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;

    iget-object v3, v3, Lcom/att/mobile/android/vvm/watson/WatsonHandler$1;->val$fileName:Ljava/lang/String;

    invoke-direct {v1, v2, v4, v5, v3}, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;-><init>(Lcom/att/mobile/android/vvm/watson/WatsonHandler;JLjava/lang/String;)V

    .line 461
    .local v1, "performBackgroundTask":Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    .end local v1    # "performBackgroundTask":Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;
    :goto_0
    return-void

    .line 462
    :catch_0
    move-exception v0

    .line 463
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "scheduleRetry"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error execute RunWatsonTranscriptionTask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

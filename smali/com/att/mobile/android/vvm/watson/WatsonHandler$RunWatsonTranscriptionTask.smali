.class Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;
.super Landroid/os/AsyncTask;
.source "WatsonHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/att/mobile/android/vvm/watson/WatsonHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RunWatsonTranscriptionTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field fileName:Ljava/lang/String;

.field messageID:J

.field final synthetic this$0:Lcom/att/mobile/android/vvm/watson/WatsonHandler;


# direct methods
.method public constructor <init>(Lcom/att/mobile/android/vvm/watson/WatsonHandler;JLjava/lang/String;)V
    .locals 0
    .param p2, "messageID"    # J
    .param p4, "fileName"    # Ljava/lang/String;

    .prologue
    .line 492
    iput-object p1, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->this$0:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    .line 493
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 494
    iput-wide p2, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->messageID:J

    .line 495
    iput-object p4, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->fileName:Ljava/lang/String;

    .line 496
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 5
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 500
    iget-object v1, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->this$0:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->messageID:J

    iget-object v4, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->fileName:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->access$000(Lcom/att/mobile/android/vvm/watson/WatsonHandler;JLjava/lang/String;)I

    move-result v0

    .line 501
    .local v0, "result_code":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 489
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 506
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 507
    const-string v0, "WatsonHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WatsonHandler.RunWatsonTranscriptionTask.onPostExecute HTTP request failed messageID - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->messageID:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status_code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/att/mobile/android/infra/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    sget-object v0, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->instance:Lcom/att/mobile/android/vvm/watson/WatsonHandler;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-wide v2, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->messageID:J

    iget-object v4, p0, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->fileName:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/att/mobile/android/vvm/watson/WatsonHandler;->access$100(Lcom/att/mobile/android/vvm/watson/WatsonHandler;IJLjava/lang/String;)V

    .line 510
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 489
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/att/mobile/android/vvm/watson/WatsonHandler$RunWatsonTranscriptionTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.class public Lcom/squareup/picasso/PicassoTools;
.super Ljava/lang/Object;
.source "PicassoTools.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearCache(Lcom/squareup/picasso/Picasso;)V
    .locals 1
    .param p0, "p"    # Lcom/squareup/picasso/Picasso;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/squareup/picasso/Picasso;->cache:Lcom/squareup/picasso/Cache;

    invoke-interface {v0}, Lcom/squareup/picasso/Cache;->clear()V

    .line 10
    return-void
.end method
